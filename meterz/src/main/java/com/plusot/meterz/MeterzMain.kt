package com.plusot.meterz

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemLongClickListener
import android.widget.ListAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.plusot.meterz.activities.ActivityResult
import com.plusot.meterz.activities.DataSelectActivity
import com.plusot.meterz.dialogs.MenuAdapter
import com.plusot.meterz.dialogs.SessionAdapter
import com.plusot.meterz.dialogs.SplashDialog
import com.plusot.meterz.notifications.Notifications.showNotificationAlert
import com.plusot.meterz.preferences.PreferenceKey
import com.plusot.meterz.preferences.SettingsActivity
import com.plusot.meterz.preferences.SettingsType
import com.plusot.meterz.widget.MyMapView
import com.plusot.meterz.widget.RelativeDataView
import com.plusot.sensez.SensezLib
import com.plusot.sensez.device.Device
import com.plusot.sensez.device.Man
import com.plusot.sensez.device.ScanManager
import com.plusot.sensez.device.ScannedDevice
import com.plusot.sensez.internal.GpsDevice
import com.plusot.shareddata.db.DbData
import com.plusot.shareddata.share.HttpTask
import com.plusot.util.Globals
import com.plusot.util.data.Data
import com.plusot.util.data.DataType
import com.plusot.util.dialog.Alerts
import com.plusot.util.kotlin.getString
import com.plusot.util.logging.LLog
import com.plusot.util.share.Http
import com.plusot.util.share.LogMail
import com.plusot.util.share.SendMail
import com.plusot.util.util.*
import java.lang.ref.WeakReference
import java.util.*

// 66FJgj
class MeterzMain : Activity(), Man.Listener, MyMapView.Listener, SensezLib.EventListener {
    private val views = arrayOfNulls<RelativeDataView>(9)
    private var clickZone: View? = null
    private var menuClickListener: View.OnClickListener? = null
    private lateinit var menuItems: List<Tuple<Int, String>>
    private var mayStopSenzezLib = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MeterzApp.mainCreated = true
        if (PreferenceKey.SCREEN_ON.isTrue) {
            LLog.d("Screen will stay on")
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            LLog.d("Screen may dim")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
        }
        SensezLib.addEventListener(this)
        GpsDevice.gpsInterval = 1500
        GpsDevice.gpsMinDistance = 0
        val permits: Array<SensezLib.Permit> = when {
            Build.VERSION.SDK_INT >= 30 -> {
                arrayOf(
//                    SensezLib.Permit(arrayOf(Manifest.permission.READ_PHONE_STATE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BLUETOOTH_ADMIN)),
                    SensezLib.Permit(arrayOf(Manifest.permission.FOREGROUND_SERVICE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BODY_SENSORS)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACTIVITY_RECOGNITION))
                )
            }
            Build.VERSION.SDK_INT >= 29 -> {
                arrayOf(
//                    SensezLib.Permit(arrayOf(Manifest.permission.READ_PHONE_STATE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BLUETOOTH_ADMIN)),
                    SensezLib.Permit(arrayOf(Manifest.permission.FOREGROUND_SERVICE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BODY_SENSORS)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACTIVITY_RECOGNITION))
                )
            }
            Build.VERSION.SDK_INT >= 28 -> {
                arrayOf(
//                    SensezLib.Permit(arrayOf(Manifest.permission.READ_PHONE_STATE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BLUETOOTH_ADMIN)),
                    SensezLib.Permit(arrayOf(Manifest.permission.FOREGROUND_SERVICE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BODY_SENSORS))
                )
            }
            else -> {
                arrayOf(
//                    SensezLib.Permit(arrayOf(Manifest.permission.READ_PHONE_STATE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)),
                    SensezLib.Permit(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)),
                    SensezLib.Permit(arrayOf(Manifest.permission.BLUETOOTH_ADMIN)))
            }
        }
        SensezLib.create(
            application,
            this,
            EnumSet.of(
                SensezLib.Option.USE_HTTP,
                SensezLib.Option.USE_DB,
                SensezLib.Option.USE_NMEA,  //                        SensezLib.Option.CHECK_GPS,
                SensezLib.Option.DEFAULT_FIELDS
            ),
            SensezLib.Permits(
                permits,
                false,
                false
            )
        )
        setContentView(R.layout.activity_main)
        (findViewById<View>(R.id.appVersion) as TextView).text = UserInfo.appVersion()
        //        (clickZone2 = findViewById(R.id.clickZone2)).setOnClickListener(view -> startSpeechRecognition());
        menuItems = listOf (
            Tuple(R.mipmap.ic_start, getString(R.string.start_and_stop)),
            Tuple(R.mipmap.ic_sensors, getString(R.string.values_and_sensors)),
            Tuple(R.mipmap.ic_map, getString(R.string.chooseMapType)),
            Tuple(R.mipmap.ic_gpx, getString(R.string.GPX)),
            Tuple(R.mipmap.ic_share, getString(R.string.share)),
            Tuple(R.mipmap.ic_www, getString(R.string.www)),
            Tuple(R.mipmap.ic_preferences, getString(R.string.settings)),
            Tuple(R.mipmap.ic_finish, getString(R.string.stop_app))
        )

        showNotificationAlert(this, intent)
        instanceRef = WeakReference(this)
        hideSystemUI()
    }

    private fun hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        val windowInsetsController = WindowCompat.getInsetsController(
            window, window.decorView
        )
        // Configure the behavior of the hidden system bars
        windowInsetsController!!.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        // Hide both the status bar and the navigation bar
        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.navigationBars())
    }

    private enum class MenuOption {
        QUERY_HISTORY,
        SELECT_DEVICE,
        CHOOSE_MAP_SOURCE,
        GPX,
        SHARE,
        INTERNET,
        SETTINGS,
        STOP,
        NO_CHOICE;

        companion object {
            fun fromInt(id: Int): MenuOption {
                return if (id < 0 || id >= values().size) NO_CHOICE else values()[id]
            }
        }
    }

    private fun share() {
        val menuItems = mutableListOf(
            R.string.mailGpx.getString(),
            R.string.sendLink.getString()
        )

        Alerts.showItemsDialog(this, R.string.menu, menuItems.toTypedArray()) { _, whichString, _ ->
            when (whichString) {
                R.string.mailGpx.getString() -> {
                    val list: FileExplorer.FileList? = FileExplorer.getFileList(arrayOf(
                        Globals.getDataPath(),
//                            Globals.getDownloadPath(),
                        MeterzGlobals.getGpxPath()
//                            Globals.getSdcardPath() + "/osmand/tracks/"
                    ), arrayOf(".gpx"),
                        false
                    )
                    if (list != null) {
                        Alerts.showSingleChoiceDialog(this, R.string.gpxTitle, list.items, null) {
                                clickResult, whichString2, _ ->
                            when (clickResult) {
                                Alerts.ClickResult.YES -> {
                                    val path = list.lookup[whichString2]
                                    if (path != null) SendMail(
                                        path,
                                        "Meterz GPX $whichString2",
                                        R.string.sendMail.getString(whichString2)) {}
                                }
                                Alerts.ClickResult.NO -> {}
                                Alerts.ClickResult.CANCEL -> {}
                                Alerts.ClickResult.NEUTRAL -> {}
                                Alerts.ClickResult.DISMISS -> {}
                                null -> {}
                            }
                        }
                    }
                }
                R.string.sendLink.getString() -> {
                    Alerts.showYesNoDialog(this@MeterzMain, R.string.shareTitle, R.string.shareMsg, R.string.allActivities, R.string.thisActivity, { clickResult: Alerts.ClickResult? ->
                        when (clickResult) {
                            Alerts.ClickResult.YES -> share(true)
                            Alerts.ClickResult.NO -> share(false)
                            Alerts.ClickResult.CANCEL -> {}
                            Alerts.ClickResult.NEUTRAL -> {}
                            Alerts.ClickResult.DISMISS -> {}
                            null -> {}
                        }
                    }, R.mipmap.ic_share)
                }
            }
        }
    }

    private fun settings(settingsType: SettingsType) {
        val intent = Intent(this, SettingsActivity::class.java)
            .putExtra(SettingsActivity.SETTINGS_CHOICE, settingsType.ordinal)
        startActivityForResult(intent, ActivityResult.RESULT_PREFERENCES.ordinal)
    }

    private fun settings() {
        val menuItems = mutableListOf(
            R.string.header_title_application_preferences.getString(),
            R.string.header_title_bicycle_preferences.getString(),
            R.string.header_title_map_preferences.getString(),
            R.string.header_title_server_preferences.getString(),
            R.string.header_title_technology_preferences.getString(),
        )

        Alerts.showItemsDialog(this, R.string.menu, menuItems.toTypedArray()) { _, whichString, _ ->
            settings(when (whichString) {
                R.string.header_title_application_preferences.getString() -> SettingsType.ApplicationSettings
                R.string.header_title_bicycle_preferences.getString() -> SettingsType.BicycleSettings
                R.string.header_title_map_preferences.getString() -> SettingsType.MapSettings
                R.string.header_title_server_preferences.getString() -> SettingsType.ServerSettings
                R.string.header_title_technology_preferences.getString() -> SettingsType.TechnologiesSettings
                else -> SettingsType.ApplicationSettings
            })
        }
    }

    private fun handleMenuOption(which: MenuOption) {
        val myMapView = MyMapView.lastInstance
        when (which) {
            MenuOption.QUERY_HISTORY -> queryHistory()
            MenuOption.SELECT_DEVICE -> startActivity(Intent(this@MeterzMain, DataSelectActivity::class.java))
            MenuOption.CHOOSE_MAP_SOURCE -> myMapView?.chooseMapType()
            MenuOption.GPX -> myMapView?.gpx
            MenuOption.SHARE -> share()
            MenuOption.INTERNET -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(com.plusot.shareddata.preferences.PreferenceKey.getSessionsUrl())))
            MenuOption.SETTINGS -> settings()
            MenuOption.STOP -> stopAppDialog()
            MenuOption.NO_CHOICE -> {}
        }
    }

    private fun showMenu() {
        val listAdapter: ListAdapter = MenuAdapter(this, menuItems)
        Alerts.showItemsDialog(this, R.string.menu, listAdapter, { _, _, which -> handleMenuOption(MenuOption.fromInt(which)) }, 10000)
    }

    @SuppressLint("InflateParams")
    private fun queryHistory() {
        val sessions: MutableList<DbData.SessionObj> = ArrayList()
        sessions.addAll(DbData.getSessions(false))
        var freshStart = false
        if (DbData.getSession(Globals.getSessionId()) == null) freshStart = true
        sessions.sortWith({ o1: DbData.SessionObj, o2: DbData.SessionObj -> ((o2.timeStamp - o1.timeStamp) / Math.abs(o2.timeStamp - o1.timeStamp)).toInt() })
        val listSize = sessions.size
        if (listSize == 0) {
            start = false
            fadedInClickZone()
            clear()
            Device.setPausing(false)
            Man.setShowNew(true)
            explain()
            return
        }
        val builder = AlertDialog.Builder(this)
        val inflater = builder.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
            ?: return
        builder
            .setTitle(R.string.activity)
            .setCancelable(true)
            .setPositiveButton(R.string.startNewActivity) { _, _ ->
                start = false
                fadedInClickZone()
                clear()
                Device.setPausing(false)
                Man.setShowNew(true)
                explain()
            }
        builder.setOnCancelListener { _ ->
            start = false
            fadedInClickZone()
            Device.setPausing(true)
            explain()
        }
        if (!freshStart) {
            if (Device.isPausing()) {
                builder.setNeutralButton(R.string.resume) { _, _ ->
                    Device.setPausing(false)
                    if (start || !Man.hasData()) {
                        start = false
                        fadedInClickZone()
                        clear()
                        DbData.restoreData(Globals.getSessionId(), DEFAULT_RESTORE_INTERVAL) {
                            Man.setShowNew(true)
                            explain()
                        }
                    }
                }
            } else {
                builder.setNeutralButton(R.string.stop) { _, _ -> Device.setPausing(true) }
            }
        }
        var view: View
        builder.setView(inflater.inflate(R.layout.dialog_session, null).also { view = it })
        val listView = view.findViewById<ListView>(R.id.sessionListView)
        listView.adapter = SessionAdapter(this, R.layout.adapter_session, sessions)
        val dialog: Dialog = builder.create()
        listView.onItemClickListener = OnItemClickListener { _, _, position, _ ->
            start = false
            fadedInClickZone()
            //LLog.i("Item clicked " + id + "   " + position);
            clear()
            val sessionObj: DbData.SessionObj? = sessions[position]
            if (sessionObj != null) {
                //final String date = SessionAdapter.sessionIdToDate(sessionObj.getSessionId());
                //ToastHelper.showToastLong(getString(R.string.replayStarting, date));
                DbData.restoreData(sessions[position].sessionId, HISTORY_RESTORE_INTERVAL) {
                    ToastHelper.showToastLong(R.string.done)
                    explain()
                }
            }
            dialog.dismiss()
        }
        listView.onItemLongClickListener = OnItemLongClickListener { _, _, position, _ ->
            Alerts.showOkDialog(this, R.string.resetSharedTitle, R.string.resetSharedMsg, 0) { clickResult: Alerts.ClickResult? ->
                when (clickResult) {
                    Alerts.ClickResult.YES -> {
                        val sessionObj = sessions[position]
                        DbData.updateShared(sessionObj.sessionId, DbData.Shared.NOT_SHARED)
                        ToastHelper.showToastLong(R.string.resetShared)
                    }
                    Alerts.ClickResult.NO -> {}
                    Alerts.ClickResult.CANCEL -> {}
                    Alerts.ClickResult.NEUTRAL -> {}
                    Alerts.ClickResult.DISMISS -> {}
                    null -> {}
                }
            }
            true
        }
        dialog.show()
    }

    private fun explain() {
        if (com.plusot.sensez.preferences.PreferenceKey.EXPLAIN_MENU.isTrue) {
            Alerts.showYesNoDialog(this, R.string.usageTitle, R.string.usageMsg, R.string.ok, R.string.noMore, { clickResult: Alerts.ClickResult? ->
                when (clickResult) {
                    Alerts.ClickResult.YES -> {
                    }
                    Alerts.ClickResult.NO -> com.plusot.sensez.preferences.PreferenceKey.EXPLAIN_MENU.set(false)
                    Alerts.ClickResult.CANCEL -> {
                    }
                    Alerts.ClickResult.NEUTRAL -> {
                    }
                    Alerts.ClickResult.DISMISS -> {}
                    null -> {}
                }
            }, -1)
        }
    }

    private fun clear() {
        Man.reset()
        MyMapView.clear()
        for (view in views) view?.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        HttpTask.setListener { error: Http.Error, time: Long ->
            if (error == Http.Error.NOERROR) SleepAndWake.runInMain({
                (findViewById<View>(R.id.cloudInfo) as TextView).text = getString(R.string.cloudTime,
                    TimeUtil.formatTime(if (time < 0) System.currentTimeMillis() else time, "HH:mm:ss")
                )
                val vOn = findViewById<View>(R.id.cloud_led_on)
                val vOff = findViewById<View>(R.id.cloud_led_off)
                if (vOn.visibility == View.GONE) {
                    vOn.visibility = View.VISIBLE
                    vOff.visibility = View.GONE
                    SleepAndWake.runInMain({
                        vOff.visibility = View.VISIBLE
                        vOn.visibility = View.GONE
                    }, 1000, "$CLASS_TAG.onResume")
                }
            }, "$CLASS_TAG.onResume")
        }
        Man.addListener(this)
        val metrics = Globals.getMetrics()
        LLog.i("Screen width = " + Globals.screenWidth + " x height = " + Globals.screenHeight + ", xdpi = " + metrics.xdpi + ", ydpi = " + metrics.ydpi)
        Globals.runMode = Globals.RunMode.RUN
        for (view in views) if (view != null) {
            view.checkSize()
            view.checkVisibility()
            view.setOnClickListener(menuClickListener)
        }

//        startAsyncTask();
        MyMapView.onResume(this)
        super.onResume()
    }

    override fun onDestroy() {
        LLog.i("Destroy called")
        super.onDestroy()
    }

    private fun stopAppDialog() {
        Alerts.showYesNoNeutralDialog(this, R.string.quit_title, R.string.quit_message, R.string.share) { clickResult: Alerts.ClickResult? ->
            when (clickResult) {
                Alerts.ClickResult.YES -> {
                    mayStopSenzezLib = true
                    super@MeterzMain.onBackPressed()
                }
                Alerts.ClickResult.NO -> {
                }
                Alerts.ClickResult.CANCEL -> {
                }
                Alerts.ClickResult.NEUTRAL -> share(false)
                Alerts.ClickResult.DISMISS -> {}
                null -> {}
            }
        }
    }

    override fun onBackPressed() {
        stopAppDialog()
    }

    private fun share(all: Boolean) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        if (all) {
            intent.putExtra(Intent.EXTRA_TEXT, com.plusot.shareddata.preferences.PreferenceKey.getSessionsUrl())
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.my_activities))
        } else {
            intent.putExtra(Intent.EXTRA_TEXT, com.plusot.shareddata.preferences.PreferenceKey.getMapUrl())
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.my_activity, SessionAdapter.sessionIdToDate(Globals.getSessionId())))
        }
        startActivity(Intent.createChooser(intent, "Share"))
    }

    override fun onPause() {
        Man.removeListener(this)
        HttpTask.clearListener()
        if (mayStopSenzezLib) SensezLib.close()
        super.onPause()
    }

    private fun fadeIn(view: View?, fadeInDuration: Long = 1000.toLong(), fadeOutDelay: Long = 2000.toLong(), isAlpha: Boolean = false, minAlpha: Float = 0.0f, maxAlpha: Float = 1.0f) {
        if (view == null || view.tag != null) return
        view.tag = true
        val fadeIn: Animation = AlphaAnimation(if (isAlpha) minAlpha else 0.0f, if (isAlpha) maxAlpha else 1.0f)
        fadeIn.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                if (isAlpha) {
                    view.alpha = maxAlpha
                } else {
                    view.visibility = View.VISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animation) {
                SleepAndWake.runInMain({ fadeOut(view, fadeOutDelay, isAlpha, minAlpha, maxAlpha) }, "MeterzBaseMain.fadeIn")
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        fadeIn.duration = fadeInDuration
        fadeIn.startOffset = 100
        view.startAnimation(fadeIn)
    }

    private fun fadedInClickZone() {
        fadeIn(clickZone, 1000, 5000, true, 0.2f, 0.8f)
    }

    private fun fadeOut(view: View?, offset: Long, isAlpha: Boolean, minAlpha: Float, maxAlpha: Float) {
        if (view == null) return
        val fadeOut: Animation = AlphaAnimation(if (isAlpha) maxAlpha else 1.0f, if (isAlpha) minAlpha else 0.0f)
        fadeOut.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                if (isAlpha) view.alpha = minAlpha else view.visibility = View.GONE
                view.tag = null
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        fadeOut.duration = 2000
        fadeOut.startOffset = offset
        view.startAnimation(fadeOut)
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val keyCode = event.keyCode
        var result = false

        val mgr = getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        if (mgr != null) when (keyCode) {
            KeyEvent.KEYCODE_CAMERA -> {
            }
            KeyEvent.KEYCODE_VOLUME_UP -> {
                mgr.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI or AudioManager.FLAG_PLAY_SOUND)
                result = true
            }
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                mgr.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI or AudioManager.FLAG_PLAY_SOUND)
                result = true
            }
        }
        if (!result) {
            result = super.dispatchKeyEvent(event)
        }
        return result
    }

    private var proxEvent: Long = 0
    override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
        if (data.hasDataType(DataType.PROXIMITY) && System.currentTimeMillis() - proxEvent > 5000) {
            proxEvent = System.currentTimeMillis()
            SleepAndWake.runInMain{ fadedInClickZone() }
        }
    }

    override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {}
    override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo) {}
    private val scanTypes = EnumSet.noneOf(ScanManager.ScanType::class.java)
    override fun onScanning(scanType: ScanManager.ScanType, on: Boolean) {
        if (on) scanTypes.add(scanType) else scanTypes.remove(scanType)
        val text = Stringer(",")
        for (type in scanTypes) {
            text.append(StringUtil.proper(type.toString()))
        }
        SleepAndWake.runInMain{ (findViewById<View>(R.id.scannerInfo) as TextView).text = text.toString() }
    }

    override fun onTouch() {
        for (view in views) if (view != null && view.visibility == View.VISIBLE) view.visibility = View.INVISIBLE
    }

    private var splashCanceled = false

    private fun showSplash() {
        if (!Globals.runMode.isRun || isFinishing) return
        startSplash = false
        splashCanceled = false
        try {
            val splash = SplashDialog(this, false)
            splash.setOnCancelListener { _ ->
                splashCanceled = true
                if (Globals.runMode.isRun && start) queryHistory()
            }
            splash.show()
            val sleep: Long = 4000
            SleepAndWake.runInMain({
                if (!splashCanceled && Globals.runMode.isRun) try {
                    splash.dismiss()
                    if (start) queryHistory()
                } catch (e: IllegalArgumentException) {
                    LLog.e(".removeSplashScreen: Can not dismiss dialog", e)
                }
            }, sleep, "MeterzMain.showSplash")
        } catch (e: Exception) {
            LLog.e("Could not start splash dialog", e)
        }
    }

    private var sensezLibInited = false

    override fun onSensezLibEvent(event: SensezLib.SensezLibEvent) {
        LLog.i("Calling onSensezLibEvent with: $event")
        when (event) {
            SensezLib.SensezLibEvent.SENSEZ_REINIT, SensezLib.SensezLibEvent.SENSEZ_INIT -> {
                if (!sensezLibInited) {
                    sensezLibInited = true
                    views[0] = findViewById(R.id.view_north)
                    views[1] = findViewById(R.id.view_northeast)
                    views[2] = findViewById(R.id.view_east)
                    views[3] = findViewById(R.id.view_southeast)
                    views[4] = findViewById(R.id.view_south)
                    views[5] = findViewById(R.id.view_southwest)
                    views[6] = findViewById(R.id.view_west)
                    views[7] = findViewById(R.id.view_northwest)
                    views[8] = findViewById(R.id.view_center)
                    (application as MeterzApp).addListener(object: MeterzApp.Listener {
                        override fun onInfo(info: String) {
                            val str = info.replace("null", "-")
                            SleepAndWake.runInMain({
                                Alerts.showOkDialog(this@MeterzMain, R.string.info, str, 120000) { clickResult: Alerts.ClickResult ->
                                    if (clickResult != Alerts.ClickResult.YES) return@showOkDialog
                                    LogMail(
                                        getString(
                                            R.string.meterz_report,
                                            com.plusot.shareddata.preferences.PreferenceKey.getMapUrl(),
                                            TimeUtil.formatTime(),
                                            str
                                        ),
                                        R.string.mail_sending,
                                        R.string.no_mail_to_send,
                                        null
                                    )
                                }
                            }, CLASS_TAG)
                        }
                    })
//
                    menuClickListener = View.OnClickListener { _ ->
                        clickZone?.visibility = View.VISIBLE
                        fadedInClickZone()
                        showMenu()
                    }
                    findViewById<View>(R.id.big_m).setOnClickListener(menuClickListener)
                    findViewById<View>(R.id.main_layout).setOnClickListener(menuClickListener)
                    findViewById<View>(R.id.clickZone).also { clickZone = it }.setOnClickListener(menuClickListener)
                    fadedInClickZone()
                    fadeIn(findViewById(R.id.big_m))
                    //if (start || startSplash) SensezLib.init(this, false);
                    if (startSplash) showSplash() else if (start) queryHistory()
                }
            }
            SensezLib.SensezLibEvent.SENSEZ_CLOSE -> {
            }
        }
    }

    companion object {
        private val CLASS_TAG = MeterzMain::class.java.simpleName
        private const val DEFAULT_RESTORE_INTERVAL: Long = 2
        private const val HISTORY_RESTORE_INTERVAL: Long = 20
        private var start = true
        private var startSplash = true
        private var instanceRef: WeakReference<MeterzMain>? = null
        @JvmStatic
        fun startWait() {
            if (instanceRef != null) {
                val instance = instanceRef?.get()
                if (instance != null) SleepAndWake.runInMain {
                    val view = instance.findViewById<View>(R.id.wait)
                    if (view != null) view.visibility = View.VISIBLE
                }
            }
        }

        @JvmStatic
        fun stopWait() {
            if (instanceRef != null) {
                val instance = instanceRef?.get()
                if (instance != null) SleepAndWake.runInMain {
                    val view = instance.findViewById<View>(R.id.wait)
                    if (view != null) view.visibility = View.GONE
                }
            }
        }
    }
}