package com.plusot.meterz.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.plusot.meterz.MeterzApp
import com.plusot.meterz.R
import com.plusot.meterz.notifications.Notifications
import com.plusot.meterz.preferences.PreferenceKey
import com.plusot.sensez.device.Device
import com.plusot.sensez.device.Man
import com.plusot.sensez.device.ScanManager
import com.plusot.sensez.device.ScannedDevice
import com.plusot.util.Globals
import com.plusot.util.data.Data
import com.plusot.util.data.DataType
import com.plusot.util.kotlin.MINUTE
import com.plusot.util.kotlin.getString
import com.plusot.util.logging.LLog
import java.lang.ref.WeakReference

class BackgroundService : Service(), Man.Listener {
    private var mayRun = true
    private var prevValue = mutableMapOf<DataType, Double>()
    private var prevLongValue = mutableMapOf<DataType, Long>()
    private var prevNotification = mutableMapOf<DataType, Long>()
    private var prevLog = 0L

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        instanceRef = WeakReference(this)
        initialized = true;
        Man.addListener(this)
        val thread = Thread {
            Thread.currentThread().name = "BackgroundThread_${Thread.currentThread().id}"
            LLog.i("Thread: ${Thread.currentThread().name} started" )
            while (Globals.runMode.isRun && mayRun) {
                try {
                    Thread.sleep(60000)
                } catch (e: InterruptedException) {
                    LLog.i("Sleep interrupted")
                }
            }
            LLog.i("Thread: ${Thread.currentThread().name} stopped" )
        }
        thread.start()
        val notification = Notifications.createNotification(
                this,
                R.string.app_name.getString(),
                R.string.appActive.getString(R.string.app_name.getString())
        )
        startForeground(startId, notification)
        LLog.i("Starting ${super.onStartCommand(intent, flags, startId)}")

        return START_STICKY
    }


    override fun onDestroy() {
        LLog.i("Destroy called")
        mayRun = false
        super.onDestroy()
    }

    override fun onDeviceState(device: ScannedDevice?, state: Device.StateInfo?) {
//        LLog.i("Device state for $device = $state")
    }

    override fun onScanning(scanType: ScanManager.ScanType?, on: Boolean) {
//        LLog.i("Scanning $scanType = $on")
    }

    override fun onDeviceScanned(device: ScannedDevice?, isNew: Boolean) {
//        LLog.i("Scanned $device as $isNew")
//        if (device is StepDevice) device.isSelected = true
    }

    override fun onDeviceData(device: ScannedDevice?, data: Data?, isNew: Boolean) {
        if (data == null) return
        if (device == null) return
        val now = System.currentTimeMillis()
        if (PreferenceKey.SEND_NOTIFICATIONS.isTrue) data.dataTypes.forEach { type ->
            when (type) {
                DataType.HEART_RATE, DataType.STEPS, DataType.DISTANCE, DataType.SPEED -> {
                    val value = data.getDouble(device.address, type)
                    if (value > prevValue[type] ?: 0.0 + type.minInterval()) {
                        val msg = data.toValueString(device.address, type)
                        if (now - (prevNotification[type] ?: 0L) > MINUTE) {
                            LLog.i("$type $msg")
                            Notifications.sendNotification(type.toProperString(), msg)
                            prevNotification[type] = now
                        }
                        prevValue[type] = value
                    }
                }
                DataType.TIME_ACTIVE, DataType.TIME_ACTIVITY -> {
                    val value = data.getLong(device.address, type)
                    if (value > prevLongValue[type] ?: 0 + type.minInterval().toLong()) {
                        val msg = data.toValueString(device.address, type)
                        if (now - (prevNotification[type] ?: 0L) > 5 * MINUTE) {
                            LLog.i("$type $msg")
                            Notifications.sendNotification(type.toProperString(), msg)
                            prevNotification[type] = now
                        }
                        prevLongValue[type] = value
                    }
                }
                DataType.SATELLITES_USED,
//                DataType.SATELLITES_VISIBLE,
                DataType.SATELLITES_FIX,
                DataType.SATELLITES_EVENT,
                DataType.SATELLITES_STATUS -> if (now - prevLog > 5 * MINUTE) {
                    LLog.i("$type ${data.toValueString(device.address, type)}")
                    prevLog = now
                }
                else ->{}
            }
        }
    }

    companion object {
        @JvmField var initialized = false
        private var instanceRef : WeakReference<BackgroundService>? = null
    }
}
