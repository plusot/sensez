package com.plusot.meterz.notifications

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.text.Html
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.plusot.meterz.MeterzMain
import com.plusot.meterz.R
import com.plusot.meterz.dialogs.showOkDialog
import com.plusot.util.Globals
import com.plusot.util.kotlin.getString
import com.plusot.util.logging.LLog
import com.plusot.util.util.SleepAndWake


object Notifications {
    const val channelId = "meterz_channel"
    private var channelCreated = false
    private var notificationId: Int = (System.currentTimeMillis() % 100000).toInt()

    const val NOTIFICATION_TITLE = "NOTIFICATION_TITLE"
    const val NOTIFICATION_MSG = "NOTIFICATION_MSG"
    const val NOTIFICATION_ID = "NOTIFICATION_ID"

    var prevTitle : String? = null
    var prevMsg : String? = null

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel(context: Context) {
        if (channelCreated) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val name = R.string.channel_name.getString()
            val description = R.string.channel_description.getString()
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelId, name, importance)
            mChannel.description = description
            mChannel.enableLights(true)
            mChannel.lightColor = Color.RED
            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mNotificationManager.createNotificationChannel(mChannel)
            channelCreated = true
        } else {
            LLog.i("Could not create notification channel as SDK version is below Oreo")
        }
    }

    fun createNotification(context: Context?, title: String, msg: String): Notification? {
        if (context == null) return null
        val resultIntent = Intent(context, MeterzMain::class.java)
        resultIntent.putExtra(NOTIFICATION_TITLE, title)
        resultIntent.putExtra(NOTIFICATION_MSG, msg)

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(MeterzMain::class.java)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT) //PendingIntent.FLAG_ONE_SHOT)

        val html = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(msg, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(msg)
        }

        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(context)
            NotificationCompat.Builder(context, channelId)
        } else {
            @Suppress("DEPRECATION")
            NotificationCompat.Builder(context)
        }
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(html)
            .setAutoCancel(true)
        builder.setContentIntent(resultPendingIntent)

        LLog.i("title $title, text $html")
        return builder.build()
    }

    fun sendNotification(title: String, msg: String) {
        val context : Context = Globals.getAppContext() ?: return
        if (!Globals.runMode.isRun) return
        val id = ++notificationId
        SleepAndWake.runInMain { (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?)?.notify(id, createNotification(context, title, msg)) }
    }

    @JvmStatic fun showNotificationAlert(context: Context, intent: Intent) {
        val title: String? = intent.getStringExtra(NOTIFICATION_TITLE)
        val msg: String? = intent.getStringExtra(NOTIFICATION_MSG)
        val id: Int = intent.getIntExtra(NOTIFICATION_ID, -1)
        if (id > 0 && title != null && msg != null && msg != prevMsg && title != prevTitle) {
            showOkDialog(context, title, msg) {}
            prevMsg = msg
            prevTitle = title
        }
    }

}