package com.plusot.meterz

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.speech.tts.TextToSpeech
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import com.plusot.meterz.preferences.PreferenceKey
import com.plusot.meterz.services.BackgroundService
import com.plusot.meterz.store.GpxWriter
import com.plusot.sensez.SensezLib
import com.plusot.sensez.device.Device
import com.plusot.sensez.device.Man
import com.plusot.sensez.device.ScanManager
import com.plusot.sensez.device.ScannedDevice
import com.plusot.util.Globals
import com.plusot.util.data.Data
import com.plusot.util.data.DataType
import com.plusot.util.logging.LLog
import com.plusot.util.util.SleepAndWake
import com.plusot.util.util.ToastHelper
import java.io.File
import java.lang.ref.WeakReference
import java.util.*


class MeterzApp : Application(), TextToSpeech.OnInitListener, Man.Listener, SensezLib.EventListener {
    private var tts: TextToSpeech? = null
    private val lastSpoken = HashMap<DataType, Data>()
    protected val listeners = HashSet<WeakReference<Listener>>()
    private var sensezLibInited = false

    override fun onSensezLibEvent(event: SensezLib.SensezLibEvent) {
        when (event) {
            SensezLib.SensezLibEvent.SENSEZ_REINIT, SensezLib.SensezLibEvent.SENSEZ_INIT -> {
                if (!sensezLibInited) {
                    sensezLibInited = true
                    val tireSize = PreferenceKey.WHEEL_SIZE.int / 100000.0
                    if (tireSize > 500) com.plusot.sensez.preferences.PreferenceKey.WHEEL_CIRCUMFERENCE.set(tireSize)

                    Man.addListener(this)
                    GpxWriter.getInstance()

                    val appContext = Globals.getAppContext()
                    if (appContext != null) tts = TextToSpeech(appContext, this)
                }
            }
            SensezLib.SensezLibEvent.SENSEZ_CLOSE -> {
                val stopped = stopService(Intent(this, BackgroundService::class.java))
                if (!stopped) LLog.d("Background service not stopped")
                val context = Globals.getAppContext()
                var killString = "Killing app"
                if (context != null) {
                    val appName = context.getString(R.string.app_name)
                    killString = context.getString(R.string.killing, appName)
                }
                ToastHelper.showToastLong(killString)

                GpxWriter.stopInstance(MeterzApp::class.java, true)
                Man.removeListener(this)
            }
        }
    }

    interface Listener {
        fun onInfo(info: String)
    }

    override fun onCreate() {
        super.onCreate()
        Globals.init(this, MeterzApp::class.java.simpleName)
        SensezLib.addEventListener(this)
        appReference = WeakReference(this)

        SleepAndWake.runInMain(2000) {
            if (!mainCreated) {
                LLog.i("Main activity not created. Not calling startService")
                Runtime.getRuntime().exit(0)
            } else if (BackgroundService.initialized)
                LLog.i("Background service already initialized. Not calling startService again!")
            else try {
                LLog.d("Starting the background service")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(Intent(this, BackgroundService::class.java))
                } else {
                    startService(Intent(this, BackgroundService::class.java))
                }
            } catch (e: IllegalStateException) {
                LLog.e("Illegal state starting background service: " + e.message)
            }
        }
    }

    override fun openOrCreateDatabase(name: String?, mode: Int, factory: SQLiteDatabase.CursorFactory?): SQLiteDatabase {
        return openOrCreateDatabase(name, mode, factory, null)
    }

    override fun openOrCreateDatabase(name: String?, mode: Int, factory: SQLiteDatabase.CursorFactory?, errorHandler: DatabaseErrorHandler?): SQLiteDatabase {
        val externalFilesDir = getExternalFilesDir(null)
        if (externalFilesDir == null) {
            LLog.i(".openOrCreateDatabase null for external dir")
            return super.openOrCreateDatabase(name, mode, factory, errorHandler)
        }
        val dbFile = File(externalFilesDir, name ?: "app.db")
//        LLog.i(".openOrCreateDatabase ${dbFile.absolutePath}")
        return SQLiteDatabase.openDatabase(dbFile.absolutePath, factory, SQLiteDatabase.CREATE_IF_NECESSARY)
    }

    fun addListener(listener: Listener) {
        listeners.add(WeakReference(listener))
    }

    override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
        val address = device.address
        for (type in data.dataTypes)
            if (data.hasDataType(type)) {
                when (type) {

                    DataType.INFO ->
                        //                    LLog.i("Getting here with " + data);
                        if (listeners.size > 0)
                            for (listenerRef in listeners) {
                                val listener = listenerRef.get()
                                listener?.onInfo(data.getStringWithoutLabel(address, type))
                            }
                    else -> if (isNew && device.isDataTypeSpeech(type)) {
                        val prevData = lastSpoken[type]
                        if (prevData == null || data.time - prevData.time > SPEECH_INTERVAL && prevData.valueChanged(data, type)) {
                            say(data.getVoiceString(address, type))
                            lastSpoken[type] = data.clone()
                        }
                    }
                }
            }
    }

    override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {}

    override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo) {}

    override fun onScanning(scanType: ScanManager.ScanType, on: Boolean) {}

    fun say(text: String?) {
        val appContext = Globals.getAppContext()
        if (appContext != null && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.READ_PHONE_STATE)) {
            val telManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
            if (telManager != null && telManager.callState != TelephonyManager.CALL_STATE_IDLE) return
        }
        if (!Globals.runMode.isRun) {
            LLog.i("not in run mode")
            return
        }

        if (tts == null) {
            LLog.i("tts == null")
            if (appContext != null) tts = TextToSpeech(appContext, this)
        } else tts?.let { tts ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text, TextToSpeech.QUEUE_ADD, null, null)
            } else {
                @Suppress("DEPRECATION")
                tts.speak(text, TextToSpeech.QUEUE_ADD, null)
            }
        }
        //LLog.i("say: " + text);

    }

    override fun onInit(status: Int) {
        var speechLocale: Locale? = null

        if (status == TextToSpeech.SUCCESS) tts?.let { tts->
            var result = tts.isLanguageAvailable(Locale.getDefault())
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val voice = tts.voice
                    if (voice != null) speechLocale = voice.locale
                } else {
                    @Suppress("DEPRECATION")
                    speechLocale = tts.language
                }
            } else {
                result = tts.setLanguage(Locale.getDefault())
                speechLocale = Locale.getDefault()
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    LLog.e("onSensezLibInit: Requested language is not available.")
                    //Toast.makeText(Globals.appContext, "Requested language not available", Toast.LENGTH_SHORT).show();
                }
            }
            speechLocale?.let {
                LLog.i("Current speech locale: " + it.displayCountry + ", " + speechLocale)
            }
        } else {
            // Initialization failed.
            LLog.e("onSensezLibInit: Could not initialize TextToSpeech.")
            ToastHelper.showToastLong(R.string.no_init_speech)
        }
    }

    companion object {
        private val SPEECH_INTERVAL: Long = 60000
        private var appReference: WeakReference<MeterzApp>? = null
        var mainCreated = false

        fun sayIt(text: String) {
            appReference?.get()?.say(text)
        }
    }

}
