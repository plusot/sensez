package com.plusot.meterz.preferences

import android.os.Bundle
import androidx.preference.ListPreference
import com.plusot.meterz.R
import com.plusot.util.logging.LLog
import com.plusot.util.util.SleepAndWake

class BicycleSettingsFragment : PreferencesFragment(R.xml.bicycle_settings) {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreatePreferences(savedInstanceState, rootKey)
        findPreference<ListPreference>("tiresize_preference")?.setOnPreferenceChangeListener{ listPref, newValue ->
            if (newValue !is String || listPref !is ListPreference) return@setOnPreferenceChangeListener true
            LLog.i("onPreferenceChange: $newValue")
            //enableWheelInMM(((String) newValue).equals("0"));
            PreferenceKey.WHEEL_SIZE.set(newValue)
            SleepAndWake.runInMain({
                listPref.value = PreferenceKey.WHEEL_SIZE.string
                updateSummary(listPref)
            }, 50, "BicyclePreferenceFragment.onCreate")
            true
        }
    }
}