package com.plusot.meterz.preferences

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.plusot.meterz.R
import com.plusot.meterz.widget.SeekBarPreference
import com.plusot.util.logging.LLog
import com.skydoves.colorpickerpreference.ColorPickerPreference
import com.skydoves.colorpickerview.ColorEnvelope
import com.skydoves.colorpickerview.flag.FlagView
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener

class CustomFlag(context: Context, layout: Int) : FlagView(context, layout) {

    private val textView: TextView = findViewById(R.id.flag_color_code)
    private val view: View = findViewById(R.id.flag_color_layout)

    @SuppressLint("SetTextI18n")
    override fun onRefresh(colorEnvelope: ColorEnvelope) {
        textView.text = "#${colorEnvelope.hexCode}"
        view.setBackgroundColor(colorEnvelope.color)
    }
}

class MapSettingsFragment : PreferencesFragment(R.xml.map_settings) {

    init {
        LLog.i(
            PreferenceKey.HEREWEGO_APPCODE.getString("****") +
                    "\r\n${PreferenceKey.HEREWEGO_APPID.getString("****")}" +
                    "\r\n${PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****")}" +
                    "\r\n${PreferenceKey.MAPQUEST_ACCESS_TOKEN.getString("****")}" +
                    "\r\n${PreferenceKey.THUNDERFOREST_MAPIP.getString("****")}"
        )
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreatePreferences(savedInstanceState, rootKey)

        findPreference<ColorPickerPreference>(PreferenceKey.TRACE_COLOR.label)?.apply {
            getColorPickerView().flagView = CustomFlag(requireContext(), R.layout.layout_flag)
            preferenceColorListener = ColorEnvelopeListener { envelope, _ ->
                LLog.d("Trace Color: #${envelope.hexCode} is selected")
            }
        }
        findPreference<ColorPickerPreference>(PreferenceKey.GPX_COLOR.label)?.apply {
            getColorPickerView().flagView = CustomFlag(requireContext(), R.layout.layout_flag)
            preferenceColorListener = ColorEnvelopeListener { envelope, _ ->
                LLog.d("GPX Color: #${envelope.hexCode} is selected")
            }
        }
        findPreference<SeekBarPreference>(PreferenceKey.TRACE_WIDTH.label)?.apply {
            this.setValue(PreferenceKey.TRACE_WIDTH.int)
        }
        findPreference<SeekBarPreference>(PreferenceKey.GPX_WIDTH.label)?.apply {
            this.setValue(com.plusot.meterz.preferences.PreferenceKey.GPX_WIDTH.int)
        }
    }
}