package com.plusot.meterz.preferences

import android.os.Bundle
import androidx.preference.Preference
import com.plusot.meterz.R
import com.plusot.util.util.ToastHelper

class TechnologiesSettingsFragment  : PreferencesFragment(R.xml.technologies_settings) {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreatePreferences(savedInstanceState, rootKey)
        findPreference<Preference>("use_ant")?.setOnPreferenceChangeListener { _,_ ->
            ToastHelper.showToastLong(R.string.effectiveAfterRestart)
            true
        }
        findPreference<Preference>("use_ble")?.setOnPreferenceChangeListener { _,_ ->
            ToastHelper.showToastLong(R.string.effectiveAfterRestart)
            true
        }
        findPreference<Preference>("use_bt")?.setOnPreferenceChangeListener { _,_ ->
            ToastHelper.showToastLong(R.string.effectiveAfterRestart)
            true
        }
        findPreference<Preference>("use_internal")?.setOnPreferenceChangeListener { _,_ ->
            ToastHelper.showToastLong(R.string.effectiveAfterRestart)
            true
        }
    }
}