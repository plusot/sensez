package com.plusot.meterz.preferences

import android.os.Build
import android.os.Bundle
import androidx.preference.Preference
import com.plusot.meterz.R
import com.plusot.meterz.dialogs.SplashDialog
import com.plusot.sensez.device.Man
import com.plusot.sensez.internal.ClockDevice
import com.plusot.sensez.internal.GpsDevice
import com.plusot.shareddata.preferences.PreferenceKey
import com.plusot.util.dialog.Alerts
import com.plusot.util.share.LogMail
import com.plusot.util.util.TimeUtil
import com.plusot.util.util.ToastHelper

class ApplicationSettingsFragment: PreferencesFragment(R.xml.application_settings) {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreatePreferences(savedInstanceState, rootKey)
        findPreference<Preference>("report_preference")?.setOnPreferenceClickListener {
            LogMail(
                getString(
                    R.string.meterz_report,
                    PreferenceKey.getMapUrl(),
                    TimeUtil.formatTime(),
                    ""
                ),
                R.string.mail_sending,
                R.string.no_mail_to_send,
                null
            )
            true
        }

        findPreference<Preference>("pref_reset")?.setOnPreferenceClickListener {
            alertReset()
            true
        }

        findPreference<Preference>("use_strict")?.setOnPreferenceChangeListener { _, _ ->
            ToastHelper.showToastLong(R.string.effectiveAfterRestart)
            true
        }
        findPreference<Preference>("use_leakage")?.apply {
            setOnPreferenceChangeListener { _, _ ->
                ToastHelper.showToastLong(R.string.effectiveAfterRestart)
                true
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                isEnabled = false
            }
        }


        findPreference<Preference>("about_preference")?.setOnPreferenceClickListener {
            val splash: SplashDialog
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                splash = SplashDialog(context, true)
                splash.show()
            } else {
                splash = SplashDialog(activity, true)
                splash.show()
            }
            true
        }

        findPreference<Preference>("altitude_offset")?.setOnPreferenceChangeListener { _, newValue ->
            try {
                com.plusot.sensez.preferences.PreferenceKey.setAltitudeOffset((newValue as String).toDouble())
                return@setOnPreferenceChangeListener true
            } catch (e: NumberFormatException) {
                ToastHelper.showToastLong(getString(R.string.incorrectNumber, newValue))
            }
            false
        }
    }
    private fun alertReset() {
        Alerts.showYesNoDialog(
            this.context, R.string.resetTitle, R.string.resetMsg
        ) { clickResult ->
            if (clickResult == Alerts.ClickResult.YES) {
                Man.resetTotals()
                GpsDevice.resetTotalDistance()
                ClockDevice.resetTotal()
            }
        }
    }
}