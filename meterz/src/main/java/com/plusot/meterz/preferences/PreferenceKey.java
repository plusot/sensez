package com.plusot.meterz.preferences;

import com.plusot.meterz.widget.TileSourceType;
import com.plusot.util.preferences.PreferenceDefaults;
import com.plusot.util.preferences.PreferenceKeyElements;
import com.plusot.util.util.Format;


public enum PreferenceKey {
    SEND_NOTIFICATIONS("sendNotifications", false, Boolean.class),
    SCALE_TO_DPI("scaleMapToDpi", true, Boolean.class),
    ZOOM("zoom_double", 14.0, Double.class),
    TILE_SOURCE("tilesource_v2_preference", TileSourceType.CYCLEMAP.ordinal(), Integer.class),
    VIEW_SIZE("viewsize_preference", 32, Integer.class),
    WHEEL_SIZE("tiresize_preference", 2100026, null, Integer.class, PreferenceDefaults.FLAG_ISARRAY),
    MAP_ALWAYS("map_always_preference", true, Boolean.class),
    GPX_PATH("gpx_path", null, String.class),
    GPX_FOLLOW("gpx_follow", false, Boolean.class),
    GPX_REVERSE("gpx_reverse", false, Boolean.class),
    CONTOUR_LINES("contour_preference", true, Boolean.class),
    CHECK_LEAKAGE("use_leakage", false, Boolean.class),
    STRICT_MODE("use_strict", false, Boolean.class),
    POI_INDEX("poi_index", -1, Integer.class),
    TRACE_COLOR("trace_color_preference", 0xdf09c5ff, Integer.class),
    GPX_COLOR("gpx_color_preference", 0xcc0afa16, Integer.class),
    TRACE_WIDTH("trace_width_preference", 10, Integer.class),
    GPX_WIDTH("gpx_width_preference", 8, Integer.class),
    SPEECH_INFO_CHECKED("speech_info", false, Boolean.class),
    VIEW_TRANSPARENCY("view_transparency", 20, Integer.class),
    HEREWEGO_APPID("herewego_appid", "****PHlcAdmn9WXx****", "RsglPHlcAdmn9WXxlVVx", String.class),
    HEREWEGO_APPCODE("herewego_appcode", "****FxPzQMWi64PuET****", "X6ciFxPzQMWi64PuET2OdA", String.class),
    MAPBOX_ACCESS_TOKEN("mapbox_accesstoken", "py.****IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO980****",
            "pk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO9801rmw", String.class),
    MAPQUEST_ACCESS_TOKEN("mapquest_accesstoken", "py.****IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO980****",
                                "pk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO9801rmw", String.class),
    THUNDERFOREST_MAPIP("thunderforest_mapid", "****7c783ddc496e8d225f65eeee****", "080a7c783ddc496e8d225f65eeee8c22", String.class),
    SCREEN_ON("screen_on_preference", true, Boolean.class),
    ;

    private final PreferenceKeyElements elements;

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass) {
        this(label, defaultValue, null, valueClass, 0);
    }

    PreferenceKey(final String label, final Object defaultValue, Object fallbackValue, final Class<?> valueClass) {
        this(label, defaultValue, fallbackValue, valueClass, 0);
    }

    PreferenceKey(final String label, final Object defaultValue, Object fallbackValue, final Class<?> valueClass, final long flags) {
        elements = new PreferenceKeyElements (label, defaultValue, fallbackValue, valueClass, flags);
    }

    public String getLabel() { return elements.getLabel(); }
    public int getInt() { return elements.getInt(); }
    public double getDouble() { return elements.getDouble(); }
    public String getString() { return elements.getString(); }
    public String getString(String unsetCheck) { return elements.getStringCheckUnset(unsetCheck); }
    public boolean isTrue() {
        return elements.getBoolean();
    }

    public boolean set(int value) { return elements.set(value); }
    public void set(double value) { elements.set(value); }
    public void set(String value) { elements.set(value); }
    public void set(boolean value) { elements.set(value); }

    public static PreferenceKey fromString(final String label) {
        for (PreferenceKey key : PreferenceKey.values()) {
            if (key.elements.getLabel().equals(label)) return key;
        }
        return null;
    }

}
