package com.plusot.meterz.preferences

import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.plusot.meterz.R

enum class SettingsType {
    ApplicationSettings,
    BicycleSettings,
    MapSettings,
    ServerSettings,
    TechnologiesSettings,

//    CloudSettings,
//    TestSettings,
//    AdminSettings,
    ;

    companion object {
        fun fromOrdinal(value: Int) :SettingsType = if (value in values().indices) values()[value] else ApplicationSettings
    }
}


class SettingsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_settings)

        val settingsType = SettingsType.fromOrdinal(intent?.getIntExtra(SETTINGS_CHOICE, 0) ?: 0)
        findViewById<View>(R.id.back_button)?.setOnClickListener { finish() }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_layout,
                when (settingsType) {
                    SettingsType.ApplicationSettings -> {
                        setHeaderTitle(R.string.header_title_application_preferences)
                        ApplicationSettingsFragment()
                    }
                    SettingsType.BicycleSettings -> {
                        setHeaderTitle(R.string.header_title_bicycle_preferences)
                        BicycleSettingsFragment()
                    }
                    SettingsType.MapSettings -> {
                        setHeaderTitle(R.string.header_title_map_preferences)
                        MapSettingsFragment()
                    }
                    SettingsType.ServerSettings -> {
                        setHeaderTitle(R.string.header_title_server_preferences)
                        ServerSettingsFragment()
                    }
                    SettingsType.TechnologiesSettings -> {
                        setHeaderTitle(R.string.header_title_technology_preferences)
                        TechnologiesSettingsFragment()
                    }
                })
            .commit()
    }

    private fun setHeaderTitle(@StringRes title: Int) {
        findViewById<TextView>(R.id.title_view)?.also {
            it.visibility = View.VISIBLE
            it.text = getString(title)
        }
    }

    companion object {
        const val SETTINGS_CHOICE = "SettingsChoice"
    }
}
