package com.plusot.meterz.preferences

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.text.InputType
import androidx.annotation.CallSuper
import androidx.annotation.XmlRes
import androidx.preference.*
import com.plusot.util.logging.LLog


import com.plusot.meterz.util.StringUtil.toString
import com.plusot.util.Globals

abstract class PreferencesFragment(@param:XmlRes private val preferenceId: Int) :
    PreferenceFragmentCompat(), OnSharedPreferenceChangeListener {

    @CallSuper override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(preferenceId, rootKey)
        initSummaries()
        preferenceScreen.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        addPreferencesFromResource(preferenceId)
//        //PreferenceManager.setDefaultValues(Globals.appContext, preferenceXML, false);
//        initSummaries()
//        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
//    }

    override fun onDestroy() {
        super.onDestroy()
        preferenceScreen.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        updateSummary(findPreference(key))
    }

    private fun initSummaries() {
        for (i in 0 until preferenceScreen.preferenceCount) {
            initSummary(preferenceScreen.getPreference(i))
        }
    }

    private fun initSummary(pref: Preference) {
        if (pref is PreferenceCategory) {
            val pCat = pref
            for (i in 0 until pCat.preferenceCount) {
                initSummary(pCat.getPreference(i))
            }
        } else {
            updateSummary(pref)
        }
    }

    protected fun updateSummary(pref: Preference?) {
        //LLog.d(Common"updatePrefSummary");
        val edit = when (pref) {
            is ListPreference -> "" + pref.entry
            is EditTextPreference -> {
                //if (pref. and EditorInfo.TYPE_TEXT_VARIATION_PASSWORD == 0)
                pref.text
            }
            is MultiSelectListPreference -> "\n   *${toString(pref.values, "\n   *")}"
            else -> return
        }

        //if (edit == null) return
        var sum: String? = pref.summary.toString()
        if (sum == null) return
        if (summaries[pref.key] == null) {
            summaries[pref.key] = sum
        }
        sum = summaries[pref.key]
        if (sum == null) return
        if (edit != null && sum.matches(".*\\[.*].*".toRegex())) sum =
            sum.replace("\\[.*]".toRegex(), edit) else sum += ": $edit"
        pref.summary = sum
    }

    protected fun setIntent(key: String, activityClass: Class<*>?) {
        val myPref = findPreference(key) as Preference?
        if (myPref == null) {
            LLog.e("setIntent: Could not find key: $key")
        } else myPref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val appContext = Globals.getAppContext() ?: return@OnPreferenceClickListener true
            startActivity(Intent(appContext, activityClass))
            true
        }
    }

    fun setInputType(vararg key: String, inputType: Int = InputType.TYPE_CLASS_NUMBER) {
        key.forEach { keyId ->
            findPreference<EditTextPreference>(keyId)?.let {
                it.setOnBindEditTextListener { editText ->
                    editText.inputType = inputType
                }
            }
        }
    }

    companion object {
        private val summaries = mutableMapOf<String, String?>()
    }
}