package com.plusot.meterz.preferences

import android.os.Bundle
import androidx.preference.EditTextPreference
import com.plusot.meterz.R
import com.plusot.util.logging.LLog

class ServerSettingsFragment : PreferencesFragment(R.xml.server_settings) {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreatePreferences(savedInstanceState, rootKey)
        findPreference<EditTextPreference>("url_preference")?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue !is String) return@setOnPreferenceChangeListener true
            LLog.i(newValue)
            true
        }
    }
}