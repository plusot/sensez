package com.plusot.meterz.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.plusot.meterz.R;
import com.plusot.util.util.Tuple;

import java.util.List;

public class MenuAdapter extends ArrayAdapter<Tuple<Integer, String>> {
    private final int resource;
    private final LayoutInflater inflater;

    private MenuAdapter(final Context context, int resource, List<Tuple<Integer, String>> items) {
        super(context, resource, items);
        this.resource = resource;
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public MenuAdapter(final Context context, List<Tuple<Integer, String>> items) {
        this(context, R.layout.adapter_menu, items);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        Holder holder;
        Tuple<Integer,String> item = getItem(position);
        if (view == null) {
            view = inflater.inflate(resource, parent, false);
            holder = new Holder(view);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        if (item != null) {
            holder.menuIcon.setImageDrawable(getContext().getResources().getDrawable(item.t1()));
            holder.menuText.setText(item.t2());
        }
        return view;
    }


    private class Holder {
        final ImageView menuIcon;
        final TextView menuText;

        Holder(View view) {
            this.menuIcon = (ImageView) view.findViewById(R.id.menuIcon);
            this.menuText = (TextView) view.findViewById(R.id.menuItem);
        }
    }

}
