package com.plusot.meterz.dialogs;

import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.*;
import com.plusot.meterz.*;
import com.plusot.util.util.*;


public class SplashDialog extends Dialog {
    private final boolean about;

    public SplashDialog(Context context, boolean about) {
        super(context, R.style.SplashScreen);
        this.about = about;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_splash);
        setCancelable(true);
        TextView tv = (TextView) findViewById(R.id.version);
        tv.setText(UserInfo.appVersionShort() + " " + getContext().getString(R.string.build) + " " + UserInfo.appVersionCode() + " - " + TimeUtil.formatTime(BuildConfig.BUILD_TIME, "yyyyMMdd"));
		if (MeterzGlobals.hasExtraFeatures()) {
            ImageView imageView = (ImageView) findViewById(R.id.splash_image);
            if (imageView != null) imageView.setImageResource(R.drawable.meterz_plus_512);
//            View view = findViewById(R.id.nagText);
//            if (view != null) view.setVisibility(View.GONE);
//            view = findViewById(R.id.googlePlayButton);
//            if (view != null) view.setVisibility(View.GONE);
        }
//        else if (Math.random() < 0.25 || about){
//            View view = findViewById(R.id.nagText);
//            if (view != null) fadeIn(view, 250, 2000);
//            view = findViewById(R.id.googlePlayButton);
//            if (view != null) fadeIn(view, 750, 2000);
//            if (view != null) {
//                view.setVisibility(View.INVISIBLE);
//                view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        try {
//                            Intent intent = new Intent(Intent.ACTION_VIEW);
//                            intent.setData(Uri.parse("market://details?id=com.plusot.meterz.plus"));
//                            getContext().startActivity(intent);
//                        } catch (ActivityNotFoundException e) {
//                            ToastHelper.showToastLong(R.string.could_not_find_playstore);
//                        }
//
//                    }
//                });
//            }
//
//        }
        if (about) {
            View view = findViewById(R.id.usedSources);
            if (view != null) fadeIn(view, 1500, 2000);
        }

    }

    private void fadeIn(final View view, final long offset, final long duration) {
        fadeIn(view, offset, duration, false, 0.0f, 1.0f);
    }

    private void fadeIn(final View view, final long offset, final long duration, final boolean isAlpha, final float minAlpha, final float maxAlpha) {
        if (view == null || view.getTag() != null) return;
        view.setTag(true);
        Animation fadeIn = new AlphaAnimation(isAlpha ? minAlpha : 0.0f, isAlpha ? maxAlpha : 1.0f);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                if (isAlpha) {
                    view.setAlpha(maxAlpha);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeIn.setDuration(duration);
        fadeIn.setStartOffset(offset);
        view.startAnimation(fadeIn);

    }

}
