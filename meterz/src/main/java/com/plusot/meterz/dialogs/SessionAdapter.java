package com.plusot.meterz.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.plusot.meterz.R;
import com.plusot.shareddata.db.DbData;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;

import java.util.ArrayList;
import java.util.List;

public class SessionAdapter extends ArrayAdapter<DbData.SessionObj> {
    private final int resource;
    private final LayoutInflater inflater;
    //private List<DbData.SessionObj> sessions;

    public static String sessionIdToDate(final String sessionId) {
        if (sessionId.length() >= 6) return "20" + sessionId.substring(0, 2) + "-" + sessionId.substring(2, 4) + "-" + sessionId.substring(4, 6);
        return sessionId;
    }

    public SessionAdapter(final Context context, final int resource, List<DbData.SessionObj> sessions) {
        super(context, resource, sessions);
        this.resource = resource;
        //this.sessions = sessions;
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        Holder holder;
        DbData.SessionObj sessionObj = getItem(position);
        if (view == null) {
            view = inflater.inflate(resource, parent, false);
            holder = new Holder(view);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        if (sessionObj != null) {
            String date = sessionIdToDate(sessionObj.getSessionId());
            holder.sessionId.setText(date);
            Data data = sessionObj.getData();
            int index = 0;

            if (data != null) for (DataType type: data.getDataTypes()) for (String sensorId: data.getSensorIds(type)) {
                TableRow row;
                if (holder.rows.size() == index) {
                    holder.rows.add(row = (TableRow) inflater.inflate(R.layout.row_session, null));
                    holder.tableView.addView(row);
                } else
                    row = holder.rows.get(index);
                row.setVisibility(View.VISIBLE);
                ((TextView) row.findViewById(R.id.aggregateName)).setText(type.toString());
                ((TextView) row.findViewById(R.id.aggregateValue)).setText(data.toValueString(sensorId, type));
                index++;
            }
            if (holder.rows.size() > index ) for (int i = index; i < holder.rows.size(); i++) holder.rows.get(i).setVisibility(View.GONE);
        }
        return view;
    }


    private class Holder {
        final TextView sessionId;
        final TableLayout tableView;
        final List<TableRow> rows = new ArrayList<>();

        Holder(View view) {
            this.sessionId = view.findViewById(R.id.sessionId);
            this.tableView = view.findViewById(R.id.aggregates);
        }
    }

}
