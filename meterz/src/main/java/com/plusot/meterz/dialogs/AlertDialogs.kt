package com.plusot.meterz.dialogs

import android.app.AlertDialog
import android.content.Context
import com.plusot.util.dialog.Alerts
import com.plusot.util.util.Watchdog

/**
 * Package: com.bronstenkate.diabetes24.dialogs
 * Project: $diabetes24android
 *
 * Created by Peter Bruinink on 30-01-18.
 * Copyright © 2018 Brons Ten Kate. All rights reserved.
 */

fun showOkDialog(context: Context, titleId: Int, listener: (clickResult: Alerts.ClickResult) -> Unit) {
    if (!Watchdog.isRunning()) return

    AlertDialog.Builder(context)
            .setTitle(titleId)
            .setPositiveButton(android.R.string.ok) { _, _ -> listener(Alerts.ClickResult.YES) }
            .setOnCancelListener{ listener(Alerts.ClickResult.CANCEL) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
}

fun showOkDialog(context: Context, title: String, msg: String, listener: (clickResult: Alerts.ClickResult) -> Unit) {
    if (!Watchdog.isRunning()) return

    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton(android.R.string.ok) { _, _ -> listener(Alerts.ClickResult.YES) }
            .setOnCancelListener{ listener(Alerts.ClickResult.CANCEL) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
}

fun showOkDialog(context: Context, titleId: Int, msgId: Int, listener: (clickResult: Alerts.ClickResult) -> Unit) {
    if (!Watchdog.isRunning()) return

    AlertDialog.Builder(context)
            .setTitle(titleId)
            .setMessage(msgId)
            .setPositiveButton(android.R.string.ok) { _, _ -> listener(Alerts.ClickResult.YES) }
            .setOnCancelListener{ listener(Alerts.ClickResult.CANCEL) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
}