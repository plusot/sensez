package com.plusot.meterz.store;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.plusot.meterz.MeterzMain;
import com.plusot.util.logging.LLog;
import com.plusot.util.parsers.XmlParser;
import com.plusot.util.parsers.XmlParser.NodeEvent;
import com.plusot.util.parsers.XmlParser.XmlNode;

import org.osmdroid.util.GeoPoint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GpxReader {
    private static final boolean DEBUG = false;
    private final ReadGpxTask task;

    private final Listener listener;

    public GpxReader(Listener listener, String path) {
        this.listener = listener;
        task = new ReadGpxTask();
        MeterzMain.startWait();
        task.execute(path);
    }

    public void stop() {
        if (task != null) task.stop();
    }

    public interface Listener {
        void onPoints(int index, List<GeoPoint> list);
        void onDone();
    }

    private class ProgressItem {
        final List<GeoPoint> points = new ArrayList<>();
        final int index;

        private ProgressItem(int index) {
            this.index = index;
        }

        int size() {
            return points.size();
        }
    }

    //private int firstTime = 0;

    @SuppressLint("StaticFieldLeak")
    private class ReadGpxTask extends AsyncTask<String, ProgressItem, ProgressItem> {
        private ProgressItem progressItem;
        private int index = 0;
        private double distance = 0;
        private boolean mayRun = true;

        private void stop() {
            mayRun = false;
        }


        @Override
        protected ProgressItem doInBackground(String... params) {
            if (params == null || params.length < 1) return null;

            BufferedReader in = null;

            XmlParser parser = new XmlParser((XmlNode node, NodeEvent event) -> {
                if (mayRun) switch (event) {
                    case INIT:
                        break;
                    case COMPLETE:
                        String info = node.toString();
                        if (DEBUG) {
                            XmlNode parent = node.getParent();
                            String parentInfo = "";
                            if (parent != null) parentInfo = ":(" + parent.getName() + ")";
                            LLog.i("Node " + node.getName() + parentInfo + " = " + info.substring(0, Math.min(60, info.length())));
                        }
                        String nodeName = node.getName();
                        if (nodeName.equals("trk")) {
                            ReadGpxTask.this.publishProgress(progressItem);
                            progressItem = new ProgressItem(index++);
                        } else if (nodeName.equals("trkpt") || nodeName.equals("rtept")) {
                            Double lat = node.getDouble("lat");
                            Double lng = node.getDouble("lon");

                            if (lat != null && lng != null) {
                                boolean mayAdd = true;
                                GeoPoint geoPoint = new GeoPoint(lat, lng);
                                XmlParser.XmlNode child = node.getChild("ele");
                                if (child != null) {
                                    Double value = child.getDouble("_");
                                    if (value != null) geoPoint.setAltitude(value);
                                }
                                child = node.getChild("extensions");
                                if (child != null) {
                                    child = node.getChild("gpxdata:distance");
                                    if (child != null) {

                                        Double value = child.getDouble("_");
                                        if (value != null && value > distance) {
                                            distance = value;
                                        } else {
                                            mayAdd = false;
                                        }
                                    }
                                }
                                if (mayAdd) progressItem.points.add(geoPoint);
                            }
                        }
                        break;
                }
                return null;
            });

            try {
                LLog.i("Reading GPX file: " + params[0]);
                in = new BufferedReader(new FileReader(new File(params[0])), 8192 * 6);

                String line;
                progressItem = new ProgressItem(index++);

                while (mayRun && (line = in.readLine()) != null) parser.parse(line);
            } catch (IOException e) {
                LLog.e("ReadGpxTask.doInBackground: Failure in reading line", e);
            } finally {
                if (in != null) try {
                    in.close();
                } catch (IOException e) {
                    LLog.e("ReadGpxTask.doInBackground: Could not close stream.");
                }
            }


            return progressItem;
        }


        @Override
        protected void onProgressUpdate(ProgressItem... progress) {
//            if (progress.length > 0) LLog.i("ReadGpxTask.onProgressUpdate " + progress[0].count);
            if (progress[0].size() > 1) listener.onPoints(progress[0].index, progress[0].points);

        }

        @Override
        protected void onPostExecute(ProgressItem progressItem) {
            MeterzMain.stopWait();
            if (progressItem != null && progressItem.size() > 0) {
                LLog.i("ReadGpxTask done. " + progressItem.size() + " Points imported");
                if (progressItem.size() > 1) listener.onPoints(progressItem.index, progressItem.points);

            }
            listener.onDone();
        }

    }


}
