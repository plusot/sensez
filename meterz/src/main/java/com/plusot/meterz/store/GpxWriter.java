package com.plusot.meterz.store;

import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.FileUtil;
import com.plusot.util.util.Format;
import com.plusot.util.util.TimeUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;

import static com.plusot.meterz.MeterzGlobals.LN;

public class GpxWriter implements Man.Listener {
    private static final String DELIM_DETAIL = "_";
    private static final String SPEC = "m";
    private static final String EXT = ".gp";
    private static final String FINAL_EXT = "x";
    private static GpxWriter instance = null;
    private boolean hasLatLng = false;
    private double lat = 0;
    private double lng = 0;
    private Integer hrm = null;
    private Integer hbi = null;
    private Double pow = null;
    private Double cad = null;
    private Double speed = null;
    private Double alt = null;
    private Double dist = null;
    private Double temp = null;
    private long timestamp = 0;

    private PrintWriter writer = null;
    private String logFilename = null;
    private long refTime = 0;
    private boolean closeing = false;
    private long timeFlushed = 0;

    @SuppressWarnings("unused")
    public static void stopInstance(final Class caller, final boolean writeConcat) {
        if (instance != null) synchronized (GpxWriter.class) {
            instance.close(caller, writeConcat);
            instance = null;

        }
        else {
            LLog.i("stopInstance(): no GPX file to close!");
        }
    }

    private GpxWriter() {
        Man.addListener(this);
    }

    @SuppressWarnings("unused")
    public static GpxWriter getInstance() {
        if (instance != null) return instance;
        if (!Globals.runMode.isRun()) return null;
        LLog.i("getInstance: Creating object");
        synchronized (GpxWriter.class) {
            if (instance == null) instance = new GpxWriter();
        }
        return instance;
    }

    @SuppressWarnings("unused")
    public static String getFileName(String session) {
        return Globals.getDataPath() + SPEC + "_" + session + EXT + FINAL_EXT;
    }

    private void reset() {
        hasLatLng = false;
        lat = 0;
        lng = 0;
        hrm = null;
        hbi = null;
        pow = null;
        cad = null;
        speed = null;
        alt = null;
        dist = null;
        temp = null;
        timestamp = 0;
        //		offset = null;
        //		slope = null;
        refTime = 0;
    }

    private void setPow(long time, Double pow) {
        if (pow == null) return;
        checkTime(time);
        this.pow = pow;
    }

    private void setHrm(long time, Integer hrm) {
        if (hrm == null) return;
        checkTime(time);
        this.hrm = hrm;
    }

    private void setHbi(long time, Integer hbi) {
        if (hbi == null) return;
        checkTime(time);
        this.hbi = hbi;
    }

    private void setCad(long time, Double cad) {
        if (cad == null) return;
        checkTime(time);
        this.cad = cad;
    }

    private void setLatLng(long time, double[] latlng) {
        if (latlng == null || latlng.length < 2) return;
        this.lat = latlng[0];
        this.lng = latlng[1];
        hasLatLng = true;
        checkTime(time);
    }

    private void setSpeed(long time, Double speed) {
        if (speed == null) return;
        checkTime(time);
        this.speed = speed;
    }

    private void setAlt(long time, Double alt) {
        if (alt == null) return;
        checkTime(time);
        this.alt = alt;
    }

    //	private void setOffset(long time, Double offset) {
    //		checkTime(time);
    //		this.offset = offset;
    //	}

    //	private void setSlope(long time, Double slope) {
    //		checkTime(time);
    //		this.slope = slope;
    //	}
    //
    //	private double getOffset() {
    //		return offset;
    //	}
    //
    //	private double getSlope() {
    //		return slope;
    //	}

    private void setDist(long time, Double dist) {
        if (dist == null) return;
        checkTime(time);
        this.dist = dist;
    }

    private void setTemp(long time, Double temp) {
        if (temp == null) return;
        checkTime(time);
        this.temp = temp;
    }

    private void deleteOld(final String ignoreFile) {
        File folder = new File(Globals.getDataPath());

        String[] fileList = folder.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {

                if (filename.endsWith(EXT) && !filename.contains(ignoreFile)) return true;

                return false;
            }
        });
        File file;
        for (String fileName : fileList) {
            file = new File(Globals.getDataPath() + fileName);
            file.delete();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private boolean open() {
        if (Globals.runMode.isFinished()) return false;
        boolean isNew = true;

        File file = new File(Globals.getDataPath());
        if (!file.isDirectory()) {
            file.mkdirs();
        }

        logFilename = Globals.getDataPath() + SPEC + DELIM_DETAIL + Globals.getSessionId() + EXT;

        file = new File(logFilename);
        try {
            //if (!file.exists()) {
                deleteOld(SPEC + DELIM_DETAIL + Globals.getSessionId() + EXT);
            //}
            isNew = file.createNewFile();
            if (isNew)
                LLog.i("open: Opening new file " + logFilename);
            else
                LLog.i("open: Appending to " + logFilename);
            writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        } catch (IOException e) {
            LLog.e("open: Error creating file");
            writer = null;
        }
        return isNew;
    }

    private void _save(String logStr) {
        if (writer != null && writer.checkError()) {
            LLog.i("log: Closing & opening writer due to error.");
            writer.close();
            writer = null;
        }
        if (writer == null) {
            if (open() && writer != null) {
                //				writeGpxOpen();
                //				writeTrackOpen();
                LLog.i("Writing file");
            }
        }
        if (writer == null) {
            LLog.i("save: Could not write: " + logStr);
        } else {
            //recordsWritten++;
            writer.println(logStr);
            flush();
        }
    }

    private void flush() {
        long now = System.currentTimeMillis();
        if (now - timeFlushed < Globals.FLUSH_TIME) return;
        if (writer != null) writer.flush();
        timeFlushed = System.currentTimeMillis();
    }

    private String getGpxOpen() {
        return "<?xml version=\"1.0\"?>" + LN +
                "<gpx version=\"1.1\" creator=\"Plusot - http://www.plusot.com\" " + LN +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " + LN +
                "xmlns=\"http://www.topografix.com/GPX/1/1\" " + LN +
                "xmlns:gpxdata=\"http://www.bikesenses.com/xmlschemas/GpxExtension/v2\" " + LN +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://plusot.net:6002/xmlschemas/GpxExtension/v2 http://plusot.net:6002/xmlschemas/GpxExtensionv2.xsd\"" + LN +
                //"xmlns:gpxdata=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"" +
                //"xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"" +
                ">" + LN;
    }

    private String getGpxClose() {
        return "</gpx>";
    }

    private String getTrackOpen() {
        return "<trk>" + LN +
                "<name>" + SPEC + DELIM_DETAIL + TimeUtil.formatTime(System.currentTimeMillis(), "yyyyMMdd-HHmmss") + "</name>" + LN +
                "<trkseg>" + LN;
    }

    private String getTrackClose() {
        return "</trkseg>" + LN +
                "</trk>" + LN;
    }

    private void writeTrackPoint() {
        synchronized (this) {
            _save("<trkpt lat=\"" + Format.format(lat, 7) + "\" lon=\"" + Format.format(lng, 7) + "\">");

            if (alt != null) _save("<ele>" + Format.format(alt, 1) + "</ele>");
            _save("<time>" + TimeUtil.formatTime(timestamp, "yyyy-MM-dd'T'HH:mm:ss'Z'") + "</time>");
            if (temp != null || cad != null || pow != null || speed != null || dist != null) {
                _save("<extensions>");
                if (hrm != null) _save("<gpxdata:hr>" + hrm + "</gpxdata:hr>");
                if (hbi != null) _save("<gpxdata:hbi>" + hbi + "</gpxdata:hbi>");
                if (cad != null)
                    _save("<gpxdata:cadence>" + Format.format(cad, 0) + "</gpxdata:cadence>");
                if (temp != null)
                    _save("<gpxdata:temp>" + Format.format(temp, 0) + "</gpxdata:temp>");
                if (pow != null)
                    _save("<gpxdata:power>" + Format.format(pow, 0) + "</gpxdata:power>");
                if (speed != null)
                    _save("<gpxdata:speed>" + Format.format(speed, 0) + "</gpxdata:speed>");
                if (dist != null)
                    _save("<gpxdata:distance>" + Format.format(dist, 0) + "</gpxdata:distance>");
                _save("</extensions>");
            }
            _save("</trkpt>");
        }
    }

    private void checkTime(long time) {
        timestamp = time;
        if (refTime == 0) refTime = time;
        if (!closeing && time - refTime >= 1000 && hasLatLng) {
            writeTrackPoint();
            refTime = time;
            hasLatLng = false;
        }
    }

    private void close(final Class caller, final boolean writeConcat) {
        Man.removeListener(this);
        try {
            synchronized (this) {
                closeing = true;
                LLog.i("File closed by " + caller.getSimpleName());
                if (writer == null) return;
                writer.close();
                writer = null;

                if (writeConcat) {
                    String start = getGpxOpen() + getTrackOpen();
                    String end = getTrackClose() + getGpxClose();
                    FileUtil.concat(start, end, new File(logFilename), new File(logFilename + FINAL_EXT));
                }
                logFilename = null;

                reset();
            }
        } finally {
            closeing = false;
        }
    }

    @Override
    public void onDeviceData(ScannedDevice device, Data data, boolean isNew) {
        if (!isNew) return;
        for (DataType type: data.getDataTypes())  {
            long timeStamp = data.getTime(device.getAddress(), type);
            switch (type) {
                case LOCATION:
                    setLatLng(timeStamp, data.getDoubles(device.getAddress(), type));
                    break;
                case POWER:
                    setPow(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case CADENCE:
                    setCad(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case DISTANCE:
                    setDist(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case SPEED:
                    setSpeed(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case TEMPERATURE:
                    setTemp(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case ALTITUDE:
                    setAlt(timeStamp, data.getDouble(device.getAddress(), type));
                    break;
                case HEART_RATE:
                    setHrm(timeStamp, data.getInt(device.getAddress(), type, 0));
                    break;
                case PULSE_WIDTH:
                    setHbi(timeStamp, data.getInt(device.getAddress(), type, 0));
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onDeviceScanned(ScannedDevice device, boolean isNew) {

    }

    @Override
    public void onDeviceState(ScannedDevice device, Device.StateInfo state) {

    }

    @Override
    public void onScanning(ScanManager.ScanType scanType, boolean on) {

    }
}
