package com.plusot.meterz;

import com.plusot.util.*;
import com.plusot.util.logging.*;

public class MeterzGlobals {
    public static final String LN = "\r\n";

    public static String getGpxPath() {
        return Globals.getStorePath() + "data/";
    }

	public static boolean hasExtraFeatures() {
		//return BuildConfig.EXTRA_FEATURES;   // Not working on AIDE
		boolean extraFeatures = false;
		try {
			java.lang.reflect.Field field = BuildConfig.class.getField("EXTRA_FEATURES");
			extraFeatures = field.getBoolean(BuildConfig.class);
		} catch (NoSuchFieldException e) {
			LLog.e("Could not find field");
		} catch (SecurityException e) {
			LLog.e("Not allow to find field");
		} catch (IllegalArgumentException e) {
			LLog.e("Illegal argument");	
		} catch (IllegalAccessException e) {
			LLog.e("Illegal access");
		}
		return extraFeatures;
	} 


}
