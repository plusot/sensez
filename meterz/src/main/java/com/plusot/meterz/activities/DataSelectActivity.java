package com.plusot.meterz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.plusot.meterz.R;
import com.plusot.meterz.preferences.PreferenceKey;
import com.plusot.sensez.activities.DeviceListActivity;
import com.plusot.sensez.activities.BackButtonActivity;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceDataType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.dialog.Alerts;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;


public class DataSelectActivity extends BackButtonActivity implements Man.Listener  {
    private DataSelectAdapter adapter = null;
    private boolean isPause = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //init(com.plusot.sensez.R.string.dataChoice, com.plusot.sensez.R.string.dataChoiceExplanation);

        setContentView(R.layout.activity_dataselect);

        ListView listView = findViewById(R.id.dataListView);
        adapter = new DataSelectAdapter(this, R.layout.adapter_dataselect);
        if (listView != null) listView.setAdapter(adapter);
        View view;

        if ((view = findViewById(R.id.addDevice)) != null) view.setOnClickListener(v -> {
            Intent intent = new Intent(DataSelectActivity.this, DeviceListActivity.class);
            //intent.putExtra(INT_EXTRA_TIMELOCATION, EventResult.Location.SPLIT1.ordinal());
            startActivity(intent);
        });

        SeekBar seekBar = findViewById(R.id.view_size_seek);
        TextView textView = findViewById(R.id.view_size_value);
        if (textView != null) textView.setText(getString(R.string.percentage_string, PreferenceKey.VIEW_SIZE.getInt()));
        if (seekBar != null) {
            seekBar.setMax(100);
            seekBar.setProgress(PreferenceKey.VIEW_SIZE.getInt());
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        PreferenceKey.VIEW_SIZE.set(Math.max(10, progress));
                        TextView textView = findViewById(R.id.view_size_value);
                        if (textView != null)
                            textView.setText(getString(R.string.percentage_string, Math.max(10, progress)));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
        seekBar = findViewById(R.id.transparency_seek);
        textView = findViewById(R.id.transparency_value);
        if (textView != null) textView.setText(getString(R.string.percentage_string, PreferenceKey.VIEW_TRANSPARENCY.getInt()));
        if (seekBar != null) {
            seekBar.setMax(100);
            seekBar.setProgress(PreferenceKey.VIEW_TRANSPARENCY.getInt());
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        PreferenceKey.VIEW_TRANSPARENCY.set(progress);
                        TextView textView = findViewById(R.id.transparency_value);
                        if (textView != null)
                            textView.setText(getString(R.string.percentage_string, progress));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
        isPause = false;
        if (listView != null && listView.getCount() == 0) Alerts.showOkAlert(this, R.string.noDataYet, R.string.selectSensors);
        start();
    }

    @Override
    public String getCaption() {
        return getString(com.plusot.sensez.R.string.dataChoice);
    }

    private void start() {
        if (com.plusot.sensez.preferences.PreferenceKey.EXPLAIN_DATA.isTrue()) {
            Alerts.showYesNoDialog(this, R.string.usageTitle, R.string.usageDataMsg, R.string.ok, R.string.noMore, clickResult -> {
                switch (clickResult) {
                    case YES:
                        break;
                    case NO:
                        com.plusot.sensez.preferences.PreferenceKey.EXPLAIN_DATA.set(false);
                        break;
                    case CANCEL:
                        break;
                    case NEUTRAL:
                        break;
                }
            }, -1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        isPause = false;
        Man.addListener(this);
        adapt();
        super.onResume();
    }

    @Override
    protected void onPause() {
        isPause = true;
        Man.removeListener(this);
        super.onPause();
    }

    @Override
    public void onDeviceData(final ScannedDevice device, final Data data, final boolean isNew) {
        if (!isNew || isPause) return;
        SleepAndWake.runInMain(() -> {
            for (DataType type: data.getDataTypes()) if (type.isSupported() && (device.isSelected() || device.isBeacon()) && !adapter.dataTypes.contains(new DeviceDataType(device, type))) {
                LLog.i("Adding data type: " + type + " for " + device);
                adapt();
                break;
            }
        }, "DataSelectActivity.onDeviceData");
    }


    @Override
    public void onDeviceScanned(final ScannedDevice device, final boolean isNew) {
        if (!isNew || isPause) return;
        SleepAndWake.runInMain(() -> {
            LLog.i("Adding device: " + device);
            adapt();
        }, "DataSelectActivity.onDeviceScanned");
    }

    @Override
    public void onDeviceState(ScannedDevice device, Device.StateInfo state) {

    }

    @Override
    public void onScanning(ScanManager.ScanType scanType, boolean on) {

    }

    protected void adapt() {
        if (adapter == null) return;
        adapter.dataSetChanged();
    }

}
