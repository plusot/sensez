package com.plusot.meterz.activities;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.plusot.meterz.MeterzApp;
import com.plusot.meterz.R;
import com.plusot.meterz.preferences.PreferenceKey;
import com.plusot.sensez.device.DeviceDataType;
import com.plusot.sensez.device.Man;
import com.plusot.util.data.DataType;
import com.plusot.util.dialog.Alerts;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.ToastHelper;

import java.util.List;


class DataSelectAdapter extends ArrayAdapter<DeviceDataType> {
    private final static boolean DEBUG = false;
    private final int resource;
    private final LayoutInflater inflater;
    //private List<Holder> holders = new ArrayList<>();
    List<DeviceDataType> dataTypes;


    DataSelectAdapter(final Context context, final int resource) {
        super(context, resource);
        addAll(dataTypes = Man.getDeviceDataTypes(true));
        this.resource = resource;
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    void dataSetChanged() {
        List<DeviceDataType> newDeviceDataTypes = Man.getDeviceDataTypes(true);
        boolean hasNew = false;
        for (DeviceDataType newType: newDeviceDataTypes) {
            if (dataTypes.contains(newType)) {
                if (DEBUG) LLog.i("List already contains: " + newType.getDataType());
            } else {
                LLog.i("List contains new data type: " + newType.getDataType());
                if (newType.getDataType() == DataType.TIME_ACTIVITY) {
                    LLog.i("New device Data types: " + newDeviceDataTypes);
                }
                hasNew = true;
                break;
            }
        }
        if (hasNew) {
            clear();
            addAll(dataTypes = newDeviceDataTypes);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        Holder holder;
        DeviceDataType deviceDataType = getItem(position);
        if (view == null) {
            view = inflater.inflate(resource, parent, false);
            holder = new Holder(view);
            view.setTag(holder);
            //holders.add(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        if (deviceDataType != null) {

            //holder.eventResult = result;
            holder.deviceName.setText(deviceDataType.getDevice().getName());
            holder.dataTypeName.setText(deviceDataType.getDataType().toString());
            //holder.deviceSetting.setText()
            holder.setDeviceDataType(deviceDataType);
            holder.draw();

            //drawUpdate(holder, deviceDataType);
        }
        return view;
    }


    private class Holder {
        final TextView dataTypeName;
        final TextView deviceName;
        final TextView deviceSetting;
        final ImageView checkedSpeech;
        final ImageView uncheckedSpeech;
        final Spinner viewLocationSpinner;

        DeviceDataType deviceDataType;

        Holder(View view) {
            this.dataTypeName = view.findViewById(R.id.dataType);
            this.deviceName = view.findViewById(R.id.deviceName);
            this.deviceSetting = view.findViewById(R.id.deviceSetting);
            this.checkedSpeech = view.findViewById(R.id.checkedSpeech);
            this.uncheckedSpeech = view.findViewById(R.id.uncheckedSpeech);
            this.viewLocationSpinner = view.findViewById(R.id.viewLocationSpinner);

            viewLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    deviceDataType.getDevice().setDataTypeView(deviceDataType.getDataType(), position);
                    if (position == 0) {
                        viewLocationSpinner.setBackgroundResource(R.drawable.background_spinner);
                    } else {
                        viewLocationSpinner.setBackgroundResource(R.drawable.background_spinner_checked);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            checkedSpeech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    uncheckedSpeech.setVisibility(View.VISIBLE);
                    checkedSpeech.setVisibility(View.GONE);
                    //listener.onSpeechSelect(deviceDataType, false);
                    deviceDataType.getDevice().setDataTypeSpeech(deviceDataType.getDataType(), false);
                }
            });

            uncheckedSpeech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PreferenceKey.SPEECH_INFO_CHECKED.isTrue()) {
                        Alerts.showYesNoDialog(getContext(), R.string.speechCheckedTitle, R.string.speechCheckedMsg, R.string.test, R.string.setup, R.string.noMore, new Alerts.Listener() {

                            @Override
                            public void onClick(Alerts.ClickResult clickResult) {
                                switch(clickResult) {

                                    case YES:
                                        MeterzApp.Companion.sayIt(getContext().getString(R.string.demoText));
                                        ToastHelper.showToastLong(R.string.demoText);
                                        break;
                                    case NO:
                                        getContext().startActivity(new Intent("com.android.settings.TTS_SETTINGS"));
                                        break;
                                    case CANCEL:
                                        break;
                                    case NEUTRAL:
                                        PreferenceKey.SPEECH_INFO_CHECKED.set(true);
                                        break;
                                }
                            }
                        });
                    }
                    checkedSpeech.setVisibility(View.VISIBLE);
                    uncheckedSpeech.setVisibility(View.GONE);
                    //listener.onSpeechSelect(deviceDataType, true);
                    deviceDataType.getDevice().setDataTypeSpeech(deviceDataType.getDataType(), true);
                }
            });


        }

        void setDeviceDataType(DeviceDataType deviceDataType) {
            this.deviceDataType = deviceDataType;
        }

        void draw() {
            int viewNr = deviceDataType.getDevice().getDataTypeView(deviceDataType.getDataType());
            viewLocationSpinner.setSelection(Math.min(viewNr, viewLocationSpinner.getAdapter().getCount() - 1));
            if (viewLocationSpinner.getSelectedItemPosition() == 0) {
                viewLocationSpinner.setBackgroundResource(R.drawable.background_spinner);
            } else {
                viewLocationSpinner.setBackgroundResource(R.drawable.background_spinner_checked);
            }

            if (deviceDataType.getDevice().isDataTypeSpeech(deviceDataType.getDataType())) {
                checkedSpeech.setVisibility(View.VISIBLE);
                uncheckedSpeech.setVisibility(View.GONE);
            } else {
                checkedSpeech.setVisibility(View.GONE);
                uncheckedSpeech.setVisibility(View.VISIBLE);
            }
        }
    }

}
