package com.plusot.meterz.activities;

public enum ActivityResult {
    RESULT_PREFERENCES,
    RESULT_UNKNOWN;

    public static ActivityResult fromOrdinal(int ordinal) {
        for (ActivityResult result : ActivityResult.values()) {
            if (result.ordinal() == ordinal) return result;
        }
        return RESULT_UNKNOWN;
    }
}