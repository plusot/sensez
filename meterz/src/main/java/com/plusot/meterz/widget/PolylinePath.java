package com.plusot.meterz.widget;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;
import android.view.MotionEvent;

import com.plusot.meterz.preferences.PreferenceKey;
import com.plusot.sensez.SensezGlobals;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.Timing;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.PointL;
import org.osmdroid.util.TileSystem;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.OverlayWithIW;

import java.util.ArrayList;
import java.util.List;


class PolylinePath extends OverlayWithIW {

    private final static float CROSS_BCK = 0.8f;
    private final static float CROSS_FWD = 1.2f;
    private final static float CROSS_WID = 0.8f;

    private final static float DEFAULT_CROSS_SIZE_RATIO = 0.05f;
    private final Path mPath = new Path();
    private Paint mPaint = new Paint();
    /** points, converted to the map projection */
    private ArrayList<PathPoint> mPoints;
    private int mPointsPrecomputed;
    private float strokeWidth = 12.0f;

    private final Point mTempPoint1 = new Point();
    private final Point mTempPoint2 = new Point();

    private OnClickListener onClickListener;

    private boolean touched = false;
    private long touchedSet = 0;
    private int pathId;
    private int color = Color.RED;
    private float crossSizeRatio = DEFAULT_CROSS_SIZE_RATIO;
    private GeoPoint prevPoint = null;
    private double distance = 0;
    private boolean showAlt = false;
    private boolean isActiveGpx = false;

    private enum PointType {
        INTERMEDIATE,
        END
    }

    class PathPoint {
        final PointL point;
        final PointType type;
        final double distance;
        final double altitude;

        private PathPoint(PointL point, PointType type, double distance, double altitude) {
            this.point = point;
            this.type = type;
            this.distance = distance;
            this.altitude = altitude;
        }
    }

    PolylinePath(int pathId){
        super();
        this.pathId = pathId;
        mPaint.setColor(color);
        mPaint.setStrokeWidth(strokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setAntiAlias(true);
        this.clearPath();
        //mGeodesic = false;
    }

    void clearPath() {
        this.mPoints = new ArrayList<>();
        this.mPointsPrecomputed = 0;
        distance = 0;
        prevPoint = null;
    }

    void addPoint(final GeoPoint aPoint) {
        addPoint(aPoint, true);
    }

    PathPoint getPoint(int index) {
        if (index < 0 || index >= mPoints.size()) return null;
        return mPoints.get(index);
    }

    @SuppressWarnings("deprecation")
    private void addPoint(final GeoPoint geoPoint, final boolean checkDistance) {
        PointType type = PointType.INTERMEDIATE;
        double delta = 0;
        if (prevPoint != null) delta = geoPoint.distanceToAsDouble(prevPoint);
        if (checkDistance && delta > 300) type = PointType.END;
        distance += delta;
        mPoints.add(
                new PathPoint(
                        new PointL(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()),
                        type, distance,
                        geoPoint.getAltitude()
                )
        );
        prevPoint = geoPoint;
    }

    public int getColor(){
        return mPaint.getColor();
    }

    public boolean isVisible(){
        return isEnabled();
    }

    public void setColor(int color){
        mPaint.setColor(this.color = color);
    }

    void setWidth(float width){
        mPaint.setStrokeWidth(this.strokeWidth = width);
    }

    void setShowAlt(boolean showAlt){
        this.showAlt = showAlt;
    }

    void setIsGpx(boolean isGpx){
        this.isActiveGpx = isGpx;
    }

    void setDashed(float[] dashes){
        if (dashes != null)
            mPaint.setPathEffect(new DashPathEffect(dashes, 0));
        else
            mPaint.setPathEffect(null);
    }

    public void setVisible(boolean visible){
        setEnabled(visible);
    }

    void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
        int iPoi = com.plusot.meterz.preferences.PreferenceKey.POI_INDEX.getInt();
        if (iPoi >= 0 && iPoi < mPoints.size()) fireClick(null, new int[]{iPoi, -1}, true);
    }

    double setPoints(List<GeoPoint> points){
        clearPath();
        int size = points.size();
        if (size == 0) return 0;
        for (int i=0; i < size; i++){
            GeoPoint p = points.get(i);
            addPoint(p, false);
        }
        if (mPoints.size() > 100) LLog.i("Added " + mPoints.size() + " points");
        return distance;
    }

    private void precomputePoints(Projection pj){
        final int size = this.mPoints.size();
        while (this.mPointsPrecomputed < size) {
            final PointL pt = this.mPoints.get(this.mPointsPrecomputed).point;
            pj.toProjectedPixels(pt.x, pt.y, pt);
            this.mPointsPrecomputed++;
        }
    }

    private boolean firstTime = true;

    @Override
    public void draw(final Canvas canvas, final MapView mapView, final boolean shadow) {
        Timing.log(PolylinePath.class.getSimpleName() + "_" + pathId, Timing.Action.BEGIN);

        if (shadow || touched) {
            if (touched && System.currentTimeMillis() - touchedSet > 5000)
                touched = false;
            else
                return;
        }

        //int fillColor = 0xff07a4;
        int fillAlpha = 60;
        int crossSize = 0;
        int skipped = 0;
        final float canvasSize = 0.5f * (float) Math.sqrt(1.0 * canvas.getHeight() * canvas.getHeight() + 1.0 * canvas.getWidth() * canvas.getWidth());

        if (crossSizeRatio > 0) crossSize = (int) (crossSizeRatio * canvasSize);

        final int size = mPoints.size();
        if (size == 0) {
            // nothing to paint
            return;
        }

        final Projection pj = mapView.getProjection();

        final int halfMapSize = TileSystem.MapSize((int) mapView.getProjection().getZoomLevel()) / 2; // 180° in longitude in pixels
        final int southLimit = pj.toPixelsFromMercator(0, halfMapSize * 2, null).y;            // southern Limit of the map in Pixels

        // precompute new points to the intermediate projection.
        precomputePoints(pj);

        PointL projectedPoint0 = mPoints.get(0).point; // points from the points list

        Point screenPoint0 = pj.toPixelsFromProjected(projectedPoint0, mTempPoint1); // points on screen
        Point screenPoint1;

        mPath.rewind();
        boolean sizeSkip = false;
        if (Math.abs(screenPoint0.x + canvas.getWidth() / 2) > canvas.getWidth() * 3 ||
                Math.abs(screenPoint0.y + canvas.getHeight() / 2) > canvas.getHeight() * 3) {
            sizeSkip = true;
            skipped++;
        } else
            mPath.moveTo(screenPoint0.x, screenPoint0.y);


        double deltaX = 0;
        double deltaY = 0;
        boolean madePath = false;
        int minDist = 1;
        if (size > 5) minDist = 3;
        double prevAltitude = Double.NaN;
        double prevAltitudeDistance = 0;
        int iPoi = com.plusot.meterz.preferences.PreferenceKey.POI_INDEX.getInt();

        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double maxY = Double.MIN_VALUE;

        if (size >= 2) for (int i = 1; i < size; i++) {
            // compute next points
            PathPoint pathPoint = mPoints.get(i);
            PointL projectedPoint1 = pathPoint.point;
            screenPoint1 = pj.toPixelsFromProjected(projectedPoint1, this.mTempPoint2);
            if (    Math.abs(screenPoint1.x + canvas.getWidth()  / 2) > canvas.getWidth()  * 3 ||
                    Math.abs(screenPoint1.y + canvas.getHeight() / 2) > canvas.getHeight() * 3) {
                sizeSkip = true;
                skipped++;
                continue;
            }

            if (isActiveGpx) {
                if (i - 1 == iPoi) {
                    if (firstTime) fireClick(null, new int[]{iPoi, -1}, true);
                    mPaint.setStyle(Paint.Style.FILL);
                    mPaint.setColor(0xBBFFFFFF);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 1.5f, mPaint);
                    mPaint.setStrokeWidth(1.5f * strokeWidth);
                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setColor(0xBBFF0000);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 2, mPaint);
                }
                if (i - 1 == iCloseToMyPos) {
                    mPaint.setStyle(Paint.Style.FILL);
                    mPaint.setColor(0xBBFFFFFF);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 1.5f, mPaint);
                    mPaint.setStrokeWidth(1.5f * strokeWidth);
                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setColor(0xBB00FF00);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 2, mPaint);
                }
                if (i - 1 == iMapIndex) {
                    mPaint.setStyle(Paint.Style.FILL);
                    mPaint.setColor(0xBBFFFFFF);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 1.5f, mPaint);
                    mPaint.setStrokeWidth(1.5f * strokeWidth);
                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setColor(0xBB007700);
                    canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 2, mPaint);
                }
                firstTime = false;
            }
            if (Math.abs(screenPoint1.x - screenPoint0.x) + Math.abs(screenPoint1.y - screenPoint0.y) <= minDist) {
                // skip this point, too close to previous point
                skipped++;
                continue;
            }

            // check for lines exceeding 180° in longitude, or lines crossing to another map:
            // cut line into two segments
            if ((Math.abs(screenPoint1.x - screenPoint0.x) > halfMapSize)
                    // check for lines crossing the southern limit
                    || (screenPoint1.y >= southLimit) != (screenPoint0.y >= southLimit)) {
                // handle x and y coordinates separately
                int x0 = screenPoint0.x;
                int y0 = screenPoint0.y;
                int x1 = screenPoint1.x;
                int y1 = screenPoint1.y;

                // first check x
                if (Math.abs(screenPoint1.x - screenPoint0.x) > halfMapSize) {// x has to be adjusted
                    if (screenPoint1.x < mapView.getWidth() / 2) {
                        // screenPoint1 is left of screenPoint0
                        x1 += halfMapSize * 2; // set x1 360° east of screenPoint1
                        x0 -= halfMapSize * 2; // set x0 360° west of screenPoint0
                    } else {
                        x1 -= halfMapSize * 2;
                        x0 += halfMapSize * 2;
                    }
                }

                // now check y
                if ((screenPoint1.y >= southLimit) != (screenPoint0.y >= southLimit)) {
                    // line is crossing from one map to the other
                    if (screenPoint1.y >= southLimit) {
                        // screenPoint1 was switched to map below
                        y1 -= halfMapSize * 2;  // set y1 into northern map
                        y0 += halfMapSize * 2;  // set y0 into map below
                    } else {
                        y1 += halfMapSize * 2;
                        y0 -= halfMapSize * 2;
                    }
                }
                mPath.lineTo(x1, y1);
                mPath.moveTo(x0, y0);
            } // end of line break check

            if (sizeSkip || pathPoint.type == PointType.END) {
                sizeSkip = false;
                mPath.moveTo(screenPoint1.x, screenPoint1.y);
            } else
                mPath.lineTo(screenPoint1.x, screenPoint1.y);


            maxX = Math.max(maxX, screenPoint1.x);
            minX = Math.min(minX, screenPoint1.x);
            maxY = Math.max(maxY, screenPoint1.y);
            minY = Math.min(minY, screenPoint1.y);


            deltaX = screenPoint0.x - screenPoint1.x;
            deltaY = screenPoint0.y - screenPoint1.y;


            if (showAlt && PreferenceKey.CONTOUR_LINES.isTrue() && !Double.isNaN(pathPoint.altitude)) {
                double delta;
                if (Double.isNaN(prevAltitude)) {
                    prevAltitude = pathPoint.altitude;
                    prevAltitudeDistance = pathPoint.distance;
                } else if (Math.abs(delta = pathPoint.altitude - prevAltitude) >= 5) {
                    double length = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                    float altX = (float) (deltaX * (pathPoint.altitude / 100 + 2 * strokeWidth) / length);
                    float altY = (float) (deltaY * (pathPoint.altitude / 100 + 2 * strokeWidth) / length);
                    mPaint.setStrokeWidth(7 * strokeWidth / 10);
                    //mPaint.setAlpha(128);
                    double slope = delta / (pathPoint.distance - prevAltitudeDistance + 0.1);
                    if (delta > 0) {
                        if (slope > 0.09)
                            mPaint.setColor(0xFAFF00FF);
                        if (slope > 0.06)
                            mPaint.setColor(0xFAFF0000);
                        else if (slope > 0.03)
                            mPaint.setColor(0xFAFF7700);
                        else
                            mPaint.setColor(0xFAFFFF00);

                    } else
                        mPaint.setColor(0xFA00FF00);

                    canvas.drawLine(
                            screenPoint0.x - altY,
                            screenPoint0.y + altX,
                            screenPoint0.x + altY,
                            screenPoint0.y - altX,
                            mPaint);
                    mPaint.setStrokeWidth(strokeWidth);

                    prevAltitude = pathPoint.altitude;
                    prevAltitudeDistance = pathPoint.distance;
                }

            }
            // update starting point to next position
            screenPoint0.x = screenPoint1.x;
            screenPoint0.y = screenPoint1.y;
            madePath = true;
        }

        if (madePath) {
            mPaint.setStrokeWidth(strokeWidth);
            mPaint.setAlpha(255);
            mPaint.setColor(color);
            mPaint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(mPath, mPaint);
            if (crossSize > 0) {
                if (deltaX == 0 && deltaY == 0) deltaY = 1;
                double length = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                deltaX *= crossSize / length;
                deltaY *= crossSize / length;
                mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                mPaint.setColor(color);
                mPaint.setAlpha(fillAlpha);
                mPaint.setStrokeWidth(strokeWidth / 2);
                Path fill = new Path();
                fill.moveTo(screenPoint0.x, screenPoint0.y);
                fill.lineTo(screenPoint0.x + (int) (CROSS_BCK * deltaX) - (int) (CROSS_WID * deltaY), screenPoint0.y + (int) (CROSS_BCK * deltaY) + (int) (CROSS_WID * deltaX));
                fill.lineTo(screenPoint0.x - (int) (CROSS_FWD * deltaX), screenPoint0.y - (int) (CROSS_FWD * deltaY));
                fill.lineTo(screenPoint0.x + (int) (CROSS_BCK * deltaX) + (int) (CROSS_WID * deltaY), screenPoint0.y + (int) (CROSS_BCK * deltaY) - (int) (CROSS_WID * deltaX));
                fill.lineTo(screenPoint0.x, screenPoint0.y);
                canvas.drawPath(fill, this.mPaint);
                Path arrow = new Path();
                mPaint.setAlpha(255);
                //mPaint.setColor(color);
                mPaint.setStyle(Paint.Style.STROKE);
                arrow.moveTo(screenPoint0.x, screenPoint0.y);
                arrow.lineTo(screenPoint0.x + (int) (CROSS_BCK * deltaX) - (int) (CROSS_WID * deltaY), screenPoint0.y + (int) (CROSS_BCK * deltaY) + (int) (CROSS_WID * deltaX));
                arrow.lineTo(screenPoint0.x - (int) (CROSS_FWD * deltaX), screenPoint0.y - (int) (CROSS_FWD * deltaY));
                arrow.lineTo(screenPoint0.x + (int) (CROSS_BCK * deltaX) + (int) (CROSS_WID * deltaY), screenPoint0.y + (int) (CROSS_BCK * deltaY) - (int) (CROSS_WID * deltaX));
                arrow.lineTo(screenPoint0.x, screenPoint0.y);
                canvas.drawPath(arrow, this.mPaint);
            }
        } else if (crossSize > 0) {
            mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            mPaint.setColor(color);
            mPaint.setAlpha(fillAlpha);
            mPaint.setStrokeWidth(strokeWidth / 2);
            canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 1.5f, mPaint);
            mPaint.setColor(color);
            mPaint.setAlpha(255);
            mPaint.setStyle(Paint.Style.STROKE);
            canvas.drawCircle(screenPoint0.x, screenPoint0.y, strokeWidth * 1.5f, mPaint);
        }

//        Timing.log(PolylinePath.class.getSimpleName() + "_" + pathId, Timing.Action.END, "Drawing " + mPoints.size() + " points, skipped: " + skipped);
//        LLog.d("Drawing " + mPoints.size() + " points, skipped: " + skipped +  ", box = (" +
//                Format.format(minX, 2) + ", " +
//                Format.format(minY, 2) + " - " +
//                Format.format(maxX, 2) + ", " +
//                Format.format(maxY, 2) + "), canvas: " +
//                canvas.getWidth() + ", " +
//                canvas.getHeight()
//        );

    }

    private int iCloseToMyPos = -1;


    private int[] isCloseTo(GeoPoint point, double tolerance, MapView mapView) {
        final Projection pj = mapView.getProjection();
        precomputePoints(pj);
        Point p = pj.toPixels(point, null);
        Location location = SensezGlobals.lastLocation;
        Point myPos = null;
        if (location != null) {
            GeoPoint geoPointMyPos = new GeoPoint(location.getLatitude(), location.getLongitude());
            myPos = pj.toPixels(geoPointMyPos, null);
        }int i = 0;
        iCloseToMyPos = -1;
        int iFound = -1;
        double distMyPos = Double.NaN;
        boolean found; // = false;
        while (i < mPointsPrecomputed - 1) { // && !found) {
            PointL projectedPoint1 = mPoints.get(i).point;
            if (i == 0){
                pj.toPixelsFromProjected(projectedPoint1, mTempPoint1);
            } else {
                //reuse last b:
                mTempPoint1.set(mTempPoint2.x, mTempPoint2.y);
            }
            if (myPos != null) {
                double dist = distance(myPos, mTempPoint1);
                if (Double.isNaN(distMyPos) || dist < distMyPos) {
                    distMyPos = dist;
                    iCloseToMyPos = i;
                }
            }
            PointL projectedPoint2 = mPoints.get(i + 1).point;
            pj.toPixelsFromProjected(projectedPoint2, mTempPoint2);

            found = (linePointDist(mTempPoint1, mTempPoint2, p, true) <= tolerance);
            if (found) {
                iFound = i;
            }
            i++;
        }

        return new int[]{iFound, iCloseToMyPos};
    }

    // Compute the dot product AB x AC
    private double dot(Point A, Point B, Point C) {
        double AB_X = B.x - A.x;
        double AB_Y = B.y - A.y;
        double BC_X = C.x - B.x;
        double BC_Y = C.y - B.y;
        return AB_X * BC_X + AB_Y * BC_Y;
    }

    // Compute the cross product AB x AC
    private double cross(Point A, Point B, Point C) {
        double AB_X = B.x - A.x;
        double AB_Y = B.y - A.y;
        double AC_X = C.x - A.x;
        double AC_Y = C.y - A.y;
        return AB_X * AC_Y - AB_Y * AC_X;
    }

    // Compute the distance from A to B
    private double distance(Point A, Point B) {
        double dX = A.x - B.x;
        double dY = A.y - B.y;
        return Math.sqrt(dX * dX + dY * dY);
    }

    private double linePointDist(Point A, Point B, Point C, boolean isSegment) {
        double dAB = distance(A, B);
        if (dAB == 0.0)
            return distance(A, C);
        double dist = cross(A, B, C) / dAB;
        if (isSegment) {
            double dot1 = dot(A, B, C);
            if (dot1 > 0)
                return distance(B, C);
            double dot2 = dot(B, A, C);
            if (dot2 > 0)
                return distance(A, C);
        }
        return Math.abs(dist);
    }

    @Override public boolean onSingleTapConfirmed(final MotionEvent event, final MapView mapView){
        findPoi(event, mapView, false);
        return false;
    }

    private void findPoi(final MotionEvent event, final MapView mapView, final boolean use) {
        final Projection pj = mapView.getProjection();
        final GeoPoint eventPos = (GeoPoint) pj.fromPixels((int)event.getX(), (int)event.getY());

        SleepAndWake.runInNewThread(new Runnable() {
            @Override public void run() {
                double tolerance = mPaint.getStrokeWidth() * 4;
                final int[] indices = isCloseTo(eventPos, tolerance, mapView);
                if (indices[0] >= 0) SleepAndWake.runInMain(new Runnable() {
                    @Override public void run() {
                        fireClick(eventPos, indices, use);
                    }
                }, "PolylinePath.runOnClicked");
            }
        }, "PolylinePath.onSingleTapConfirmed");
    }

    @Override public boolean onLongPress(final MotionEvent event, final MapView mapView){
        findPoi(event, mapView, true);
        return false;
    }

    private void fireClick(GeoPoint eventPos, final int[] indices, boolean use) {
        if (onClickListener != null) onClickListener.onClick(PolylinePath.this, eventPos, indices, use);
    }

    private int iMapIndex = -1;

    double distanceToPoi(int myIndex) {
        iCloseToMyPos = -1;
        iMapIndex = myIndex;
        int index = com.plusot.meterz.preferences.PreferenceKey.POI_INDEX.getInt();
        if (index >= 0 && index < mPoints.size() && myIndex < mPoints.size()) {
            return Math.abs(
                    mPoints.get(index).distance -
                            mPoints.get(myIndex).distance);
        }
        return Double.NaN;
    }

    interface OnClickListener{
        boolean onClick(PolylinePath polyline, GeoPoint eventPos, int[] indices, boolean use);
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public void onDetach(MapView mapView) {
        onClickListener = null;
        onDestroy();
    }

    void isTouched(boolean value) {
        touched = value;
        touchedSet = System.currentTimeMillis();
    }

    void setCross(boolean value) {
        if (value)
            crossSizeRatio = DEFAULT_CROSS_SIZE_RATIO;
        else
            crossSizeRatio = 0;
    }

}

