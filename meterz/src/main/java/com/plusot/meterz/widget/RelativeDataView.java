package com.plusot.meterz.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plusot.meterz.R;
import com.plusot.meterz.preferences.PreferenceKey;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceDataType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.Timing;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.Watchdog;
import com.plusot.util.widget.FitLabelView;
import com.plusot.util.widget.SmoothLineView;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RelativeDataView extends RelativeLayout implements Man.Listener, Watchdog.Watchable {
    private HashMap<String, Data> viewData = new HashMap<>();
    private EnumMap<DataType, Boolean> isNew = new EnumMap<>(DataType.class);
    private EnumMap<DataType, Long> prevUpdates = new EnumMap<>(DataType.class);
    private List<DeviceDataType> deviceDataTypeSet = new ArrayList<>();

    private static final long VIEW_LIFE = 180000;
    private int viewCounter = 0;
    private int viewNr = 1;
    private DeviceDataType currentDdt = null;
    private long referenceTime = System.currentTimeMillis();
    private View rootView;
    private int width;
    private int height;
    private FitLabelView valueView;
    private FitLabelView maxView;
    private FitLabelView avgView;
    private FitLabelView minView;
    private Map<String, SmoothLineView> sparkLines = new HashMap<>();
    private SmoothLineView currentSparkLine = null;
    private TextView unitView;
    private TextView typeView;
    private TextView deviceView;
    private int alpha = -1;

    public RelativeDataView(Context context) {
        super(context);
    }

    public RelativeDataView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

    }

    public RelativeDataView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }
//
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public RelativeDataView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        initAfterPermissionsChecked(context, attrs);
//    }

    private void init(Context context, AttributeSet attrs) {
        if (isInEditMode()) return;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.relativeDataView);
        viewNr = a.getInt(R.styleable.relativeDataView_viewId, 1);
//        maxViews = a.getInt(R.styleable.LinearDataView_maxViews, 3);

        a.recycle();
        rootView = inflate(context, R.layout.relative_data_view, this);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            Man.addListener(this);
            Watchdog.addWatchable(this, 5000);

//            findViewById(R.id.value).setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (getContext() instanceof Activity) {
//                        Activity activity = (Activity) getContext();
//                        Intent intent = new Intent(activity, DataSelectActivity.class);
//                        activity.startActivity(intent);
//                    }
//                }
//            });

            SleepAndWake.runInMain(new Runnable() {
                @Override
                public void run() {
                    checkSize();
                }
            }, 200, "RelativeDataView.onFinishInflate");
            //ViewGroup layout = (ViewGroup) rootView; // findViewById(R.id.textViews);

        }
        typeView = ((TextView) findViewById(R.id.type));
        valueView = ((FitLabelView) findViewById(R.id.value));
        maxView = ((FitLabelView) findViewById(R.id.max));
        avgView = ((FitLabelView) findViewById(R.id.avg));
        minView = ((FitLabelView) findViewById(R.id.min));
        unitView = ((TextView) findViewById(R.id.unit));
        deviceView = ((TextView) findViewById(R.id.device));
        this.setVisibility(INVISIBLE);
    }

    public void checkSize() {
        int screenSize = Math.min(Globals.screenWidth, Globals.screenHeight);
        int size = Math.max(screenSize * PreferenceKey.VIEW_SIZE.getInt() / 100, screenSize / 10);
        width = size;
        height = size;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rootView.getLayoutParams();
        RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(width, height);
        if (params != null) {
            int[] rules = params.getRules();
            for (int i = 0; i < rules.length; i++) if (rules[i] != 0) newParams.addRule(i, rules[i]);
        }
        if (viewNr < 3 || viewNr > 7)
            newParams.setMargins(10, 70, 10, 10);
        else if (viewNr >= 4 && viewNr < 7)
            newParams.setMargins(10, 10, 10, 70);
        else
            newParams.setMargins(10, 10, 10, 10);
        rootView.setLayoutParams(newParams);
        //valueView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.min(height / 3, width / 3));
        typeView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.min(height / 7, width / 7));
        unitView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.min(height / 7, width / 7));
        deviceView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.min(height / 12, width / 12));

    }

    public void checkSize(SmoothLineView sparkLine) {
        RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(width - 10, height - 10);
        newParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        newParams.setMargins(20, 20, 20, 20);
        sparkLine.setLayoutParams(newParams);
    }


    @Override
    public void onDeviceData(final ScannedDevice device, final Data data, boolean isNew) {
        if (data == null) return;
//        if (data.hasDataType(DataType.HEART_RATE) && device.getDataTypeView(DataType.HEART_RATE) == viewNr) LLog.i("New Data: " + data);

        long now = System.currentTimeMillis();
        //LLog.i(viewNr + ": " + device.getSensorId() + ": " + data.toString());
        for (DataType type : data.getDataTypes()) {
            //LLog.i(viewNr + ", checked view = " + device.getDataTypeView(type) + " for " + type);
            if (device.getDataTypeView(type) == viewNr && type.isSupported() && !type.isMapType()) {
                Long prevUpdate = prevUpdates.get(type);
                if (prevUpdate != null && now - prevUpdate < 50 && Man.isShowNew()) continue;
                this.isNew.put(type, isNew);
                prevUpdates.put(type, now);

                final DeviceDataType ddt = new DeviceDataType(device, type);
                if (!deviceDataTypeSet.contains(ddt)) {
                    deviceDataTypeSet.add(ddt);
                    LLog.i("Added " + ddt + " to view " + viewNr);
                }
                final DataType fType = type;
                final String address = device.getAddress();
                SleepAndWake.runInMain(() -> {
                    if (fType.isGraphable()) {
                        final double[] vals = data.getDoubles(address, fType);
                        if (vals != null && vals.length >= 1) {
                            SmoothLineView sparkLine = sparkLines.get(ddt.toAddressDataTypeString());
                            if (sparkLine == null) {
                                if (sparkLines.size() < 10) {
                                    sparkLines.put(ddt.toAddressDataTypeString(), sparkLine = new SmoothLineView(getContext(), 200, viewNr) {
                                        {
                                            setHasCircles(false);
                                            setId(View.NO_ID);
                                            setColor(getResources().getColor(R.color.spriteColor)); //0x70cd81fc);
                                            setStrokeWidth(4);
                                            setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    ((RelativeLayout) rootView).addView(sparkLine, 0);

                                    final SmoothLineView sp = sparkLine;
                                    SleepAndWake.runInMain(new Runnable() {
                                        @Override
                                        public void run() {
                                            checkSize(sp);
                                        }
                                    }, 200, "RelativeDataView.onDeviceData.checkSize");
                                    sparkLine.addValue((float) vals[0]);
                                }
                            } else
                                sparkLine.addValue((float) vals[0]);


                        }
                    }
                    if (currentDdt == null || ddt.equals(currentDdt)) showData(ddt, data);
                }, "RelativeDataView.onDeviceData");
                Data vd;
                if ((vd = viewData.get(device.getAddress())) == null)
                    viewData.put(device.getAddress(), data.clone());
                else
                    vd.merge(data);
            }

        }
    }

    @Override
    protected void onDetachedFromWindow() {
        Man.removeListener(this);
        Watchdog.remove(this);
        super.onDetachedFromWindow();
    }

    @Override
    public void onDeviceScanned(ScannedDevice device, boolean isNew) {

    }

    @Override
    public void onDeviceState(ScannedDevice device, Device.StateInfo state) {

    }

    @Override
    public void onScanning(ScanManager.ScanType scanType, boolean on) {

    }

    private void showData(final DeviceDataType ddt, final Data data)  {
        if (currentDdt == null) currentDdt = ddt;
        if (data == null) return;
        if (ddt == null) return;
        if (alpha != PreferenceKey.VIEW_TRANSPARENCY.getInt()) {
            alpha = PreferenceKey.VIEW_TRANSPARENCY.getInt();
            this.rootView.getBackground().setAlpha(255 * (100 - alpha) / 100);
        }
        SmoothLineView sparkLine = sparkLines.get(ddt.toAddressDataTypeString());

        DataType type = ddt.getDataType();
//        Application app = Globals.getApp();
//        if (app == null) return;
        if (!Globals.runMode.isRun()) return;

        if (ddt.getViewNr() != viewNr) return;

        String[] val;
        Data maxData;
        if (    type.hasMax() && //ddt.getDevice().isMaxDataType(type) &&
                (maxData = Man.getMax(ddt.getDevice().getAddress(), type)) != null &&
                (val = maxData.toValueStringParts(ddt.getDevice().getAddress(), type)) != null &&
                val.length >= 1
                )
            maxView.setText(val[0]);
        else
            maxView.setText("");

        Data avgData;
        if (    type.hasAvg() &&
                (avgData = Man.getAvg(ddt.getDevice().getAddress(), type)) != null &&
                (val = avgData.toValueStringParts(ddt.getDevice().getAddress(), type)) != null &&
                val.length >= 1
                )
            avgView.setText(val[0]);
        else
            avgView.setText("");

        Data minData;
        if (    type.hasMin() &&
                (minData = Man.getMin(ddt.getDevice().getAddress(), type)) != null &&
                (val = minData.toValueStringParts(ddt.getDevice().getAddress(), type)) != null &&
                val.length >= 1
                )
            minView.setText(val[0]);
        else
            minView.setText("");

        if ((val = data.toValueStringParts(ddt.getDevice().getAddress(), type)) == null) return;

        if (getVisibility() == View.INVISIBLE && !MyMapView.isTouched()) setVisibility(View.VISIBLE);
        if (currentSparkLine != null && currentSparkLine != sparkLine) currentSparkLine.setVisibility(View.GONE);
        if (sparkLine != null && sparkLine.getVisibility() != View.VISIBLE) sparkLine.setVisibility(View.VISIBLE);
        currentSparkLine = sparkLine;

        typeView.setText(ddt.getDataType().toProperString());
        if (ddt.getDataType().toProperString().equalsIgnoreCase(ddt.getDevice().getName()))
            deviceView.setText("");
        else
            deviceView.setText(ddt.getDevice().getName());
        if (val.length >= 1) valueView.setText(val[0]);

        if (val.length >= 2)
            unitView.setText(val[1]);
        else
            unitView.setText("");
        referenceTime = System.currentTimeMillis();
    }

    @Override
    public void onWatchdogTimer(long count) {
        Timing.log(RelativeDataView.class.getSimpleName() + "_onWatchdogCheck_" + viewNr, Timing.Action.BEGIN);
        long now = System.currentTimeMillis();
        //if (System.currentTimeMillis() - referenceTime < 10000 && getVisibility() == View.INVISIBLE) setVisibility(VISIBLE);
        List<DeviceDataType> list = deviceDataTypeSet;
        if (Man.isShowNew()) {
            if (now - referenceTime > VIEW_LIFE && getVisibility() == View.VISIBLE) {
                SleepAndWake.runInMain(new Runnable() {
                    @Override
                    public void run() {
                        setVisibility(INVISIBLE);
                    }
                }, "RelativeDataView.onWatchdogTimer");
            }
            //list = Man.getViewCheckedDeviceDataTypes(viewNr, false);
        } /*else {
            list = deviceDataTypeSet;

        }*/
        if (list == null || list.size() == 0) return;

        for (int i = 0; i < list.size(); i++) {
            viewCounter++;
            viewCounter %= list.size();
            currentDdt = list.get(viewCounter);
            if (currentDdt == null) continue;
            String address = currentDdt.getDevice().getAddress();
            if (address == null) continue;
            final Data data = viewData.get(address);
            DataType type = currentDdt.getDataType();
            if (data != null && data.hasDataType(type) && (now - data.getTime(address, type) < VIEW_LIFE) || (!Man.isShowNew() && isNew.get(type) == false)) {
                SleepAndWake.runInMain(new Runnable() {
                    @Override
                    public void run() {
                        showData(currentDdt, data);
                    }
                }, "RelativeDataView.onWatchdogTimer");
                break;
            }
        }
        Timing.log(RelativeDataView.class.getSimpleName() + "_onWatchdogCheck_" + viewNr, Timing.Action.END);

    }

    @Override
    public void onWatchdogClose() {

    }

    public void checkVisibility(){
        List<DeviceDataType> list = Man.getViewCheckedDeviceDataTypes(viewNr);
        if (list == null || list.size() == 0) {
            currentDdt = null;
            setVisibility(INVISIBLE);
        }
    }

    public void clear() {
        deviceDataTypeSet.clear();
        sparkLines.clear();
        setVisibility(INVISIBLE);

    }

}
