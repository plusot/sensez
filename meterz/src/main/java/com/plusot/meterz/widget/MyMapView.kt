package com.plusot.meterz.widget

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.os.Build
import android.util.AttributeSet
import android.util.SparseArray
import android.view.MotionEvent
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.plusot.meterz.BuildConfig
import com.plusot.meterz.MeterzGlobals
import com.plusot.meterz.R
import com.plusot.meterz.activities.ActivityResult
import com.plusot.meterz.preferences.PreferenceKey
import com.plusot.meterz.preferences.SettingsActivity
import com.plusot.meterz.preferences.SettingsType
import com.plusot.meterz.store.GpxReader
import com.plusot.sensez.SensezLib
import com.plusot.sensez.device.*
import com.plusot.sensez.internal.GpsDevice
import com.plusot.sensez.internal.InternalDevice
import com.plusot.sensez.internal.InternalMan
import com.plusot.util.Globals
import com.plusot.util.data.Data
import com.plusot.util.data.DataType
import com.plusot.util.dialog.Alerts
import com.plusot.util.logging.LLog
import com.plusot.util.preferences.PreferenceHelper
import com.plusot.util.util.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.ScaleBarOverlay
import org.osmdroid.views.overlay.TilesOverlay
import java.lang.ref.WeakReference
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.pow

class MyMapView : MapView, Man.Listener, SensezLib.EventListener, OnSharedPreferenceChangeListener {
    private val gpxPointsLock = Any()
    private var currentPoint: GeoPoint? = null
    private var trace: PolylinePath? = null
    private var startOverlay: PolylinePath? = null
    private val gpxOverlays = mutableMapOf<Int, PolylinePath>()
    private var gpxDistanceOverlay: PolylinePath? = null
    private var prevZoom = 0.0
    private var initDone = false
    private var prev: Long = 0
    private var activeGpx = 0
    private var destinationMarker: Marker? = null
    private var inited = false

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    override fun onSensezLibEvent(event: SensezLib.SensezLibEvent) {
        if (event == SensezLib.SensezLibEvent.SENSEZ_CLOSE) return
        if (inited) return
        inited = true
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
        tileProvider = MapTileProviderBasic(context,  /*TileSourceType.CYCLEMAP.getSourceBase(Context context)));*/
                TileSourceType.fromInt(PreferenceKey.TILE_SOURCE.int).getSourceBase(context)
        )
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            LLog.i("Could not getDouble fine or coarse location permission")
            return
        }
        isClickable = true
        isLongClickable = true
        setUseDataConnection(true)
        setMultiTouchControls(true)
        zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
//        setBuiltInZoomControls(true)
        PreferenceHelper.getPrefs().registerOnSharedPreferenceChangeListener(this)
        isTilesScaledToDpi = PreferenceKey.SCALE_TO_DPI.isTrue
        val scaleBar = ScaleBarOverlay(this)
        scaleBar.setAlignBottom(true)
        scaleBar.setMaxLength(2.54f) // = 1.5 cm
        scaleBar.setEnableAdjustLength(true)
        val metrics = Globals.getMetrics()
        if (metrics != null) scaleBar.setTextSize(12.0f * metrics.xdpi / 100)
        overlays.add(scaleBar)
        val zoom = PreferenceKey.ZOOM.double
        controller.setZoom(zoom)
        var pt: GeoPoint? = GeoPoint(51.917168, 5.830994)
        val locationMgr = this.context.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        if (locationMgr != null) {
            val loc = locationMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (loc != null) pt = GeoPoint(loc.latitude, loc.longitude)
        }
        controller.setCenter(pt)
        trace = PolylinePath(PATH)
        trace?.color = PreferenceKey.TRACE_COLOR.int //ContextCompat.getColor(getContext(), R.color.pathColor));
        trace?.setWidth(PreferenceKey.TRACE_WIDTH.int.toFloat())
        trace?.setShowAlt(true)
        overlays.add(trace)
        trace?.setPoints(pathPoints)
        if (gpxPoints.size() == 0) {
            getGpx(PreferenceKey.GPX_PATH.string)
        } else synchronized(gpxPointsLock) {
            for (i in 0 until gpxPoints.size()) addGpx(i, gpxPoints.valueAt(i))
            setActiveGpx(activeGpx)
        }
        if (startOverlay == null) {
            startOverlay = PolylinePath(START_DISTANCE)
            startOverlay?.setCross(false)
            startOverlay?.setWidth(6f)
            startOverlay?.color = ContextCompat.getColor(context, R.color.toStartColor)
            startOverlay?.setDashed(floatArrayOf(30f, 20f))
            overlays.add(0, startOverlay)
        }
        maxZoomLevel = 20.0
        minZoomLevel = 4.0
        initDone = true
        Man.addListener(this)
        if (PreferenceKey.MAP_ALWAYS.isTrue) visibility = View.VISIBLE
        //        else
        runDistanceTo()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        isTilesScaledToDpi = PreferenceKey.SCALE_TO_DPI.isTrue
    }

    constructor(context: Context?) : super(
            context
    ) {
        if (!isInEditMode) {
            SensezLib.addEventListener(this)
        }
        lastInstanceRef = WeakReference(this)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
            context,
            attrs
    ) {
        if (!isInEditMode) {
            SensezLib.addEventListener(this)
            //initAfterPermissionsChecked();
        }
        lastInstanceRef = WeakReference(this)
    }

    override fun getMaxZoomLevel(): Double {
        val zoom = this.zoomLevelDouble
        if (zoom != prevZoom) {
//            LLog.i("getMaxZoomLevel zoom = " + zoom);
            val type = TileSourceType.fromInt(PreferenceKey.TILE_SOURCE.int)
            val appContext = Globals.getAppContext()
            if (appContext != null) ToastHelper.showToastShort(
                    appContext.getString(
                            R.string.zoom_level,
                            StringUtil.proper(type.name),
                            Format.format(zoom, 0)
                    )
            )
            if (zoom > 0 && initDone) PreferenceKey.ZOOM.set(zoom)
        }
        prevZoom = zoom
        return super.getMaxZoomLevel()
    }

    private var prevActive: Long = 0
    override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
        if (!data.hasDataType(DataType.LOCATION)) return
        val xy = data.getDoubles(device.address, DataType.LOCATION)
        val alt = data.getDouble(device.address, DataType.ALTITUDE)
        if (xy == null || xy.size < 2) return
        SleepAndWake.runInMain({
            if (visibility != View.VISIBLE) visibility = View.VISIBLE
            val point = GeoPoint(xy[0], xy[1])
            if (!java.lang.Double.isNaN(alt)) point.altitude = alt
            var active = Man.getLong(InternalDevice.InternalType.CLOCK.address, DataType.TIME_ACTIVE, -1)
            if (active == -1L) active = Man.getLong(InternalDevice.InternalType.CLOCK.address, DataType.TIME_ACTIVITY, -1)

            //if ((active = Man.getLong(InternalDevice.InternalType.CLOCK.getAddress(), DataType.TIME_ACTIVITY)) < prevActive)
            if (active in 1 until prevActive) {
                trace?.clearPath()
            }
            prevActive = active
            trace?.addPoint(point.also { currentPoint = it })
            val now = System.currentTimeMillis()
            if (now - prev > GpsDevice.gpsInterval) {
                pathPoints.add(point)
                if (pathPoints.size >= 2) {
                    val points: MutableList<GeoPoint?> = ArrayList()
                    points.add(currentPoint)
                    val startPoint = pathPoints[0]
                    points.add(startPoint)
                    startOverlay?.setPoints(points)
                    currentPoint?.distanceToAsDouble(startPoint)?.let { GpsDevice.inject(it, DataType.DISTANCE_START) }
                }
                showDistanceTo(gpxMinDistanceIndex)
                if (!isTouched()) controller.setCenter(point)
                prev = now
            }
        }, "MyMapView.onDeviceData")
    }

    override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {
        if (device.deviceType == DeviceType.INTERNAL_GPS && visibility != View.VISIBLE && InternalMan.hasLocation()) {
            LLog.i("After init setting map visible")
            visibility = View.VISIBLE
        }
    }

    override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo) {}
    override fun onScanning(scanType: ScanManager.ScanType, on: Boolean) {}

    private fun setActiveGpx(activeGpx: Int) {
        this.activeGpx = activeGpx
        gpxOverlays.keys.forEach{ key ->
            val path = gpxOverlays[key]
            if (path != null) {
                if (key == activeGpx) {
                    path.setWidth(PreferenceKey.GPX_WIDTH.int.toFloat())
                    path.setShowAlt(true)
                    path.setIsGpx(true)
                    path.setOnClickListener(polyLineOnClickListener)
                } else {
                    path.setWidth(PreferenceKey.GPX_WIDTH.int / 2f)
                    path.setShowAlt(false)
                    path.setIsGpx(false)
                    path.setOnClickListener(null)
                }
            }
        }
        if (PreferenceKey.GPX_FOLLOW.isTrue) showGpx() else invalidate()
    }

    private var polyLineOnClickListener = PolylinePath.OnClickListener { polyline: PolylinePath, eventPos: GeoPoint?, indices: IntArray, use: Boolean ->
        //polyline.showInfoWindow(eventPos);
        if (indices.size < 2) return@OnClickListener true
        val point = polyline.getPoint(indices[0])
        val me = polyline.getPoint(indices[1])
        if (point == null) return@OnClickListener true
        var geoPoint = eventPos
        gpxPoints[activeGpx]?.let {
            if (indices[0] < it.size) geoPoint = it[indices[0]]
        }
        if (geoPoint == null) return@OnClickListener true
        if (use) {
            PreferenceKey.POI_INDEX.set(indices[0])
            destinationMarker = PointMarker.createPointMarker(
                    this@MyMapView,
                    destinationMarker,
                    PointMarker.PointInfo(
                            1,
                            indices[0],
                            indices[1],
                            geoPoint,
                            polyline
                    )
            )
        }
        val stringer = Stringer("\n")
        if (me != null) {
            val dist = abs(point.distance - me.distance)
            stringer.append(context.getString(R.string.fromMe, Format.format(dist / 1000, 1)))
        }
        stringer.append(context.getString(R.string.fromStart, Format.format(point.distance / 1000, 1)))
        stringer.append(context.getString(R.string.fromEnd, Format.format((polyline.distance - point.distance) / 1000, 1)))
        var alt: Double
        if (!java.lang.Double.isNaN(point.altitude.also { alt = it })) {
            stringer.append(context.getString(R.string.gpxAltitude, Format.format(alt, 0)))
        }
        ToastHelper.showToastLong(stringer.toString())
        invalidate()
        true
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    fun addGpx(index: Int, points: List<GeoPoint>?) {
        if (points == null) return
        var path = gpxOverlays[index]
        if (path == null) {
            path = PolylinePath(GPX_BASE + index)
            path.color = PreferenceKey.GPX_COLOR.int //ContextCompat.getColor(getContext(), R.color.gpxColor));
            path.setWidth(PreferenceKey.GPX_WIDTH.int.toFloat())
            path.setShowAlt(true)
            path.setCross(false)
            overlays.add(0, path)
            gpxOverlays[index] = path
        }
        if (gpxDistanceOverlay == null) {
            gpxDistanceOverlay = PolylinePath(GPX_DISTANCE)
            gpxDistanceOverlay?.color = ContextCompat.getColor(context, R.color.toGpxColor)
            gpxDistanceOverlay?.setCross(false)
            gpxDistanceOverlay?.setWidth(6f)
            gpxDistanceOverlay?.setDashed(floatArrayOf(20f, 20f))
            overlays.add(0, gpxDistanceOverlay)
        }
        val totalDistance = path.setPoints(points)
        ToastHelper.showToastLong(context.getString(R.string.gpxDistance, Format.format(totalDistance / 1000, 1)))
        if (points.isNotEmpty()) controller.setCenter(points[0])
    }

    private fun getGpx(path: String?) {
        if (path == null) return
        //final String trace = SenseGlobals.getGpxPath() + "test.gpx";
        gpxOverlays.values.forEach {  overlays.remove(it) }
        //gpxDistances.clear();
        gpxOverlays.clear()
        synchronized(gpxPointsLock) { gpxPoints.clear() }
        if (reader != null) reader?.stop()
        reader = GpxReader(object : GpxReader.Listener {
            override fun onPoints(index: Int, list: List<GeoPoint>) {

                val listed = if (PreferenceKey.GPX_REVERSE.isTrue) list.asReversed() else list //Collections.reverse(list)
                LLog.i("Receiving list of " + listed.size + " points")
                if (gpxDistanceOverlay != null) gpxDistanceOverlay?.clearPath()
                if (listed.size > 1) {
                    synchronized(gpxPointsLock) { gpxPoints.put(index, listed) }
                    addGpx(index, listed)
                    //                    PreferenceKey.GPX_PATH.set(path);
                }
            }

            override fun onDone() {
                synchronized(gpxPointsLock) {
                    if (gpxPoints.size() > 1) {
                        val tracks = arrayOfNulls<String>(gpxPoints.size())
                        for (i in 0 until gpxPoints.size()) {
                            tracks[i] = context.getString(R.string.track, i + 1)
                        }
                        Alerts.showItemsDialog(
                                context,
                                R.string.choose_gpx_track,
                                tracks
                        ) { clickResult, _, which ->
                            when (clickResult) {
                                Alerts.ClickResult.YES -> setActiveGpx(which)
                                Alerts.ClickResult.NO -> {}
                                Alerts.ClickResult.CANCEL -> {}
                                Alerts.ClickResult.NEUTRAL -> {}
                                Alerts.ClickResult.DISMISS -> {}
                                null -> {}
                            }
                        }
                    } else {
                        setActiveGpx(0)
                    }
                }
            }
        }, path)
    }

    val gpx: Unit
        get() {
            val list = FileExplorer.getFileList(arrayOf(
                    Globals.getDataPath(),
                    Globals.getDownloadPath(),
                    MeterzGlobals.getGpxPath(),
                    Globals.getSdcardPath() + "/osmand/tracks/"
            ), arrayOf(".gpx"),
                    false
            )
                    ?: return
            val items = Arrays.copyOf(list.items, list.items.size + 1)
            items[items.size - 1] = context.getString(R.string.no_gpx)
            var selection = PreferenceKey.GPX_PATH.string
            if (selection == null) selection = context.getString(R.string.no_gpx)
            Alerts.showSingleChoiceDialog(context, R.string.gpxTitle, items, selection) { clickResult, whichString, which ->
                when (clickResult) {
                    Alerts.ClickResult.YES -> {
                        if (which == items.size - 1) {
                            if (reader != null) reader?.stop()
                            gpxOverlays.values.forEach { it.clearPath() }
                            if (gpxDistanceOverlay != null) gpxDistanceOverlay?.clearPath()
                            PreferenceKey.GPX_PATH.set(null)
                        } else {
                            var choice = 0
                            if (PreferenceKey.GPX_REVERSE.isTrue) choice = 1
                            val context = Globals.getAppContext()
                            PreferenceKey.GPX_PATH.set(list.lookup[whichString])
                            if (context != null) Alerts.showSingleChoiceDialog(
                                    getContext(),
                                    R.string.gpxDirectionTitle,
                                    arrayOf(context.getString(R.string.defaultDirection),
                                            context.getString(R.string.reverseDirection)
                                    ),
                                    choice,
                                    { _, _, which2 ->
                                        PreferenceKey.GPX_REVERSE.set(which2 == 1)
                                        getGpx(list.lookup[whichString])
                                    },
                                    0
                            )
                        }
                    }
                    Alerts.ClickResult.NO -> {}
                    Alerts.ClickResult.CANCEL -> {}
                    Alerts.ClickResult.NEUTRAL -> {}
                    Alerts.ClickResult.DISMISS -> {}
                    null -> {}
                }

            }
        }

    private fun showGpx() {
        val t = Thread(object : Runnable {
            var index = 0
            override fun run() {
                synchronized(gpxPointsLock) { if (gpxPoints[activeGpx] == null || gpxPoints[activeGpx]?.size == 0) return }
                gpxPoints[activeGpx]?.let { points ->
                    while (Globals.runMode.isRun && mayRun) {
                        try {
                            val zoom = zoomLevelDouble
                            val step = max(1, 2 * (8 - zoomLevelDouble.toInt()))
                            Thread.sleep((2.0.pow(max(zoom - 6.0, 0.0)) * 5).toLong())
                            if (!mayRun) break
                            val geoPoint = synchronized(gpxPointsLock) {
                                if (index < points.size) points[index] else null
                            } ?: break
                            index += step
                            SleepAndWake.runInMain({ controller.setCenter(geoPoint) }, MyMapView::class.java)
                        } catch (e: InterruptedException) {
                            LLog.i("Interrupted")
                        }
                    }
                }
            }
        })
        t.name = "MyMapView_ShowGpx"
        t.start()
    }

    private var distanceToLastShown: Long = 0
    private fun showDistanceTo(index: Int) {
        val now = System.currentTimeMillis()
        if (now - distanceToLastShown < 500) return
        if (index < 0) return
        val polyline = gpxOverlays[activeGpx] ?: return
        val pt = polyline.getPoint(index) ?: return
        val refPt: GeoPoint? = synchronized(gpxPointsLock) { gpxPoints[activeGpx]?.get(index) }
        val distanceToGpx = refPt?.distanceToAsDouble(currentPoint)
        if (distanceToGpx == null || java.lang.Double.isNaN(distanceToGpx)) return
        GpsDevice.inject(distanceToGpx, DataType.GPX_DISTANCE)
        distanceToLastShown = now
        val distance = polyline.distance - pt.distance
        GpsDevice.inject(distanceToGpx + distance, DataType.GPX_DISTANCE_TO_DO)
        val dist = polyline.distanceToPoi(index)
        if (!java.lang.Double.isNaN(dist)) GpsDevice.inject(distanceToGpx + dist, DataType.DISTANCE_POI)
        if (gpxDistanceOverlay != null) {
            val points: MutableList<GeoPoint?> = ArrayList()
            points.add(currentPoint)
            points.add(refPt)
            SleepAndWake.runInMain({ gpxDistanceOverlay?.setPoints(points) }, MyMapView::class.java)
        }
        PointMarker.updateMarker(context, destinationMarker, index)
    }

    private var mayRun = true
    private var distanceToThread: Thread? = null
    private var gpxMinDistanceIndex = -1
    private fun runDistanceTo() {
        distanceToThread = Thread(Runnable {
            var index = 0
            val sleep: Long = 5
            var step = 20
            while (Globals.runMode.isRun && mayRun) {
                try {
                    Thread.sleep(sleep)
                    if (!mayRun) break
                    var pt: GeoPoint
                    var show = false
                    synchronized(gpxPointsLock) {
                        gpxPoints[activeGpx]?.let { points ->
                            if (currentPoint != null && points.isNotEmpty()) {
                                if (index < 0 || index >= points.size) {
                                    index = 0
                                    step += 5
                                    step %= 30
                                    show = true
                                }
                                pt = points[index]
                                //if (pt != null) {
                                if (gpxMinDistanceIndex >= points.size || gpxMinDistanceIndex < 0) gpxMinDistanceIndex = 0
                                val distance = pt.distanceToAsDouble(currentPoint)
                                val refPt = points[gpxMinDistanceIndex]
                                val refDistance = refPt.distanceToAsDouble(currentPoint)
                                if (distance < refDistance) {
                                    gpxMinDistanceIndex = index
                                    show = true
                                }
                                //}
                                index += step + 1
                            }
                        }
                    }
                    if (show) showDistanceTo(gpxMinDistanceIndex)
                } catch (e: InterruptedException) {
                    LLog.i("Interrupted")
                }
            }
        })
        distanceToThread?.name = "MyMapView_DistanceTo"
        distanceToThread?.start()
    }

    private fun startSettings() {
        SleepAndWake.runInMain {
            val intent = Intent(context, SettingsActivity::class.java).putExtra(
                SettingsActivity.SETTINGS_CHOICE,
                SettingsType.MapSettings.ordinal
            )
            context.startActivity(intent)
        }
    }

    private fun showApiAlert() {
        Alerts.showYesNoDialog(context, R.string.apiAlertTitle, R.string.apiAlertMsg, R.string.ok, R.string.settings, { clickResult: Alerts.ClickResult? ->
            when (clickResult) {
                Alerts.ClickResult.NO -> startSettings()
                Alerts.ClickResult.YES -> {}
                Alerts.ClickResult.CANCEL -> {}
                Alerts.ClickResult.NEUTRAL -> {}
                Alerts.ClickResult.DISMISS -> {}
                null -> {}
            }
        }, R.mipmap.ic_map)
    }

    fun chooseMapType() {
        val tileSources = arrayOfNulls<String>(TileSourceType.values().size)
        var i = 0
        for (type in TileSourceType.values()) {
            tileSources[i++] = StringUtil.proper(type.name)
        }
        Alerts.showSingleChoiceDialog(
                this.context,
                R.string.ChooseMapProvider,
                tileSources,
                PreferenceKey.TILE_SOURCE.int,
                { clickResult, _, which ->
                    if (clickResult == Alerts.ClickResult.YES) {
                        if (zoomLevelDouble > 14) {
                            controller.setZoom(14.0)
                            PreferenceKey.ZOOM.set(14.0)
                        }
                        val type = TileSourceType.fromInt(which)
                        if (type.needsApiKeys()) showApiAlert()
                        if (type.isOverlay) {
                            val appContext = Globals.getAppContext()
                            if (appContext != null) {
                                val tileProvider = MapTileProviderBasic(appContext)
                                tileProvider.tileSource = type.getSourceBase(context)
                                val tilesOverlay = TilesOverlay(tileProvider, context)
                                tilesOverlay.loadingBackgroundColor = Color.TRANSPARENT
                                overlays.add(tilesOverlay)
                                invalidate()
                            }
                        } else {
                            PreferenceKey.TILE_SOURCE.set(which)
                            tileProvider = MapTileProviderBasic(context, type.getSourceBase(context))
                        }
                    }
                },
                10000
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val action = event.action
        if (trace != null) {
            when (action) {
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_POINTER_UP ->                     //pathOverlay.isTouched(false);
                    trace?.isTouched(false)
                else -> {
                    //pathOverlay.isTouched(true);
                    trace?.isTouched(true)
                    touched = System.currentTimeMillis()
                }
            }
        }
        //LLog.i("Action = " + action);
        if (listener != null) {
            val l = listener?.get()
            l?.onTouch()
        }
        return super.onTouchEvent(event)
    }

    fun _clear() {
        pathPoints.clear()
        if (trace != null) trace?.clearPath()
    }

    interface Listener {
        fun onTouch()
    }

    override fun onDetach() {
        PreferenceHelper.getPrefs().unregisterOnSharedPreferenceChangeListener(this)
        mayRun = false
        Man.removeListener(this)
        if (distanceToThread != null) distanceToThread?.interrupt()
        super.onDetach()
    }

    companion object {
        private var lastInstanceRef: WeakReference<MyMapView>? = null
        private const val PATH = 1
        private const val GPX_DISTANCE = 3
        private const val START_DISTANCE = 4
        private const val GPX_BASE = 2
        private val pathPoints: MutableList<GeoPoint> = mutableListOf()
        private val gpxPoints: SparseArray<List<GeoPoint>?> = SparseArray()
        private var touched: Long = 0
        private var reader: GpxReader? = null
        val lastInstance: MyMapView?
            get() = lastInstanceRef?.get()

        fun clear() {
            lastInstance?._clear()
        }

        private var listener: WeakReference<Listener>? = null

        //    public static void setOnTouchListener(Listener listener) {
        //        MyMapView.listener = new WeakReference<>(listener);
        //    }
        fun onResume(listener: Listener) {
            Companion.listener = WeakReference(listener)
            val instance = lastInstance
            if (instance?.trace != null) {
                instance.trace?.color = PreferenceKey.TRACE_COLOR.int
                instance.trace?.setWidth(PreferenceKey.TRACE_WIDTH.int.toFloat())
                instance.gpxOverlays.values.forEach {  path ->
                    path.color = PreferenceKey.GPX_COLOR.int
                    path.setWidth(PreferenceKey.GPX_WIDTH.int.toFloat())
                }
            }
        }

        @JvmStatic
        fun isTouched(): Boolean {
            return 5000 > System.currentTimeMillis() - touched
        }
    }
}