package com.plusot.meterz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

public class ClickableOverlay extends Overlay {

    private Listener mListener;

    public ClickableOverlay(Context context, Listener listener) {
        super(context);
        mListener = listener;
    }

    public void setOnLongClickListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void draw(Canvas c, MapView osmv, boolean shadow) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event, MapView mapView) {
        mListener.onTouch(mapView, event);
        return super.onTouchEvent(event, mapView);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e, MapView mapView) {
        mListener.onClick(mapView, e);
        return super.onSingleTapConfirmed(e, mapView);
    }

    public interface Listener {
        boolean onTouch(MapView mapView, MotionEvent event);

        boolean onClick(MapView mapView, MotionEvent event);
    }

}
