package com.plusot.meterz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

public class LongClickableOverlay extends Overlay {

    private OnLongClickListener mListener;

    public LongClickableOverlay(Context context, OnLongClickListener listener) {
        super(context);
        mListener = listener;
    }

    public void setOnLongClickListener(OnLongClickListener listener) {
        mListener = listener;
    }

    @Override
    public void draw(Canvas c, MapView osmv, boolean shadow) {

    }


    @Override
    public boolean onLongPress(MotionEvent e, MapView mapView) {
        if (mListener != null) {
            final IGeoPoint point = mapView.getProjection().fromPixels((int) e.getX(), (int) e.getY());
            return mListener.onLongClick(mapView, point);
        }
        return super.onLongPress(e, mapView);
    }

    public interface OnLongClickListener {
        boolean onLongClick(MapView mapView, IGeoPoint point);
    }

}
