package com.plusot.meterz.widget

import android.content.Context
import kotlin.jvm.JvmOverloads
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.preference.PreferenceViewHolder
import android.widget.TextView
import com.plusot.meterz.R
import android.widget.SeekBar
import android.content.res.TypedArray
import android.util.AttributeSet
import androidx.preference.Preference
import java.lang.UnsupportedOperationException

class SeekBarPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : Preference(context, attrs, defStyleAttr), OnSeekBarChangeListener {
    private var seekBarProgress = 1
    private var seekBarMaxValue = 0

    fun setValue(pValue: Int) {
        val value = if (pValue >= 1 && pValue <= seekBarMaxValue) pValue else 1
        if (shouldPersist()) persistInt(value)
        if (pValue != seekBarProgress) {
            seekBarProgress = value
            notifyChanged()
        }
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        holder.isDividerAllowedAbove = false
        holder.isDividerAllowedBelow = false
        val seekBarTitle = holder.findViewById(R.id.seekBarTitle) as TextView
        seekBarTitle.text = this.title
        val seekBarMin = holder.findViewById(R.id.seekBarMin) as TextView
        seekBarMin.text = "1"
        val seekBarMax = holder.findViewById(R.id.seekBarMax) as TextView
        seekBarMax.text = seekBarMaxValue.toString()
        val preferenceSeekBar = holder.findViewById(R.id.preferenceSeekBar) as SeekBar
        preferenceSeekBar.max = seekBarMaxValue - 1
        preferenceSeekBar.progress = seekBarProgress - 1
        preferenceSeekBar.setOnSeekBarChangeListener(this)
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        if (!fromUser) return
        setValue(progress + 1)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        // empty
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        // empty
    }

    override fun onSetInitialValue(defaultValue: Any?) {
        if (defaultValue != null && defaultValue is Int) setValue((defaultValue as Int?)!!)
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any {
        return a.getInt(index, 1)
    }

    init {
        val xmlAttrs = context.obtainStyledAttributes(attrs, R.styleable.SeekBarPreference, defStyleAttr, defStyleRes)
        try {
            seekBarMaxValue = xmlAttrs.getInteger(R.styleable.SeekBarPreference_maxValue, 2)
            if (seekBarMaxValue < 2) seekBarMaxValue = 2
        } catch (e: UnsupportedOperationException) {
            seekBarMaxValue = 2
        } finally {
            xmlAttrs.recycle()
        }
        layoutResource = R.layout.preference_seekbar
    }
}