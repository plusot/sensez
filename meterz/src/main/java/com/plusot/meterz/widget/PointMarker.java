package com.plusot.meterz.widget;

import android.content.Context;
import android.os.Build;
import android.util.SparseArray;

import com.plusot.meterz.R;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.Stringer;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

class PointMarker {

    static class PointInfo {
        final int id;
        final int indexPoint;
        int indexMe;
        final GeoPoint geoPoint;
        final PolylinePath polylinePath;

        PointInfo(int id, int indexPoint, int indexMe, GeoPoint geoPoint, PolylinePath polylinePath) {
            this.id = id;
            this.indexPoint = indexPoint;
            this.indexMe = indexMe;
            this.geoPoint = geoPoint;
            this.polylinePath = polylinePath;
        }

        public int getId() {
            return id;
        }

        GeoPoint getGeoPoint() {
            return geoPoint;
        }

        String getDescription(Context context) {
            return context.getString(R.string.destination);
        }

        String getInfoString(Context context) {
            PolylinePath.PathPoint point = polylinePath.getPoint(indexPoint);
            PolylinePath.PathPoint me = polylinePath.getPoint(indexMe);
            if (point == null) return "";
            Stringer stringer = new Stringer("<br>");
            if (me != null) {
                double dist = Math.abs(point.distance - me.distance);
                stringer.append(context.getString(R.string.fromMe, Format.format(dist / 1000, 1)));
            }
            stringer.append(context.getString(R.string.fromStart, Format.format(point.distance / 1000, 1)));
            stringer.append(context.getString(R.string.fromEnd, Format.format((polylinePath.getDistance() - point.distance) / 1000, 1)));

            double alt;
            if (!Double.isNaN(alt = point.altitude)) {
                stringer.append(context.getString(R.string.gpxAltitude, Format.format(alt, 0)));
            }

            return stringer.toString();
        }
    }

    private static class PointInfoWindow extends MarkerInfoWindow {
        //PointInfo pointInfo;

        PointInfoWindow(MapView mapView) {
            super(R.layout.marker_bubble_point, mapView);
            /*View btn = (mView.findViewById(R.id.bubble_moreinfo));
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (bicycleInfo != null && markerClickListener != null) {
                        markerClickListener.onGetBicycleMenu(bicycleInfo);
                    } else {
                        ToastHelper.showToastLong(R.string.clicked);
                    }
                }
            });*/
        }

        @Override
        public void onOpen(Object item) {
            super.onOpen(item);
            //mView.findViewById(R.id.bubble_button).setVisibility(View.VISIBLE);
            //Marker marker = (Marker) item;
            //pointInfo = (PointInfo) marker.getRelatedObject();
        }
    }


    private static Marker buildPointMarker(MapView mapView, PointInfo info) {
        Marker marker = new Marker(mapView);
        marker.setPosition(info.getGeoPoint());

        marker.setAnchor(0.1f, 1.0f);
        marker.setTitle(info.getDescription(mapView.getContext()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            marker.setImage(mapView.getResources().getDrawable(R.mipmap.ic_finish, null));
            marker.setIcon(mapView.getResources().getDrawable(R.mipmap.ic_finish, null));
        } else {
            //noinspection deprecation
            marker.setImage(mapView.getResources().getDrawable(R.mipmap.ic_finish));
            //noinspection deprecation
            marker.setIcon(mapView.getResources().getDrawable(R.mipmap.ic_finish));
        }
        marker.setSnippet(info.getInfoString(mapView.getContext()));
        marker.setRelatedObject(info);
        marker.setInfoWindow(new PointInfoWindow(mapView));
        mapView.getOverlays().add(marker);
        return marker;
    }

    static Marker createPointMarker(final MapView mapView, Marker marker, final PointInfo info) {
        if (marker == null) {
            return buildPointMarker(mapView, info);
        } else {
            marker.setPosition(info.getGeoPoint());
            marker.setSnippet(info.getInfoString(mapView.getContext()));
            return marker;
        }
    }

    static void updateMarker(Context context, final Marker marker, int indexMe) {
        if (marker == null) return;
        PointInfo pointInfo = (PointInfo) marker.getRelatedObject();
        pointInfo.indexMe = indexMe;
        marker.setSnippet(pointInfo.getInfoString(context));
        SleepAndWake.runInMain(new Runnable() {
            @Override
            public void run() {
                if (marker.isInfoWindowShown()) {
                    marker.closeInfoWindow();
                    marker.showInfoWindow();
                }
            }
        }, "PointMarker.updateMarker");

    }
}
