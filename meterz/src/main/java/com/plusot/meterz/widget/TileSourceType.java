package com.plusot.meterz.widget;

import android.content.Context;

import com.plusot.meterz.preferences.PreferenceKey;

import org.osmdroid.tileprovider.tilesource.HEREWeGoTileSource;
import org.osmdroid.tileprovider.tilesource.MapBoxTileSource;
import org.osmdroid.tileprovider.tilesource.MapQuestTileSource;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;

/**
 * Package: com.plusot.meterz.widget
 * Project: $sensez
 * <p>
 * Created by Peter Bruinink on 06-04-18.
 * Copyright © 2018 Brons Ten Kate. All rights reserved.
 */
public enum TileSourceType {

    //        CLOUDMADE {
//            @Override
//            public OnlineTileSourceBase getSourceBase(Context context) {
//                return new XYTileSource("CloudMadeStandardTiles",
//                        0, 18, 256, ".png", new String[]{
//                        "http://a.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s",
//                        "http://b.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s",
//                        "http://c.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s"
//                });
//            }
//        },
    CYCLEMAP { //Apikey required
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.CYCLE);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },

    HERE_TERRAIN {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            HEREWeGoTileSource src =  new HEREWeGoTileSource(context);
            src.setHereWeGoMapid("terrain.day");
            src.setAppCode("usdoMucHYNvX3aFephRHqQ"); //PreferenceKey.HEREWEGO_APPCODE.getString("****"));
            src.setAppId("jAB3zjVQYQ2T5CRaO9yn"); //PreferenceKey.HEREWEGO_APPID.getString("****"));
            return src;
        }
    },
    HERE_HYBRID {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            HEREWeGoTileSource src =  new HEREWeGoTileSource(context);
            src.setHereWeGoMapid("hybrid.day");
            src.setAppCode("usdoMucHYNvX3aFephRHqQ"); //PreferenceKey.HEREWEGO_APPCODE.getString("****"));
            src.setAppId("jAB3zjVQYQ2T5CRaO9yn"); //PreferenceKey.HEREWEGO_APPID.getString("****"));
//            src.setAppCode(PreferenceKey.HEREWEGO_APPCODE.getString("****"));
//            src.setAppId(PreferenceKey.HEREWEGO_APPID.getString("****"));
            return src;
        }
    },
//    HIKEBIKEMAP {
//        @Override public OnlineTileSourceBase getSourceBase(Context context) {
//            return new XYTileSource("OpenStreetMap Hikebikemap.de",
//                    0, 18, 256, ".png", new String[]{
//                    "http://a.tiles.wmflabs.org/hikebike/",
//                    "http://b.tiles.wmflabs.org/hikebike/",
//                    "http://c.tiles.wmflabs.org/hikebike/"
//            });
//        }
//
//        @Override public boolean needsApiKeys() {
//            return false;
//        }
//    },
    OUTDOORS { //API key required
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.OUTDOORS);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    LANDSCAPE { //API key required
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.LANDSCAPE);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    //        Meterz’s Keys
//        Consumer Key	IXiRddOvHBaoxv1f0NGrwxIaSbZcwgEe
//        Consumer Secret	wEK254wAAo4uwHH5
//        Key Issued	Sun, 10/15/2017 - 15:21
//        Key Expires	Never

    //        <meta-data android:name="THUNDERFOREST_MAPID" android:value="080a7c783ddc496e8d225f65eeee8c22" />
//        <meta-data android:name="MAPQUEST_ACCESS_TOKEN" android:value="pk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO9801rmw" />
//        <meta-data android:name="MAPBOX_ACCESS_TOKEN" android:value="pk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO9801rmw" />
//        <!--Mapbox-->
//        <!--mapbox.mapbox-terrain-v2-->
//        <!--mapbox.mapbox-streets-v7-->
//        <!--mapbox.mapbox-traffic-v1-->
//        <!--mapbox.satellite-->
//        <!--Meterz: sk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkYThkczBpZGQycHBseGhtOGo5bTEifQ.AKmY-UhV8KIQaz_7eBhi5g-->
//        <!--Public: pk.eyJ1IjoiYnJ1aW5pbmtwIiwiYSI6ImNqOHRkOG5tYTBsYzQycXF1NTcxbzF4bGIifQ.SIBofcIKp64KoaO9801rmw-->
    MAPBOX_ROADS {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            MapBoxTileSource src =  new MapBoxTileSource(context);
            src.setMapboxMapid("mapbox.mapbox-streets-v8");
            src.setAccessToken(PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****"));
            return src;
        }
    },
    MAPBOX_SATELLITE {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            MapBoxTileSource src =  new MapBoxTileSource(context);
            src.setMapboxMapid("mapbox.satellite");
            src.setAccessToken(PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****"));
            return src;
        }
    },
    MAPBOX_TERRAIN {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            MapBoxTileSource src =  new MapBoxTileSource(context);
            src.setMapboxMapid("mapbox.mapbox-terrain-v2");
            src.setAccessToken(PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****"));
            return src;
        }
    },
    MAPBOX_TRAFIC {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            MapBoxTileSource src =  new MapBoxTileSource(context);
            src.setMapboxMapid("mapbox.mapbox-traffic-v1");
            src.setAccessToken(PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****"));
            return src;
        }
    },
    MAPNIK {
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            return new XYTileSource("Mapnik",
                    0, 19, 256, ".png", new String[]{
                    "http://a.tile.openstreetmap.org/",
                    "http://b.tile.openstreetmap.org/",
                    "http://c.tile.openstreetmap.org/"
            });
        }

        @Override public boolean needsApiKeys() {
            return false;
        }
    },
    //        Meterz’s Keys
//        Consumer Key	IXiRddOvHBaoxv1f0NGrwxIaSbZcwgEe
//        Consumer Secret	wEK254wAAo4uwHH5
//        Key Issued	Sun, 10/15/2017 - 15:21
//        Key Expires	Never
//    MAPQUEST {
//        @Override public OnlineTileSourceBase getSourceBase(Context context) {
//            MapQuestTileSource src = new MapQuestTileSource(
//                    "mapbox.mapquest",
//                    PreferenceKey.MAPBOX_ACCESS_TOKEN.getString("****")
//            );
//            return src;
//        }
//    },

    MOBILE_ATLAS { //API key required
        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.MOBILE_ATLAS);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    NEIGHBORHOOD { //API key required

        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.NEIGHBOURHOOD);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    PIONEER { //API key required

        @Override public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.PIONEER);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    //    PUBLIC_TRANSPORT_OVERLAY {
//        @Override public OnlineTileSourceBase getSourceBase(Context context) {
//            return new XYTileSource("OSMPublicTransport",
//                    0, 17, 256, ".png", new String[]{
//                    "http://openptmap.org/tiles/"
//            });
//        }
//
//        @Override public boolean isOverlay() {
//            return true;
//        }
//
//        @Override public boolean needsApiKeys() {
//            return false;
//        }
//    },
    SPINAL { //API key required
        @Override
        public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.SPINAL_MAP);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    TRANSPORT { //API key required
        @Override
        public OnlineTileSourceBase getSourceBase(Context context) {
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.TRANSPORT);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    TRANSPORT_DARK { //API key required
        @Override
        public OnlineTileSourceBase getSourceBase(Context context) {
            //return new ThunderForestTileSource(context, ThunderForestTileSource.TRANSPORT_DARK);
            ThunderForestTileSource src = new ThunderForestTileSource(context, ThunderForestTileSource.TRANSPORT_DARK);
            src.setMapId(PreferenceKey.THUNDERFOREST_MAPIP.getString("****"));
            return src;
        }
    },
    UMAPS {
        @Override
        public OnlineTileSourceBase getSourceBase(Context context) {
            return new XYTileSource("4uMaps",
                    0, 17, 256, ".png", new String[]{
                    "http://www.4umaps.eu/"
            });
        }

        @Override
        public boolean needsApiKeys() {
            return false;
        }
    },
    ;

    public OnlineTileSourceBase getSourceBase(Context context) {
        return null;
    }

    public static TileSourceType fromInt(int index) {
        return TileSourceType.values()[index % TileSourceType.values().length];
    }

    public boolean isOverlay() {
        return false;
    }
    public boolean needsApiKeys() {
        return true;
    }

}