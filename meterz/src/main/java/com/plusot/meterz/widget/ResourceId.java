package com.plusot.meterz.widget;

import com.plusot.meterz.R;

/**
 * Created by peet on 6/10/16.
 */

public class ResourceId {
    private static int[] ids;
    private static int current = 0;

    public static int getNewId() {
        if (current >= ids.length) return -1;
        return ids[current++];
    }

    static {
        ids[0] = R.id.id0;
        ids[1] = R.id.id1;
        ids[2] = R.id.id2;
        ids[3] = R.id.id3;
        ids[4] = R.id.id4;
        ids[5] = R.id.id5;
        ids[6] = R.id.id6;
        ids[7] = R.id.id7;
        ids[8] = R.id.id8;
        ids[9] = R.id.id9;
        ids[10] = R.id.id10;
        ids[11] = R.id.id11;
        ids[12] = R.id.id12;
        ids[13] = R.id.id13;
        ids[14] = R.id.id14;
        ids[15] = R.id.id15;
        ids[16] = R.id.id16;
        ids[17] = R.id.id17;
        ids[18] = R.id.id18;
        ids[19] = R.id.id19;
        ids[20] = R.id.id20;
        ids[21] = R.id.id21;
        ids[22] = R.id.id22;
        ids[23] = R.id.id23;
        ids[24] = R.id.id24;
        ids[25] = R.id.id25;
        ids[26] = R.id.id26;
        ids[27] = R.id.id27;
        ids[28] = R.id.id28;
        ids[29] = R.id.id29;
        ids[30] = R.id.id30;
        ids[31] = R.id.id31;
        ids[32] = R.id.id32;
        ids[33] = R.id.id33;
        ids[34] = R.id.id34;
        ids[35] = R.id.id35;
        ids[36] = R.id.id36;
        ids[37] = R.id.id37;
        ids[38] = R.id.id38;
        ids[39] = R.id.id39;
        ids[40] = R.id.id40;
        ids[41] = R.id.id41;
        ids[42] = R.id.id42;
        ids[43] = R.id.id43;
        ids[44] = R.id.id44;
        ids[45] = R.id.id45;
        ids[46] = R.id.id46;
        ids[47] = R.id.id47;
        ids[48] = R.id.id48;
        ids[49] = R.id.id49;
        ids[50] = R.id.id50;

    }
}
