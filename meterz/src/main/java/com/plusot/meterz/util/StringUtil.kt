package com.plusot.meterz.util

object StringUtil {
    @JvmStatic
    fun toString(coll: Collection<*>?, delim: String): String {
        if (coll == null) return ""
        val sb = StringBuilder()
        for (obj in coll) {
            if (obj is Collection<*>) {
                sb.append(toString(obj, delim))
            } else {
                if (sb.isNotEmpty()) sb.append(delim)
                sb.append("" + obj.toString())
            }
        }
        return sb.toString()
    }
}