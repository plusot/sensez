package com.plusot.playground

import kotlinx.coroutines.*

/**
 * Package: com.plusot.playground
 * Project: sensez
 *
 * Created by Peter Bruinink on 18-06-18.
 * Copyright © 2018 Plusot. All rights reserved.
 */

fun background() {
    GlobalScope.launch { // launch new coroutine in background and continue
        delay(1000L)
        println("background World!")
    }
    println("Hello,") // main thread continues here immediately
    runBlocking {     // but this expression blocks the main thread
        delay(2000L)  // ... while we delay for 2 seconds to keep JVM alive
    }
}

fun backgroundBlocking() = runBlocking {
    launch { // launch new coroutine in background and continue
        delay(1000L)
        println("background World!")
    }
    println("Hello,") // main thread continues here immediately
    delay(2000L)  // ... while we delay for 2 seconds to keep JVM alive
}

fun deferred() = runBlocking {
    val deferredResult: Deferred<String> = GlobalScope.async {
        delay(1000L)
        "deferred World!"
    }

    println("Hello, ${deferredResult.await()}")
}

suspend fun blockingFunc(timeToWait: Long) {
    delay(timeToWait)
}

fun scope() = runBlocking { // this: CoroutineScope
    launch {
        blockingFunc(200L)
        println("Task from runBlocking")
    }

    coroutineScope { // Creates a new coroutine scope
        launch {
            delay(500L)
            println("Task from nested launch")
        }

        delay(100L)
        println("Task from coroutine scope") // This line will be printed before nested launch
    }

    println("Coroutine scope is over") // This line is not printed until nested launch completes
}

fun main(args: Array<String>) {
    println("Start of program")
    println()
    val tests = if (args.isNotEmpty()) args else arrayOf("scope")
    tests.forEach { arg ->
//        println("Argument: $arg")
        when (arg) {
            "background" -> background()
            "backgroundB" -> backgroundBlocking()
            "convert" -> println("oops")
            "deferred" -> deferred()
            "scope" -> scope()
        }
    }
    println()
    println("End of program")
}

