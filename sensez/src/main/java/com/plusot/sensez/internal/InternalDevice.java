package com.plusot.sensez.internal;


import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceType;
import com.plusot.util.util.StringUtil;

/**
 * Created by peet on 23/1/17.
 */

public class InternalDevice extends Device {

    public enum InternalType {
        GPS,
        CLOCK,
        ACCELEROMETER,
        MAGNETIC_SENSOR,
        PROXIMITY_SENSOR,
        BATTERY_SENSOR,
        AIR_PRESSURE_SENSOR,
        ORIENTATION_SENSOR,
        MEMORY_SENSOR,
        STEP_SENSOR,
        TEMPERATURE_SENSOR
        ;

        DeviceType getDeviceType() {
            switch (this) {
                case STEP_SENSOR: return DeviceType.INTERNAL_STEP_SENSOR;
                case GPS: return DeviceType.INTERNAL_GPS;
                case CLOCK: return DeviceType.INTERNAL_CLOCK;
                case ACCELEROMETER: return DeviceType.INTERNAL_ACCELEROMETER;
                case MAGNETIC_SENSOR: return DeviceType.INTERNAL_MAGNETIC_SENSOR;
                case MEMORY_SENSOR: return DeviceType.INTERNAL_MEMORY_SENSOR;
                case PROXIMITY_SENSOR:return DeviceType.INTERNAL_PROXIMITY_SENSOR;
                case BATTERY_SENSOR: return DeviceType.INTERNAL_BATTERY_SENSOR;
                case AIR_PRESSURE_SENSOR: return DeviceType.INTERNAL_AIR_PRESSURE_SENSOR;
                case ORIENTATION_SENSOR: return DeviceType.INTERNAL_ORIENTATION_SENSOR;
                case TEMPERATURE_SENSOR: return DeviceType.INTERNAL_TEMPERATURE_SENSOR;
            }
            return null;
        }

        public String getAddress() {
            return StringUtil.proper(name());
        }
    }
    private final InternalType type;

    public InternalDevice(InternalType type, Device.Listener listener) {
        super(listener);
        this.type = type;
    }

    @Override
    public String getAddress() {
        return type.getAddress();
    }

}
