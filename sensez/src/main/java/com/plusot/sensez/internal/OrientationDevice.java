package com.plusot.sensez.internal;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;

import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;
import com.plusot.util.util.MathVector;

public class OrientationDevice extends InternalDevice implements SensorEventListener {
    private Sensor magneticField = null;
    private Sensor accelerometer = null;
    private SensorManager sensorManager = null;
    private MathVector accelerometerReading = new MathVector(3);
    private MathVector magnetometerReading = new MathVector(3);
    private final float[] rotationMatrix = new float[9];
    private final float[] inclinationMatrix = new float[9];
    //    private final float[] rotationMatrix2 = new float[9];
    private final float[] orientationAngles = new float[3];
    private int dirty = 0;



    public OrientationDevice(Listener listener) {
        super(InternalType.ORIENTATION_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            sensorManager.registerListener(this, magneticField, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        magneticField = null;
        accelerometer = null;

        LLog.i("Unregistering listener");
    }


//    private double calcCompass(double[] values, int axis1, int axis2) {
//        double compass;
//        if (Math.abs(values[axis1]) > Math.abs(values[axis2])) {
//            compass = Math.toDegrees(Math.acos(Math.abs(values[axis1])));
//        } else {
//            compass = 90.0 - Math.toDegrees(Math.acos(Math.abs(values[axis2])));
//        }
//        LLog.d("Compass raw " + Format.format(values[axis1], 2) + " " + Format.format(values[axis2], 2) + " " + Format.format(compass, 2));
//
//        if (values[axis1] < 0 && values[axis2] > 0) {
//            compass = 180 - compass;
//            LLog.d("Axis 2, Quadrant 2");
//        } else if (values[axis1] > 0 && values[axis2] < 0) {
//            compass = 360 - compass;
//            LLog.d("Axis 2, Quadrant 3");
//        } else if (values[axis1] < 0 && values[axis2] < 0) {
//            compass = 180 + compass;
//            LLog.d("Axis 2, Quadrant 4");
//        } else
//            LLog.d("Axis 2, Quadrant 1");
//        return compass;
//    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (magneticField == null || accelerometer == null) return;
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometerReading.add(event.values, 0.2);
            dirty ^= 1;
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//            LLog.i(Format.format(event.values[0], 2) + ", " +
//                    Format.format(event.values[1], 2) + ", " +
//                    Format.format(event.values[2], 2)
//            );
            magnetometerReading.add(event.values, 0.2);
//            LLog.i(Format.format(magnetometerReading.getValue(0), 2) + ", " +
//                    Format.format(magnetometerReading.getValue(1), 2) + ", " +
//                    Format.format(magnetometerReading.getValue(2), 2)
//            );
            dirty ^= 2;
        }


        if (dirty == 3) {
            dirty = 0;
//            MathVector acc = accelerometerReading.unitVector();
//            int axis = acc.largestAxis();
//            double compass = 0.0;
//
//            MathVector mag = magnetometerReading.unitVector();
//            double[] values = mag.getValues();
//            switch (axis) {
//                case 0:
//                    compass = calcCompass(values, 1, 2);
//                    break;
//                case 1:
//                    compass = calcCompass(values, 0, 2);
//                    break;
//                case 2:
//                    compass = calcCompass(values, 0, 1);
//                    break;
//            }

            if (SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix, accelerometerReading.getFloatValues(), magnetometerReading.getFloatValues())) {

//                SensorManager.remapCoordinateSystem(
//                        rotationMatrix,
//                        SensorManager.AXIS_X,
//                        SensorManager.AXIS_Z,
//                        rotationMatrix2
//                );
                SensorManager.getOrientation(rotationMatrix, orientationAngles);
                String address = getAddress();
                fireData(new Data()
                        .add(address, DataType.COMPASS, orientationAngles[0] * 180.0 / Math.PI)
                        .add(address, DataType.PITCH, orientationAngles[1] * 180.0 / Math.PI)
                        .add(address, DataType.ROLL, orientationAngles[2] * 180.0 / Math.PI)
                );
            }

        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;

        if ((sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) == null) return false;
        return sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION) != null;
    }
}