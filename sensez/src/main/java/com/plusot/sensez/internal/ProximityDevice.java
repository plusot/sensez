package com.plusot.sensez.internal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;

class ProximityDevice extends InternalDevice implements SensorEventListener {
    private SensorManager sensorManager;

    ProximityDevice(Listener listener) {
        super(InternalType.PROXIMITY_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        LLog.i("Un registering listener");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_PROXIMITY) return;

        //LLog.i("ProximityDevice.onSensorChanged: " + event.values[0]);
        fireData(new Data().add(getAddress(), DataType.PROXIMITY, 0.01 * event.values[0]));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        return
                appContext != null &&
                (sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) != null &&
                sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null;

    }
}