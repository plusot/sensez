package com.plusot.sensez.internal;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.plusot.sensez.SensezGlobals;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.util.Format;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.MathVector;
import com.plusot.util.util.Matrix;
import com.plusot.util.util.Numbers;

class MagneticDevice extends InternalDevice implements SensorEventListener {
    private static final boolean DEBUG = false;
    private Sensor sensor;
    private double slopeM = 0;
    private SensorManager sensorManager = null;

    MagneticDevice(Listener listener) {
        super(InternalType.MAGNETIC_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL); //.SENSOR_DELAY_GAME); //.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        sensor = null;

        LLog.i("Unregistering listener");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_MAGNETIC_FIELD) return;

        if (SensezGlobals.lastLocation != null) {
            long now = System.currentTimeMillis();
                /*
				 * Geomagnetic field:
				 * x = north
				 * y = east
				 * z = to earth center
				 *
				 * Measured sensor event
				 * x = to right of device
				 * y = to top of device
				 * z = point upward from screen
				 */

            GeomagneticField field = new GeomagneticField((float) SensezGlobals.lastLocation.getLatitude(), (float) SensezGlobals.lastLocation.getLongitude(), (float) SensezGlobals.lastLocation.getAltitude(), now);

            MathVector earth3D = new MathVector(field.getY(), field.getX(), -field.getZ()).unitVector();
            MathVector measured3D = new MathVector(event.values, 3).unitVector();

            Matrix m = new Matrix(3);
            double bearing = Device.getDoubleValue(DataType.BEARING);
            if (bearing == Double.NaN) return;
            m.setRotationXY(-bearing);
            MathVector v = m.product(measured3D);
            //Vector normal = v.crossProduct(earth3D);

            slopeM = Math.tan(Math.acos(v.dotProduct(earth3D)));
            //LLog.i("MagneticDevice.onSensorChanged: ---------------- Start -----------------");
            //LLog.i("MagneticDevice.onSensorChanged: earth3D = " + earth3D);
            //LLog.i("MagneticDevice.onSensorChanged: measured3D = " + measured3D);
            //LLog.i("MagneticDevice.onSensorChanged: rotated = " + v + " by " + -bearing * 180.0 / Math.PI);
            //LLog.i("MagneticDevice.onSensorChanged: normal = " + normal);

            double slope = Device.getDoubleValue(DataType.SLOPE);
            if (slope != Double.NaN && Numbers.diffRatio(slope / slopeM) > 0.1 && DEBUG)
                LLog.i("Magnetic vs gravity slope = " + Format.format(slopeM, 2) + " - " + Format.format(slope, 2));

            if (!Double.isNaN(slopeM)) fireData(new Data().add(getAddress(), DataType.SLOPE, slopeM));
        }
        fireData(new Data().add(getAddress(), DataType.MAGNETIC_FLUX_DENSITY_3D, event.values.clone()));
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        return  appContext != null &&
                (sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) != null &&
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null;

    }
}