package com.plusot.sensez.internal;

import com.plusot.sensez.R;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.sensez.preferences.ViewPosition;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InternalMan implements ScanManager {
    private static InternalMan instance = null;
    private Map<InternalDevice.InternalType, ScannedInternalDevice> scannedDevices = new HashMap<>();

//    public static synchronized InternalMan getInstance(Listener listener) {
//        if (instance == null) instance = new InternalMan();
//        instance.addListener(listener);
//        return instance;
//    }

    public static synchronized InternalMan getInstance() {
        if (instance == null) instance = new InternalMan();
        return instance;
    }


    @Override
    public List<ScannedDevice> getScannedDevices() {
        List<ScannedDevice> list =  new ArrayList<>();
        list.addAll(scannedDevices.values());
        return list;
    }

    private class ScannedInternalDevice extends ScannedDevice implements Device.Listener {
        private Device internalDevice = null;
        private final InternalDevice.InternalType type;

        ScannedInternalDevice(InternalDevice.InternalType type) {
            super(DeviceType.fromInternalType(type));
            this.type = type;
            if (isSelected()) launchConnection();
        }


        public void setSelected(boolean selected) {
            super.setSelected(selected);
            if (selected) {
                launchConnection();
            } else {
                if (internalDevice != null) internalDevice.close();
                internalDevice = null;
            }
        }

        private void launchConnection() {
            switch (type) {
                case GPS:
                    internalDevice = new GpsDevice(this);
                    break;
                case ACCELEROMETER:
                    internalDevice = new AccelerometerDevice(this);
                    break;
                case MAGNETIC_SENSOR:
                    internalDevice = new MagneticDevice(this);
                    break;
                case PROXIMITY_SENSOR:
                    internalDevice = new ProximityDevice(this);
                    break;
                case BATTERY_SENSOR:
                    internalDevice = new BatteryDevice(this);
                    break;
                case AIR_PRESSURE_SENSOR:
                    internalDevice = new AirPressureDevice(this);
                    break;
                case ORIENTATION_SENSOR:
                    internalDevice = new OrientationDevice(this);
                    break;
                case MEMORY_SENSOR:
                    internalDevice = new MemoryDevice(this);
                    break;
                case CLOCK:
                    internalDevice = new ClockDevice(this);
                    break;
                case STEP_SENSOR:
                    internalDevice = new StepDevice(this);
                    break;
                case TEMPERATURE_SENSOR:
                    internalDevice = new TemperatureDevice(this);
                    break;
            }
        }

        @Override
        public String getName() {
            return StringUtil.proper(type.name());
        }

        @Override
        public String getAddress() {
            return StringUtil.proper(type.name());
        }


        @Override
        public int getIconId() {
            switch(type) {

                case GPS:
                    return R.mipmap.ic_gps;
                case CLOCK:
                    return R.drawable.clock;
                case ACCELEROMETER:
                    return R.mipmap.ic_accelerometer;
                case MAGNETIC_SENSOR:
                    return R.mipmap.ic_magnet;
                case PROXIMITY_SENSOR:
                    return R.mipmap.ic_sensor;
                case BATTERY_SENSOR:
                    return R.mipmap.ic_battery;
                case AIR_PRESSURE_SENSOR:
                    return R.mipmap.ic_sensor;
                case ORIENTATION_SENSOR:
                    return R.mipmap.ic_sensor;
                case MEMORY_SENSOR:
                    return R.mipmap.ic_memory;
                case STEP_SENSOR:
                    return R.mipmap.ic_walking;
                case TEMPERATURE_SENSOR:
                    return  R.mipmap.ic_sensor;
            }
            return R.mipmap.ic_phone;
        }

        @Override
        public void onDeviceState(Device device, Device.StateInfo state) {
            Data data = new Data().add(getAddress(), DataType.STATE, state.toString());
            this.deviceData.merge(data);
            Man.fireData(this, data, true);
        }

        @Override
        public void onDeviceData(Device device, Data data) {
//            LLog.i(deviceData.toString());
            this.deviceData.merge(data);
            Man.fireData(this, data, true);
        }

        @Override
        public void onCommandDone(Device device, String commandId, byte[] data, boolean success, boolean closeOnDone) {
            if (closeOnDone) close();
        }

        @Override
        public void close() {
            if (internalDevice != null) {
                LLog.i("Closing: " + getName());
                internalDevice.close();
            }
            internalDevice = null;
        }

        @Override
        public String getManufacturer() {
            return android.os.Build.MANUFACTURER;
        }
    }

    private InternalMan() {
        ScannedInternalDevice sd;
        if (Man.debug) LLog.i("Starting InternalMan");

        if (GpsDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.GPS, sd = new ScannedInternalDevice(InternalDevice.InternalType.GPS));
            Man.fireScanned(sd, true);
        }

        scannedDevices.put(InternalDevice.InternalType.CLOCK, sd = new ScannedInternalDevice(InternalDevice.InternalType.CLOCK));
        Man.fireScanned(sd, true);

        if (AccelerometerDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.ACCELEROMETER, sd = new ScannedInternalDevice(InternalDevice.InternalType.ACCELEROMETER));
            Man.fireScanned(sd, true);
        }

        scannedDevices.put(InternalDevice.InternalType.BATTERY_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.BATTERY_SENSOR));
        Man.fireScanned(sd, true);

        if (AirPressureDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.AIR_PRESSURE_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.AIR_PRESSURE_SENSOR));
            Man.fireScanned(sd, true);
        }

        if (MagneticDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.MAGNETIC_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.MAGNETIC_SENSOR));
            Man.fireScanned(sd, true);
        }

        scannedDevices.put(InternalDevice.InternalType.MEMORY_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.MEMORY_SENSOR));
        Man.fireScanned(sd, true);


        if (OrientationDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.ORIENTATION_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.ORIENTATION_SENSOR));
            Man.fireScanned(sd, true);
        }

        if (ProximityDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.PROXIMITY_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.PROXIMITY_SENSOR));
            Man.fireScanned(sd, true);
        }

        if (StepDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.STEP_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.STEP_SENSOR));
            
            Man.fireScanned(sd, true);
        }

        if (TemperatureDevice.available()) {
            scannedDevices.put(InternalDevice.InternalType.TEMPERATURE_SENSOR, sd = new ScannedInternalDevice(InternalDevice.InternalType.TEMPERATURE_SENSOR ));
            Man.fireScanned(sd, true);
        }

        if (PreferenceKey.FIRST_USE.isTrue()) {
            if (PreferenceKey.USE_DEFAULT_FIELDS.isTrue()) {
                if ((sd = scannedDevices.get(InternalDevice.InternalType.GPS)) != null) {
                    sd.setSelected(true);
                    sd.setDataTypeView(DataType.LOCATION, ViewPosition.CENTER.getViewNr());
                    sd.setDataTypeView(DataType.GPX_DISTANCE_TO_DO, ViewPosition.NORTH.getViewNr());
                    sd.setDataTypeView(DataType.DISTANCE_START, ViewPosition.NORTH.getViewNr());
                    sd.setDataTypeView(DataType.DISTANCE, ViewPosition.SOUTH_EAST.getViewNr());
                    sd.setDataTypeView(DataType.DISTANCE_TOTAL, ViewPosition.SOUTH_EAST.getViewNr());
                    sd.setDataTypeView(DataType.SPEED, ViewPosition.NORTH_EAST.getViewNr());
                    sd.setDataTypeView(DataType.SPEED_AVG, ViewPosition.NORTH_EAST.getViewNr());
                    sd.setDataTypeView(DataType.ALTITUDE, ViewPosition.SOUTH_EAST.getViewNr());
                }
                if ((sd = scannedDevices.get(InternalDevice.InternalType.CLOCK)) != null) {
                    sd.setSelected(true);
                    sd.setDataTypeView(DataType.TIME_ACTIVITY, ViewPosition.NORTH_WEST.getViewNr());
                    sd.setDataTypeView(DataType.TIME_ACTIVE, ViewPosition.NORTH_WEST.getViewNr());
                    sd.setDataTypeView(DataType.TIME_CLOCK, ViewPosition.NORTH_WEST.getViewNr());
                }
                if ((sd = scannedDevices.get(InternalDevice.InternalType.STEP_SENSOR)) != null) {
                    sd.setSelected(true);
                    sd.setDataTypeView(DataType.STEPS, ViewPosition.SOUTH_WEST.getViewNr());
                }
                if ((sd = scannedDevices.get(InternalDevice.InternalType.BATTERY_SENSOR)) != null) {
                    sd.setSelected(true);
                    sd.setDataTypeView(DataType.BATTERY_LEVEL, ViewPosition.SOUTH.getViewNr());
                }
            }
            PreferenceKey.FIRST_USE.set(false);
        }
    }

    @Override
    public void close() {
        if (Man.debug) LLog.i("Closing InternalMan");

        for (ScannedInternalDevice sd : scannedDevices.values()) sd.close();
        scannedDevices.clear();

        if (Man.debug) LLog.i("Closed InternalMan");
        instance = null;
    }

    @Override
    public boolean startScan(ScanType type) {
        return false;
    }

    @Override
    public void stopScan(ScanType type) {

    }

    public static boolean hasLocation() {
        if (instance == null) return false;
        ScannedInternalDevice sd;
        if ((sd = instance.scannedDevices.get(InternalDevice.InternalType.GPS)) != null) {
            if (    sd.isSelected() &&
                    sd.getDataTypeView(DataType.LOCATION) == ViewPosition.CENTER.getViewNr()
                    )
                return true;
        }
        return false;
    }


}
