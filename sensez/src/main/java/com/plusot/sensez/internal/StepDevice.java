package com.plusot.sensez.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;

import com.plusot.sensez.device.Man;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;

public class StepDevice extends InternalDevice implements SensorEventListener {
    private SensorManager sensorManager = null;
    private static int stepsOffset = -1;
    private static long stepSpeedTime = 0;
    private static int stepSpeedOffset = 0;
    private static int stepsDetected = 0;


    StepDevice(Listener listener) {
        super(InternalType.STEP_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
            Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        }
    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        LLog.i("Un registering listener");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_STEP_COUNTER && event.sensor.getType() != Sensor.TYPE_STEP_DETECTOR) return;
        long now = System.currentTimeMillis();
        Data data = new Data(); //event.timestamp);
        switch (event.sensor.getType()) {
            case Sensor.TYPE_STEP_COUNTER:
                int stepsMan = Man.getInt(getAddress(), DataType.STEPS);
                int steps = (int) event.values[0];
                if (stepsOffset < 0 || stepsOffset > steps) stepsOffset = steps;
                if (stepSpeedTime == 0) {
                    stepSpeedTime = now;
                    stepSpeedOffset = steps;
                } else if (stepSpeedTime > 0 && now - stepSpeedTime > 100) {
                    double stepSpeed = Man.getInt(getAddress(), DataType.STEP_SPEED);
                    stepSpeed = stepSpeed * (1.0 - DataType.STEP_SPEED.floatAvgFactor()) +
                            DataType.STEP_SPEED.floatAvgFactor() * 60000.0 * (steps - stepSpeedOffset) / (now - stepSpeedTime);
                    data.add(getAddress(), DataType.STEP_SPEED, stepSpeed);
                    stepSpeedTime = now;
                    stepSpeedOffset = steps;
                }
                fireData(data
                        .add(getAddress(), DataType.STEPS, stepsMan + steps - stepsOffset)
                        .add(getAddress(), DataType.STEPS_TOTAL, steps)
                );
//                LLog.i("Steps = " + (steps - stepsOffset));
                stepsOffset = steps;
                break;
            case Sensor.TYPE_STEP_DETECTOR:
                stepsDetected++;
//                LLog.i("Steps detected = " + stepsDetected);
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;
        PackageManager packageManager = appContext.getPackageManager();
        return  Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT &&
                packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER) &&
                packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR) &&
                (sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) != null &&
                sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null &&
                sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null;

    }
}