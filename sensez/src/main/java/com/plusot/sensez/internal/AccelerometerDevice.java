package com.plusot.sensez.internal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.util.MathVector;

class AccelerometerDevice extends InternalDevice implements SensorEventListener {
    private static final double GRAVITY = 9.81f;
    private static final double FILTER = 0.99; //was oorspronkelijk 0.8
    private Sensor sensor = null;
    private MathVector refGravity = new MathVector(0.0, 0.0, 1.0);
    private MathVector gravity = new MathVector(0.0, 0.0, GRAVITY);
    private MathVector acceleration = new MathVector(0.0, 0.0, 0.0);
    private long lastGravityFired = 0;
    private float[] lastEvent = null;
    private SensorManager sensorManager = null;
    private double slope = 0;

    AccelerometerDevice(Listener listener) {
        super(InternalType.ACCELEROMETER, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL); //.SENSOR_DELAY_GAME); //.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this, sensor);
        sensorManager = null;
        //LLog.i("Unregistering listener for " + getName());
    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER || sensorManager == null) return;
        lastEvent = event.values.clone();
        //LLog.i("onSensorChanged: acceleration: " + StringUtil.toString(lastEvent));

        calcAngles();
    }

    private void calcAngles() {
        if (lastEvent == null || sensorManager == null) return;
        long now = System.currentTimeMillis();
        String address = getAddress();
        acceleration.assign(lastEvent);
        //LLog.i("calcAngles: acceleration: " + StringUtil.toString(lastEvent));
        gravity.timestimes(FILTER);
        gravity.add(acceleration.times(1 - FILTER));

        //acceleration.minmin(gravity);

        MathVector unitVector = gravity.unitVector();
        MathVector delta = unitVector.min(refGravity);

        double angle = 2 * Math.asin(delta.length() / 2);
        if (delta.getAbsValue(0) > delta.getAbsValue(1) && delta.getValue(0) > 0) angle *= -1;
        if (delta.getAbsValue(1) > delta.getAbsValue(0) && delta.getValue(1) < 0) angle *= -1;

        //if (acceleration.deltaLength() > 0.1 && now - lastAccelerationFired > 40) {
        fireData(new Data().add(address, DataType.ACCELERATION, acceleration.length()));
        fireData(new Data().add(address, DataType.ACCELERATION_XYZ, acceleration.getValues()));

        if (gravity.deltaLength() > 0.1 && now - lastGravityFired > 40) {
            //fireData(new Data().addWatchable(DataType.GRAVITY, gravity.getValues(), now);
            slope = slope * (1.0 - DataType.SLOPE.floatAvgFactor()) + DataType.SLOPE.floatAvgFactor() * Math.min(Math.tan(angle), 10.0);

            fireData(new Data().add(address, DataType.SLOPE, slope));
            gravity.setReference();
            lastGravityFired = now;

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setLevel() {
        refGravity = gravity.unitVector();
    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;
        if ((sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) == null) return false;
        return sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null;
    }

}
