package com.plusot.sensez.internal;

import android.os.Build;
import android.os.Debug;

import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.ShellExec;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.Watchdog;

import java.io.IOException;

class MemoryDevice extends InternalDevice implements Watchdog.Watchable {
    private final String[] topCmd;
    private long rss = 0;
    private long prevRss = 0;
    private int cpuUsage = 0;
    private int prevCpuUsage = 0;
    private double multiplier = 1.0;

    MemoryDevice(Listener listener) {
        super(InternalType.MEMORY_SENSOR, listener);
        int pid = android.os.Process.myPid();
        topCmd = new String[] {
                "sh",
                "-c",
                "top -m 20 -d 1 -n 1 | grep \"" + pid + "\" "};
        Watchdog.addWatchable(this, 1000);
    }

    @Override
    public void close() {
        Watchdog.remove(this);
    }

    @Override
    public void onWatchdogTimer(long count) {
        Runtime runtime = Runtime.getRuntime();
        if (runtime == null) return;
        long gcAllocated = -1;
        long gcFreed = -1;
        //Debug.getMemoryInfo();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String gcAllocatedStr = Debug.getRuntimeStat("art.gc.bytes-allocated");
            String gcFreedStr = Debug.getRuntimeStat("art.gc.bytes-freed");
            try {
                gcAllocated = Long.parseLong(gcAllocatedStr);
            } catch (NumberFormatException e) {
                LLog.i("Could not convert " + gcAllocatedStr + " to a number");
            }
            try {
                gcFreed = Long.parseLong(gcFreedStr);
            } catch (NumberFormatException e) {
                LLog.i("Could not convert " + gcFreedStr + " to a number");
            }
        }
        SleepAndWake.runInNewThread(new Runnable() {
            @Override
            public void run() {
                try {
                    new ShellExec().exec(topCmd, 200, new ShellExec.ShellCallback() {
                        @Override
                        public void shellOut(String shellLine) {
//                            LLog.i("Top = " + shellLine);
                            String[] parts = shellLine.trim().split("\\s+");
                            if (parts.length > 8) {
                                //processInfo.pid = parts[0];
                                if (parts[2].endsWith("%")) parts[4] = parts[2];
                                if (parts[4].endsWith("%")) {
                                    parts[4] = parts[4].substring(0, parts[4].length() - 1);

                                    try {
                                        cpuUsage = Integer.parseInt(parts[4]);
                                    } catch (NumberFormatException e) {
                                        LLog.i("Could not convert " + parts[4] + " to cpu usage (" + shellLine + ")");
                                    }
                                }

//                                try {
//                                    processInfo.threads = Integer.parseInt(parts[6]);
//                                } catch (NumberFormatException e) {
//                                    LLog.i("Could not convert " + parts[6] + " to number of threads (" + shellLine + ")");
//                                }
                                //processInfo.vss = parts[7];
                                if (parts[7].endsWith("g")) parts[7] = parts[6];
                                if (parts[7].endsWith("K")) {
                                    parts[7] = parts[7].substring(0, parts[7].length() - 1);
                                    multiplier = 1024.0;
                                }

                                try {
                                    rss = Long.parseLong(parts[7]);
                                } catch (NumberFormatException e) {
                                    LLog.i("Could not convert " + parts[7] + " to rss (" + shellLine +")");
                                }
                            }
                        }

                        @Override
                        public void processComplete(int tag, String commandLine, int exitValue) {

                        }
                    });
                } catch (IOException e) {
                    LLog.i("Exception calling top command", e);
                } catch (InterruptedException e) {
                    LLog.i("Top command interrupted");
                }
            }
        }, MemoryDevice.class);

        Data data = new Data();
        if (gcAllocated > 0) data.add(getAddress(), DataType.MEMORY_GC_ALLOCATED, 1.0 * gcAllocated);
        if (gcFreed > 0) data.add(getAddress(), DataType.MEMORY_GC_FREED, 1.0 * gcFreed);
        if (gcAllocated > 0 && gcFreed > 0) data.add(getAddress(), DataType.MEMORY_GC, 1.0 * (gcAllocated - gcFreed));
        if (rss != prevRss && DataType.MEMORY_RSS.isSupported()) {
            prevRss = rss;
            data.add(getAddress(), DataType.MEMORY_RSS, multiplier * rss);
        }
        if (DataType.MEMORY_FREE.isSupported()) data.add(getAddress(), DataType.MEMORY_FREE, 1.0 * runtime.freeMemory());
        if (DataType.MEMORY_TOTAL.isSupported()) data.add(getAddress(), DataType.MEMORY_TOTAL, 1.0 * runtime.totalMemory());
        if (DataType.MEMORY_MAX.isSupported()) data.add(getAddress(), DataType.MEMORY_MAX, 1.0 * runtime.maxMemory());
        if (DataType.MEMORY_HEAP_ALLOCATED.isSupported()) data.add(getAddress(), DataType.MEMORY_HEAP_ALLOCATED, 1.0 * Debug.getNativeHeapAllocatedSize());

        if (cpuUsage != prevCpuUsage) {
            prevCpuUsage = cpuUsage;
            data.add(getAddress(), DataType.CPU, 0.01 * cpuUsage);
        }
        fireData(data.add(getAddress(), DataType.MEMORY, 1.0 * (runtime.totalMemory() - runtime.freeMemory()))
//                .addWatchable(getAddress(), DataType.MEMORY_PSS, 1.0 * Debug.getPss())
        );
    }

    @Override
    public void onWatchdogClose() {

    }


}
