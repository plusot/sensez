package com.plusot.sensez.internal;

import android.location.GpsSatellite;

import com.plusot.util.util.StringUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GpsStatusHelper {
    private final int timeToFirstFix;
    private final int event;
    private GpsSatellite satellites[];

    public GpsStatusHelper(int event, android.location.GpsStatus status) {
        this.event = event;
        timeToFirstFix = status.getTimeToFirstFix();
        int max = status.getMaxSatellites();
        if (max > 0) {
            List<GpsSatellite> list = new ArrayList<>();
            Iterator<GpsSatellite> it = status.getSatellites().iterator();
            while (it.hasNext()) {
                list.add(it.next());
            }
            if (list.size() > 0) {
                satellites = list.toArray(new GpsSatellite[list.size()]);
            } else {
                satellites = null;
            }
        } else
            satellites = null;
    }

    @Override
    public String toString() {
        if (satellites == null) {
            return "GpsStatusHelper: null, event = " + event + ", timetofirstfix = " + timeToFirstFix;
        }
        return "GpsStatusHelper: " + StringUtil.toString(satellites) + ", event = " + event + ", timetofirstfix = " + timeToFirstFix;
    }

    public GpsSatellite[] getSatellites() {
        return satellites;
    }

    public int getTimeToFirstFix() {
        return timeToFirstFix;
    }

    public int getEvent() {
        return event;
    }

}
