package com.plusot.sensez.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;

public class BatteryDevice extends InternalDevice {
    private double batteryPercentage = 0;

    BatteryDevice(Listener listener) {
        super(InternalType.BATTERY_SENSOR, listener);
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        appContext.registerReceiver(batteryReceiver, filter);
        SleepAndWake.runInMain(5000, () -> {
            batteryPercentage = BatteryExtensionKt.getBatteryLevel(appContext) * 0.99;
            LLog.d("Initial battery level = " + Format.format(batteryPercentage, 2));
            fireData(new Data().add(getAddress(), DataType.BATTERY_LEVEL, batteryPercentage));
        });
    }

    private BroadcastReceiver batteryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
            if (batteryPercentage != 1.0 * level / scale) {
                batteryPercentage = 1.0 * level / scale;
                fireData(new Data().add(getAddress(), DataType.BATTERY_LEVEL, batteryPercentage));
            }
        }
    };


    @Override
    public void close() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        appContext.unregisterReceiver(batteryReceiver);
        LLog.i("Unregistering listener");
    }

}
