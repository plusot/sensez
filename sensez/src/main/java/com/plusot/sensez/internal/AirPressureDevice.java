package com.plusot.sensez.internal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.plusot.sensez.SensezGlobals;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;

public class AirPressureDevice extends InternalDevice implements SensorEventListener {
    private double speedAltitude = Double.NaN;
    private double slopeAltitude = Double.NaN;
    private double prevDistance = Double.NaN;
    private long timestamp = 0;
    private SensorManager sensorManager = null;
    private Double altOffset = null;
    private double slope = 0;

    public AirPressureDevice(Listener listener) {
        super(InternalType.AIR_PRESSURE_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        LLog.i("Unregistering listener");
    }

    long prevChange = 0;

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_PRESSURE) return;

        long now = System.currentTimeMillis();
        if (now - prevChange < SensezGlobals.SENSOR_MIN_INTERVAL) return;
        prevChange = now;

        double pressure = 100.0 * event.values[0];
        fireData(new Data().add(getAddress(), DataType.AIR_PRESSURE, pressure));

        //P = Actual pressure (Pascal)
        //Po = 101325	sea level standard pressure, Pa
        //To = 288.15   sea level standard temperature, deg K
        //g = 9.80665   gravitational constant, m/sec2
        //L =  6.5    	temperature lapse rate, deg K/km
        //R = 8.31432	gas constant, J/ mol*deg K
        //M = 28.9644	molecular weight of dry air, gm/mol
        //H = To / L * (1 - Power{ (P / Po) , ((L * R ) / (g * M)) } )

        double newAltitude = 1000.0 * (44.3308 - 4.94654 * Math.pow(pressure, 0.190263));
        //lastAltitude = newAltitude;
        //lastAltitudeTime = now;
        //LLog.i("AirPressureSensor.onSensorChanged: Pressure = " + pressure + ", Alt = " + newAltitude);
        if (altOffset == null && SensezGlobals.lastLocation != null && now - SensezGlobals.lastLocation.getTime() < 60000) {
            double offset = SensezGlobals.lastLocation.getAltitude() - newAltitude;
            LLog.i("Set altitude offset " + Format.format(offset, 1) + " m");
            altOffset = offset;
        }
        if (altOffset != null)
            fireData(new Data().add(getAddress(), DataType.ALTITUDE, newAltitude + altOffset));
        else
            fireData(new Data().add(getAddress(), DataType.ALTITUDE, newAltitude));

        if (timestamp > 0 && !Double.isNaN(speedAltitude)  && now - timestamp > 10000) {
            fireData(new Data().add(getAddress(), DataType.VERTICAL_SPEED, 1000 * (newAltitude - speedAltitude) / (now - timestamp)));
            timestamp = now;
            speedAltitude = newAltitude;
        }

        double distance = Device.getDoubleValue(DataType.DISTANCE);
        if (!Double.isNaN(slopeAltitude) && !Double.isNaN(distance) && !Double.isNaN(prevDistance) && distance - prevDistance > 10.0) {
            slope = slope * (1.0 - DataType.SLOPE.floatAvgFactor()) + DataType.SLOPE.floatAvgFactor() * (newAltitude - slopeAltitude) / (distance - prevDistance);
            fireData(new Data().add(getAddress(), DataType.SLOPE, slope));
            prevDistance = distance;
            slopeAltitude = newAltitude;
        }

        if (timestamp == 0 || Double.isNaN(speedAltitude) ) {
            timestamp = now;
            speedAltitude = newAltitude;
        }
        if (Double.isNaN(prevDistance) || Double.isNaN(slopeAltitude))  {
            prevDistance = Device.getDoubleValue(DataType.DISTANCE);
            slopeAltitude = newAltitude;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        return appContext != null && (sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) != null && sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null;

    }
}