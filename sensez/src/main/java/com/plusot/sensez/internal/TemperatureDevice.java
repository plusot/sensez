package com.plusot.sensez.internal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.plusot.sensez.SensezGlobals;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;

public class TemperatureDevice extends InternalDevice implements SensorEventListener {
    private SensorManager sensorManager = null;

    public TemperatureDevice(Listener listener) {
        super(InternalType.TEMPERATURE_SENSOR, listener);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void close() {
        if (sensorManager != null) sensorManager.unregisterListener(this);
        LLog.i("Unregistering listener");
    }

    long prevChange = 0;

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_AMBIENT_TEMPERATURE) return;

        long now = System.currentTimeMillis();
        if (now - prevChange < SensezGlobals.SENSOR_MIN_INTERVAL) return;
        prevChange = now;

        double temperature = 1.0 * event.values[0];
        fireData(new Data().add(getAddress(), DataType.TEMPERATURE, temperature));

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static boolean available() {
        SensorManager sensorManager;
        Context appContext = Globals.getAppContext();
        return appContext != null && (sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE)) != null && sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null;

    }
}