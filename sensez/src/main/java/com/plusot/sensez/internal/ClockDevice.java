package com.plusot.sensez.internal;

import com.plusot.sensez.device.Man;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.Watchdog;

public class ClockDevice extends InternalDevice implements Watchdog.Watchable {
    private static long moving = -1;
    private static long active = -1;
    private static long dummyActive = 0;
    private static long dummyMoving = 0;
    private static long total = -1;
    private static long totalSaved = 0;
    private static long old = 0;

    ClockDevice(Listener listener) {
        super(InternalType.CLOCK, listener);
        Watchdog.addWatchable(this, 1000);
    }

    @Override
    public void close() {
        LLog.i("Closing Clock");
        if (total > -1) PreferenceKey.TOTAL_TIME.set(total);
        Watchdog.remove(this);
    }

    @Override
    public void onWatchdogTimer(long count) {
        long now = System.currentTimeMillis();
        if (isPausing()) {
            old = 0;
            return;
        }
        Data data = new Data();
        if (Man.isShowNew()) {
            if (active < 0) active = Man.getLong(getAddress(), DataType.TIME_ACTIVITY);
            if (moving < 0) {
                moving = Man.getLong(getAddress(), DataType.TIME_ACTIVE);
                LLog.i("Grabbed active time from history: " + TimeUtil.formatElapsedSeconds(moving, false));
            }
        }

        if (total < 0) total = PreferenceKey.TOTAL_TIME.getLong();
        if (old != 0) {
            if (Man.isShowNew()) {
                active += (now - old) + dummyActive;
                dummyActive = 0;
                data.add(getAddress(), DataType.TIME_ACTIVITY, active);
                if (Man.getDouble(GpsDevice.getStaticAddress(), DataType.SPEED) > 2.4 / 3.6) {
                    moving += (now - old) + dummyMoving;
                    dummyMoving = 0;
                    data.add(getAddress(), DataType.TIME_ACTIVE, moving);
                }
            } else {
                dummyActive += (now - old);
                if (Man.getDouble(GpsDevice.getStaticAddress(), DataType.SPEED) > 2.4 / 3.6) dummyMoving += (now - old);
            }
            total += (now - old);
            if (now - totalSaved > 60000) {
                PreferenceKey.TOTAL_TIME.set(total);
                totalSaved = now;
            }
        }
        old = now;
        fireData(data
                .add(getAddress(), DataType.TIME_CLOCK, now)
                .add(getAddress(), DataType.TIME_TOTAL, total)
        );
    }

    @Override
    public void onWatchdogClose() {

    }

    public static void reset() {
        moving = -1;
        active = -1;
        old = 0;
    }

    public static void resetTotal() {
        total = 0;
    }

    static String getStaticAddress() {
        return StringUtil.proper(InternalType.CLOCK.name());
    }
}