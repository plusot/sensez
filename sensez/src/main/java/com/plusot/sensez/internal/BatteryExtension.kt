package com.plusot.sensez.internal

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

fun Context.getBatteryLevel(): Double {
    val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
        this.registerReceiver(null, ifilter)
    }
    return batteryStatus?.let { intent ->
        val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        (level.toDouble() / scale)
    } ?: 0.0
}

//fun getBatteryLevel(context: Context): Double = context.getBatteryLevel()