package com.plusot.sensez.internal;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.OnNmeaMessageListener;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.plusot.sensez.R;
import com.plusot.sensez.SensezGlobals;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.Nmea;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Calc;
import com.plusot.util.util.Numbers;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.Sound;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Watchdog;

import java.lang.ref.WeakReference;

public class GpsDevice extends InternalDevice implements Watchdog.Watchable {
    public static int gpsInterval = 1000;
    public static int gpsMinDistance = 0;
    private static final double ASCENT_DELTA = 5;
    private static final double SLOPE_DISTANCE_DELTA = 10; //meter
    private static double totalDistance = -1;
    public static boolean checkGPS = true;
    public static boolean useNmea = false;
    private static boolean debugNmea = false;
    private Data nmeaData = null;
    private final Object nmeaLock = new Object();


    private LocationManager locationMgr;
    private int prevSatellites = 0;
    private GpsLocationListener gpsLocationListener = new GpsLocationListener();

    private static WeakReference<GpsDevice> deviceRef = null;
    private static long gpsLastUpdate = -1;

    @Override
    public void onWatchdogTimer(long count) {
        final long now = System.currentTimeMillis();
        if (!Man.isShowNew()) return;
        if (gpsLastUpdate > 0 && now - gpsLastUpdate > 120000 && checkGPS) SleepAndWake.runInMain(() -> {
            Context context = Globals.getAppContext();
            if (context != null) Sound.play(Globals.getAppContext(), R.raw.alarm);
            ToastHelper.showToastLong(R.string.gps_not_available);
            gpsLastUpdate = now - 90000;
        }, "GpsDevice.onWatchdogTimer");
    }

    @Override
    public void onWatchdogClose() {

    }

    private enum State {
        PROVIDER_DISABLED(R.string.provider_disabled),
        PROVIDER_ENABLED(R.string.provider_enabled),
        PROVIDER_AVAILABLE(R.string.provider_available),
        PROVIDER_OUT_OF_SERVICE(R.string.provider_out_of_service),
        PROVIDER_TEMPORARILY_UNAVAILABLE(R.string.provider_temporarily_unavailable),
        SATELLITES_IN_VIEW(R.string.satellites_in_view),
        LOCATION_FIRST_FIX(R.string.location_first_fix);

        private final int idLabel;

        State(final int label) {
            this.idLabel = label;
        }

        @SuppressWarnings("unused")
        public String getLabel(Context context) {
            return context.getString(idLabel);
        }

        public String getLabel(Context context, Object... args) {
            if (context == null) {
                LLog.e("No context in Device.State.getLabel");
                return "unknown state";
            }
            return context.getString(idLabel, args);
        }
    }

    GpsDevice(Listener listener) {
        super(InternalType.GPS, listener);
        deviceRef = new WeakReference<>(this);
        Watchdog.addWatchable(this, 5000);
        launch();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void close() {
        if (Man.debug) LLog.i("Closing GPS");
        Watchdog.remove(this);

        if (totalDistance > -1) PreferenceKey.TOTAL_DISTANCE.set(totalDistance);

        if (locationMgr != null) {
            locationMgr.removeGpsStatusListener(gpsStatusListener);
            Context appContext = Globals.getAppContext();
            if (appContext == null) return;

            if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                LLog.i("No permission to access fine or coarse location");
                return;
            }
            locationMgr.removeUpdates(gpsLocationListener);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (onNmeaListener != null) locationMgr.removeNmeaListener(onNmeaListener);
            } else {
//                locationMgr.removeNmeaListener(nmeaListener);
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private OnNmeaMessageListener onNmeaListener = null; //(nmea, timestamp) -> handleNmeaString(nmea, timestamp);


//    @SuppressWarnings("deprecation")
//    private GpsStatus.NmeaListener nmeaListener = (timestamp, nmea) -> handleNmeaString(nmea, timestamp);


    private void handleNmeaString(String nmea, long timestamp) {
        if (nmea == null) return;
        if (debugNmea && !nmea.equalsIgnoreCase("")) LLog.i("nmea = " + nmea.replace("\r\n", "") + " at " + TimeUtil.formatMilli(timestamp, 3));
        String[] parts = nmea.split(",");
        if (parts.length > 0) {
            Nmea nmeaType = Nmea.fromString(parts[0]);
            if (nmeaType != null) switch (nmeaType) {
                case GPGSV:
                    if (parts.length > 4) {
                        if (Man.debug) LLog.i("nmea Satellites raw: " + parts[3]);
                        try {
                            int satellites = Numbers.parseInt(parts[3]);
                            if (satellites > 0 && satellites != prevSatellites) {
                                if (Man.debug) LLog.i( "nmea Satellites: " + satellites);
                                prevSatellites = satellites;
                                fireData(new Data().add(getAddress(), DataType.SATELLITES_VISIBLE, satellites));
                            }
                        } catch (NumberFormatException e) {
                            LLog.e("nmea Invalid number: " + parts[3]);
                        }
                    }
                    break;
                case GNGGA:
                case GPGGA:
                    if (parts.length > 13 && parts[1] != null && parts[1].length() > 0) {
                        if (Man.debug) LLog.i("nmea: " + nmea + " at " + TimeUtil.formatTime(timestamp));
                        String parse = parts[1];
                        String[] timeParts = parse.split("\\.");
                        if (timeParts.length > 1) {
                            timeParts[1] = (timeParts[1] + "000").substring(0, 3);
                            parse = TimeUtil.formatTimeUTC(System.currentTimeMillis(), "yyyy-MM-dd") + " " + timeParts[0] + '.' + timeParts[1];
                        } else {
                            parse = TimeUtil.formatTimeUTC(System.currentTimeMillis(), "yyyy-MM-dd") + " " + timeParts[0] + ".000";
                        }
                        long time = TimeUtil.parseTimeUTC(parse, "yyyy-MM-dd HHmmss.S");

                        if (Man.debug) LLog.i("nmea: " + parse + " -> Time = " + TimeUtil.formatMilli(time, 3));
                        int quality = Numbers.parseInt(parts[6]);
                        synchronized (nmeaLock) {
                            nmeaData = new Data().
                                    add(getAddress(), DataType.TIME_GPS, time).
                                    add(getAddress(), DataType.TIME_CLOCK, timestamp).
                                    add(getAddress(), DataType.TIME_ACCURACY, timestamp - System.currentTimeMillis()).
                                    add(getAddress(), DataType.POSITION_QUALITY, quality);
                            if (parts[2].length() > 3 && parts[4].length() > 3) {
                                double lat = Numbers.parseInt(parts[2].substring(0, 2)) + Numbers.parseDouble(parts[2].substring(2)) / 60;
                                if (parts[3].equals("S")) lat *= -1;
                                double lng = Numbers.parseInt(parts[4].substring(0, 3)) + Numbers.parseDouble(parts[4].substring(3)) / 60;
                                if (parts[5].equals("W")) lng *= -1;
                                nmeaData.add(getAddress(), DataType.LOCATION_NMEA, new double[]{lat, lng});
                            }
                            int satellites = Numbers.parseInt(parts[7]);
                            nmeaData.add(getAddress(), DataType.SATELLITES_FIX, satellites);

                            if (parts[8].length() > 0) {
                                double hdop = +Numbers.parseDouble(parts[8]);
                                nmeaData.add(getAddress(), DataType.HDOP, hdop);
                            }
                            if (parts[9].length() > 0 && parts[10].equalsIgnoreCase("M")) {
                                double alt = +Numbers.parseDouble(parts[9]);
                                nmeaData.add(getAddress(), DataType.ALTITUDE_NMEA, alt + PreferenceKey.getAltitudeOffset());
                            }
                            if (parts[11].length() > 0 && parts[12].equalsIgnoreCase("M")) {
                                double geoid = +Numbers.parseDouble(parts[11]);
                                nmeaData.add(getAddress(), DataType.GEOID_HEIGHT, geoid);
                            }
                            nmeaData.add(getAddress(), DataType.NMEA, nmea);
                            fireData(nmeaData);
                        }
                    }
                    break;
                default:
                    break;

            }
        }
    }

    private void launch() {
        if (totalDistance < 0) totalDistance = Math.max(Man.getDouble(getAddress(), DataType.DISTANCE_TOTAL), PreferenceKey.TOTAL_DISTANCE.getDouble());
        if (locationMgr != null) {
            close();
        }

        if (Man.debug) LLog.i("Launching GPS");
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        locationMgr = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        if (locationMgr == null) {
            return;
        }
        SensezGlobals.lastLocation = getLastLocation();
        if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            LLog.i("No permission to access fine or coarse location");
            return;
        }

        final int minDistance = gpsMinDistance;
        SleepAndWake.runInMain( () -> {
            try {
                locationMgr.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        gpsInterval,    // minTime in ms
                        minDistance,    // minDistance in meters
                        gpsLocationListener
                );
            } catch (IllegalArgumentException e) {
                try {
                    locationMgr.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            gpsInterval,    // minTime in ms
                            minDistance,    // minDistance in meters
                            gpsLocationListener
                    );
                } catch (IllegalArgumentException e2) {
                    ToastHelper.showToastLong(R.string.no_location_device);
                    return;
                }
            }
            if (useNmea) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (onNmeaListener == null) onNmeaListener = this::handleNmeaString;
                    locationMgr.addNmeaListener(onNmeaListener);
                }
//                else {
//                    locationMgr.addNmeaListener(nmeaListener);
//                }
            }
            locationMgr.addGpsStatusListener(gpsStatusListener);
        });

    }

    private final GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {
        @Override
        public void onGpsStatusChanged(int event) {
            GpsStatus status;
            Context appContext = Globals.getAppContext();

            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:
                    //LLog.i("GPS Started");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    //LLog.i("GPS Stopped");
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    if (appContext == null) return;
                    if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                        break;
                    status = locationMgr.getGpsStatus(null);
                    if (Man.debug) LLog.i("First fix");
                    if (status != null) {
                        GpsStatusHelper statusHelper = new GpsStatusHelper(event, status);
                        //LLog.i("Satellite status " + parcel.toString());
                        if (statusHelper.getSatellites() != null)
                            fireData(new Data().add(getAddress(), DataType.SATELLITES_VISIBLE, statusHelper.getSatellites().length));
                    }
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    if (appContext == null) return;
                    if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                        break;
                    status = locationMgr.getGpsStatus(null);
                    if (status != null) {
                        GpsStatusHelper statusHelper = new GpsStatusHelper(event, status);
                        if (statusHelper.getSatellites() == null) return;
                        //LLog.i("Satellite status " + parcel.toString());
                        fireData(new Data().add(getAddress(), DataType.SATELLITES_VISIBLE, statusHelper.getSatellites().length));
                    }
                    break;
            }
        }
    };

    private Location getLastLocation() {
        if (gpsLocationListener != null && gpsLocationListener.prevLocation != null) return gpsLocationListener.prevLocation;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return null;

        if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            LLog.i("No permission to access fine or coarse location");
            return null;
        }
        Location loc = locationMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (loc == null)
            loc = locationMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (loc == null) {
            loc = new Location(LocationManager.GPS_PROVIDER);
            loc.setLatitude(PreferenceKey.LAST_LOCATION_LAT.getDouble());
            loc.setLongitude(PreferenceKey.LAST_LOCATION_LONG.getDouble());
        }
        return loc;
        //return null;
    }

    private class GpsLocationListener implements LocationListener {
        private Location prevLocation = null;
        private Location prevSlopeLocation = null;
        private double slope = 0;
        private double altitude = Double.NaN;
        private int altitudeCount = 0;
        private double prevSlopeAltitude = Double.NaN;
        private double dummyDistance = 0;
        private double dummyAscent = 0;
        private double dummyDescent = 0;
        private double prevAscentAltitude = Double.NaN;
        private long distanceUpdate = 0;

        private void handleLocationChanged(final long now, final Location location) {
            double[] loc = new double[]{location.getLatitude(), location.getLongitude()};
            Data data = new Data().add(getAddress(), DataType.LOCATION, loc);
            gpsLastUpdate = now;

            if (location.hasAltitude()) {

                double rawAltitude = location.getAltitude();
                data.add(getAddress(), DataType.ALTITUDE_RAW, rawAltitude);
                rawAltitude += PreferenceKey.getAltitudeOffset();
                synchronized (nmeaLock) {
                    if (nmeaData != null && nmeaData.hasDataType(DataType.ALTITUDE_NMEA)) {
//                        double nmeaAlt
                        rawAltitude = nmeaData.getDouble(null, DataType.ALTITUDE_NMEA);
//                        LLog.d("Using NMEA altitude " + Format.format(nmeaAlt, 1) +
//                                "m instead of " + Format.format(rawAltitude, 1) + "m");
//                        rawAltitude = nmeaAlt;
                    }
                }
                if (Double.isNaN(altitude)) {
                    altitude = rawAltitude;
                    prevSlopeAltitude = altitude;
                    prevSlopeLocation = location;
                } else if (altitudeCount++ < 6) {
                    prevAscentAltitude = altitude;
                    altitude = altitude * (1.0 - DataType.ALTITUDE.floatAvgFactor()) + DataType.ALTITUDE.floatAvgFactor() * rawAltitude;
                    prevSlopeAltitude = altitude;
                    prevSlopeLocation = location;
                } else {
                    altitude = altitude * (1.0 - DataType.ALTITUDE.floatAvgFactor()) + DataType.ALTITUDE.floatAvgFactor() * rawAltitude;
                    double slopeDistance = location.distanceTo(prevSlopeLocation);
                    if (slopeDistance > SLOPE_DISTANCE_DELTA) {
                        double deltaAltitude = altitude - prevSlopeAltitude;
                        slope = slope * (1.0 - DataType.SLOPE.floatAvgFactor()) + DataType.SLOPE.floatAvgFactor() * deltaAltitude / slopeDistance;
                        data.add(getAddress(), DataType.SLOPE, slope);
                        prevSlopeAltitude = altitude;
                        prevSlopeLocation = location;
                    }
                    data.add(getAddress(), DataType.ALTITUDE, altitude);
                    if (Double.isNaN(prevAscentAltitude)) prevAscentAltitude = altitude;
                    double delta = altitude - prevAscentAltitude;
                    if (delta > ASCENT_DELTA) {
                        if (Man.isShowNew()) {
                            double ascent = Man.getDouble(getAddress(), DataType.ASCENT);
                            if (Double.isNaN(ascent)) ascent = 0;
                            ascent += delta + dummyAscent;
                            dummyAscent = 0;
                            data.add(getAddress(), DataType.ASCENT, ascent);
                        } else
                            dummyAscent += delta;
                    } else if (delta < - ASCENT_DELTA) {
                        if (Man.isShowNew()) {
                            double descent = Man.getDouble(getAddress(), DataType.DESCENT);
                            if (Double.isNaN(descent)) descent = 0;
                            descent += delta + dummyDescent;
                            dummyDescent = 0;
                            data.add(getAddress(), DataType.DESCENT, descent);
                        } else
                            dummyDescent += delta;
                    }

                    if (Math.abs(delta) > ASCENT_DELTA) prevAscentAltitude = altitude;
                }
            }
            if (prevLocation != null) {
                double deltaDistance = location.distanceTo(prevLocation);
                double speed = 1000 * deltaDistance / (location.getTime() - prevLocation.getTime());
                double bearing = Calc.bearing(prevLocation, location);
                if (!Double.isNaN(bearing)) data.add(getAddress(), DataType.BEARING, bearing);
                if (Man.isShowNew()) {
                    if (deltaDistance > 0) {
                        double distance = Man.getDouble(getAddress(), DataType.DISTANCE);
                        if (Double.isNaN(distance)) distance = 0;
                        distance += deltaDistance + dummyDistance;
                        dummyDistance = 0;
                        data.add(getAddress(), DataType.DISTANCE, distance);
                        long time = Man.getLong(ClockDevice.getStaticAddress(), DataType.TIME_ACTIVE);
                        if (time > 30000) data.add(getAddress(), DataType.SPEED_AVG, 1000.0 * distance / time);

                    }
                } else {
                    dummyDistance += deltaDistance;
                }
                if (deltaDistance > 0) {
                    totalDistance += deltaDistance;
                    data.add(getAddress(), DataType.DISTANCE_TOTAL, totalDistance);
                    if (now - distanceUpdate > 60000) {
                        PreferenceKey.TOTAL_DISTANCE.set(totalDistance);
                        distanceUpdate = now;
                    }
                }
                data.add(getAddress(), DataType.SPEED, speed);
            }

            Bundle extras = location.getExtras();
            if (extras != null && extras.containsKey("satellites")) {
                atStateChanged(State.SATELLITES_IN_VIEW, extras.getInt("satellites"));
                data.add(getAddress(), DataType.SATELLITES_USED, extras.getInt("satellites"));
            }
            fireData(data);
            prevLocation = location;
            SensezGlobals.lastLocation = location;
            //lastLocationTime = now;
        }


        public void onLocationChanged(final Location location) {
            //LLog.i("GpsDevice Location changed: " + location);
            final long now = System.currentTimeMillis(); //location.getTime();
            if (location != null && Math.abs(location.getLongitude()) > 0.01 && Math.abs(location.getLatitude()) > 0.01) {
                SleepAndWake.runInNewThread(() -> handleLocationChanged(now, location), "GpsLocationListener.onLocationChanged");

            }
        }

        private void atStateChanged(State state, Object... args) {
            if (!Man.debug) return;
            Context appContext = Globals.getAppContext();
            if (appContext == null) return;

            LLog.i("StateChanged = " + state.getLabel(appContext, args));
        }


        public void onProviderDisabled(String provider) {
            atStateChanged(State.PROVIDER_DISABLED, provider);
        }

        public void onProviderEnabled(String provider) {
            atStateChanged(State.PROVIDER_ENABLED, provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    //					LLog.i("Status provider available");
                    atStateChanged(State.PROVIDER_AVAILABLE, provider);
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    if (Man.debug) LLog.i("Status provider out of service");
                    atStateChanged(State.PROVIDER_OUT_OF_SERVICE, provider);
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    if (Man.debug) LLog.i("Status provider temporarily unavailable");
                    atStateChanged(State.PROVIDER_TEMPORARILY_UNAVAILABLE, provider);
                    break;
            }

            if (extras != null && extras.containsKey("satellites")) {
                //					LLog.i("Satellites: " + extras.getInt("satellites"));
                atStateChanged(State.SATELLITES_IN_VIEW, extras.getInt("satellites"));
                fireData(new Data().add(getAddress(), DataType.SATELLITES_USED, extras.getInt("satellites")));
            }
        }
    }

    public static boolean available() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;

        LocationManager mgr = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        return mgr != null && mgr.getProviders(true).size() > 0;
    }

    public static void resetTotalDistance() {
        GpsDevice.totalDistance = 0;
        //save();
    }

    public static void inject(double value, DataType type) {
        if (deviceRef == null) return;
        GpsDevice device = deviceRef.get();
        if (device == null) return;
        device.fireData(new Data().add(device.getAddress(), type, value));
    }

    static String getStaticAddress() {
        return StringUtil.proper(InternalType.GPS.name());
    }

}
