package com.plusot.sensez;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import com.plusot.sensez.activities.NullActivity;
import com.plusot.sensez.bluetooth.BlueMan;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.internal.GpsDevice;
import com.plusot.sensez.internal.InternalMan;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.shareddata.db.DbData;
import com.plusot.shareddata.share.HttpTask;
import com.plusot.util.Globals;
import com.plusot.util.dialog.Alerts;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.SimpleLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Watchdog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.plusot.util.preferences.PreferenceKey.PERMISSIONS_ACCEPTED;

//TODO: Option: iBeacon optimized settings
//TODO: Ascent calculation currently too conservative
//TODO: Do not kill -> Create background service

//TODO: Keep GPS on

public class SensezLib  {
    private static Permits permits = null;
    public static final int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 8004;
    public static final int ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE = 8005;
    public static final int PERMISSIONS_REQUEST = 8002;
    public static final String MANAGE_OVERLAY_PERMISSION_REQUEST_CODE  = "ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE";
    public static final String MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE = "ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE";
    public static final String PERMISSIONS = "permissions";

    private static boolean requestOverlay = false;
    private static boolean requestWriteSettings = false;
    private static List<String> permissionsToRequest = new ArrayList<>();

    private static EnumSet<Option> options = EnumSet.noneOf(Option.class);
    private static boolean isCreated = false;
    private static boolean withAlert = true;

    private static int permissionsStringId = R.string.askPermissionMsg;

    public static class Permit {
        final String[] permissionStrings;

        public Permit(String[] permissionStrings) {
            this.permissionStrings = permissionStrings;
        }

        boolean hasPermission(String permission) {
            for (String p: permissionStrings) {
                if (p.equals(permission)) return true;
            }
            return false;
        }

        boolean isGranted(Context context) {
            for (String p: permissionStrings) {
                if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, p)) return true;
            }
            return false;
        }

//        int permissionLength() {
//            return permissionStrings.length;
//        }

        List<String> getPermissionsList() {
            List<String> list = new ArrayList<>();
            list.addAll(Arrays.asList(permissionStrings));
            return list;
        }

    }

    private static Set<WeakReference<EventListener>> eventListeners = new HashSet<>();

    public enum SensezLibEvent {
        SENSEZ_INIT,
        SENSEZ_REINIT,
        //        PERMIT_ACCEPTED,
        SENSEZ_CLOSE
    }

    public interface EventListener {
        void onSensezLibEvent(SensezLibEvent event);
    }

    public interface PermitAcceptedListener {
        void onAccepted();
    }

    public static void addEventListener(EventListener eventListener) {
        eventListeners.add(new WeakReference<>(eventListener));
    }

    @SuppressWarnings("unused")
    public static boolean isRequestedPermission(String permission) {
        if (permits != null) for (Permit p: permits.permits) {
            if (p.hasPermission(permission)) return true;
        }
        return false;
    }

    public static class Permits {
        private Permit[] permits;
        private final boolean needsOverlays;
        private final boolean needsToWriteSettings;

        public Permits(Permit[] permits, boolean needsOverlays, boolean needsToWriteSettings) {
            this.permits = permits;
            this.needsOverlays = needsOverlays;
            this.needsToWriteSettings = needsToWriteSettings;
        }

        private void addPermit(Permit permit) {
            permits = Arrays.copyOf(permits, permits.length + 1);
            permits[permits.length - 1] = permit;
        }
    }

    public enum Option {
        USE_HTTP,           // Will also enable USE_DB
        USE_DB,
        USE_BLE_SENSORS,
        USE_BT_SENSORS,
        USE_ANT_SENSORS,
        USE_INTERNAL_SENSORS,
        USE_NMEA,
        CHECK_GPS,
        START_IMMEDIATELY,
        VERBOSE,
        DEFAULT_FIELDS
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, String[] permitsOverride, @StringRes int permissionsStringId) {
        SensezLib.permissionsStringId = permissionsStringId;
        Permit[] permitArray = new Permit[permitsOverride.length];
        for (int i = 0; i < permitsOverride.length; i++) permitArray[i] = new Permit(permitsOverride);
        Permits permits = new Permits(permitArray, false, false);
        create(app, activity, options, permits);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, String[] permitsOverride) {
        Permit[] permitArray = new Permit[permitsOverride.length];
        for (int i = 0; i < permitsOverride.length; i++) permitArray[i] = new Permit(permitsOverride);
        Permits permits = new Permits(permitArray, false, false);
        create(app, activity, options, permits);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, Permit[] permitsOverride, @StringRes int permissionsStringId) {
        SensezLib.permissionsStringId = permissionsStringId;
        Permits permits = new Permits(permitsOverride, false, false);
        create(app, activity, options, permits);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, Permits permitsOverride, @StringRes int permissionsStringId) {
        SensezLib.permissionsStringId = permissionsStringId;
        create(app, activity, options, permitsOverride);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, Permit[] permitsOverride) {
        Permits permits = new Permits(permitsOverride, false, false);
        create(app, activity, options, permits);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options) {
        create(app, activity, options, (Permits) null);
    }

    public static void create(Application app, Activity activity, EnumSet<Option> options, Permits permitsOverride) {
        if (!isCreated) {
            Watchdog.getInstance();

            Globals.init(app, SensezLib.class.getSimpleName());

            LLog.i("Create called");
            SensezLib.options = options;
            if (options.contains(Option.USE_HTTP)) {
                DbData.setMayUseDB(true);
                HttpTask.startInstance();
            } else {
                DbData.setMayUseDB(options.contains(Option.USE_DB));
            }

            if (    options.contains(Option.USE_BLE_SENSORS) ||
                    options.contains(Option.USE_BT_SENSORS) ||
                    options.contains(Option.USE_ANT_SENSORS) ||
                    options.contains(Option.USE_INTERNAL_SENSORS)
                    ) {
                PreferenceKey.USE_BLE.set(options.contains(Option.USE_BLE_SENSORS));
                PreferenceKey.USE_BT.set(options.contains(Option.USE_BT_SENSORS));
                PreferenceKey.USE_ANT.set(options.contains(Option.USE_ANT_SENSORS));
                PreferenceKey.USE_INTERNAL.set(options.contains(Option.USE_INTERNAL_SENSORS));
            }

            if (options.contains(Option.VERBOSE)) Globals.verbose = true;

            GpsDevice.useNmea = options.contains(Option.USE_NMEA);
            GpsDevice.checkGPS = options.contains(Option.CHECK_GPS);


            if (options.contains(Option.DEFAULT_FIELDS))
                PreferenceKey.USE_DEFAULT_FIELDS.set(true);
            else
                PreferenceKey.USE_DEFAULT_FIELDS.set(false);

            app.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//                    LLog.i("Created activity = " + activity.getClass().getSimpleName());
                    if (Globals.getVisibleActivity() == null) Globals.setVisibleActivity(activity);
                }

                @Override
                public void onActivityStarted(Activity activity) {

                }

                @Override
                public void onActivityResumed(Activity activity) {
//                    LLog.i("Active activity = " + activity.getClass().getSimpleName());
                    Globals.setVisibleActivity(activity);
                }

                @Override
                public void onActivityPaused(Activity activity) {
                    if (Globals.getVisibleActivity()  == activity) {
//                        LLog.i("Paused activity = " + activity.getClass().getSimpleName());
                        Globals.setVisibleActivity(null);
                    }
                }

                @Override
                public void onActivityStopped(Activity activity) {

                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {
                    //Globals.runMode = Globals.RunMode.FINISHING;
                }
            });
            if (permitsOverride != null)
                SensezLib.permits = permitsOverride;
            else if (PreferenceKey.USE_INTERNAL.isTrue() || PreferenceKey.USE_BLE.isTrue()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    permits = new Permits(
                            new Permit[]{
//                                    new Permit(new String[]{Manifest.permission.READ_PHONE_STATE}),
                                    //new Permit(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}),
                                    new Permit(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}),
                                    new Permit(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}),
                                    new Permit(new String[]{Manifest.permission.ACTIVITY_RECOGNITION}),
                                    new Permit(new String[]{Manifest.permission.BODY_SENSORS}),
                                    new Permit(new String[]{Manifest.permission.BLUETOOTH_ADMIN})
                            },
                            false,
                            false
                    );
                } else {
                    permits = new Permits(
                            new Permit[]{
//                                    new Permit(new String[]{Manifest.permission.READ_PHONE_STATE}),
                                    new Permit(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}),
                                    new Permit(new String[]{Manifest.permission.BLUETOOTH_ADMIN})
                            },
                            false,
                            false
                    );
                }
            } else {
                permits = new Permits(
                        new Permit[]{
//                                new Permit(new String[]{Manifest.permission.READ_PHONE_STATE}),
                                new Permit(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE})
                        },
                        false,
                        false
                );

            }
        }
        isCreated = true;

        if (activity != null && permits != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasEnoughPermits(activity)) {
            if (permissionsStringId == -1) withAlert = false;
            queryPermits(activity);
        } else
            runInit(activity);
    }

    public static void queryPermits(Activity activity, Permits permits) {
        SensezLib.permits = permits;
        if (permits != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasEnoughPermits(activity)) queryPermits(activity);
    }

    public static void queryPermit(Activity activity, Permit permit) {
        SensezLib.permits.addPermit(permit);
        if (permits != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasEnoughPermits(activity)) queryPermits(activity);
    }

    public static void close() {
        //SleepAndWake.runInNewThread(() -> {
        LLog.i("Terminating ...");

        Globals.runMode = Globals.RunMode.FINISHING;
        for (WeakReference<EventListener> eventListenerRef : eventListeners) {
            EventListener eventListener = eventListenerRef.get();
            if (eventListener != null) eventListener.onSensezLibEvent(SensezLibEvent.SENSEZ_CLOSE);
        }
        eventListeners.clear();
        HttpTask.stopInstance();

        Man.stopInstance();
        DbData.stopInstance();

        SimpleLog.stopInstances();
        Watchdog.stopInstance();
        LLog.i("Terminated");

        //}, com.plusot.sensez.SensezLib.class);
    }


    private static boolean hasEnoughPermits(Activity activity) {
        Context appContext = Globals.getAppContext();
        permissionsToRequest.clear();
        if (appContext != null && permits != null && permits.permits != null) for (Permit permit : permits.permits) {
            if (!permit.isGranted(appContext)) permissionsToRequest.addAll(permit.getPermissionsList());
        }

        if (permits != null) {
            requestOverlay = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permits.needsOverlays && !Settings.canDrawOverlays(activity));
            requestWriteSettings = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permits.needsToWriteSettings && !Settings.System.canWrite(activity));
        }
        return !(!PERMISSIONS_ACCEPTED.isTrue() || (permissionsToRequest.size() > 0) || requestOverlay || requestWriteSettings);
    }

    private static void __queryPermits(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (requestOverlay || requestWriteSettings || permissionsToRequest.size() > 0)) {


            if (requestOverlay) {
                Intent intent = new Intent(activity, NullActivity.class);
                intent.putExtra(MANAGE_OVERLAY_PERMISSION_REQUEST_CODE, true);
                activity.startActivity(intent);

//                                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
//                                activity.startActivityForResult(intent, SensezLib.ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);


            }
            if (requestWriteSettings) {
                Intent intent = new Intent(activity, NullActivity.class);
                intent.putExtra(MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE, true);
                activity.startActivity(intent);
//                                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
//                                activity.startActivityForResult(intent, SensezLib.ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE);
//                                LLog.e("No permission to write settings yet");

            }
            if (permissionsToRequest.size() > 0) {
                LLog.i("Requesting permission for " + StringUtil.toString(permissionsToRequest, ", "));
                Intent intent = new Intent(activity, NullActivity.class);
                String[] permissions = permissionsToRequest.toArray(new String[permissionsToRequest.size()]);
                intent.putExtra(PERMISSIONS, permissions);
                activity.startActivity(intent);
//                                ActivityCompat.requestPermissions(activity, permissions, PERMISSIONS_REQUEST);
            }
        } else {
            runInit(activity);
        }

    }

    private static void queryPermits(final Activity activity){
        if (withAlert) Alerts.showYesNoDialog(activity, R.string.askPermissionTitle, permissionsStringId, new Alerts.Listener() {
            boolean handled = false;
            @Override
            public void onClick(Alerts.ClickResult clickResult) {
                switch (clickResult) {
                    case YES:
                        handled = true;
                        PERMISSIONS_ACCEPTED.set(true);
                        __queryPermits(activity);
                        break;
                    case NO:
                    case CANCEL:
                    case NEUTRAL:
                        if (!handled) Alerts.showOkDialog(
                                activity,
                                R.string.permissions_not_accepted_title,
                                R.string.permissions_not_accepted_msg, 0,
                                clickResult1 -> activity.finish()
                        );

                        break;
                }
            }
        });
        else {
            __queryPermits(activity);
        }
    }

    private static boolean initialized = false;


    private static void runInit(final Activity activity) {
        SleepAndWake.runInMain(() -> {
            //LLog.i("Running runInit");
            if (initialized) for (WeakReference<EventListener> eventListenerRef : eventListeners) {
                EventListener eventListener = eventListenerRef.get();
                if (eventListener != null) eventListener.onSensezLibEvent(SensezLibEvent.SENSEZ_REINIT);
            } else {
                for (WeakReference<EventListener> eventListenerRef : eventListeners) {
                    EventListener eventListener = eventListenerRef.get();
                    if (eventListener != null) eventListener.onSensezLibEvent(SensezLibEvent.SENSEZ_INIT);
                }
                initialized = true;
                if (activity instanceof Man.Listener)
                    Man.init((Man.Listener) activity, options.contains(Option.START_IMMEDIATELY));
                else
                    Man.init(null, options.contains(Option.START_IMMEDIATELY));

                if (PreferenceKey.USE_BLE.isTrue()) {
                    Context context = Globals.getAppContext();
                    if (context != null) {
                        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                        boolean gps_enabled = false;
                        boolean network_enabled = false;

                        if (lm != null) {
                            try {
                                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch (Exception ex) {
                                LLog.i("Could not check GPS status");
                            }

                            try {
                                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                            } catch (Exception ex) {
                                LLog.i("Could not check Network location status");
                            }
                        }

                        if ((activity != null) && (lm == null || (!gps_enabled && !network_enabled))) {
                            Alerts.showOkDialog(activity,
                                    R.string.locationNeededTitle,
                                    R.string.locationNeededMsg,
                                    0,
                                    clickResult -> {
                                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        activity.startActivity(myIntent);
                                    });

                        }
                    }
                }
            }
        }, 100, SensezLib.class);
    }

    @SuppressWarnings("unused")
    public static void onRequestPermissionsResult(Activity activity, int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                if (grantResults != null && grantResults.length > 0) {
                    int count = 0;
                    for (int result : grantResults)
                        if (result == PackageManager.PERMISSION_GRANTED) count++;
                    LLog.i("Permissions granted: " + count);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasEnoughPermits(activity) && withAlert)
                    SleepAndWake.runInMain(() -> queryPermits(activity) );
                else
                    runInit(activity);
                if (listener != null) listener.onAccepted();

            }
        }
        listener = null;
    }

    public static void onActivityResult(Activity activity, int resultCode) {
        switch(resultCode) {
            case BlueMan.REQUEST_ENABLE_BT:
                ToastHelper.showToastLong(R.string.ble_enabled);
                runInit(activity);
                break;
            case ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    LLog.i("Overlays granted = " + Settings.canDrawOverlays(activity));
                }
                break;
            case ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    LLog.i("Write settings = " + Settings.System.canWrite(activity));
                }
                break;
        }
    }

    private static PermitAcceptedListener listener = null;

    @SuppressWarnings("unused")
    public static void askPermission(Context context, String[] permissions) {
        SensezLib.listener = null;
        Intent intent = new Intent(context, NullActivity.class);
        intent.putExtra(PERMISSIONS, permissions);
        context.startActivity(intent);
    }

    @SuppressWarnings("unused")
    public static void askPermission(Context context, String[] permissions, PermitAcceptedListener listener) {
        SensezLib.listener = listener;
        Intent intent = new Intent(context, NullActivity.class);
        intent.putExtra(PERMISSIONS, permissions);
        context.startActivity(intent);
    }



}
