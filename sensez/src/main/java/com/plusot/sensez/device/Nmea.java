package com.plusot.sensez.device;

public enum Nmea {
    GPGGA("$GPGGA"),
    GNGGA("$GNGGA"),
    GPGSV("$GPGSV");
    private final String label;

    Nmea(final String label) {
        this.label = label;
    }

    public static Nmea fromString(String value) {
        for (Nmea nmea : Nmea.values()) if (nmea.label.equals(value)) return nmea;
        return null;
    }
}