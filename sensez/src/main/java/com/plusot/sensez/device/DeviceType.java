package com.plusot.sensez.device;

import android.bluetooth.BluetoothDevice;

import com.plusot.sensez.internal.InternalDevice;

import java.util.EnumSet;

public enum DeviceType {
    TYPE_UNKNOWN,
    ANT_BIKE_POWER,
    ANT_CONTROLLABLE_DEVICE,
    ANT_FITNESS_EQUIPMENT,
    ANT_BLOOD_PRESSURE,
    ANT_GEOCACHE,
    ANT_ENVIRONMENT,
    ANT_WEIGHT_SCALE,
    ANT_HEARTRATE,
    ANT_BIKE_SPDCAD,
    ANT_BIKE_CADENCE,
    ANT_BIKE_SPD,
    ANT_STRIDE_SDM,
    BLUETOOTH_TYPE_CLASSIC,
    BLUETOOTH_TYPE_LE,
    BLUETOOTH_DUAL,
    INTERNAL_GPS,
    INTERNAL_STEP_SENSOR,
    INTERNAL_CLOCK,
    INTERNAL_ACCELEROMETER,
    INTERNAL_MAGNETIC_SENSOR,
    INTERNAL_PROXIMITY_SENSOR,
    INTERNAL_BATTERY_SENSOR,
    INTERNAL_MEMORY_SENSOR,
    INTERNAL_AIR_PRESSURE_SENSOR,
    INTERNAL_ORIENTATION_SENSOR,
    INTERNAL_TEMPERATURE_SENSOR,
    ;

    public boolean isInternal() {
        switch(this) {
            case TYPE_UNKNOWN:
                return false;
            case ANT_BIKE_POWER:
                return false;
            case ANT_CONTROLLABLE_DEVICE:
                return false;
            case ANT_FITNESS_EQUIPMENT:
                return false;
            case ANT_BLOOD_PRESSURE:
                return false;
            case ANT_GEOCACHE:
                return false;
            case ANT_ENVIRONMENT:
                return false;
            case ANT_WEIGHT_SCALE:
                return false;
            case ANT_HEARTRATE:
                return false;
            case ANT_BIKE_SPDCAD:
                return false;
            case ANT_BIKE_CADENCE:
                return false;
            case ANT_BIKE_SPD:
                return false;
            case ANT_STRIDE_SDM:
                return false;
            case BLUETOOTH_TYPE_CLASSIC:
                return false;
            case BLUETOOTH_TYPE_LE:
                return false;
            case BLUETOOTH_DUAL:
                return false;
            case INTERNAL_GPS:
                return true;
            case INTERNAL_STEP_SENSOR:
                return true;
            case INTERNAL_CLOCK:
                return true;
            case INTERNAL_ACCELEROMETER:
                return true;
            case INTERNAL_MAGNETIC_SENSOR:
                return true;
            case INTERNAL_PROXIMITY_SENSOR:
                return true;
            case INTERNAL_BATTERY_SENSOR:
                return true;
            case INTERNAL_MEMORY_SENSOR:
                return true;
            case INTERNAL_AIR_PRESSURE_SENSOR:
                return true;
            case INTERNAL_ORIENTATION_SENSOR:
                return true;
            case INTERNAL_TEMPERATURE_SENSOR:
                return true;
        }
        return false;
    }

    public boolean isANT() {
        switch(this) {
            case TYPE_UNKNOWN:
                return false;
            case ANT_BIKE_POWER:
                return true;
            case ANT_CONTROLLABLE_DEVICE:
                return true;
            case ANT_FITNESS_EQUIPMENT:
                return true;
            case ANT_BLOOD_PRESSURE:
                return true;
            case ANT_GEOCACHE:
                return true;
            case ANT_ENVIRONMENT:
                return true;
            case ANT_WEIGHT_SCALE:
                return true;
            case ANT_HEARTRATE:
                return true;
            case ANT_BIKE_SPDCAD:
                return true;
            case ANT_BIKE_CADENCE:
                return true;
            case ANT_BIKE_SPD:
                return true;
            case ANT_STRIDE_SDM:
                return true;
            case BLUETOOTH_TYPE_CLASSIC:
                return false;
            case BLUETOOTH_TYPE_LE:
                return false;
            case BLUETOOTH_DUAL:
                return false;
            case INTERNAL_GPS:
                return false;
            case INTERNAL_STEP_SENSOR:
                return false;
            case INTERNAL_CLOCK:
                return false;
            case INTERNAL_ACCELEROMETER:
                return false;
            case INTERNAL_MAGNETIC_SENSOR:
                return false;
            case INTERNAL_PROXIMITY_SENSOR:
                return false;
            case INTERNAL_BATTERY_SENSOR:
                return false;
            case INTERNAL_MEMORY_SENSOR:
                return false;
            case INTERNAL_AIR_PRESSURE_SENSOR:
                return false;
            case INTERNAL_ORIENTATION_SENSOR:
                return false;
            case INTERNAL_TEMPERATURE_SENSOR:
                return false;
        }
        return false;
    }



    @Override
    public String toString() {
        switch(this) {
            case ANT_BIKE_POWER:
                return "ANT Bike Power";
            case ANT_CONTROLLABLE_DEVICE:
                return "ANT Controllable";
            case ANT_FITNESS_EQUIPMENT:
                return "ANT Fitness";
            case ANT_BLOOD_PRESSURE:
                return "ANT Blood Pressure";
            case ANT_GEOCACHE:
                return "ANT Geocache";
            case ANT_ENVIRONMENT:
                return "ANT Environment";
            case ANT_WEIGHT_SCALE:
                return "ANT Weight scale";
            case ANT_HEARTRATE:
                return "ANT Heart rate";
            case ANT_BIKE_SPDCAD:
                return "ANT Speed Cadence";
            case ANT_BIKE_CADENCE:
                return "ANT Cadence";
            case ANT_BIKE_SPD:
                return "ANT Speed";
            case ANT_STRIDE_SDM:
                return "ANT Stride";
            case BLUETOOTH_TYPE_CLASSIC:
                return "Bluetooth Classic";
            case BLUETOOTH_TYPE_LE:
                return "Bluetooth LE";
            case BLUETOOTH_DUAL:
                return "Bluetooth DUAL";
            case INTERNAL_GPS:
                return "GPS";
            case INTERNAL_CLOCK:
                return "Clock";
            case INTERNAL_ACCELEROMETER:
                return "Accelerometer";
            case INTERNAL_MAGNETIC_SENSOR:
                return "Magnetic sensor";
            case INTERNAL_PROXIMITY_SENSOR:
                return "Proximity sensor";
            case INTERNAL_BATTERY_SENSOR:
                return "Battery sensor";
            case INTERNAL_AIR_PRESSURE_SENSOR:
                return "Air pressure Sensor";
            case INTERNAL_ORIENTATION_SENSOR:
                return "Orientation Sensor";
            case INTERNAL_MEMORY_SENSOR:
                return "Memory Sensor";
            case INTERNAL_STEP_SENSOR:
                return "Step Sensor";
            case INTERNAL_TEMPERATURE_SENSOR:
                return "Temperature Sensor";
            default:
            case TYPE_UNKNOWN:
                return "?";
        }
    }

    public static DeviceType fromBluetoothDeviceTypeInt(int value) {
        switch (value) {
            case BluetoothDevice.DEVICE_TYPE_CLASSIC: return BLUETOOTH_TYPE_CLASSIC;
            case BluetoothDevice.DEVICE_TYPE_DUAL: return BLUETOOTH_DUAL;
            case BluetoothDevice.DEVICE_TYPE_LE: return BLUETOOTH_TYPE_LE;
            default:
            case BluetoothDevice.DEVICE_TYPE_UNKNOWN: return TYPE_UNKNOWN;
        }
    }

    public static DeviceType fromAntType(com.dsi.ant.plugins.antplus.pcc.defines.DeviceType value) {
        switch (value) {
            case BIKE_POWER: return ANT_BIKE_POWER;
            case CONTROLLABLE_DEVICE: return ANT_CONTROLLABLE_DEVICE;
            case FITNESS_EQUIPMENT: return ANT_FITNESS_EQUIPMENT;
            case BLOOD_PRESSURE: return ANT_BLOOD_PRESSURE;
            case GEOCACHE: return ANT_GEOCACHE;
            case ENVIRONMENT: return ANT_ENVIRONMENT;
            case WEIGHT_SCALE: return ANT_WEIGHT_SCALE;
            case HEARTRATE: return ANT_HEARTRATE;
            case BIKE_SPDCAD: return ANT_BIKE_SPDCAD;
            case BIKE_CADENCE: return ANT_BIKE_CADENCE;
            case BIKE_SPD: return ANT_BIKE_SPD;
            case STRIDE_SDM: return ANT_STRIDE_SDM;
            default:
            case UNKNOWN: return TYPE_UNKNOWN;
        }
    }

    public static DeviceType fromInternalType(InternalDevice.InternalType type) {
        switch(type) {
            case GPS:
                return INTERNAL_GPS;
            case CLOCK:
                return INTERNAL_CLOCK;
            case ACCELEROMETER:
                return INTERNAL_ACCELEROMETER;
            case MAGNETIC_SENSOR:
                return INTERNAL_MAGNETIC_SENSOR;
            case PROXIMITY_SENSOR:
                return INTERNAL_PROXIMITY_SENSOR;
            case BATTERY_SENSOR:
                return INTERNAL_BATTERY_SENSOR;
            case AIR_PRESSURE_SENSOR:
                return INTERNAL_AIR_PRESSURE_SENSOR;
            case ORIENTATION_SENSOR:
                return INTERNAL_ORIENTATION_SENSOR;
            case MEMORY_SENSOR:
                return INTERNAL_MEMORY_SENSOR;
            case TEMPERATURE_SENSOR:
                return INTERNAL_TEMPERATURE_SENSOR;
            case STEP_SENSOR:
                return INTERNAL_STEP_SENSOR;
            default:
                return TYPE_UNKNOWN;
        }
    }

    public static DeviceType fromString(String value) {
        if (value == null) return TYPE_UNKNOWN;
        for (DeviceType type: values()) {
            if (type.name().equalsIgnoreCase(value)) return type;
        }
        return TYPE_UNKNOWN;
    }

    public static EnumSet<DeviceType> fromStrings(String[] values) {

        EnumSet<DeviceType> set = EnumSet.noneOf(DeviceType.class);
        if (values == null) return set;
        for (String value: values) {
            DeviceType type = fromString(value);
            if (type != TYPE_UNKNOWN) set.add(type);
        }
        return set;
    }
}
