package com.plusot.sensez.device;

public enum NmeaFixQuality {
        INVALID(0),
        GPS_FIX(1),
        DGPS_FIX(2),
        PPS_FIX(3),
        RTK(4),
        RTK_FLOAT(5),
        ESTIMATED(6),
        MANUAL_INPUT(7),
        SIMULATION(8);

        final int id;
        NmeaFixQuality(final int id) {
            this.id = id;
        }

        public static NmeaFixQuality fromId(int id) {
            return values()[id % values().length];
        }
     }