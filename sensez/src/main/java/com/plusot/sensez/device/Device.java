package com.plusot.sensez.device;

import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

import java.util.HashSet;
import java.util.Set;

public abstract class Device {
    private final Set<Listener> listeners = new HashSet<>();
    private Data data = new Data();
    private static Data allData = new Data();
    protected boolean singleCall = false;
    private static boolean pausing = true;
    protected String manufacturer = "";
    private Object listenerSyncObj = new Object();

    public enum StateInfo {
        CONNECTED,
        DISCONNECTED,
        COMS,
        DEAD,
        CLOSED,
        SEARCHING,
        TRACKTING,
        PROCESSING_REQUEST,
        UNRECOGNIZED,
        ;
        public static StateInfo fromDeviceState(DeviceState deviceState) {
            switch (deviceState) {
                case DEAD:
                    return DEAD;
                case CLOSED:
                    return CLOSED;
                case SEARCHING:
                    return SEARCHING;
                case TRACKING:
                    return TRACKTING;
                case PROCESSING_REQUEST:
                    return PROCESSING_REQUEST;
                default:
                case UNRECOGNIZED:
                    return UNRECOGNIZED;
            }
        }
    }

    public interface Listener {
        void onDeviceState(Device device, StateInfo state);
        void onDeviceData(Device device, Data data);
        void onCommandDone(Device device, String commandId, byte[] data, boolean success, boolean closeOnDone);
    }

    public Device(Listener listener) {
        synchronized(listenerSyncObj) {
            this.listeners.add(listener);
        }
    }

    public void close() {
        synchronized(listenerSyncObj) {
            listeners.clear();
        }
    }

    public static void setPausing(boolean pausing) {
        if (Man.debug) LLog.i("Device pausing = " + pausing);
        Device.pausing = pausing;
    }

    public static boolean isPausing() {
        return pausing;
    }

    protected void fireState(final StateInfo state) {
        SleepAndWake.runInMain(() -> {
            synchronized(listenerSyncObj) {
                for (Listener listener : listeners) listener.onDeviceState(Device.this, state);
            }
        }, "Device.fireState");
    }

    protected void fireCommandDone(final String commandId,final byte[] data, final boolean success) {
        SleepAndWake.runInMain(() -> {
            synchronized(listenerSyncObj) {
                for (Listener listener: listeners)  listener.onCommandDone(Device.this, commandId, data, success, singleCall);
            }
        }, "Device.fireCommandDone");
    }

    protected boolean fireData(final Data data) {
        return fireData(data, true);
    }

    protected boolean fireData(final Data data, final boolean checkEqual) {
        if (pausing) return false;
        if (data == null || !data.hasData()) return false;
        //if (data.hasDataType(DataType.HEART_RATE)) LLog.i("Data = " + data);
        allData.merge(data);
        if (checkEqual && this.data.mergeCheckEqual(data, false)) {
            //if (DEBUG) LLog.i("Equal data: " + data.toString());
            return false;
        }

        synchronized(listenerSyncObj) {
            for (Listener listener : listeners) listener.onDeviceData(Device.this, data);
        }
        return true;
    }

    public static double getDoubleValue(DataType type) {
        return allData.getDouble(null, type);
    }

    public abstract String getAddress();

    public String getManufacturer() {
        return manufacturer;
    }
}
