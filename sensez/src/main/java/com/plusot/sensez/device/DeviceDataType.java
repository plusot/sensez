package com.plusot.sensez.device;

import com.plusot.util.data.DataType;

/**
 * Created by peet on 1/6/16.
 */
public class DeviceDataType {
    private final ScannedDevice device;
    private final DataType dataType;

    public DeviceDataType(ScannedDevice device, DataType dataType) {
        this.device = device;
        this.dataType = dataType;
    }

    public ScannedDevice getDevice() {
        return device;
    }

    public DataType getDataType() {
        return dataType;
    }

    @Override
    public String toString() {
        return  dataType.toProperString() + " (" + device.getName() + ')';
    }


    public String toAddressDataTypeString() {
        return  device.getAddress() + "_" + dataType.toProperString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof DeviceDataType)) return false;

        DeviceDataType that = (DeviceDataType) o;

        if (!device.getAddress().equals(that.device.getAddress())) {
            //if (dataType == DataType.TIME_ACTIVITY) LLog.i("Address different");
            return false;
        }
        boolean result = dataType == that.dataType;
        //if (!result && dataType == DataType.TIME_ACTIVITY) LLog.i("Datatype different");
        return result;
    }

    @Override
    public int hashCode() {
        int result = device.getAddress().hashCode();
        return DataType.values().length * result + dataType.ordinal();
    }

    public int getViewNr() {
        return device.getDataTypeView(dataType);
    }
}
