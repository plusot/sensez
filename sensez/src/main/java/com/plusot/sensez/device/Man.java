package com.plusot.sensez.device;

import android.os.Build;

import com.plusot.sensez.ant.AntMan;
import com.plusot.sensez.bluetooth.BlueMan;
import com.plusot.sensez.internal.ClockDevice;
import com.plusot.sensez.internal.InternalMan;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class Man {
    public static boolean debug = false;
    private static Man instance = null;
    private static final Object instanceLock = new Object();
    private Set<ScanManager> mgrs = new HashSet<>();
    private static long SCAN_PERIOD = 30000;
    private static long NEXT_SCAN = 5000;
    private Set<Listener> listeners = new HashSet<>();
    private String[] whiteList = null;
    private List<ScannedDevice> deviceList = new ArrayList<>();
    private Map<String, ScannedDevice> deviceMap = new HashMap<>();
    private static final Object listLock = new Object();
    private boolean listDirty = false;
    private static boolean mayShowNew = false;
    private static boolean maySetShowNew = false;

    private static Data currentData = new Data();
    private static Data maxValues = new Data();
    private static Data minValues = new Data();
    private static Data avgBaseValues = new Data();
    private static Data baseValues = new Data();

    private static EnumSet<ScanManager.ScanType> scansActive = EnumSet.noneOf(ScanManager.ScanType.class);
    private Thread runner = null;
    private Thread scanner = null;
    private static LinkedBlockingQueue<FireData> fireQueue = new LinkedBlockingQueue<>();
    private static LinkedBlockingQueue<FireData> ashQueue = new LinkedBlockingQueue<>();
    private static ExternalScanner externalScanner = null;

    public interface Listener {
        void onDeviceData(ScannedDevice device, Data data, boolean isNew);
        void onDeviceScanned(ScannedDevice device, boolean isNew);
        void onDeviceState(ScannedDevice device, Device.StateInfo state);
        void onScanning(ScanManager.ScanType scanType, boolean on);
    }

    public interface ExternalScanner {
        void startScan();
        void stopScan();
    }

    private enum FireDataType {
        STATE,
        SCANNED,
        SCANNING,
        DATA,
        ;
    }
    private static class FireData {
        final FireDataType type;
        final ScannedDevice device;
        final Data data;
        final boolean isNew;
        final ScanManager.ScanType scanType;
        final Device.StateInfo state;

        private FireData(FireDataType type, ScannedDevice device, Data data, boolean isNew, ScanManager.ScanType scanType, Device.StateInfo state) {
            this.type = type;
            this.device = device;
            this.data = data;
            this.isNew = isNew;
            this.scanType = scanType;
            this.state = state;
        }

        private FireData(ScannedDevice device, Data data, boolean isNew) {
            this(FireDataType.DATA, device, data, isNew, null, null);
        }

        private FireData(ScannedDevice device, boolean isNew) {
            this(FireDataType.SCANNED, device, null, isNew, null, null);
        }

        private FireData(ScannedDevice device, Device.StateInfo state) {
            this(FireDataType.STATE, device, null, true, null, state);
        }

        private FireData(ScanManager.ScanType scanType, Device.StateInfo state) {
            this(FireDataType.SCANNING, null, null, true, scanType, state);
        }
    }

    private Man() {
        runner = new Thread(() -> {
            while (Globals.runMode.isRun()) {
                FireData fire;
                if (ashQueue.isEmpty() && (mayShowNew || maySetShowNew)) {
                    mayShowNew = true;
                    fire = fireQueue.poll();
                } else
                    fire = ashQueue.poll();

                if (fire == null) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        if (Man.debug) LLog.i("Interrupted");
                    }
                } else {
                    Listener[] listenerList;
                    synchronized (listLock) {
                        listenerList = listeners.toArray(new Listener[0]);
                    }
                    for (Listener listener : listenerList) switch (fire.type) {
                        case DATA:
                            listener.onDeviceData(fire.device, fire.data, fire.isNew);
                            break;
                        case STATE:
                            listener.onDeviceState(fire.device, fire.state);
                            break;
                        case SCANNED:
                            listener.onDeviceScanned(fire.device, fire.isNew);
                            break;
                        case SCANNING:
                            if (fire.state == Device.StateInfo.SEARCHING)
                                listener.onScanning(fire.scanType, true);
                            else
                                listener.onScanning(fire.scanType, false);
                            break;
                    }
                    switch (fire.type) {
                        case DATA:
                            onDeviceData(fire.device, fire.data); //, fire.isNew);
                            break;
                        case SCANNED:
                            onDeviceScanned(fire.device, fire.isNew);
                            break;
                    }
                }
            }
            if (Man.debug) LLog.i(Thread.currentThread().getName() + " stopped");
        });
        runner.setName("Man.fireThread");
        runner.start();
    }


    private static synchronized Man getInstance() {
        if (instance == null  && Globals.runMode.isRun()) instance = new Man();
        if (instance == null) LLog.i("Could not get Instance as run mode = " + Globals.runMode);
        return instance;
    }

    @SuppressWarnings("unused")
    public static void setExternalScanner(ExternalScanner externalScanner) {
        Man.externalScanner = externalScanner;
    }

    public static synchronized void stopInstance() {
        if (instance == null) return;
        if (Man.debug) LLog.i("Stopping Instance");
        instance.close();
        instance = null;
        fireQueue.clear();
        ashQueue.clear();
        if (Man.debug) LLog.i("Stopped Instance");
    }

    private void _init(boolean showNew) {
        if (instance == null) return;
        mayShowNew = showNew;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            PreferenceKey.USE_BLE.set(false);
        }
        if ((PreferenceKey.USE_BLE.isTrue() || PreferenceKey.USE_BT.isTrue())) {
            mgrs.add(BlueMan.getInstance());
        }
        if (PreferenceKey.USE_ANT.isTrue()) mgrs.add(AntMan.getInstance());
        if (PreferenceKey.USE_INTERNAL.isTrue()) mgrs.add(InternalMan.getInstance());
        scanner = new Thread(() -> {
            long lastReduction = 0L;
            ScanManager.ScanType type = ScanManager.ScanType.values()[0];
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                LLog.i("Interrupted");
            }
            int iNoScan = 0;
            while (Globals.runMode.isRun()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LLog.i("Interrupted");
                }
                //if (mayScan || mayNotScanCount > 120) {
                //    mayNotScanCount = 0;
                if (System.currentTimeMillis() - lastReduction > 30000) {
                    //deviceList.forEach((v) -> v.reduceScanCount() );
                    lastReduction = System.currentTimeMillis();
                    synchronized (listLock) {
                        for (ScannedDevice dev : deviceList) dev.reduceScanCount();
                    }
                }
                if (scansActive.isEmpty()) {
                    iNoScan = 0;
                    switch (type) {
                        case ANT_SCAN:
                            if (PreferenceKey.USE_ANT.isTrue()) {
                                ScanManager instance = AntMan.getInstance();
                                if (instance != null && instance.startScan(ScanManager.ScanType.ANT_SCAN))
                                    scansActive.add(ScanManager.ScanType.ANT_SCAN);
                            }
                            break;
                        case BT_SCAN:
                            if (PreferenceKey.USE_BT.isTrue()) {
                                ScanManager instance = BlueMan.getInstance();
                                if (instance != null && instance.startScan(ScanManager.ScanType.BT_SCAN))
                                    scansActive.add(ScanManager.ScanType.BT_SCAN);
                            }
                            break;
                        case BLE_SCAN:
                            if (PreferenceKey.USE_BLE.isTrue()) {
                                ScanManager instance = BlueMan.getInstance();
                                if (instance != null && instance.startScan(ScanManager.ScanType.BLE_SCAN))
                                    scansActive.add(ScanManager.ScanType.BLE_SCAN);
                            }
                            break;
                        case EXTERNAL_SCAN:
                            //if (PreferenceKey.USE_EXTERNAL.isTrue() && ) {
                            if (externalScanner != null) {
                                externalScanner.startScan();
                                scansActive.add(ScanManager.ScanType.EXTERNAL_SCAN);
                            }
                            break;

                    }
                    type = ScanManager.ScanType.values()[(type.ordinal() + 1) % ScanManager.ScanType.values().length];
                } else {
                    iNoScan++;
                    if (iNoScan > 120) {
                        LLog.e("!!!!! Not scanning for too long time. Closing pending scans !!!!!");
                        iNoScan = 0;
                        for (ScanManager.ScanType typeToStop : scansActive) {
                            switch (typeToStop) {
                                case ANT_SCAN:
                                    if (PreferenceKey.USE_ANT.isTrue()) {
                                        ScanManager instance = AntMan.getInstance();
                                        if (instance != null)
                                            instance.stopScan(ScanManager.ScanType.ANT_SCAN);
                                    }
                                    break;
                                case BT_SCAN:
                                    if (PreferenceKey.USE_BT.isTrue()) {
                                        ScanManager instance = BlueMan.getInstance();
                                        if (instance != null)
                                            instance.stopScan(ScanManager.ScanType.BT_SCAN);
                                    }
                                    break;
                                case BLE_SCAN:
                                    if (PreferenceKey.USE_BLE.isTrue()) {
                                        ScanManager instance = BlueMan.getInstance();
                                        if (instance != null)
                                            instance.stopScan(ScanManager.ScanType.BLE_SCAN);
                                    }
                                    break;
                                case EXTERNAL_SCAN:
                                    //if (PreferenceKey.USE_EXTERNAL.isTrue() && ) {
                                    if (externalScanner != null) externalScanner.stopScan();
                                    scansActive.add(ScanManager.ScanType.EXTERNAL_SCAN);
                                    break;

                            }
                        }
                        scansActive.clear();
                    }
                }
//                } else {
//                    mayNotScanCount++;
//                }
            }
            LLog.i(Thread.currentThread().getName() + " stopped");
        });
        scanner.setName("Man.scannerThread");
        scanner.start();

    }

    public static void add(ScanManager mgr) {
        if (getInstance() != null) synchronized (instanceLock){
            instance.mgrs.add(mgr);

        }
    }

    public static void remove(ScanManager mgr) {
        if (instance != null)  synchronized (instanceLock) {
            instance.mgrs.remove(mgr);
        }
    }

    public static void init(Listener listener, boolean showNew) {
        if (getInstance() != null) {
            instance._init(showNew);
            if (showNew) Device.setPausing(false);
        }
        if (listener != null) addListener(listener);

    }

    private void _addListener(Listener listener) {
        synchronized (listLock) {
            listeners.add(listener);
        }
    }

    public static void addListener(Listener listener) {
        if (getInstance() != null) instance._addListener(listener);
    }

    private void _removeListener(Listener listener) {
        synchronized (listLock) {
            listeners.remove(listener);
        }
    }

    public static void removeListener(Listener listener) {
        if (getInstance() != null) instance._removeListener(listener);
    }


    private void close() {
        if (Man.debug) LLog.i("Man Closing");

        synchronized (listLock) {
            listeners.clear();
        }
        if (runner != null) runner.interrupt();
        if (scanner != null) scanner.interrupt();
        runner = null;
        scanner = null;

        for (ScanManager mgr: mgrs) {
            if (Man.debug) LLog.i("Closing: " + mgr.getClass());
            mgr.close();
        }
        mgrs.clear();
        if (Man.debug) LLog.i("Man Closed");
    }

    private boolean firstTimeGetDeviceList = true;

    private Collection<ScannedDevice> _getScannedDevices(boolean sort) {
//        synchronized (listLock) {
        List<ScannedDevice> resultList = new ArrayList<>();
        if (!firstTimeGetDeviceList &&(!listDirty || !sort))  {
            synchronized (listLock) {
                resultList.addAll(deviceList);
            }
            return resultList;
        }
        listDirty = false;
        if (firstTimeGetDeviceList) {
            firstTimeGetDeviceList = false;
            List<ScannedDevice> list = new ArrayList<>();
            Collection<ScannedDevice> partial;
            for (ScanManager mgr : mgrs) if ((partial = mgr.getScannedDevices()) != null) {
                list.addAll(partial);
                synchronized (listLock) {
                    for (ScannedDevice device : partial)
                        deviceMap.put(device.getAddress(), device);
                }
            }
            synchronized (listLock) {
                deviceList = list;
            }
        }
        if (sort) try {
            synchronized (listLock) {
                Collections.sort(deviceList, (lhs, rhs) -> lhs.getName().compareToIgnoreCase(rhs.getName()));
            }
        } catch (IllegalArgumentException e) {
            LLog.e("Could not sort list: " + e.getMessage());
        }
        synchronized (listLock) {
            resultList.addAll(deviceList);
        }
        return resultList;
    }


    private ScannedDevice _getDeviceByAddress(String address) {
        ScannedDevice device;
        if (listDirty) _getScannedDevices(false);
        synchronized (listLock) {
            device =  deviceMap.get(address);
        }
        return device;
    }

    private ScannedDevice _getDeviceByName(String name) {
        ScannedDevice device;
        if (listDirty) _getScannedDevices(false);
        synchronized (listLock) {
            for (String address: deviceMap.keySet()) {
                device = deviceMap.get(address);
                if (device!= null && (device.getName().equalsIgnoreCase(name) || (device.getName().startsWith(name) && name.endsWith(address)))) return device;
            }
        }
        return null;
    }

    public static Collection<ScannedDevice> getScannedDevices(boolean sort) {
        if (instance != null) return instance._getScannedDevices(sort);
        return new ArrayList<>();
    }

    private List<DeviceDataType> _getDeviceDataTypes(boolean sort) {
        List<DeviceDataType> deviceDataTypes = new ArrayList<>();
        Collection<ScannedDevice> list = _getScannedDevices(sort);
        for (ScannedDevice dev: list) {
            if (dev.isSelected() || dev.isBeacon()) {
                EnumSet<DataType> types = dev.getSupportedDataTypes();
                for (DataType type: types) deviceDataTypes.add(new DeviceDataType(dev, type));
            }
        }
        if (sort) Collections.sort(deviceDataTypes, (lhs, rhs) -> lhs.toString().compareToIgnoreCase(rhs.toString()));
        return deviceDataTypes;
    }

    public static List<DeviceDataType> getDeviceDataTypes(boolean sort) {
        if (instance != null) return instance._getDeviceDataTypes(sort);
        return new ArrayList<>();
    }

    public static ScannedDevice getDeviceByAddress(String address) {
        if (instance != null) return instance._getDeviceByAddress(address);
        return null;
    }

    public static ScannedDevice getDeviceByName(String name) {
        if (instance != null) return instance._getDeviceByName(name);
        return null;
    }

    private List<DeviceDataType> _getViewCheckedDeviceDataTypes(int viewId, boolean sort) {
        List<DeviceDataType> viewCheck = new ArrayList<>();
        Collection<ScannedDevice> list = _getScannedDevices(sort);
        for (ScannedDevice dev: list) if (dev.isSelected() || dev.isBeacon()) {
            DataType[] types = dev.getDataTypes();
            for (DataType type: types) if (dev.getDataTypeView(type) == viewId || viewId == -1) {
                //if (viewId == Globals.VIEW_TO_WATCH ) LLog.i("Adding " + type + " to view");
                viewCheck.add(new DeviceDataType(dev, type));
            }
        }
        if (sort) Collections.sort(viewCheck, (lhs, rhs) -> lhs.toString().compareToIgnoreCase(rhs.toString()));
        return viewCheck;
    }

    private Set<DeviceDataType> _getViewCheckedDeviceDataTypes(int viewId, Set<DeviceDataType> reUse) {
        Set<DeviceDataType> viewCheck = reUse;
        Collection<ScannedDevice> list = _getScannedDevices(false);
        for (ScannedDevice dev: list) if (dev.isSelected() || dev.isBeacon()) {
            DataType[] types = dev.getDataTypes();
            for (DataType type: types) if (dev.getDataTypeView(type) == viewId || viewId == -1) {
                //if (viewId == Globals.VIEW_TO_WATCH ) LLog.i("Adding " + type + " to view");
                viewCheck.add(new DeviceDataType(dev, type));
            }
        }
        return viewCheck;
    }

    public static List<DeviceDataType> getViewCheckedDeviceDataTypes(int viewId) {
        if (instance != null) return instance._getViewCheckedDeviceDataTypes(viewId, false);
        return new ArrayList<>();
    }

    public static Set<DeviceDataType> getViewCheckedDeviceDataTypes(int viewId, Set<DeviceDataType> reUse) {
        if (instance != null) return instance._getViewCheckedDeviceDataTypes(viewId, reUse);
        return reUse;
    }

    private void onDeviceData(final ScannedDevice device, final Data data){ //, final boolean isNew) {
        currentData.merge(data);

        String address = device.getAddress();
        for (DataType type : data.getDataTypes()) if (data.hasDataType(type)) {
            if (type.isGraphable()) {
                if (maxValues.hasDataType(type)) {
                    Boolean result = data.greater(address, type, maxValues);
                    if (result != null && result) maxValues.add(address, type, data.getValueTime(address, type));
                } else
                    maxValues.add(address, type, data.getValueTime(address, type));
                if (minValues.hasDataType(type)) {
                    Boolean result = data.smaller(address, type, minValues);
                    if (result != null && result) minValues.add(address, type, data.getValueTime(address, type));
                } else
                    minValues.add(address, type, data.getValueTime(address, type));
                if (!baseValues.hasDataType(type)) baseValues.add(address, type, data.getValueTime(address, type));
                double d = data.getDouble(address, type);
                if (!Double.isNaN(d)) {
                    double dOld = avgBaseValues.getDouble(address, type);
                    long prevTime = avgBaseValues.getTime(address, type);
                    long actTime = data.getTime(address, type);
                    if (Double.isNaN(dOld)) dOld = 0;
                    avgBaseValues.add(address, type, d * (actTime - prevTime) + dOld, actTime);

                }
            }

        }
    }

    public static Data getAvg(String sensorId, DataType type) {
        double d = avgBaseValues.getDouble(sensorId, type);
        if (Double.isNaN(d)) return null;
        //if (activeTime == null)
        double devider = avgBaseValues.getTime(sensorId, type) - baseValues.getTime(sensorId, type);
        if (devider <= 0) return null;
        return new Data().add(sensorId, type, avgBaseValues.getDouble(sensorId, type) / devider);
//        else
//            return new Data().add(sensorId, type, avgBaseValues.getDouble(sensorId, type) / activeTime);
    }

    public static double getDouble(String sensorId, DataType type) {
        double d = currentData.getDouble(sensorId, type);
        if (Double.isNaN(d)) return 0.0;
        return d;
    }

    public static long getLong(String sensorId, DataType type) {
        Long l = currentData.getLong(sensorId, type);
        if (l == null) return 0L;
        return l;
    }

    public static long getLong(String sensorId, DataType type, long defaultValue) {
        Long l = currentData.getLong(sensorId, type);
        if (l == null) return defaultValue;
        return l;
    }

    public static int getInt(String sensorId, DataType type) {
        return currentData.getInt(sensorId, type, 0);
    }

    public static Data getMax(String sensorId, DataType type) {
        return maxValues.clone(sensorId, type);
    }
    public static Data getMin(String sensorId, DataType type) {
        return minValues.clone(sensorId, type);
    }

    private void onDeviceScanned(final ScannedDevice device, final boolean isNew) {
//        LLog.i("Scanned: " + device);
        synchronized (listLock) {
            if (deviceMap.get(device.getAddress()) == null) {
                listDirty = true;
                deviceMap.put(device.getAddress(), device);
            }
            if (!deviceList.contains(device)) deviceList.add(device);
        }
    }

    public static void reset() {
        LLog.i("Reset called");
        mayShowNew = false;
        maySetShowNew = false;
        ashQueue.clear();
        fireQueue.clear();
        currentData = new Data();
        maxValues = new Data();
        minValues = new Data();
        avgBaseValues = new Data();
        baseValues = new Data();
        ClockDevice.reset();
    }

    public static void resetTotals() {
        PreferenceKey.TOTAL_TIME.set(0L);
        PreferenceKey.TOTAL_DISTANCE.set(0.0);
    }

    public static void setShowNew(boolean value) {
        LLog.i("Man show new = " + value);

        if (value)
            maySetShowNew = true;
        else
            mayShowNew = false;
    }

    public static boolean isShowNew() {
        return mayShowNew;
    }

    public static void fireData(final ScannedDevice device, final Data data, final boolean isNew) {
        if (isNew)
            fireQueue.offer(new FireData(device, data, true));
        else
            ashQueue.offer(new FireData(device, data, false));
    }

    public static void fireScanning(final ScanManager.ScanType type, boolean active) {
        if (!Globals.runMode.isRun()) return;
//        LLog.i("Scanning " + type + " = " + active);
        if (!active) SleepAndWake.runInMain(() -> {
//                mayStartScan = true;
            scansActive.remove(type);
        }, NEXT_SCAN, "Man.fireScanning");
        if (active)
            fireQueue.offer(new FireData(type, Device.StateInfo.SEARCHING));
        else
            fireQueue.offer(new FireData(type, Device.StateInfo.CLOSED));
    }

    public static void fireScanned(ScannedDevice device, boolean isNew) {
        if (!Globals.runMode.isRun()) return;
        if (isNew)
            fireQueue.offer(new FireData(device, true));
        else
            ashQueue.offer(new FireData(device, false));

    }
    public static void fireState(ScannedDevice device, Device.StateInfo state) {
        if (!Globals.runMode.isRun()) return;
        fireQueue.offer(new FireData(device, state));
    }

    public static boolean hasData() {
        return currentData.hasData();
    }

    public static void setScanPeriods(long scanPeriod, long scanWait) {
        NEXT_SCAN = scanWait;
        SCAN_PERIOD = scanPeriod;
    }

    public static long getScanPeriod() {
        return SCAN_PERIOD;
    }

}
