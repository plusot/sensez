package com.plusot.sensez.device;

/**
 * Created by peet on 19/10/16.
 */

public enum CommandType {
    INIT,
    START,
    STOP,
    RESET,

}
