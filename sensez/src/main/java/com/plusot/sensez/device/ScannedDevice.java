package com.plusot.sensez.device;

import androidx.annotation.NonNull;

import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.preferences.PreferenceHelper;
import com.plusot.util.util.Tuple;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ScannedDevice {
    public static boolean readSelectedFromPreferences = true;
    private static final String SELECTED = "Selected";
    public static double REDUCE_fACTOR = 0.95;
    private double scanCount = 0;
    private boolean expanded = false;
    protected String name = null;
    protected Data deviceData = new Data();
    protected EnumSet<DataType> supported = EnumSet.noneOf(DataType.class);
    private Boolean selected = null;
    private static Map<String, Boolean> selectedMap = new HashMap<>();
    private static Map<String, Integer> viewChecked = new HashMap<>();
    private static Map<String, Boolean> speechChecked = new HashMap<>();
    public int reconnectCount = 0;
    private final DeviceType type;
    private int rssi = Integer.MIN_VALUE;
    //private static boolean viewChanged = false;
    private static boolean supportedTypesPersistant = false;
    private static Map<String, List<Integer>> supportedTypes = new HashMap<>();

    public ScannedDevice(DeviceType type) {
        this.type = type;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean value) {
        expanded = value;
    }

    @NonNull
    @Override
    public String toString() {
        return getName();  // + ": " + deviceData.toString();
    }

    public Tuple<String, String> toNameValueString() {
        return deviceData.toNameValueString();
    }

    public Data getData() {
        return deviceData;
    }

    public DataType[] getDataTypes() {
        return deviceData.getDataTypes();
    }

    public boolean hasDataType(DataType type) {
        return deviceData.hasDataType(type);
    }

    public EnumSet<DataType> getSupportedDataTypes() {
        int count = supported.size();
        if (supported.isEmpty()) {
            List<Integer> list = null;
            if (supportedTypesPersistant)
                list = PreferenceHelper.getIntList("Supported_" + getAddress());
            else {
                list = supportedTypes.get("Supported_" + getAddress());
                if (list == null) supportedTypes.put("Supported_" + getAddress(), list = new ArrayList<>());
            }
            if (list != null && list.size() > 0) for (Integer index : list){
                DataType type = DataType.fromInt(index);
                if (type != null) supported.add(type);
            }
            count = supported.size();
        }
        supported.addAll(deviceData.getSupportedDataTypes());
        if (supported.size() > count) {
            List<Integer> list = new ArrayList<>();
            for (DataType type: supported) list.add(type.ordinal());
            PreferenceHelper.setIntList("Supported_" + getAddress(), list);
            if (!supportedTypesPersistant) supportedTypes.put("Supported_" + getAddress(), list);
        }
        return supported;
    }

    public static String hasSelected(String name) {
        if (name == null) return null;
        for (String key: selectedMap.keySet()) {

            if (key.toLowerCase().startsWith(name.toLowerCase()) && selectedMap.get(key) == true) return key;
        }
        return null;
    }

    public long getTimeRead() {
        return deviceData.getTime();
    }

    public void setDataTypeView(DataType type, int val) {
        //viewChanged = true;
        String id = getAddress() + "_" + type.name();
        viewChecked.put(id, val);
        PreferenceHelper.set(id + "_VIEW_CHECK_VAL", val);
    }

    public int getDataTypeView(DataType type) {
        return ScannedDevice.getDataTypeView(getAddress(), type);
    }

    public boolean isDataTypeInView(DataType[] types) {
        if (types == null) return false;
        for (DataType type: types) if (ScannedDevice.getDataTypeView(getAddress(), type) > 0) return true;
        return false;
    }

    private static int getDataTypeView(String address, DataType type) {
        Integer result;
        String id = address + "_" + type.name();

        if ((result = viewChecked.get(id)) == null) {
            viewChecked.put(id, result = PreferenceHelper.get(id + "_VIEW_CHECK_VAL", 0));
//            LLog.i("Getting preference for " + getAddress() + " " + type.name() + " = " + result);

        }
        return result;
    }

    public void setDataTypeSpeech(DataType type, boolean value) {
        String id = getAddress() + "_" + type.name();

        speechChecked.put(id, value);
        PreferenceHelper.set(id + "_SPEECH_CHECKED", value);
    }

    public boolean isDataTypeSpeech(DataType type) {
        return ScannedDevice.isDataTypeSpeech(getAddress(), type);
    }

    private static boolean isDataTypeSpeech(String address, DataType type) {
        Boolean result;
        String id = address + "_" + type.name();

        if ((result = speechChecked.get(id)) == null) {
            speechChecked.put(id, result = PreferenceHelper.get(id + "_SPEECH_CHECKED", false));
        }
        return result;
    }

    public static boolean isDataTypeViewOrSpeech(String address, DataType type) {
        return isDataTypeSpeech(address, type) || (getDataTypeView(address, type) > 0);
    }

    public boolean isSelected() {
        if (selected == null) {
            if (readSelectedFromPreferences) {
                if (getName().startsWith("IB300")) {
                    selected = PreferenceHelper.get(ScannedDevice.class.getSimpleName() + "_" + SELECTED + "_" + getName(), false);
                } else {
                    selected = PreferenceHelper.get(ScannedDevice.class.getSimpleName() + "_" + SELECTED + "_" + getName() + "_" + getAddress(), false);
                    if (!selected) {
                        selected = PreferenceHelper.get(ScannedDevice.class.getSimpleName() + "_" + SELECTED + "_" + getAddress(), false);
                    }
                }
            } else {
                selected = false;
            }
            selectedMap.put(getName() + "_" + getAddress(), selected);
        }
        return selected;
    }

    public void setSelected(boolean selected) {
        if (this.selected == null ||  this.selected != selected) {
            if (getName().startsWith("IB300")) {
                PreferenceHelper.set(ScannedDevice.class.getSimpleName() + "_" + SELECTED + "_" + getName(), selected);
            } else
                PreferenceHelper.set(ScannedDevice.class.getSimpleName() + "_" + SELECTED + "_" + getName() + "_" + getAddress(), selected);
            selectedMap.put(getName() + "_" + getAddress(), selected);
            this.selected = selected;
        }
    }

    public abstract String getName();
    public abstract String getAddress();
    public final DeviceType getDeviceType() { return type; };
    public abstract int getIconId();
    public abstract void close();

    public boolean isBeacon() {
        return false;
    }

    public void incScanCount() { scanCount += 1.0; }
    public double getScanCount() { return scanCount; }
    public void reduceScanCount() { scanCount *= REDUCE_fACTOR; }

    public abstract String getManufacturer();

    public int getRssi() {
        if (rssi == Integer.MIN_VALUE) return 0;
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public static void setSupportedTypesPersistant(boolean usePreferences) {
        ScannedDevice.supportedTypesPersistant = usePreferences;
    }
}
