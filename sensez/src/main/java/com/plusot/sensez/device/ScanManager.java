package com.plusot.sensez.device;

import java.util.Collection;


public interface ScanManager {

    enum ScanType {
        ANT_SCAN,
        BT_SCAN,
        BLE_SCAN,
        EXTERNAL_SCAN,
        ;
    }

    Collection<ScannedDevice> getScannedDevices();

    void close();

    boolean startScan(ScanType type);

    void stopScan(ScanType type);

}
