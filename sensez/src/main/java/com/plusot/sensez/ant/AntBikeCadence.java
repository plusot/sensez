/*
This software is subject to the license described in the License.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
 */

package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc.ICalculatedCadenceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc.IMotionAndCadenceDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc.IRawCadenceDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.BatteryStatus;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusBikeSpdCadCommonPcc.IBatteryStatusReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.ICumulativeOperatingTimeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.IManufacturerAndSerialReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.IVersionAndModelReceiver;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;

import java.math.BigDecimal;
import java.util.EnumSet;

public class AntBikeCadence extends AntDevice {
    PccReleaseHandle<AntPlusBikeCadencePcc> bcReleaseHandle = null;
    AntPlusBikeSpeedDistancePcc bsPcc = null;
    PccReleaseHandle<AntPlusBikeSpeedDistancePcc> bsReleaseHandle = null;

    public AntBikeCadence(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(searchResult, listener);
        LLog.i("Initializing ...");
        resetPcc();
    }

    private void resetPcc() {
        LLog.i("Handling reset");
        //Release the old access if it exists
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
            bcReleaseHandle = null;
        }
        if (bsReleaseHandle != null) {
            bsReleaseHandle.close();
            bsReleaseHandle = null;
        }
        requestAccessToPcc();
    }

    protected void requestAccessToPcc() {
        final Context appContext = Globals.getAppContext();
        if(appContext == null) return;
        boolean isBSC = searchResult.getAntDeviceType().equals(DeviceType.BIKE_SPDCAD);
        bcReleaseHandle = AntPlusBikeCadencePcc.requestAccess(
                appContext,
                searchResult.getAntDeviceNumber(),
                0,
                isBSC,
                resultReceiver,
                deviceStateChangeReceiver);
        // starts the plugins UI search
//            bcReleaseHandle = AntPlusBikeCadencePcc.requestAccess(this, this, mResultReceiver,
//        mDeviceStateChangeReceiver);
    }

    IPluginAccessResultReceiver<AntPlusBikeCadencePcc> resultReceiver = new IPluginAccessResultReceiver<AntPlusBikeCadencePcc>() {
        // Handle the result, connecting to events on success or reporting
        // failure to user.
        @Override
        public void onResultReceived(AntPlusBikeCadencePcc result,
                                     RequestAccessResult resultCode, DeviceState initialDeviceState) {
            onAccessResultReceived(result, resultCode, initialDeviceState, true);

        }
    };

    @Override
    protected void subscribeToEvents() {
        AntPlusBikeCadencePcc bcPcc;
        if (pcc instanceof AntPlusBikeCadencePcc) {
            bcPcc = (AntPlusBikeCadencePcc) pcc;
            final BigDecimal wc = new BigDecimal(PreferenceKey.WHEEL_CIRCUMFERENCE.getDouble());

            bcPcc.subscribeCalculatedCadenceEvent(new ICalculatedCadenceReceiver() {
                @Override
                public void onNewCalculatedCadence(final long estTimestamp,
                                                   final EnumSet<EventFlag> eventFlags, final BigDecimal calculatedCadence) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.CADENCE, calculatedCadence.doubleValue()));
                }
            });

            bcPcc.subscribeRawCadenceDataEvent(new IRawCadenceDataReceiver() {
                @Override
                public void onNewRawCadenceData(final long estTimestamp,
                                                final EnumSet<EventFlag> eventFlags, final BigDecimal timestampOfLastEvent,
                                                final long cumulativeRevolutions) {
                    if (AntMan.debug) LLog.i("\n\tCumulative revolutions: " + cumulativeRevolutions);
                }
            });

            final Context appContext = Globals.getAppContext();
            if (bcPcc.isSpeedAndCadenceCombinedSensor() && appContext!= null) {
                if (AntMan.debug) LLog.i("Trying to subscribe to combined sensor functions");
                bsReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(
                        appContext,
                        bcPcc.getAntDeviceNumber(), 0, true,
                        new IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
                            // Handle the result, connecting to events
                            // on success or reporting failure to user.
                            @Override
                            public void onResultReceived(AntPlusBikeSpeedDistancePcc result,
                                                         RequestAccessResult resultCode,
                                                         DeviceState initialDeviceState) {
                                AntPluginPcc ret = onAccessResultReceived(result, resultCode,
                                        initialDeviceState, false);
                                if (ret != null && ret instanceof AntPlusBikeSpeedDistancePcc) {
                                    bsPcc = (AntPlusBikeSpeedDistancePcc) ret;
                                    bsPcc.subscribeCalculatedSpeedEvent(new CalculatedSpeedReceiver(wc) {
                                        @Override
                                        public void onNewCalculatedSpeed(
                                                long estTimestamp,
                                                EnumSet<EventFlag> eventFlags,
                                                final BigDecimal calculatedSpeed) {
                                            fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.SPEED, calculatedSpeed.doubleValue()));
                                        }
                                    });
                                }

                            }
                        },
                        // Receives state changes and shows it on the
                        // status display line
                        new IDeviceStateChangeReceiver() {
                            @Override
                            public void onDeviceStateChange(final DeviceState newDeviceState) {
                                if (bsPcc != null && AntMan.debug) LLog.i(bsPcc.getDeviceName() + ": " + newDeviceState);
                                fireState(StateInfo.fromDeviceState(newDeviceState));
                                if (newDeviceState == DeviceState.DEAD) bsPcc = null;
                            }
                        }
                );


            } else {
                bcPcc.subscribeCumulativeOperatingTimeEvent(new ICumulativeOperatingTimeReceiver() {
                    @Override
                    public void onNewCumulativeOperatingTime(final long estTimestamp,
                                                             final EnumSet<EventFlag> eventFlags, final long cumulativeOperatingTime) {
                        if (AntMan.debug) LLog.i("Cumulative operating time = " + cumulativeOperatingTime);
                        fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.TIME_ACTIVITY, cumulativeOperatingTime)
                        );
                    }
                });

                bcPcc.subscribeManufacturerAndSerialEvent(new IManufacturerAndSerialReceiver() {
                    @Override
                    public void onNewManufacturerAndSerial(final long estTimestamp,
                                                           final EnumSet<EventFlag> eventFlags, final int manufacturerID,
                                                           final int serialNumber) {
                        fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.MANUFACTURER_NAME_STRING, manufacturer = AntDeviceManufacturer.fromId(manufacturerID).getLabel())
                                .add(getAddress(), DataType.SERIAL_NUMBER_STRING, "" + serialNumber)
                        );

                    }
                });

                bcPcc.subscribeVersionAndModelEvent(new IVersionAndModelReceiver() {
                    @Override
                    public void onNewVersionAndModel(final long estTimestamp,
                                                     final EnumSet<EventFlag> eventFlags, final int hardwareVersion,
                                                     final int softwareVersion, final int modelNumber) {
                        fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.HARDWARE_REVISION_STRING, "" + hardwareVersion)
                                .add(getAddress(), DataType.SOFTWARE_REVISION_STRING, "" + softwareVersion)
                                .add(getAddress(), DataType.MODEL_NUMBER_STRING, "" + modelNumber)
                        );
                    }
                });

                bcPcc.subscribeBatteryStatusEvent(new IBatteryStatusReceiver() {
                    @Override
                    public void onNewBatteryStatus(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                   final BigDecimal batteryVoltage, final BatteryStatus batteryStatus) {
                        fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.BATTERY_LEVEL, batteryVoltage.doubleValue())
                                .add(getAddress(), DataType.BATTERY_INFO, batteryStatus.toString()));
                    }
                });

                bcPcc.subscribeMotionAndCadenceDataEvent(new IMotionAndCadenceDataReceiver() {
                    @Override
                    public void onNewMotionAndCadenceData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                          final boolean isPedallingStopped) {
                        if (AntMan.debug) LLog.i("Pedalling stopped = " + isPedallingStopped);
                    }
                });
            }
        }
    }


    @Override
    public void close() {
        if (bcReleaseHandle != null) bcReleaseHandle.close();
        if (bsReleaseHandle != null) bsReleaseHandle.close();
        bcReleaseHandle = null;
        bsReleaseHandle = null;
        super.close();
    }
}