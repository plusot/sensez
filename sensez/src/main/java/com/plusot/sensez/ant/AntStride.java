/*
This software is subject to the license described in the License.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
 */

package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.ICalorieDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IComputationTimestampReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IDataLatencyReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IInstantaneousCadenceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IInstantaneousSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.ISensorStatusReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IStrideCountReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorHealth;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorLocation;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorUseState;
import com.dsi.ant.plugins.antplus.pcc.defines.BatteryStatus;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.IManufacturerIdentificationReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.IManufacturerSpecificDataReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.IProductInformationReceiver;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.StringUtil;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Connects to Stride Sdm Plugin and receives data
 */
public class AntStride extends AntDevice {

    PccReleaseHandle<AntPlusStrideSdmPcc> releaseHandle = null;

    public AntStride(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Device.Listener listener) {
        super(searchResult, listener);
        handleReset();
    }

    protected void handleReset()
    {
        if(releaseHandle != null) {
            releaseHandle.close();
        }
        requestAccessToPcc();
    }

    protected void requestAccessToPcc() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        LLog.i("Trying to get access to PCC");
        releaseHandle = AntPlusStrideSdmPcc.requestAccess(
                appContext,
                searchResult.getAntDeviceNumber(), 0,
                resultReceiver,
                deviceStateChangeReceiver);

    }
    IPluginAccessResultReceiver<AntPlusStrideSdmPcc> resultReceiver = new IPluginAccessResultReceiver<AntPlusStrideSdmPcc>()
    {
        @Override
        public void onResultReceived(AntPlusStrideSdmPcc result,
                                     RequestAccessResult resultCode, DeviceState initialDeviceState)
        {
            onAccessResultReceived(result, resultCode, initialDeviceState, true);
        }
    };

    @Override
    protected void subscribeToEvents()
    {
        AntPlusStrideSdmPcc sdmPcc;
        if (pcc instanceof AntPlusStrideSdmPcc) {
            sdmPcc = (AntPlusStrideSdmPcc) pcc;

            sdmPcc.subscribeInstantaneousSpeedEvent(new IInstantaneousSpeedReceiver() {
                @Override
                public void onNewInstantaneousSpeed(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final BigDecimal instantaneousSpeed) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.SPEED, instantaneousSpeed.doubleValue()));
                }

            });

            sdmPcc.subscribeInstantaneousCadenceEvent(new IInstantaneousCadenceReceiver() {
                @Override
                public void onNewInstantaneousCadence(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final BigDecimal instantaneousCadence) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.CADENCE, instantaneousCadence.doubleValue()));
                }
            });

            sdmPcc.subscribeDistanceEvent(new IDistanceReceiver() {
                @Override
                public void onNewDistance(final long estTimestamp,
                                          final EnumSet<EventFlag> eventFlags,
                                          final BigDecimal cumulativeDistance) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.DISTANCE, cumulativeDistance.doubleValue()));
                }
            });

            sdmPcc.subscribeStrideCountEvent(new IStrideCountReceiver() {
                @Override
                public void onNewStrideCount(final long estTimestamp,
                                             final EnumSet<EventFlag> eventFlags, final long cumulativeStrides) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.STEPS, cumulativeStrides));
                }
            });

            sdmPcc.subscribeComputationTimestampEvent(new IComputationTimestampReceiver() {
                @Override
                public void onNewComputationTimestamp(final long estTimestamp,
                                                      final EnumSet<EventFlag> eventFlags,
                                                      final BigDecimal timestampOfLastComputation) {
                    //LLog.d("Last computation: " + timestampOfLastComputation.toString());
                }
            });

            sdmPcc.subscribeDataLatencyEvent(new IDataLatencyReceiver() {
                @Override
                public void onNewDataLatency(final long estTimestamp,
                                             final EnumSet<EventFlag> eventFlags, final BigDecimal updateLatency) {
                    //LLog.i("Update Latency: " + updateLatency.toString());
                }
            });

            sdmPcc.subscribeSensorStatusEvent(new ISensorStatusReceiver() {
                @Override
                public void onNewSensorStatus(final long estTimestamp,
                                              EnumSet<EventFlag> eventFlags,
                                              final SensorLocation sensorLocation, final BatteryStatus batteryStatus,
                                              final SensorHealth sensorHealth, final SensorUseState useState) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.BATTERY_INFO, batteryStatus.toString())
                            .add(getAddress(), DataType.SENSOR_LOCATION, StringUtil.proper(sensorLocation.toString()))
                            .add(getAddress(), DataType.STATE, StringUtil.proper(sensorHealth.toString())));
                }
            });

            sdmPcc.subscribeCalorieDataEvent(new ICalorieDataReceiver() {
                @Override
                public void onNewCalorieData(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long cumulativeCalories) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.ENERGY, 4.184 * cumulativeCalories));
                }
            });

            sdmPcc.subscribeManufacturerIdentificationEvent(new IManufacturerIdentificationReceiver() {
                @Override
                public void onNewManufacturerIdentification(final long estTimestamp,
                                                            final EnumSet<EventFlag> eventFlags, final int hardwareRevision,
                                                            final int manufacturerID, final int modelNumber) {
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.MANUFACTURER_NAME_STRING, manufacturer = AntDeviceManufacturer.fromId(manufacturerID).getLabel())
                            .add(getAddress(), DataType.HARDWARE_REVISION_STRING, "" + hardwareRevision)
                            .add(getAddress(), DataType.MODEL_NUMBER_STRING, "" + modelNumber));

                }
            });

            sdmPcc.subscribeProductInformationEvent(new IProductInformationReceiver() {
                @Override
                public void onNewProductInformation(final long estTimestamp,
                                                    final EnumSet<EventFlag> eventFlags, final int mainSoftwareRevision,
                                                    final int supplementalSoftwareRevision, final long serialNumber) {
                    String softwareRev = "" + mainSoftwareRevision;
                    if (supplementalSoftwareRevision != -2 && supplementalSoftwareRevision != 0xFF)
                        softwareRev += supplementalSoftwareRevision;
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.SOFTWARE_REVISION_STRING, softwareRev)
                            .add(getAddress(), DataType.SERIAL_NUMBER_STRING, "" + serialNumber));
                }
            });

            sdmPcc.subscribeManufacturerSpecificDataEvent(new IManufacturerSpecificDataReceiver() {
                @Override
                public void onNewManufacturerSpecificData(final long estTimestamp,
                                                          final EnumSet<EventFlag> eventFlags, final byte[] rawDataBytes) {
                    StringBuilder hexString = new StringBuilder();
                    for (byte rawDataByte : rawDataBytes) {
                        hexString
                                .append("[")
                                .append(String.format("%02X",
                                        rawDataByte & 0xFF)).append("]");
                    }
                    //LLog.i("Manufacturer specific data: " + hexString.toString());
                }
            });
        }
    }

    @Override
    public void close()
    {
        releaseHandle.close();
        super.close();
    }

}
