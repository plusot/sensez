/*
This software is subject to the license described in the License.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
 */

package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc.ICalculatedCadenceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.CalculatedAccumulatedDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.IMotionAndSpeedDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.IRawSpeedAndDistanceDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.BatteryStatus;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusBikeSpdCadCommonPcc.IBatteryStatusReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.ICumulativeOperatingTimeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.IManufacturerAndSerialReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.IVersionAndModelReceiver;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.EnumSet;

/**
 * Connects to Bike Speed Plugin and display all the event data.
 */
public class AntBikeSpeedDistance extends AntDevice {
    //AntPlusBikeSpeedDistancePcc bsdPcc = null;
    private PccReleaseHandle<AntPlusBikeSpeedDistancePcc> bsdReleaseHandle = null;
    private AntPlusBikeCadencePcc bcPcc = null;
    private PccReleaseHandle<AntPlusBikeCadencePcc> bcReleaseHandle = null;
    private double prevTs = Double.NaN;
    private long prevRevolutions = 0;


    public AntBikeSpeedDistance(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(searchResult, listener);
        LLog.i("Initializing ...");
        resetPcc();
    }

    private void resetPcc() {
        LLog.i("Handling reset");
        //Release the old access if it exists
        if (bsdReleaseHandle != null) {
            bsdReleaseHandle.close();
            bsdReleaseHandle = null;
        }
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
            bcReleaseHandle = null;
        }
        requestAccessToPcc();
    }

    protected void requestAccessToPcc() {
        boolean isBSC = searchResult.getAntDeviceType().equals(DeviceType.BIKE_SPDCAD);
        final Context appContext = Globals.getAppContext();
        if(appContext == null) return;

        bsdReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(
                appContext,
                searchResult.getAntDeviceNumber(),
                0,
                isBSC,
                resultReceiver,
                deviceStateChangeReceiver);
        // starts the plugins UI search
//            bsdReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(this, this,
//                    resultReceiver, deviceStateChangeReceiver);
    }

    protected IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> resultReceiver = new IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
        // Handle the result, connecting to events on success or reporting
        // failure to user.
        @Override
        public void onResultReceived(AntPlusBikeSpeedDistancePcc result,
                                     RequestAccessResult resultCode, DeviceState initialDeviceState) {
            onAccessResultReceived(result, resultCode, initialDeviceState, true);
        }
    };

    @Override
    protected  void subscribeToEvents() {
        AntPlusBikeSpeedDistancePcc bsdPcc;
        if (pcc instanceof AntPlusBikeSpeedDistancePcc) {
            bsdPcc = (AntPlusBikeSpeedDistancePcc) pcc;
            BigDecimal wc = new BigDecimal(PreferenceKey.WHEEL_CIRCUMFERENCE.getDouble());


            bsdPcc.subscribeCalculatedSpeedEvent(new CalculatedSpeedReceiver(wc) {

                @Override
                public void onNewCalculatedSpeed(final long estTimestamp,
                                                 final EnumSet<EventFlag> eventFlags, final BigDecimal calculatedSpeed) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.SPEED, calculatedSpeed.doubleValue()));
                }
            });

            bsdPcc.subscribeCalculatedAccumulatedDistanceEvent(new CalculatedAccumulatedDistanceReceiver(wc) {

                @Override
                public void onNewCalculatedAccumulatedDistance(final long estTimestamp,
                                                               final EnumSet<EventFlag> eventFlags,
                                                               final BigDecimal calculatedAccumulatedDistance) {
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.DISTANCE,
                            calculatedAccumulatedDistance.setScale(3, RoundingMode.HALF_UP).doubleValue()));
                }
            });


            bsdPcc.subscribeRawSpeedAndDistanceDataEvent(new IRawSpeedAndDistanceDataReceiver() {
                @Override
                public void onNewRawSpeedAndDistanceData(final long estTimestamp,
                                                         final EnumSet<EventFlag> eventFlags,
                                                         final BigDecimal timestampOfLastEvent, final long cumulativeRevolutions) {

                    double ts = timestampOfLastEvent.doubleValue();
                    if (!Double.isNaN(prevTs) && ts > prevTs) {
                        double rpm = 60.0 * (cumulativeRevolutions - prevRevolutions) / (ts - prevTs);
                        fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.WHEEL_REVOLUTIONS, rpm));
                    }
                    prevTs = ts;
                    prevRevolutions = cumulativeRevolutions;
                }
            });

            final Context appContext = Globals.getAppContext();

            if (bsdPcc.isSpeedAndCadenceCombinedSensor() && bcReleaseHandle == null && appContext != null) {
                LLog.i("Trying to subscribe to combined sensor functions");

                bcReleaseHandle = AntPlusBikeCadencePcc.requestAccess(
                        appContext,
                        bsdPcc.getAntDeviceNumber(), 0, true,
                        new IPluginAccessResultReceiver<AntPlusBikeCadencePcc>() {
                            // Handle the result, connecting to events
                            // on success or reporting failure to user.
                            @Override
                            public void onResultReceived(AntPlusBikeCadencePcc result,
                                                         RequestAccessResult resultCode,
                                                         DeviceState initialDeviceState) {
                                AntPluginPcc ret = onAccessResultReceived(result, resultCode,
                                        initialDeviceState, false);
                                if (ret != null && ret instanceof AntPlusBikeCadencePcc) {
                                    bcPcc = (AntPlusBikeCadencePcc) ret;
                                    bcPcc.subscribeCalculatedCadenceEvent(new ICalculatedCadenceReceiver() {
                                        @Override
                                        public void onNewCalculatedCadence(
                                                long estTimestamp,
                                                EnumSet<EventFlag> eventFlags,
                                                final BigDecimal calculatedCadence) {
                                            fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.CADENCE, calculatedCadence.doubleValue()));
                                        }
                                    });
                                }

                            }
                        },
                        // Receives state changes and shows it on the
                        // status display line
                        new IDeviceStateChangeReceiver() {
                            @Override
                            public void onDeviceStateChange(final DeviceState newDeviceState) {
                                //if (bcPcc != null) LLog.i(bcPcc.getDeviceName() + ": " + newDeviceState);
                                fireState(StateInfo.fromDeviceState(newDeviceState));
                                if (newDeviceState == DeviceState.DEAD) bcPcc = null;
                            }
                        }
                );

            } else {
                bsdPcc.subscribeCumulativeOperatingTimeEvent(new ICumulativeOperatingTimeReceiver() {
                    @Override
                    public void onNewCumulativeOperatingTime(final long estTimestamp,
                                                             final EnumSet<EventFlag> eventFlags, final long cumulativeOperatingTime) {
//                        LLog.i("Cumulative operating time = " + cumulativeOperatingTime);
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.TIME_ACTIVITY, cumulativeOperatingTime)
                        );
                    }
                });


                bsdPcc.subscribeManufacturerAndSerialEvent(new IManufacturerAndSerialReceiver() {
                    @Override
                    public void onNewManufacturerAndSerial(final long estTimestamp,
                                                           final EnumSet<EventFlag> eventFlags, final int manufacturerID,
                                                           final int serialNumber) {
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.MANUFACTURER_NAME_STRING, manufacturer = AntDeviceManufacturer.fromId(manufacturerID).getLabel())
                                        .add(getAddress(), DataType.SERIAL_NUMBER_STRING, "" + serialNumber)
                        );
                    }
                });

                bsdPcc.subscribeVersionAndModelEvent(new IVersionAndModelReceiver() {
                    @Override
                    public void onNewVersionAndModel(final long estTimestamp,
                                                     final EnumSet<EventFlag> eventFlags, final int hardwareVersion,
                                                     final int softwareVersion, final int modelNumber) {
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.HARDWARE_REVISION_STRING, "" + hardwareVersion)
                                        .add(getAddress(), DataType.SOFTWARE_REVISION_STRING, "" + softwareVersion)
                                        .add(getAddress(), DataType.MODEL_NUMBER_STRING, "" + modelNumber)
                        );
                    }
                });

                bsdPcc.subscribeBatteryStatusEvent(new IBatteryStatusReceiver() {
                    @Override
                    public void onNewBatteryStatus(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                   final BigDecimal batteryVoltage, final BatteryStatus batteryStatus) {
                        fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.BATTERY_LEVEL, batteryVoltage.doubleValue())
                                .add(getAddress(), DataType.BATTERY_INFO, batteryStatus.toString()));
                    }
                });


                bsdPcc.subscribeMotionAndSpeedDataEvent(new IMotionAndSpeedDataReceiver() {
                    @Override
                    public void onNewMotionAndSpeedData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                        final boolean isPedallingStopped) {
                        //LLog.d("Pedalling stopped = " + isPedallingStopped, 300000);
                    }
                });
            }
        }
    }


    @Override
    public void close() {
        if (bsdReleaseHandle != null) bsdReleaseHandle.close();
        if (bcReleaseHandle != null) bcReleaseHandle.close();
        bcReleaseHandle = null;
        bsdReleaseHandle = null;
        super.close();
    }


}
