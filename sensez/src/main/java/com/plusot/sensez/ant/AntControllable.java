package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc;
import com.dsi.ant.plugins.antplus.pcc.controls.AntPlusGenericControllableDevicePcc;
import com.dsi.ant.plugins.antplus.pcc.controls.defines.CommandStatus;
import com.dsi.ant.plugins.antplus.pcc.controls.defines.GenericCommandNumber;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;

import java.util.EnumSet;

/**
 * Package: com.plusot.sensez.ant
 * Project: sensez
 * <p>
 * Created by Peter Bruinink on 31/12/2018.
 * Copyright © 2018 Plusot. All rights reserved.
 */

public class AntControllable extends AntDevice {
    private PccReleaseHandle<AntPlusGenericControllableDevicePcc> releaseHandle = null;
    private boolean debug = false;


    AntControllable(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(searchResult, listener);
        LLog.i("Initializing ...");
        resetPcc();
    }

    private void resetPcc() {
        if (releaseHandle != null) {
            releaseHandle.close();
        }
        requestAccessToPcc();
    }

    @Override
    protected void requestAccessToPcc() {
        final Context appContext = Globals.getAppContext();
        if(appContext == null) return;
        releaseHandle = AntPlusGenericControllableDevicePcc.requestAccess(
                appContext,
                resultReceiver,
                stateChangeReceiver,
                genericCommandReceiver,
                0
        );
    }

    private AntPluginPcc.IPluginAccessResultReceiver<AntPlusGenericControllableDevicePcc> resultReceiver = new AntPluginPcc.IPluginAccessResultReceiver<AntPlusGenericControllableDevicePcc>() {
        @Override
        public void onResultReceived(AntPlusGenericControllableDevicePcc result, RequestAccessResult resultCode, DeviceState initialDeviceState) {
            onAccessResultReceived(result, resultCode, initialDeviceState, true);

        }
    };

    private AntPluginPcc.IDeviceStateChangeReceiver stateChangeReceiver = new AntPluginPcc.IDeviceStateChangeReceiver() {

        @Override
        public void onDeviceStateChange(DeviceState deviceState) {
            LLog.i("Device state = " + deviceState);
        }
    };

    private AntPlusGenericControllableDevicePcc.IGenericCommandReceiver genericCommandReceiver = new AntPlusGenericControllableDevicePcc.IGenericCommandReceiver() {

        @Override
        public CommandStatus onNewGenericCommand(long timestamp, EnumSet<EventFlag> enumSet, int serialNumber, int manufacturerId, int sequenceNumber, GenericCommandNumber genericCommandNumber) {
            for (EventFlag flag : enumSet) LLog.i("Command " + genericCommandNumber + ", event: " + flag + "at" + timestamp);
            return CommandStatus.PASS;
        }
    };

    @Override
    protected void subscribeToEvents() {
        if (pcc == null) return;
        //AntPlusGenericControllableDevicePcc ctrlPcc;
        if (pcc instanceof AntPlusGenericControllableDevicePcc) {
            //ctrlPcc = (AntPlusGenericControllableDevicePcc) pcc;
            LLog.i("May subscribe");
        }

    }

    @Override
    public void close() {
        releaseHandle.close();
        super.close();
    }

}
