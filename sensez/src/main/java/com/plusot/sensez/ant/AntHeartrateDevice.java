package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;

import java.math.BigDecimal;
import java.util.EnumSet;

class AntHeartrateDevice extends AntDevice {
    private PccReleaseHandle<AntPlusHeartRatePcc> releaseHandle = null;


    AntHeartrateDevice(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(searchResult, listener);
        handleReset();
    }

    private void handleReset() {
        if(releaseHandle != null) {
            releaseHandle.close();
        }
        requestAccessToPcc();
    }

    @Override protected void requestAccessToPcc()
    {
        //LLog.i("Trying to get access to PCC");
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        releaseHandle = AntPlusHeartRatePcc.requestAccess(appContext,
                searchResult.getAntDeviceNumber(), 0,
                resultReceiver,
                deviceStateChangeReceiver);

    }

    private IPluginAccessResultReceiver<AntPlusHeartRatePcc> resultReceiver =
            new IPluginAccessResultReceiver<AntPlusHeartRatePcc>() {
                //Handle the result, connecting to events on success or reporting failure to user.
                @Override
                public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode,
                                             DeviceState initialDeviceState) {
                    onAccessResultReceived(result, resultCode, initialDeviceState, true);
                }
            };

    @Override
    protected void subscribeToEvents() {
        AntPlusHeartRatePcc hrPcc;
        if (pcc instanceof AntPlusHeartRatePcc) {
            hrPcc = (AntPlusHeartRatePcc) pcc;

            hrPcc.subscribeHeartRateDataEvent(new AntPlusHeartRatePcc.IHeartRateDataReceiver() {
                @Override
                public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                               final int computedHeartRate, final long heartBeatCount,
                                               final BigDecimal heartBeatEventTime, final AntPlusHeartRatePcc.DataState dataState) {
                    //Data data;
                    fireData(new Data(checkBias(estTimestamp)).add(getAddress(), DataType.HEART_RATE, computedHeartRate));
                    //LLog.i("Data = " + data);
                }
            });

            hrPcc.subscribePage4AddtDataEvent(new AntPlusHeartRatePcc.IPage4AddtDataReceiver() {
                @Override
                public void onNewPage4AddtData(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                                               final int manufacturerSpecificByte,
                                               final BigDecimal previousHeartBeatEventTime) {
                }
            });

            hrPcc.subscribeCumulativeOperatingTimeEvent(new AntPlusLegacyCommonPcc.ICumulativeOperatingTimeReceiver() {
                @Override
                public void onNewCumulativeOperatingTime(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long cumulativeOperatingTime) {

                }
            });

            hrPcc.subscribeManufacturerAndSerialEvent(new AntPlusLegacyCommonPcc.IManufacturerAndSerialReceiver() {
                @Override
                public void onNewManufacturerAndSerial(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int manufacturerID,
                                                       final int serialNumber) {
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.MANUFACTURER_NAME_STRING, manufacturer = AntDeviceManufacturer.fromId(manufacturerID).getLabel())
                            .add(getAddress(), DataType.SERIAL_NUMBER_STRING, "" + serialNumber));
                }
            });

            hrPcc.subscribeVersionAndModelEvent(new AntPlusLegacyCommonPcc.IVersionAndModelReceiver() {
                @Override
                public void onNewVersionAndModel(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int hardwareVersion,
                                                 final int softwareVersion, final int modelNumber) {
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.HARDWARE_REVISION_STRING, "" + hardwareVersion)
                            .add(getAddress(), DataType.SOFTWARE_REVISION_STRING, "" + softwareVersion)
                            .add(getAddress(), DataType.MODEL_NUMBER_STRING, "" + modelNumber));
                }
            });

            hrPcc.subscribeCalculatedRrIntervalEvent(new AntPlusHeartRatePcc.ICalculatedRrIntervalReceiver() {
                @Override
                public void onNewCalculatedRrInterval(final long estTimestamp,
                                                      EnumSet<EventFlag> eventFlags, final BigDecimal rrInterval, final AntPlusHeartRatePcc.RrFlag flag) {
//                if (flag.equals(AntPlusHeartRatePcc.RrFlag.DATA_SOURCE_CACHED)
//                                || flag.equals(AntPlusHeartRatePcc.RrFlag.DATA_SOURCE_PAGE_4))
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.RR_INTERVAL, rrInterval.intValue()));
                }
            });

            hrPcc.subscribeRssiEvent(new AntPlusCommonPcc.IRssiReceiver() {
                @Override
                public void onRssiData(final long estTimestamp, final EnumSet<EventFlag> evtFlags, final int rssi) {
                    fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.RSSI, rssi));
                }
            });
        }
    }

    @Override
    public void close()
    {
        releaseHandle.close();
        super.close();
    }
}
