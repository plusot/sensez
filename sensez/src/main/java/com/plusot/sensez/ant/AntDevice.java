package com.plusot.sensez.ant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.plusot.sensez.R;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

abstract class AntDevice extends Device {
    MultiDeviceSearch.MultiDeviceSearchResult searchResult;
    private long bias = -1;
    AntPluginPcc pcc = null;


    AntDevice(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(listener);
        this.searchResult = searchResult;
    }

    @Override
    public void close() {

//        bsdReleaseHandle.close();
//        if(bcReleaseHandle != null)
//        {
//            bcReleaseHandle.close();
//        }
        super.close();
    }

    AntPluginPcc onAccessResultReceived(AntPluginPcc result, RequestAccessResult resultCode,
                                        DeviceState initialDeviceState, boolean setPcc) {

        AntPluginPcc ret = null;
        if (result != null && AntMan.debug) LLog.i("ANT device name = " + result.getDeviceName());
        final Context appContext = Globals.getAppContext();
        switch(resultCode) {
            case SUCCESS:
                if (setPcc) pcc = result;
                ret = result;
                if (AntMan.debug) LLog.i(R.string.ant_access_succes);
                subscribeToEvents();
                break;
            case CHANNEL_NOT_AVAILABLE:
                LLog.i(R.string.channel_not_available);
                break;
            case ADAPTER_NOT_DETECTED:
                LLog.i(R.string.no_ant_adapter);
                break;
            case DEVICE_ALREADY_IN_USE:
                LLog.i("Device already in use");
                break;
            case SEARCH_TIMEOUT:
                LLog.i("Search time-out");
                SleepAndWake.runInMain(new Runnable() {
                    @Override
                    public void run() {
                        requestAccessToPcc();
                    }
                }, 2000, "AntDevice.onAccessResultReceived.SEARCH_TIMEOUT");
                break;
            case ALREADY_SUBSCRIBED:
                LLog.i("Device already subscribed to");
                break;
            case BAD_PARAMS:
                LLog.i(R.string.bad_params);
                break;
            case OTHER_FAILURE:
                LLog.i(R.string.ant_access_failed);
                break;
            case DEPENDENCY_NOT_INSTALLED:
                if (appContext == null) break;
                AlertDialog.Builder adlgBldr = new AlertDialog.Builder(appContext);
                adlgBldr.setTitle(R.string.missing_dependency);
                adlgBldr.setMessage(appContext.getString(R.string.missing_dependency_msg, AntPlusHeartRatePcc.getMissingDependencyName()));
                adlgBldr.setCancelable(true);
                adlgBldr.setPositiveButton(R.string.to_store, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Intent startStore = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + AntPlusHeartRatePcc.getMissingDependencyPackageName()));
                        startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        appContext.startActivity(startStore);
                    }
                });
                adlgBldr.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });

                final AlertDialog waitDialog = adlgBldr.create();
                waitDialog.show();
                break;
            case USER_CANCELLED:
                LLog.i("User cancelled");
                break;
            case UNRECOGNIZED:
                LLog.i(R.string.ant_unrecognized);
                break;
            default:

                if (appContext == null) break;
                LLog.i(appContext.getString(R.string.ant_unrecognized_result, resultCode.toString()));
                break;
        }
        return ret;
    }


    protected abstract void requestAccessToPcc();
    protected abstract void subscribeToEvents();

    public boolean sendCommand(Enum<?> cmd, double doubleParam, byte[] bytesParam) {
        LLog.e("Calling unimplemented command: " + cmd.toString());
        return false;
    }

    AntPluginPcc.IDeviceStateChangeReceiver deviceStateChangeReceiver = new AntPluginPcc.IDeviceStateChangeReceiver() {
        @Override
        public void onDeviceStateChange(final DeviceState newDeviceState) {
            LLog.i(pcc.getDeviceName() + ": " + newDeviceState);
            fireState(StateInfo.fromDeviceState(newDeviceState));
            if (newDeviceState == DeviceState.DEAD) pcc = null;

        }
    };

    long checkBias(long time) {
        if (bias == -1 || Math.abs(time + bias - System.currentTimeMillis()) > 10000) bias = System.currentTimeMillis() - time;
        return bias + time;

    }

    @Override
    public String getAddress() {
        //searchResult.getDeviceDisplayName();
        return "ANT_" + searchResult.getAntDeviceNumber();
    }
}
