package com.plusot.sensez.ant;
/*******************************************************************************
 * Copyright (c) 2012 Plusot Biketech
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Peter Bruinink - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/

public enum AntDeviceManufacturer {
    GARMIN("Garmin", 1),
    GARMIN_FR("Garmin FR405", 2),
    ZEPHYR("Zephyr",3),
    DAYTON("Dayton",4),
    IDT("Idt",5),
    SRM("Srm",6),
    QUARQ("Quarq",7),
    IBIKE("Ibike",8),
    SARIS("Saris",9),
    SPARK_HK("Spark Hk",10),
    TANITA("Tanita",11),
    ECHOWELL("Echowell",12),
    DYNASTREAM_OEM("Dynastream Oem",13),
    NAUTILUS("Nautilus",14),
    DYNASTREAM("Dynastream",15),
    TIMEX("Timex",16),
    METRIGEAR("Metrigear",17),
    XELIC("Xelic",18),
    BEURER("Beurer",19),
    CARDIOSPORT("Cardiosport",20),
    A_AND_D("A And D",21),
    HMM("Hmm",22),
    SUUNTO("Suunto",23),
    THITA_ELEKTRONIK("Thita Elektronik",24),
    GPULSE("Gpulse",25),
    CLEAN_MOBILE("Clean Mobile",26),
    PEDAL_BRAIN("Pedal Brain",27),
    PEAKSWARE("Peaksware",28),
    SAXONAR("Saxonar",29),
    LEMOND_FITNESS("Lemond Fitness",30),
    DEXCOM("Dexcom",31),
    WAHOO_FITNESS("Wahoo Fitness",32),
    OCTANE_FITNESS("Octane Fitness",33),
    ARCHINOETICS("Archinoetics",34),
    THE_HURT_BOX("The Hurt Box",35),
    CITIZEN_SYSTEMS("Citizen Systems",36),
    MAGELLAN("Magellan",37),
    OSYNCE("Osynce",38),
    HOLUX("Holux",39),
    CONCEPT2("Concept2",40),
    ONE_GIANT_LEAP("One Giant Leap",42),
    ACE_SENSOR("Ace Sensor",43),
    BRIM_BROTHERS("Brim Brothers",44),
    XPLOVA("Xplova",45),
    PERCEPTION_DIGITAL("Perception Digital",46),
    BF1SYSTEMS("Bf1Systems",47),
    PIONEER("Pioneer",48),
    SPANTEC("Spantec",49),
    METALOGICS("Metalogics",50),
    IIIIS("4iiiis",51),
    SEIKO_EPSON("Seiko Epson",52),
    SEIKO_EPSON_OEM("Seiko Epson Oem",53),
    IFOR_POWELL("Ifor Powell",54),
    MAXWELL_GUIDER("Maxwell Guider",55),
    STAR_TRAC("Star Trac",56),
    BREAKAWAY("Breakaway",57),
    ALATECH_TECHNOLOGY_LTD("Alatech Technology Ltd",58),
    MIO_TECHNOLOGY_EUROPE("Mio Technology Europe",59),
    ROTOR("Rotor",60),
    GEONAUTE("Geonaute",61),
    ID_BIKE("Id Bike",62),
    SPECIALIZED("Specialized",63),
    WTEK("Wtek",64),
    PHYSICAL_ENTERPRISES("Physical Enterprises",65),
    NORTH_POLE_ENGINEERING("North Pole Engineering",66),
    BKOOL("Bkool",67),
    CATEYE("Cateye",68),
    STAGES_CYCLING("Stages Cycling",69),
    SIGMASPORT("Sigmasport",70),
    TOMTOM("Tomtom",71),
    PERIPEDAL("Peripedal",72),
    WATTBIKE("Wattbike",73),
    MOXY("Moxy",76),
    CICLOSPORT("Ciclosport",77),
    POWERBAHN("Powerbahn",78),
    ACORN_PROJECTS_APS("Acorn Projects Aps",79),
    LIFEBEAM("Lifebeam",80),
    BONTRAGER("Bontrager",81),
    WELLGO("Wellgo",82),
    SCOSCHE("Scosche",83),
    MAGURA("Magura",84),
    WOODWAY("Woodway",85),
    ELITE("Elite",86),
    NIELSEN_KELLERMAN("Nielsen Kellerman",87),
    DK_CITY("Dk City",88),
    TACX("Tacx",89),
    DIRECTION_TECHNOLOGY("Direction Technology",90),
    MAGTONIC("Magtonic",91),
    ONEPARTCARBON("1 Partcarbon",92),
    INSIDE_RIDE_TECHNOLOGIES("Inside Ride Technologies",93),
    SOUND_OF_MOTION("Sound Of Motion",94),
    STRYD("Stryd",95),
    ICG("Icg",96),
    MI_PULSE("Mi Pulse",97),
    BSX_ATHLETICS("Bsx Athletics",98),
    LOOK("Look",99),
    DEVELOPMENT("Development",255),
    HEALTHANDLIFE("Healthandlife",257),
    LEZYNE("Lezyne",258),
    SCRIBE_LABS("Scribe Labs",259),
    ZWIFT("Zwift",260),
    WATTEAM("Watteam",261),
    RECON("Recon",262),
    FAVERO_ELECTRONICS("Favero Electronics",263),
    DYNOVELO("Dynovelo",264),
    STRAVA("Strava",265),
    ACTIGRAPHCORP("Actigraphcorp",5759),
    UNKNOWN("Unknown Manufacturer", -1),
    PLUSOT("Plusot", 123);

    private final String label;
    private final int id;

    AntDeviceManufacturer(final String label, final int id) {
        this.label = label;
        this.id = id;
    }

    public static AntDeviceManufacturer fromId(int id) {
        for (AntDeviceManufacturer manufacturer : AntDeviceManufacturer.values()) {
            if (manufacturer.id == id) return manufacturer;
        }
        return AntDeviceManufacturer.UNKNOWN;
    }

    public String getLabel() {
        if (this == UNKNOWN) return label + " " + id;
        return label;
    }
}
