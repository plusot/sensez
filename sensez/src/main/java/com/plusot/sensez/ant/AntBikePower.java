/*
This software is subject to the license described in the License.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
*/

package com.plusot.sensez.ant;

import android.content.Context;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.CalculatedWheelDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.CalculatedWheelSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.DataSource;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.plusot.sensez.R;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Stringer;
import com.plusot.util.util.ToastHelper;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Connects to Bike Power Plugin and display all the event data.
 */
class AntBikePower extends AntDevice
{
    //NOTE: We're using 2.07m as the wheel circumference to pass to the calculated events
    private BigDecimal wheelCircumferenceInMeters = new BigDecimal(PreferenceKey.WHEEL_CIRCUMFERENCE.getDouble());
    private PccReleaseHandle<AntPlusBikePowerPcc> releaseHandle = null;
    private double prevAccumulatedTimeStamp = -1.0;

    AntBikePower(MultiDeviceSearch.MultiDeviceSearchResult searchResult, Listener listener) {
        super(searchResult, listener);
        if (AntMan.debug) LLog.i("Initializing ...");
        resetPcc();
    }


    private void resetPcc() {
        if (releaseHandle != null) {
            releaseHandle.close();
        }
        requestAccessToPcc();
    }

    protected void requestAccessToPcc() {
        final Context appContext = Globals.getAppContext();
        if(appContext == null) return;
        releaseHandle = AntPlusBikePowerPcc.requestAccess(
                appContext,
                searchResult.getAntDeviceNumber(),
                0,
                resultReceiver,
                deviceStateChangeReceiver);
    }

    private IPluginAccessResultReceiver<AntPlusBikePowerPcc> resultReceiver = (result, resultCode, initialDeviceState) -> onAccessResultReceived(result, resultCode, initialDeviceState, true);

    /*
         NOTE: The power events will send an initial value code if it needed to calculate a
         NEW average. This is important if using the calculated power event to record user
         data, as an initial value indicates an average could not be guaranteed.
         The event prioritizes calculating with torque data over power only data.

            case POWER_ONLY_DATA:
            case INITIAL_VALUE_POWER_ONLY_DATA:
                // New data calculated from initial
                // value data source
            case WHEEL_TORQUE_DATA:
            case INITIAL_VALUE_WHEEL_TORQUE_DATA:
                // New data calculated from initial
                // value data source
            case CRANK_TORQUE_DATA:
            case INITIAL_VALUE_CRANK_TORQUE_DATA:
                // New data calculated from initial
                // value data source
            case CTF_DATA:
            case INITIAL_VALUE_CTF_DATA:
            case INVALID_CTF_CAL_REQ:
                // The event cannot calculate power
                // from CTF until a zero offset is
                // collected from the sensor.
            case COAST_OR_STOP_DETECTED:
                //A coast or stop condition detected by the ANT+ Plugin.
                //This is automatically sent by the plugin after 3 seconds of unchanging events.
                //NOTE: This value should be ignored by apps which are archiving the data for accuracy.
            case UNRECOGNIZED:
                Unrecognized Bike Power data source. Please update your ANT+ Plugin library
    */

    @Override
    public void subscribeToEvents() {
        if (pcc == null) return;
        AntPlusBikePowerPcc pwrPcc;
        if (pcc instanceof AntPlusBikePowerPcc) {
            pwrPcc = (AntPlusBikePowerPcc) pcc;
            pwrPcc.subscribeCalculatedPowerEvent((estTimestamp, eventFlags, dataSource, calculatedPower) -> {
                //LLog.i("Calculated power " + calculatedPower);
                if (calculatedPower.doubleValue() > -1.0) fireData(new Data(checkBias(estTimestamp))
                                .add(getAddress(), DataType.POWER, calculatedPower.doubleValue())
                        /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
            });


            pwrPcc.subscribeCalculatedTorqueEvent((estTimestamp, eventFlags, dataSource, calculatedTorque) -> {
                //LLog.i("Calculated torque " + calculatedTorque);
                if (calculatedTorque.doubleValue() > -1.0) switch (dataSource) {
                    case WHEEL_TORQUE_DATA:
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.WHEEL_TORQUE, calculatedTorque.doubleValue())
                                /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                        break;
                    case CRANK_TORQUE_DATA:
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.CRANK_TORQUE, calculatedTorque.doubleValue())
                                /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                        break;
                    case POWER_ONLY_DATA:
                    case CTF_DATA:
                        fireData(new Data(checkBias(estTimestamp))
                                        .add(getAddress(), DataType.CRANK_TORQUE, calculatedTorque.doubleValue())
                                /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                        break;
                    case COAST_OR_STOP_DETECTED:
                    case INITIAL_VALUE_POWER_ONLY_DATA:
                    case INITIAL_VALUE_WHEEL_TORQUE_DATA:
                    case INITIAL_VALUE_CRANK_TORQUE_DATA:
                    case INITIAL_VALUE_CTF_DATA:
                    case INVALID:
                    case INVALID_CTF_CAL_REQ:
                    case UNRECOGNIZED:
                    default:
                        fireData(new Data(checkBias(estTimestamp))
                                /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                }
            });

            pwrPcc.subscribeCalculatedCrankCadenceEvent((estTimestamp, eventFlags, dataSource, calculatedCrankCadence) -> fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.CADENCE, calculatedCrankCadence.doubleValue())
                    /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/));

            pwrPcc.subscribeCalculatedWheelSpeedEvent(new CalculatedWheelSpeedReceiver(
                    wheelCircumferenceInMeters) {
                @Override
                public void onNewCalculatedWheelSpeed(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final DataSource dataSource,
                        final BigDecimal calculatedWheelSpeed) {
                    fireData(new Data(checkBias(estTimestamp))
                                    .add(getAddress(), DataType.WHEEL_REVOLUTIONS, calculatedWheelSpeed.doubleValue())
                            /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                }
            });

            pwrPcc.subscribeCalculatedWheelDistanceEvent(new CalculatedWheelDistanceReceiver(
                    wheelCircumferenceInMeters) {
                @Override
                public void onNewCalculatedWheelDistance(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final DataSource dataSource,
                        final BigDecimal calculatedWheelDistance) {
                    fireData(new Data(checkBias(estTimestamp))
                                    .add(getAddress(), DataType.DISTANCE, calculatedWheelDistance.doubleValue())
                            /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/);
                }
            });

            pwrPcc.subscribeInstantaneousCadenceEvent((estTimestamp, eventFlags, dataSource, instantaneousCadence) -> fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.CADENCE_INSTANT, instantaneousCadence)
                    /*.add(getAddress(), DataType.DATA_SOURCE, StringUtil.proper(dataSource.toString()))*/));

            pwrPcc.subscribeRawPowerOnlyDataEvent((estTimestamp, eventFlags, powerOnlyUpdateEventCount, instantaneousPower, accumulatedPower) -> fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.POWER_INSTANT, instantaneousPower)
                    /*add(getAddress(), DataType.POWER_ACCUMULATED, accumulatedPower)*/));

            pwrPcc.subscribePedalPowerBalanceEvent((estTimestamp, eventFlags, rightPedalIndicator, pedalPowerPercentage) -> {
                double pedalRatio = 0.01 * pedalPowerPercentage;
                if (rightPedalIndicator) {
                    pedalRatio = 1.0 - pedalRatio;
                }
                fireData(new Data(checkBias(estTimestamp)).
                        add(getAddress(), DataType.POWER_PEDAL, pedalRatio));
            });

            pwrPcc.subscribeRawWheelTorqueDataEvent((estTimestamp, eventFlags, wheelTorqueUpdateEventCount, accumulatedWheelTicks, accumulatedWheelPeriod, accumulatedWheelTorque) -> {

                if (AntMan.debug)
                    LLog.i("\n\tWheelTicks (sum): " + accumulatedWheelTicks +
                        "\n\tEvent count: " + wheelTorqueUpdateEventCount +
                        "\n\tWheel period (sum): " + accumulatedWheelPeriod.toString() + "s" +
                        "\n\tWheel torque (sum): " + accumulatedWheelTorque.toString() + "Nm");
            });

            pwrPcc.subscribeRawCrankTorqueDataEvent((estTimestamp, eventFlags, crankTorqueUpdateEventCount, accumulatedCrankTicks, accumulatedCrankPeriod, accumulatedCrankTorque) -> {
                if (AntMan.debug)
                    LLog.i("\n\tCrankTicks (sum): " + accumulatedCrankTicks +
                        "\n\tEvent count: " + crankTorqueUpdateEventCount +
                        "\n\tCrank period (sum): " + accumulatedCrankPeriod.toString() + "s" +
                        "\n\tCrank torque (sum): " + accumulatedCrankTorque.toString() + "Nm");
            });

            pwrPcc.subscribeTorqueEffectivenessEvent((estTimestamp, eventFlags, powerOnlyUpdateEventCount, leftTorqueEffectiveness, rightTorqueEffectiveness) -> {
                if (AntMan.debug) LLog.i("\n\tEvent count: " + powerOnlyUpdateEventCount +
                        "\n\tLeft torque effectiveness " + leftTorqueEffectiveness.toString() + "%" +       // -1 == invalid
                        "\n\tRight torque effectiveness: " + rightTorqueEffectiveness.toString() + "%");   // -1 == invalid
            });

            pwrPcc.subscribePedalSmoothnessEvent((estTimestamp, eventFlags, powerOnlyUpdateEventCount, separatePedalSmoothnessSupport, leftOrCombinedPedalSmoothness, rightPedalSmoothness) -> {
                Stringer st = new Stringer("\n\t");
                st.append("\n\tEvent count: " + powerOnlyUpdateEventCount);
                st.append("Separate Pedal Smoothness Support: " + separatePedalSmoothnessSupport);
                if (leftOrCombinedPedalSmoothness.intValue() != -1)
                    st.append("Left or combined pedal smoothness: " +
                            leftOrCombinedPedalSmoothness.toString() + "%");
                if (rightPedalSmoothness.intValue() != -1)
                    st.append("Right pedal smoothness: " +
                            rightPedalSmoothness.toString() + "%");
                if (AntMan.debug) LLog.i(st.toString());
                fireData(new Data(checkBias(estTimestamp))
                        .add(getAddress(), DataType.PEDAL_SMOOTHNESS, leftOrCombinedPedalSmoothness.doubleValue() / 100.0));
            });

            pwrPcc.subscribeRawCtfDataEvent((estTimestamp, eventFlags, ctfUpdateEventCount, instantaneousSlope, accumulatedTimeStamp, accumulatedTorqueTicksStamp) -> {
//                    if (AntMan.debug)
//                if (prevAccumulatedTimeStamp > -1.0 && prevAccumulatedTimeStamp < accumulatedTimeStamp.doubleValue()) {
//                    double val = accumulatedTimeStamp.doubleValue() - prevAccumulatedTimeStamp;
//                    if (AntMan.debug) LLog.i("Torque = " + val * instantaneousSlope.doubleValue() + " Nm");
//                }
                if (AntMan.debug) LLog.i("\n\tEvent count: " + ctfUpdateEventCount +
                        "\n\tInstantaneous slope " + instantaneousSlope.toString() + "Nm/Hz" +
                        "\n\tTime stamp (sum): " + accumulatedTimeStamp.toString() + "s" +
                        "\n\tTorque tick stamp (sum): " + accumulatedTorqueTicksStamp);
                prevAccumulatedTimeStamp = accumulatedTimeStamp.doubleValue();
            });

            pwrPcc.subscribeCalibrationMessageEvent((estTimestamp, eventFlags, calibrationMessage) -> {
                Stringer st = new Stringer("\n\t");
                st.append("\n\tCalibrationId: " + calibrationMessage.calibrationId.toString());

                switch (calibrationMessage.calibrationId) {
                    case GENERAL_CALIBRATION_FAIL:
                    case GENERAL_CALIBRATION_SUCCESS:
                        st.append("Calibration data: " + calibrationMessage.calibrationData.toString());
                        break;
                    case CUSTOM_CALIBRATION_RESPONSE:
                    case CUSTOM_CALIBRATION_UPDATE_SUCCESS:
                        String bytes = "";
                        for (byte manufacturerByte : calibrationMessage.manufacturerSpecificData)
                            bytes += "[" + manufacturerByte + "]";
                        st.append("Manufacturer specific bytes: " + bytes);
                        break;
                    case CTF_MESSAGE:
                        break;
                    case CTF_ZERO_OFFSET:
                        st.append("Ctf Offset: " + calibrationMessage.ctfOffset.toString());
                        break;
                    case UNRECOGNIZED:
                        st.append("Failed: UNRECOGNIZED. PluginLib Upgrade Required?");
                    case CTF_SLOPE_ACK:
                        st.append("Slope acknowledgement");
                        break;
                    case CTF_SERIAL_NUMBER_ACK:
                        st.append("Serial number acknowledgement");
                        break;
                    case CAPABILITIES:
                        st.append("Capabilities");
                        break;
                    case INVALID:
                        st.append("Invalid");
                        break;
                    default:
                        break;
                }
                if (AntMan.debug) LLog.i(st.toString());
            });

            pwrPcc.subscribeAutoZeroStatusEvent((estTimestamp, eventFlags, autoZeroStatus) -> {
                switch (autoZeroStatus) {
                    case NOT_SUPPORTED:
                    case ON:
                    case OFF:
                        if (AntMan.debug) LLog.i("Auto zero status: " + autoZeroStatus.toString());
                        break;
                    default:
                        break;
                }
            });

            pwrPcc.subscribeCrankParametersEvent((estTimestamp, eventFlags, crankParameters) -> {
                Stringer st = new Stringer("\n\t");
                st.append("\n\tCrank length status: " + crankParameters.getCrankLengthStatus().toString());
                st.append("Sensor software mismatch status: " + crankParameters.getSensorSoftwareMismatchStatus().toString());
                st.append("Sensor availability status: " + crankParameters.getSensorAvailabilityStatus().toString());
                st.append("Custom calibration status: " + crankParameters.getCustomCalibrationStatus().toString());
                st.append("Auto crank length supported: " + crankParameters.isAutoCrankLengthSupported());

                switch (crankParameters.getCrankLengthStatus()) {
                    case INVALID_CRANK_LENGTH:
                        st.append("Crank length invalid");
                        break;
                    case DEFAULT_USED:
                    case SET_AUTOMATICALLY:
                    case SET_MANUALLY:
                        st.append("Crank length: " + crankParameters.getFullCrankLength() + "mm");
                        break;
                    default:
                        break;
                }
                if (AntMan.debug) LLog.i(st.toString());
            });

            pwrPcc.subscribeManufacturerIdentificationEvent((estTimestamp, eventFlags, hardwareRevision, manufacturerID, modelNumber) -> fireData(new Data(checkBias(estTimestamp))  .
                    add(getAddress(), DataType.MANUFACTURER_NAME_STRING, manufacturer = AntDeviceManufacturer.fromId(manufacturerID).getLabel()).
                    add(getAddress(), DataType.MODEL_NUMBER_STRING, "" + modelNumber).
                    add(getAddress(), DataType.HARDWARE_REVISION_STRING, "" + hardwareRevision)));

            pwrPcc.subscribeProductInformationEvent((estTimestamp, eventFlags, mainSoftwareRevision, supplementalSoftwareRevision, serialNumber) -> fireData(new Data(checkBias(estTimestamp))
                    .add(getAddress(), DataType.SOFTWARE_REVISION_STRING, "" + mainSoftwareRevision + "." + supplementalSoftwareRevision)
                    .add(getAddress(), DataType.SERIAL_NUMBER_STRING, "" + serialNumber)
            ));

            pwrPcc.subscribeBatteryStatusEvent((estTimestamp, eventFlags, cumulativeOperatingTime, batteryVoltage, batteryStatus, cumulativeOperatingTimeResolution, numberOfBatteries, batteryIdentifier) -> fireData(new Data(checkBias(estTimestamp))
                            .add(getAddress(), DataType.TIME_ACTIVITY, cumulativeOperatingTime)
                            .add(getAddress(), DataType.BATTERY_VOLTAGE, batteryVoltage.doubleValue())
                    //add(getAddress(), DataType.BATTERY_INFO, StringUtil.proper(batteryStatus.toString()))
            ));
        }
    }


    private final AntPlusCommonPcc.IRequestFinishedReceiver requestFinishedReceiver = requestStatus -> {
        switch(requestStatus) {
            case SUCCESS:
                ToastHelper.showToastLongSave(R.string.send_successful);
                break;
            case FAIL_PLUGINS_SERVICE_VERSION:
                ToastHelper.showToastLongSave(R.string.ant_plugin_upgrade);
                break;
            default:
                ToastHelper.showToastLongSave(R.string.send_failed);
                break;
        }
    };

    private enum Command {
        MANUAL_CALIBRATION,
        AUTOZERO_ON,
        AUTOZERO_OFF,
        REQUEST_CRANKPARAMS,
        AUTO_CRANKLENGTH,
        MANUAL_CRANKLENGTH,
        REQUEST_CUSTOM_CALIBRATIONPARAMS,
        SET_CUSTOM_CALIBRATIONPARAMS,
        SET_SLOPE,
        COMMAND_BURST
    }

    @Override
    public boolean sendCommand(Enum<?> cmd, double doubleParam, byte[] bytesParam) {
        if (pcc == null) return false;
        AntPlusBikePowerPcc pwrPcc;
        if (pcc instanceof AntPlusBikePowerPcc) {
            pwrPcc = (AntPlusBikePowerPcc) pcc;
            if (cmd instanceof Command)
                switch ((Command) cmd) {
                    case MANUAL_CALIBRATION:
                        return pwrPcc.requestManualCalibration(requestFinishedReceiver);
                    case AUTOZERO_ON:
                        return pwrPcc.requestSetAutoZero(true, requestFinishedReceiver);
                    case AUTOZERO_OFF:
                        return pwrPcc.requestSetAutoZero(false, requestFinishedReceiver);
                    case REQUEST_CRANKPARAMS:
                        return pwrPcc.requestCrankParameters(requestFinishedReceiver);
                    case AUTO_CRANKLENGTH:
                        return pwrPcc.requestSetCrankParameters(AntPlusBikePowerPcc.CrankLengthSetting.AUTO_CRANK_LENGTH, null, requestFinishedReceiver);
                    case MANUAL_CRANKLENGTH:
                        BigDecimal newCrankLength = new BigDecimal(doubleParam);
                        return pwrPcc.requestSetCrankParameters(AntPlusBikePowerPcc.CrankLengthSetting.MANUAL_CRANK_LENGTH, newCrankLength, requestFinishedReceiver);
                    case REQUEST_CUSTOM_CALIBRATIONPARAMS:
                        //TO DO Manufacturer specific data required here
                        //byte[] customParameters = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};

                        return pwrPcc.requestCustomCalibrationParameters(bytesParam, requestFinishedReceiver);
                    case SET_CUSTOM_CALIBRATIONPARAMS:
                        //TO DO This is only an example, manufacturer specific data required here
                        //byte[] customParameters = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};

                        return pwrPcc.requestSetCustomCalibrationParameters(bytesParam, requestFinishedReceiver);
                    case SET_SLOPE:
                        BigDecimal newSlope = new BigDecimal(doubleParam);
                        return pwrPcc.requestSetCtfSlope(newSlope,  requestFinishedReceiver);
                    case COMMAND_BURST:
                        //TO DO This is only an example, manufacturer specific data required here
                        int exampleCommandId = 42;
                        return pwrPcc.requestCommandBurst(exampleCommandId, bytesParam, requestFinishedReceiver);
                }
        }
        return false;
    }

    @Override
    public void close() {
        releaseHandle.close();
        super.close();
    }

}
