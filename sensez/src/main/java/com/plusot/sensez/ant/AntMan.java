package com.plusot.sensez.ant;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.WindowManager;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch.MultiDeviceSearchResult;
import com.plusot.sensez.R;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.dialog.Alerts;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Tuple;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class AntMan implements ScanManager {
    private static AntMan instance = null;
    private Map<String, ScannedAntDevice> scannedDevices = new HashMap<>();
    private MultiDeviceSearch search;
    private final Object searchLock = new Object();
    private EnumSet<com.dsi.ant.plugins.antplus.pcc.defines.DeviceType> searchDevices = EnumSet.allOf(com.dsi.ant.plugins.antplus.pcc.defines.DeviceType.class);
    private Queue<ScannedAntDevice> sdsToConnect = new LinkedBlockingQueue<>();
    private SleepAndWake.Stopper stopper = null;
//    private Object stopperLock = new Object();
    public static boolean debug = false;

    public static synchronized AntMan getInstance() {
        if (instance == null) instance = new AntMan();
        return instance;
    }

    @Override
    public List<ScannedDevice> getScannedDevices() {
        List<ScannedDevice> list =  new ArrayList<>();
        list.addAll(scannedDevices.values());
        return list;
    }

    private class ScannedAntDevice extends ScannedDevice implements Device.Listener {
        private MultiDeviceSearchResult searchResult;
        private AntDevice antDevice = null;

        ScannedAntDevice(MultiDeviceSearchResult searchResult) {
            super(DeviceType.fromAntType(searchResult.getAntDeviceType()));
            this.searchResult = searchResult;
        }

        public void setSelected(boolean selected) {
            super.setSelected(selected);
            if (selected) {
                SleepAndWake.runInMain(this::launchConnection);
            } else {
                if (antDevice != null) antDevice.close();
                antDevice = null;
            }
        }

        @Override
        public String getName() {
            String name = searchResult.getDeviceDisplayName();
            if (name.toLowerCase().startsWith("device"))
                return getDeviceType().toString(); // + " " + searchResult.getAntDeviceNumber();
            return name;
        }

        @Override
        public String getAddress() {
            return "ANT_" + searchResult.getAntDeviceNumber();
        }

        @Override
        public int getIconId() {
            return R.mipmap.ic_ant;
        }

        void launchConnection() {
            if (AntMan.debug) LLog.i("Launching connection");
            switch (searchResult.getAntDeviceType()) {

                case BIKE_POWER:
                    if (antDevice == null)
                        antDevice = new AntBikePower(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case CONTROLLABLE_DEVICE:
                    if (antDevice == null)
                        antDevice = new AntControllable(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case FITNESS_EQUIPMENT:
                    break;
                case BLOOD_PRESSURE:
                    break;
                case GEOCACHE:
                    break;
                case ENVIRONMENT:
                    break;
                case WEIGHT_SCALE:
                    break;
                case HEARTRATE:
                    if (antDevice == null)
                        antDevice = new AntHeartrateDevice(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case BIKE_SPDCAD:
                    if (antDevice == null)
                        antDevice = new AntBikeSpeedDistance(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case BIKE_CADENCE:
                    if (antDevice == null)
                        antDevice = new AntBikeCadence(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case BIKE_SPD:
                    if (antDevice == null)
                        antDevice = new AntBikeSpeedDistance(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case STRIDE_SDM:
                    if (antDevice == null)
                        antDevice = new AntStride(searchResult, this);
                    else if (AntMan.debug)
                        LLog.i("Found " + searchResult.getAntDeviceType() + ", which already exists");
                    return;
                case UNKNOWN:
                    break;
            }
            final Context appContext = Globals.getAppContext();
            if (appContext != null) ToastHelper.showToastLong(appContext.getString(R.string.not_yet_implemented, StringUtil.proper(searchResult.getAntDeviceType().toString())));

        }

        @Override
        public void onDeviceState(Device device, Device.StateInfo state) {
            Data data = new Data().add(getAddress(), DataType.STATE, state.toString());
            this.deviceData.merge(data);
            Man.fireData(this, data, true);
        }

        @Override
        public void onDeviceData(Device device, Data data) {
//            LLog.i(data.toString());
            this.deviceData.merge(data);
            Man.fireData(this, data, true);
        }

        @Override
        public void onCommandDone(Device device, String commandId, byte[] data, boolean success, boolean closeOnDone) {
            if (closeOnDone) close();
        }

        @Override
        public void close() {
            if (antDevice != null) antDevice.close();
            antDevice = null;
        }

        @Override
        public String getManufacturer() {
            if (antDevice == null) return "" ;
            return antDevice.getManufacturer();
        }
    }

    private AntMan() {
        if (debug) LLog.i("Starting AntMan");
    }

    private MultiDeviceSearch.SearchCallbacks callback = new MultiDeviceSearch.SearchCallbacks()
    {
        /**
         * Called when a device is found. Display found searchDevices in connected and
         * found lists
         */
        public void onDeviceFound(final com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch.MultiDeviceSearchResult deviceFound)
        {
            ScannedAntDevice sd;
            String id = deviceFound.getAntDeviceType() + "_" + deviceFound.getAntDeviceNumber();

//            if (search != null) search.close();
//            search = null;
            boolean isNew = false;

            if (AntMan.debug) LLog.i("Found ANT+ device " + id);
            if ((sd = scannedDevices.get(id)) == null) {
                scannedDevices.put(id, sd = new ScannedAntDevice(deviceFound));
                isNew = true;
            }
            sd.incScanCount();

            if (deviceFound.isAlreadyConnected()) {
                if (AntMan.debug) LLog.i("Device " + id + " already connected");
            } else {
                if (sd.isSelected() && !sdsToConnect.contains(sd)) sdsToConnect.offer(sd);
                //ScannedAntDevice sd = sdsToConnect.poll();
               searchClose(true);

            }
            Man.fireScanned(sd, isNew);
        }


        public void onSearchStopped(RequestAccessResult reason) {
//            LLog.i("Search stopped for the following reason: " + reason);

            if (Globals.runMode.isRun() && onStopSearch(reason)) {
                final ScannedAntDevice finalSd = sdsToConnect.poll();
                if (finalSd != null) SleepAndWake.runInMain(() -> finalSd.launchConnection(), 200, "AntMan.onSearchStopped");
            } else {
                searchClose(false);
            }
        }

        @Override
        public void onSearchStarted(MultiDeviceSearch.RssiSupport supportsRssi) {
            if(supportsRssi == MultiDeviceSearch.RssiSupport.UNAVAILABLE) {
                //ToastHelper.showToastLong(R.string.rssi_info_not_available);
                if (debug) LLog.i("RSSI info not available");
            } else if(supportsRssi == MultiDeviceSearch.RssiSupport.UNKNOWN_OLDSERVICE) {
                ToastHelper.showToastLong(R.string.rssi_might_be_supported);
            }
            if (AntMan.debug) LLog.i("Started searching for ANT+ devices");
        }
    };

    private void searchClose(boolean inNewThread) {
        if (stopper != null) {
            stopper.stop();
            stopper = null;
        }
        Man.fireScanning(ScanType.ANT_SCAN, false);
//        LLog.i("Closing search");
        if (inNewThread) {
            SleepAndWake.runInNewThread(() -> {
                if (search != null) synchronized (searchLock) {
                    if (search != null) search.close();
                    search = null;
                }
            }, "AntMan.searchClose");
        } else {
            if (search != null) synchronized (searchLock) {
                if (search != null) search.close();
                search = null;
            }
        }
    }

    private MultiDeviceSearch.RssiCallback rssiCallback = new MultiDeviceSearch.RssiCallback()
    {
        /**
         * Receive an RSSI data update from a specific found device
         */
        @Override
        public void onRssiUpdate(final int resultId, final int rssi) {
            SleepAndWake.runInMain(() -> {
                for (ScannedAntDevice sd : scannedDevices.values()) {
                    if (sd.searchResult.resultID == resultId) {
                        if (AntMan.debug) LLog.i("RSSI for " + sd.getName() + " with resultId = " + resultId + " = " + rssi + " dB");
                        sd.setRssi(rssi);
                        break;
                    }
                }
            }, "AntMan.rssiCallback");
        }
    };

    private static boolean firstTryMissing = true;

    private boolean onStopSearch(RequestAccessResult reason) {
        boolean mayContinue = true;
        switch(reason)
        {
            case SUCCESS:
                // Do nothing on success
                break;
            case CHANNEL_NOT_AVAILABLE:
                if (AntMan.debug) LLog.i("Channel Not Available");
                break;
            case ADAPTER_NOT_DETECTED:
//                LLog.i("ANT Adapter Not Available. Built-in ANT hardware or external adapter required.");
                mayContinue = false;
                break;
            case BAD_PARAMS:
                // Note: Since we compose all the params ourself, we should
                // never see this result
                if (AntMan.debug) LLog.i("Bad request parameters.");
                break;
            case OTHER_FAILURE:
                if (AntMan.debug) LLog.i("RequestAccess failed. See logcat for details.");
                break;
            case DEPENDENCY_NOT_INSTALLED:
                final Context appContext = Globals.getAppContext();
                if(appContext == null) break;

                if (PreferenceKey.ANTPLUS_INSTALL_CANCELED.getInt() < 4 && firstTryMissing) try {
                    firstTryMissing = false;
                    Alerts.showYesNoDialog(
                            Globals.getVisibleActivity(),
                            R.string.missing_dependency,
                            appContext.getString(R.string.missing_dependency_msg, AntPlusHeartRatePcc.getMissingDependencyName()),
                            R.string.go_to_store,
                            R.string.showNoMore,
                            new Alerts.Listener() {
                                @Override
                                public void onClick(Alerts.ClickResult clickResult) {
                                    switch(clickResult) {

                                        case YES:
                                            Intent startStore = new Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse("market://details?id="
                                                            + AntPlusHeartRatePcc.getMissingDependencyPackageName()));
                                            startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                            try {
                                                appContext.startActivity(startStore);
                                            } catch (ActivityNotFoundException e) {
                                                ToastHelper.showToastLong(R.string.could_not_find_playstore);
                                            }
                                            break;
                                        case NO:
                                            PreferenceKey.ANTPLUS_INSTALL_CANCELED.set(PreferenceKey.ANTPLUS_INSTALL_CANCELED.getInt() + 4);
                                            break;
                                        case CANCEL:
                                            PreferenceKey.ANTPLUS_INSTALL_CANCELED.set(PreferenceKey.ANTPLUS_INSTALL_CANCELED.getInt() + 1);
                                            break;
                                    }
                                }
                            },
                            -1
                    );
                    mayContinue = false;

                } catch (WindowManager.BadTokenException e) {
                    if (AntMan.debug) LLog.i("Could not show Alert", e);
                }
                break;
            case USER_CANCELLED:
                break;
            case UNRECOGNIZED:
                LLog.i("Failed: UNRECOGNIZED. PluginLib Upgrade Required?");
                break;
            default:
                LLog.i("Unrecognized result: " + reason);
                break;
        }
        return mayContinue;
    }

    @Override
    public void close() {
        if (Man.debug) LLog.i("Closing AntMan");
        searchClose(true);
        for (ScannedAntDevice sd : scannedDevices.values()) sd.close();
        scannedDevices.clear();

        instance = null;
        if (Man.debug) LLog.i("Closed AntMan");
    }

    private void startSearch() {
        final Context appContext = Globals.getAppContext();
        if (appContext != null && Globals.runMode.isRun()) {
            Man.fireScanning(ScanType.ANT_SCAN, true);
            synchronized (searchLock) {
                search = new MultiDeviceSearch(appContext, searchDevices, callback, rssiCallback);
            }
            stopper = SleepAndWake.runInMain(() -> {
                stopper = null;
                searchClose(false);
            }, Man.getScanPeriod(), "AntMan.startSearch");
        }
    }

    @Override
    public boolean startScan(ScanType type) {
        SleepAndWake.runInMain(() -> startSearch(), "AntMan.startScan");
        return true;
    }

    @Override
    public void stopScan(ScanType type) {
        LLog.i("Forced stop scan");
        searchClose(true);
    }

}
