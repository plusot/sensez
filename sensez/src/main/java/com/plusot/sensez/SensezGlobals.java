package com.plusot.sensez;

import android.location.Location;

public class SensezGlobals {
    public static final long SENSOR_MIN_INTERVAL = 1000;
    public static Location lastLocation = null;
}
