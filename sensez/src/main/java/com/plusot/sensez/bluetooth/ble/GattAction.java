package com.plusot.sensez.bluetooth.ble;

/**
 * Created by peet on 15-04-15.
 */
public enum GattAction {
    ACTION_GATT_CONNECTED,
    ACTION_GATT_DISCONNECTED,
    ACTION_GATT_SERVICES_DISCOVERED,
    ACTION_DATA_AVAILABLE
}