package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

import java.io.IOException;
import java.util.UUID;

public class BTConnectThread extends Thread {
	//public static final int CONNECT_FAILED = 2;

    private static final UUID BS_UUID= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothSocket socket;
    private final BluetoothDevice device;
    private final BluetoothAdapter adapter;
    private final Listener listener;

    public enum ConnectResult {
        CONNECT_FAILED,
        CONNECTED,
    }

    public interface Listener {
        void onBluetoothConnected(ConnectResult connectResult, BluetoothDevice device, BluetoothSocket socket);
		//public void onTerminate(BluetoothConnectThread sender);
	}

    public BTConnectThread(Listener listener, BluetoothAdapter adapter, BluetoothDevice device) {
        this.adapter = adapter;
        this.listener = listener;
        this.device = device;

		setName("ConnectThread-" + BTHelper.getName(device));
		try {
			socket = getBluetoothSocket(device);
		} catch (IOException e) {
			LLog.e("failed", e);
		}
	}

    public String getAddress(){
        return device.getAddress();
    }

    private BluetoothSocket getBluetoothSocket(BluetoothDevice bluetoothDevice) throws IOException {
        BluetoothSocket socket;
        try {
            socket =  bluetoothDevice.createInsecureRfcommSocketToServiceRecord(BS_UUID);
            //LLog.i("Created insecure socket connection");
            return socket;
        } catch (IOException e) {
            LLog.i("Unable to create insecure connection", e);
        }
        socket = bluetoothDevice.createRfcommSocketToServiceRecord(BS_UUID);
        //LLog.i("Created secure socket connection");
        return socket;
    }

	@Override
	public void run() {
		// Always close discovery because it will slow down a connection
		adapter.cancelDiscovery();
		if (socket != null) try {
			// This is a blocking call and will only return on a
			// successful connection or an exception
			socket.connect();
            SleepAndWake.runInMain(() -> listener.onBluetoothConnected(ConnectResult.CONNECTED, device, socket), "BTConnectThread.run");

		} catch (IOException e) {
//			LLog.e("Bluetooth connection failed:" + e.getMessage());
			try {
				socket.close();
			} catch (IOException e2) {
				LLog.e("Unable to close() socket during connection failure", e2);
			}
            SleepAndWake.runInMain(() -> listener.onBluetoothConnected(ConnectResult.CONNECT_FAILED, device, socket), "BTConnectThread.run.close");
			// Start the service over to restart listening mode
			//BlueWindActivity.this.start();
			
		} else {
            SleepAndWake.runInMain(() -> listener.onBluetoothConnected(ConnectResult.CONNECT_FAILED, device, socket), "BTConnectThread.run.close");
        }
	}
	
	public void close() {
		try {
			interrupt();
			socket.close();
		} catch (IOException e) {
			LLog.e("Close() of connect socket failed", e);
		}
	}

}
