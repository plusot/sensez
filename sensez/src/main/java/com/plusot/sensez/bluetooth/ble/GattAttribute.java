package com.plusot.sensez.bluetooth.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Build;

import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.sensez.device.CommandType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;
import com.plusot.util.util.Numbers;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Stringer;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Tuple;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public enum GattAttribute {
    GENERIC_ACCESS_SERVICE(GattAttributeType.SERVICE, "00001800-0000-1000-8000-00805f9b34fb"),
    GENERIC_ATTRIBUTE_SERVICE(GattAttributeType.SERVICE, "00001801-0000-1000-8000-00805f9b34fb"),
    HEART_RATE_SERVICE(GattAttributeType.SERVICE, "0000180d-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    DEVICE_INFORMATION_SERVICE(GattAttributeType.SERVICE, "0000180a-0000-1000-8000-00805f9b34fb"),
    BATTERY_SERVICE(GattAttributeType.SERVICE, "0000180f-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    CYCLING_SPEED_AND_CADENCE_SERVICE(GattAttributeType.SERVICE, "00001816-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    ALERT_NOTIFICATION_SERVICE(GattAttributeType.SERVICE, "00001811-0000-1000-8000-00805f9b34fb"),
    BLOOD_PRESSURE_SERVICE(GattAttributeType.SERVICE, "00001810-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    BODY_COMPOSITION_SERVICE(GattAttributeType.SERVICE, "0000181B-0000-1000-8000-00805f9b34fb"),
    BOND_MANAGEMENT(GattAttributeType.SERVICE, "0000181E-0000-1000-8000-00805f9b34fb"),
    CONTINUOUS_GLUCOSE_SERVICE(GattAttributeType.SERVICE, "0000181F-0000-1000-8000-00805f9b34fb"),
    CURRENT_TIME_SERVICE(GattAttributeType.SERVICE, "00001805-0000-1000-8000-00805f9b34fb"),
    CYCLING_POWER_SERVICE(GattAttributeType.SERVICE, "00001818-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    ENVIRONMENTAL_SENSING_SERVICE(GattAttributeType.SERVICE, "0000181A-0000-1000-8000-00805f9b34fb"),
    GLUCOSE_SERVICE(GattAttributeType.SERVICE, "00001808-0000-1000-8000-00805f9b34fb"),
    HEALTH_THERMOMETER_SERVICE(GattAttributeType.SERVICE, "00001809-0000-1000-8000-00805f9b34fb"),
    HUMAN_INTERFACE_DEVICE_SERVICE(GattAttributeType.SERVICE, "00001812-0000-1000-8000-00805f9b34fb"),
    IMMEDIATE_ALERT_SERVICE(GattAttributeType.SERVICE, "00001802-0000-1000-8000-00805f9b34fb"),
    INTERNET_PROTOCOL_SUPPORT_SERVICE(GattAttributeType.SERVICE, "00001820-0000-1000-8000-00805f9b34fb"),
    LINK_LOSS_SERVICE(GattAttributeType.SERVICE, "00001803-0000-1000-8000-00805f9b34fb"),
    LOCATION_AND_NAVIGATION_SERVICE(GattAttributeType.SERVICE, "00001819-0000-1000-8000-00805f9b34fb"),
    NEXT_DST_CHANGE_SERVICE(GattAttributeType.SERVICE, "00001807-0000-1000-8000-00805f9b34fb"),
    PHONE_ALERT_STATUS_SERVICE(GattAttributeType.SERVICE, "0000180E-0000-1000-8000-00805f9b34fb"),
    REFERENCE_TIME_UPDATE_SERVICE(GattAttributeType.SERVICE, "00001806-0000-1000-8000-00805f9b34fb"),
    RUNNING_SPEED_AND_CADENCE_SERVICE(GattAttributeType.SERVICE, "00001814-0000-1000-8000-00805f9b34fb"),
    SCAN_PARAMETERS_SERVICE(GattAttributeType.SERVICE, "00001813-0000-1000-8000-00805f9b34fb"),
    TX_POWER_SERVICE(GattAttributeType.SERVICE, "00001804-0000-1000-8000-00805f9b34fb"),
    USER_DATA_SERVICE(GattAttributeType.SERVICE, "0000181C-0000-1000-8000-00805f9b34fb"),
    WEIGHT_SCALE_SERVICE(GattAttributeType.SERVICE, "0000181D-0000-1000-8000-00805f9b34fb"),
    AUTOMATION_IO_SERVICE(GattAttributeType.SERVICE, "00001815-0000-1000-8000-00805f9b34fb"),
    ESTIMOTE_SERVICE(GattAttributeType.SERVICE, "b9403000-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_AUTHENTICATION_SERVICE(GattAttributeType.SERVICE, "b9402000-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_VERSION_SERVICE(GattAttributeType.SERVICE, "b9404000-f5f8-466e-aff9-25556b57fe6d"),
    UNKNOWN(GattAttributeType.CHARACTERISTIC, "00000000-0000-0000-0000-000000000000"),

    ESTIMOTE_UUID(GattAttributeType.CHARACTERISTIC, "b9403003-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_MAJOR(GattAttributeType.CHARACTERISTIC, "b9403001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_MINOR(GattAttributeType.CHARACTERISTIC, "b9403002-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_BATTERY(GattAttributeType.CHARACTERISTIC, "b9403041-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_TEMPERATURE(GattAttributeType.CHARACTERISTIC, "b9403021-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_POWER(GattAttributeType.CHARACTERISTIC, "b9403011-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_INTERVAL(GattAttributeType.CHARACTERISTIC, "b9403012-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_SOFTWARE_VERSION(GattAttributeType.CHARACTERISTIC, "b9404001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_HARDWARE_VERSION(GattAttributeType.CHARACTERISTIC, "b9404002-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_SEED(GattAttributeType.CHARACTERISTIC, "b9402001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_VECTOR(GattAttributeType.CHARACTERISTIC, "b9402002-f5f8-466e-aff9-25556b57fe6d"),

    AEROBIC_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a7e-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_HEART_RATE_LOWER_LIMIT),
    AEROBIC_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a84-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_HEART_RATE_UPPER_LIMIT),
    AEROBIC_THRESHOLD(GattAttributeType.CHARACTERISTIC, "00002a7f-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_THRESHOLD),
    AGE(GattAttributeType.CHARACTERISTIC, "00002a80-0000-1000-8000-00805f9b34fb", DataType.AGE),
    ALERT_CATEGORY_ID(GattAttributeType.CHARACTERISTIC, "00002a43-0000-1000-8000-00805f9b34fb", DataType.ALERT_CATEGORY_ID),
    ALERT_CATEGORY_ID_BIT_MASK(GattAttributeType.CHARACTERISTIC, "00002a42-0000-1000-8000-00805f9b34fb", DataType.ALERT_CATEGORY_ID_BIT_MASK),
    ALERT_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a06-0000-1000-8000-00805f9b34fb", DataType.ALERT_LEVEL),
    ALERT_NOTIFICATION_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a44-0000-1000-8000-00805f9b34fb", DataType.ALERT_NOTIFICATION_CONTROL_POINT),
    ALERT_STATUS(GattAttributeType.CHARACTERISTIC, "00002a3f-0000-1000-8000-00805f9b34fb", DataType.ALERT_STATUS),
    ANAEROBIC_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a81-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_HEART_RATE_LOWER_LIMIT),
    ANAEROBIC_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a82-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_HEART_RATE_UPPER_LIMIT),
    ANAEROBIC_THRESHOLD(GattAttributeType.CHARACTERISTIC, "00002a83-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_THRESHOLD),
    APPARENT_WIND_DIRECTION_(GattAttributeType.CHARACTERISTIC, "00002a73-0000-1000-8000-00805f9b34fb", DataType.APPARENT_WIND_DIRECTION_) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            if (value != 0)
                return new Data().add(gatt.getDevice().getAddress(), DataType.WIND_DIRECTION, 0.01 * value);
            return null;
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    APPARENT_WIND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a72-0000-1000-8000-00805f9b34fb", DataType.APPARENT_WIND_SPEED) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            if (value != 0)
                return new Data().add(gatt.getDevice().getAddress(), DataType.WIND_SPEED, 0.01 * value);
            return null;
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    APPEARANCE(GattAttributeType.CHARACTERISTIC, "00002a01-0000-1000-8000-00805f9b34fb", new DataType[] { DataType.APPEARANCE }) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            if (value != 0)
                return new Data().add(gatt.getDevice().getAddress(), DataType.APPEARANCE, BTHelper.getAppearance(value));
            return null;
        }
    },
    BAROMETRIC_PRESSURE_TREND(GattAttributeType.CHARACTERISTIC, "00002aa3-0000-1000-8000-00805f9b34fb", DataType.BAROMETRIC_PRESSURE_TREND),
    BATTERY_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a19-0000-1000-8000-00805f9b34fb", new DataType[] { DataType.BATTERY_LEVEL }) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            final int batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            return new Data().add(gatt.getDevice().getAddress(), DataType.BATTERY_LEVEL, 0.01 * batteryLevel);
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    BATTERY_LEVEL2(GattAttributeType.CHARACTERISTIC, "00002a1b-0000-1000-8000-00805f9b34fb", new DataType[] { DataType.BATTERY_LEVEL }) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
//            byte[] bytes = characteristic.getValue();
//            int batteryLevel;
//            int batteryState = 0;
//            if (bytes.length >= 2) {
//                batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
//                batteryState = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 1);
//            } else {
            int batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
//            }
            //LLog.i("Battery level = " + StringUtil.toHexString(bytes) + ", " + batteryLevel + ", " + batteryState);
            return new Data().add(gatt.getDevice().getAddress(), DataType.BATTERY_LEVEL, 0.01 * (double) batteryLevel);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    BATTERY_STATE(GattAttributeType.CHARACTERISTIC, "00002a1a-0000-1000-8000-00805f9b34fb", new DataType[] {}) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            byte[] bytes = characteristic.getValue();
            int state= characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);

            LLog.i("Battery state = " + StringUtil.toHexString(bytes) + " = " + state);

            return null;
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    BLOOD_PRESSURE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a49-0000-1000-8000-00805f9b34fb", DataType.BLOOD_PRESSURE_FEATURE) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            if ((flags & 0x1) == 0x1) LLog.i("Movement detection feature supported");
            if ((flags & 0x2) == 0x2) LLog.i("Cuff fit detection feature supported");
            if ((flags & 0x4) == 0x4) LLog.i("Irregular pulse detection feature supported");
            if ((flags & 0x8) == 0x8) LLog.i("Pulse rate range detection feature supported");
            if ((flags & 0x10) == 0x10) LLog.i("Measurement position detection feature supported");
            if ((flags & 0x20) == 0x20) LLog.i("Multiple bonds supported");
            LLog.i("Blood pressure feature: " + StringUtil.toHexString(characteristic.getValue()));
            return null;
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    BLOOD_PRESSURE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a35-0000-1000-8000-00805f9b34fb",
            new DataType[] {
                    DataType.HEART_RATE,
                    DataType.BLOOD_PRESSURE_DIASTOLIC,
                    DataType.BLOOD_PRESSURE_SYSTOLIC,
                    DataType.BLOOD_PRESSURE_MEAN
            }
    ) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            byte[] bytes = characteristic.getValue();
            int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            double systolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 1);
            double diastolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 3);
            double mean = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 5);
            if ((flags & 0x1) == 0x0) {
                // measurement in mmHg. Convert to kPa
                systolic *= 133.322387415; // /= 0.00750061561303;
                diastolic *= 133.322387415;
                mean *= 133.322387415;
            }
            Data data = new Data()
                    .add(gatt.getDevice().getAddress(), DataType.BLOOD_PRESSURE_SYSTOLIC, systolic)
                    .add(gatt.getDevice().getAddress(), DataType.BLOOD_PRESSURE_DIASTOLIC, diastolic)
                    .add(gatt.getDevice().getAddress(), DataType.BLOOD_PRESSURE_MEAN, mean);
            int index = 7;
            if ((flags & 0x2) == 0x2) {
                int year = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, index);
                index += 2;
                int month = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++);
                int day = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++);
                int hour = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++);
                int minutes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++);
                int seconds = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++);
                Calendar cal = Calendar.getInstance();
                cal.set(year, month, day, hour, minutes, seconds);
                LLog.i("Time stamp present: " + TimeUtil.formatTime(cal.getTimeInMillis()));
            }
            if ((flags & 0x4) == 0x4) {
                double pulseRate = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, index);
                LLog.i("Pulse rate present: " + Format.format(pulseRate, 1));
                index += 2;
                data.add(gatt.getDevice().getAddress(), DataType.HEART_RATE, pulseRate);

            }
            if ((flags & 0x8) == 0x8) {
                LLog.i("User ID present:" + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index++));
            }
            if ((flags & 0x10) == 0x10) {
                LLog.i("Measurement status present");
                int status = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, index /*index++*/);
                if (Numbers.checkBit(status, 0x1)) LLog.i("Body movement detected");
                if (Numbers.checkBit(status, 0x2)) LLog.i("Cuff too loose");
                if (Numbers.checkBit(status, 0x4)) LLog.i("Irregular pulse");
                int pulseRateStatus = Numbers.getBits(status, 3, 2);
                switch (pulseRateStatus) {
                    case 0:
                        LLog.i("Pulse rate within range");
                    case 1:
                        LLog.i("Pulse rate exceeds upper limit");
                    case 2:
                        LLog.i("Pulse rate exceeds lower limit");
                    case 3:
                        LLog.i("Pulse rate future use");
                }
                if (Numbers.checkBit(status, 0x20)) LLog.i("Improper measurement position");
            }
            LLog.i("Blood pressure measurement: " + StringUtil.toHexString(bytes));
            return data;

        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    BODY_COMPOSITION_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a9b-0000-1000-8000-00805f9b34fb", DataType.BODY_COMPOSITION_FEATURE),
    BODY_COMPOSITION_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a9c-0000-1000-8000-00805f9b34fb", DataType.BODY_COMPOSITION_MEASUREMENT),
    BODY_SENSOR_LOCATION(GattAttributeType.CHARACTERISTIC, "00002a38-0000-1000-8000-00805f9b34fb", DataType.BODY_SENSOR_LOCATION),
    BOND_MANAGEMENT_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002aa4-0000-1000-8000-00805f9b34fb", DataType.BOND_MANAGEMENT_CONTROL_POINT),
    BOND_MANAGEMENT_FEATURE(GattAttributeType.CHARACTERISTIC, "00002aa5-0000-1000-8000-00805f9b34fb", DataType.BOND_MANAGEMENT_FEATURE),
    BOOT_KEYBOARD_INPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a22-0000-1000-8000-00805f9b34fb", DataType.BOOT_KEYBOARD_INPUT_REPORT),
    BOOT_KEYBOARD_OUTPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a32-0000-1000-8000-00805f9b34fb", DataType.BOOT_KEYBOARD_OUTPUT_REPORT),
    BOOT_MOUSE_INPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a33-0000-1000-8000-00805f9b34fb", DataType.BOOT_MOUSE_INPUT_REPORT),
    CENTRAL_ADDRESS_RESOLUTION(GattAttributeType.CHARACTERISTIC, "00002aa6-0000-1000-8000-00805f9b34fb", DataType.CENTRAL_ADDRESS_RESOLUTION),
    CGM_FEATURE(GattAttributeType.CHARACTERISTIC, "00002aa8-0000-1000-8000-00805f9b34fb", DataType.CGM_FEATURE),
    CGM_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002aa7-0000-1000-8000-00805f9b34fb", DataType.CGM_MEASUREMENT),
    CGM_SESSION_RUN_TIME(GattAttributeType.CHARACTERISTIC, "00002aab-0000-1000-8000-00805f9b34fb", DataType.CGM_SESSION_RUN_TIME),
    CGM_SESSION_START_TIME(GattAttributeType.CHARACTERISTIC, "00002aaa-0000-1000-8000-00805f9b34fb", DataType.CGM_SESSION_START_TIME),
    CGM_SPECIFIC_OPS_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002aac-0000-1000-8000-00805f9b34fb", DataType.CGM_SPECIFIC_OPS_CONTROL_POINT),
    CGM_STATUS(GattAttributeType.CHARACTERISTIC, "00002aa9-0000-1000-8000-00805f9b34fb", DataType.CGM_STATUS),
    CSC_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a5c-0000-1000-8000-00805f9b34fb", DataType.CSC_FEATURE),
    CSC_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a5b-0000-1000-8000-00805f9b34fb",
            new DataType[] {
                    DataType.WHEEL_REVOLUTIONS,
                    DataType.SPEED,
                    DataType.DISTANCE
                    //DataType.WHEEL_CIRCUMFERENCE
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            RefData refDataItem;
            if ((refDataItem = refData.get(gatt.getDevice().getAddress())) == null || !(refDataItem instanceof WheelDataItem)) {
                refDataItem = new WheelDataItem();
                refData.put(gatt.getDevice().getAddress(), refDataItem);
            }
            long now = System.currentTimeMillis();
            WheelDataItem wdi = (WheelDataItem) refDataItem;
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000) {
                wdi.reset();
            }
            wdi.prevTimeCalled = now;

            if (getDataTypes() == null || getDataTypes().length == 0) {
                LLog.e("No datatypes defined for " + toString());
                return null;
            }
            int flag = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            boolean hasWheelRevolutions = false;
            boolean hasCrankRevolutions = false;
            if ((flag & 0x01) != 0) hasWheelRevolutions = true;
            if ((flag & 0x02) != 0) hasCrankRevolutions = true;
            int index = 1;
            Data data = new Data();
            if (hasWheelRevolutions) {
                final int wheelRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, index);
                index += 4;
                final int wheelEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, index);
                index += 2;
                if (wdi.prevWheelRevolutions != -1 && wdi.prevWheelEventTime != wheelEventTime) {
                    int divRevolutions = (int) ((UINT32_LIMIT + wheelRevolutions - wdi.prevWheelRevolutions) % UINT32_LIMIT);
                    double divTime = 1.0 / 1024 * ((UINT16_LIMIT + wheelEventTime - wdi.prevWheelEventTime) % UINT16_LIMIT);
                    double wheelCircumference = PreferenceKey.WHEEL_CIRCUMFERENCE.getDouble();

                    wdi.wheelRpm *= 0.8;
                    wdi.wheelRpm += 0.2 * 60.0 * divRevolutions / divTime;

                    if (Man.isShowNew()) {
                        if (wdi.distance == -1) {
                            wdi.distance = Man.getDouble(gatt.getDevice().getAddress(), DataType.DISTANCE);
                            if (Double.isNaN(wdi.distance)) wdi.distance = 0;
                        }
                        wdi.distance += wheelCircumference * divRevolutions + wdi.dummyDistance;
                        wdi.dummyDistance = 0;
                        data.add(gatt.getDevice().getAddress(), DataType.DISTANCE, wdi.distance);
                    } else {
                        wdi.dummyDistance += wheelCircumference * divRevolutions;
                    }

                    data.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm);
                    data.add(gatt.getDevice().getAddress(), DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60);
                    //data.add(gatt.getDevice().getAddress(), DataType.WHEEL_CIRCUMFERENCE, wheelCircumference);

                } else if (wdi.prevWheelEventTime == wheelEventTime && wdi.prevWheelRevolutions == wheelRevolutions) {
                    data.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, 0.0);
                    data.add(gatt.getDevice().getAddress(), DataType.SPEED, 0.0);
                }
                wdi.prevWheelEventTime = wheelEventTime;
                wdi.prevWheelRevolutions = wheelRevolutions;
            }
            if (hasCrankRevolutions) {
                final int crankRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, index);
                index += 2;
                final int crankEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, index);
//			Log.d("Received crank revolutions: " + crankRevolutions + ", " + Format.format(crankEventTime, 3));
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime) {
                    int divRevolutions = (int) ((UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT);
                    double divTime = 1.0 / 1024 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT);
                    wdi.crankRpm *= 0.8;
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions / divTime;
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.getDevice().getAddress(), DataType.CADENCE, wdi.crankRpm);
                } else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions) {
                    data.add(gatt.getDevice().getAddress(), DataType.CADENCE, 0.0);
                }
                wdi.prevCrankEventTime = crankEventTime;
                wdi.prevCrankRevolutions = crankRevolutions;

            }
            return data;
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    CURRENT_TIME(GattAttributeType.CHARACTERISTIC, "00002a2b-0000-1000-8000-00805f9b34fb", DataType.TIME_CLOCK),
    CYCLING_POWER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a66-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_CONTROL_POINT) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            RefData refDataItem;
            boolean isNew = false;
            if ((refDataItem = refData.get(gatt.getDevice().getAddress() + "_CONTROL")) == null || !(refDataItem instanceof WheelDataItem)) {
                isNew = true;
                refDataItem = new WheelDataItem();
                refData.put(gatt.getDevice().getAddress() + "_CONTROL", refDataItem);
            }
            if (isNew) {
                LLog.i("Power control point data");
            }
            return null;
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    CYCLING_POWER_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a65-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_FEATURE) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            RefData refDataItem;
            boolean isNew = false;
            if ((refDataItem = refData.get(gatt.getDevice().getAddress() + "_FEATURE")) == null || !(refDataItem instanceof WheelDataItem)) {
                isNew = true;
                refDataItem = new WheelDataItem();
                refData.put(gatt.getDevice().getAddress() + "_FEATURE", refDataItem);
            }
            if (isNew) {
                final int feature = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 0);
                if ((feature & 0x1) == 0x1) LLog.i("Pedal power balance supported");
                if ((feature & 0x2) == 0x2) LLog.i("Accumulated torque supported");
                if ((feature & 0x4) == 0x4) LLog.i("Wheel revolution data supported");
                if ((feature & 0x8) == 0x8) LLog.i("Crank revolution data supported");
                if ((feature & 0x10) == 0x10) LLog.i("Extreme magnitudes supported");
                if ((feature & 0x20) == 0x20) LLog.i("Extreme angles supported");
                if ((feature & 0x40) == 0x40) LLog.i("Top and bottom dead spot angles supported");
                if ((feature & 0x80) == 0x80) LLog.i("Accumulated energy supported");
                if ((feature & 0x100) == 0x100) LLog.i("Offset indication indicator supported");
                if ((feature & 0x200) == 0x200) LLog.i("Offset indication supported");
                if ((feature & 0x400) == 0x400) LLog.i("Measurement characteristic masking supported");
                if ((feature & 0x800) == 0x800) LLog.i("Multiple sensor locations supported");
                if ((feature & 0x1000) == 0x1000) LLog.i("Crank length adjustment supported");
                if ((feature & 0x2000) == 0x2000) LLog.i("Chain length adjustment supported");
                if ((feature & 0x4000) == 0x4000) LLog.i("Chain weight adjustment supported");
                if ((feature & 0x8000) == 0x8000) LLog.i("Span length adjustment supported");
                if ((feature & 0x10000) == 0x10000) LLog.i("Sensor measurement context");
                if ((feature & 0x20000) == 0x20000) LLog.i("Instantaneous measurement direction supported");
                if ((feature & 0x40000) == 0x40000) LLog.i("Factory calibration supported");
                if ((feature & 0x80000) == 0x80000) LLog.i("Enhanced offset compensation supported");
                if ((feature & 0x300000) == 0x100000) LLog.i("Not for use in distributed system");
                if ((feature & 0x300000) == 0x200000) LLog.i("Can be used in distributed system");
                if ((feature & 0x300000) == 0x300000) LLog.i("RFU");
                if ((feature & 0x400000) == 0x400000) LLog.i("Future use");
            }
            return null;
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    CYCLING_POWER_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a63-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_MEASUREMENT) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            RefData refDataItem;
            boolean isNew = false;
            if ((refDataItem = refData.get(gatt.getDevice().getAddress())) == null || !(refDataItem instanceof WheelDataItem)) {
                isNew = true;
                refDataItem = new WheelDataItem();
                refData.put(gatt.getDevice().getAddress(), refDataItem);
            }
            long now = System.currentTimeMillis();
            WheelDataItem wdi = (WheelDataItem) refDataItem;
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000) { wdi.reset(); }
            wdi.prevTimeCalled = now;

            int offset = 0;
            final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            offset += 2;

            int power = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
            offset += 2;
            wdi.powerAvg = wdi.powerAvg * 0.9 + power * 0.1;

            Data data = new Data()
                    .add(gatt.getDevice().getAddress(), DataType.POWER_INSTANT, Double.valueOf(power))
                    .add(gatt.getDevice().getAddress(), DataType.POWER, wdi.powerAvg);

            if ((flags & 0x1) == 0x1) { // bit 0
                if (isNew) LLog.i("Pedal power balance present");
                int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
                offset += 1;
                double valueD = Math.min(value / 200.0, 1.0);
                if ((flags & 0x2) == 0x2) { // bit 1
                    if (isNew) LLog.i("Pedal power balance reference = left");
                    valueD = 1.0 - valueD;
                }
                data.add(gatt.getDevice().getAddress(), DataType.POWER_PEDAL, valueD);
            }
            if ((flags & 0x4) == 0x4) { // bit 2
                if (isNew) LLog.i("Accumulated torque present");
                int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                double valueD = value / 32.0;
                if ((flags & 0x8) == 0x8) { // bit 3
                    if (isNew) LLog.i("Accumulated torque source = crank");
                    data.add(gatt.getDevice().getAddress(), DataType.CRANK_TORQUE, valueD);
                } else {
                    data.add(gatt.getDevice().getAddress(), DataType.WHEEL_TORQUE, valueD);
                }
            }
            if ((flags & 0x10) == 0x10) { // bit 4
                if (isNew) LLog.i("Wheel revolution data present");
                int wheelRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, offset);
                offset += 4;
                int wheelEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset); // / 2048.0;
                offset += 2;
                //data.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, valueD);
                double wheelCircumference = PreferenceKey.WHEEL_CIRCUMFERENCE.getDouble();
                if (wdi.prevWheelRevolutions != -1 && wdi.prevWheelEventTime != wheelEventTime) {
                    int divRevolutions = (int) ((UINT32_LIMIT + wheelRevolutions - wdi.prevWheelRevolutions) % UINT32_LIMIT);
                    double divTime = ((UINT16_LIMIT + wheelEventTime - wdi.prevWheelEventTime) % UINT16_LIMIT) / 2048.0;

                    wdi.wheelRpm *= 0.8;
                    wdi.wheelRpm += 0.2 * 60.0 * divRevolutions / divTime;

                    if (Man.isShowNew()) {
                        if (wdi.distance == -1) {
                            wdi.distance = Man.getDouble(gatt.getDevice().getAddress(), DataType.DISTANCE);
                            if (Double.isNaN(wdi.distance)) wdi.distance = 0;
                        }
                        wdi.distance += wheelCircumference * divRevolutions + wdi.dummyDistance;
                        wdi.dummyDistance = 0;
                        data.add(gatt.getDevice().getAddress(), DataType.DISTANCE, wdi.distance);
                    } else {
                        wdi.dummyDistance += wheelCircumference * divRevolutions;
                    }

                    data.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm);
                    data.add(gatt.getDevice().getAddress(), DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60);
                    //data.add(gatt.getDevice().getAddress(), DataType.WHEEL_CIRCUMFERENCE, wheelCircumference);

                } else if (wdi.prevWheelEventTime == wheelEventTime && wdi.prevWheelRevolutions == wheelRevolutions) {
                    wdi.wheelRpm *= 0.8;
                    data.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm);
                    data.add(gatt.getDevice().getAddress(), DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60);
                }
                wdi.prevWheelEventTime = wheelEventTime;
                wdi.prevWheelRevolutions = wheelRevolutions;
            }
            if ((flags & 0x20) == 0x20) { // bit 5
                if (isNew) LLog.i("Crank revolution data present");
                final int crankRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                final int crankEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime) {
                    int divRevolutions = ((UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT);
                    double divTime = 1.0 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT) / 1024.0;
                    wdi.crankRpm *= 0.8;
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions / divTime;
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.getDevice().getAddress(), DataType.CADENCE, wdi.crankRpm);
                } else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions) {
                    wdi.crankRpm *= 0.8;
                    data.add(gatt.getDevice().getAddress(), DataType.CADENCE, wdi.crankRpm);
                }
                wdi.prevCrankEventTime = crankEventTime;
                wdi.prevCrankRevolutions = crankRevolutions;
            }
            if ((flags & 0x40) == 0x40) { // bit 6
                int maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
                offset += 2;
                int minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
                offset += 2;
                if (isNew) LLog.i("Extreme force magnitudes present: max = " + maxForce + " N, min = " + minForce + " N");
            }
            if ((flags & 0x80) == 0x80) { // bit 7
                int maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
                offset += 2;
                int minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
                offset += 2;
                if (isNew) LLog.i("Extreme torque magnitudes present: max = " + Format.format(maxForce / 32.0, 2) + " Nm, min = " + Format.format(minForce / 32.0, 2) + " Nm");
            }
            if ((flags & 0x100) == 0x100) { // bit 8
                offset += 3;
                if (isNew) LLog.i("Extreme angles present");
            }
            if ((flags & 0x200) == 0x200) { // bit 9
                int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                if (isNew) LLog.i("Top dead spot angle present: " + value + " degrees");
            }
            if ((flags & 0x400) == 0x400) { // bit 10
                int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                if (isNew) LLog.i("Bottom dead spot angle present: " + value + " degrees");
            }
            if ((flags & 0x800) == 0x800) { // bit 11
                int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                if (wdi.energyAtStart == 0) wdi.energyAtStart = value;
                data.add(gatt.getDevice().getAddress(), DataType.ENERGY, ((UINT16_LIMIT + value - wdi.energyAtStart) % UINT16_LIMIT) * 1000.0);
                if (isNew) LLog.i("Accumulated energy present");
            } else {
                 ;
               double energy = Man.getDouble(gatt.getDevice().getAddress(), DataType.ENERGY) + power * (now - wdi.prevTime) / 1000.0;
                data.add(gatt.getDevice().getAddress(), DataType.ENERGY, energy);
                wdi.prevTime = now;
            }

            if ((flags & 0x1000) == 0x1000) if (isNew) LLog.i("Offset compensation indicator"); // bit 12
            if ((flags & 0x2000) == 0x2000) if (isNew) LLog.i("Future use"); // bit 13

            return data;
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    CYCLING_POWER_VECTOR(GattAttributeType.CHARACTERISTIC, "00002a64-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_VECTOR) {
        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            RefData refDataItem;
            boolean isNew = false;
            if ((refDataItem = refData.get(gatt.getDevice().getAddress() + "_VECTOR")) == null || !(refDataItem instanceof WheelDataItem)) {
                isNew = true;
                refDataItem = new WheelDataItem();
                refData.put(gatt.getDevice().getAddress() + "_VECTOR", refDataItem);
            }
            long now = System.currentTimeMillis();
            WheelDataItem wdi = (WheelDataItem) refDataItem;
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000) { wdi.reset(); }
            wdi.prevTimeCalled = now;
            int offset = 0;
            final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            offset++;

            Data data = new Data();
            if ((flags & 0x1) == 0x1) { // bit 0
                if (isNew) LLog.i("Crank revolution data present");
                final int crankRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                final int crankEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime) {
                    int divRevolutions = (UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT;
                    double divTime = 1.0 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT) / 1024.0;
                    wdi.crankRpm *= 0.8;
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions / divTime;
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.getDevice().getAddress(), DataType.CADENCE, wdi.crankRpm);
                } else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions) {
                    data.add(gatt.getDevice().getAddress(), DataType.CADENCE, 0.0);
                }
                wdi.prevCrankEventTime = crankEventTime;
                wdi.prevCrankRevolutions = crankRevolutions;

            }
            if ((flags & 0x2) == 0x2) { // bit 1
                final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                offset += 2;
                if (isNew) LLog.i("First crank measurement angle present: " + value + " degrees");
            }
            if ((flags & 0x4) == 0x4) { // bit 2
                final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
                offset += 2;
                if (isNew) LLog.i("Instant force magnitude array present. First value: " + value + " N");
            }
            if ((flags & 0x8) == 0x8) { // bit 3
                final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
//                offset += 2;
                if (isNew) LLog.i("Instant torque magnitude array present. First value: " + value + " N");
            }
            if ((flags & 0x10) == 0x10) { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Tangential component");
            }
            if ((flags & 0x20) == 0x20) { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Radial component");
            }
            if ((flags & 0x30) == 0x30) { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Lateral component");
            }
            if ((flags & 0x40) == 0x40) { // bit 6
                if (isNew) LLog.i("Future use");
            }

            return null;
        }


        @Override public boolean isSupported() {
            return true;
        }
    },
    DATABASE_CHANGE_INCREMENT(GattAttributeType.CHARACTERISTIC, "00002a99-0000-1000-8000-00805f9b34fb", DataType.DATABASE_CHANGE_INCREMENT),
    DATE_OF_BIRTH(GattAttributeType.CHARACTERISTIC, "00002a85-0000-1000-8000-00805f9b34fb", DataType.DATE_OF_BIRTH),
    DATE_OF_THRESHOLD_ASSESSMENT(GattAttributeType.CHARACTERISTIC, "00002a86-0000-1000-8000-00805f9b34fb", DataType.DATE_OF_THRESHOLD_ASSESSMENT),
    DATE_TIME(GattAttributeType.CHARACTERISTIC, "00002a08-0000-1000-8000-00805f9b34fb", DataType.DATE_TIME),
    DAY_DATE_TIME(GattAttributeType.CHARACTERISTIC, "00002a0a-0000-1000-8000-00805f9b34fb", DataType.DAY_DATE_TIME),
    DAY_OF_WEEK(GattAttributeType.CHARACTERISTIC, "00002a09-0000-1000-8000-00805f9b34fb", DataType.DAY_OF_WEEK),
    CHARACTERISTIC_VALUE_CHANGED(GattAttributeType.CHARACTERISTIC, "00002a7d-0000-1000-8000-00805f9b34fb", DataType.DESCRIPTOR_VALUE_CHANGED),
    DEVICE_NAME(GattAttributeType.CHARACTERISTIC, "00002a00-0000-1000-8000-00805f9b34fb", DataType.DEVICE_NAME),
    DEW_POINT(GattAttributeType.CHARACTERISTIC, "00002a7b-0000-1000-8000-00805f9b34fb", DataType.DEW_POINT),
    DST_OFFSET(GattAttributeType.CHARACTERISTIC, "00002a0d-0000-1000-8000-00805f9b34fb", DataType.DST_OFFSET),
    ELEVATION(GattAttributeType.CHARACTERISTIC, "00002a6c-0000-1000-8000-00805f9b34fb", DataType.ELEVATION),
    EMAIL_ADDRESS(GattAttributeType.CHARACTERISTIC, "00002a87-0000-1000-8000-00805f9b34fb", DataType.EMAIL_ADDRESS),
    EXACT_TIME_256(GattAttributeType.CHARACTERISTIC, "00002a0c-0000-1000-8000-00805f9b34fb", DataType.EXACT_TIME_256),
    FAT_BURN_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a88-0000-1000-8000-00805f9b34fb", DataType.FAT_BURN_HEART_RATE_LOWER_LIMIT),
    FAT_BURN_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a89-0000-1000-8000-00805f9b34fb", DataType.FAT_BURN_HEART_RATE_UPPER_LIMIT),
    FIRMWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a26-0000-1000-8000-00805f9b34fb", DataType.FIRMWARE_REVISION_STRING),
    FIRST_NAME(GattAttributeType.CHARACTERISTIC, "00002a8a-0000-1000-8000-00805f9b34fb", DataType.FIRST_NAME),
    FIVE_ZONE_HEART_RATE_LIMITS(GattAttributeType.CHARACTERISTIC, "00002a8b-0000-1000-8000-00805f9b34fb", DataType.FIVE_ZONE_HEART_RATE_LIMITS),
    GENDER(GattAttributeType.CHARACTERISTIC, "00002a8c-0000-1000-8000-00805f9b34fb", DataType.GENDER),
    GLUCOSE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a51-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_FEATURE),
    GLUCOSE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a18-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_MEASUREMENT),
    GLUCOSE_MEASUREMENT_CONTEXT(GattAttributeType.CHARACTERISTIC, "00002a34-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_MEASUREMENT_CONTEXT),
    GUST_FACTOR(GattAttributeType.CHARACTERISTIC, "00002a74-0000-1000-8000-00805f9b34fb", DataType.GUST_FACTOR),
    HARDWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a27-0000-1000-8000-00805f9b34fb", DataType.HARDWARE_REVISION_STRING),
    HEART_RATE_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a39-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE_CONTROL_POINT),
    HEART_RATE_MAX(GattAttributeType.CHARACTERISTIC, "00002a8d-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE_MAX),
    HEART_RATE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a37-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE
    ) {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            int flag = characteristic.getProperties();
            int format;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                //Log.d("Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                //Log.d("Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            return new Data().add(gatt.getDevice().getAddress(), DataType.HEART_RATE, Double.valueOf(heartRate));
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    RESERVOIR_LEVEL_SERVICE(GattAttributeType.SERVICE, "00002c00-0000-1000-8000-00805f9b34fb", DataType.LEVEL) {
        @Override public boolean isSupported() {
            return true;
        }
    },
    RESERVOIR_LEVEL(GattAttributeType.CHARACTERISTIC, "00002c01-0000-1000-8000-00805f9b34fb", DataType.LEVEL) {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int flag = characteristic.getProperties();
            int format;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                //Log.d("Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                //Log.d("Heart rate format UINT8.");
            }
            final int level = characteristic.getIntValue(format, 1);
            return new Data().add(gatt.getDevice().getAddress(), DataType.LEVEL, Double.valueOf(level));
        }
        @Override public boolean isSupported() { return true; }
    },
    PASSKEY_SERVICE(GattAttributeType.SERVICE, "00002c02-0000-1000-8000-00805f9b34fb", DataType.LEVEL) {
        @Override public boolean isSupported() {
            return true;
        }
    },
    PASSKEY_KEY(GattAttributeType.CHARACTERISTIC, "00002c03-0000-1000-8000-00805f9b34fb", DataType.KEY) {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            final String key = characteristic.getStringValue(0);
            SleepAndWake.runInMain(() ->ToastHelper.showToastShort("Key: " + key));
            try {
                return new Data().add(gatt.getDevice().getAddress(), DataType.KEY, Integer.valueOf(key));
            } catch (NumberFormatException e) {
                return null;
            }
        }
        @Override public boolean isSupported() { return true; }
    },
    KALEIDO_INSULIN_DELIVERY(GattAttributeType.SERVICE,  "81231800-5ea1-589e-004d-6548f98fc73c"),
    KALEIDO_RESERVOIR_LEVEL(GattAttributeType.CHARACTERISTIC, "81232104-5ea1-589e-004d-6548f98fc73c", DataType.LEVEL) {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            int flag = characteristic.getProperties();
            int format;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                //Log.d("Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                //Log.d("Heart rate format UINT8.");
            }
            final int level = characteristic.getIntValue(format, 1);
            return new Data().add(gatt.getDevice().getAddress(), DataType.LEVEL, Double.valueOf(level));
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    HEAT_INDEX(GattAttributeType.CHARACTERISTIC, "00002a7a-0000-1000-8000-00805f9b34fb", DataType.HEAT_INDEX),
    HEIGHT(GattAttributeType.CHARACTERISTIC, "00002a8e-0000-1000-8000-00805f9b34fb", DataType.HEIGHT),
    HID_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a4c-0000-1000-8000-00805f9b34fb", DataType.HID_CONTROL_POINT),
    HID_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a4a-0000-1000-8000-00805f9b34fb", DataType.HID_INFORMATION),
    HIP_CIRCUMFERENCE(GattAttributeType.CHARACTERISTIC, "00002a8f-0000-1000-8000-00805f9b34fb", DataType.HIP_CIRCUMFERENCE),
    HUMIDITY(GattAttributeType.CHARACTERISTIC, "00002a6f-0000-1000-8000-00805f9b34fb", DataType.HUMIDITY),
    IEEE_CERTIFICATION(GattAttributeType.CHARACTERISTIC, "00002a2a-0000-1000-8000-00805f9b34fb", DataType.IEEE_CERTIFICATION),
    INTERMEDIATE_CUFF_PRESSURE(GattAttributeType.CHARACTERISTIC, "00002a36-0000-1000-8000-00805f9b34fb", DataType.INTERMEDIATE_CUFF_PRESSURE),
    INTERMEDIATE_TEMPERATURE(GattAttributeType.CHARACTERISTIC, "00002a1e-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE_INTERMEDIATE),
    IRRADIANCE(GattAttributeType.CHARACTERISTIC, "00002a77-0000-1000-8000-00805f9b34fb", DataType.IRRADIANCE),
    LANGUAGE(GattAttributeType.CHARACTERISTIC, "00002aa2-0000-1000-8000-00805f9b34fb", DataType.LANGUAGE),
    LAST_NAME(GattAttributeType.CHARACTERISTIC, "00002a90-0000-1000-8000-00805f9b34fb", DataType.LAST_NAME),
    LN_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a6b-0000-1000-8000-00805f9b34fb", DataType.LN_CONTROL_POINT),
    LN_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a6a-0000-1000-8000-00805f9b34fb", DataType.LN_FEATURE),
    LOCAL_TIME_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a0f-0000-1000-8000-00805f9b34fb", DataType.LOCAL_TIME_INFORMATION),
    LOCATION_AND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a67-0000-1000-8000-00805f9b34fb", DataType.LOCATION_AND_SPEED),
    MAGNETIC_DECLINATION(GattAttributeType.CHARACTERISTIC, "00002a2c-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_DECLINATION),
    MAGNETIC_FLUX_DENSITY_2D(GattAttributeType.CHARACTERISTIC, "00002aa0-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_FLUX_DENSITY_2D),
    MAGNETIC_FLUX_DENSITY_3D(GattAttributeType.CHARACTERISTIC, "00002aa1-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_FLUX_DENSITY_3D),
    MANUFACTURER_NAME_STRING(GattAttributeType.CHARACTERISTIC, "00002a29-0000-1000-8000-00805f9b34fb", DataType.MANUFACTURER_NAME_STRING),
    MAXIMUM_RECOMMENDED_HEART_RATE(GattAttributeType.CHARACTERISTIC, "00002a91-0000-1000-8000-00805f9b34fb", DataType.MAXIMUM_RECOMMENDED_HEART_RATE),
    MEASUREMENT_INTERVAL(GattAttributeType.CHARACTERISTIC, "00002a21-0000-1000-8000-00805f9b34fb", DataType.MEASUREMENT_INTERVAL),
    MODEL_NUMBER_STRING(GattAttributeType.CHARACTERISTIC, "00002a24-0000-1000-8000-00805f9b34fb", DataType.MODEL_NUMBER_STRING),
    NAVIGATION(GattAttributeType.CHARACTERISTIC, "00002a68-0000-1000-8000-00805f9b34fb", DataType.NAVIGATION),
    NEW_ALERT(GattAttributeType.CHARACTERISTIC, "00002a46-0000-1000-8000-00805f9b34fb", DataType.NEW_ALERT),
    PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS(GattAttributeType.CHARACTERISTIC, "00002a04-0000-1000-8000-00805f9b34fb",
            new DataType[] {
                    DataType.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            final Integer minConnectionInterval = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            final Integer maxConnectionInterval = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 2);
            final Integer slaveLatency = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
            final Integer connectionSupervisorTimeoutMulitplier = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 6);
            Stringer result = new Stringer(", ");

            if (minConnectionInterval != null) result.append(minConnectionInterval);
            if (maxConnectionInterval != null) result.append(maxConnectionInterval);
            if (slaveLatency != null) result.append(slaveLatency);
            if (connectionSupervisorTimeoutMulitplier != null)
                result.append(connectionSupervisorTimeoutMulitplier + " ms");
            return new Data().add(gatt.getDevice().getAddress(), DataType.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS, result.toString());
        }
    },
    PERIPHERAL_PRIVACY_FLAG(GattAttributeType.CHARACTERISTIC, "00002a02-0000-1000-8000-00805f9b34fb", DataType.PERIPHERAL_PRIVACY_FLAG),
    PNP_ID(GattAttributeType.CHARACTERISTIC, "00002a50-0000-1000-8000-00805f9b34fb", DataType.PNP_ID),
    POLLEN_CONCENTRATION(GattAttributeType.CHARACTERISTIC, "00002a75-0000-1000-8000-00805f9b34fb", DataType.POLLEN_CONCENTRATION),
    POSITION_QUALITY(GattAttributeType.CHARACTERISTIC, "00002a69-0000-1000-8000-00805f9b34fb", DataType.POSITION_QUALITY),
    PRESSURE(GattAttributeType.CHARACTERISTIC, "00002a6d-0000-1000-8000-00805f9b34fb", DataType.PRESSURE),
    PROTOCOL_MODE(GattAttributeType.CHARACTERISTIC, "00002a4e-0000-1000-8000-00805f9b34fb", DataType.PROTOCOL_MODE),
    RAINFALL(GattAttributeType.CHARACTERISTIC, "00002a78-0000-1000-8000-00805f9b34fb", DataType.RAINFALL),
    RECONNECTION_ADDRESS(GattAttributeType.CHARACTERISTIC, "00002a03-0000-1000-8000-00805f9b34fb", DataType.RECONNECTION_ADDRESS),
    RECORD_ACCESS_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a52-0000-1000-8000-00805f9b34fb", DataType.RECORD_ACCESS_CONTROL_POINT),
    REFERENCE_TIME_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a14-0000-1000-8000-00805f9b34fb", DataType.REFERENCE_TIME_INFORMATION),
    REPORT(GattAttributeType.CHARACTERISTIC, "00002a4d-0000-1000-8000-00805f9b34fb", DataType.REPORT),
    REPORT_MAP(GattAttributeType.CHARACTERISTIC, "00002a4b-0000-1000-8000-00805f9b34fb", DataType.REPORT_MAP),
    RESTING_HEART_RATE(GattAttributeType.CHARACTERISTIC, "00002a92-0000-1000-8000-00805f9b34fb", DataType.RESTING_HEART_RATE),
    RINGER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a40-0000-1000-8000-00805f9b34fb", DataType.RINGER_CONTROL_POINT),
    RINGER_SETTING(GattAttributeType.CHARACTERISTIC, "00002a41-0000-1000-8000-00805f9b34fb", DataType.RINGER_SETTING),
    RSC_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a54-0000-1000-8000-00805f9b34fb", DataType.RSC_FEATURE),
    RSC_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a53-0000-1000-8000-00805f9b34fb", DataType.RSC_MEASUREMENT),
    SC_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a55-0000-1000-8000-00805f9b34fb", DataType.SC_CONTROL_POINT),
    SCAN_INTERVAL_WINDOW(GattAttributeType.CHARACTERISTIC, "00002a4f-0000-1000-8000-00805f9b34fb", DataType.SCAN_INTERVAL_WINDOW),
    SCAN_REFRESH(GattAttributeType.CHARACTERISTIC, "00002a31-0000-1000-8000-00805f9b34fb", DataType.SCAN_REFRESH),
    SENSOR_LOCATION(GattAttributeType.CHARACTERISTIC, "00002a5d-0000-1000-8000-00805f9b34fb", DataType.SENSOR_LOCATION) {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    SERIAL_NUMBER_STRING(GattAttributeType.CHARACTERISTIC, "00002a25-0000-1000-8000-00805f9b34fb", DataType.SERIAL_NUMBER_STRING),
    SERVICE_CHANGED(GattAttributeType.CHARACTERISTIC, "00002a05-0000-1000-8000-00805f9b34fb", DataType.SERVICE_CHANGED),
    SOFTWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a28-0000-1000-8000-00805f9b34fb", DataType.SOFTWARE_REVISION_STRING),
    SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS(GattAttributeType.CHARACTERISTIC, "00002a93-0000-1000-8000-00805f9b34fb", DataType.SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS),
    SUPPORTED_NEW_ALERT_CATEGORY(GattAttributeType.CHARACTERISTIC, "00002a47-0000-1000-8000-00805f9b34fb", DataType.SUPPORTED_NEW_ALERT_CATEGORY),
    SUPPORTED_UNREAD_ALERT_CATEGORY(GattAttributeType.CHARACTERISTIC, "00002a48-0000-1000-8000-00805f9b34fb", DataType.SUPPORTED_UNREAD_ALERT_CATEGORY),
    SYSTEM_ID(GattAttributeType.CHARACTERISTIC, "00002a23-0000-1000-8000-00805f9b34fb", DataType.SYSTEM_ID),
    TEMPERATURE(GattAttributeType.CHARACTERISTIC, "00002a6e-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE),
    TEMPERATURE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a1c-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            byte[] bytes = characteristic.getValue();
            int flag = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            double celsius = (double) characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_FLOAT, 1);
            LLog.i(StringUtil.toHexString(bytes));
            if ((flag & 0x1) == 0x1) {
                LLog.i("Fahrenheit measurement with value: " + celsius + " = " + (celsius - 32.0f) * 5.0f / 9.0f + " Celsius");
                if (gatt.getDevice().getName().contains("BlueSC")) {
                    return null;
                } else {
                    celsius = (celsius - 32.0f) * 5.0f / 9.0f;
                }
            }
            if ((flag & 0x2) == 0x2) {
                if (bytes.length > 5)
                    LLog.i("Timestamp present in measurement");
                else
                    return null;
            }
            if ((flag & 0x4) == 0x4) {
                LLog.i("Temperature type present in measurement");
                if (gatt.getDevice().getName().contains("BlueSC")) return null;
            }
            LLog.i("Celsius = " + celsius);
            return new Data().add(gatt.getDevice().getAddress(), DataType.TEMPERATURE, celsius);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TEMPERATURE_TYPE(GattAttributeType.CHARACTERISTIC, "00002a1d-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE_TYPE),
    THREE_ZONE_HEART_RATE_LIMITS(GattAttributeType.CHARACTERISTIC, "00002a94-0000-1000-8000-00805f9b34fb", DataType.THREE_ZONE_HEART_RATE_LIMITS),
    TIME_ACCURACY(GattAttributeType.CHARACTERISTIC, "00002a12-0000-1000-8000-00805f9b34fb", DataType.TIME_ACCURACY),
    TIME_SOURCE(GattAttributeType.CHARACTERISTIC, "00002a13-0000-1000-8000-00805f9b34fb", DataType.TIME_SOURCE),
    TIME_UPDATE_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a16-0000-1000-8000-00805f9b34fb", DataType.TIME_UPDATE_CONTROL_POINT),
    TIME_UPDATE_STATE(GattAttributeType.CHARACTERISTIC, "00002a17-0000-1000-8000-00805f9b34fb", DataType.TIME_UPDATE_STATE),
    TIME_WITH_DST(GattAttributeType.CHARACTERISTIC, "00002a11-0000-1000-8000-00805f9b34fb", DataType.TIME_WITH_DST),
    TIME_ZONE(GattAttributeType.CHARACTERISTIC, "00002a0e-0000-1000-8000-00805f9b34fb", DataType.TIME_ZONE),
    TRUE_WIND_DIRECTION(GattAttributeType.CHARACTERISTIC, "00002a71-0000-1000-8000-00805f9b34fb", DataType.TRUE_WIND_DIRECTION),
    TRUE_WIND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a70-0000-1000-8000-00805f9b34fb", DataType.TRUE_WIND_SPEED),
    TWO_ZONE_HEART_RATE_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a95-0000-1000-8000-00805f9b34fb", DataType.TWO_ZONE_HEART_RATE_LIMIT),
    TX_POWER_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a07-0000-1000-8000-00805f9b34fb", DataType.TX_POWER_LEVEL),
    UNREAD_ALERT_STATUS(GattAttributeType.CHARACTERISTIC, "00002a45-0000-1000-8000-00805f9b34fb", DataType.UNREAD_ALERT_STATUS),
    USER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a9f-0000-1000-8000-00805f9b34fb", DataType.USER_CONTROL_POINT),
    USER_INDEX(GattAttributeType.CHARACTERISTIC, "00002a9a-0000-1000-8000-00805f9b34fb", DataType.USER_INDEX),
    UV_INDEX(GattAttributeType.CHARACTERISTIC, "00002a76-0000-1000-8000-00805f9b34fb", DataType.UV_INDEX),
    VO2_MAX(GattAttributeType.CHARACTERISTIC, "00002a96-0000-1000-8000-00805f9b34fb", DataType.VO2_MAX),
    WAIST_CIRCUMFERENCE(GattAttributeType.CHARACTERISTIC, "00002a97-0000-1000-8000-00805f9b34fb", DataType.WAIST_CIRCUMFERENCE),
    WEIGHT(GattAttributeType.CHARACTERISTIC, "00002a98-0000-1000-8000-00805f9b34fb", DataType.WEIGHT),
    WEIGHT_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a9d-0000-1000-8000-00805f9b34fb", DataType.WEIGHT_MEASUREMENT),
    WEIGHT_SCALE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a9e-0000-1000-8000-00805f9b34fb", DataType.WEIGHT_SCALE_FEATURE),
    WIND_CHILL(GattAttributeType.CHARACTERISTIC, "00002a79-0000-1000-8000-00805f9b34fb", DataType.WIND_CHILL),
    AGGREGATE(GattAttributeType.CHARACTERISTIC, "00002a5a-0000-1000-8000-00805f9b34fb", DataType.AGGREGATE),
    ANALOG(GattAttributeType.CHARACTERISTIC, "00002a58-0000-1000-8000-00805f9b34fb", DataType.ANALOG),
    DIGITAL(GattAttributeType.CHARACTERISTIC, "00002a56-0000-1000-8000-00805f9b34fb", DataType.DIGITAL),
    CHARACTERISTIC_EXTENDED_PROPERTIES(GattAttributeType.DESCRIPTOR, "00002900-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_USER_DESCRIPTION(GattAttributeType.DESCRIPTOR, "00002901-0000-1000-8000-00805f9b34fb"),
    CLIENT_CHARACTERISTIC_CONFIGURATION(GattAttributeType.DESCRIPTOR, "00002902-0000-1000-8000-00805f9b34fb"),
    SERVER_CHARACTERISTIC_CONFIGURATION(GattAttributeType.DESCRIPTOR, "00002903-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_PRESENTATION_FORMAT(GattAttributeType.DESCRIPTOR, "00002904-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_AGGREGATE_FORMAT(GattAttributeType.DESCRIPTOR, "00002905-0000-1000-8000-00805f9b34fb"),
    VALID_RANGE(GattAttributeType.DESCRIPTOR, "00002906-0000-1000-8000-00805f9b34fb"),
    EXTERNAL_REPORT_REFERENCE(GattAttributeType.DESCRIPTOR, "00002907-0000-1000-8000-00805f9b34fb"),
    REPORT_REFERENCE(GattAttributeType.DESCRIPTOR, "00002908-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_CONFIGURATION(GattAttributeType.DESCRIPTOR, "0000290b-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_MEASUREMENT(GattAttributeType.DESCRIPTOR, "0000290c-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290d-0000-1000-8000-00805f9b34fb"),
    NUMBER_OF_DIGITALS(GattAttributeType.DESCRIPTOR, "00002909-0000-1000-8000-00805f9b34fb"),
    VALUE_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290a-0000-1000-8000-00805f9b34fb"),
    TIME_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290e-0000-1000-8000-00805f9b34fb"),
    RADBEACON_DEVICENAME(GattAttributeType.CHARACTERISTIC, "0000aaa2-0000-1000-8000-00805f9b34fb", DataType.RADBEACON_DEVICE_NAME),

    TI_IRTEMPERATURE_SERVICE(GattAttributeType.SERVICE, "f000aa00-0451-4000-b000-000000000000") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_IRTEMPERATURE_DATA(GattAttributeType.CHARACTERISTIC, "f000aa01-0451-4000-b000-000000000000", new DataType[] {DataType.TEMPERATURE_IR, DataType.TEMPERATURE_OBJECT}) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            float SCALE_LSB = 0.03125f;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;

            int rawObject = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            int rawAmbient = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 2);
            double tObject = 0.25f * SCALE_LSB * rawObject;
            double tAmbient = 0.25f * SCALE_LSB * rawAmbient;

            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.TEMPERATURE_OBJECT, tObject)
                    .add(gatt.getDevice().getAddress(), DataType.TEMPERATURE_IR, tAmbient);

        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_IRTEMPERATURE_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa02-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_TEMPERATURE_CONFIG
            }
    ) {

        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{1}, "Initializer"));
                    return inits;
            }
            return null;
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_IRTEMPERATURE_DATA.getDataTypes();
        }

        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_TEMPERATURE_CONFIG, value);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    }, // 0: disable, 1: enable
    TI_IRTEMPERATURE_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa03-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_TEMPERATURE_PERIOD
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            long period = 10 * value;
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_TEMPERATURE_PERIOD, period);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    }, // Period in tens of milliseconds

    TI_ACCELEROMETER_SERVICE(GattAttributeType.SERVICE, "f000aa10-0451-4000-b000-000000000000"),
    TI_ACCELEROMETER_DATA(GattAttributeType.CHARACTERISTIC, "f000aa11-0451-4000-b000-000000000000"),
    TI_ACCELEROMETER_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa12-0451-4000-b000-000000000000"), // 0: disable, 1: enable
    TI_ACCELEROMETER_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa13-0451-4000-b000-000000000000"), // Period in tens of milliseconds

    TI_HUMIDITY_SERVICE(GattAttributeType.SERVICE, "f000aa20-0451-4000-b000-000000000000") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_HUMIDITY_DATA(GattAttributeType.CHARACTERISTIC, "f000aa21-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.HUMIDITY,
                    DataType.TEMPERATURE}) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int rawTemp = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            int rawHum = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 2);
            double temp = ((double) rawTemp / UINT16_LIMIT) * 165 - 40;
            //-- calculate relative humidity [%RH]
            double hum = ((double) rawHum / UINT16_LIMIT);
            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.HUMIDITY, hum)
                    .add(gatt.getDevice().getAddress(), DataType.TEMPERATURE, temp);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_HUMIDITY_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa22-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_HUMIDITY_CONFIG
            }
    ) {
        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{1}, "Initializer"));
                    return inits;
                default:
                    return null;
            }
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_HUMIDITY_DATA.getDataTypes();
        }


        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_HUMIDITY_CONFIG, value);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_HUMIDITY_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa23-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_HUMIDITY_PERIOD
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            long period = 10 * value;
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_HUMIDITY_PERIOD, period);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_MAGNETIC_SERVICE(GattAttributeType.SERVICE, "f000aa30-0451-4000-b000-000000000000"),
    TI_MAGNETIC_DATA(GattAttributeType.CHARACTERISTIC, "f000aa31-0451-4000-b000-000000000000"),
    TI_MAGNETIC_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa32-0451-4000-b000-000000000000"),
    TI_MAGNETIC_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa33-0451-4000-b000-000000000000"),
    TI_OPTICAL_SERVICE(GattAttributeType.SERVICE, "f000aa70-0451-4000-b000-000000000000") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_OPTICAL_DATA(GattAttributeType.CHARACTERISTIC, "f000aa71-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.LIGHT
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int rawLight = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            int mantisse = (rawLight & 0x0FFF);
            int exponent = (rawLight & 0xF000) >> 12;
            double light = 0.01 * mantisse * Math.pow(2, exponent);
            return new Data().add(gatt.getDevice().getAddress(), DataType.LIGHT, light);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_OPTICAL_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa72-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_LIGHT_CONFIG
            }
    ) {
        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{1}, "Initializer"));
                    return inits;
            }
            return null;
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_OPTICAL_DATA.getDataTypes();
        }


        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_LIGHT_CONFIG, value);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_OPTICAL_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa73-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_LIGHT_PERIOD
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            long period = 10 * value;
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_LIGHT_PERIOD, period);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_BAROMETER_SERVICE(GattAttributeType.SERVICE, "f000aa40-0451-4000-b000-000000000000") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_BAROMETER_DATA(GattAttributeType.CHARACTERISTIC, "f000aa41-0451-4000-b000-000000000000", new DataType[] { DataType.PRESSURE }) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            byte value[] = characteristic.getValue();
            int rawValue = Numbers.getUInt24(value, 3);
            double pres = 1.0 * rawValue;
            return new Data().add(gatt.getDevice().getAddress(), DataType.PRESSURE, pres);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_BAROMETER_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa42-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_BAROMETER_CONFIG
            }
    ) {
        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{1}, "Initializer"));
                    return inits;
            }
            return null;
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_BAROMETER_DATA.getDataTypes();
        }

        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);

            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_BAROMETER_CONFIG, value);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_BAROMETER_CALIBRATION(GattAttributeType.CHARACTERISTIC, "f000aa43-0451-4000-b000-000000000000") {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            LLog.e("Not implemented yet!!!!");
            return null;
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_BAROMETER_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa44-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_BAROMETER_PERIOD
            }
    )  {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            long period = 10 * value;
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_BAROMETER_PERIOD, period);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_GYROSCOPE_SERVICE(GattAttributeType.SERVICE, "f000aa50-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_DATA(GattAttributeType.CHARACTERISTIC, "f000aa51-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa52-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa53-0451-4000-b000-000000000000"),
    TI_IO_SERVICE(GattAttributeType.SERVICE, "f000aa64-0451-4000-b000-000000000000"){
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_IO_DATA(GattAttributeType.CHARACTERISTIC, "f000aa65-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_IO_DATA
            }
    ) {
        //        @Override
//        public byte[] initValue() {
//            return new byte[]{IO_REDLED};
//        }
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int io = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            IOService service = ioservices.get(gatt.getDevice().getAddress());
            if (service != null) {
                switch (service) {
                    case LOCAL:
                        break;
                    case REMOTE:
                        if ((io & 0x1) > 0) LLog.i("Red led ON");
                        else LLog.i("Red led OFF");
                        if ((io & 0x2) > 0) LLog.i("Green led ON");
                        else LLog.i("Green led OFF");
                        if ((io & 0x4) > 0) LLog.i("Buzzer ON");
                        else LLog.i("Buzzer OFF");
                        break;
                    case TEST:
                        if ((io & 0x1) > 0) LLog.i("IR Temperature sensor started correctly");
                        if ((io & 0x2) > 0) LLog.i("Humidity sensor started correctly");
                        if ((io & 0x4) > 0) LLog.i("Optical sensor started correctly");
                        if ((io & 0x8) > 0) LLog.i("Pressure sensor started correctly");
                        if ((io & 0x10) > 0) LLog.i("Pressure sensor started correctly");
                        if ((io & 0x20) > 0)
                            LLog.i("Gyroscope and Accelerometer started correctly");
                        if ((io & 0x40) > 0) LLog.i("Magnetometer started correctly");
                        if ((io & 0x80) > 0) LLog.i("Unknown sensor started correctly");
                        break;
                }
            }
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_IO_DATA, io);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_IO_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa66-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_IO_CONFIG
            }
    ) {
        //        @Override
//        public byte[] initValue() {
//            return new byte[]{IO_LOCAL};
//        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_IO_DATA.getDataTypes();
        }

        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            IOService service = IOService.fromOrdinal(value);
            ioservices.put(gatt.getDevice().getAddress(), service);
            if (service != null) return new Data().add(gatt.getDevice().getAddress(), DataType.TI_IO_CONFIG, service.toString());
            return null;
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_REGISTER_SERVICE(GattAttributeType.SERVICE, "f000ac00-0451-4000-b000-000000000000"),
    TI_REGISTER_DATA(GattAttributeType.CHARACTERISTIC, "f000ac01-0451-4000-b000-000000000000"),
    TI_REGISTER_ADDRESS(GattAttributeType.CHARACTERISTIC, "f000ac02-0451-4000-b000-000000000000"),
    TI_REGISTER_DEV(GattAttributeType.CHARACTERISTIC, "f000ac03-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_SERVICE(GattAttributeType.SERVICE, "f000ccc0-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR1(GattAttributeType.CHARACTERISTIC, "f000ccc1-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR2(GattAttributeType.CHARACTERISTIC, "f000ccc2-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR3(GattAttributeType.CHARACTERISTIC, "f000ccc3-0451-4000-b000-000000000000"),
    TI_OAD_SERVICE(GattAttributeType.SERVICE, "f000ffc0-0451-4000-b000-000000000000"),
    TI_OAD_IMG_IDENTIFY(GattAttributeType.CHARACTERISTIC, "f000ffc1-0451-4000-b000-000000000000"),
    TI_OAD_IMG_BLOCK(GattAttributeType.CHARACTERISTIC, "f000ffc2-0451-4000-b000-000000000000"),
    TI_MOVEMENT_SERVICE(GattAttributeType.SERVICE, "f000aa80-0451-4000-b000-000000000000") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_MOVEMENT_DATA(GattAttributeType.CHARACTERISTIC, "f000aa81-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.ACCELERATION,
                    DataType.MAGNETIC_FLUX_DENSITY_3D,
                    DataType.GYROSCOPE
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
//            byte[] values = characteristic.getValue();
            float scale = 65536f / 500f;
            float[] gyro = new float[]{
                    1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0) / scale,
                    1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 2) / scale,
                    1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 4) / scale
            };
            Integer range = accelerationRange.get(gatt.getDevice().getAddress());
            if (range == null) range = 8;
            scale = (float) (32768 / range);
            float[] accel = new float[]{
                    9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 6) / scale,
                    9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 8) / scale,
                    9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 10) / scale
            };
            scale = (float) (32768 / 4912);
            float[] magn = new float[]{
                    -1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 12) / scale,
                    1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 14) / scale,
                    -1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 16) / scale
            };
            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.ACCELERATION, accel)
                    .add(gatt.getDevice().getAddress(), DataType.MAGNETIC_FLUX_DENSITY_3D, magn)
                    .add(gatt.getDevice().getAddress(), DataType.GYROSCOPE, gyro);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_MOVEMENT_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa82-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_ON,
                    DataType.TI_MOVEMENT_CONFIG_GYRO_ON,
                    DataType.TI_MOVEMENT_CONFIG_MAGNETOMETER_ON,
                    DataType.TI_MOVEMENT_CONFIG_WAKE_ON_MOTION,
                    DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE
            }
    )  {
        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{(byte) (GYRO_ENABLE | ACCEL_ENABLE | MAGNET_ENABLE | WAKE_ENABLE), (byte) ACCEL_RANGE_2G}, "Initializer"));
                    return inits;
            }
            return null;
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.TI_MOVEMENT_DATA.getDataTypes();
        }

        @Override
        public GattAttribute characteristicToReadAfter() {
            return TI_MOVEMENT_DATA;
        }

        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int gyro[] = new int[]{
                    getBitValue(characteristic, 0, 1),
                    getBitValue(characteristic, 1, 1),
                    getBitValue(characteristic, 2, 1)
            };

            int accel[] = new int[]{
                    getBitValue(characteristic, 3, 1),
                    getBitValue(characteristic, 4, 1),
                    getBitValue(characteristic, 5, 1)
            };
            int magnet = getBitValue(characteristic, 6, 1);
            int wake = getBitValue(characteristic, 7, 1);
            int range;
            accelerationRange.put(gatt.getDevice().getAddress(), range = Numbers.pow(2, 1 + getBitValue(characteristic, 8, 2)));
            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_CONFIG_GYRO_ON, gyro)
                    .add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_ON, accel)
                    .add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_CONFIG_MAGNETOMETER_ON, magnet)
                    .add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_CONFIG_WAKE_ON_MOTION, wake)
                    .add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE, range * 9.81);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_MOVEMENT_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa83-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.TI_MOVEMENT_PERIOD
            }
    ) {
        @Override
        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    //minimum =   10 ms = 0x0A
                    //maximum = 2550 ms = 0xFF
                    inits.add(new Tuple<>(new byte[]{(byte) 0xB}, "Initializer"));
                    return inits;
            }
            return null;
        }

        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            long period = 10 * value;
            return new Data().add(gatt.getDevice().getAddress(), DataType.TI_MOVEMENT_PERIOD, period);
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_KEY_SERVICE(GattAttributeType.SERVICE, "0000ffe0-0000-1000-8000-00805f9b34fb") {
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    TI_KEY_DATA(GattAttributeType.CHARACTERISTIC, "0000ffe1-0000-1000-8000-00805f9b34fb",
            new DataType[] {
                    DataType.TI_LEFT_KEY,
                    DataType.TI_RIGHT_KEY,
                    DataType.TI_REED_RELAY
            }) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            int rawKeys = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
//            LLog.i("KEYS: " + StringUtil.toHexString(rawKeys));
            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.TI_LEFT_KEY, (rawKeys & 0x1))
                    .add(gatt.getDevice().getAddress(), DataType.TI_RIGHT_KEY, (rawKeys & 0x2) >> 1)
                    .add(gatt.getDevice().getAddress(), DataType.TI_REED_RELAY, (rawKeys & 0x4) >> 2);
        }
        @Override
        public boolean isSupported() {
            return true;
        }
    },
    SOAP_SERVICE(GattAttributeType.SERVICE, "f000da90-0451-4000-b000-000000000000") {

    },
    SOAP_DATA(GattAttributeType.CHARACTERISTIC, "f000da91-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.ROTATION_ACCELERO,
                    DataType.ROTATION,
                    DataType.ROTATION_RAW,
                    DataType.SOAPSENSE_TOTAL_STROKES,
                    DataType.ACCELERATION,
                    DataType.SOAPSENSE_PARTIAL_STROKES_REFILL,
                    DataType.SOAPSENSE_REFILLS,
                    DataType.SOAPSENSE_USED_VOLUME_ACCELERO,
                    DataType.SOAPSENSE_USED_COUNT,
                    DataType.SOAPSENSE_MIN_ANGLE
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            //LLog.i("Reading Soap data");
            int angleX = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, BleConst.ACTUATOR_ANGLE_OFFSET);
            int angleX2 = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, BleConst.ACTUATOR_ANGLE2_OFFSET);
            int angleRaw = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.RAW_ANGLE_OFFSET);
            int totalStrokes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.TOTAL_STROKES_OFFSET) +
                    (characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.TOTAL_STROKES_OFFSET + 1) << 8);
            int acceleration = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.ACCELERATION_OFFSET);
            int partialStrokes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.PARTIAL_STROKES_REFILL_OFFSET);
            int refills = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.REFILLS_OFFSET);
            int usedVolume = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.USED_VOLUME_OFFSET);
            int usedCount = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.USED_COUNT_OFFSET);
            int minAngle = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.MIN_ANGLE_OFFSET);

            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.ROTATION_ACCELERO, 0.01f * angleX)
                    .add(gatt.getDevice().getAddress(), DataType.ROTATION, 0.01f * angleX2)
                    .add(gatt.getDevice().getAddress(), DataType.ROTATION_RAW, 0.1f * angleRaw)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_TOTAL_STROKES, totalStrokes)
                    .add(gatt.getDevice().getAddress(), DataType.ACCELERATION, 9.81f * acceleration / (32768 / 2))
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_PARTIAL_STROKES_REFILL, partialStrokes)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_REFILLS, refills)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_USED_VOLUME_ACCELERO, 0.00002f * usedVolume)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_USED_COUNT, usedCount)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_MIN_ANGLE, 0.01 * minAngle)
                    ;

        }
    },
    SOAP_DATA2(GattAttributeType.CHARACTERISTIC, "f000da92-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.ROTATION_ACCELERO_MIN,
                    DataType.ROTATION_ACCELERO_MAX,
                    DataType.SOAPSENSE_TIME_SINCE_REFILL,
                    DataType.TIME_ACTIVITY,
                    DataType.BATTERY_LEVEL,
                    DataType.SOAPSENSE_CALIBRATION_RESULT
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            Data data = new Data();
            int magnetPartialStrokes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.MAGNET_PARTIAL_STROKES_REFILL_OFFSET);
            if ((magnetPartialStrokes & 0x8000) == 0x8000) {
                int magnetUsedVolume = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, BleConst.MAGNET_USED_VOLUME_OFFSET);
                int magnetStartAngle = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.MAGNET_START_ANGLE_OFFSET);
                int magnetEndAngle = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.MAGNET_END_ANGLE_OFFSET);
                data.add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_PARTIAL_STROKES_REFILL_2, magnetPartialStrokes & 0x7fff)
                        .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_USED_VOLUME, 0.00002f * magnetUsedVolume)
                        .add(gatt.getDevice().getAddress(), DataType.ROTATION_MIN, 0.1f * magnetStartAngle)
                        .add(gatt.getDevice().getAddress(), DataType.ROTATION_MAX, 0.1f * magnetEndAngle);
            } else {
                int rotSpeed = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, BleConst.ROT_SPEED_OFFSET);
                int rotSpeedOff = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, BleConst.ROT_SPEED_OFF_OFFSET);
                int stdDev = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.STD_DEV_OFFSET);
                int stdDevRot = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.STD_DEV_ROT_OFFSET);
                data.add(gatt.getDevice().getAddress(), DataType.ROTATION_SPEED, 0.01 * rotSpeed)
                        .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_ROTATION_SPEED_OFFSET, 0.01 * rotSpeedOff)
                        .add(gatt.getDevice().getAddress(), DataType.STANDARD_DEVIATION, 0.1 * stdDev)
                        .add(gatt.getDevice().getAddress(), DataType.STANDARD_DEVIATION_2, 0.1 * stdDevRot);
            }
            int startAngle = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.START_ANGLE_OFFSET);
            int endAngle = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.END_ANGLE_OFFSET);
            int calibration = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.CALIBRATION_OFFSET);

            long timeSinceRefill = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.HOURS_SINCE_REFILL_ATTR_OFFSET);
            if ((timeSinceRefill & 0x8000) == 0x8000)
                timeSinceRefill = 1000 * (timeSinceRefill & 0x7fff);
            else
                timeSinceRefill = timeSinceRefill * 3600000;

            long tenthsAlive = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.ALIVE_ATTR_OFFSET) +
                    (characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, BleConst.ALIVE_ATTR_OFFSET + 1) << 8);
            if ((tenthsAlive & 0x800000) == 0x800000)
                tenthsAlive = 100 * (tenthsAlive & 0x7fffff);
            else
                tenthsAlive *= 86400000;

            int battery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, BleConst.BATTERY_ATTR_OFFSET);

            data.add(gatt.getDevice().getAddress(), DataType.ROTATION_ACCELERO_MIN, 0.1f * startAngle)
                    .add(gatt.getDevice().getAddress(), DataType.ROTATION_ACCELERO_MAX, 0.1f * endAngle)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_TIME_SINCE_REFILL, timeSinceRefill)
                    .add(gatt.getDevice().getAddress(), DataType.TIME_ACTIVITY, tenthsAlive)
                    .add(gatt.getDevice().getAddress(), DataType.BATTERY_LEVEL, 0.1f * battery)
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_CALIBRATION_RESULT, calibration)
            ;
            return data;
        }
    },
    SOAP_CONFIG(GattAttributeType.CHARACTERISTIC, "f000da93-0451-4000-b000-000000000000",
            new DataType[] {
                    DataType.SOAPSENSE_REFILL_COMMAND
            }
    ) {
        @Override
        public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;

            return new Data()
                    .add(gatt.getDevice().getAddress(), DataType.SOAPSENSE_REFILL_COMMAND, characteristic.getValue());
        }
    },
    SMARTPULSE_SERVICE(GattAttributeType.SERVICE, "0000970d-0000-1000-8000-00805f9b34fb") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    SMARTPULSE_CHECKER(GattAttributeType.CHARACTERISTIC, "00004a36-0000-1000-8000-00805F9B34FB",
            new DataType[] {
                    DataType.HEART_RATE,
                    DataType.PULSE_DATA
            }) {
        @Override
        public boolean needsReadAfterWrite() {
            return false;
        }

        private void handleData(String sensorId, Data data, int value, int interval) {
            smartPulseData.count++;
            //if (smartPulseData.count % 100 == 0) LLog.i("Counted " + smartPulseData.count + " pulses" );
            data.add(sensorId, DataType.PULSE_DATA, 4096 - value, true);
            if (interval > 0) {
                data.add(sensorId, DataType.HEART_RATE, 60000 / interval);
//                LLog.i("Pulse data: value = " + (4096 - value) + " heart rate = " + 60000 / interval + ", count = " + smartPulseData.count);
            }

        }

        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            byte[] val = characteristic.getValue();
            if ((val[0] & 255) == 153) {
                smartPulseData.dataMode = 1;
                smartPulseData.dataIndex = 0;
                smartPulseData.dataSetChanged = true;
            }
//            if (smartPulseData.count == 0) {
//                smartPulseData.count++;
//                if (smartPulseData.count % 100 == 0) LLog.i("Counted " + smartPulseData.count + " pulses" );
//            }
            Data data = new Data();
            try {
                //0x5504047E0000
                //LLog.i("Receiving: " + StringUtil.toHex(characteristic.getValue()));
                if (val[0] == (byte) 85 && val[1] == (byte) 4) {
                    smartPulseData.dataCnt++;
                    handleData(
                            gatt.getDevice().getAddress(),
                            data,
                            ((val[2] & 255) * 256) + (val[3] & 255),
                            ((val[4] & 255) * 256) + (val[5] & 255)
                    );

                } else if (smartPulseData.dataMode > 0 && val.length == 9) {
                    for (int i = 0; i < 4; i++) {
                        smartPulseData.dataCnt++;
                        handleData(
                                gatt.getDevice().getAddress(),
                                data,
                                ((val[(i * 2) + 1] & 255) * 256) + (val[(i * 2) + 2] & 255),
                                0);
                    }
                    smartPulseData.preReceiveTime = System.currentTimeMillis();
                    smartPulseData.dataMode--;

                } else if (smartPulseData.dataMode > 0 && val.length == 11) {
                    for (int i = 0; i < 4; i++) {
                        smartPulseData.dataCnt++;
                        int hrval = ((val[(i * 2) + 1] & 255) * 256) + (val[(i * 2) + 2] & 255);
                        if (i == 3) {
                            handleData(
                                    gatt.getDevice().getAddress(),
                                    data,
                                    hrval,
                                    ((val[9] & 255) * 256) + (val[10] & 255));
                        } else {
                            handleData(
                                    gatt.getDevice().getAddress(),
                                    data,
                                    hrval,
                                    0);
                        }
                    }
                    smartPulseData.preReceiveTime = System.currentTimeMillis();
                    smartPulseData.dataMode--;
                } else {
                    LLog.i("Unhandled Smart Pulse data " + StringUtil.toHex(val));
                }
            } catch (Exception e) {
                LLog.e("Error in handling SmartPulse data", e);
                return null;
            }
            return data;
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    SMARTPULSE_COMM(GattAttributeType.CHARACTERISTIC, "00004A35-0000-1000-8000-00805F9B34FB") {
        @Override public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    //inits.add(new Tuple<>(new byte[]{(byte)  119, (byte) 1, (byte) 6, (byte) 0, (byte) 6, (byte)  0, (byte)    8, (byte) 0, (byte) 0}, "Interval P"));
                    inits.add(new Tuple<>(new byte[]{(byte) 119, (byte) 1, (byte) 6, (byte) 0, (byte) 12, (byte) 0, (byte) 30, (byte) 0, (byte) 0}, "Interval P"));
                    //inits.add(new Tuple<>(new byte[]{(byte) -120, (byte) 1, (byte) 6, (byte) 0, (byte) 6, (byte) 12, (byte) -128, (byte) 0, (byte) 0}, "Interval S"));
                    inits.add(new Tuple<>(new byte[]{(byte)-120, (byte)1, (byte)6, (byte)0, (byte)12, (byte)0, (byte)30, (byte)0, (byte)0}, "Interval S"));
                    //inits.add(new Tuple<>(new byte[]{(byte) -112, (byte) 1, (byte) 6, (byte) 0, (byte) 0, (byte) 19, (byte) -120, (byte) 0, (byte) 0}, "Multiplier change T"));
                    //inits.add(new Tuple<>(new byte[]{(byte) -111, (byte) 1, (byte) 6, (byte) 0, (byte) 0, (byte) 22, (byte)  -88, (byte) 0, (byte) 0}, "Multiplier change I"));
                    return inits;
                case START:
                    inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{(byte) -103, (byte) 1, (byte) 0}, "Packet size change"));
                    inits.add(new Tuple<>(convertValues(4, 0), "Start cmd"));
                    return inits;
                case STOP:
                    inits = new ArrayList<>();
                    //inits.add(new Tuple<>(new byte[]{(byte) -103, (byte) 1, (byte) 0}, "Packet size change"));
                    inits.add(new Tuple<>(convertValues(5, 0), "Stop cmd"));
                    inits.add(new Tuple<>(new byte[]{(byte) -35, (byte) 1, (byte) 0}, "Reset connection")); // Was: DD, now: Reset connection
                    return inits;
                case RESET:
                    inits = new ArrayList<>();
                    inits.add(new Tuple<>(new byte[]{(byte) -35, (byte) 1, (byte) 0}, "Reset connection")); // Was: DD, now: Reset connection
                    return inits;

            }
            return null;
        }

        public boolean initAlways() {
            return true;
        }

        @Override public DataType[] getReferencedDataTypes() {
            return GattAttribute.SMARTPULSE_CHECKER.getDataTypes();
        }

        @Override public boolean needsReadAfterWrite() {
            return false;
        }

        @Override public byte[] convertValues(int... values) {
            if (values == null || values.length < 2) return super.convertValues(values);
            int ecode = values[0];
            int offset = values[1];
            byte[] val;
            int eecode = 0;
//            int wimode = 0;
            if (ecode == 3) {
                Calendar calendar = Calendar.getInstance();
                val = new byte[]{(byte) 104, (byte) 1, (byte) 6,
                        (byte) ((calendar.get(Calendar.YEAR) - 2000) & 255),
                        (byte) ((calendar.get(Calendar.MONTH) + 1) & 255),
                        (byte) (calendar.get(Calendar.DAY_OF_MONTH) & 255),
                        (byte) (calendar.get(Calendar.HOUR_OF_DAY) & 255),
                        (byte) (calendar.get(Calendar.MINUTE) & 255),
                        (byte) (calendar.get(Calendar.SECOND) & 255)};
                //Log.i(f190TAG, "setting : " + val[3] + ", " + val[4] + ", " + val[5] + ", " + val[6]);
            } else {
                //Log.i(f190TAG, "exec : " + ecode + ", " + offset);
                switch (ecode) {
                    case 0:
                        eecode = 221;
//                        wimode = 3;
                        break;
                    case 1:
                        eecode = offset + 49;
//                        wimode = 1;
                        break;
                    case 2:
                        eecode = offset + 97;
//                        wimode = 5;
                        break;
                    case 4:
                        eecode = 85;
//                        wimode = 2;
                        break;
                    case 5:
                        eecode = 170;
//                        wimode = 3;
                        break;
                    case 6:
                        eecode = offset + 150;
//                        wimode = 3;
                        //Log.i(f190TAG, "send BLEManagerGoogle 0x65");
                        break;
                }
                val = new byte[]{(byte) (eecode & 255), (byte) 1, (byte) 0};
            }
            return val;
        }

        @Override public boolean isSupported() {
            return true;
        }
    },
    INBALANCE_READ_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "271c3db0-3e8a-11e6-bdf4-0800200c9a66",
            new DataType[] {
                    DataType.AGE,
                    DataType.BODY_MASS_INDEX,
                    DataType.FAT_RATIO,
                    DataType.HEIGHT,
                    DataType.MUSCLE_RATIO,
                    DataType.PROTEIN,
                    DataType.VISCERAL_FAT_AREA,
                    DataType.WAIST_HIP_RATIO,
                    DataType.WEIGHT,
            }) { //Server characteristic
        @Override public boolean hasDescriptor() {
            return false;
        }

        @Override public boolean needsReadAfterWrite() {
            return false;
        }

        @Override public boolean mayRead() {
            return false;
        }


        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null;
            byte[] value = characteristic.getValue();
            Data data = null;
            if (value != null && value.length > 0) {
                String str = new String(value, 0, value.length);
                LLog.i("ReceivedData00 :: " + str);
                if (str.equals("NAME")) {
                    //this.ibBleManager.onBtConnectResult(6, str);
                    inbalanceDataFlag = 0;
                } else if (value[0] == (byte) 89 && value[1] == (byte) 2 && value[2] == (byte) 0 && value[3] == (byte) 0) {
                    inbalanceDataFlag = 1;
                    if (inbalanceData.size() == 0 || !Arrays.equals(inbalanceData.get(inbalanceData.size() - 1), value))
                        inbalanceData.add(value);
                    else
                        LLog.i("Skipping double " + StringUtil.toHex(value));
                } else if (inbalanceDataFlag == 1) {
                    String str2 = "";
                    if (inbalanceData.size() > 1) {
                        byte[] bArr = inbalanceData.get(inbalanceData.size() - 1);
                        str2 = new String(bArr, 0, bArr.length);
                    }
                    if (inbalanceData.size() == 0 || !Arrays.equals(inbalanceData.get(inbalanceData.size() - 1), value))
                        inbalanceData.add(value);
                    else
                        LLog.i("Skipping double " + StringUtil.toHex(value));
                    if (str.contains("EPC") || (str2 + str).contains("EPC")) {
                        String str3 = "";
                        for (byte[] bytes : inbalanceData) {
                            str3 += new String(bytes, 0, bytes.length);

                        }
                        String[] results = str3.split("#");
                        int i = 0;
                        Stringer stringer = new Stringer("\n");
                        data = new Data();
                        double skeletalMuscleMass = 0;
                        double weight = 0;
                        for (String result: results) {
                            InbalanceDataType ibdt = InbalanceDataType.fromOrdinal(i++);
                            stringer.append(ibdt + " = " + result);
                            try {
                                switch (ibdt) {
                                    case age:
                                        data.add(gatt.getDevice().getAddress(), DataType.AGE, Integer.valueOf(result));
                                        break;
                                    case bmi:
                                        data.add(gatt.getDevice().getAddress(), DataType.BODY_MASS_INDEX, Double.valueOf(result));
                                        break;
                                    case pbf:
                                        data.add(gatt.getDevice().getAddress(), DataType.FAT_RATIO, Double.valueOf(result) / 100.0);
                                        break;
                                    case height:
                                        data.add(gatt.getDevice().getAddress(), DataType.HEIGHT, Double.valueOf(result) / 100.0);
                                        break;
                                    case smm:
                                        skeletalMuscleMass = Double.valueOf(result);
                                        data.add(gatt.getDevice().getAddress(), DataType.MUSCLE_RATIO, Double.valueOf(result));
                                        break;
//                                    case stdProtein:
//                                        data.addWatchable(gatt.getDevice().getAddress(), DataType.PROTEIN, Double.valueOf(result));
//                                        break;
                                    case vfa:
                                        data.add(gatt.getDevice().getAddress(), DataType.VISCERAL_FAT_AREA, Double.valueOf(result));
                                        break;
                                    case weight:
                                        weight = Double.valueOf(result);
                                        data.add(gatt.getDevice().getAddress(), DataType.WEIGHT, weight);
                                        break;
                                    case whr:
                                        data.add(gatt.getDevice().getAddress(), DataType.WAIST_HIP_RATIO, Double.valueOf(result));
                                        break;
                                }
                            } catch (NumberFormatException e) {
                                LLog.e("Could not convert " + result + " for " + ibdt + " to a number");
                            }
                        }
                        if (weight != 0 && skeletalMuscleMass != 0) {
                            data.add(gatt.getDevice().getAddress(), DataType.MUSCLE_RATIO, skeletalMuscleMass / weight);
                        }
                        inbalanceData.clear();
                        data.add(gatt.getDevice().getAddress(), DataType.INFO, stringer.toString()).add(gatt.getDevice().getAddress(), DataType.EOF, 0);
                        //LLog.i("BCA Results:" + data);

                    }
                }
            }
            return data;
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    INBALANCE_WRITE_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "1e678ad0-3e8a-11e6-bdf4-0800200c9a66") { // Terminal characteristic
        @Override public boolean hasDescriptor() {
            return false;
        }

        @Override public boolean needsReadAfterWrite() {
            return false;
        }

        @Override public boolean mayRead() {
            return false;
        }

        public int getWriteType() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                return BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;
            } else
                return 0x1;
        }

        public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case START:
                    String userAge = "" + PreferenceKey.AGE.getInt();
                    String userLength = "" + PreferenceKey.BODY_LENGTH.getInt();
                    String userId = "M";// + (int) (10 + 80 * random());
                    String userSex = PreferenceKey.getGender().toString(); //.substring(0,1).toUpperCase();

                    String delim = "#";
                    String userMsg = delim + userId + delim + userSex + delim + userAge + delim + userLength + delim;
                    ByteBuffer byteBuffer = ByteBuffer.allocate(userMsg.length() + 4); //6);
                    byteBuffer.put((byte) 88);
                    byteBuffer.put((byte) 73);
                    byteBuffer.put((byte) 66);
                    byteBuffer.put((byte) 2);
//                    byteBuffer.put("XIBSTX".getBytes(Charset.forName("UTF-8")));
                    byteBuffer.put(userMsg.getBytes(Charset.forName("UTF-8")));
                    byte userDataBytes[] = byteBuffer.array();

                    String msg = delim + "STX" + delim + userDataBytes.length + delim;
                    byteBuffer = ByteBuffer.allocate(msg.length());
                    byteBuffer.put(msg.getBytes(Charset.forName("UTF-8")));

                    List<Tuple<byte[], String>> inits = new ArrayList<>();
                    inits.add(new Tuple<>(byteBuffer.array(), "Init user data"));

                    if (userDataBytes.length > 20) {
                        ByteBuffer allocate2 = ByteBuffer.allocate(20);
                        int i = 1;
                        int iPart = 0;
                        while (i <= userDataBytes.length) {
                            allocate2.put(userDataBytes[i - 1]);
                            if (i != 0 && i % 20 == 0) {
                                inits.add(new Tuple<>(allocate2.array(), "User data, part " + ++iPart));
                                allocate2 = ByteBuffer.allocate(20);
                            }
                            i++;
                        }
                        if (userDataBytes.length % 20 > 0) {
                            allocate2 = ByteBuffer.allocate(userDataBytes.length % 20);
                            for (i = ((userDataBytes.length / 20) * 20) + 1; i <= userDataBytes.length; i++) {
                                allocate2.put(userDataBytes[i - 1]);
                            }
                            inits.add(new Tuple<>(allocate2.array(), "User data, part " + ++iPart));
                        }
                    } else
                        inits.add(new Tuple<>(userDataBytes, "User data"));
                    return inits;
            }
            return null;
        }
        @Override public boolean isSupported() {
            return true;
        }
    },
    NORDIC_AND_TAIDOC_SERVICE(GattAttributeType.SERVICE, "00001523-1212-efde-1523-785feabcd123") {
        @Override public boolean isSupported() {
            return true;
        }
    },
    NORDIC_AND_TAIDOC_CHARACTERISITC(GattAttributeType.CHARACTERISTIC, "00001524-1212-efde-1523-785feabcd123") {
        private byte[] addCheck(byte[] bytes) {
            int sum = 0;
            for (int i = 0; i < bytes.length - 1; i++) {
                sum += bytes[i];
            }
            bytes[bytes.length - 1] = (byte) (sum & 0xFF);
            return bytes;
        }

        @Override public List<Tuple<byte[], String>> commands(CommandType cmd) {
            switch (cmd) {
                case INIT:
//                    List<Tuple<byte[], String>> inits = new ArrayList<>();
//                    inits.add(new Tuple<>(addCheck(new byte[]{(byte) 0x51, (byte)  0x24, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0xA3, (byte) 0}), "Read device model"));
//                    inits.add(new Tuple<>(addCheck(new byte[]{(byte) 0x51, (byte)  0x23, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0xA3, (byte) 0}), "Read device time"));
                    return null;
                case START:
                    List<Tuple<byte[], String>> inits = new ArrayList<>();
//                    inits.add(new Tuple<>(addCheck(new byte[]{(byte) 0x51, (byte)  0x23, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0xA3, (byte) 0}), "Read device time"));
                    inits.add(new Tuple<>(addCheck(new byte[]{(byte) 0x51, (byte)  0x43, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0xA3, (byte) 0}), "Start measurement"));
                    return inits;
                case STOP:
                    inits = new ArrayList<>();
                    inits.add(new Tuple<>(addCheck(new byte[]{(byte) 0x51, (byte)  0x50, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0xA3, (byte) 0}), "Stop"));
                    return inits;
                case RESET:
                    break;
            }
            return null;
        }

        public boolean initAlways() {
            return true;
        }

        @Override public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                LLog.i("Nordic / Taidoc characteristic value: " + StringUtil.toHexString(characteristic.getValue()));
            }
            return null;
        }

        @Override public boolean isSupported() {
            return true;
        }

        @Override public boolean needsReadAfterWrite() {
            return false;
        }
    },

    ;

    private final static int UINT16_LIMIT = 65536;
    private final static long UINT32_LIMIT = 4294967296L;

    private final static int GYRO_ENABLE = 0x07;
    private final static int ACCEL_ENABLE = 0x38;
    private final static int MAGNET_ENABLE = 0x40;
    private final static int WAKE_ENABLE = 0x80;
    private final static int ACCEL_RANGE_2G = 0x0;
//    private final static int ACCEL_RANGE_4G = 0x1;
//    private final static int ACCEL_RANGE_8G = 0x2;
//    private final static int ACCEL_RANGE_16G = 0x4;
//
//    public final static byte IO_LOCAL = 0x01;
//    public final static byte IO_REMOTE = 0x02;
//    public final static byte IO_TEST = 0x04;
//
//    public final static byte IO_REDLED = 0x01;
//    public final static byte IO_GREENLED = 0x02;
//    public final static byte IO_BUZZER = 0x04;



    private final UUID uuid;
    private final GattAttributeType type;
    private final DataType[] dataTypes;
    private static Map<String, Integer> accelerationRange = new HashMap<>();
    private static Map<String, IOService> ioservices = new HashMap<>();
    static class RefData {
        long prevTimeCalled = -1;
    }
    private static SmartPulseData smartPulseData = new SmartPulseData();
    private static int inbalanceDataFlag = 0;
    private static List<byte[]> inbalanceData = new ArrayList<>();
    private static Map<String, RefData> refData = new HashMap<>();
    private static EnumMap<GattAttribute, ParseDelegate> parseDelegates = new EnumMap<>(GattAttribute.class);

    static class WheelDataItem extends RefData {
        private int prevWheelRevolutions = -1;
        private int prevCrankRevolutions = -1;
        private int prevWheelEventTime = -1;
        private int prevCrankEventTime = 1;
        private double wheelRpm = 0;
        private double crankRpm = 0;
        double distance = -1;
        double dummyDistance = 0;
//        double calibrationWheelDistance = 0;
//        double calibrationGpsDistance = 0;
        double powerAvg = 0.0;
        int energyAtStart = 0;
        long prevTime = System.currentTimeMillis();

        void reset() {
//            dummyDistance = 0;
//            distance = 0;
            prevWheelRevolutions = -1;
            prevCrankRevolutions = -1;
            prevWheelEventTime = -1;
            prevCrankEventTime = 1;
            wheelRpm = 0;
            crankRpm = 0;
            powerAvg = 0;
            energyAtStart = 0;
            prevTime = System.currentTimeMillis();
            prevTimeCalled = System.currentTimeMillis();
        }
    }

    static class SmartPulseData {
        int dataMode = 0;
        int dataIndex = 0;
        boolean dataSetChanged = true;
        int count;
        int dataCnt = 0;
        long preReceiveTime = 0;
    }

//public static boolean SHOW_ALL = true;

    public enum IOService {
        LOCAL,
        REMOTE,
        TEST;
        public static IOService fromOrdinal(int ord) {
            if (ord >= 0 && ord < IOService.values().length) return IOService.values()[ord];
            return null;
        }
    }

    public interface ParseDelegate {
        Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic);
    }

    GattAttribute(final GattAttributeType type, final String sUUID, final DataType[] dataTypes) {
        this.uuid = UUID.fromString(sUUID);
        //this.sUUID = sUUID;
        this.type = type;
        this.dataTypes = dataTypes;
    }

    GattAttribute(final GattAttributeType type, final String sUUID, final DataType dataType) {
        this(type, sUUID, new DataType[] {dataType});
    }

    GattAttribute(final GattAttributeType type, final String sUUID ) {
        this(type, sUUID, (DataType[]) null);
    }

    private static int getBitValue(BluetoothGattCharacteristic characteristic, int pos, int bits) {
        return Numbers.getBits(characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0), pos, bits);
    }

    public void setParseDelegate(ParseDelegate delegate) {
        parseDelegates.put(this, delegate);
    }

    public DataType[] getDataTypes() {
        return dataTypes;
    }

    //    public static String stringFromUUID(String uuid, GattAttributeType type) {
    //    	for (GattAttribute attr : GattAttribute.values()) if (attr.sUUID.equals(uuid)) return attr.label;
    //        return UNKNOWN_CHARACTERISTIC.label + " " + type + " " + uuid;
    //    }

    public static GattAttribute fromUUID(UUID uuid) {
        for (GattAttribute attr : GattAttribute.values()) if (attr.uuid.equals(uuid)) return attr;
        return UNKNOWN;
    }

    public static GattAttribute fromString(String value) {
        for (GattAttribute attr : GattAttribute.values()) if (attr.toString().toUpperCase().equals(value.toUpperCase())) return attr;
        return UNKNOWN;
    }

    private static String capitalize(final String line) {
        if (line == null) return null;
        if (line.length() < 2) return line.toUpperCase();
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static String stringFromUUID(UUID uuid) {
        for (GattAttribute attr : GattAttribute.values())
            if (attr.uuid.equals(uuid)) return attr.toString();
        return UNKNOWN + " " + uuid;
    }

    public static GattAttribute fromCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) for (GattAttribute attr : GattAttribute.values())
            if (attr.uuid.equals(characteristic.getUuid())) return attr;
        return UNKNOWN;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return capitalize(super.toString().replace('_', ' '));
    }

    public GattAttributeType getType() {
        return type;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public Data stringMeasurement(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        if (getDataTypes() == null || getDataTypes().length == 0) return null;
        String result;
        try {
            result = new String(characteristic.getValue(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            result = StringUtil.toReadableString(characteristic.getValue());
        }
        return new Data().add(gatt.getDevice().getAddress(), getDataTypes()[0], result);
    }


    public Data bytesMeasurement(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        if (getDataTypes() == null || getDataTypes().length == 0) {
            LLog.e("No datatypes defined for " + this.toString(), 300000);
            return null;
        }
        return new Data().add(gatt.getDevice().getAddress(), getDataTypes()[0], characteristic.getValue());
    }

    public Data longUintMeasurement(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        if (getDataTypes() == null || getDataTypes().length == 0) {
            LLog.e("No datatypes defined for " + this.toString(), 300000);
            return null;
        }
        byte[] bytes = characteristic.getValue();
        //  LLog.i("Length of bytes = " + bytes.length);

        return new Data().add(gatt.getDevice().getAddress(), getDataTypes()[0], StringUtil.toHex(bytes));
    }

    public Data uint8Measurement(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        if (getDataTypes() == null || getDataTypes().length == 0) {
            LLog.e("No datatypes defined for " + this.toString(), 300000);
            return null;
        }
        final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        return new Data().add(gatt.getDevice().getAddress(), getDataTypes()[0], value);
    }

    public Data uint16Measurement(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        if (getDataTypes() == null || getDataTypes().length == 0) {
            LLog.e("No datatypes defined for " + this, 300000);
            return null;
        }
        final int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        return new Data().add(gatt.getDevice().getAddress(), getDataTypes()[0], value);
    }

    public Data parse(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        switch (this) {
            case SYSTEM_ID: return longUintMeasurement(gatt, characteristic);
            case FIRMWARE_REVISION_STRING:
            case HARDWARE_REVISION_STRING:
            case SOFTWARE_REVISION_STRING:
            case MANUFACTURER_NAME_STRING:
            case MODEL_NUMBER_STRING:
            case SERIAL_NUMBER_STRING:
            case DEVICE_NAME:
            case RADBEACON_DEVICENAME:
                return stringMeasurement(gatt, characteristic);
            case UNKNOWN:
                LLog.i("Attribute " + this.toString() + " " + characteristic.getUuid() + " = " + StringUtil.toReadableString(characteristic.getValue()));
                return null;
            default:
                byte[] bytes = characteristic.getValue();
                switch (bytes.length) {
                    case 1:
                        return uint8Measurement(gatt, characteristic);
                    case 2:
                        return uint16Measurement(gatt, characteristic);
                    default:
                        return bytesMeasurement(gatt, characteristic);
                }
        }
    }

    public Data parseCharacteristic(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        ParseDelegate delegate;
        if ((delegate = parseDelegates.get(this)) != null) return delegate.parse(gatt, characteristic);
        return parse(gatt, characteristic);
    }

    public boolean maySetNotification() {
        switch (this) {
            case TI_REGISTER_DATA:
            case TI_CONNECTIONCONTROL_CHAR1:
            case TI_CONNECTIONCONTROL_CHAR2:
            case TI_CONNECTIONCONTROL_CHAR3:
            case TI_OAD_IMG_BLOCK:
            case TI_OAD_IMG_IDENTIFY: return false;
            default: return true;
        }
    }

    public boolean needsInit() {
        return commands(CommandType.INIT) != null;
    }

    public boolean initAlways() {
        return false;
    }

    public List<Tuple<byte[], String>> commands(CommandType cmdType) {
        //case TI_IO_DATA: return new byte[]{IO_REDLED};
        //case TI_IO_CONFIG: return new byte[]{IO_LOCAL};
        return null;
    }

    public boolean allowed(String deviceName) {
        //LLog.i("Checking " + this + " allowed for: " + deviceName);
        if (deviceName.startsWith("Tink")) {
            switch (this) {
                case SOAP_SERVICE:
                case SOAP_DATA:
                case SOAP_DATA2:
                    return true;
                default:
                    return false;
            }
        } else if (deviceName.startsWith("IB300")) {
            switch (this) {
                case BODY_COMPOSITION_SERVICE:
                case INBALANCE_READ_CHARACTERISTIC:
                case INBALANCE_WRITE_CHARACTERISTIC:
                    return true;
                default:
                    return false;
            }

        }
        switch(this) {
            case TI_CONNECTIONCONTROL_CHAR1:
            case TI_CONNECTIONCONTROL_CHAR2:
            case TI_CONNECTIONCONTROL_CHAR3:
            case TI_REGISTER_DATA:
            case TI_REGISTER_ADDRESS:
            case TI_REGISTER_DEV:
            case TI_OAD_IMG_IDENTIFY:
            case TI_OAD_IMG_BLOCK:
                return false;
            default:
                return true;
        }
    }


    public boolean needsReadAfterWrite() { return true; }

    public GattAttribute characteristicToReadAfter() {
        return null;
    }

    public byte[] convertValues(int ...values) {
        byte[] result = new byte[values.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = (byte)(values[i] & 255);
        }
        return result;
    }

    public boolean hasDescriptor() {
        return true;
    }

    public boolean mayRead() {
        return true;
    }

    public int getWriteType() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT;
        } else
            return -1;
    }

    public boolean isSupported() {
        return Globals.verbose;
    }

    public DataType[] getReferencedDataTypes() {
        return null;
    }


}
