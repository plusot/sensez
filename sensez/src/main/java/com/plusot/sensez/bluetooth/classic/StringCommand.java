package com.plusot.sensez.bluetooth.classic;


public class StringCommand extends Command{
	private final String cmd;
	
	public StringCommand(final String cmd, final int startDelay, final int interval) {
		super(startDelay, interval);
		this.cmd = cmd;
	}

	@Override 
	public String toString() {
		return cmd;
	}
	
	@Override
	public byte[] getBytes() {
		return cmd.getBytes();
	}
}