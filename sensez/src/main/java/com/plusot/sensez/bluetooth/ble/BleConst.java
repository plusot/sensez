package com.plusot.sensez.bluetooth.ble;

/**
 * Created by peet on 18-03-16.
 */
public class BleConst {
    public static final int ACTUATOR_ANGLE_OFFSET = 0;
    public static final int ACTUATOR_ANGLE2_OFFSET = 2;
    public static final int RAW_ANGLE_OFFSET = 4;

    public static final int TOTAL_STROKES_OFFSET = 5;  // 3 byte!
    public static final int ACCELERATION_OFFSET = 8;
    public static final int PARTIAL_STROKES_REFILL_OFFSET = 10;

    public static final int REFILLS_OFFSET = 12;
    public static final int USED_VOLUME_OFFSET = 14;
    public static final int USED_COUNT_OFFSET = 16;
    public static final int MIN_ANGLE_OFFSET = 18;

    public static final int HOURS_SINCE_REFILL_ADVERT_OFFSET = 20;
    public static final int ALIVE_ADVERT_OFFSET = 22; // 3 bytes
    public static final int BATTERY_ADVERT_OFFSET = 25;

    public static final int MAGNET_PARTIAL_STROKES_REFILL_OFFSET = 0;
    public static final int MAGNET_USED_VOLUME_OFFSET = 2;
    public static final int MAGNET_START_ANGLE_OFFSET = 4;
    public static final int MAGNET_END_ANGLE_OFFSET = 5;

    public static final int ROT_SPEED_OFFSET = 0;
    public static final int ROT_SPEED_OFF_OFFSET = 2;
    public static final int STD_DEV_OFFSET = 4;
    public static final int STD_DEV_ROT_OFFSET = 5;

    public static final int START_ANGLE_OFFSET = 6;
    public static final int END_ANGLE_OFFSET = 7;
    public static final int CALIBRATION_OFFSET = 8;
    public static final int HOURS_SINCE_REFILL_ATTR_OFFSET = 9;
    public static final int ALIVE_ATTR_OFFSET = 11; // 3 bytes
    public static final int BATTERY_ATTR_OFFSET = 14;

    public static final int MTAG_TEMPERATURE_OFFSET	= 0x05;
    public static final int MTAG_HUMIDITY_OFFSET = 0x07;
    public static final int MTAG_LUMINENCE_OFFSET =	0x09;
    public static final int MTAG_IR_TEMPERATURE_LOCAL_OFFSET = 0x0B;
    public static final int MTAG_IR_TEMPERATURE_TARGET_OFFSET = 0x0D;
    public static final int MTAG_TEMPERATURE2_OFFSET = 0x0F;
    public static final int MTAG_PRESSURE_OFFSET = 0x12;

}

