package com.plusot.sensez.bluetooth;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.plusot.sensez.R;
import com.plusot.sensez.bluetooth.ble.BleDevice;
import com.plusot.sensez.bluetooth.ble.GattAttribute;
import com.plusot.sensez.bluetooth.classic.BTConnectThread;
import com.plusot.sensez.bluetooth.classic.BTDevice;
import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.Globals;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.ToastHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class BlueMan implements ScanManager {
    public static final int REQUEST_ENABLE_BT = 8001;

    private static BlueMan instance = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private boolean mScanning;
    private BluetoothAdapter.LeScanCallback leScanCallback = null;
    private ScanCallback leScanCallbackAPI21 = null;
    private Set<String> foundDevices = new HashSet<>();
    private BluetoothLeScanner scannerAPI21 = null;
    private long nanoOffset = 0;
    private SleepAndWake.Stopper stopper = null;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public enum ScanFailure {
        NO_ERROR(0),
        SCAN_FAILED_ALREADY_STARTED(ScanCallback.SCAN_FAILED_ALREADY_STARTED),
        SCAN_FAILED_APPLICATION_REGISTRATION_FAILED(ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED),
        SCAN_FAILED_INTERNAL_ERROR(ScanCallback.SCAN_FAILED_INTERNAL_ERROR),
        SCAN_FAILED_FEATURE_UNSUPPORTED(ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED),
        SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES (5), //ScanCallback.SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES),
        ;
        final int id;

        ScanFailure(int id) {
            this.id = id;
        }

        public static ScanFailure fromId(int id) {
            for (ScanFailure failure : ScanFailure.values()) {
                if (failure.id == id) return failure;
            }
            return NO_ERROR;
        }
    }




    private Map<String, ScannedBluetoothDevice> scannedDevices = new HashMap<>();
    private Set<String> unScannedDevices = new HashSet<>();

    private BlueMan() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {

            Context appContext = Globals.getAppContext();
            if (appContext == null) return;

            android.bluetooth.BluetoothManager bluetoothManager = (android.bluetooth.BluetoothManager) appContext.getSystemService(Context.BLUETOOTH_SERVICE);

            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                leScanCallbackAPI21 = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
//                        LLog.i("BLE Scan result");
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            long now = System.currentTimeMillis();
                            long nanoTime = result.getTimestampNanos() / 1000000;
                            if (nanoOffset == 0) nanoOffset = now - nanoTime;

                            ScanRecord record = result.getScanRecord();

                            if (record != null) parseScanResult(
                                    result.getDevice(),
                                    result.getRssi(),
                                    record.getBytes(),
                                    now
                            );
//                        }
                    }

                    public void onBatchScanResults(List<ScanResult> results) {
//                        LLog.i("BLE Scan batch results");
                        long now = System.currentTimeMillis();
                        for (ScanResult result : results) {
                            long nanoTime = result.getTimestampNanos() / 1000000;
                            if (nanoOffset == 0) nanoOffset = now - nanoTime;

                            ScanRecord record = result.getScanRecord();
                            if (record != null) parseScanResult(
                                    result.getDevice(),
                                    result.getRssi(),
                                    record.getBytes(),
                                    now
                            );
                        }
                    }

                    public void onScanFailed(int errorCode) {
                        LLog.i("BLE Scan failed. Reason: " + ScanFailure.fromId(errorCode));

                    }
                };
            }
            leScanCallback = (device, rssi, scanRecord) -> {
//                LLog.i("Scan callback called");
                parseScanResult(device, rssi, scanRecord, System.currentTimeMillis());
            };

            if (bluetoothManager != null) bluetoothAdapter = bluetoothManager.getAdapter();
        } else {
            LLog.i("BlueMan initializing with BluetoothAdapter");
            if (bluetoothAdapter == null) bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        if ( bluetoothAdapter == null || !bluetoothAdapter.isEnabled() ) {
            ToastHelper.showToastLong(R.string.bluetooth_not_enabled);
            Activity activity = Globals.getVisibleActivity();
            if (activity != null) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            instance = null;
            //return;
        }
    }

    private void parseScanResult(final BluetoothDevice device, int rssi, byte[] scanRecord, final long now) {
//        LLog.i("Parsing scan result");
        ScannedBluetoothDevice sd = scannedDevices.get(device.getAddress());

        if (sd == null && !Globals.verbose) {
            if (unScannedDevices.contains(device.getAddress())) return;
            DeviceType type = DeviceType.fromBluetoothDeviceTypeInt(device.getType());
            String name = device.getName();
            if (name == null && type == DeviceType.TYPE_UNKNOWN) {
                unScannedDevices.add(device.getAddress());
                return;
            }
            if (name != null) {
                if (name.toLowerCase().startsWith("[tv]")) {
                    unScannedDevices.add(device.getAddress());
                    return;
                }
                if (name.toLowerCase().contains("undefined")) {
                    unScannedDevices.add(device.getAddress());
                    return;
                }
            }
        }

        boolean isNew = false;
        if (sd == null) {
            isNew = true;
            scannedDevices.put(device.getAddress(), sd = new ScannedBluetoothDevice(device, now, true));
        }
        sd.incScanCount();
        sd.setTimeRead(now);
        sd.getData().add(device.getAddress(), DataType.RSSI, rssi);
        sd.setRssi(rssi);

        sd.parseScanRecord(scanRecord);
        if (isNew && Man.debug) LLog.i("New device found: " + device.getAddress() + " " + device.getName()); // + " " + sd.getData().toString());
        Man.fireScanned(sd, isNew);

        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        BluetoothManager bluetoothManager = (BluetoothManager) appContext.getSystemService(Context.BLUETOOTH_SERVICE);

        if (sd.isSelected()) {
            if (sd.bleDevice == null) {
                stopLeScan();
                sd.bleDevice = new BleDevice(sd, device);
            } else /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)*/ {
                if (bluetoothManager != null && bluetoothManager.getConnectionState(device, BluetoothProfile.GATT) == BluetoothProfile.STATE_DISCONNECTED ||
                        sd.bleDevice.getState() == BluetoothProfile.STATE_DISCONNECTED) {
                    stopLeScan();
                    if (sd.reconnectCount > 4) {
                        LLog.i("ReconnectCount too high. Creating new BleDevice for " + sd);
                        if (sd.bleDevice != null) sd.bleDevice.close();
                        final ScannedBluetoothDevice sdFinal = sd;
                        //TODO: Does this still work???
                        SleepAndWake.runInNewThread(() -> sdFinal.bleDevice = new BleDevice(sdFinal, device), /*2000,*/ "BleDevice.parseScanResult");
                        sd.reconnectCount = 0;
                    } else if (sd.reconnectCount < 1 && sd.bleDevice.reconnect()) {
                        sd.reconnectCount++;
                        LLog.i("Trying to reconnect GATT for " + sd.getName() + ", reconnect count = " + sd.reconnectCount);
                    } else {
                        sd.reconnectCount++;
                        LLog.i("Device reconnect for " + sd.getName() + ", reconnect count = " + sd.reconnectCount);
                        sd.bleDevice.connect();
                    }

                }
            }
        }
    }

    public static BlueMan getInstance() {
        if (instance == null) {
            instance = new BlueMan();
        }
        return instance;
    }

    @Override
    public List<ScannedDevice> getScannedDevices() {
        return new ArrayList<>(scannedDevices.values());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressWarnings("deprecation")
    private void stopLeScan() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        if (!mScanning) return;
        if (stopper != null) {
            stopper.stop();
            stopper = null;
        }
        mScanning = false;
        try {
            if (scannerAPI21 != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                LLog.i("OFF using API21");
                scannerAPI21.stopScan(leScanCallbackAPI21);
            } else {
//                LLog.i("OFF");
                bluetoothAdapter.stopLeScan(leScanCallback);
            }
            Man.fireScanning(ScanType.BLE_SCAN, false);
        } catch (IllegalStateException e) {
            LLog.i("Could not stop BLE scan", e);
        }
    }

//    private long nextScan() {
//        if (System.currentTimeMillis() - ScannedBluetoothDevice.sensorTagFound < 60000 && !PreferenceKey.USE_BT.isTrue()) {
//            LLog.i("Using short scan");
//            return Man.SHORT_NEXT_SCAN;
//        } else {
//            return Man.NEXT_SCAN;
//        }
//    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private boolean startBLEScan() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;
        if (bluetoothAdapter == null) return false;

        if (mScanning) return false;
        if (!Globals.runMode.isRun()) return false;

//        LLog.i("Starting BLE scan");

        stopper = SleepAndWake.runInMain(() -> {
            stopper = null;
//            LLog.i("Stopping BLE .......................");
            stopLeScan();
        }, Man.getScanPeriod(), "BleDevice.startLeScan");
        mScanning = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            LLog.i("using API21");
            scannerAPI21 = bluetoothAdapter.getBluetoothLeScanner();
            ScanSettings.Builder scanSettingsBuilder = new ScanSettings.Builder().setReportDelay(0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    scanSettingsBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
                } catch (IllegalArgumentException e) {
                    LLog.i("Could not set scan mode");
                }
                scanSettingsBuilder.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES);
                scanSettingsBuilder.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE);

            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                LLog.i("Setting legacy");
//                scanSettingsBuilder.setLegacy(true);
//            }
//            scanSettingsBuilder.setReportDelay(100);

//            ScanFilter.Builder filter = new ScanFilter.Builder();
//            List<ScanFilter> list = new ArrayList<>();
//            list.addWatchable(filter.build());
            if (scannerAPI21 == null) {
//                LLog.i("using older API");
                Man.fireScanning(ScanType.BLE_SCAN, true);
                bluetoothAdapter.startLeScan(leScanCallback);
            } else {
                Man.fireScanning(ScanType.BLE_SCAN, true);
                scannerAPI21.startScan(null, scanSettingsBuilder.build(), leScanCallbackAPI21);
            }
        } else {
            LLog.i("using older API");
            Man.fireScanning(ScanType.BLE_SCAN, true);
            bluetoothAdapter.startLeScan(leScanCallback);
        }
        return true;
    }

    private BTConnectThread btConnectThread = null;
    private final Queue<BluetoothDevice> btDeviceQueue = new LinkedBlockingQueue<>();

    @SuppressWarnings("Convert2Lambda")
    private BTConnectThread.Listener btConnectListener = new BTConnectThread.Listener() {
        @Override
        public void onBluetoothConnected(BTConnectThread.ConnectResult connectResult, BluetoothDevice device, BluetoothSocket socket) {

            synchronized(btDeviceQueue) {
                btConnectThread = null;
//                connectAddresses.remove(bluetoothDevice.getAddress());
            }
            ScannedBluetoothDevice scannedBluetoothDevice = scannedDevices.get(device.getAddress());
            if (scannedBluetoothDevice != null && connectResult != null) {
                switch (connectResult) {
                    case CONNECTED:
                        scannedBluetoothDevice.incScanCount();
                        LLog.i("Opening: " + scannedBluetoothDevice.getName());
                        if (scannedBluetoothDevice.btDevice == null) scannedBluetoothDevice.btDevice = BTDevice.create(device, scannedBluetoothDevice);
                        if (scannedBluetoothDevice.btDevice != null && socket != null) scannedBluetoothDevice.btDevice.open(socket);
                        break;
                    case CONNECT_FAILED:
//                    LLog.i("Connect failed to : " + scannedBluetoothDevice.getName());
                        break;
                }
            }
            Man.fireScanning(ScanType.BT_SCAN, false);

        }
    };
    private boolean startBTScan() {
        if (bluetoothAdapter == null) {
            LLog.i("No bluetooth adapter available to start BT Scan");
            return false;
        }

        if (btConnectThread != null) {
            LLog.i("Connecting to bluetoothDevice still in process.");
            return false;
        }
        if (!Globals.runMode.isRun()) return false;

//        LLog.i("Starting BT scan");
        Man.fireScanning(ScanType.BT_SCAN, true);

        if (btDeviceQueue.size() <= 0) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() == 0) {
                LLog.i("No paired devices found");

            } else for (BluetoothDevice btDevice : pairedDevices) {
                BluetoothClass bluetoothClass = btDevice.getBluetoothClass();
                if (bluetoothClass != null) {
                    String name = BTHelper.getName(btDevice);
                    BTHelper.BTTypeMajor major = BTHelper.BTTypeMajor.fromInt(bluetoothClass.getMajorDeviceClass());
                    if (major == null) continue;

                    switch (major) {
                        case COMPUTER:
                            if (!foundDevices.contains(btDevice.getAddress()))
                                LLog.i("Found computer " + major + " at: " + btDevice.getAddress() + " with name: " +  name + ".");

                            break;
                        case PHONE:
//                            //if (Globals.testing.isVerbose())
//                            if (!foundDevices.contains(btDevice.getAddress()))
//                                LLog.i("Found phone " + major + " at: " + btDevice.getAddress() + " with name: " +  name + ".");
//                            break;
                        case PERIPHERAL:
                        default:
                            //DeviceModel model = BTHelper.fromDevice(btDevice);
                            //if (model != null && DeviceModel.getChosenModels().contains(model)) {
                            ScannedBluetoothDevice sd;
                            if ((sd = scannedDevices.get(btDevice.getAddress())) == null) {
                                scannedDevices.put(btDevice.getAddress(), sd = new ScannedBluetoothDevice(btDevice, System.currentTimeMillis(), false));
                            }
                            if (!foundDevices.contains(btDevice.getAddress())) {
//                                LLog.i("Found peripheral " + major + " at: " + btDevice.getAddress() + " with name: " + name + ".");
                                //    Device bluetoothDevice = getDevice(model);
                                //    if (bluetoothDevice != null && !bluetoothDevice.isActive() && !btDeviceQueue.contains(bluetoothDevice)) btDeviceQueue.addWatchable(btDevice);
                                //}
                                Man.fireScanned(sd, true);
                            }
                            if (sd.isSelected() && sd.btDevice == null && !btDeviceQueue.contains(btDevice)) btDeviceQueue.add(btDevice);
                    }
                    foundDevices.add(btDevice.getAddress());
                }
            }
        }
        if (btDeviceQueue.size() > 0) {
            BluetoothDevice bluetoothDevice = btDeviceQueue.poll();
//            LLog.i("Trying to connect to Bluetooth bluetoothDevice: " + bluetoothDevice.getAddress() + " " + bluetoothDevice.getName());
            synchronized(btDeviceQueue) {
//                Man.fireScanning(ScanType.BT_SCAN, true);
                btConnectThread = new BTConnectThread(btConnectListener, bluetoothAdapter, bluetoothDevice);
                btConnectThread.start();
            }
        } else {
            Man.fireScanning(ScanType.BT_SCAN, false);
        }
        return true;
    }

    @Override
    public void close() {
        if (Man.debug) LLog.i("Closing BlueMan");

        for (ScannedBluetoothDevice sd : scannedDevices.values()) sd.close();
        scannedDevices.clear();

        if (bluetoothAdapter != null && bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();

        synchronized(btDeviceQueue) {
            if (btConnectThread != null) btConnectThread.close();
            btConnectThread = null;

//            if (secureAcceptThread != null) secureAcceptThread.close();
//            secureAcceptThread = null;
        }
//        BTAcceptThread.setStateDisconnected();
        if (Man.debug) LLog.i("Closed BlueMan");

    }

    @Override
    public boolean startScan(ScanType type) {
//        LLog.i("Starting scan " + type);
        switch (type) {
            case ANT_SCAN:
                break;
            case BT_SCAN:
                return startBTScan();
            case BLE_SCAN:
                return startBLEScan();
        }
        return false;
    }

    @Override
    public void stopScan(ScanType type) {
        LLog.i("Stopping scan " + type);
        switch (type) {
            case ANT_SCAN:
                break;
            case BT_SCAN:
                if (btConnectThread != null) btConnectThread.close();
                btConnectThread = null;
                Man.fireScanning(ScanType.BT_SCAN, false);
                break;
            case BLE_SCAN:
                stopLeScan();
                break;
        }
        GattAttribute.BATTERY_LEVEL.setParseDelegate((gatt, characteristic) -> null);
    }


}
