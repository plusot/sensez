package com.plusot.sensez.bluetooth.ble;

public enum GattAttributeType {
    SERVICE("Service"),
    CHARACTERISTIC("Characteristic"),
    DESCRIPTOR("Descriptor");

    private final String label;

    GattAttributeType(final String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
