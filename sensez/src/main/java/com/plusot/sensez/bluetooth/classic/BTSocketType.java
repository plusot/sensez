package com.plusot.sensez.bluetooth.classic;

public enum BTSocketType {
    SECURE,
    INSECURE
}
