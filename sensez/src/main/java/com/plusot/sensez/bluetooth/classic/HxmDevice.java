package com.plusot.sensez.bluetooth.classic;


import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.util.Format;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;

class HxmDevice extends BTDevice {
	private static final boolean DEBUG = false;

	private int pFirmwareId = 0;
	private int pHardwareId = 0;
	private int pCharge = 0;
	private int pBeat = -1;
	private int pIntDistance = 0;
	private float totDistance = 0f;
	private float pSpeed = -1f;

	private int pStrides = -1;
	private int totStrides = 0;
	private long lastStridesTime = -1;
//	private String rawValue = null;

	HxmDevice(BluetoothDevice bluetoothDevice, Listener listener) {
		super(bluetoothDevice, listener);
	}

	@Override
	public String getModelName() {
		return "Zephyr HXM";
	}

    @Override
    public BTModel getModel() {
        return BTModel.HXM;
    }

	public void open(final BluetoothSocket socket) {
		super.open(socket);
		synchronized(this) {
			connectedThread = new BTConnectedThread(
					this,
                    socket,
					null, 
					BTConnectedThread.Type.WITH_STARTENDBYTE, 
					new byte[] {(byte)0x2, (byte) 0x3}, 
					2, 
					5
					//0
					);
			connectedThread.start();
		}
	}

	private int crc8PushByte(int crc, final int ch) {
		crc ^= ch;
		for (int i = 0; i < 8; i++) {
			if ((crc & 1) == 1) {
				crc >>>= 1; crc ^= 0x8C;
			} else {
				crc >>>= 1;
			}
		} 
		return crc;
	}

	private int crc8(byte[] block, int pos, int count) {
		int crc = 0;
		for (int i = 0; i < Math.min(block.length - pos, count); i++) {
			crc = crc8PushByte(crc, 0xFF & block[pos + i]);
		}
		return crc;
	}

	private int getInt16(byte[] msg, int pos) {
		return (0xFF & msg[pos]) + ((0xFF & msg[pos + 1]) << 8);	
	}


	private String getAscii(byte[] msg, int pos) {
		return "" + (char)msg[pos] + (char)msg[pos + 1];	
	}

	private int getInt8(byte[] msg, int pos) {
		return (0xFF & msg[pos]);	
	}

	@Override
	public void onBluetoothReadBytes(BTConnectedThread sender, byte[] msg, long now) {
		active = System.currentTimeMillis();
        Data data = new Data();
		if (msg.length < 6) 
			LLog.e("Msg too short (" + StringUtil.toHexString(msg) + ")!!!!!!");
		else {
			fireState(StateInfo.COMS);
			int crc = crc8(msg, 3, msg.length - 5);
			int crc2 = (0xFF & msg[msg.length - 2]);
			if (crc != crc2) LLog.i("CRC failed (" + crc + ", " + crc2 +  ")!!!!!!");

			if ((0xFF & msg[1]) == 0x26 && (0xFF & msg[2]) == 55 && msg.length >= 60) {
				int firmwareId = getInt16(msg, 3);
				String firmwareVersion = getAscii(msg, 5);
				int hardwareId = getInt16(msg, 7);
				String hardwareVersion = getAscii(msg, 9);
				int charge = getInt8(msg, 11);
				int hr = getInt8(msg, 12);
				int beat = getInt8(msg, 13);
				int deltaBeat = 14;
				if (pBeat != -1) deltaBeat = Math.min((0xFF + beat - pBeat) % 0xFF, 14);
				int stamp;
				int prevStamp = getInt16(msg, 14);
				int[] intervals = new int[14];
				for (int i = 0; i < 14; i++) {
					stamp = getInt16(msg, 16 + i * 2);
					intervals[i] = (0xFFFF + prevStamp - stamp) %  0xFFFF;
					if (intervals[i] == 0 && i < deltaBeat) deltaBeat = i; 
					prevStamp = stamp;
				}
				int intDistance = getInt16(msg, 50);
				float speed = 1.0f / 256f * getInt16(msg, 52);
				int strides = getInt8(msg, 54);
				StringBuilder result = new StringBuilder();
				result.append('{');
				if (pFirmwareId != firmwareId) {
					result.append("\"firmware\":\"").append("9500." + firmwareId + ".V" + firmwareVersion).append("\",");
					data.add(getAddress(), DataType.SOFTWARE_REVISION_STRING, "9500." + firmwareId + ".V" + firmwareVersion);
				}
				if (pHardwareId != hardwareId) {
					result.append("\"hardware\":\"").append("9800." + hardwareId + ".V" + hardwareVersion).append("\",");
                    data.add(getAddress(), DataType.HARDWARE_REVISION_STRING, "9800." + hardwareId + ".V" + hardwareVersion);
				}
				if (pCharge != charge) {
					result.append("\"charge\":").append(charge).append(",");
					data.add(getAddress(), DataType.BATTERY_INFO, charge + "%");
				}
				result.append("\"hr\":").append(hr).append(",");
				result.append("\"beat\":").append(beat).append(",");
				int totalInter = 0;
				if (deltaBeat > 0) {
					result.append("\"inter\":[");
					for (int i = 0; i < deltaBeat; i++) {
						result.append(intervals[i]);
						totalInter += intervals[i];
						if (i < deltaBeat - 1) result.append(",");
					}
					result.append("],");
				}
				if (pIntDistance != intDistance) {
					int id = (4096 + intDistance - pIntDistance) % 4096;
					totDistance += 1.0f / 16f * id; 
					result.append("\"distance\":").append(Format.format(totDistance, 1)).append(",");
				}
				if (pSpeed != speed) {
					result.append("\"speed\":").append(speed).append(",");
//					if (SenseGlobals.lastLocation == null || Math.abs(now - SenseGlobals.lastLocationTime) > 60000 || SenseGlobals.lastLocation.getSpeed() < 0.9f * speed)
						data.add(getAddress(), DataType.SPEED, (double) (speed));
				}
				if (pStrides != strides && pStrides != -1) {
					int delta = (128 + strides - pStrides) % 128;
					totStrides += delta;
					result.append("\"strides\":").append(totStrides).append(",");
					data.add(getAddress(), DataType.STEPS, delta);
					if (lastStridesTime != -1)
						data.add(getAddress(), DataType.CADENCE, 60000.0 * delta / (now - lastStridesTime));
					lastStridesTime = now;	

				}
//				if (totDistance > 1000)
//					rawValue = "" + hr + " bpm\n" + Format.format(totDistance / 1000, 0) + " km\n" + Format.format(3.6 * speed, 1) + " kph\n" + totStrides; 
//				else
//					rawValue = "" + hr + " bpm\n" + Format.format(totDistance, 0) + " m\n" + Format.format(3.6 * speed, 1) + " kph\n" + totStrides + " strides"; 
				result.append("\"time\":\"" + TimeUtil.formatTime(now) + "\"");
				result.append('}');

//				if (SenseGlobals.activity.equals(SenseGlobals.ActivityMode.RUN) && SenseGlobals.hxmTesting) SimpleLog.getInstance(SimpleLogType.JSON, "hxm").log(result.toString());
				//else
				if (DEBUG) LLog.i("onBluetoothReadBytes: " + result.toString());
				pCharge = charge;
				pHardwareId = hardwareId;
				pFirmwareId = firmwareId;
				pBeat = beat;
				pStrides = strides;
				pSpeed = speed;
				pIntDistance = intDistance;
				for (int i = deltaBeat - 1; i >=  0; i--) {
                    Data pulseData = new Data(now - totalInter);
					pulseData.add(getAddress(), DataType.PULSE_WIDTH, intervals[i]);
					totalInter -= intervals[i];
                    fireData(pulseData);
				}
                data.add(getAddress(), DataType.HEART_RATE, hr);
                fireData(data);
			} else
				LLog.i(StringUtil.toHexString(msg));

		}
	}

	@Override
	public void onBluetoothRead(BTConnectedThread sender, String msg, long time) {
		active = time;
		LLog.i(StringUtil.toHexString(msg.getBytes()));
	}

}

