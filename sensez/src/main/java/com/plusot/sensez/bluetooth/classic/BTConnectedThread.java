package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothSocket;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;

import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

class BTConnectedThread extends Thread {

	private final BluetoothSocket mmSocket;
	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private boolean mayRun = true;
	private final Listener listener;
	private final Command[] commands;
	private final Type type;
	private final byte startByte;
	private final byte endByte;
	private final int lengthByteIndex;
	private final int extraBytes;
	private TimedBuffer timedBuffer = null;
	private boolean maySendCommands = true;
	private static final boolean debug = false;

	public enum Type {
		STRINGS,
		WITH_LINEENDS,
		WITH_STARTENDBYTE,
		CSAFE,
		TIMED_BYTES,
		BYTES
    }

	private class TimedBuffer extends Thread {
		long timeRead;
		final long timeInterval;
		byte[] bytes = null;

		TimedBuffer(final long timeInterval) {
			this.timeInterval = timeInterval;
			start();
		}

		@Override
		public void run() {
			while (Globals.runMode.isRun() && mayRun) {
				synchronized(this) {
					if (System.currentTimeMillis() - timeRead > timeInterval && bytes != null && bytes.length > 0) {
						final byte[] temp = Arrays.copyOf(bytes, this.bytes.length);
						SleepAndWake.runInMain(new Runnable() {
							@Override
							public void run() {
								listener.onBluetoothReadBytes(BTConnectedThread.this, temp, timeRead);
							}
						}, "BTConnectedThread.run");
						bytes = null;
						timeRead = 0;
					}
				}
				try {
					sleep(timeInterval / 2);
				} catch (InterruptedException e) {
                    LLog.e("Interrupted");
				}
			}
		}

		synchronized void addBytes(byte[] bytes) {
			long now = System.currentTimeMillis();
			if (now - timeRead > timeInterval && timeRead != 0 && this.bytes != null) {
				final byte[] temp = Arrays.copyOf(this.bytes, this.bytes.length);
				SleepAndWake.runInMain(new Runnable() {
					@Override
					public void run() {
						listener.onBluetoothReadBytes(BTConnectedThread.this, temp, timeRead);
					}
				}, "BTConnectedThread.addBytes");
				this.bytes = null;
			}
			if (this.bytes == null)
				this.bytes = bytes;
			else
				this.bytes = ArrayUtils.addAll(this.bytes, bytes);
			timeRead = now;
		}
	}

	public interface Listener {
		void onBluetoothStart(BTConnectedThread sender);
		void onBluetoothRead(BTConnectedThread sender, String msg, long time);
		void onBluetoothReadBytes(BTConnectedThread sender, byte[] msg, long time);
		void onBluetoothTerminate(BTConnectedThread sender);
	}

//	public BTConnectedThread(final Listener listener, final BluetoothSocket socket, final Type type) {
//		this(listener, socket, null, type, null, -1, 0);
//	}

	BTConnectedThread(final Listener listener, final BluetoothSocket socket, final Command[] commands, final Type type) {
		this(listener, socket, commands, type, null, -1, 0);
	}

//	public BTConnectedThread(
//			final Listener listener,
//			final BluetoothSocket socket,
//			final Command[] commands,
//			final Type type,
//			final byte[] cutBytes) {
//		this(listener, socket, commands, type, cutBytes, -1, 0);
//	}
//
//	public BTConnectedThread(
//			final Listener listener,
//			final BluetoothSocket socket,
//			final Command[] commands,
//			final long timeInterval) {
//		this(listener, socket, commands, Type.TIMED_BYTES, null, -1, 0);
//		timedBuffer = new TimedBuffer(timeInterval);
//	}

	BTConnectedThread(
            final Listener listener,
            final BluetoothSocket socket,
            final Command[] commands,
            final Type type,
            final byte[] cutBytes,
            final int lengthByteIndex,
            final int extraBytes) {
//		LLog.i("create");
		this.listener = listener;
		this.commands = commands;
		this.type = type;
		this.lengthByteIndex = lengthByteIndex;
		this.extraBytes = extraBytes;
		mmSocket = socket;

		if (cutBytes == null || cutBytes.length < 1 || (cutBytes.length < 2 && type.equals(Type.WITH_STARTENDBYTE))) {
			startByte = (byte) 0;
			endByte = (byte) 0;
		} else switch (type) {
			//		case WITH_STARTBYTE:
			//			startByte = cutBytes[0]; endByte = (byte)0; break;
			//		case WITH_ENDBYTE:
			//			startByte = (byte)0; endByte = cutBytes[0]; break;
			case WITH_STARTENDBYTE:
				startByte = cutBytes[0]; endByte = cutBytes[1]; break;
			case WITH_LINEENDS:
			default:
				startByte = (byte) 0; endByte = (byte) 0; break;
		}

		InputStream tmpIn = null;
		OutputStream tmpOut = null;

		// Get the BluetoothSocket input and output streams
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) {
			LLog.e("Temporary sockets not created", e);
		}

		mmInStream = tmpIn;
		mmOutStream = tmpOut;
	}

	public void run() {
		byte[] buffer = new byte[1024];
		int bytes;
		int pos = 0;
		if (commands != null && commands.length > 0) write(commands[0].getBytes());
		if (mayRun) {
			SleepAndWake.runInMain(new Runnable() {
				@Override
				public void run() {
					listener.onBluetoothStart(BTConnectedThread.this);
				}
			}, "BTConnectedThread.run.start");
			if (commands != null) new Thread(new Runnable() {
				@Override
				public void run() {
					if (debug) LLog.i("CommandThread started");
					while (mayRun) {
						if (maySendCommands) for (Command command: commands) if (command.maySend()) synchronized(this) {
							write(command.getBytes());
							break;
						}
						try {
							sleep(100);
						} catch (InterruptedException e) {
							LLog.e("Interrupted");
						}
					}
					LLog.i("CommandThread stopped");
				}

			}).start();
		}

		while (mayRun) {
			try {
				// Read from the InputStream
				if (pos >= buffer.length - 10) pos = 0;
				bytes = mmInStream.read(buffer, pos, buffer.length - pos);
				final long now = System.currentTimeMillis();

				switch (type) {
					case WITH_LINEENDS:
						pos += bytes;
						if	(pos > 0 && ((buffer[pos - 1] & 0xFF) == 0x0D || (buffer[pos - 1] & 0xFF) == 0x0A)) {
							if (pos > 1) {
								if (debug) LLog.i("LINE_ENDS: " + StringUtil.toReadableString(buffer, pos, true));
								final String temp = new String(buffer, 0, pos);
								SleepAndWake.runInMain(new Runnable() {
									@Override
									public void run() {
										listener.onBluetoothRead(BTConnectedThread.this, temp, now);
									}
								}, "BTConnectedThread.run.WITH_LINE_ENDS");
							}
							//buffer = new byte[1024];
							pos = 0;
						}
						break;
					case WITH_STARTENDBYTE:
						if (debug) LLog.i("START_END_BYTES (" + pos + "," + bytes + "): " + StringUtil.toHexString(buffer, pos + bytes)); //.toReadableString(buffer, bytes, true));
						int i = 0;
						int iEnd = bytes;
						while (i < iEnd) {
							if (buffer[0] == startByte && buffer[i + pos] == endByte && (lengthByteIndex < 1 || i + pos + 1 == (0xFF & buffer[lengthByteIndex]) + extraBytes)) {
								final byte[] temp = Arrays.copyOf(buffer, pos + i + 1);
								SleepAndWake.runInMain(new Runnable() {
									@Override
									public void run() {
										listener.onBluetoothReadBytes(BTConnectedThread.this, temp, now);
									}
								}, "BTConnectedThread.run.STARTENDBYTE");
								if (iEnd + pos > i + pos + 1) {
									buffer = Arrays.copyOfRange(buffer, pos + i + 1, pos + i + 1 + 1024);
									iEnd = bytes - i - 1;
								} else {
									iEnd = 0;
								}
								i = 0;
								pos = 0;
							} else
								i++;
						}
						pos += iEnd;
						break;
					case STRINGS:
						final String s = new String(buffer, 0, bytes);
						SleepAndWake.runInMain(new Runnable() {
							@Override
							public void run() {
								listener.onBluetoothRead(BTConnectedThread.this, s, now);
							}
						}, "BTConnectedThread.run.STRINGS");

						pos = 0;
						break;
					case TIMED_BYTES:
						if (timedBuffer != null) timedBuffer.addBytes(Arrays.copyOf(buffer, bytes));
						if (debug) LLog.i("TIMED: " + StringUtil.toHexString(buffer, bytes)); //.toReadableString(buffer, bytes, true));

						pos = 0;
						break;

					case BYTES:
					default:
						if (debug) LLog.i("BYTES: " + StringUtil.toHexString(buffer, bytes)); //.toReadableString(buffer, bytes, true));
						final byte[] temp = Arrays.copyOf(buffer, bytes);
						SleepAndWake.runInMain(new Runnable() {
							@Override
							public void run() {
								listener.onBluetoothReadBytes(BTConnectedThread.this, temp, now);
							}
						}, "BTConnectedThread.run.BYTES");

						pos = 0;
						break;
				}
				//				if (commands != null) for (Command command: commands) if (command.maySend()) synchronized(this) {
				//					write(command.getBytes());
				//					break;
				//				}	

			} catch (IOException e) {
				LLog.e("Disconnected => " + e.getMessage());
				//connectionLost();
				break;
			}
		}
		try {
			synchronized(this) {
				mmSocket.close();
			}
		} catch (IOException e) {
			LLog.e("Close of socket failed", e);
		}
		SleepAndWake.runInMain(new Runnable() {
			@Override
			public void run() {
				listener.onBluetoothTerminate(BTConnectedThread.this);
			}
		}, "BTConnectedThread.run.terminate");
	}

	private synchronized void write(byte[] buffer) {
		if (type == Type.CSAFE) try {
			mmOutStream.write(0xF1);
			byte check = 0;
			for (byte b : buffer) {
				check ^= b;
				switch (b) {
					case (byte) 0xF0:
					case (byte) 0xF1:
					case (byte) 0xF2:
					case (byte) 0xF3: {
						mmOutStream.write(0xF3);
						mmOutStream.write(b & 0xF);
						break;
					}
					default:
						mmOutStream.write(b);
						break;
				}
			}
			mmOutStream.write(check);
			mmOutStream.write(0xF2);

			//if (Globals.testing.isVerbose()) LLog.i("write: " + StringUtil.toHexString(buffer));
		} catch (IOException e) {
			LLog.e("Exception during write", e);
		} else if (mmSocket.isConnected()) try {
			mmOutStream.write(buffer);
			//if (Globals.testing.isVerbose()) 
			//LLog.i("write: " + StringUtil.toHexString(buffer));
		} catch (IOException e) {
			LLog.e("Exception during write", e);
		}
	}

	@SuppressWarnings("unused")
    public void write(String message) {
		write(message.getBytes());
	}

	void startLoopCommands() {
		maySendCommands  = true;
	}

	void stopLoopCommands() {
		maySendCommands = false;
	}

	public void close() {
		try {
			synchronized(this) {
				mmSocket.close();
			}
			mayRun = false;
			interrupt();
		} catch (IOException e) {
			LLog.e("Close of socket failed", e);
		}
	}
}


