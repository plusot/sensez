package com.plusot.sensez.bluetooth.ble

import android.bluetooth.BluetoothGatt
import com.plusot.util.logging.LLog

/**
 * Package: com.plusot.bluelib.sensor.bluetooth.ble
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-09-10.
 * Copyright © 2019 Plusot. All rights reserved.
 */
enum class GattState(internal val id: Int) {
    SUCCESS(BluetoothGatt.GATT_SUCCESS),                                        // 0x00
    INVALID_HANDLE(0x01),
    READ_NOT_PERMITTED(BluetoothGatt.GATT_READ_NOT_PERMITTED),                  // 0x02
    WRITE_NOT_PERMITTED(BluetoothGatt.GATT_WRITE_NOT_PERMITTED),                // 0x03
    INVALID_PDU(0x04),
    INSUF_AUTHENTICATION(0x05),
    REQUEST_NOT_SUPPORTED(BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED),            // 0x06
    INVALID_OFFSET(BluetoothGatt.GATT_INVALID_OFFSET),                          // 0x07
    INSUFFICIENT_AUTHENTICATON(BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION), // 0x08
    PREPARE_Q_FULL(0x09),
    NOT_FOUND(0x0a),
    NOT_LONG(0x0b),
    INSUF_KEY_SIZE(0x0c),
    INVALID_ATTRIBUTE_LENGTH(BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH),      // 0x0d
    ERR_UNLIKELY(0x0e),
    INSUFFICIENT_ENCRYPTION(BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION),        // 0x0f
    UNSUPPORT_GRP_TYPE(0x10),
    INSUF_RESOURCE(0x11),

    ILLEGAL_PARAMETER(0x87),
    NO_RESOURCES(0x80),
    INTERNAL_ERROR(0x81),
    WRONG_STATE(0x82),
    DB_FULL(0x83),
    BUSY(0x84),
    ERROR(0x85),
    CMD_STARTED(0x86),
    PENDING(0x88),
    AUTHORIZATION_FAIL(0x89),
    MORE(0x8a),
    INVALID_CFG(0x8b),
    SERVICE_STARTED(0x8c),
    ENCRYPED_NO_MITM(0x8d),
    NOT_ENCRYPTED(0x8e),
    CONNECTION_CONGESTED(BluetoothGatt.GATT_CONNECTION_CONGESTED),              // 0x8f

    DUP_REG(0x90),
    ALREADY_OPEN(0x91),
    CANCEL(0x92),
    /* Client Characteristic Configuration Descriptor Improperly Configured */
    CCC_CFG_ERR(0xFD),
    /* Procedure Already in progress */
    PRC_IN_PROGRESS(0xFE),
    /* Attribute value out of range */
    OUT_OF_RANGE(0xFF),
    FAILURE(BluetoothGatt.GATT_FAILURE),                                        // 0x101
    UNKNOWN_STATUS(-1);


    companion object {
        fun fromId(id: Int): GattState {
            for (status in values()) {
                if (status.id == id) return status
            }
            LLog.i("Unknown status $id")
            return UNKNOWN_STATUS
        }
    }
}

fun Int.toGattState() = GattState.fromId(this)
