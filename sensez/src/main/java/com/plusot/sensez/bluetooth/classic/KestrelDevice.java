package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.data.Unit;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

class KestrelDevice extends BTDevice {
	private static final boolean DEBUG = false;
    private static final double R_SPECIFIC_AIR = 287.058;	// J/(kg⋅K)
    private static final double R_SPECIFIC_WATER = 461.495;	// J/(kg⋅K)

    private boolean infoRead = false;
	private boolean unitRead = false;
	private StringBuilder info = new StringBuilder();
	private StringBuilder units = new StringBuilder();

    private Map<String, String> unitMap = new HashMap<>();
	private WindData windData = new WindData();

	private class WindData {
		String speed = null;
		String humidity = null;
		String pressure = null;
		String lastMsgId = null;
		String temperature = null;
		String units1 = null;
		String units2 = null;
		String bearing = null;
	}

    KestrelDevice(BluetoothDevice bluetoothDevice, Listener listener) {
		super(bluetoothDevice, listener);
	}

	@Override
	public String getModelName() {
		return "Kestrel4000";
	}

	@Override
	public BTModel getModel() {
		return BTModel.KESTREL;
	}

	@Override
	public void open(final BluetoothSocket socket) {
		super.open(socket);

		connectedThread = new BTConnectedThread(
				this,
				socket,
				new StringCommand[]{
						new StringCommand("p\r", 10, 300000),
						new StringCommand("i?\r", 5000, 30000),
						new StringCommand("S\r", 2000, 20000)
				},
				BTConnectedThread.Type.WITH_LINEENDS);
		connectedThread.start();
	}

	private Unit getUnit(DataType type, String id) {
		String unit = unitMap.get(id);
		if (unit != null) {
			if (unit.equalsIgnoreCase("kt"))
				return Unit.KNOT;
			else if (unit.equalsIgnoreCase("mph"))
				return Unit.MPH;
			else if (unit.equalsIgnoreCase("fpm"))
				return Unit.FPM;
			else if (unit.equalsIgnoreCase("Bft"))
				return Unit.BEAUFORT;
			else if (unit.equalsIgnoreCase("m/s"))
				return Unit.MPS;
			else if (unit.equalsIgnoreCase("km/h"))
				return Unit.KPH;
			else if (unit.equalsIgnoreCase("F"))
				return Unit.FAHRENHEIT;
			else if (unit.equalsIgnoreCase("C"))
				return Unit.CELSIUS;
			else if (unit.equalsIgnoreCase("inHg"))
				return Unit.INHG;
			else if (unit.equalsIgnoreCase("hPa"))
				return Unit.HPA;
			else if (unit.equalsIgnoreCase("psi"))
				return Unit.PSI;
			else if (unit.equalsIgnoreCase("mb"))
				return Unit.MBAR;
			else if (unit.equalsIgnoreCase("m"))
				return Unit.METER;
			else if (unit.equalsIgnoreCase("ft"))
				return Unit.FEET;
			else if (unit.equalsIgnoreCase("s"))
				return Unit.SECOND;
			else if (unit.equalsIgnoreCase("%"))
				return Unit.PERCENT;
			else if (unit.equalsIgnoreCase("Mag"))
				return Unit.DEGREE;
		}
		return type.getUnit();
	}

	@Override
	public void onBluetoothRead(BTConnectedThread sender, String msg, long time) {
		Data data = new Data();
        if (DEBUG) LLog.i("Received: " + msg);
		fireState(StateInfo.COMS);
		for (String msgPart: msg.replaceAll("> ", "").split("\r\n")) {
			if (msgPart.contains("DT,")) {
				//LLog.i("onBluetoothRead: Start of unit message: "+ Util.trimAll(msgPart));
				unitRead = true;
				units.setLength(0);
				units.append(msgPart).append('\r');
				windData.units1 = StringUtil.trimAll(msgPart);
			} else if (msgPart.contains("Iss")) {
				//LLog.i("onBluetoothRead: Software message: "+ Util.trimAll(msgPart));
				infoRead = true;
				info.setLength(0);
				info.append(msgPart).append('\r');
				data.add(getAddress(), DataType.SOFTWARE_REVISION_STRING, StringUtil.trimAll(msgPart.replace("Iss", "")));
			} else if (msgPart.contains("S/N:")) {
				//LLog.i("onBluetoothRead: Serial number message: "+ StringUtil.trimAll(msgPart));
                info.append(msgPart).append('\r');
                data.add(getAddress(), DataType.SERIAL_NUMBER_STRING, StringUtil.trimAll(msgPart.replace("S/N:", "")));
			} else if (msgPart.contains("Battery:")) {
				//LLog.i("onBluetoothRead: Battery level message: "+ StringUtil.trimAll(msgPart));
                info.append(msgPart).append('\r');
                String battery =  StringUtil.trimAll(msgPart.replace("Battery:", ""));
				if (battery != null && battery.contains("-")) battery = battery.split("-")[0];
				data.add(getAddress(), DataType.BATTERY_INFO, battery);
			} else if (msgPart.startsWith("K4")) {
				//LLog.i("onBluetoothRead: Device model message: "+ StringUtil.trimAll(msgPart));
                info.append(msgPart).append('\r');
                data.add(getAddress(), DataType.FIRMWARE_REVISION_STRING, StringUtil.trimAll(msgPart));

			} else if (msgPart.contains("DCOCTL")) {
				//LLog.i("onBluetoothRead: End of units message: "+ StringUtil.trimAll(msgPart));
				infoRead = false;
                info.append(msgPart).append('\r');
                if (DEBUG) LLog.i(info.toString());
			} else if (infoRead) {
				//LLog.i("onBluetoothRead: Info message part: "+ StringUtil.trimAll(msgPart));

				if (msgPart.length() > 3) info.append(msgPart).append('\r');


            } else if (unitRead && msgPart.length() > 3) {
				//LLog.i("onBluetoothRead: Unit message part: "+ StringUtil.trimAll(msgPart));

                info.append(msgPart).append('\r');
                windData.units2 = StringUtil.trimAll(msgPart);
				unitRead = false;
                if (DEBUG) LLog.i("Units: " + units.toString());
				if (windData.units1 == null || windData.units2 == null) continue;
				String parts[] = windData.units2.split(",");
				String unitParts[] = windData.units1.split(",");
				for (int i = 0; i < Math.min(parts.length, unitParts.length); i++) {
					unitMap.put(unitParts[i], parts[i]);
				}
			} else {
				//LLog.i("onBluetoothRead: Data message: "+ StringUtil.trimAll(msgPart));

				String[] parts = msgPart.split(",");
				if (parts.length >= 5 && parts.length <= 6) try {
					windData.lastMsgId = StringUtil.trimAll(parts[0]);
					windData.speed = StringUtil.trimAll(parts[1]);
					windData.temperature = StringUtil.trimAll(parts[2]);
					windData.humidity = StringUtil.trimAll(parts[3]);
					windData.pressure = StringUtil.trimAll(parts[4]);
					if (parts.length >= 6) windData.bearing = StringUtil.trimAll(parts[5]); else windData.bearing = null;

					//LLog.i("onBluetoothRead: " + msg); //toText(", "));

                    //ValueItem<WindData> value = new ValueItem<WindData>(windData, now);
                    String WIND = "WS";
                    double windSpeed = StringUtil.toDouble(windData.speed, getUnit(DataType.WIND_SPEED, WIND));
					data.add(getAddress(), DataType.WIND_SPEED, windSpeed);
                    String TEMPERATURE = "TP";
					double temperature;
					double pressure;
					double humidity;
					data.add(getAddress(), DataType.TEMPERATURE, temperature = StringUtil.toDouble(windData.temperature, getUnit(DataType.TEMPERATURE, TEMPERATURE)));
                    String PRESSURE = "BP";
                    data.add(getAddress(), DataType.AIR_PRESSURE, pressure = StringUtil.toDouble(windData.pressure, getUnit(DataType.AIR_PRESSURE, PRESSURE)));
                    String HUMIDITY = "RH";
                    data.add(getAddress(), DataType.HUMIDITY, humidity = StringUtil.toDouble(windData.humidity, getUnit(DataType.HUMIDITY, HUMIDITY)));
					if (windData.bearing != null) {
						Double windDirection = StringUtil.toDouble(windData.bearing, null);
						if (windDirection >= 0 && windDirection <= 360) {
							data.add(getAddress(), DataType.WIND_DIRECTION, windDirection);
						} else
							LLog.e("Invalid winddirection " + windDirection + " (" + windData.bearing + " from " + msgPart + ")");
					}
					double tKelvin = temperature + 273.15;
//					double rhoDryAir = pressure / (tKelvin * R_SPECIFIC_AIR); // kg/m3
                    //double pSaturation = 610.78 * Math.pow(10, 7.5 * temperature / (temperature + 237.3)); //Pa
                    //double pSaturation = 610.78 * Math.exp(17.27 * temperature / (temperature + 237.3)); // Tetens equation, Pa
                    double pSaturation = 611.21 * Math.exp((18.678 - temperature / 234.5) * (temperature / (temperature + 257.14))); // Buck equation, Pa
                    double pVapor = humidity * pSaturation;
                    double pDryAir = pressure - pVapor;
                    double rho = pDryAir / (tKelvin * R_SPECIFIC_AIR) + pVapor / (tKelvin * R_SPECIFIC_WATER); // kg/m3
//                    LLog.i("Rho = " + Format.format(rho, 3) + ", Rho dry air = " + Format.format(rhoDryAir, 3));
                    data.add(getAddress(),DataType.RHO, rho);
                } catch (ClassCastException e){
					LLog.e("Invalid cast in conversion from string to number", e);
				}
			}
		}
		if (data.hasData()) fireData(data);
	}

	@Override
	public void onBluetoothReadBytes(BTConnectedThread sender, byte[] msg, long time) {
	}

}