package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * This thread runs while listening for incoming connections. It behaves
 * like a server-side client. It runs until a connection is accepted
 * (or until cancelled).
 */
public class BTAcceptThread extends Thread {
    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";
    private static final boolean DEBUG = false;
    private static ConnectState state = ConnectState.STATE_NONE;
    private static LinkedBlockingQueue<BTSocketInfo> socketQueue = new LinkedBlockingQueue<>();
    private BTSocketType socketType;
    private final Listener listener;
    private final BluetoothAdapter adapter;
    private boolean mayRun = true;

    public BTAcceptThread(final Listener listener, final BluetoothAdapter adapter, BTSocketType socketType) {
        this.socketType = socketType;
        this.adapter = adapter;
        this.listener = listener;
        state = ConnectState.STATE_LISTEN;
    }

    static synchronized void setStateConnected() {
        state = ConnectState.STATE_CONNECTED;
    }

//    public static synchronized void setStateConnecting() {
//        state = ConnectState.STATE_CONNECTING;
//    }

    public static synchronized void setStateDisconnected() {
        state = ConnectState.STATE_NONE;
    }

    public void run() {
        setName("AcceptThread" + socketType + "_" + getId());
        if (DEBUG) LLog.i("running ...");

        BluetoothServerSocket serverSocket;
        try {
            if (socketType == BTSocketType.SECURE) {
                serverSocket = adapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, BTGlobals.CHAT_UUID_SECURE);
            } else {
                serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, BTGlobals.CHAT_UUID_INSECURE);
            }
        } catch (IOException e) {
            LLog.e("Socket Type: " + socketType + "listen() failed", e);
            return;
//            try {
//                serverSocket.close();
//            } catch (IOException e1) {
//                LLog.i("Exception on close");
//            }
        }

        BluetoothSocket socket;

        // Listen to the server socket if we're not connected
        while (state != ConnectState.STATE_CONNECTED && mayRun) {
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                socket = serverSocket.accept(30000);
                if (DEBUG)
                    LLog.i("Accepted " + socket.getRemoteDevice().getName());

            } catch (IOException e) {
                if (DEBUG) LLog.e("Socket accept() failed.", e);
                break;
            }

            // If a connection was accepted
            if (socket != null) {
                //				synchronized (BTChat.this) {
                switch (state) {
                    case STATE_LISTEN:
                    case STATE_CONNECTING:
                        if (DEBUG)
                            LLog.i("Connecting to " + socket.getRemoteDevice().getName());
                        socketQueue.offer(new BTSocketInfo(socket, socket.getRemoteDevice(), socketType));
                        SleepAndWake.runInMain(new Runnable() {
                            @Override
                            public void run() {
                                BTSocketInfo socketInfo;
                                if ((socketInfo = socketQueue.poll()) != null) {
                                    listener.onBluetoothServerConnect(socketInfo.device, socketInfo.socket, socketInfo.type);
                                }
                            }
                        }, "BTAcceptThread.run.STATE_CONNECTING");
                        //handler.obtainMessage(CONNECTING).sendToTarget();
                        //connected(socket, socket.getRemoteDevice(), mSocketType);
                        break;
                    case STATE_NONE:
                    case STATE_CONNECTED:
                        if (DEBUG)
                            LLog.i("Closing socket " + socket.getRemoteDevice().getName());
                        try {
                            socket.close();
                        } catch (IOException e) {
                            LLog.e("Could not close unwanted socket", e);
                        }
                        break;
                }
            }
        }
        if (DEBUG) LLog.i("END, Socket Type: " + socketType);
        try {
            serverSocket.close();
        } catch (IOException e) {
            LLog.i("Exception on close: " + e.getMessage());
        }
        SleepAndWake.runInMain(new Runnable() {
            @Override
            public void run() {
                listener.onTerminate();
            }
        }, "BTAcceptThread.run.terminating");
    }

    public void cancel() {
        if (DEBUG) LLog.i("Socket Type" + socketType + "close " + this);
        mayRun = false;
        interrupt();

    }

    private enum ConnectState {
        STATE_NONE,       // we're doing nothing
        STATE_LISTEN,     // now listening for incoming connections
        STATE_CONNECTING, // now initiating an outgoing connection
        STATE_CONNECTED   // now connected to a remote device

    }

    public interface Listener {
        void onBluetoothServerConnect(BluetoothDevice device, BluetoothSocket socket, BTSocketType socketType);
        void onTerminate();
    }

    private class BTSocketInfo {
        final BluetoothSocket socket;
        final BluetoothDevice device;
        final BTSocketType type;

        BTSocketInfo(final BluetoothSocket socket,
                     final BluetoothDevice device,
                     final BTSocketType type) {
            this.socket = socket;
            this.device = device;
            this.type = type;
        }
    }
}
