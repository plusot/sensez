package com.plusot.sensez.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.os.Build;

import com.plusot.sensez.R;
import com.plusot.sensez.bluetooth.ble.BleConst;
import com.plusot.sensez.bluetooth.ble.BleDevice;
import com.plusot.sensez.bluetooth.ble.BleManufacturer;
import com.plusot.sensez.bluetooth.ble.GattAttribute;
import com.plusot.sensez.bluetooth.classic.BTDevice;
import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.sensez.config.SoapConfig;
import com.plusot.sensez.device.CommandType;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Numbers;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Tuple;

public class ScannedBluetoothDevice extends ScannedDevice implements Device.Listener {
    private static final boolean DEBUG = false;
    private static final double R_SPECIFIC_AIR = 287.058;	// J/(kg⋅K)
    private static final double R_SPECIFIC_WATER = 461.495;	// J/(kg⋅K)


    private long timeRead = -1;
    private final BluetoothDevice bluetoothDevice;
    BleDevice bleDevice = null;
    BTDevice btDevice = null;
    private int iconId = R.mipmap.ic_bt;
    private boolean beacon = false;
    private String manufacturer = null;
    private int manufacturerId = 0;

    ScannedBluetoothDevice(BluetoothDevice bluetoothDevice, long timeScanned, boolean isLowEnergy) {
        super(DeviceType.fromBluetoothDeviceTypeInt(bluetoothDevice.getType()));
        //this.blueMan = blueMan;
        this.timeRead = timeScanned;
        this.bluetoothDevice = bluetoothDevice;
        //this.isLowEnergy = isLowEnergy;
        int deviceClassInt;
        BTHelper.BTTypeMajor typeMajor = BTHelper.BTTypeMajor.fromInt(bluetoothDevice.getBluetoothClass().getMajorDeviceClass());
        BTHelper.BTTypeMinor deviceClass = BTHelper.BTTypeMinor.fromInt(deviceClassInt = bluetoothDevice.getBluetoothClass().getDeviceClass());
        if (DEBUG) LLog.i("Device class is " + deviceClass.toString() + " (" + StringUtil.toHexString(deviceClassInt) + ")");
        if (typeMajor == null) return;
        switch (typeMajor) {
            case COMPUTER:
                iconId = R.mipmap.ic_computer;
            case PHONE:
                iconId = R.mipmap.ic_phone;
            default:
                String name = BTHelper.getName(bluetoothDevice).toLowerCase();
                if (name.contains("vívosmart"))
                    iconId = R.mipmap.ic_vivosmart;
                else if (name.contains("estimote"))
                    iconId = R.mipmap.ic_estimote;
                else if (name.contains("keyboard"))
                    iconId = R.mipmap.ic_keyboard;
                else if (name.startsWith("mio"))
                    iconId = R.mipmap.ic_mio;
                else if (name.startsWith("alpha"))
                    iconId = R.mipmap.ic_alpha;
                else if (name.startsWith("ib300"))
                    iconId = R.mipmap.ic_ib300;
                else if (name.startsWith("k4"))
                    iconId = R.mipmap.ic_kestrel;
                else if (name.contains("radbeacon"))
                    iconId = R.mipmap.ic_radbeacon;
                else if (name.contains("ticwatch"))
                    iconId = R.drawable.mobvoi_ticwatch_e;
                else if (isLowEnergy)
                    iconId = R.mipmap.ic_ble;
        }
        deviceData.add(getAddress(), DataType.TYPE_MAJOR_STRING, typeMajor.toString());
    }

    @Override public String getName() {
        String name;
        if (this.name != null) return this.name;
        if ((name = deviceData.getStringWithoutLabel(getAddress(), DataType.RADBEACON_DEVICE_NAME))!= null) return name;
        if ((name = deviceData.getStringWithoutLabel(getAddress(), DataType.DEVICE_NAME))!= null) return name;
        return BTHelper.getName(bluetoothDevice);
    }

    @Override public String getAddress() {
        //if (bluetoothDevice.getName().startsWith("IB300"))
        return bluetoothDevice.getAddress();
    }

    public long getTimeRead() {
        return timeRead;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        if (selected) {
            if (bleDevice == null)
                bleDevice = new BleDevice(this, bluetoothDevice);
            else
                bleDevice.reconnect();
        } else {
            close();
        }
    }

    @SuppressWarnings("unused")
    public void sendCommand(String attributeAsString, byte[] values, BleDevice.WriteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        GattAttribute attr = GattAttribute.fromString(attributeAsString);
        if (bleDevice == null) {
            bleDevice = new BleDevice(this, bluetoothDevice, new Tuple<>(attr, values), true);
        } else if (bleDevice.getState() == BluetoothProfile.STATE_CONNECTED) {
            bleDevice.write(attr, listener, "", values);
        } else {
            bleDevice.reconnect();
        }
    }

//        public void write(GattAttribute attribute, String comment, int ... values) {
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
//            if (bleDevice != null && bleDevice.getState() == BluetoothProfile.STATE_CONNECTED) {
//                bleDevice.write(attribute, comment, values);
//            }
//        }


    @SuppressWarnings("unused")
    public void write(GattAttribute attribute, CommandType commandType, BleDevice.WriteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        if (bleDevice != null && bleDevice.getState() == BluetoothProfile.STATE_CONNECTED) {
            bleDevice.writeCommand(attribute, commandType, listener);
        }
    }

    public void close() {
        if (bleDevice != null) bleDevice.close();
        if (btDevice != null) btDevice.close();
        bleDevice = null;
        btDevice = null;
    }


    public void disconnect() {
        if (bleDevice != null) bleDevice.disconnect();
    }

    /*
            * 0	4C - Byte 1 (LSB) of Company identifier code
            * 1	00 - Byte 0 (MSB) of Company identifier code (0x004C == Apple)
            * 2	02 - Byte 0 of iBeacon advertisement indicator
            * 3	15 - Byte 1 of iBeacon advertisement indicator
            * 4	e2 |\
            * 5	c5 |\\
            * 6	6d |#\\
            * 7	b5 |##\\
            * 8	df |###\\
            * 9	fb |####\\
            * 10	48 |#####\\
            * 11	d2 |#####|| iBeacon proximity UUID
                    * 12	b0 |#####||
            * 13	60 |#####//
            * 14	d0 |####//
            * 15	f5 |###//
            * 16	a7 |##//
            * 17	10 |#//
            * 18	96 |//
            * 19	e0 |/
            * 20	00 - major
            * 21	00
            * 22	00 - minor
            * 23	00
            * 24	c5 - The 2's complement of the calibrated Tx Power
    */
    void parseScanRecord(byte[] scanRecord) {
        deviceData.setTime(System.currentTimeMillis());
//            LLog.i("Scan record: " + StringUtil.toHexString(scanRecord));
//            LLog.i("Scan record: " + StringUtil.toReadableString(scanRecord));
        int iCount = 0;
        while (iCount < scanRecord.length) {
            int dataLen = scanRecord[iCount] & 0xFF;
            if (dataLen == 0) break;
            if (iCount + 1 >= scanRecord.length) break;
            Tuple<ScanRecordType, String> type = ScanRecordType.stringFromId(scanRecord[iCount + 1]);
            //LLog.i("Record at " + iCount + ", datatype " + type.t2() + " = " + type.t1().parse(scanRecord, iCount + 2, dataLen - 1));

            int offset = iCount + 2;
            switch (type.t1()) {
                case DATA_TYPE_MANUFACTURER_SPECIFIC_DATA:
                    manufacturerId = Numbers.getUInt16(scanRecord, offset);
                    manufacturer = BleManufacturer.INSTANCE.getManufacturerName(manufacturerId);
                    deviceData.add(getAddress(), DataType.MANUFACTURER_ID, manufacturer);
                    switch (manufacturer) {
                        case "Texas Instruments Inc.":
                            iconId = R.mipmap.ic_sensortag;
                            //sensorTagFound = System.currentTimeMillis();
                            int keys = Numbers.getByte(scanRecord, 13);
                            deviceData.add(getAddress(), DataType.TI_LEFT_KEY, (keys & 0x1));
                            deviceData.add(getAddress(), DataType.TI_RIGHT_KEY, (keys & 0x2) >> 1);
                            deviceData.add(getAddress(), DataType.TI_REED_RELAY, (keys & 0x4) >> 1);
                            break;
                        case "Radius Networks Inc.":
                            iconId = R.mipmap.ic_radbeacon;
                            name = "RadBeacon";
                        default:
                            deviceData.add(getAddress(), DataType.IBEACON_ADVERTISEMENT, String.format("%X", Numbers.getUInt16(scanRecord, offset + 2)));
                            deviceData.add(getAddress(), DataType.IBEACON_MAJOR, Numbers.getUInt16(scanRecord, offset + 20));
                            deviceData.add(getAddress(), DataType.IBEACON_UUID, StringUtil.toHex(scanRecord, offset + 4, 16));
                            deviceData.add(getAddress(), DataType.IBEACON_MINOR, Numbers.getUInt16(scanRecord, offset + 22));
                            deviceData.add(getAddress(), DataType.IBEACON_POWER, Numbers.getUInt8(scanRecord, offset + 24));
                            break;
                    }
                    if (isSelected()) Man.fireData(this, deviceData, true);
                    break;
                case DATA_TYPE_PLUSOT_SCANDATA:
                    manufacturer = "Fooom Advanced Dispensing";
                    int magnetPartialStrokes = Numbers.getUInt16(scanRecord, offset + BleConst.MAGNET_PARTIAL_STROKES_REFILL_OFFSET);
                    if ((magnetPartialStrokes & 0x8000) == 0x8000) {
                        int magnetUsedVolume  = Numbers.getUInt16(scanRecord, offset + BleConst.MAGNET_USED_VOLUME_OFFSET);
                        int magnetStartAngle  = Numbers.getUInt8 (scanRecord, offset + BleConst.MAGNET_START_ANGLE_OFFSET);
                        int magnetEndAngle    = Numbers.getUInt8 (scanRecord, offset + BleConst.MAGNET_END_ANGLE_OFFSET);
                        deviceData.add(getAddress(), DataType.SOAPSENSE_PARTIAL_STROKES_REFILL_2, magnetPartialStrokes & 0x7fff);
                        deviceData.add(getAddress(), DataType.SOAPSENSE_USED_VOLUME, 0.00002f * magnetUsedVolume);
                        deviceData.add(getAddress(), DataType.ROTATION_MIN, 0.1f * magnetStartAngle);
                        deviceData.add(getAddress(), DataType.ROTATION_MAX, 0.1f * magnetEndAngle);
                    } else {
                        int stdDev = Numbers.getUInt8(scanRecord, offset + BleConst.STD_DEV_OFFSET);
                        int stdDevRot = Numbers.getUInt8(scanRecord, offset + BleConst.STD_DEV_ROT_OFFSET);
                        int rotationSpeed = Numbers.getSInt16(scanRecord, offset + BleConst.ROT_SPEED_OFFSET);
                        int rotationSpeedOff = Numbers.getSInt16(scanRecord, offset + BleConst.ROT_SPEED_OFF_OFFSET);
                        deviceData.add(getAddress(), DataType.ROTATION_SPEED, 0.01f * rotationSpeed);
                        deviceData.add(getAddress(), DataType.SOAPSENSE_ROTATION_SPEED_OFFSET, 0.01f * rotationSpeedOff);
                        deviceData.add(getAddress(), DataType.STANDARD_DEVIATION, 0.1f * stdDev);
                        deviceData.add(getAddress(), DataType.STANDARD_DEVIATION_2, 0.1f * stdDevRot);
                    }


                    int startAngle       = Numbers.getUInt8 (scanRecord, offset + BleConst.START_ANGLE_OFFSET);
                    int endAngle         = Numbers.getUInt8 (scanRecord, offset + BleConst.END_ANGLE_OFFSET);
                    int calibration      = Numbers.getUInt8 (scanRecord, offset + BleConst.CALIBRATION_OFFSET);

                    deviceData.add(getAddress(), DataType.ROTATION_ACCELERO_MIN, 0.1f * startAngle);
                    deviceData.add(getAddress(), DataType.ROTATION_ACCELERO_MAX, 0.1f * endAngle);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_CALIBRATION_RESULT, calibration);

                    if (isSelected()) Man.fireData(this, deviceData, true);
                    break;
                case DATA_TYPE_PLUSOT_SOAPDATA:
                    manufacturer = "Fooom Advanced Dispensing";

                    deviceData.add(getAddress(), DataType.MANUFACTURER_ID, "Fooom Advanced Dispensing");
                    iconId = R.mipmap.ic_soap;
                    //sensorTagFound = System.currentTimeMillis();

                    int angleX           = Numbers.getSInt16(scanRecord, offset + BleConst.ACTUATOR_ANGLE_OFFSET);
                    int angleX2          = Numbers.getSInt16(scanRecord, offset + BleConst.ACTUATOR_ANGLE2_OFFSET);
                    int angleRaw         = Numbers.getUInt8(scanRecord, offset + BleConst.RAW_ANGLE_OFFSET);
                    int totalStrokes     = Numbers.getUInt24(scanRecord, offset + BleConst.TOTAL_STROKES_OFFSET);
                    int acceleration     = Numbers.getUInt16(scanRecord, offset + BleConst.ACCELERATION_OFFSET);
                    int partialStrokes   = Numbers.getUInt16(scanRecord, offset + BleConst.PARTIAL_STROKES_REFILL_OFFSET);
                    int refills          = Numbers.getUInt16(scanRecord, offset + BleConst.REFILLS_OFFSET);
                    int usedVolume       = Numbers.getUInt16(scanRecord, offset + BleConst.USED_VOLUME_OFFSET);
                    int usedCount        = Numbers.getUInt16(scanRecord, offset + BleConst.USED_COUNT_OFFSET);
                    int minAngle         = Numbers.getUInt16(scanRecord, offset + BleConst.MIN_ANGLE_OFFSET);

                    long timeSinceRefill = Numbers.getUInt16(scanRecord, offset + BleConst.HOURS_SINCE_REFILL_ADVERT_OFFSET);
                    if ((timeSinceRefill & 0x8000) == 0x8000)
                        timeSinceRefill = 1000 * (timeSinceRefill & 0x7fff);
                    else
                        timeSinceRefill = timeSinceRefill * 3600000;
                    long tenthsAlive = Numbers.getUInt24(scanRecord, offset + BleConst.ALIVE_ADVERT_OFFSET);
                    if ((tenthsAlive & 0x800000) == 0x800000)
                        tenthsAlive = 100 * (tenthsAlive & 0x7fffff);
                    else
                        tenthsAlive *= 86400000;

                    int battery = Numbers.getUInt8(scanRecord, offset + BleConst.BATTERY_ADVERT_OFFSET);


                    deviceData.add(getAddress(), DataType.SOAPSENSE_MIN_ANGLE, 0.01f * minAngle);
                    deviceData.add(getAddress(), DataType.ROTATION_ACCELERO, 0.01f * angleX);
                    deviceData.add(getAddress(), DataType.ROTATION, 0.01f * angleX2);
                    deviceData.add(getAddress(), DataType.ROTATION_RAW, 0.1f * angleRaw);
                    deviceData.add(getAddress(), DataType.ACCELERATION, 9.81f * acceleration / 16384);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_TOTAL_STROKES, totalStrokes);
                    //deviceData.addWatchable(getAddress(), DataType.SOAPSENSE_FULL_STROKES_REFILL, fullStrokes);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_PARTIAL_STROKES_REFILL, partialStrokes);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_REFILLS, refills);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_USED_VOLUME_ACCELERO, 0.00002f * usedVolume);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_USED_COUNT, usedCount);
                    deviceData.add(getAddress(), DataType.SOAPSENSE_TIME_SINCE_REFILL, timeSinceRefill);
                    deviceData.add(getAddress(), DataType.TIME_ACTIVITY, tenthsAlive);
                    deviceData.add(getAddress(), DataType.BATTERY_LEVEL, 0.1f * battery);

                    if (isSelected()) Man.fireData(this, deviceData, true);
                    break;
                case DATA_TYPE_PLUSOT_MTAG:
                    manufacturer = "Plusot";
                    iconId = R.mipmap.ic_sensortag;
                    int rawObject = Numbers.getUInt16(scanRecord, BleConst.MTAG_IR_TEMPERATURE_LOCAL_OFFSET);
                    int rawAmbient = Numbers.getUInt16(scanRecord, BleConst.MTAG_IR_TEMPERATURE_TARGET_OFFSET);
                    deviceData.add(getAddress(), DataType.TEMPERATURE_OBJECT, 1.0 * rawObject / 128);
                    deviceData.add(getAddress(), DataType.TEMPERATURE_IR, 1.0 * rawAmbient / 128);

                    int rawTemp = Numbers.getUInt16(scanRecord, BleConst.MTAG_TEMPERATURE_OFFSET);
                    int rawHum = Numbers.getUInt16(scanRecord, BleConst.MTAG_HUMIDITY_OFFSET);
                    double temperature = 165.0 * rawTemp / 65536 - 40;
                    double humidity = 1.0 *  rawHum / 65536;
                    deviceData.add(getAddress(), DataType.HUMIDITY, humidity);
                    deviceData.add(getAddress(), DataType.TEMPERATURE, temperature);

                    int rawLight = Numbers.getUInt16(scanRecord, BleConst.MTAG_LUMINENCE_OFFSET);
                    int mantissa = (rawLight & 0x0FFF);
                    int exponent = (rawLight & 0xF000) >> 12;
                    double light = 0.01 * mantissa * Math.pow(2, exponent);
                    deviceData.add(getAddress(), DataType.LIGHT, light);

                    int rawTemperatureValue = Numbers.getUInt24(scanRecord, BleConst.MTAG_TEMPERATURE2_OFFSET);
                    int rawPressureValue = Numbers.getUInt24(scanRecord, BleConst.MTAG_PRESSURE_OFFSET);
                    double pressure;
                    deviceData.add(getAddress(), DataType.AIR_PRESSURE, pressure = 1.0 * rawPressureValue);
                    deviceData.add(getAddress(), DataType.TEMPERATURE_2, temperature = 0.01 * rawTemperatureValue);

                    double tKelvin = temperature + 273.15;
					double pSaturation = 611.21 * Math.exp((18.678 - temperature / 234.5) * (temperature / (temperature + 257.14))); // Buck equation, Pa
                    double pVapor = humidity * pSaturation;
                    double pDryAir = pressure - pVapor;
                    double rho = pDryAir / (tKelvin * R_SPECIFIC_AIR) + pVapor / (tKelvin * R_SPECIFIC_WATER); // kg/m3
//                    LLog.i("Rho = " + Format.format(rho, 3) + ", Rho dry air = " + Format.format(rhoDryAir, 3));
                    deviceData.add(getAddress(),DataType.RHO, rho);

                    Man.fireData(this, deviceData, true);
                    beacon = true;

            }
            iCount += dataLen + 1;

        }
    }

    @Override public void onDeviceState(Device device, Device.StateInfo state) {
        if (DEBUG) LLog.i("State = " + state);
        switch (state) {
            case DISCONNECTED:
                break;
            case CLOSED:
                if (device == bleDevice) bleDevice = null;
                if (device == btDevice) btDevice = null;
                break;

        }
        Man.fireState(this, state);
    }

    @Override public void onDeviceData(Device device, Data data) {
        reconnectCount = 0;
        timeRead = data.getTime();
        deviceData.merge(data);
        Man.fireData(this, data, true);
    }

    @Override public void onCommandDone(Device device, String commandId, byte[] data, boolean success, boolean closeOnDone) {
        LLog.i("Result of command for " + commandId + " = " + success);
        if (commandId.equals(GattAttribute.SOAP_CONFIG.toString())) {
            if (data.length == 1 && data[0] != 0) {
                if ((data[0] & SoapConfig.CALIBRATE_COMMAND) > 0) ToastHelper.showToastLong(R.string.calibrationRunning);
                LLog.i("Sending confirmation command " + commandId);
                if (bleDevice != null && bleDevice.write(GattAttribute.SOAP_CONFIG, null, "Confirmation", new byte[]{0})) return;
            }
        }
        if (closeOnDone && bleDevice != null) {
            bleDevice.close();
            bleDevice = null;
        }
    }


    @Override public int getIconId() {
        return iconId;
    }

    @Override public boolean isBeacon() {
        return beacon;
    }

    void setTimeRead(long timeRead) {
        this.timeRead = timeRead;
    }

    @Override public String getManufacturer() {
        if (manufacturerId > 0) manufacturer = BleManufacturer.INSTANCE.getManufacturerName(manufacturerId);
        if (manufacturer == null) return "";
        return manufacturer;
    }


}
