package com.plusot.sensez.bluetooth.classic;

import java.util.Date;

public abstract class Command {
	private final int interval;
	private final int startDelay;
	private long lastUsed = 0;
	private final long started = System.currentTimeMillis();

	public Command(final int startDelay, final int interval) {
		this.interval = interval;
		this.startDelay = startDelay;
		
	}
	
	public boolean maySend() {
		long now = new Date(). getTime();
		if (lastUsed == 0) {
			if (startDelay < 0)
				return false;
			else if (Math.abs(now - started) > startDelay) {
				lastUsed = now;
				return true;
			}
		} else if (interval <= 0 )
			return false;
		else if (Math.abs(now - lastUsed) > interval) {
			lastUsed = now;
			return true;
		}
		return false;
	}
	
	public abstract byte[] getBytes();
}