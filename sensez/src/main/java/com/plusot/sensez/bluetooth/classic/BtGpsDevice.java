package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.plusot.sensez.device.Nmea;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Numbers;
import com.plusot.util.util.TimeUtil;

class BtGpsDevice extends BTDevice {
    private static final boolean DEBUG = false;
    private int prevSatellites = 0;

    BtGpsDevice(BluetoothDevice bluetoothDevice, Listener listener) {
        super(bluetoothDevice, listener);
    }

    @Override
    public String getModelName() {
        return "NmeaBtGPS";
    }

    @Override
    public BTModel getModel() {
        return BTModel.BT_GPS;
    }

    @Override
    public void open(final BluetoothSocket socket) {
        super.open(socket);

        connectedThread = new BTConnectedThread(
                this,
                socket,
                null,
                BTConnectedThread.Type.WITH_LINEENDS);
        connectedThread.start();
    }

    private void parseNmea(long timestamp, String nmea) {
        //if (DEBUG && nmea != null && !nmea.equalsIgnoreCase("")) LLog.i("nmea = " + nmea + " at " + TimeUtil.formatTime(timestamp));
        if (nmea == null) return;
        if (nmea.startsWith("$GP")) {
            String[] parts = nmea.split(",");

            if (parts.length > 0) {
                Nmea nmeaType = Nmea.fromString(parts[0]);
                if (nmeaType != null) switch (nmeaType) {
                    case GPGSV:
                        if (parts.length > 4) {
                            if (DEBUG) LLog.i("nmea Satellites raw: " + parts[3]);
                            try {
                                int satellites = Numbers.parseInt(parts[3]);
                                if (satellites > 0 && satellites != prevSatellites) {
                                    if (DEBUG) LLog.i("nmea Satellites: " + satellites);
                                    prevSatellites = satellites;
                                    fireData(new Data().add(getAddress(), DataType.SATELLITES_VISIBLE, satellites));
                                }
                            } catch (NumberFormatException e) {
                                LLog.e("nmea Invalid number: " + parts[3]);
                            }
                        }
                        break;
                    case GNGGA:
                    case GPGGA:
                        if (parts.length > 7 && parts[1] != null && parts[1].length() > 0) {
                            Data data = new Data();
                            if (DEBUG)
                                LLog.i("nmea: " + nmea + " at " + TimeUtil.formatTime(timestamp));
                            String parse = parts[1];
                            String[] timeParts = parse.split("\\.");
                            if (timeParts.length > 1) {
                                timeParts[1] = (timeParts[1] + "000").substring(0, 3);
                                parse = TimeUtil.formatTimeUTC(System.currentTimeMillis(), "yyyy-MM-dd") + " " + timeParts[0] + '.' + timeParts[1];
                            } else {
                                parse = TimeUtil.formatTimeUTC(System.currentTimeMillis(), "yyyy-MM-dd") + " " + timeParts[0] + ".000";
                            }
                            long time = TimeUtil.parseTimeUTC(parse, "yyyy-MM-dd HHmmss.S");

                            if (DEBUG)
                                LLog.i("nmea: " + parse + " -> Time = " + TimeUtil.formatMilli(time, 3));
                            data.   add(getAddress(), DataType.TIME_GPS, time).
                                    add(getAddress(), DataType.TIME_CLOCK, timestamp).
                                    add(getAddress(), DataType.TIME_ACCURACY, timestamp - time);


                            if (parts[2].length() > 3 && parts[4].length() > 3) {
                                double lat = Numbers.parseInt(parts[2].substring(0, 2)) + Numbers.parseDouble(parts[2].substring(2)) / 60;
                                if (parts[3].equals("S")) lat *= -1;
                                double lng = Numbers.parseInt(parts[4].substring(0, 3)) + Numbers.parseDouble(parts[4].substring(3)) / 60;
                                if (parts[5].equals("W")) lng *= -1;
                                data.add(getAddress(), DataType.LOCATION, new double[]{lat, lng});
                            }
                            int quality = Numbers.parseInt(parts[6]);
                            data.add(getAddress(), DataType.POSITION_QUALITY, quality);
                            int satellites = Numbers.parseInt(parts[7]);
                            data.add(getAddress(), DataType.SATELLITES_FIX, satellites);
                            if (parts[8].length() > 0) {
                                double hdop = +Numbers.parseDouble(parts[8]);
                                data.add(getAddress(), DataType.HDOP, hdop);
                            }
                            if (parts[9].length() > 0) {
                                double alt = +Numbers.parseDouble(parts[9]);
                                data.add(getAddress(), DataType.ALTITUDE, alt);
                            }
                            if (parts[11].length() > 0) {
                                double geoid = +Numbers.parseDouble(parts[11]);
                                data.add(getAddress(), DataType.GEOID_HEIGHT, geoid);
                            }
                            data.add(getAddress(), DataType.NMEA, nmea);
                            fireData(data);
                        }
                        break;
                    default:
                        break;

                }
            }
        } else {
            fireData(new Data().add(getAddress(), DataType.INFO, nmea));
        }
    }


    @Override
    public void onBluetoothRead(BTConnectedThread sender, String msg, long time) {
        Data data = new Data();
        if (DEBUG) LLog.i("Received: " + msg);
        fireState(StateInfo.COMS);

//					data.add(getAddress(), DataType.WIND_SPEED, windSpeed);
        msg = msg.replace("\r\n", "\n").replace("\r", "\n");
        String[] nmeaStrs = msg.split("\n");
        for (String nmea: nmeaStrs) parseNmea(time, nmea);

        if (data.hasData()) fireData(data);
    }

    @Override
    public void onBluetoothReadBytes(BTConnectedThread sender, byte[] msg, long time) {
    }

}