package com.plusot.sensez.bluetooth;

import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Tuple;

/**
 * Created by peet on 2/11/16.
 */

public enum ScanRecordType {
    DATA_TYPE_FLAGS((byte)0x01),
    DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL((byte)0x02),
    DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE((byte)0x03),
    DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL((byte)0x04),
    DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE((byte)0x05),
    DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL((byte)0x06),
    DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE((byte)0x07),
    DATA_TYPE_LOCAL_NAME_SHORT((byte)0x08) {
        @Override
        public String parse(byte[] record, int pos, int len) {
            return StringUtil.toReadableString(record, pos, len);
        }
    },
    DATA_TYPE_LOCAL_NAME_COMPLETE((byte)0x09) {
        @Override
        public String parse(byte[] record, int pos, int len) {
            return StringUtil.toReadableString(record, pos, len);
        }
    },
    DATA_TYPE_TX_POWER_LEVEL((byte)0x0A),
    DATA_TYPE_DEVICE_CLASS((byte)0x0D),
    DATA_TYPE_SIMPLE_PAIRING_HASH_C192((byte)0x0E),
    DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R192((byte)0x0F),
    DATA_TYPE_DEVICE_ID_OR_SECURITY_TK((byte)0x10),
    DATA_TYPE_SECURITY_OUT_OF_BANDS_FLAG((byte)0x11),
    DATA_TYPE_SLAVE_CONNECTION_INTERVAL_RANGE((byte)0x12),
    DATA_TYPE_SOLICITATION_UUID_16_BIT((byte)0x14),
    DATA_TYPE_SOLICITATION_UUID_128_BIT((byte)0x15),
    DATA_TYPE_SERVICE_DATA_16_BIT((byte)0x16),
    DATA_TYPE_PUBLIC_TARGET((byte)0x17),
    DATA_TYPE_RANDOM_TARGET((byte)0x18),
    DATA_TYPE_APPEARANCE((byte)0x19),
    DATA_TYPE_ADVERTISING_INTERVAL((byte)0x1A),
    DATA_TYPE_BLE_ADDRESS((byte)0x1B),
    DATA_TYPE_LE_ROLE((byte)0x1C),
    DATA_TYPE_SIMPLE_PAIRING_HASH_C256((byte)0x1D),
    DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R256((byte)0x1E),
    DATA_TYPE_SOLICITATION_UUID_32_BIT((byte)0x1F),
    DATA_TYPE_SERVICE_DATA_32_BIT((byte)0x20),
    DATA_TYPE_SERVICE_DATA_128_BIT((byte)0x21),
    DATA_TYPE_LE_SECURE_CONFIRMATION((byte)0x22),
    DATA_TYPE_LE_SECURE_RANDOM((byte)0x23),
    DATA_TYPE_URI((byte)0x24),
    DATA_TYPE_INDOOR_POSITIONING((byte)0x25),
    DATA_TYPE_TRANSPORT_DISCOVERY((byte)0x26),
    DATA_TYPE_3D_INFO((byte)0x3D),
    DATA_TYPE_PLUSOT_MTAG((byte)0xFC),
    DATA_TYPE_PLUSOT_SCANDATA((byte)0xFD),
    DATA_TYPE_PLUSOT_SOAPDATA((byte)0xFE),
    DATA_TYPE_MANUFACTURER_SPECIFIC_DATA((byte)0xFF),
    UNKNOWN((byte)0)
    ;
    private final byte id;
    ScanRecordType(byte id) {
        this.id = id;
    }

    public static ScanRecordType fromId(byte id) {
        for (ScanRecordType type : values())
            if (id == type.id) return type;
        return UNKNOWN;
    }

    public static Tuple<ScanRecordType, String> stringFromId(byte id) {
        ScanRecordType type = fromId(id);
        if (type != UNKNOWN) return new Tuple<>(type, type.toString());
        return new Tuple<> (UNKNOWN, StringUtil.toHexString(id));
    }

    public String parse(byte[] record, int pos, int len) {
        return StringUtil.toHex(record, pos, len);
    }
}
