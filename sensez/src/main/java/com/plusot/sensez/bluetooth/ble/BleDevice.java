package com.plusot.sensez.bluetooth.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;

import com.plusot.sensez.bluetooth.ScannedBluetoothDevice;
import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.sensez.device.CommandType;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Numbers;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Tuple;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class BleDevice extends Device {
    private static final long THREAD_TIMEOUT = 800; //10000;
    private static long GATT_WAIT = 250;
    //    private final ScannedDevice scannedDevice;
    private BluetoothGatt gatt;
    private final BluetoothDevice device;
    private Map<GattAttribute, Boolean> notifications = new HashMap<>();
    private Map<GattAttribute, Boolean> indications = new HashMap<>();
    private LinkedBlockingQueue<GattCall> callQueue = new LinkedBlockingQueue<>();
    private final Object callLock = new Object();
    private final Object listLock = new Object();

    private int state = BluetoothProfile.STATE_CONNECTING;
    private boolean mayRun = true;
    private Thread runner;
    private BluetoothGattCallback gattCallback;
    private Map<GattAttribute, BluetoothGattCharacteristic> characteristics = new HashMap<>();
    private Field deviceBusyField = null;
    //    private Tuple<GattAttribute, byte[]> command;
//    private boolean soapOnly = false;
    private BluetoothGattCharacteristic smartPulseCharacteristic = null;

    private String success(boolean value) {
        if (value) return "successful";
        return "failed";
    }

    abstract class GattCall {
        public abstract void fireGatt();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private enum BluetoothGattStatus {
        SUCCESS(BluetoothGatt.GATT_SUCCESS),
        FAILURE(BluetoothGatt.GATT_FAILURE),
        READ_NOT_PERMITTED(BluetoothGatt.GATT_READ_NOT_PERMITTED),
        WRITE_NOT_PERMITTED(BluetoothGatt.GATT_WRITE_NOT_PERMITTED),
        INSUFFICIENT_AUTHENTICATON  (BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION),
        REQUEST_NOT_SUPPORTED(BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED),
        INSUFFICIENT_ENCRYPTION (BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION),
        INVALID_OFFSET(BluetoothGatt.GATT_INVALID_OFFSET),
        INVALID_ATTRIBUTE_LENGTH(BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH),
        CONNECTION_CONGESTED(BluetoothGatt.GATT_CONNECTION_CONGESTED),
        UNKNOWN_STATUS(-1),
        ;
        final int id;
        BluetoothGattStatus(int id) {
            this.id = id;
        }

        static BluetoothGattStatus fromId(int id) {
            for (BluetoothGattStatus status: BluetoothGattStatus.values()) {
                if (status.id == id) return status;
            }
            LLog.i("Unknown status " + id);
            return UNKNOWN_STATUS;
        }
    }

    public int getState() {
        return state;
    }

    private class CharacteristicReadCall extends GattCall {
        final BluetoothGattCharacteristic characteristic;

        CharacteristicReadCall(BluetoothGattCharacteristic characteristic) {
            this.characteristic = characteristic;
        }

        @Override
        public void fireGatt() {
            //LLog.i("Firing read: " + GattAttribute.fromCharacteristic(characteristic));
            readCharacteristic(characteristic);
        }
    }
    private class CharacteristicWriteCall extends GattCall {
        final BluetoothGattCharacteristic characteristic;
        final byte[] bytes;
        final String comment;
        final int writeType;
        final WriteListener listener;

        CharacteristicWriteCall(BluetoothGattCharacteristic characteristic, final byte[] bytes, final int writeType, final String comment, final WriteListener listener) {
            this.characteristic = characteristic;
            this.bytes = bytes;
            this.comment = comment;
            this.writeType = writeType;
            this.listener = listener;
        }

        @Override
        public void fireGatt() {
            if (comment != null)
                LLog.i("Firing write for " + comment + ": " + GattAttribute.fromCharacteristic(characteristic) + ": " + StringUtil.toHexString(bytes));
            else
                LLog.i("Firing write: " + GattAttribute.fromCharacteristic(characteristic) + ": " + StringUtil.toHexString(bytes));
            writeCharacteristic(characteristic, bytes, writeType, listener);
        }
    }

    private class CharacteristicWriteCall2 extends GattCall {
        final BluetoothGattCharacteristic characteristic;
        final List<Tuple<byte[], String>> cmds;
        final int writeType;
        final WriteListener listener;

        CharacteristicWriteCall2(BluetoothGattCharacteristic characteristic,
                                 final List<Tuple<byte[], String>> cmds,
                                 final int writeType,
                                 final WriteListener listener) {
            this.characteristic = characteristic;
            this.cmds = cmds;
            this.writeType = writeType;
            this.listener = listener;
        }

        @Override
        public void fireGatt() {
            int i = 0;
            for (Tuple<byte[], String> cmd: cmds) {
                if (cmd.t2() != null)
                    LLog.i("Firing write for " + cmd.t2() + ": " + GattAttribute.fromCharacteristic(characteristic) + ": " + StringUtil.toReadableString(cmd.t1()));
                else
                    LLog.i("Firing write: " + GattAttribute.fromCharacteristic(characteristic) + ": " + StringUtil.toHex(cmd.t1()));
                if (++i == cmds.size())
                    writeCharacteristic(characteristic, cmd.t1(), writeType, listener);
                else
                    writeCharacteristic(characteristic, cmd.t1(), writeType, null);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    LLog.i("Interrupted");
                }
            }
        }
    }

    private class NotificationCall extends GattCall {
        final BluetoothGattCharacteristic characteristic;
        final boolean enable;

        NotificationCall(BluetoothGattCharacteristic characteristic, boolean enable) {
            this.characteristic = characteristic;
            this.enable = enable;
        }

        @Override
        public void fireGatt() {
            GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
            //LLog.d("Firing notification '" + enable + "' for " + attr);
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
            if (gatt.setCharacteristicNotification(characteristic, enable) && attr.hasDescriptor()) {
                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.getUuid());
                if (descriptor != null) {
                    Boolean done;
                    synchronized (listLock) {
                        done = notifications.get(attr);
                        if (done == null) {
                            done = false;
                            notifications.put(attr, false);
                        }
                    }
                    if (done != enable) {
//                        LLog.d("Switch notifications for " + attr + " of " + getName() + " to: " + enable);
                        if (enable)
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        else
                            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                        boolean result = gatt.writeDescriptor(descriptor);
                        LLog.d("Result of writing descriptor " + attr + "  = " + success(result));
//                        notifications.put(attr, result);
                    }
                }
                SleepAndWake.runInMain(4000, () -> {
                    synchronized (listLock) {
                        Boolean value = notifications.get(attr);
                        if (gatt != null
                                && state == BluetoothProfile.STATE_CONNECTED
                                && Globals.runMode.isRun() &&  (value == null || value != enable))
                            synchronized (callLock) {
                                callQueue.offer(new NotificationCall(characteristic, true));
                            }
                    }
                });
            }
        }
    }

    private class IndicationCall extends GattCall {
        final BluetoothGattCharacteristic characteristic;
        final boolean enable;


        IndicationCall(BluetoothGattCharacteristic characteristic, boolean enable) {
            this.characteristic = characteristic;
            this.enable = enable;
        }

        @Override
        public void fireGatt() {
            GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
            //LLog.d("Firing indication '" + enable + "' for " + attr);
            if (gatt.setCharacteristicNotification(characteristic, enable) && attr.hasDescriptor()) {
                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.getUuid());
                if (descriptor != null) {
                    Boolean done;
                    synchronized (listLock) {
                        done = indications.get(attr);
                        if (done == null) {
                            done = false;
                            indications.put(attr, false);
                        }
                    }

                    if (done != enable) {
//                        LLog.d("Switch indications for " + attr + " of " + getName() + " to: " + enable);
                        if (enable)
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                        else
                            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                        boolean result = gatt.writeDescriptor(descriptor);
                        LLog.d("Result of writing descriptor " + attr + " = " + success(result));
//                        indications.put(attr, result);
                    }
                    SleepAndWake.runInMain(4000, () -> {
                        synchronized (listLock) {
                            Boolean value = indications.get(attr);
                            if (gatt != null
                                    && state == BluetoothProfile.STATE_CONNECTED
                                    && Globals.runMode.isRun() && (value == null || value != enable))
                                synchronized (callLock) {
                                    callQueue.offer(new IndicationCall(characteristic, true));
                                }
                        }
                    });
                }
            }
        }
    }

//    public interface Listener {
//        void onBleData(BluetoothDevice device, BluetoothGattCharacteristic characteristic, GattAction action, Data data);
////        void onBluetoothRawData(BluetoothDevice device, BluetoothGattCharacteristic characteristic, GattAction action, byte[] data);
//        void onBleClose(BluetoothDevice device);
//    }

    public BleDevice(final ScannedBluetoothDevice scannedDevice, final BluetoothDevice device) {
        this(scannedDevice, device, null, false);

    }


    public BleDevice(final ScannedBluetoothDevice scannedDevice, final BluetoothDevice device, final Tuple<GattAttribute, byte[]> command, boolean singleCall) {
        super(scannedDevice);
//        this.scannedDevice = scannedDevice;
//        this.command = command;
        this.singleCall = singleCall;
        this.device = device;
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;


        gattCallback = new BluetoothGattCallback() {

            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
//                LLog.i("Connection state changed: " + BTHelper.getName(gatt.getDevice()));
                state = newState;
                switch (newState) {
                    case BluetoothProfile.STATE_CONNECTING:
//                        LLog.i(BTHelper.getName(gatt.getDevice()) + " Connecting to GATT server.");
                        break;
                    case BluetoothProfile.STATE_CONNECTED:
//                        LLog.i(BTHelper.getName(gatt.getDevice()) + " Connected to GATT server.");
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            //boolean result =
                            gatt.discoverServices();
                            //LLog.i(BTHelper.getName(gatt.getDevice()) + " Attempting to start service discovery:" + result);
                        }
                        BleDevice.this.fireState(StateInfo.CONNECTED);
                        break;
                    case BluetoothProfile.STATE_DISCONNECTING:
                        LLog.i(BTHelper.getName(gatt.getDevice()) + " Disconnecting from GATT server.");
                        break;
                    case BluetoothProfile.STATE_DISCONNECTED:
                        LLog.i(BTHelper.getName(gatt.getDevice()) + " Disconnected from GATT server.");

                        synchronized (listLock) {
                            notifications.clear();
                            indications.clear();
                        }
                        BleDevice.this.fireState(StateInfo.DISCONNECTED);
                        break;
                    default:
                        LLog.i("New state: " + newState);
                        break;
                }

            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                LLog.i("Services discovered: " + BTHelper.getName(gatt.getDevice()) +  " with status " + BluetoothGattStatus.fromId(status) + "(" + status + ")");
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (gatt.getDevice().getAddress().startsWith("B0:B4:48")) {
                        LLog.i("TI device found: " + gatt.getDevice().getAddress());
//                        soapOnly = true;
                    }
                    SleepAndWake.runInMain(() -> { for (BluetoothGattService gattService : gatt.getServices()) {
                        UUID serviceUuid = gattService.getUuid();
                        GattAttribute service = GattAttribute.fromUUID(serviceUuid);
                        if (service == GattAttribute.UNKNOWN) {
                            LLog.i("Found unknown gatt service: " + serviceUuid);
                            continue;
                        }

                        if (!service.isSupported()) {
                            LLog.i("Skipping verbose service: " + service);
                            continue;
                        }

                        LLog.i("Service = " + service);
                        if (mayRun && service.allowed(getName())) for (BluetoothGattCharacteristic characteristic : gattService.getCharacteristics()) {
                            GattAttribute attribute = GattAttribute.fromCharacteristic(characteristic);
                            if (attribute == GattAttribute.UNKNOWN) {
                                LLog.i("Found unknown gatt attribute: " + characteristic.getUuid());
                                continue;
                            }

                            if (!attribute.isSupported()) {
                                LLog.i("Skipping verbose gatt attribute: " + attribute);
                                continue;
                            }

//                                    if (!blockedAttribute.contains(characteristic)) {
                            if (BleDevice.this.singleCall) {
                                if (attribute == command.t1()) {
                                    //LLog.i("Command added for " + command.t1());
                                    callQueue.add(new CharacteristicWriteCall(characteristic, command.t2(), attribute.getWriteType(), "Single command", null));
                                }
                            } else if (attribute.allowed(getName())) {
                                LLog.i("Adding characteristic " + attribute);
                                characteristics.put(attribute, characteristic);
                                int properties = characteristic.getProperties();
                                //TODO: Check whether this works!!!!!
                                if (attribute.needsInit()) {  /*!initsDone &&*/
                                    LLog.i("Checking inits for " + attribute);
                                    if (attribute.initAlways() || scannedDevice.isDataTypeInView(attribute.getReferencedDataTypes())) {
                                        if ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE) == 0) {
                                            LLog.e("Property read not found!!!!!!!!!!!");
                                        } else {
                                            LLog.i("Adding inits for " + attribute + " to callQueue");

                                            List<Tuple<byte[], String>> inits = attribute.commands(CommandType.INIT);
                                            if (inits != null)
                                                for (Tuple<byte[], String> init : inits) {
                                                    callQueue.add(new CharacteristicWriteCall(characteristic, init.t1(), attribute.getWriteType(), init.t2(), null));
                                                }
                                            //initsDone = true;
                                        }
                                    }
                                }
                                if ((properties & BluetoothGattCharacteristic.PROPERTY_READ) > 0 && attribute.mayRead()) {
                                    //LLog.i("Adding read call for " + attribute);
                                    fireData(Data.fromDataTypes(attribute.getDataTypes()));
                                    callQueue.add(new CharacteristicReadCall(characteristic));
                                }
                                if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                                    //LLog.i("Adding notification call for " + attribute);
                                    fireData(Data.fromDataTypes(attribute.getDataTypes()));
                                    callQueue.add(new NotificationCall(characteristic, true));
                                }
                                if ((properties & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
                                    //LLog.i("Adding indication call for " + attribute);
                                    fireData(Data.fromDataTypes(attribute.getDataTypes()));
//                                            callQueue.addWatchable(new CharacteristicReadCall(characteristic));
                                    callQueue.add(new IndicationCall(characteristic, true));
                                }

                                if (attribute == GattAttribute.SMARTPULSE_COMM) {
                                    smartPulseCharacteristic = characteristic;
                                }
//                                            isSmartPulse = true;
//                                            //Interval S
//                                            callQueue.addWatchable(new CharacteristicWriteCall(characteristic,
//                                                    new byte[]{(byte) -120, (byte) 1, (byte) 6, (byte) 0, (byte) 12, (byte) 0, (byte) 30, (byte) 0, (byte) 0}, "Interval S"));
//                                            //Interval P
//                                            callQueue.addWatchable(new CharacteristicWriteCall(characteristic,
//                                                    new byte[]{(byte) 119, (byte) 1, (byte) 6, (byte) 0, (byte) 12, (byte) 0, (byte) 30, (byte) 0, (byte) 0}, "Interval P"));
//                                            callQueue.addWatchable(new CharacteristicWriteCall(characteristic,
//                                                    new byte[]{(byte) -35, (byte) 1, (byte) 0}, "DD"));
////                                            final BluetoothGattCharacteristic c = characteristic;
////                                            SleepAndWake.runInMain(new Runnable() {
////                                                @Override
////                                                public void runInThread() {
////                                                    write(c, "EXECUTING 4 - 0!!!", 4, 0);
////                                                }
////                                            }, 10000);
//                                        }
                            }
                        }
                    }
                        synchronized (callLock) {
                            //LLog.i("Notified after services discovered");
                            callLock.notifyAll();
                        }
                    }, GATT_WAIT * 4, "BleDevice.onServicesDiscovered");
                } else {
                    LLog.i("Received: " + status);
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
                    //LLog.i("Characteristic read: " + attr + " = " + StringUtil.toHex(characteristic.getValue()));
                    if (command != null && attr == command.t1()) {
                        fireCommandDone(attr.toString(), characteristic.getValue(), Arrays.equals(command.t2(), characteristic.getValue()));
                    }
                    fire(gatt, GattAction.ACTION_DATA_AVAILABLE, characteristic);
                } else {
                    LLog.i("Characteristic received status: " + BluetoothGattStatus.fromId(status) + "(" + status + ") for " + GattAttribute.fromCharacteristic(characteristic));
                }
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
                LLog.i("Characteristic written: " + attr + " / Notified");
//                if (isSmartPulse) {
//                    smartPulseCharacteristicFlag = true;
                //LLog.i("cWrite");
                //TO DO: Writing to SmartPulse???
//                    BLEManagerGoogle.this.imode = BLEManagerGoogle.this.iimode;
//                    BLEManagerGoogle.this.iimode = 0;
//                    BLEManagerGoogle.this.lastFN = 0;
//                    if (writeQueue.isEmpty()) return;

//                    BLEManagerGoogle.this.available = false;
//                    ReadyToSendElement readyToSendElement =  writeQueue.poll();
//                    BLEManagerGoogle.this.iimode = e.imode;
//                    ReceivedDataLogger.initFile(1, e.f199val[0]);
//                    LLog.i("Send element added");
//                    callQueue.addWatchable(new CharacteristicWriteCall(smartPulseCharacteristic, readyToSendElement.bytes, "Send Element"));
//                }
                if (mayRun && attr.needsReadAfterWrite()) {
                    callQueue.add(new CharacteristicReadCall(characteristic));

                    if (attr.characteristicToReadAfter() != null && characteristics.get(attr.characteristicToReadAfter()) != null) {
                        callQueue.add(new CharacteristicReadCall(characteristics.get(attr.characteristicToReadAfter())));
                    }
                }
                synchronized (callLock) {
                    callLock.notifyAll();
                }
                super.onCharacteristicWrite(gatt, characteristic, status);
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                //if (DEBUG) LLog.i("Characteristic changed: " + GattAttribute.fromCharacteristic(characteristic) + " / Notified");
//                switch (GattAttribute.fromCharacteristic(characteristic)) {
//                    case SMARTPULSE_CHECKER:
//                        //LLog.i("SmartPulse checker data received");
//                        handleSmartPulseData(characteristic);
//                        break;
//                    case SMARTPULSE_COMM:
//                        //handleSmartPulseData(characteristic);
//                        LLog.i("SmartPulse comm data received");
//                        break;
//                }
                fire(gatt, GattAction.ACTION_DATA_AVAILABLE, characteristic);
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//                LLog.i("Descriptor read: " + descriptor.getUuid() + " / Notified");
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//                LLog.i("Descriptor write: " + descriptor.getUuid() + " / Notified");
                GattAttribute attr = GattAttribute.fromCharacteristic(descriptor.getCharacteristic());
                fireState(StateInfo.COMS);

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    synchronized(listLock) {
                        if (indications.get(attr) != null) {
                            boolean enabled = Numbers.equalBytes(descriptor.getValue(),BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                            LLog.i(gatt.getDevice().getAddress() + " Indications for " + attr + " = " + enabled);
                            indications.put(attr, enabled);
                        } else if (notifications.get(attr) != null) {
                            boolean enabled = Numbers.equalBytes(descriptor.getValue(),BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            LLog.i(gatt.getDevice().getAddress() + " Notifications for " + attr + " = " + enabled);
                            notifications.put(attr, enabled);
                        } else
                            LLog.i(gatt.getDevice().getAddress() + " Descriptor write: " + descriptor.getUuid() + ",  " +
                                    attr + ": " + GattState.Companion.fromId(status) + " (" + StringUtil.toHexString(status) + ")");
                    }
                } else {
                    LLog.i(gatt.getDevice().getAddress() + " Descriptor write: " + descriptor.getUuid() + ",  " +
                            attr + ": " + GattState.Companion.fromId(status) + " (" + StringUtil.toHexString(status) + ")");
                }
                synchronized(callLock) {
                    callLock.notifyAll();
                }
                if (smartPulseCharacteristic != null) {
                    callQueue.add(new CharacteristicWriteCall(smartPulseCharacteristic, new byte[]{(byte) -35, (byte) 1, (byte) 0},
                            BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT,
                            "After write descriptor", null));
                }
            }


            public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
                LLog.i("Mtu changed. / Notified");
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

            public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                LLog.i("Read remote Rssi: " + rssi + " / Notified");
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

            public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
                LLog.i("Reliable write completed. / Notified");
                synchronized (callLock) {
                    callLock.notifyAll();
                }
            }

        };
//        LLog.i("Trying to connect Gatt for " + BTHelper.getName(device));

        runner = new Thread(() -> {
            while (Globals.runMode.isRun() && /*state != BluetoothProfile.STATE_DISCONNECTED && */mayRun) {
                try {

                    if (state == BluetoothProfile.STATE_CONNECTED) {
                        Boolean busy = getDeviceBusy();
                        if (busy == null || !busy) callQueue.take().fireGatt();
                    }
                    synchronized (callLock) {
                        long longAgo = System.currentTimeMillis();
                        callLock.wait(THREAD_TIMEOUT);
                        long w = System.currentTimeMillis() - longAgo;
                        //if (w < THREAD_TIMEOUT)
                        //LLog.i("Waking " + runner.getName() + " after " + w + "ms");
                        if (w < 250) Thread.sleep(250 - w);
                    }
                } catch (InterruptedException e) {
                    LLog.i("Runner interrupted");
                }
            }
        });
        runner.setName("Runner " + runner.getId());
        runner.start();

        connect();
    }


    public void connect() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        if (gatt != null) gatt.close();
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        gatt = device.connectGatt(appContext, false, gattCallback);
        try {
            deviceBusyField = BluetoothGatt.class.getDeclaredField("mDeviceBusy");
            deviceBusyField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            LLog.e("Could not access mDeviceBusy field of BluetoothGatt", e);
        }
    }

    public boolean reconnect() {
        if (gatt != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            LLog.i("Called reconnect for " + getName());
            return gatt.connect();
        }
        return false;
    }

    private Boolean getDeviceBusy() {
        if (gatt == null) return null;
        if (deviceBusyField == null) return null;
        try {
            Object obj = deviceBusyField.get(gatt);
            if (obj instanceof Boolean) {
                return (Boolean)obj;
            }
        } catch (IllegalAccessException e) {
            LLog.e("Could not access mDeviceBusy field of BluetoothGatt", e);
        }
        return null;
    }


    private static  Tuple<UUID, String> getServiceInfo(BluetoothGattService gattService) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return new Tuple<>(new UUID(0, 0), null);
        UUID serviceUuid = gattService.getUuid();
        return new Tuple<>(serviceUuid, GattAttribute.stringFromUUID(serviceUuid));
    }


    private static  Tuple<UUID, String> getServiceInfo(BluetoothGattCharacteristic characteristic) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return new Tuple<>(new UUID(0, 0), null);
        return getServiceInfo(characteristic.getService());
    }


    public void close() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        synchronized (listLock) {
            for (GattAttribute attr : notifications.keySet()) {
                Boolean value = notifications.get(attr);
                if (value != null && value) {
                    notifications.put(attr, false);
                    BluetoothGattCharacteristic characteristic;
                    if ((characteristic = characteristics.get(attr)) != null)
                        callQueue.add(new NotificationCall(characteristic, false));
                }
            }
            for (GattAttribute attr : indications.keySet()) {
                Boolean value = indications.get(attr);
                if (value != null && value) {
                    indications.put(attr, false);
                    BluetoothGattCharacteristic characteristic;
                    if ((characteristic = characteristics.get(attr)) != null)
                        callQueue.add(new IndicationCall(characteristic, false));
                }
            }
        }
        gatt.close();
        fireState(StateInfo.CLOSED);
        SleepAndWake.runInMain(() -> {
            mayRun = false;
            callQueue.clear();
            runner.interrupt();
        }, 1000, "BleDevice.close");
    }

    public void disconnect() {
        if (gatt != null) if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            gatt.disconnect();
        }
    }

    @Override
    public String getAddress() {
        return device.getAddress();
    }


    public static String getName(BluetoothGatt gatt) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return "unknown";
        return BTHelper.getName(gatt.getDevice());
    }


    public String getName() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return "unknown";
        return BTHelper.getName(gatt.getDevice());
    }


    private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
//        if (blockedAttribute.contains(attr)) {
//            synchronized (callLock) {
//                LLog.i("Notified wait by " + GattAttribute.fromCharacteristic(characteristic));
//                callLock.notifyAll();
//            }
//            return;
//        }

        if (!gatt.readCharacteristic(characteristic)) {
            LLog.i("Could not read characteristic " + attr + " for " + BleDevice.getName(gatt) + ", service " + getServiceInfo(characteristic).t2());
            //blockedAttribute.addWatchable(attr);
            try {
                Thread.sleep(250);
                if (!gatt.readCharacteristic(characteristic)) {
                    LLog.i("Could still not read characteristic " + attr + " for " + BleDevice.getName(gatt) + ", service " + getServiceInfo(characteristic).t2());
                    return;
                }
            } catch (InterruptedException e) {
                LLog.i("Interrupted");
            }
        }

        if (!singleCall && mayRun) synchronized (listLock) {
            Boolean done = notifications.get(attr);
            if (done == null) {
                done = false;
                notifications.put(attr, false);
            }
            if (!done && (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0 & attr.maySetNotification()) {
//                LLog.i("Set notify for " + BleDevice.getName(gatt) + " " + attr);
                callQueue.add(new NotificationCall(characteristic, true));
            }
        }
    }

    private void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] bytes, WriteListener listener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            writeCharacteristic(characteristic, bytes,  BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT, listener);
        }
    }

    public interface WriteListener {
        void onWrite(boolean success);
    }


    private void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] bytes, int writeType, WriteListener listener) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
        characteristic.setValue(bytes);
        characteristic.setWriteType(writeType);
        if (!gatt.writeCharacteristic(characteristic)) {
            LLog.i("Could not write characteristic " + attr + " for " + BleDevice.getName(gatt) + ", service " + getServiceInfo(characteristic).t2());
            try {
                Thread.sleep(250);
                if (gatt.writeCharacteristic(characteristic)) {
                    if (listener != null) listener.onWrite(true);
                }else {
                    if (listener != null) listener.onWrite(false);
                    LLog.i("Could still not write characteristic " + attr + " for " + BleDevice.getName(gatt) + ", service " + getServiceInfo(characteristic).t2());
                }
            } catch (InterruptedException e) {
                if (listener != null) listener.onWrite(false);
            }
        } else {
//            LLog.i("Write performed for " + attr +  " " + StringUtil.toHex(bytes));
            if (listener != null) listener.onWrite(true);
        }
    }


    private void fire(final BluetoothGatt gatt, final GattAction action,
                      final BluetoothGattCharacteristic characteristic) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return;
        GattAttribute attr = GattAttribute.fromUUID(characteristic.getUuid());
        fireData(attr.parseCharacteristic(gatt, characteristic), false);
    }

    public boolean write(GattAttribute gattAttribute, WriteListener listener, final String comment, byte[] values) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return false;
//        this.command = new Tuple<>(gattAttribute, values);
        for (BluetoothGattService gattService : gatt.getServices()) {
            UUID serviceUuid = gattService.getUuid();
            GattAttribute service = GattAttribute.fromUUID(serviceUuid);
            LLog.i("Service = " + GattAttribute.stringFromUUID(serviceUuid));
            if (mayRun && service.allowed(getName()))  {
                BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(gattAttribute.getUuid());
                if (characteristic != null) { // && callLock.singleCall) {
                    LLog.i("Command added for " + gattAttribute);
                    callQueue.add(new CharacteristicWriteCall(characteristic, values, gattAttribute.getWriteType(), comment, listener));
                    return true;
                }
            }
        }
        return false;

    }

    public boolean write(GattAttribute gattAttribute, WriteListener listener, final String comment, int ... values) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return false;
        for (BluetoothGattService gattService : gatt.getServices()) {
            LLog.i("Service = " + GattAttribute.stringFromUUID(gattService.getUuid()));
            if (mayRun) {
                BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(gattAttribute.getUuid());
                if (characteristic != null) {
                    LLog.i("Write added for " + gattAttribute + " " + StringUtil.toHex(values));
                    callQueue.add(new CharacteristicWriteCall(characteristic, gattAttribute.convertValues(values), gattAttribute.getWriteType(), comment, listener));
                    return true;
                }
            }
        }
        return false;

    }

    private final static boolean SPLIT_CMDS = false;

    public boolean writeCommand(GattAttribute gattAttribute, CommandType commandType, WriteListener listener) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return false;
        for (BluetoothGattService gattService : gatt.getServices()) {
//            LLog.i("Service = " + GattAttribute.stringFromUUID(gattService.getUuid()));
            if (mayRun) {
                BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(gattAttribute.getUuid());
                List<Tuple<byte[], String>> cmds = gattAttribute.commands(commandType);
                if (characteristic != null && cmds != null) {
                    if (SPLIT_CMDS) {
                        for (Tuple<byte[], String> cmd : cmds)
                            callQueue.add(new CharacteristicWriteCall(characteristic, cmd.t1(), gattAttribute.getWriteType(), cmd.t2(), listener));
                    } else {
                        callQueue.add(new CharacteristicWriteCall2(characteristic, cmds, gattAttribute.getWriteType(), listener));
                    }
                    return true;
                }
            }
        }
        return false;

    }

    public boolean write(BluetoothGattCharacteristic characteristic, WriteListener listener, final String comment, int ... values) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return false;
        if (mayRun) {
            if (characteristic != null) {
                GattAttribute attr = GattAttribute.fromCharacteristic(characteristic);
                LLog.i("Write added for " + attr + " " + StringUtil.toHex(values));
                callQueue.add(new CharacteristicWriteCall(characteristic, attr.convertValues(values), attr.getWriteType(), comment, listener));
                return true;
            }
        }
        return false;
    }

//        private class PulseData {
//        final int data;
//        final int interval;
//
//        private PulseData(int data, int interval) {
//            this.data = data;
//            this.interval = interval;
//        }
//    }
//    private List<PulseData> pulseData = new ArrayList<>();
//
//    private void addPulseData(int data, int interval) {
//        if (interval > 0) LLog.i("SmartPulse data " + data + " interval " + interval + " hr = " + (60000 / interval));
//        if (data >= 4095) {
//
//        } else
//            pulseData.addWatchable(new PulseData(data,interval));
//
//    }


}