package com.plusot.sensez.bluetooth.classic;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.plusot.sensez.bluetooth.util.BTHelper;
import com.plusot.sensez.device.Device;
import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;


public abstract class BTDevice extends Device implements BTConnectedThread.Listener {
	protected BTConnectedThread connectedThread;
    protected final BluetoothDevice bluetoothDevice;
	protected long active = 0;

	public enum BTModel {
		HXM,
		KESTREL,
		BT_GPS,
		BT_CHAT,
		UNKNOWN;

		public static BTModel fromDevice(BluetoothDevice device) {
			String name = BTHelper.getName(device);
			BTHelper.BTTypeMajor major = BTHelper.BTTypeMajor.fromDevice(device);
			if (name == null) return BTModel.UNKNOWN;
//			LLog.i("Name : " + name + " Major " + major);
			switch (major) {
				case COMPUTER:
				case PHONE:
					//if (value.startsWith("Nexus"))
					return BTModel.BT_GPS;
				case HEALTH:
				case IMAGING:
				case MISC:
				case NETWORKING:
				case TOY:
				case WEARABLE:
				case AUDIO_VIDEO:
				    break;
                case PERIPHERAL:
                case UNCATEGORIZED:
				default:
					if (name.startsWith("HXM"))
						return BTModel.HXM;
					if (name.startsWith("K4000") || name.startsWith("K4200") ||
							name.startsWith("K4300") || name.startsWith("K4400") ||
							name.startsWith("K4500") ||
							name.startsWith("CSR - bc4") || name.startsWith("00:06:66"))
						return BTModel.KESTREL;
					return BTModel.BT_GPS;
			}
			return BTModel.UNKNOWN;
		}
	}
	public BTDevice(BluetoothDevice bluetoothDevice, Listener listener) {
		super(listener);
        this.bluetoothDevice = bluetoothDevice;
	}

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public abstract String getModelName();
	public abstract BTModel getModel();

    public String getName() {
        return BTHelper.getName(bluetoothDevice);
    }

	public void open(final BluetoothSocket socket) {
		active = System.currentTimeMillis();
		LLog.i("Starting " + getModelName());
	}

	@Override
    public void close() {
		if (connectedThread == null) return; 
		LLog.i("Closing " + getModelName());
		connectedThread.close();
		connectedThread = null;
	}

	@Override
	public void onBluetoothStart(BTConnectedThread sender) {
		fireState(StateInfo.CONNECTED);
	}
	
	@Override
	public void onBluetoothTerminate(BTConnectedThread sender) {
		connectedThread = null;
		if (Globals.runMode.isRun()) {
			LLog.i("Closed " + getModelName());
			fireState(StateInfo.CLOSED);
		}
	}

	public void startLoopCommands() {
		if (connectedThread != null) {
			connectedThread.startLoopCommands();
		}
	}

	public void stopLoopCommands() {
		if (connectedThread != null) {
			connectedThread.stopLoopCommands();
		}
	}

	public static BTDevice create(BluetoothDevice device, Listener listener) {

		BTModel model = BTModel.fromDevice(device);
		if (model == null) return null;
		switch (model) {

			case HXM:
				return new HxmDevice(device, listener);
			case KESTREL:
				return new KestrelDevice(device, listener);
            case BT_GPS:
                return new BtGpsDevice(device, listener);
            case BT_CHAT:
                break;
            case UNKNOWN:
				break;
		}
        return null;
	}

	@Override
	public String getAddress() {
		return bluetoothDevice.getAddress();
	}


}