package com.plusot.sensez.config;

/**
 * Created by peet on 21-04-16.
 */
public class SoapConfig {
    public static final byte NEW_REFILL_COMMAND = (byte) 0x80;
    public static final byte OLD_REFILL_COMMAND = (byte) 0x40;
    public static final byte CALIBRATE_COMMAND  = (byte) 0x20;

}
