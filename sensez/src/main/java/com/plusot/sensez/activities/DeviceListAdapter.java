package com.plusot.sensez.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.plusot.sensez.R;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.sensez.preferences.PreferenceKey;
import com.plusot.util.dialog.Alerts;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.Tuple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DeviceListAdapter extends ArrayAdapter<ScannedDevice> {
    //    private final Context context;
    private final int resource;
    private final LayoutInflater inflater;
    //    private final ListView listView;
    private List<Holder> holders = new ArrayList<>();
    private Collection<ScannedDevice> scannedDevices;
    private final Listener listener;
    private Map<String, Boolean> ledMap = new HashMap<>();
    private final EnumSet<DeviceType> types;
    private final Object listLock = new Object();

    public interface Listener {
        void onIconClick(ScannedDevice device);
    }

    DeviceListAdapter(final Context context, EnumSet<DeviceType> types, Listener listener) {
        super(context, R.layout.adapter_devicelist); //getResultsReverse());
        this.types = types;
        synchronized (listLock) {
            addAll(scannedDevices = getAll());
        }
        this.resource = R.layout.adapter_devicelist;
        this.listener = listener;
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private Collection<ScannedDevice> getAll()  {
        if (types == null || types.isEmpty()) {
            return Man.getScannedDevices(true);
        }
        List<ScannedDevice> list = new ArrayList<>();
        for (ScannedDevice device: Man.getScannedDevices(true)) if (types.contains(device.getDeviceType())){
            list.add(device);
        }
        return list;
    }

    void dataSetChanged() {
        synchronized (listLock) {
            Collection<ScannedDevice> newlyScanned = getAll(); //Man.getScannedDevices(whiteList, true);
            if (newlyScanned.containsAll(scannedDevices) && scannedDevices.containsAll(newlyScanned)) return;
            clear();
            addAll(scannedDevices = newlyScanned);
        }
    }

    void drawUpdate(ScannedDevice device) {
        if (device != null) for (Holder holder: holders) if (holder.device == device) {
            //holder.deviceName.setText(device.getName());
            Tuple<String, String> t = device.toNameValueString();
            if (t != null) {
                holder.deviceDetailNames.setText(t.t1());
                holder.deviceDetailValues.setText(t.t2());
            }
            if (device.getScanCount() > 0 && device.getRssi() > Integer.MIN_VALUE)
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss") + " " + device.getRssi() + "dB ("+ Format.format(device.getScanCount(), 1) + "x)");
            else if (device.getScanCount() > 0)
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss") + " ("+ Format.format(device.getScanCount(), 1) + "x)");
            else
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss"));
            if (holder.ledOff.getVisibility() == View.VISIBLE) {
                holder.ledOff.setVisibility(View.GONE);
                holder.ledOn.setVisibility(View.VISIBLE);
                ledMap.put(device.getAddress(), true);

            } else {
                holder.ledOn.setVisibility(View.GONE);
                holder.ledOff.setVisibility(View.VISIBLE);
                ledMap.put(device.getAddress(), false);
            }
        }
    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        Holder holder;
        ScannedDevice device;
        synchronized (listLock) {
            device = getItem(position);
        }
        if (view == null) {
            view = inflater.inflate(resource, parent, false);
            holder = new Holder(view);
            view.setTag(holder);
            holders.add(holder);
            //view.setOnTouchListener(holder.listViewSwipeDetector);
        } else {
            holder = (Holder) view.getTag();
        }
        if (device != null) {
            //holder.eventResult = result;
            String name = StringUtil.proper(device.getName());
            holder.deviceName.setText(name);
            holder.deviceAddress.setText(device.getAddress());
            if (device.getAddress().equalsIgnoreCase(device.getName()))
                holder.deviceAddress.setVisibility(View.GONE);
            else
                holder.deviceAddress.setVisibility(View.VISIBLE);

            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            if (device.getDeviceType().toString().equalsIgnoreCase(device.getName()) || device.getName().toLowerCase().contains(device.getDeviceType().toString().toLowerCase()))
                holder.deviceType.setVisibility(View.GONE);
            else
                holder.deviceType.setVisibility(View.VISIBLE);

            holder.deviceType.setText(device.getDeviceType().toString());
            if (device.isSelected()) {
                //noinspection deprecation
                view.setBackgroundColor(getContext().getResources().getColor(R.color.backgroundHighlight));
                holder.addDevice.setTag(R.id.maySwipe, 0);
                holder.removeDevice.setTag(R.id.maySwipe, 1);
            } else {
                //noinspection deprecation
                view.setBackgroundColor(getContext().getResources().getColor(android.R.color.background_light));
                holder.addDevice.setTag(R.id.maySwipe, 1);
                holder.removeDevice.setTag(R.id.maySwipe, 0);
            }

            Tuple<String, String> t = device.toNameValueString();
            if (t != null) {
                holder.deviceDetailNames.setText(t.t1());
                holder.deviceDetailValues.setText(t.t2());
            }
            if (device.getScanCount() > 0 && device.getRssi() > Integer.MIN_VALUE)
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss") + " " + device.getRssi() + "dB ("+ Format.format(device.getScanCount(),1) + "x)");
            else if (device.getScanCount() > 0)
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss") + " ("+ Format.format(device.getScanCount(), 1) + "x)");
            else
                holder.deviceTime.setText(TimeUtil.formatTime(device.getTimeRead(), "HH:mm:ss"));

            holder.setDevice(device);
            if (ledMap.get(device.getAddress()) == null) {
                holder.ledOn.setVisibility(View.GONE);
                holder.ledOff.setVisibility(View.GONE);
            }

            if (device.isExpanded()) {
                holder.deviceDetail.setVisibility(View.VISIBLE);
            } else {
                holder.deviceDetail.setVisibility(View.GONE);
            }
            if (device.isSelected()) {
                holder.removeDevice.setVisibility(View.VISIBLE);
                holder.addDevice.setVisibility(View.GONE);
            } else {
                holder.addDevice.setVisibility(View.VISIBLE);
                holder.removeDevice.setVisibility(View.GONE);
            }
            holder.iconView.setImageResource(device.getIconId());
        }
        return view;
    }

    private class Holder {
        final TextView deviceName;
        final TextView deviceAddress;
        final View deviceDetail;
        final TextView deviceDetailNames;
        final TextView deviceDetailValues;
        final TextView deviceTime;
        final TextView deviceType;
        //final ListViewSwipeDetector2 listViewSwipeDetector;
        final ImageView addDevice;
        final ImageView removeDevice;
        final ImageView iconView;
        final ImageView ledOff;
        final ImageView ledOn;
        ScannedDevice device;

        Holder(View view) {
            this.deviceName =view.findViewById(R.id.deviceName);
            this.deviceAddress = view.findViewById(R.id.deviceAddress);
            this.deviceDetail = view.findViewById(R.id.deviceDetail);
            this.deviceDetailNames = view.findViewById(R.id.deviceDetailNames);
            this.deviceDetailValues = view.findViewById(R.id.deviceDetailValues);
            this.deviceTime = view.findViewById(R.id.deviceTime);
            this.deviceType = view.findViewById(R.id.deviceType);
            this.addDevice = view.findViewById(R.id.addDevice);
            this.removeDevice = view.findViewById(R.id.removeDevice);
            this.iconView = view.findViewById(R.id.deviceTypeIcon);
            this.ledOff = view.findViewById(R.id.led_off);
            this.ledOn = view.findViewById(R.id.led_on);

            iconView.setOnClickListener(v -> listener.onIconClick(device));
            removeDevice.setOnClickListener(v -> {
                addDevice.setVisibility(View.VISIBLE);
                removeDevice.setVisibility(View.GONE);
                if (device != null) device.setSelected(false);

                //dataSetChanged();
                SleepAndWake.runInMain(DeviceListAdapter.this::notifyDataSetChanged, "DeviceListAdapter.removeDevice.onClick");

            });
            addDevice.setOnClickListener(v -> {
                addDevice.setVisibility(View.GONE);
                removeDevice.setVisibility(View.VISIBLE);
                if (device != null) device.setSelected(true);

                if (PreferenceKey.EXPLAIN_PARAMS.isTrue()) {
                    Alerts.showYesNoDialog(getContext(), R.string.paramWarningTitle, R.string.paramWarningMsg, R.string.ok, R.string.showNoMore, clickResult -> {
                        switch (clickResult) {
                            case YES:
                                break;
                            case NEUTRAL:
                                break;
                            case CANCEL:
                                break;
                            case NO:
                                PreferenceKey.EXPLAIN_PARAMS.set(false);
                                break;
                        }
                    }, -1);
                }
                //dataSetChanged();
                SleepAndWake.runInMain(DeviceListAdapter.this::notifyDataSetChanged, "DeviceListAdapter.addDevice.onClick");
            });

        }

        void setDevice(ScannedDevice device) {
            this.device = device;
        }
    }

}
