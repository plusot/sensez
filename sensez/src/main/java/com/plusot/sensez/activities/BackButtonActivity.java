package com.plusot.sensez.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.plusot.sensez.R;

public abstract class BackButtonActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        View back = findViewById(R.id.back);
        if (back != null) back.setOnClickListener((View view) -> finish());
        TextView text = findViewById(R.id.captionText);
        if (text != null) text.setText(getCaption());
    }

    public abstract String getCaption();

}
