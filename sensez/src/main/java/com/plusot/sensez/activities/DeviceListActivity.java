package com.plusot.sensez.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

import com.plusot.sensez.R;
import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.data.Data;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;

import java.util.EnumSet;

public class DeviceListActivity extends BackButtonActivity implements Man.Listener {
    private DeviceListAdapter adapter = null;
    public static final String EXTRA_DEVICEADDRESS = "EXTRA_DEVICEADDRESS";
    public static final String EXTRA_DEVICENAME = "EXTRA_DEVICENAME";
    public static final String EXTRA_DEVICETYPES = "EXTRA_DEVICETYPES";
    private static EnumSet<DeviceType> deviceTypes = null;
    private static String action = null; //"plusot.intent.action";

    public static void setAction(String action) {
        DeviceListActivity.action = action;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            deviceTypes = DeviceType.fromStrings(intent.getStringArrayExtra(EXTRA_DEVICETYPES));
        }

        setContentView(R.layout.activity_devicelist);

        ListView listView = (findViewById(R.id.deviceListView));
        adapter = new DeviceListAdapter(this, deviceTypes,  device -> {
            try {
                if (device == null || action == null) return;
                Intent intent1 = new Intent(action + ".DEVICEICONCLICK");
                intent1.putExtra(EXTRA_DEVICEADDRESS, device.getAddress());
                intent1.putExtra(EXTRA_DEVICENAME, device.getName());
                startActivity(intent1);
            } catch (ActivityNotFoundException e) {
                LLog.i("Activity not found exception");
            }
        });
        listView.setAdapter(adapter);
//        listView.setOnItemClickListener((parent, view, position, id) -> {
////                adapter.notifyDataSetChanged();
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //@Override
    protected void adapt() {
        if (adapter == null) return;
        adapter.dataSetChanged();
    }


    protected void adapt(final ScannedDevice sd) {
        if (adapter == null) return;
        adapter.drawUpdate(sd);
    }

    @Override
    protected void onResume() {
        Man.addListener(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        Man.removeListener(this);
        super.onPause();
    }

    @Override
    public void onDeviceData(final ScannedDevice device, final Data data, final boolean isNew) {
        SleepAndWake.runInMain(() -> {
            //if (isNew) adapt() else
            adapt(device);
        }, "DeviceListActivity.onDeviceData");
    }


    @Override
    public void onDeviceScanned(final ScannedDevice device, final boolean isNew) {
        SleepAndWake.runInMain(() -> {
            if (isNew) {
                adapt();
            } else {
                adapt(device);
            }
        }, "DeviceListActivity.onDeviceScanned");
    }

    @Override
    public void onDeviceState(ScannedDevice device, Device.StateInfo state) {

    }

    @Override
    public void onScanning(ScanManager.ScanType scanType, boolean on) {

    }

    @Override
    public String getCaption() {
        return getString(R.string.sensorChoice);
    }
}
