package com.plusot.sensez.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import com.plusot.sensez.R
import com.plusot.sensez.SensezLib
import com.plusot.util.logging.LLog

class NullActivity : Activity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_null)

        val permissions = intent?.getStringArrayExtra(SensezLib.PERMISSIONS)
        if (permissions != null) {
            ActivityCompat.requestPermissions(this, permissions, SensezLib.PERMISSIONS_REQUEST)
        }
        if (intent?.getBooleanExtra(SensezLib.MANAGE_OVERLAY_PERMISSION_REQUEST_CODE, false) == true) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + packageName))
            startActivityForResult(intent, SensezLib.ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE)
        }

        if (intent?.getBooleanExtra(SensezLib.MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE, false) == true) {
            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + packageName))
            startActivityForResult(intent, SensezLib.ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE)
            LLog.e("No permission to write settings yet")
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        SensezLib.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        SensezLib.onActivityResult(this, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
        finish()
    }

}
