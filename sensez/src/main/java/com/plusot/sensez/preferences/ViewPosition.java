package com.plusot.sensez.preferences;

import com.plusot.sensez.R;

/**
 * Created by peet on 20/12/16.
 */

public enum ViewPosition {
    NONE(R.string.no_view),
    NORTH(R.string.north),
    NORTH_EAST(R.string.north_east),
    EAST(R.string.east),
    SOUTH_EAST(R.string.south_east),
    SOUTH(R.string.south),
    SOUTH_WEST(R.string.south_west),
    WEST(R.string.west),
    NORTH_WEST(R.string.north_west),
    CENTER(R.string.center),
    ;

    public final int id;

    ViewPosition(final int id) {
        this.id = id;
    }

    public int getViewNr() {
        return ordinal();
    }

    public static ViewPosition fromViewNr(int i) {
        return ViewPosition.values()[i % ViewPosition.values().length];
    }
}