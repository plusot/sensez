package com.plusot.sensez.preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.inputmethod.EditorInfo;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractPreferencesFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
//	private static final String CLASSTAG = AbstractPreferencesFragment.class.getSimpleName();
	private final int preferenceXML;
	private static Map<String, String> summaries = new HashMap<>();

	public AbstractPreferencesFragment(final int preferenceXML) {
		this.preferenceXML = preferenceXML;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(preferenceXML);
		//PreferenceManager.setDefaultValues(Globals.appContext, preferenceXML, false);

		initSummaries();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);    
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		updateSummary(findPreference(key));
	}
	
	private void initSummaries() {
		for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++){
			initSummary(getPreferenceScreen().getPreference(i));
		}
	}

	private void initSummary(Preference pref){
		if (pref instanceof PreferenceCategory){
			PreferenceCategory pCat = (PreferenceCategory)pref;
			for (int i=0; i < pCat.getPreferenceCount(); i++){
				initSummary(pCat.getPreference(i));
			}
		}else{
			updateSummary(pref);
		}
	}

	protected void updateSummary(Preference pref){
		//LLog.i(Common".updatePrefSummary");
		String edit = null;

		if (pref instanceof ListPreference) {
			ListPreference listPref = (ListPreference) pref;
			edit = "" + listPref.getEntry();
		} else if (pref instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) pref; 
			if ((editTextPref.getEditText().getInputType() & EditorInfo.TYPE_TEXT_VARIATION_PASSWORD) == 0) {
				edit = editTextPref.getEditText().getText().toString();
				if (edit.equals("")) edit = editTextPref.getText();
			}
		} else if (pref instanceof MultiSelectListPreference) {
			MultiSelectListPreference multiPref = (MultiSelectListPreference) pref; 
			edit = "\n   *" + StringUtil.toString(multiPref.getValues(), "\n   *");
		}
//		} else if (pref instanceof TimeWheelPreference) {
//			SharedPreferences sharedPreferences = pref.getSharedPreferences();
//			edit = TimeUtil.formatTime(sharedPreferences.getInt(pref.getKey(), 0), "m:ss");
//		}
		if (edit == null) return;
		String sum = pref.getSummary().toString();
		if (sum == null) return;
		if (summaries.get(pref.getKey()) == null) {
			summaries.put(pref.getKey(), sum);
		}
		sum = summaries.get(pref.getKey());
		if (sum == null) return;
		if (sum.matches(".*\\[.*\\].*")) 
			sum = sum.replaceAll("\\[.*\\]", edit); 
		else
			sum += ": " + edit; 
		pref.setSummary(sum);

	}

	protected void setIntent(String key, final Class<?> activityClass) {

		Preference myPref = findPreference(key);

		if (myPref == null) {
			LLog.e(".setIntent: Could not find key: " + key);
		} else myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Context appContext = Globals.getAppContext();
				if (appContext != null) startActivity(new Intent(appContext, activityClass));
				return true;
			}

		});
	}

	protected void removePreferenceScreen(String owner, String key) {
		PreferenceScreen myPref = (PreferenceScreen) findPreference(owner);

		if (myPref == null) {
			LLog.e(".removePreferenceScreen: Could not find key: " + owner);
		} else {
			PreferenceScreen sub = (PreferenceScreen) findPreference(key);
			if (sub != null) myPref.removePreference(sub);
		}
	}

	protected void disablePreferenceScreen(String owner, String key) {
		PreferenceScreen myPref = (PreferenceScreen) findPreference(owner);

		if (myPref == null) {
			LLog.e(".disablePreferenceScreen: Could not find key: " + owner);
		} else {
			PreferenceScreen sub = (PreferenceScreen) findPreference(key);
			sub.setEnabled(false);
		}
	}
}
