package com.plusot.sensez.preferences;

import com.plusot.util.data.Gender;
import com.plusot.util.preferences.PreferenceKeyElements;
import com.plusot.util.util.Format;


public enum PreferenceKey {
    FIRST_USE("first_use", true, Boolean.class),
    USE_DEFAULT_FIELDS("first_use_default_fields", true, Boolean.class),
    AGE("age", 56, Integer.class),
    BODY_LENGTH("length", 186, Integer.class),
    GENDER("gender", 0, Integer.class),
    LAST_LOCATION_LONG("last_location_preference_lng", 5.830994f, Double.class),
    LAST_LOCATION_LAT("last_location_preference_lat", 51.917168f, Double.class),
    REFERENCE_TIME("reference_time", System.currentTimeMillis(), Long.class),
    WHEEL_CIRCUMFERENCE("wheel_circumference", 2.07, Double.class),
    ANTPLUS_INSTALL_CANCELED("ant_plus_install_canceled", 0, Integer.class),
//    SESSION("session", null, String.class),
    TOTAL_DISTANCE("total_distance", 0, Double.class),
    TOTAL_TIME("total_time", 0, Long.class),
    USE_ANT("use_ant", true, Boolean.class),
    USE_BT("use_bt", true, Boolean.class),
    USE_BLE("use_ble", true, Boolean.class),
    USE_INTERNAL("use_internal", true, Boolean.class),
    EXPLAIN_MENU("explain_menu", true, Boolean.class),
    EXPLAIN_DATA("explain_data", true, Boolean.class),
    EXPLAIN_PARAMS("explain_params", true, Boolean.class),
    ALTITUDE_OFFSET("altitude_offset", "0", String.class),

    ;

    private final PreferenceKeyElements elements;
    private static double altitudeOffset = Double.NaN;


    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass) {
        this(label, defaultValue, valueClass, 0);
    }

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass, final long flags) {
        elements = new PreferenceKeyElements (label, defaultValue, valueClass, flags);
    }

    public boolean isTrue() { return elements.getBoolean(); }
    public double getDouble() { return elements.getDouble(); }
    public int getInt() { return elements.getInt(); }
    public long getLong() { return elements.getLong(); }
    public String getString() { return elements.getString(); }
    public boolean set(int value) { return elements.set(value); }
    public void set(double value) { elements.set(value); }
    public void set(long value) { elements.set(value); }
    public void set(String value) { elements.set(value); }
    public void set(boolean value) { elements.set(value); }

    public static Gender getGender() {
        return Gender.fromOrdinal(GENDER.getInt());
    }

    public static void setGender(Gender gender) {
        GENDER.set(gender.ordinal());
    }

    public static PreferenceKey fromString(final String label) {
        for (PreferenceKey key : PreferenceKey.values()) {
            if (key.elements.getLabel().equals(label)) return key;
        }
        return null;
    }

    public static double getAltitudeOffset() {
        if (Double.isNaN(altitudeOffset)) try {
            altitudeOffset = Double.parseDouble(PreferenceKey.ALTITUDE_OFFSET.getString());
        } catch (NumberFormatException e) {
            altitudeOffset = 0;
            PreferenceKey.ALTITUDE_OFFSET.set("0.0");
        }
        return altitudeOffset;
    }

    public static void setAltitudeOffset(double value) {
        PreferenceKey.ALTITUDE_OFFSET.set(Format.format(value, 0));
        altitudeOffset = value;
    }

}