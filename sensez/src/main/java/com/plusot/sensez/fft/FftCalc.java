package com.plusot.sensez.fft;

import com.plusot.util.util.Format;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Tuple;
import java.util.Arrays;

/**
 * Created by peet on 26/5/16.
 */
public class FftCalc {
    private final boolean DEBUG = true;
    private final int size;
    private final int power;
    private Complex[] WNs = null;

    public static class FFTException extends Exception {

        public FFTException(String s) {
            super(s);
        }
    }

    static double log(int base, double value) {
        return Math.log(value) / Math.log(base);
    }

    public FftCalc() throws FFTException {
        this(1024);
    }

    /**
     * size has to be power of 2!!!
     */
    public FftCalc(int size) throws FFTException {
        this.size = size;
        double power = log(2, size);
        if (power - (int) power > 0) throw new FFTException("Illegal FFT size: " + size + "!!!");
        this.power = (int) power;
        WNs = new Complex[size / 2];
    }


    public static class Complex extends Tuple<Double, Double> {

        public Complex(Double o, Double o2) {
            super(o, o2);
        }

        public Complex multiply(Complex other) {
            return new Complex(
                    t1() * other.t1() - t2() * other.t2(),
                    t1() * other.t2() + t2() * other.t1()
            );
        }

        public Complex plus(Complex other) {
            return new Complex(
                    t1() + other.t1(),
                    t2() + other.t2()
            );
        }

        public Complex minus(Complex other) {
            return new Complex(
                    t1() - other.t1(),
                    t2() - other.t2()
            );
        }

        @Override
        public String toString() {
            return Format.format(t1(), 3) + ", " + Format.format(t2(), 3) + "i";
        }
    }

    public static String printComplex(String title, Complex[] outputs) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(title).append('\n');
        for (FftCalc.Complex output: outputs) {
            sb.append("   Index " + i + " = " + output).append('\n');
            i++;
        }
        return sb.toString();
    }

    private Complex calcWN(int bin, int N) {
        int masterBin = bin * size / N;
        if (WNs[masterBin] != null) return WNs[masterBin];
        return WNs[masterBin] = new Complex(
                Math.cos(2 * bin * Math.PI / N),
                -Math.sin(2 * bin * Math.PI / N)
        );
    }

    private Tuple<Complex, Complex> calcButterfly(Complex x, Complex y, int k, int N) {
        Complex WN = calcWN(k, N);
        Complex y2 = y.multiply(WN);
        Complex x2 = x.plus(y2);
        y2 = x.minus(y2);
        return new Tuple<>(x2, y2);
    }

    public int reverse(int value) {
        return Integer.reverse(value) >>> (32 - power);
    }

    public Complex[] calc(double[] values) throws FFTException {
        int padCount = size - values.length;
        if (padCount < 0) throw new FFTException("values array too large: " + values.length);
        if (padCount > 0) {
            values = Arrays.copyOf(values, size);
            for (int i = size - padCount; i < size; i++) {
                values[i] = 0.0;
            }
        }
        Complex[] outputs = new Complex[size];

        for (int k = 0; k < size; k++) {
            outputs[k] = new Complex(values[reverse(k)], 0.0);
        }
        int N = 2;
        if (DEBUG) {
            LLog.i(printComplex("Outputs", outputs));
        }

        for (int p = 1; p <= power; p++) {
            int k = 0;
            while (k < size) {
                Tuple<Complex, Complex> result = calcButterfly(outputs[k], outputs[k + N / 2], k % N, N);
                outputs[k] = result.t1();
                outputs[k + N / 2] = result.t2();
                k++;
                if (k % (N / 2) == 0) k += N / 2;
            }
            if (DEBUG) {
                LLog.i("Power = " + p + ", N = " + N);
                LLog.i(printComplex("W", WNs));
                LLog.i(printComplex("Outputs", outputs));
            }
            N *= 2;
        }
        return outputs;
    }


}
