package com.plusot.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.plusot.util.logging.LLog;
import com.plusot.util.logging.SimpleLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.TimeUtil;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Locale;


public class Globals {

    public static final String FILE_DETAIL_DELIM = "_";
    private static final String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String CLASSTAG = Globals.class.getSimpleName();
    public static String TAG = "PlusotApp";
    private static WeakReference<Application> appRef = null;
    public static RunMode runMode = RunMode.RUN;
    public static long FLUSH_TIME = 20000;
    public static final String TAB = "   ";
    public static final int TAG_WIDTH = 80;
    public static final String LOG_SPEC = "syslog";
    public static final String LOG_EXT = ".txt";
    private static final long bootTime = System.currentTimeMillis();
    public static int screenWidth = 640;
    public static int screenHeight = 640;
    private static DisplayMetrics metrics = null;
    private static WeakReference<Activity> lastActiveActivity = null;
    public static boolean verbose = false;

    public static Context getAppContext() {
        if (appRef == null) return null;
        Application app = appRef.get();
        if (app != null) return app.getApplicationContext();
        return null;
    }

    public static String getStorePath() {
        Context context = Globals.getAppContext();
        if (context != null) {
            File file = context.getExternalFilesDir(null);
            if (file != null) return file.getAbsolutePath();
        }
//        SD_PATH + "/" + TAG.toLowerCase(Locale.US) + '/';
        return "";
    }

    public static String getDownloadPath() {
        return SDCARD_PATH + "/Download/";
    }

    public static String getSdcardPath() {
        return SDCARD_PATH;
    }

    public static String getDataPath() {
        return getStorePath() + "/data/";
    }

    public static String getLogPath() {
        return getStorePath() + "/log/";
    }

    public static String getSessionId() {
        return TimeUtil.formatTime(bootTime, "yyMMdd");
    }

    public static DisplayMetrics getMetrics() {
        if (metrics == null) {
            Context appContext = getAppContext();
            if (appContext == null) return null;
            WindowManager manager = (WindowManager) appContext.getSystemService(Context.WINDOW_SERVICE);
            if (manager == null) return null;
            Display display = manager.getDefaultDisplay();
            if (display != null)  {
                if (metrics == null) metrics = new DisplayMetrics();
                display.getMetrics(metrics);
            }
        }
        return metrics;
    }

    private static String boardStr(String value) {
        return ("   *" +
                        "                          ".substring(0, 26 - value.length() / 2) +
                        value +
                        "                             "
        ).substring(0, 56) + "*\n";
    }

    public static void init(Application app, String caller) {
        if (appRef != null) {
//            Log.d(TAG, CLASSTAG + ".init: Application already exists. Called by " + caller);
            Log.i(TAG, "Application already exists. Called by " + caller);
            if (appRef.get() == app) return;
        }
        appRef = new WeakReference<>(app);

        Context appContext = app.getApplicationContext();
        DisplayMetrics metrics = getMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;

        String appName = appContext.getString(appContext.getApplicationInfo().labelRes);
        TAG = appName.replace("'", "").replace(" ", "");
        runMode = RunMode.RUN;

        Log.i(TAG, "Globals.init for " + appName);

        SimpleLog.setSession(TimeUtil.formatFileTime());
        SleepAndWake.runInNewThread(() -> {
            File file = new File(Globals.getStorePath());
            if (!file.isDirectory() && !file.mkdirs()) {
                Log.w(TAG, CLASSTAG + ".init: Could not create: " + Globals.getStorePath());
            }
            file = new File(Globals.getLogPath());
            if (!file.isDirectory() && !file.mkdirs()) {
                Log.w(TAG,CLASSTAG + ".init: Could not create: " + Globals.getLogPath());
            }
            file = new File(Globals.getDataPath());
            if (!file.isDirectory() && !file.mkdirs()) {
                Log.w(TAG,CLASSTAG + ".init: Could not create: " + Globals.getDataPath());
            }
            String name = appName;
            try {
                PackageInfo info = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 0);
                name += " " + info.versionName;
            } catch (NameNotFoundException e) {
                Log.e(TAG, CLASSTAG + ".init: name not found");
            }

            String pidStr = String.valueOf(android.os.Process.myPid());
            LLog.i("\n" +
                    "   ******************************************************\n" +
                    "   *                                                    *\n" +
                    boardStr(name) +
                    "   *                 Application Started                *\n" +
                    boardStr(pidStr) +
                    "   *                                                    *\n" +
                    "   ******************************************************");
        }, "Globals.init");

    }

    public enum RunMode {
        RUN,
        FINISHING,
        FINISHED;

        public boolean isRun() {
            return this == RUN;
        }

        public boolean isFinished() {
            return this == FINISHED;
        }

    }

    public static Activity getVisibleActivity() {
        if (lastActiveActivity == null) return null;
        return lastActiveActivity.get();
    }

    public static void setVisibleActivity(Activity lastActiveActivity) {
        Globals.lastActiveActivity = new WeakReference<>(lastActiveActivity);
    }
}
