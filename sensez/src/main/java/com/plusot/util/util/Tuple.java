package com.plusot.util.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Tuple<T, V> {
    private final T t;
    private final V v;

    public Tuple(T t, V v) {
        this.t = t;
        this.v = v;
    }
    public T getFirst(){
        return t;
    }

    public V getSecond() {
        return v;
    }

    public T t1(){
        return t;
    }

    public V t2() {
        return v;
    }

    public static Tuple<String, Integer> splitInt(String value) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(value);
        int i = 0;
        // Find all matches
        if (matcher.find()) {
            i = Integer.valueOf(matcher.group());
        }

        pattern = Pattern.compile("[a-zA-Z]+");
        matcher = pattern.matcher(value);
        String str = "";
        // Find all matches
        if (matcher.find()) {
            // Get the matching string
            str = matcher.group();
        }
        return new Tuple<>(str, i);

    }

}



