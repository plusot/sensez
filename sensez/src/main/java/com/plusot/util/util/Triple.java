package com.plusot.util.util;

/**
 * Created by peet on 14-04-15.
 */
public class Triple<T, U, V> {
    private final T t;
    private final U u;
    private final V v;

    public Triple(T t, U u, V v) {
        this.t = t;
        this.u = u;
        this.v = v;
    }

    public T getFirst(){
        return t;
    }

    public U getSecond() {
        return u;
    }

    public V getThird() {
        return v;
    }

    public T t1(){
        return t;
    }

    public U t2() {
        return u;
    }

    public V t3() {
        return v;
    }

}