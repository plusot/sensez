package com.plusot.util.util;

import android.app.ActivityManager;
import android.content.Context;
import android.util.SparseArray;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.Timing;

import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class Watchdog extends Thread {
    private static final boolean DEBUG = false;
    private static final long FORCE_STOP = 60000;
    private static final long NOPROCESS_STOP = 2000;
    private static final Set<Listener> listeners = new HashSet<>();
    private static final Object listenerLock = new Object();
    private static final Vector<TopListener> topListeners = new Vector<>();
    private static Watchdog instance = null;
    private static long iCount = 0;
    private static ProcessInfo processInfo = new ProcessInfo();
    private boolean mayRun = true;
    private boolean stopInProcess = false;
    private SparseArray<String> processes = new SparseArray<>();
    private int idSeed = 0;
    private long stopTime = 0;
    private BeforeKill beforeKill = null;
    private long topChecked = System.currentTimeMillis();
    private int pid = -1;
    private Stringer topStringer = null;


    private Watchdog() {
        this.setName("Watchdog_" + getId());

        //mayRun = true;
        start();
    }

    public static Watchdog getInstance() {
        if (instance != null) return instance;
        if (!Globals.runMode.isRun()) return null;
        synchronized (Watchdog.class) {
            if (instance == null) instance = new Watchdog();
        }
        return instance;
    }

    public static void stopInstance() {
        if (instance != null) synchronized (Watchdog.class) {
            if (instance != null) {
                Globals.runMode = Globals.RunMode.FINISHED;
                if (!instance.stopInProcess) instance.goStopping();
                //instance.finish();
            }
            //instance = null;
        }
    }

    public static int addProcess(String processName) {
        if (getInstance() == null) return 0;
        return instance._addProcess(processName + "." + Thread.currentThread().getName() + "." + Thread.currentThread().getId());
    }

    public static void removeProcess(int id, final String caller) {
        if (getInstance() == null) return;
        instance._removeProcess(id, caller);
    }

    private void _add(Watchable watchable, long intervalInMilliSeconds) {
        synchronized (listenerLock) {
            for (Listener listener : listeners)
                if (listener.watchable == watchable) {
                    listener.interval = intervalInMilliSeconds / 10;
                    return;
                }
            listeners.add(new Listener(watchable, intervalInMilliSeconds / 10));
        }
    }

    private void _add(TopListener listener) {
        synchronized (listenerLock) {
            topListeners.add(listener);
        }
    }

    public static void addWatchable(Watchable watchable, long intervalInMilliSeconds) {
        Watchdog instance = getInstance();
        if (instance != null) {
            instance._add(watchable,intervalInMilliSeconds);
        }
    }

    public static void addTopListener(TopListener listener) {
        Watchdog instance = getInstance();
        if (instance != null) {
            instance._add(listener);
        }
    }

    public void _remove(TopListener listener) {
        synchronized (listenerLock) {
            topListeners.remove(listener);
        }
    }

    public static void remove(TopListener listener) {
        Watchdog instance = getInstance();
        if (instance != null) {
            instance._remove(listener);
        }
    }

    public static void remove(Watchable watchable) {
        Watchdog instance = getInstance();
        if (instance != null) {
            instance._remove(watchable);
        }
    }

    public void _remove(Watchable watchable) {
        synchronized (listenerLock) {
            Listener removable = null;
            for (Listener listener : listeners) {
                if (listener.watchable == watchable) {
                    removable = listener;
                    break;
                }
            }
            if (removable != null) listeners.remove(removable);
        }
    }

    private void checkTop() {
        if (System.currentTimeMillis() - topChecked < Timing.LOG_INTERVAL) return;
        final Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        topChecked = System.currentTimeMillis();
        topStringer = new Stringer("\n");
        String[] cmds = {"top -d 1 -n 1 -m 8"};
        try {
            topStringer.append("Top");
            new ShellExec().exec(cmds, -1, new ShellExec.ShellCallback() {

                @Override
                public void shellOut(String shellLine) {
                    topStringer.append(Globals.TAB + shellLine);
                    String appPackage = appContext.getPackageName();
                    if (shellLine.endsWith(appPackage)) {
                        String[] parts = shellLine.trim().split("\\s+");
                        if (parts.length > 8) {
                            processInfo.pid = parts[0];
                            if (parts[4].endsWith("%")) parts[4] = parts[4].substring(0, parts[4].length() - 1);
                            try {
                                processInfo.cpu = Integer.parseInt(parts[4]);
                            } catch (NumberFormatException e) {
                                LLog.i("Could not convert " + parts[4] + " to cpu usage (" + shellLine +")");
                            }
                            try {
                                processInfo.threads = Integer.parseInt(parts[6]);
                            } catch (NumberFormatException e) {
                                LLog.i("Could not convert " + parts[6] + " to number of threads (" + shellLine + ")");
                            }
                            processInfo.vss = parts[7];
                            processInfo.rss = parts[8];
                        }
                    } else if (shellLine.contains("Idle") && shellLine.startsWith("User")) {
                        String[] parts = shellLine.split(" ");
                        int idle = Integer.MIN_VALUE;
                        for (int i = 0; i < parts.length; i++)
                            if (parts[i].length() > 0) {
                                if (parts[i].equals("Idle")) try {
                                    idle = Integer.parseInt(parts[i + 1]);
                                } catch (NumberFormatException e) {
                                    LLog.i("Could not convert " + parts[i+ 1] + " to idle");

                                }
                                else if (parts[i].equals("=")) try {
                                    processInfo.total = Integer.parseInt(parts[i + 1]);
                                    if (idle != Integer.MIN_VALUE && processInfo.total != 0)
                                        processInfo.idle = 100 * idle / processInfo.total;
                                } catch (NumberFormatException e) {
                                    LLog.i("Could not convert " + parts[i + 1] + " to total");

                                }
                            }

                    }

                }

                @Override
                public void processComplete(int tag, String commandLine,
                                            int exitValue) {
                    //LLog.i("Idle = " + processInfo.idle + "% App = " + processInfo.cpu + "% VSS = " + processInfo.vss + " RSS = " + processInfo.rss + " Threads = " + processInfo.threads);
                    LLog.i(topStringer.toString());
                    callTopListeners();


                }
            });
        } catch (IOException e) {
            LLog.i("IOException in calling 'top'", e);
        } catch (InterruptedException e) {
            LLog.i("Interrupted");
        }
    }

    @Override
    public void run() {
        pid = android.os.Process.myPid();

        while (mayRun) {
            try {
                sleep(10);
                if (stopInProcess)
                    checkStop();
                else {
                    callWatchdogTimerListeners();
                    if (topListeners.size() > 0) checkTop();
                }
            } catch (InterruptedException e) {
                LLog.i("Interrupted");
            }
        }
        instance = null;

        if (Globals.runMode == Globals.RunMode.RUN) {
            LLog.i("Watchdog stopped for pid = " + pid + ", but process not killed, as it seems to be restarted");
        } else SleepAndWake.runInMain (1000L, () -> {
            Runtime.getRuntime().exit(0);
        });
    }

    private void finish() {
        LLog.i("Finish called");
        synchronized (listenerLock) {
            listeners.clear();
        }
        synchronized (listenerLock) {
            topListeners.clear();
        }
        mayRun = false;
        interrupt();
    }

    private void checkStop() {
        if (Globals.runMode == Globals.RunMode.RUN) {
            LLog.i("Reseting stopInProcess");
            stopInProcess = false;
            return;
        }

        synchronized (listenerLock) {
            Set<Listener> tempListeners = new HashSet<>();
            tempListeners.addAll(listeners);
            for (Listener listener : tempListeners) listener.watchable.onWatchdogClose();
            listeners.clear();
        }
        if (processes.size() == 0) {
            if (beforeKill != null) {
                stopTime = System.currentTimeMillis();
                LLog.i("checkStop: Before kill executing");
                beforeKill.onBeforeKill();
                beforeKill = null;
            }
            if (System.currentTimeMillis() - stopTime > NOPROCESS_STOP) {
                finish();
            }
        } else if (System.currentTimeMillis() - stopTime > FORCE_STOP) {
            for (int i = 0; i < processes.size(); i++) {
                LLog.i("checkStop: process " + i + " = " + processes.valueAt(i) + " still running.");
            }
            LLog.i("checkStop: Forcing application kill");
            finish();
        }
    }

    private int _addProcess(String processName) {
        synchronized (this) {
            LLog.i("_addProcess: process " + idSeed + " = " + processName + "." + idSeed);
            processes.append(idSeed, processName + "." + idSeed);
            return idSeed++;
        }
    }

    private void goStopping() {
        LLog.i("Stopping in progress");
        stopInProcess = true;
        stopTime = System.currentTimeMillis();
        for (int i = 0; i < processes.size(); i++) {
            LLog.i("Process " + i + " = " + processes.valueAt(i));
        }
    }

    private void _removeProcess(int id, final String caller) {
        synchronized (this) {
            LLog.i("_removeProcess: process " + id + " = " + processes.get(id) + " by " + caller);

            processes.remove(id);
        }
    }

    public interface Watchable {
        void onWatchdogTimer(long count);
        void onWatchdogClose();
    }

    public interface TopListener {
        void onTopUpdate(ProcessInfo info);
    }

    interface BeforeKill {
        void onBeforeKill();
    }

    public static class ProcessInfo {
        int idle = 0;
        int cpu = 0;
        int total = 0;
        int threads = 0;
        String pid = "?";
        String vss = "?";
        String rss = "?";

        @Override
        public String toString() {
            return "cpu: " + cpu + "%" +
                    ", idle: " + idle + "%" +
//                    ", total: " + total +
                    ", threads: " + threads +
                    ", pid: " + pid +
                    ", vss: " + vss +
                    ", rss: " + rss;
        }

    }

    public static ProcessInfo getProcessInfo() {
        return processInfo;
    }

    private class Listener {
        private final Watchable watchable;
        private long interval = 10;

        private Listener(final Watchable watchable, final long interval) {
            this.watchable = watchable;
            this.interval = interval;
        }
    }

    public static boolean isRunning() {
        Watchdog watchdog;
        if (!Globals.runMode.isRun()) return false;
        if ((watchdog = Watchdog.getInstance()) == null) return false;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;

        ActivityManager manager = (ActivityManager) appContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = manager.getRunningAppProcesses();
        if (list != null) for (ActivityManager.RunningAppProcessInfo info : list) {
            if (info.pid == watchdog.pid) {
//                LLog.i("App " + info.processName +  " " + watchdog.pid + " is running");
                return true;
            }
        }
        return false;
    }

    private void callTopListeners() {
//        SleepAndWake.runInThread(new Runnable() {
//            @Override
//            public void run() {
        synchronized (listenerLock) {
            for (TopListener listener : topListeners) {
                listener.onTopUpdate(processInfo);
            }
        }
//            }
//        }, 100);
    }

    private void callWatchdogTimerListeners() {
        iCount++;
        synchronized (listenerLock) {
            try {
                Set<Listener> tempListeners = new HashSet<>();
                tempListeners.addAll(listeners);
                for (Listener listener : tempListeners) {
                    if (iCount % listener.interval == 0) {
//                        final Listener l = listener;
//                        SleepAndWake.runInMain(new Runnable() {
//                            @Override
//                            public void run() {
//                                l.watchable.onWatchdogCheck(iCount);
//                            }
//                        }, "Watchdog.callWatchdogTimerListeners");
                        listener.watchable.onWatchdogTimer(iCount);
                    }
                }
            } catch (ConcurrentModificationException e) {
                LLog.e("Concurrent access to listeners", e);
            }
        }
    }
}

