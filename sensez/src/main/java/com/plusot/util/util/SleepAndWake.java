package com.plusot.util.util;

import android.os.Handler;
import android.os.Looper;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SleepAndWake {
    private final static boolean DEBUG = false;
    private static int KEEP_ALIVE_TIME = 10;
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static int MAX_THREADS = 1000;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();
    private static final Object usageLock = new Object();
    private static Map<String, EnumMap<ThreadType, Integer>> usage = new HashMap<>();

    private static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            NUMBER_OF_CORES,       // Initial pool size
            MAX_THREADS,       // Max pool size
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            workQueue);

    private enum ThreadType {
        MAIN,
        IN_THREAD,
        NEW_THREAD,
        POOL_THREAD,
        ;
    }

    public static class Stopper {
        private Handler handler;
        private final Runnable runnable;
        private final Object lock = new Object();

        Stopper(Handler handler, Runnable runnable) {
            this.handler = handler;
            this.runnable = runnable;
        }

        public boolean stop() {
            if (handler != null) synchronized (lock) {
                if (handler != null && runnable != null) handler.removeCallbacks(runnable);
                handler = null;
                return true;
            }
            return false;
        }

        public boolean execAndStop() {
            if (handler != null) synchronized (lock) {
                if (handler != null && runnable != null) {
                    handler.removeCallbacks(runnable);
                    handler.post(runnable);
                }
                handler = null;
                return true;
            }
            return false;
        }
    }

    private static void incCount(String caller, ThreadType type) {
        EnumMap<ThreadType, Integer> map;
        synchronized (usageLock) {
            if ((map = usage.get(caller)) == null)
                usage.put(caller, map = new EnumMap<>(ThreadType.class));
            Integer count;
            if ((count = map.get(type)) == null)
                map.put(type, 1);
            else
                map.put(ThreadType.MAIN, count + 1);
        }
    }

    public static Stopper runInMain(final Runnable runnable, final long delayInMillis, String caller) {
        incCount(caller, ThreadType.MAIN);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(runnable, delayInMillis);
        return new Stopper(handler, runnable);
    }

    public static Stopper runInMain(final Runnable runnable, final long delayInMillis, Class caller) {
        incCount(caller.getSimpleName(), ThreadType.MAIN);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(runnable, delayInMillis);
        return new Stopper(handler, runnable);
    }

    public static Stopper runInMain(final Runnable runnable, final long delayInMillis) {
        incCount("Anonymous", ThreadType.MAIN);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(runnable, delayInMillis);
        return new Stopper(handler, runnable);
    }


    public static Stopper runInMain(final long delayInMillis, final Runnable runnable) {
        incCount("Anonymous", ThreadType.MAIN);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(runnable, delayInMillis);
        return new Stopper(handler, runnable);
    }

    public static void runInMain(final Runnable runnable, String caller) {
        incCount(caller, ThreadType.MAIN);
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void runInMain(final Runnable runnable) {
        incCount("Anonymous", ThreadType.MAIN);
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void runInMain(final Runnable runnable, Class caller) {
        incCount(caller.getSimpleName(), ThreadType.MAIN);
        new Handler(Looper.getMainLooper()).post(runnable);
    }

//    public static void runInThread(final Runnable runnable, final long delayInMillis, String caller) {
//        incCount(caller, ThreadType.IN_THREAD);
//        if (Looper.myLooper() == null) {
//            Looper.prepare();
//            new Handler().postDelayed(runnable, delayInMillis);
//            Looper.loop();
//
//        } else
//            new Handler().postDelayed(runnable, delayInMillis);
//    }

    public static void runInNewThread(final Runnable runnable) {
        try {
            incCount("Anonymous", ThreadType.POOL_THREAD);
            threadPool.execute(runnable);
        } catch (Exception e) {
            LLog.e("Exception while creating thread", e);
        }
    }

    public static void runInNewThread(final Runnable runnable, String caller) {
        try {
            incCount(caller, ThreadType.POOL_THREAD);
            threadPool.execute(runnable);
        } catch (Exception e) {
            LLog.e("Exception while creating thread", e);
        }
    }

    public static void runInNewThread(final Runnable runnable, Class caller) {
        runInNewThread(runnable, caller.getSimpleName());
    }

    public static void logPoolSize() {
//        LLog.i("Number of cores = " + NUMBER_OF_CORES + ", number of threads in pool = " + threadPool.getPoolSize());
        Stringer stringer = new Stringer("\n");
        Set<String> callers;
        synchronized (usageLock) {
            callers = usage.keySet();
        }
        for (String caller : callers) {
            synchronized (usageLock) {
                EnumMap<ThreadType, Integer> map = usage.get(caller);
                if (map != null) for (ThreadType type : map.keySet()) {
                    Integer count = map.get(type);
                    stringer.append(Globals.TAB + Format.format(caller, Globals.TAG_WIDTH) + Format.format(type.toString(), 15) + " " + count);
                }
            }
        }
        if (DEBUG) LLog.i("SleepAndWakeUsage:\n" + stringer.toString());
    }

}
