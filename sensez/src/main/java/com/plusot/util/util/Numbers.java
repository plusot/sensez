package com.plusot.util.util;

import com.plusot.util.logging.LLog;

/**
 * Created by peet on 11-11-15.
 */
public class Numbers {

    public static int parseInt(String intStr) {
        char[] chars = intStr.toCharArray();
        int sign = 1;
        int result = 0;
        for (char ch : chars) {
            if (ch == '-')
                sign = -1;
            else {
                result *= 10;
                result += (ch - 48);
            }
        }
        return sign * result;
    }

    public static double parseDouble(String intStr) {
        char[] chars = intStr.toCharArray();
        boolean countDecimals = false;
        double sign = 1.0;
        double result = 0;
        int dec = 1;
        for (char ch : chars) {
            if (ch == '-')
                sign = -1.0;
            else if (ch == '.' || ch == ',')
                countDecimals = true;
            else {
                if (countDecimals) dec *= 10;
                result *= 10;
                result += (ch - 48);
            }
        }
        return sign * result / dec;
    }

    public static int getBits(int value, int pos, int bits) {
        int result = (0xffff & value) >> pos;
        switch (bits) {
            case 1: return result & 0x1;
            case 2: return result & 0x3;
            case 3: return result & 0x7;
            case 4: return result & 0xF;
            case 5: return result & 0x1F;
            case 6: return result & 0x3F;
            case 7: return result & 0x7F;
            default: return result;
        }
    }




    public static int pow(int base, int power) {
        if (power == 0) return 1;
        return base * pow(base, power - 1);
    }

    public static int unsigned2Signed(int unsigned, int size) {
        if ((unsigned & (1 << size-1)) != 0) {
            unsigned = -1 * ((1 << size-1) - (unsigned & ((1 << size-1) - 1)));
        }
        return unsigned;
    }

    public static boolean checkBit(int value, int bit) {
        return (value & bit) == bit;
    }


    public static Object cloneObject(Object value) {
        if (value == null) return null;
        if (value instanceof double[]) {
            double[] array =  new double[((double[]) value).length];
            System.arraycopy(value, 0, array, 0, array.length);
            return array;
        }
        if (value instanceof long[]) {
            long[] array =  new long[((long[]) value).length];
            System.arraycopy(value, 0, array, 0, array.length);
            return array;
        }
        if (value instanceof float[]) {
            float[] array =  new float[((float[]) value).length];
            System.arraycopy(value, 0, array, 0, array.length);
            return array;
        }
        if (value instanceof int[]) {
            int[] array =  new int[((int[]) value).length];
            System.arraycopy(value, 0, array, 0, array.length);
            return array;
        }
        if (value instanceof String[]) {
            String[] array =  new String[((String[]) value).length];
            System.arraycopy(value, 0, array, 0, array.length);
            return array;
        }
        if (value instanceof Double) return Double.valueOf((Double)value);
        if (value instanceof Long) return Long.valueOf((Long) value);
        if (value instanceof Float) return Float.valueOf((Float) value);
        if (value instanceof Integer) return Integer.valueOf((Integer) value);
        if (value instanceof String) return new String ((String) value);
        return null;
    }

    public static boolean inRange(Object value, double minValue, double maxValue) {
        double[] doubles = getDoubleValues(value, true);
        if (doubles == null) return false;
        for (double aDouble : doubles) {
            if (aDouble > maxValue || aDouble < minValue) return false;
        }
        return true;
    }

    public static boolean greaterThan(Object value, double minValue) {
        double[] doubles = getDoubleValues(value, true);
        if (doubles == null) return false;
        for (double aDouble : doubles) {
            if (aDouble > minValue) return true;
        }
        return false;
    }

    public static double max(double[] value) {
        float max = 0;
        if (value != null) {
            for (double aValue : value) {
                max += Math.max(max, aValue);
            }
        }
        return max;
    }

    public static double average(double[] value) {
        double avg = 0;
        if (value != null) {
            for (double aValue : value) {
                avg += aValue;
            }
            avg /= value.length;
        }
        return avg;
    }

    public static float average(float[] value) {
        float avg = 0;
        if (value != null) {
            for (float aValue : value) {
                avg += aValue;
            }
            avg /= value.length;
        }
        return avg;
    }

    public static double getDoubleValue(final Object value) {
        if (value instanceof double[]) {
            return average((double[])value);
        }
        if (value instanceof float[]) {
            return average((float[])value);
        }
        if (value instanceof Double) return (Double) value;
        if (value instanceof Long) return ((Long) value).doubleValue();
        if (value instanceof Float) return ((Float) value).doubleValue();
        if (value instanceof Integer) return ((Integer) value).doubleValue();
        if (value instanceof String) try {
            return Double.valueOf((String) value);
        } catch (NumberFormatException e) {
            LLog.e("NumberFormatException " + e.getMessage());

        }
        return 0;
    }

    public static double[] getDoubleValues(final Object value, final boolean acceptSingleValue) {
        if (value instanceof double[]) {
            double result[] = new double[((double[]) value).length];
            System.arraycopy(value, 0, result, 0, result.length);
            return result;
        }
        if (value instanceof float[]) {
            double[] result = new double[((float[]) value).length];
            int i = 0;
            for (float f : (float[]) value) {
                result[i++] = f;
            }
            return result;
        }
        if (acceptSingleValue) {
            return new double[] {getDoubleValue(value)};
        }
        return null;
    }


    public static int getUInt8(byte[] arr, int i) {
        if (i >= arr.length) return 0;
        return (0xff & arr[i]);
    }

    public static int getUInt16(byte[] arr, int i) {
        if (i >= arr.length - 1) return 0;
        return (0xff & arr[i]) + ((0xff & arr[i + 1]) << 8);
    }

    public static long getUInt32(byte[] arr, int i) {
        if (i >= arr.length - 3) return 0;
        return (0xff & arr[i]) + ((0xff & arr[i + 1]) << 8) + ((0xff & arr[i + 2]) << 16) + ((0xff & arr[i + 3]) << 24);
    }

    public static int getUInt24(byte[] arr, int i) {
        if (i >= arr.length - 2) return 0;
        return (0xff & arr[i]) + ((0xff & arr[i + 1]) << 8) + ((0xff & arr[i + 2]) << 16);
    }

//    public static int getUInt24(byte[] c, int offset) {
//        int lowerByte = (int) c[offset] & 0xFF;
//        int mediumByte = (int) c[offset+1] & 0xFF;
//        int upperByte = (int) c[offset + 2] & 0xFF;
//        return (upperByte << 16) + (mediumByte << 8) + lowerByte;
//    }

    public static int getSInt16(byte[] arr, int i) {
        if (i >= arr.length) return 0;
        return Numbers.unsigned2Signed(getUInt16(arr, i), 16);
    }

    public static int getByte(byte[] arr, int i) {
        if (i >= arr.length) return 0;
        return (0xff & arr[i]);
    }

    public static double diffRatio(double value) {
        return Math.abs(1.0 - value);
    }


    public static boolean equalBytes(byte[] array1, byte[] array2) {
        if (array1 == null || array2 == null) return false;
        if (array1.length != array2.length) return false;
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) return false;
        }
        return true;
    }


}
