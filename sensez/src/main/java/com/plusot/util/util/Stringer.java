package com.plusot.util.util;

/**
 * Created by peet on 12-09-15.
 */
public class Stringer {
    private StringBuilder sb = new StringBuilder();
    private final String delim;
    private final String header;
    private final String footer;
    private int added = 0;

    public Stringer(String delim, String header, String footer) {
        this.delim = delim;
        this.footer = footer;
        this.header = header;
    }

    public Stringer(String delim) {
        this(delim, "", "");
    }

    public Stringer append(String value) {
        if (added++ > 0) sb.append(delim);
        if (value == null) return this;
        sb.append(value);
        return this;
    }

    public Stringer append(int value) {
        if (added++ > 0) sb.append(delim);
        sb.append(value);
        return this;
    }

    public Stringer appendNoDelim(String value) {
        if (value == null) return this;
        sb.append(value);
        return this;
    }

    public Stringer appendNoDelim(int value) {
        sb.append(value);
        return this;
    }

    public Stringer append(String[] values) {
        for (String value : values) append(value);
        return this;
    }

    @Override
    public String toString() {
        return header + sb.toString() + footer;
    }
}
