package com.plusot.util.util;


import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ShellExec {

    public ShellExec() {
        //context = _context;
        //fileBinDir = context.getDir("bin",0);
    }

    public int exec(String[] cmds, int tag, ShellCallback sc) throws IOException, InterruptedException {
        if (!Globals.runMode.isRun()) return -1;
        int exitVal = -1;
        try {

            ProcessBuilder pb = new ProcessBuilder(cmds);
            //pb.directory(fileBinDir);

            StringBuilder cmdlog = new StringBuilder();

            for (String cmd : cmds) {
                if (cmdlog.length() > 0) cmdlog.append(' ');
                cmdlog.append(cmd);
            }

            //LLog.v(Common"" + cmdlog.toString());

            //	pb.redirectErrorStream(true);
            Process process = pb.start();

            // any error message?
            StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream(), "ERROR", sc);

            // any output?
            StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream(), "OUTPUT", sc);

            // kick them off
            errorGobbler.start();
            outputGobbler.start();

            exitVal = process.waitFor();

            sc.processComplete(tag, cmdlog.toString(), exitVal);
        } catch (Exception e) {
            LLog.e("Could not execute " + StringUtil.toString(cmds) + ": " + e.getMessage());
        }

        return exitVal;
    }

    public interface ShellCallback {
        void shellOut(String shellLine);

        void processComplete(int tag, String commandLine, int exitValue);
    }

    class StreamGobbler extends Thread {
        InputStream is;
        String type;
        ShellCallback sc;

        StreamGobbler(InputStream is, String type, ShellCallback sc) {
            this.is = is;
            this.type = type;
            this.sc = sc;
            setName("StreamGobler" + getId());
        }

        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null)
                    if (sc != null)
                        sc.shellOut(line);

            } catch (IOException ioe) {
                LLog.e("error reading shell slog", ioe);
            }
        }
    }
}
