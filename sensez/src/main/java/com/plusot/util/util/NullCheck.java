package com.plusot.util.util;

/**
 * Created by peet on 28/9/16.
 */

public class NullCheck<T> {
    public interface Listener<T> {
        void onChecked(T t);
    }

    public NullCheck(T t, Listener listener) {
        if (t != null) listener.onChecked(t);
    }

}
