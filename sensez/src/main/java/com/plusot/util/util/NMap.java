package com.plusot.util.util;

import java.util.*;

/**
 * Created by peet on 17/7/16.
 */
public class NMap <T> {
    private Map<String, Map> map = new HashMap<>();
    private final int n;

    public NMap(int n) {
        this.n = n;
    }

    public static int parseInt(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException n) {
            return defaultValue;
        }
    }

    public static Integer parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException n) {
            return null;
        }
    }

    public static double parseDouble(String value, double defaultValue) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException n) {
            return defaultValue;
        }
    }

    public static Double parseDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException n) {
            return null;
        }
    }

    public void put(T value, String ... keys) {
        if (keys.length < n) return;
        Map<String, Map> m = map;
        Map<String, T> valueMap = null;
        int i = 0;

        for (String key: keys) {
            i++;
            if (i == n) {
                if (valueMap != null) valueMap.put(key, value);
            } else if (i == n - 1) {
                valueMap = m.get(key);
                if (valueMap == null) m.put(key, valueMap = new HashMap<>());
            } else {
                Map tempM = m.get(key);
                if (tempM == null) m.put(key, tempM = new HashMap<>());
                m = tempM;
            }
        }
    }

    private String getKeyMember(Map m, String key) {
        List<String> members = new ArrayList<>(m.keySet());
        Collections.sort(members, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                Integer int1, int2;
                if ((int1 = parseInt(lhs)) != null && (int2 = parseInt(rhs)) != null) {
                    return int1 - int2;
                }
                return lhs.compareTo(rhs);
            }
        });
        String keyMember = null;
        for (String member : members) {
            Integer iMem = parseInt(member);
            Integer iKey = parseInt(key);
            if (iMem != null && iKey != null) {
                if (iKey.equals(iMem)) {
                    return member;
                } else if (iKey < iMem) {
                    if (keyMember == null) return member;
                    return keyMember;
                }
            } else {
                if (key.equals(member)) {
                    return member;
                } else if (key.compareTo(member) < 0) {
                    if (keyMember == null) return member;
                    return keyMember;
                }
            }
            keyMember = member;
        }
        return keyMember;
    }

    public T get(boolean interpoll, String ... keys) {
        if (keys.length < n) return null;
        Map<String, Map> m = map;
        Map<String, T> valueMap = null;
        int i = 0;

        for (String key: keys) {
            i++;
            if (i == n) {
                if (valueMap != null) {
                    T t = valueMap.get(key);
                    if (t == null) {
                        if (interpoll) {
                            if (valueMap.size() == 0) return null;
                            if (valueMap.size() == 1)
                                return valueMap.get(valueMap.keySet().toArray(new String[1])[0]);
                            else {
                                return valueMap.get(getKeyMember(valueMap, key));
                            }
                        } else
                            return null;
                    } else
                        return t;
                }
            } else if (i == n - 1) {
                valueMap = m.get(key);
                if (valueMap == null) {
                    if (interpoll) {
                        if (m.size() == 0) return null;
                        if (m.size() == 1)
                            valueMap =  m.get(m.keySet().toArray(new String[1])[0]);
                        else {
                            valueMap = m.get(getKeyMember(m, key));
                        }
                    } else
                        return null;
                }
            } else {
                Map<String, Map> tempM = m.get(key);
                if (tempM == null) {
                    if (interpoll) {
                        if (m.size() == 0) return null;
                        if (m.size() == 1)
                            m =  m.get(m.keySet().toArray(new String[1])[0]);
                        else {
                            m = m.get(getKeyMember(m, key));
                        }
                    } else
                        return null;
                } else {
                    m = tempM;
                }
            }
        }
        return null;
    }
}
