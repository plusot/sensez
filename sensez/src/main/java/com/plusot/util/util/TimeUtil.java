package com.plusot.util.util;

import android.content.Context;

import com.plusot.util.Globals;
import com.plusot.sensez.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtil {
    //private static final String CLASSTAG = TimeUtil.class.getSimpleName();
    private static final long HOUR = 3600000;
    private static final long DAY = 86400000L;
    public static final long YEAR = 365 * 86400000L;
    private static final long MIN_TIME = getTime(2012, 7, 1, 0, 0, 0);

    @SuppressWarnings("unused")
    public static int getTimeInt() {
        Calendar cal = Calendar.getInstance();
//        String timezoneID = TimeZone.getDefault().getID();
//        cal.setTimeZone(TimeZone.getTimeZone(timezoneID));
        return cal.get(Calendar.YEAR) % 2000 * 100000000 +
                (cal.get(Calendar.MONTH) + 1) * 1000000 +
                cal.get(Calendar.DAY_OF_MONTH) * 10000 +
                cal.get(Calendar.HOUR_OF_DAY) * 100 +
                cal.get(Calendar.MINUTE);
        //cal.get(Calendar.SECOND);
    }

    @SuppressWarnings("unused")
    public static int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
//        String timezoneID = TimeZone.getDefault().getID();
//        cal.setTimeZone(TimeZone.getTimeZone(timezoneID));
        return cal.get(Calendar.YEAR);
    }

    public static long getTime(int year, int month, int day, int hour, int minute, int second) {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.set(year, month, day, hour, minute, second);
        return cal.getTime().getTime();
    }

    @SuppressWarnings("unused")
    public static int toSeconds(long time) {
        return (int)((time - MIN_TIME) / 1000);
    }

    @SuppressWarnings("unused")
    public static int toComparable(long time, long resolution) {
        return (int)((time - MIN_TIME) / resolution);
    }

    @SuppressWarnings("unused")
    public static long fromSeconds(int time) {
        return 1000L * time + MIN_TIME;
    }

    public static int getOffset() {
        return TimeZone.getDefault().getOffset(System.currentTimeMillis());
    }

    public static String formatTime(String pattern) {
        return formatTime(System.currentTimeMillis(), pattern);
    }

    public static String formatTime(long value, String pattern) {
        final SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
        //longDateFormat.setTimeZone(UTC_TIMEZONE);
        return format.format(new Date(value));
    }

    public static String formatTimeUTC(long value, String pattern) {
        final SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(new Date(value));
    }

    @SuppressWarnings("unused")
    public static String formatTimeUTC(long value) {
        return formatTimeUTC(value, "yyyy-MM-dd HH:mm:ss");
    }

    //	public static String formatTimeNL(long value) {
    //		final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //		format.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdams"));
    //		return format.format(new Date(value));
    //	}

    public static String formatTime(long value) {
        return formatTime(value, "yyyy-MM-dd HH:mm:ss");
    }

    public static String formatTime() {
        return formatTime(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
    }

    @SuppressWarnings("unused")
    public static String formatTimeT(long time) {
        return formatTime(time, "yyyy-MM-dd HH:mm:ss").replace(' ', 'T');
    }

    public static String formatFileTime() {
        return formatTime(System.currentTimeMillis(), "yyyyMMdd-HHmmss");
    }

    @SuppressWarnings("unused")
    public static String formatTimeShort(long value) {
        String dateStr = formatDate(value);
        long now = System.currentTimeMillis();
        if (!dateStr.equals(formatDate(now))) return dateStr;
        return formatTime(value, "HH:mm:ss");
    }

    private static String formatDate(long value) {
        return formatTime(value, "yyyy-MM-dd");
    }

    @SuppressWarnings("unused")
    static String formatDateShort(long value) {
        return formatTime(value, "yyyyMMdd");
    }

    public static String formatMilli() {
        return formatMilli(-1);
    }

    private static String formatMilli(long value) {
        if (value == -1) value = System.currentTimeMillis();
        return formatTime(value, "yyyy-MM-dd HH:mm:ss") + "." + String.format(Locale.US, "%03d", value % 1000);
    }

    @SuppressWarnings("unused")
    public static String formatMilliTime(long value) {
        return formatTime(value, "HH:mm:ss") + "." + String.format(Locale.US, "%03d", value % 1000);
    }

    @SuppressWarnings("unused")
    public static String formatMilli(long value, String pattern) {
        return formatTime(value, pattern) + "." + String.format(Locale.US, "%03d", value % 1000);
    }

    public static String formatMilli(long value, int decimals) {
        return TimeUtil.formatMilli(value, "yyyy-MM-dd HH:mm:ss", decimals);
    }

    @SuppressWarnings("unused")
    public static String formatMilliTime(long value, int decimals) {
        return TimeUtil.formatMilli(value, "HH:mm:ss", decimals);
    }

    private static String formatMilli(long value, String pattern, int decimals) {
        switch (decimals) {
            case 0:
                return formatTime(value, pattern);
            case 1:
                return formatTime(value, pattern) + "." + String.format(Locale.US, "%01d", (value % 1000) / 100);
            case 2:
                return formatTime(value, pattern) + "." + String.format(Locale.US, "%02d", (value % 1000) / 10);
            default:
                return formatTime(value, pattern) + "." + String.format(Locale.US, "%03d", value % 1000);
        }
    }

    private static long parseTime(String value, String pattern) {
        final SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
        Date date;
        try {
            date = format.parse(value);
        } catch (ParseException e) {
            return 0;
        }
        //longDateFormat.setTimeZone(UTC_TIMEZONE);
        return date.getTime();
    }

    public static long parseTimeUTC(String value, String pattern) {
        int milli = 0;
        if (pattern.contains(".S")) {
            String[] valueParts = value.split("\\.");
            String[] patternParts = pattern.split("\\.");
            for (int i = 0; i < Math.min(patternParts.length, valueParts.length); i++) {
                if (patternParts[i].startsWith("S")) {
                    String milliPart = (valueParts[i] + "000").substring(0, 3);
                    try {
                        milli = Integer.parseInt(milliPart);
                    } catch (NumberFormatException e) {
                        milli = 0;
                    }
                    switch (patternParts[i].length()) {
                        case 3:
                            pattern = pattern.replace(".SSS", "");
                            break;
                        case 2:
                            pattern = pattern.replace(".SS", "");
                            break;
                        default:
                        case 1:
                            pattern = pattern.replace(".S", "");
                            break;
                    }

                }
            }
        }
        final SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date;
        try {
            date = format.parse(value);
        } catch (ParseException e) {
            return 0;
        }
        return date.getTime() + milli;
    }

    @SuppressWarnings("unused")
    public static long parseTimeUTC(String value) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date;
        try {
            date = format.parse(value);
        } catch (ParseException e) {
            return 0;
        }
        return date.getTime();
    }

    @SuppressWarnings("unused")
    public static long parseTime(String value) {
        return parseTime(value, "yyyy-MM-dd HH:mm:ss");
    }

    @SuppressWarnings("unused")
    public static long timeToUTC(long time) {
        //		String sTime = formatTime(time);
        //		return parseTimeToZone(sTime, "UTC");
        return time + getOffset();
    }

    @SuppressWarnings("unused")
    public static long timeToLocal(long time) {
        return time - getOffset();
    }

    public static String formatElapsedMilli(long value, boolean countDays) {
        Context appContext =Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days);
        int hours = (int) (value / HOUR);
        if (hours > 99)
            return hours + " " + appContext.getString(R.string.hour);
        value %= HOUR;
        int min = (int) (value / 60000);
        value %= 60000;
        int seconds = (int) (value / 1000);
        value %= 1000;
        if (hours == 0 && min == 0 && seconds == 0) return String.format(Locale.US, ".%d", value);
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d.%03d", seconds, value);
        if (hours == 0) return String.format(Locale.US, "%d:%02d.%03d", min, seconds, value);
        return String.format(Locale.US, "%d:%02d:%02d.%03d", hours, min, seconds, value);
    }

    public static String formatElapsedMilliSpoken(long value, boolean countDays) {
        String result = formatElapsedHourSecondsSpoken(value, countDays);
        Context appContext = Globals.getAppContext();

        if (value < DAY && appContext != null) {
            value %= 1000;
            result +=  " " + String.format(Locale.US, "%d", value) + appContext.getString(R.string.ms_voice);
        }
        return result;
    }

    public static String formatElapsedTenths(long value, boolean countDays) {
        Context appContext =Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days);
        int hours = (int) (value / HOUR);
        if (hours > 99)
            return hours + " " + appContext.getString(R.string.hour);

        value %= HOUR;
        int min = (int) (value / 60000);
        value %= 60000;
        int seconds = (int) (value / 1000);
        value %= 1000;
        value /= 100;
        if (hours == 0 && min == 0 && seconds == 0) return String.format(Locale.US, ".%d", value);
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d.%01d", seconds, value);
        if (hours == 0) return String.format(Locale.US, "%d:%02d.%01d", min, seconds, value);
        return String.format(Locale.US, "%d:%02d:%02d.%01d", hours, min, seconds, value);
    }

    public static String formatElapsedTenthsSpoken(long value, boolean countDays) {
        String result = formatElapsedHourSecondsSpoken(value, countDays);
        Context appContext = Globals.getAppContext();

        if (value < DAY && appContext != null) {
            value %= 1000;
            value /= 100;
            result += " " + String.format(Locale.US, "%d", value) + appContext.getString(R.string.tenths_voice);
        }
        return result;
    }

    public static String formatElapsedSeconds(long value, boolean countDays) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days);
        int hours = (int) (value / HOUR);
        if (hours > 99)
            return hours + " " + appContext.getString(R.string.hour);
        value %= HOUR;
        int min = (int) (value / 60000);
        value %= 60000;
        int seconds = (int) (value / 1000);
        //value %= 1000;
        if (hours == 0 && min == 0 && seconds == 0) return "0";
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d", seconds);
        if (hours == 0) return String.format(Locale.US, "%d:%02d", min, seconds);
        return String.format(Locale.US, "%d:%02d:%02d", hours, min, seconds);
    }

    public static String formatElapsedSecondsSpoken(long value, boolean countDays) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days) + " " + (value % DAY) / HOUR + " " + appContext.getString(R.string.hours_voice);;
        int hours = (int) (value / HOUR);
        if (hours > 99) return hours + " " + appContext.getString(R.string.hours_voice);
        value %= HOUR;
        int min = (int) (value / 60000);
        value %= 60000;
        int seconds = (int) (value / 1000);
        //value %= 1000;
        if (hours == 0 && min == 0 && seconds == 0) return "0 " +
                appContext.getString(R.string.sec_voice);
        if (hours == 0 && min == 0) return
                String.format(Locale.US, "%d", seconds) + " " +
                        appContext.getString(R.string.sec_voice);
        if (hours == 0) return
                String.format(Locale.US, "%d", min) + "  " +
                        appContext.getString(R.string.minutes_voice) +
                        String.format(Locale.US, "%d", seconds) + " " +
                        appContext.getString(R.string.sec_voice);
        if (hours == 1) return
                String.format(Locale.US, "%d", hours) + "  " +
                        appContext.getString(R.string.hour_voice) +
                        String.format(Locale.US, "%d", min) + "  " +
                        appContext.getString(R.string.minutes_voice) +
                        String.format(Locale.US, "%d", seconds) + " " +
                        appContext.getString(R.string.sec_voice);
        return String.format(Locale.US, "%d", hours) + "  " +
                appContext.getString(R.string.hours_voice) +
                String.format(Locale.US, "%d", min) + "  " +
                appContext.getString(R.string.minutes_voice) +
                String.format(Locale.US, "%d", seconds) + " " +
                appContext.getString(R.string.sec_voice);
    }

    public static String formatElapsedHourSeconds(long value, boolean countDays) {
        Context appContext =Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days);
        int hours = (int) (value / HOUR);
        if (hours >= 1) return hours + " " + appContext.getString(R.string.hour);
        return formatElapsedSeconds(value, countDays);
    }

    public static String formatElapsedHourSecondsSpoken(long value, boolean countDays) {
        Context appContext =Globals.getAppContext();
        if (appContext == null) return "";

        if (countDays && value > DAY) return value / DAY + " " + appContext.getString(R.string.days) + " " + (value % DAY) / HOUR + " " + appContext.getString(R.string.hours_voice);
        int hours = (int) (value / HOUR);
        if (hours == 1) return hours + " " + appContext.getString(R.string.hour_voice);
        if (hours > 1) return hours + " " + appContext.getString(R.string.hours_voice);
        return formatElapsedSecondsSpoken(value, countDays);
    }

    public static String fileNameTime() {
        return TimeUtil.formatTime(System.currentTimeMillis(), "yyyyMMdd-HHmmss");
    }

    public static String fileNameDate() {
        return TimeUtil.formatTime(System.currentTimeMillis(), "yyyyMMdd");
    }



}
