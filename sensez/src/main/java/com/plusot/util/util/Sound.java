package com.plusot.util.util;

import android.content.Context;
import android.media.MediaPlayer;


public class Sound {

    public static void play(Context context, int rawFile) {
        MediaPlayer mp = MediaPlayer.create(context, rawFile);
        final MediaPlayer mpRelease = mp;
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                mpRelease.release();
            }

        });
        mp.start();
    }

}
