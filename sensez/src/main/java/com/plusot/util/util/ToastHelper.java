package com.plusot.util.util;

import android.content.Context;
import android.widget.Toast;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;


public class ToastHelper {
    public static void showToastLong(String value) {
        //if (toast != null) toast.cancel();
        Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        Toast toast = Toast.makeText(
                appContext,
                value,
                Toast.LENGTH_LONG);
        LLog.i(value);

        toast.show();
    }

    public static void showToastLong(String value, int gravity) {
        //if (toast != null) toast.cancel();
        Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        Toast toast = Toast.makeText(
                appContext,
                value,
                Toast.LENGTH_LONG);
        LLog.i(value);

        toast.setGravity(gravity, 0,0);
        toast.show();
    }

    public static void showToastLong(int value) {
        //if (toast != null) toast.cancel();
        Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        Toast toast = Toast.makeText(
                appContext,
                appContext.getString(value),
                Toast.LENGTH_LONG);
        LLog.i(appContext.getString(value));
        toast.show();
    }

    public static void showToastLongSave(final int value) {
        //if (toast != null) toast.cancel();
        final Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        SleepAndWake.runInMain(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(
                        appContext,
                        appContext.getString(value),
                        Toast.LENGTH_LONG);
                LLog.i(appContext.getString(value));
                toast.show();
            }
        }, "ToastHelper.showToastLongSave");
    }

    private static long lastShown = 0L;

    public static void showToastShort(String value) {
        //if (toast != null) toast.cancel();
        if (System.currentTimeMillis() - lastShown < 500) return;
        lastShown = System.currentTimeMillis();
        Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        Toast toast = Toast.makeText(
                appContext,
                value,
                Toast.LENGTH_SHORT);
        LLog.i(value);
        toast.show();
    }

    public static void showToastShort(int value, int gravity, int xOffset, int yOffset) {
        //if (toast != null) toast.cancel();
        Context appContext =Globals.getAppContext();
        if (appContext == null) return;

        Toast toast = Toast.makeText(
                appContext,
                appContext.getString(value),
                Toast.LENGTH_SHORT);
        if (gravity != -1) toast.setGravity(gravity, xOffset, yOffset);
        LLog.i(appContext.getString(value));
        toast.show();
    }

    public static void showToastShort(int value) {
        showToastShort(value, -1, 0, 0);
    }

    public static void showToastShort(int value, int gravity) {
        showToastShort(value, gravity, 0, 0);
    }

}
