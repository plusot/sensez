package com.plusot.util.util;

import com.plusot.util.logging.LLog;

import java.util.Locale;

public class Format {

    public static String format(double value, int decimals, Locale locale) {
        if (decimals < 0 || decimals > 10) return format(value);
        String format = "%1." + decimals + "f";
//        LLog.d("Format " + value + ", " + decimals);
        return String.format(locale, format, value).trim();
    }

    public static String format(double value, int decimals) {
        return format(value, decimals, Locale.US);
    }

    public static String format(double value, int pre, int decimals) {
        if (decimals < 0 || decimals > 10) return format(value);
        String format = "%" + (pre + 1 + decimals) + "." + decimals + "f";
        return String.format(Locale.US, format, value);
    }

    public static String format(int value, int pre) {
        String format = "%" + pre  + "d";
        return String.format(Locale.US, format, value);
    }

    public static String format(double value) {
        int decimals;
        double power = Math.log10(Math.abs(value));
        if (power > 1.5)
            decimals = 0;
        else
            decimals = 2 - (int) Math.round(power);

        String format = "%1." + decimals + "f";
        return String.format(Locale.US, format, value);
    }

    public static String format(String string, int length) {
        String format = "%" + length + "s";
        return (string + String.format(format, " ")).substring(0,length);
    }


}
