package com.plusot.util.util;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.preferences.PreferenceKey;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtil {
    private static final int BUFFER = 2048;


    public static boolean concat(String start, String end, File infile, File outfile) {

        FileChannel inChan = null, outChan = null;
        PrintWriter fw = null;
        try {
            fw = new PrintWriter(new BufferedWriter(new FileWriter(outfile, false)));
            fw.write(start);
            fw.flush();
        } catch (FileNotFoundException e) {
            LLog.e("concatFiles, start: File Not Found", e);
            return false;
        } catch (IOException e) {
            LLog.e("concatFiles, start: IO Exception", e);
            return false;
        } finally {
            if (fw != null) fw.close();
        }

        FileOutputStream outStream = null;
        RandomAccessFile inStream = null;
        try {
            // create the channels appropriate for appending:
            outStream = new FileOutputStream(outfile, true);
            outChan = outStream.getChannel();
            inStream = new RandomAccessFile(infile, "r");
            inChan = inStream.getChannel();

            long startSize = outfile.length();
            long inFileSize = infile.length();
            long bytesWritten = 0;

            //set the position where we should start appending the data:
            outChan.position(startSize);
            long startByte = outChan.position();

            while (bytesWritten < inFileSize) {
                bytesWritten += outChan.transferFrom(inChan, startByte, inFileSize);
                startByte = bytesWritten + 1;
            }


        } catch (FileNotFoundException e) {
            LLog.e("concatFiles, work: File Not Found", e);
            return false;
        } catch (IOException e) {
            LLog.e("concatFiles, work: IO Exception", e);
            return false;
        } finally {
            try {
                if (inChan != null) inStream.close();
                if (outChan != null) outStream.close();
            } catch (IOException e) {
                LLog.i("Exception closing streams", e);
            }
        }
        try {
            fw = new PrintWriter(new BufferedWriter(new FileWriter(outfile, true)));
            fw.write(end);
            fw.flush();
            fw.close();
            return true;
        } catch (FileNotFoundException e) {
            LLog.e("concatFiles, end: File Not Found", e);
            return false;
        } catch (IOException e) {
            LLog.e("concatFiles, end: IO Exception", e);
            return false;
        } finally {
            fw.close();
        }
    }

    public static boolean toGZip(String start, String end, File infile, File outfile) {
        GZIPOutputStream outStream = null;
        RandomAccessFile inStream = null;
        if (!infile.exists()) return false;
        boolean result = true;
        try {
            // create the channels appropriate for appending:
            outStream = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(outfile)));
            if (start != null) outStream.write(start.getBytes());
            inStream = new RandomAccessFile(infile, "r");

            byte[] bytes = new byte[BUFFER];
            int bytesRead;

            while ((bytesRead = inStream.read(bytes)) != -1) {
                outStream.write(bytes, 0, bytesRead);
            }
            if (end != null) outStream.write(end.getBytes());
        } catch (FileNotFoundException e) {
            LLog.e("concatFilesToGZip, work: File Not Found", e);
            return false;
        } catch (IOException e) {
            LLog.e("concatFilesToGZip, work: IO Exception", e);
            return false;
        } finally {
            try {
                if (inStream != null) inStream.close();
                if (outStream != null) outStream.close();
            } catch (IOException e) {
                LLog.e("concatFilesToGZip, work: IO Exception while closing files", e);
                result = false;
            }
        }
        return result;
    }

//	public static boolean toGZip(File infile, File outfile) {
//		return toGZip(null, null, infile, outfile);
//	}


    public static void toZip(String[] files, String zipFile) {
        try {
            BufferedInputStream origin;
            FileOutputStream dest = new FileOutputStream(zipFile);

            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

            byte data[] = new byte[BUFFER];

            for (String file : files) {
                LLog.v("toZip adding: " + file);
                FileInputStream fi = new FileInputStream(file);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(file.substring(file.lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                    Thread.yield();
                }
                origin.close();
                Thread.sleep(5);
            }

            out.close();
        } catch (Exception e) {
            LLog.e("toZip exception: " + e.getMessage());
        }
    }

    private static void systemLogToFile(int pid, String[] cmd, String[] endCmd) {
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()), 1024);
            String line = reader.readLine();
            if (line != null) {
                File file = new File(Globals.getLogPath());
                if (!file.isDirectory()) file.mkdirs();
                String fileName = Globals.getLogPath() +
                        Globals.LOG_SPEC +
                        Globals.FILE_DETAIL_DELIM + pid +
                        Globals.FILE_DETAIL_DELIM + TimeUtil.fileNameDate() +
                        Globals.LOG_EXT;
                file = new File(fileName);
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
                PrintWriter os = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                do {
                    os.println(line);
                    Thread.yield();
                } while ((line = reader.readLine()) != null);
                os.close();
            }
            reader.close();
            Runtime.getRuntime().exec(endCmd);
        } catch (IOException e) {
            LLog.e("IOException " + e.getMessage());
        }

    }

    public static void systemLogToFile() {
        int pid = android.os.Process.myPid();
        int previousPid = PreferenceKey.PREVIOUS_PID.getInt();

        if (pid != previousPid && previousPid != 0) systemLogToFile(
                previousPid,
                new String[]{"logcat", "--pid=" + previousPid, "-d"},
                new String[]{"logcat", "--pid=" + previousPid, "-c"}
        );
        PreferenceKey.PREVIOUS_PID.set(pid);
        systemLogToFile(
                pid,
                new String[]{"logcat", "--pid=" + pid, "-d"},
                new String[]{"logcat", "--pid=" + pid, "-c"}
        );
    }

}