package com.plusot.util.util;

public interface Scaler {
    Number scale(Number value);
    Number scaleBack(Number value);
}
