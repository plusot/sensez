package com.plusot.util.util;

/**
 * Created by peet on 3/6/16.
 */
public class Null {
    public static boolean ifNull(Boolean value, boolean defaultValue) {
        if (value == null) return defaultValue;
        return value;
    }

    public static long ifNull(Long value, long defaultValue) {
        if (value == null) return defaultValue;
        return value;
    }

    public static int ifNull(Integer value, int defaultValue) {
        if (value == null) return defaultValue;
        return value;
    }

    public static double ifNull(Double value, double defaultValue) {
        if (value == null) return defaultValue;
        return value;
    }

    public static float ifNull(Float value, float defaultValue) {
        if (value == null) return defaultValue;
        return value;
    }

//    public static <T> T checkNull(T t) {
//        if (t == null) {
//
//        }
//        return t;
//    }
}
