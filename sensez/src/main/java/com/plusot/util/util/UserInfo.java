package com.plusot.util.util;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import androidx.core.content.ContextCompat;

import com.plusot.util.Globals;
import com.plusot.sensez.R;
import com.plusot.util.logging.LLog;
import com.plusot.util.preferences.PreferenceKey;

public class UserInfo {
    private static String deviceId = null;
    private static long deviceNr = -1;

    public static void resetDeviceId() {
        deviceId = null;
        deviceNr = -1;
    }

    public static long getDeviceId() {
        if (deviceId == null) {
            String temp = "";
            Context appContext = Globals.getAppContext();
            if (appContext != null) {
//                if (    Build.VERSION.SDK_INT < Build.VERSION_CODES.Q &&
//                        PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.READ_PHONE_STATE)) {
//                    TelephonyManager telManager = (TelephonyManager) appContext.getSystemService(Context.TELEPHONY_SERVICE);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        deviceId = telManager.getMeid();
//                    } else
//                        deviceId = telManager.getDeviceId();
//                }
                if (deviceId == null) {
                    deviceId = Secure.getString(appContext.getContentResolver(), Secure.ANDROID_ID);
                } else {
                    temp = " " + Secure.getString(appContext.getContentResolver(), Secure.ANDROID_ID);
                }
                if (deviceId == null) {
                    @SuppressLint("WifiManagerLeak") WifiManager manager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo info = manager.getConnectionInfo();
                    deviceId = info.getMacAddress();
                }
            }
            if (deviceId == null) deviceId = PreferenceKey.ID.getString();
            if (deviceId == null) {
                deviceId = "";
                for (int i = 0; i < 12; i++) {
                    int random = (int) Math.floor(35.9 * Math.random());
                    if (random < 10)
                        deviceId += "" + random;
                    else
                        deviceId += (char) ('a' + random);
                }
                LLog.d("Getting a random device id = " + deviceId);
                PreferenceKey.ID.set(deviceId);

            }
            if (deviceId != null) try {
                deviceNr = Math.abs(Long.parseLong(deviceId));
            } catch (NumberFormatException e) {
                deviceNr = 0;
                for (int i = 0; i < Math.min(deviceId.length(), 16); i++) {
                    if (i > 0) deviceNr *= 16;
                    char ch = deviceId.charAt(i);
                    if (ch >= 'a' && ch <= 'z') deviceNr += (ch - 'a') + 10;
                    if (ch >= '0' && ch <= '9') deviceNr += (ch - '0');
                    if (i == 0 && deviceId.length() >= 16) deviceNr = (deviceNr & 0x3);
                }
                deviceNr = Math.abs(deviceNr);
                //}
            }
            //if (Globals.testing.isTest()) ToastHelper.showToastLong("DeviceId  = " + deviceNr);
            LLog.i("DeviceId  = " + deviceId + " (" + deviceNr + ")" + temp);
        }


        return deviceNr;
    }

    public static String appNameVersion() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return Globals.TAG;
        try {
            PackageInfo info = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 0);
            return Globals.TAG + " " + info.versionName; // + "." + info.versionCode;
        } catch (NameNotFoundException e) {
            return Globals.TAG;
        }

    }

    public static String appVersion() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "v 0.1";

        try {
            PackageManager pm = appContext.getPackageManager();
            PackageInfo info = pm.getPackageInfo(appContext.getPackageName(), 0);
            ApplicationInfo ai = pm.getApplicationInfo(appContext.getPackageName(), 0);
            return pm.getApplicationLabel(ai) + " " + info.versionName; // + "." + info.versionCode;
        } catch (NameNotFoundException e) {
            return "v 0.1";
        }

    }

    public static String appVersionShort() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "v 0.1";

        try {
            PackageManager pm = appContext.getPackageManager();
            PackageInfo info = pm.getPackageInfo(appContext.getPackageName(), 0);
            return info.versionName;
        } catch (NameNotFoundException e) {
            return "v 0.1";
        }

    }

    public static int appVersionCode() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return 1;

        try {
            PackageManager pm = appContext.getPackageManager();
            PackageInfo info = pm.getPackageInfo(appContext.getPackageName(), 0);
            return info.versionCode;
        } catch (NameNotFoundException e) {
            return 1;
        }

    }

    public static class UserData {
        public String name;
        public String email;
    }

}
