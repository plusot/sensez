package com.plusot.util.kotlin

/**
 * Package: com.plusot.util.kotlin
 * Project: $sensez
 *
 * Created by Peter Bruinink on 17-04-18.
 * Copyright © 2018 Brons Ten Kate. All rights reserved.
 */

const val MINUTE = 60000
