package com.plusot.util.kotlin

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.text.Selection
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView

/**
 * File: DrawableRadioGroup
 * Package: com.bronstenkate.diabetes24.widget
 * Project: diabetes24
 *
 * Created by Peter Bruinink on 05-01-18.
 * Copyright © 2017 Brons Ten Kate. All rights reserved.
 *
 * <p>
 * Provide a list of view ids for the subsequent radio buttons. The view ids at odd indexes should be
 * for the not selected radio button images, the ones at even indexes for the selected button images.
 * This means: first id is view id of the unselected image of the first radio button, the second the
 * selected one of the same radio button, the third view id is an unselected view id of the second radio
 * button, etc.
 */

enum class ButtonAction {
    CLICK,
    DELETE
}

inline fun <reified T : Activity> Activity.startActivity() = this.startActivity(Intent(this, T::class.java))

inline fun <reified T : Activity> Activity.startActivity(requestCode: Int) = this.startActivityForResult(Intent(this, T::class.java), requestCode)


inline fun <reified T : Activity> Activity.startActivity(func: (Intent) -> Unit) {
    val intent = Intent(this, T::class.java)
    func(intent)
    this.startActivity(intent)
}

inline fun <reified T : Activity> Activity.startActivity(requestCode: Int, func: (Intent) -> Unit) {
    val intent = Intent(this, T::class.java)
    func(intent)
    this.startActivityForResult(intent, requestCode)
}


var View.visible: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) { if (value) this.visibility = View.VISIBLE else this.visibility = View.GONE }

var View.invisible: Boolean
    get() = this.visibility == View.INVISIBLE
    set(value) { if (value) this.visibility = View.INVISIBLE else this.visibility = View.VISIBLE }

var List<View>.visibleIndex: Int
    get() {
        this.forEachIndexed { index, view ->
            if (view.visible) return index
        }
        return -1
    }
    set(value) {
        this.forEachIndexed { index, view ->
            view.visible = (index  == value)
        }
    }

fun <T: View> List<T>.setVisible(first: Boolean)  {
    if (first) {
        this.first().visible = true
        this.forEachIndexed { index, view ->
            if (index > 0) view.visible = false
        }
    } else {
        this.last().visible = true
        this.forEachIndexed { index, view ->
            if (index < this.size - 1) view.visible = false
        }
    }
}

fun <T: View> List<T>.setVisible(value: Int) {
    this.forEachIndexed { index, view ->
        view.visible = (index  == value)
    }
}

fun <T: TextView> List<T>.setBold(value: Int) {
    this.forEachIndexed { index, view ->
        if (index == value)
            view.setTypeface(null, Typeface.BOLD)
        else
            view.setTypeface(null, Typeface.NORMAL)

    }
}

fun <T: View> List<T>.getVisible(): Int {
    this.forEachIndexed { index, view ->
        if (view.visible) return index
    }
    return -1
}

val View.xy: Pair<Int, Int>
    get() {
        val array = IntArray(2)
        this.getLocationInWindow(array)
        return array[0] to array[1]
    }

fun EditText.toDouble(defaultValue: Double = 0.0): Double = try {
    this.text.toString().toDouble()
} catch (e: NumberFormatException) {
    defaultValue
}

fun EditText.toInt(defaultValue: Int = 0): Int = try {
    this.text.toString().toInt()
} catch (e: NumberFormatException) {
    defaultValue
}

fun Activity.hideSoftKeyboard(v: View) {
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //val view: View? = this.currentFocus
    inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
}

@SuppressLint("SetTextI18n")
fun TextView.addText(text: Spanned) {
    val newText = SpannableString(TextUtils.concat(this.text,/*"\n",*/text))
    Selection.setSelection(newText, newText.length)
    this.setText(newText, TextView.BufferType.SPANNABLE)
}

fun View.fadeOut(duration: Long, offset: Long = 0, isAlpha: Boolean = true, minAlpha: Float = 0.0f, maxAlpha: Float = 1.0f) {
    val fadeOut = AlphaAnimation(if (isAlpha) maxAlpha else 1.0f, if (isAlpha) minAlpha else 0.0f)
    fadeOut.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationStart(animation: Animation) {
        }

        override fun onAnimationEnd(animation: Animation) {

            if (isAlpha)
                this@fadeOut.alpha = minAlpha
            else
                this@fadeOut.visibility = View.GONE
        }

        override fun onAnimationRepeat(animation: Animation) {

        }
    })
    fadeOut.duration = duration
    fadeOut.startOffset = offset
    this.startAnimation(fadeOut)
}

fun View.slideUp(duration: Long, offset: Long = 0) {
    val animation = ScaleAnimation(
            1.0f, 1.0f, 1.0f, 0.0f,
            Animation.RELATIVE_TO_PARENT, 0.0f,
            Animation.RELATIVE_TO_PARENT, 0.0f
    )
    animation.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationStart(animation: Animation) {
        }

        override fun onAnimationEnd(animation: Animation) {
            this@slideUp.visibility = View.GONE
        }

        override fun onAnimationRepeat(animation: Animation) {

        }
    })
    animation.duration = duration
    animation.startOffset = offset
    this.startAnimation(animation)
}

