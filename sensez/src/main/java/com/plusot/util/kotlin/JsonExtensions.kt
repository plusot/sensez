package com.plusot.util.kotlin

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Package: com.bronstenkate.diabetes24.extensions
 * Project: $diabetes24android
 *
 * Created by Peter Bruinink on 19/1/18.
 * Copyright © 2018 Brons Ten Kate. All rights reserved.
 */

fun JSONObject.string2Int(item: String, defaultValue: Int = 0): Int = try {
    this.getString(item).toInt()
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}

fun JSONArray.string2Int(index: Int, defaultValue: Int = 0): Int = try {
    this.getString(index).toInt()
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}


fun JSONArray.string2Long(index: Int, defaultValue: Long = 0): Long = try {
    this.getString(index).toLongOrNull() ?: defaultValue
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}

fun JSONObject.string2Double(item: String, defaultValue: Double = 0.0): Double = try {
    this.getString(item).toDouble()
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}

fun JSONObject.asString(item: String, defaultValue: String = ""): String = try {
    this.getString(item)
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}

fun JSONArray.asString(index: Int, defaultValue: String = ""): String = try {
    this.getString(index)
} catch (e: JSONException) {
    defaultValue
} catch (e: NumberFormatException) {
    defaultValue
}
