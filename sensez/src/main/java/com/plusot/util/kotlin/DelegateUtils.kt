package com.plusot.util.kotlin

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


fun <T> mutableLazy(initializer: () -> T): ReadWriteProperty<Any?, T> = MutableLazy<T>(initializer)

private class MutableLazy<T>(val initializer: () -> T) : ReadWriteProperty<Any?, T> {
    private var value: T? = null //3
    private var initialized = false

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {//4
        synchronized(this) { //5
            if (!initialized) {
                value = initializer()
            }
            @Suppress("UNCHECKED_CAST")
            return value as T //6
        }
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        synchronized(this) {
            this.value = value
            initialized = true
        }
    }

}