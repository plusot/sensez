package com.plusot.util.kotlin

import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import androidx.annotation.ColorRes
import com.plusot.util.Globals
import com.plusot.util.util.Format
import java.util.*

/**
 * File: Extensions
 * Package: com.bronstenkate.diabetes24.util
 * Project: Diabetes24
 *
 * Created by Peter Bruinink on 12/12/17.
 * Copyright © 2017 Brons Ten Kate. All rights reserved.
 */


const val downArrow = '▾'

fun Int.getString() : String {
    val context = Globals.getAppContext()
    return context?.getString(this) ?: "Unknown String $this"
}

fun Int.getString(vararg formatArgs: Any) : String {
    val context = Globals.getAppContext()
    return context?.getString(this, *formatArgs) ?: "Unknown String $this"
}


fun Int.getColor() : Int {
    val context = Globals.getAppContext()
    return context?.resources?.getColored(this) ?: 0xFFAAAAAA.toInt()
}

fun Double.format(decimals: Int): String = Format.format(this, decimals)

fun Float.format(decimals: Int): String = Format.format(this.toDouble(), decimals)

fun String.toIntOr(default: Int = 0): Int = this.toIntOrNull() ?: default


fun String.toIntOr(func: (Int) -> Unit) {
    val i = this.toIntOrNull()
    if (i != null) func(i)
}

fun String.toDoubleOr(default: Double = 0.0): Double = this.toDoubleOrNull() ?: default

fun String.sub(length: Int): String {
    val l = Math.min(length, this.length)
    return this.substring(0, l)
}

fun String.toLongOr(default: Long = 0): Long = this.toLongOrNull() ?: default

inline fun <reified E : Enum<E>> enumSetOf(): EnumSet<E> = EnumSet.noneOf(E::class.java)

val Int.dp2Px: Int
    get() = Math.round(this * (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

//val Int.px2Dp: Int
//    get() = Math.round(this / (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

inline fun <reified T : Enum<T>> Int.toEnum() : T {
    return enumValues<T>()[this % enumValues<T>().size]
}


inline fun <reified T : Enum<T>> String.toEnum() : T {
    enumValues<T>().forEach { if (it.toString().toLowerCase() == this.toLowerCase()) return it }
    return enumValues<T>()[0]
}

inline fun <reified T : Enum<T>> String.toEnum(func: (T) -> Unit)  {
    enumValues<T>().forEach { if (it.toString().toLowerCase() == this.toLowerCase()) func(it) }
}

fun Resources.getColored(@ColorRes color: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.getColor(color, null)
    } else {
        @Suppress("DEPRECATION")
        this.getColor(color)
    }
}

fun randomInt(maxPlus1 : Int) = (1.0 * (maxPlus1) * Math.random()).toInt()
