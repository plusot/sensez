package com.plusot.util.dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.core.content.ContextCompat;

import com.plusot.util.Globals;
import com.plusot.sensez.R;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.Watchdog;


public class Alerts {

    public enum ClickResult {
        YES,
        NO,
        CANCEL,
        NEUTRAL,
        DISMISS,
        ;
    }

    public interface Listener {
        void onClick(ClickResult clickResult);
    }

    public interface ItemListener {
        void onClick(ClickResult clickResult, String whichString, int which);
    }

    public interface InputListener {
        void onClick(ClickResult clickResult, String string);
    }

    public interface NumberInputListener {
        void onClick(ClickResult clickResult, int number);
    }

    @SuppressLint("SetTextI18n")
    public static void showInputDialog(Context context, int titleId, int message, final int inputNumber, int hint, final NumberInputListener listener) {
        if (!Watchdog.isRunning()) return;
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.alert_edit_text, null);

        final EditText input = (EditText) view.findViewById(R.id.input);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
        input.setText("" + inputNumber);
        input.setHint("" + hint);

        new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(message)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            int number = Integer.parseInt(input.getText().toString());
                            listener.onClick(ClickResult.YES, number);
                        } catch (NumberFormatException e) {
                            listener.onClick(ClickResult.YES, inputNumber);
                        }
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        listener.onClick(ClickResult.CANCEL, inputNumber);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();

    }

    public static void showOkDialog(Context context, int titleId, String message) {
        showOkDialog(context, titleId, message, 0, null);
    }

    public static void showOkDialog(Context context, int titleId, int messageId, long timeOut) {
        showOkDialog(context, titleId, messageId, timeOut, null);
    }


    public static void showOkDialog(Context context, int titleId, String message, long timeOut) {
        showOkDialog(context, titleId, message, timeOut, null);
    }

    public static void showOkDialog(Context context, int titleId, String message, Listener listener) {
        showOkDialog(context, titleId, message, 0, listener);
    }

    public static void showOkDialog(Context context, int titleId, String message, long timeOut, final Listener listener) {
        if (!Watchdog.isRunning()) return;
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) listener.onClick(ClickResult.YES);
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (listener != null) listener.onClick(ClickResult.CANCEL);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        if (timeOut > 0) {
            SleepAndWake.runInMain(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        LLog.e("Could not dismiss dialog: " + e.getMessage());
                    }
                }
            },timeOut, "Alerts.showOkDialog");
        }
    }

    public static void showOkDialog(Context context, int titleId, int messageId, long timeOut, final Listener listener) {
        if (!Watchdog.isRunning()) return;
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok, (dialog1, which) -> {
                    if (listener != null) listener.onClick(ClickResult.YES);
                })
                .setOnCancelListener(dialog12 -> {
                    if (listener != null) listener.onClick(ClickResult.CANCEL);
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        if (timeOut > 0) {
            SleepAndWake.runInMain(() -> {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    LLog.e("Could not dismiss dialog: " + e.getMessage());
                }
            },timeOut, "Alert.showOkDialog");
        }
    }

    public static void showYesNoDialog(Context context, int titleId, String message, final Listener listener) {
        showYesNoDialog(context, titleId, message, listener, android.R.drawable.ic_dialog_info);
    }

    public static void showYesNoDialog(Context context, int titleId, String message, final Listener listener, int iconId) {
        if (Watchdog.isRunning()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(message)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.YES);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.NO);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            listener.onClick(ClickResult.CANCEL);
                        }
                    })
                    .setIcon(iconId);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        listener.onClick(ClickResult.DISMISS);
                    }
                });
            }
            builder.show();
        }
    }

    public static void showYesNoDialog(Context context, int titleId, int messageId, final Listener listener) {
        if (Watchdog.isRunning()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(messageId)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.YES);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.NO);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            listener.onClick(ClickResult.CANCEL);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info);
            //.show();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        listener.onClick(ClickResult.DISMISS);
                    }
                });
            }
            AlertDialog dialog = builder.create();
            //dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show();
        }
    }


    public static void showYesNoNeutralDialog(Context context, int titleId, int messageId, int neutral, final Listener listener) {
        if (Watchdog.isRunning()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(messageId)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.YES);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.NO);
                        }
                    })
                    .setNeutralButton(neutral, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.NEUTRAL);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            listener.onClick(ClickResult.CANCEL);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info);
            //.show();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        listener.onClick(ClickResult.DISMISS);
                    }
                });
            }
            AlertDialog dialog = builder.create();
            //dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show();
        }
    }

    public static void showYesNoDialog(Context context, int titleId, int messageId, final Listener listener, int icon) {
        showYesNoDialog(context, titleId, messageId,R.string.yes, R.string.no, listener, icon);
    }

    public static void showYesNoDialog(Context context, int titleId, int messageId, int positive, int negative, final Listener listener, int icon) {
        if (icon == -1) icon = android.R.drawable.ic_dialog_info;
        if (Watchdog.isRunning()) try {
            boolean isOverlay = false;
            Context appContext = Globals.getAppContext();
            if (appContext == null) return;
            if (context == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.SYSTEM_ALERT_WINDOW)) {
                        LLog.i("Granted system alert window");
                        isOverlay = true;
                    } else {
                        context = Globals.getVisibleActivity();
                        if (context == null) return;
                    }
                } else
                    context = appContext;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(messageId)
                    .setPositiveButton(positive, (dialog, which) -> listener.onClick(ClickResult.YES))
                    .setNegativeButton(negative, (dialog, which) -> listener.onClick(ClickResult.NO))
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            listener.onClick(ClickResult.CANCEL);
                        }
                    })
                    .setCancelable(true)
                    .setIcon(icon);

//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    listener.onClick(ClickResult.DISMISS);
                }
            });
//            }
            AlertDialog dialog = builder.create();
            // for Android API 23+
            if (isOverlay && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.SYSTEM_ALERT_WINDOW)) {
                Window w = dialog.getWindow();
                if (w != null) w.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            }
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            LLog.i("Could not show Alert", e);
        }
    }

    public static void showYesNoDialog(Context context, int titleId, final int messageId, final int positive, final int negative, final int neutral, final Listener listener) {
        int icon = android.R.drawable.ic_dialog_info;

        if (Watchdog.isRunning()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(messageId)
                    .setPositiveButton(positive, (dialog, which) -> listener.onClick(ClickResult.YES))
                    .setNegativeButton(negative, (dialog, which) -> listener.onClick(ClickResult.NO))
                    .setNeutralButton(neutral, (dialog, which) -> listener.onClick(ClickResult.NEUTRAL))
                    .setOnCancelListener(dialog -> listener.onClick(ClickResult.CANCEL))
                    //.setCancelable(false)
                    .setIcon(icon);

//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener(dialog -> listener.onClick(ClickResult.DISMISS));
//            }
            final AlertDialog dialog = builder.create();
            dialog.show();

        }
    }

    public static void showYesNoDialog(Context context, int titleId, final String message, final int positive, final int negative, final Listener listener, int icon) {
        if (icon == -1) icon = android.R.drawable.ic_dialog_info;

        if (Watchdog.isRunning()) {
            if (context == null) {
                LLog.i("Context is NULL for dialog with " + message);
                context = Globals.getVisibleActivity();
                if (context == null) return;

            }
//             {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.SYSTEM_ALERT_WINDOW))
//                        LLog.i("Granted system alert window");
//                    else {
//                        context = Globals.getVisibleActivity();
//                        if (context == null) return;
//                    }
//                } else
//                    context = appContext;
//            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle(titleId)
                    .setMessage(message)
                    .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.YES);
                        }
                    })
                    .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onClick(ClickResult.NO);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            listener.onClick(ClickResult.CANCEL);
                        }
                    })
                    //.setCancelable(false)
                    .setIcon(icon);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        listener.onClick(ClickResult.DISMISS);
                    }
                });
            }
            final AlertDialog dialog = builder.create();
            SleepAndWake.runInMain(() -> {
                // for Android API 23+
//                if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.SYSTEM_ALERT_WINDOW)) {
//                    LLog.i("Granted system alert window");
//                    Window w = dialog.getWindow();
//                    if (w != null) w.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//                }
                LLog.i("Trying dialog: " + message);
                dialog.show();
            }, "Alerts.showYesNoDialog");
//        } catch (WindowManager.BadTokenException e) {
//            LLog.i("Could not show Alert", e);
        }
    }

    public static void showOkAlert(Context context, int titleId, String messageId) {

        if (Watchdog.isRunning()) new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void showOkAlert(Context context, int titleId, int messageId) {
        if (Watchdog.isRunning() ) new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void showItemsDialog(Context context, int titleId, final String[] strings, final ItemListener listener) {
        if (!Watchdog.isRunning() ) return;
        AlertDialog.Builder builder =  new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setItems(strings, (dialog, which) -> {
                    listener.onClick(ClickResult.YES, strings[which], which);
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, null)
                .setOnCancelListener(dialog -> listener.onClick(ClickResult.CANCEL, null, -1));
        AlertDialog dialog = builder.create();
        ListView listView = dialog.getListView();
        listView.setDivider(new ColorDrawable(dialog.getContext().getResources().getColor(R.color.blueAccentColor)));
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(2); // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false);
        listView.setHeaderDividersEnabled(false);

        dialog.show();
    }

    public static void showItemsDialog(Context context, int titleId, final String[] strings, final boolean[] checked, final ItemListener listener) {
        if (!Watchdog.isRunning() ) return;
        AlertDialog.Builder builder =  new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMultiChoiceItems(strings, checked, (dialog, which, isChecked) -> {
                    if (isChecked)
                        listener.onClick(ClickResult.YES, strings[which], which);
                    else
                        listener.onClick(ClickResult.NO, strings[which], which);
                })
//                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, (dialog, which) -> listener.onClick(ClickResult.YES, null, -1))
                .setOnCancelListener(dialog -> listener.onClick(ClickResult.CANCEL, null, -1));
        AlertDialog dialog = builder.create();
        ListView listView = dialog.getListView();
        listView.setDivider(new ColorDrawable(dialog.getContext().getResources().getColor(R.color.blueAccentColor)));
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(2); // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false);
        listView.setHeaderDividersEnabled(false);

        dialog.show();
    }

    public static void showItemsDialog(Context context, int titleId, final ListAdapter adapter, final Alerts.ItemListener listener, final long timeOut) {
        if (!Watchdog.isRunning() ) return;
        AlertDialog.Builder builder =  new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onClick(Alerts.ClickResult.YES, null, which);
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        listener.onClick(Alerts.ClickResult.CANCEL, null, -1);
                    }
                });
        final AlertDialog dialog = builder.create();
        ListView listView = dialog.getListView();
        listView.setDivider(new ColorDrawable(dialog.getContext().getResources().getColor(R.color.blueAccentColor)));
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(2); // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false);
        listView.setHeaderDividersEnabled(false);

        dialog.show();
        if (timeOut > 0) {
            SleepAndWake.runInMain(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        LLog.e("Could not dismiss dialog: " + e.getMessage());
                    }
                }
            },timeOut, "Alerts.showItemsDialog");
        }
    }


    public static void showSingleChoiceDialog(Context context, int titleId, final String[] strings, final int iSelected, final ItemListener listener, final long timeOut) {
        if (!Watchdog.isRunning() ) return;
        AlertDialog.Builder builder =  new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setSingleChoiceItems(strings, iSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onClick(ClickResult.YES, strings[which], which);
                        dialog.dismiss();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        listener.onClick(ClickResult.CANCEL, null, -1);
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
        if (timeOut > 0) {
            SleepAndWake.runInMain(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        LLog.e("Could not dismiss dialog: " + e.getMessage());
                    }
                }
            },timeOut, "Alerts.showOkDialog");
        }
    }

    public static void showSingleChoiceDialog(Context context, int titleId, final String[] strings, final String selected, final ItemListener listener) {
        if (!Watchdog.isRunning() ) return;
        int iSelected = 0;
        if (selected != null) for (String str: strings) {
            if (selected.contains(str)) break;
            iSelected++;
        }
        if (iSelected >= strings.length) iSelected = -1;
        AlertDialog.Builder builder =  new AlertDialog.Builder(context)
                .setTitle(titleId)
                .setSingleChoiceItems(strings, iSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onClick(ClickResult.YES, strings[which], which);
                        dialog.dismiss();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        listener.onClick(ClickResult.CANCEL, null, -1);
                    }
                });
        AlertDialog dialog = builder.create();
//        ListView listView= dialog.getListView();
//        listView.setHeaderDividersEnabled(true);
//        listView.setDivider(new ColorDrawable(Color.GRAY)); // set color
//        listView.setDividerHeight(2); // set height
//                .setMessage(messageId)
//                .setIcon(android.R.drawable.ic_dialog_alert)
        dialog.show();
    }
}
