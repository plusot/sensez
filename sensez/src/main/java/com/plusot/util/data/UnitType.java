package com.plusot.util.data;


enum UnitType {
    DENSITY_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.KG_PER_CUBIC_METER;
        }
    },
    DIGITAL_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.BYTE;
        }
    },
    VOLTAGE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.VOLT;
        }
    },
    PRESSURE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.MBAR;
        }
    },
    TEMPERATURE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.CELSIUS;
        }
    },
    EFFICIENCY_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.JOULEPERBEAT;
        }
    },
    DATETIME_UNIT(Long.class) {
        @Override
        public boolean isNumeric() {
            return false;
        }
        @Override
        public Unit defaultUnit() {
            return Unit.DATETIME;
        }
        @Override
        public Unit metricUnit() {
            return Unit.MILLISECOND;
        }
    },
    PULSE_WIDTH_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.MILLISECOND;
        }
    },
    SECOND_TIME_UNIT(Long.class){
        @Override
        public Unit defaultUnit() {
            return Unit.SECOND;
        }

        @Override
        public Unit metricUnit() {
            return Unit.MILLISECOND;
        }
    },
    WIND_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.KPH;
        }
        @Override
        public Unit metricUnit() {
            return Unit.MPS;
        }
    },
    SPEED_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.KPH;
        }
        @Override
        public Unit metricUnit() {
            return Unit.MPS;
        }
    },
    MAGNETIC_FIELD_UNIT(Float.class) {
        @Override
        public int getDimensions() {
            return 3;
        }
        @Override
        public Unit defaultUnit() {
            return Unit.MICROTESLA;
        }

    },
    ACCELERATION_UNIT(Float.class){
        @Override
        public int getDimensions() {
            return 3;
        }
        @Override
        public Unit defaultUnit() {
            return Unit.MPSS;
        }

    },
    DISTANCE_UNIT(Double.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.METER;
        }
    },
    ANGLE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.DEGREE;
        }
    },
    ANGLE3D_UNIT(Float.class){
        @Override
        public int getDimensions() {
            return 3;
        }

        @Override
        public Unit defaultUnit() {
            return Unit.DEGREE;
        }
    },
    CYCLES_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.RPM;
        }
    },
    BEATS_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.BPM;
        }
    },
    WEIGHT_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.KG;
        }
    },
    COUNT_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.COUNT;
        }
    },
    HEXCOUNT_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.NONE;
        }
    },
    REFERENCED_POWER_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.DBM;
        }
    },
    POWER_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.WATT;
        }
    },
    FORCE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.N;
        }
    },
    ENERGY_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.JOULE;
        }
    },
    TORQUE_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.NM;
        }
    },
    RATIO_UNIT(Double.class){
        @Override
        public Unit defaultUnit() {
            return Unit.RATIO;
        }
    },
    STEP_UNIT(Integer.class){
        @Override
        public Unit defaultUnit() {
            return Unit.COUNT;
        }
    },
    INT_WITH_NO_UNIT(Integer.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.NONE;
        }
    },
    DOUBLE_WITH_NO_UNIT(Double.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.NONE;
        }
    },
    TEXT_UNIT(String.class){
        @Override
        public boolean isNumeric() {
            return false;
        }
        @Override
        public Unit defaultUnit() {
            return Unit.TEXT;
        }
    },
    LIGHT_UNIT(Double.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.LUX;
        }
    },
    VOLUME_UNIT(Double.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.MILLILITER;
        }

        @Override
        public Unit metricUnit() {
            return Unit.LITER;
        }
    },
    ROTATIONSPEED_UNIT(Double.class) {
        @Override
        public Unit defaultUnit() {
            return Unit.DEGREESPERSECOND;
        }
    },
    ;

    private final Class<?> unitClass;


    UnitType(final Class<?> unitClass) {
        this.unitClass = unitClass;
    }

    public boolean isNumeric() {
        return true;
    }

    public Unit defaultUnit() {
        return Unit.NONE;
    }

    public Unit metricUnit() {
        return defaultUnit();
    }

    public Class<?> getUnitClass() {
        return unitClass;
    }

    public int getDimensions() {
        return 1;
    }
}
