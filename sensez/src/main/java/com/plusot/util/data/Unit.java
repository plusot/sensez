package com.plusot.util.data;

import android.content.Context;

import com.plusot.util.Globals;
import com.plusot.sensez.R;
import com.plusot.util.util.Scaler;

public enum Unit implements Scaler {
    BYTE(R.string.byte_text, R.string.byte_voice, UnitType.DIGITAL_UNIT, 1),
    MEGA_BYTE(R.string.mega_byte_text, R.string.mega_byte_voice, UnitType.DIGITAL_UNIT, 1.0 / (1024 * 1024)),
    KILO_BYTE(R.string.kilo_byte_text, R.string.kilo_byte_voice, UnitType.DIGITAL_UNIT, 1.0 / (1024)),
    GIGA_BYTE(R.string.giga_byte_text, R.string.giga_byte_voice, UnitType.DIGITAL_UNIT, 1.0 / (1024 * 1024 + 1024)),
    MILLILITER(R.string.milliliter, R.string.milliliter_voice, UnitType.VOLUME_UNIT, 1000),
    LITER(R.string.liter, R.string.liter_voice, UnitType.VOLUME_UNIT),
    LUX(R.string.lux, R.string.lux_voice, UnitType.LIGHT_UNIT),
    KG(R.string.kg, R.string.kg_voice, UnitType.WEIGHT_UNIT, 0.001),
    DEGREESPERSECOND(R.string.degreespersecond, R.string.degreespersecond_voice, UnitType.ANGLE_UNIT),
    DEGREE(R.string.degree, R.string.degree_voice, UnitType.ANGLE_UNIT),
    CELSIUS(R.string.celsius, R.string.celsius_voice, UnitType.TEMPERATURE_UNIT),
    BPM(R.string.bpm,R.string.bpm_voice, UnitType.BEATS_UNIT),
    RPM(R.string.rpm,R.string.rpm_voice, UnitType.CYCLES_UNIT),
    VOLT(R.string.volt,R.string.volt_voice, UnitType.VOLTAGE_UNIT),
    BEAUFORT(R.string.bft,R.string.bft_voice, UnitType.WIND_UNIT),
    MPS(R.string.mps,R.string.mps_voice, UnitType.SPEED_UNIT),
    SHORTDATE(R.string.datetime,R.string.datetime_voice, UnitType.DATETIME_UNIT),
    LONGDATE(R.string.datetime,R.string.datetime_voice, UnitType.DATETIME_UNIT),
    MILLISECOND(R.string.ms,R.string.ms_voice, UnitType.SECOND_TIME_UNIT, 1),
    TENTHSECOND(R.string.tenths,R.string.tenths_voice, UnitType.SECOND_TIME_UNIT, 1.0 / 100),
    SECOND(R.string.sec,R.string.sec_voice, UnitType.SECOND_TIME_UNIT, 1.0 / 1000),
    HOURSECOND(R.string.hoursec,R.string.sec_voice, UnitType.SECOND_TIME_UNIT, 1.0 / 1000),
    FAHRENHEIT(R.string.fahrenheit,R.string.fahrenheit_voice, UnitType.TEMPERATURE_UNIT),
    INHG(R.string.inhg,R.string.inhg_voice, UnitType.PRESSURE_UNIT, 1.0 / 3386.389),
    MMHG(R.string.mmhg,R.string.mmhg_voice, UnitType.PRESSURE_UNIT, 1.0 / 133.322387415),
    HPA(R.string.hpa,R.string.hpa_voice, UnitType.PRESSURE_UNIT, 1.0 / 100),
    PSI(R.string.psi,R.string.psi_voice, UnitType.PRESSURE_UNIT, 1.0 / 6894.755),
    DATETIME(R.string.datetime,R.string.datetime_voice, UnitType.DATETIME_UNIT),
    TIME(R.string.datetime,R.string.datetime_voice, UnitType.DATETIME_UNIT),
    SHORTTIME(R.string.datetime,R.string.datetime_voice, UnitType.DATETIME_UNIT),
    MBAR(R.string.mbar,R.string.mbar_voice, UnitType.PRESSURE_UNIT, 1.0 / 100),
    METER(R.string.meter,R.string.meter_voice, UnitType.DISTANCE_UNIT),
    FPSS(R.string.fpss, R.string.fpss_voice, UnitType.ACCELERATION_UNIT, 3.280840),
    MICROTESLA(R.string.microtesla, R.string.microtesla_voice, UnitType.MAGNETIC_FIELD_UNIT),
    MPSS(R.string.mpss, R.string.mpss_voice, UnitType.ACCELERATION_UNIT),

    KPH(R.string.kph, R.string.kph_voice, UnitType.SPEED_UNIT, 3.6),
    MPH(R.string.mph, R.string.mph_voice, UnitType.SPEED_UNIT, 2.236936),
    FPS(R.string.fps, R.string.fps_voice, UnitType.SPEED_UNIT, 3.280840),
    FPM(R.string.fpm, R.string.fpm_voice,  UnitType.SPEED_UNIT, 3.280840 * 60.0),
    KNOT(R.string.knot, R.string.knot_voice, UnitType.SPEED_UNIT, 1.943844),
    KM(R.string.km, R.string.km_voice, UnitType.DISTANCE_UNIT, 0.001),
    INCH(R.string.inch, R.string.inch_voice, UnitType.DISTANCE_UNIT, 39.3700787),
    FEET(R.string.feet, R.string.feet_voice, UnitType.DISTANCE_UNIT, 3.2808399),
    YARD(R.string.yard, R.string.yard_voice, UnitType.DISTANCE_UNIT, 1.0936),
    MILE(R.string.mile, R.string.mile_voice, UnitType.DISTANCE_UNIT, 0.000621371192),
    JOULE(R.string.joule,R.string.joule_voice, UnitType.ENERGY_UNIT),
    JOULEPERBEAT(R.string.jouleperbeat,R.string.jouleperbeat_voice, UnitType.EFFICIENCY_UNIT),
    KILOJOULE(R.string.kilojoule, R.string.kilojoule_voice, UnitType.ENERGY_UNIT, 0.001),
    KCAL(R.string.kcal, R.string.kcal_voice, UnitType.ENERGY_UNIT, 0.001 / 4.184),
    KG_PER_CUBIC_METER(R.string.kg_per_cubic_meter, R.string.kg_per_cubic_meter_voice, UnitType.DENSITY_UNIT, 1),
    COUNT(R.string.count,R.string.count_voice, UnitType.COUNT_UNIT),
    WATT(R.string.watt, R.string.watt_voice, UnitType.POWER_UNIT),
    PK(R.string.pk, R.string.pk_voice, UnitType.POWER_UNIT, 0.0013596216173),
    HP(R.string.hp, R.string.hp_voice, UnitType.POWER_UNIT, 0.00134048257373),
    NM(R.string.nm, R.string.nm_voice, UnitType.TORQUE_UNIT),
    N(R.string.newton, R.string.n_voice, UnitType.FORCE_UNIT),
    PERCENT(R.string.percent, R.string.percent_voice, UnitType.RATIO_UNIT, 100.0),
    RATIO(R.string.ratio, R.string.ratio_voice, UnitType.RATIO_UNIT, 1.0),
    DBM(R.string.dbm, R.string.dbm_voice, UnitType.REFERENCED_POWER_UNIT, 1.0),
    TEXT(R.string.none, R.string.none, UnitType.TEXT_UNIT),
    NONE(R.string.none,R.string.none, UnitType.INT_WITH_NO_UNIT),
    ;

    final int label;
    final int voiceLabel;
    final UnitType unitType;
    final Number scaler;

    Unit(final int label, final int voicelabel, final UnitType unitType) {
        this(label, voicelabel, unitType, unitType.getUnitClass().equals(Double.class) ? 1.0 :1);

    }

    Unit(final int label, final int voicelabel, final UnitType unitType, final Number scaler) {
        this.label = label;
        this.voiceLabel = voicelabel;
        this.unitType = unitType;
        this.scaler = scaler;
    }

    @Override
    public Number scale(Number value) {
        switch (this) {
            case BEAUFORT:
                return Math.pow(value.doubleValue() / 0.836, 2.0 / 3.0);
            case FAHRENHEIT:
                return value.doubleValue() * 1.8 + 32.0;
            default:
                if (unitType.getUnitClass().equals(Long.class)) {
                    return value.longValue();
                } else if (unitType.getUnitClass().equals(Integer.class)) {
                    return value.intValue();
                } else if (unitType.getUnitClass().equals(String.class)) {
                    return 0;
                } else if (unitType.getUnitClass().equals(Double.class) || unitType.getUnitClass().equals(Float.class)) {
                    if (scaler == null) return value.doubleValue();
                    return scaler.doubleValue() * value.doubleValue();
                } else
                    return 0;
        }
    }


    public double scale(double value) {
        switch (this) {
            case BEAUFORT:
                return Math.pow(value / 0.836, 2.0 / 3.0);
            case FAHRENHEIT:
                return value * 1.8 + 32.0;
            default:
                if (unitType.getUnitClass().equals(Long.class)) {
                    return value;
                } else if (unitType.getUnitClass().equals(Integer.class)) {
                    return value;
                } else if (unitType.getUnitClass().equals(String.class)) {
                    return 0;
                } else if (unitType.getUnitClass().equals(Double.class) || unitType.getUnitClass().equals(Float.class)) {
                    if (scaler == null) return value;
                    return scaler.doubleValue() * value;
                } else
                    return 0;
        }
    }

    public long scale(long value) {
        return (long) (scaler.doubleValue() * value);
    }

    @Override
    public Number scaleBack(Number value) {
        switch (this) {
            case BEAUFORT:
                return Math.pow(value.doubleValue(), 1.5) * 0.836;
            case FAHRENHEIT:
                return (value.doubleValue() - 32.0) / 1.8;
            default:
                if (unitType.getUnitClass().equals(Long.class)) {
                    return value.longValue();
                } else if (unitType.getUnitClass().equals(Integer.class)) {
                    return value.intValue();
                } else if (unitType.getUnitClass().equals(String.class)) {
                    return 0;
                } else if (unitType.getUnitClass().equals(Double.class) || unitType.getUnitClass().equals(Float.class)) {
                    if (scaler == null) return value.doubleValue();
                    return value.doubleValue() / scaler.doubleValue();
                } else
                    return 0;
        }
    }

    @Override
    public String toString() {
        Context appContext = Globals.getAppContext();         if (appContext == null) return "";

        return appContext.getString(label);
    }

    public String toVoiceString() {
        Context appContext = Globals.getAppContext();         if (appContext == null) return "";
        return appContext.getString(voiceLabel);
    }

//    public String toHtmlString() {
//        Context context = Globals.getAppContext();
//        if (context == null) return "";
//        return context.getString(label).replace("˚", "&deg;").replace("²", "&sup2;");
//    }


}
