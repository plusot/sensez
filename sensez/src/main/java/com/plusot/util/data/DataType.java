package com.plusot.util.data;

import android.content.Context;

import com.plusot.util.Globals;
import com.plusot.sensez.R;
import com.plusot.util.json.JSoNField;
import com.plusot.util.json.JSoNHelper;
import com.plusot.util.preferences.PreferenceKey;
import com.plusot.util.util.Format;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.MathVector;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.TimeUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public enum DataType {
    ACCELERATION(R.string.acceleration, 2, UnitType.ACCELERATION_UNIT),
    ACCELERATION_XYZ(R.string.acceleration_xyz, 2, UnitType.ACCELERATION_UNIT),
    AEROBIC_HEART_RATE_LOWER_LIMIT(R.string.aerobic_heart_rate_lower_limit, 0, UnitType.BEATS_UNIT),
    AEROBIC_HEART_RATE_UPPER_LIMIT(R.string.aerobic_heart_rate_upper_limit, UnitType.BEATS_UNIT),
    AEROBIC_THRESHOLD(R.string.aerobic_threshold, UnitType.BEATS_UNIT),
    AGE(R.string.age, 0, UnitType.INT_WITH_NO_UNIT),
    AGGREGATE(R.string.aggregate,0),
    AIR_PRESSURE(R.string.airpressure, UnitType.PRESSURE_UNIT),                    // Pa
    ALERT_CATEGORY_ID(R.string.alert_category_id,0),
    ALERT_CATEGORY_ID_BIT_MASK(R.string.alert_category_id_bit_mask,0),
    ALERT_LEVEL(R.string.alert_level,0),
    ALERT_NOTIFICATION_CONTROL_POINT(R.string.alert_notification_control_point,0),
    ALERT_STATUS(R.string.alert_status,0),
    ALTITUDE(R.string.altitude, UnitType.DISTANCE_UNIT) {
        @Override
        public boolean hasAvg() {
            return false;
        }

        @Override
        public boolean hasMin() {
            return hasStats();
        }
    },                        // m
    ALTITUDE_NMEA(R.string.altitudeNmea, UnitType.DISTANCE_UNIT) {
        @Override
        public boolean hasAvg() {
            return false;
        }

        @Override
        public boolean hasMin() {
            return hasStats();
        }
    },                        // m
    ALTITUDE_RAW(R.string.altitudeRaw, UnitType.DISTANCE_UNIT) {
        @Override
        public boolean hasAvg() {
            return false;
        }

        @Override
        public boolean hasMin() {
            return hasStats();
        }
    },                        // m
    ANAEROBIC_HEART_RATE_LOWER_LIMIT(R.string.anaerobic_heart_rate_lower_limit, UnitType.BEATS_UNIT),
    ANAEROBIC_HEART_RATE_UPPER_LIMIT(R.string.anaerobic_heart_rate_upper_limit, UnitType.BEATS_UNIT),
    ANAEROBIC_THRESHOLD(R.string.anaerobic_threshold,0),
    ANALOG(R.string.analog,0),
    ANDROID_VERSION(R.string.android_version, UnitType.TEXT_UNIT),
    APPARENT_WIND_DIRECTION_(R.string.apparent_wind_direction,0, UnitType.ANGLE_UNIT),
    APPARENT_WIND_SPEED(R.string.apparent_wind_speed,0, UnitType.SPEED_UNIT),
    APPEARANCE(R.string.appearance, UnitType.TEXT_UNIT),
    ASCENT(R.string.ascent_total, UnitType.DISTANCE_UNIT)  {
        @Override
        public boolean hasStats() {
            return false;
        }
        @Override
        public boolean isAggregate() {
            return true;
        }
    },                        // m
    BAROMETRIC_PRESSURE_TREND(R.string.barometric_pressure_trend,0),
    BATTERY_LEVEL(R.string.battery_level, 0, UnitType.RATIO_UNIT) {
        @Override public boolean hasStats() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.PERCENT;
        }
    },
    BATTERY_VOLTAGE(R.string.battery_level, 1, UnitType.VOLTAGE_UNIT) {
        @Override public boolean hasStats() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.VOLT;
        }
    },
    BATTERY_INFO(R.string.battery_info, UnitType.TEXT_UNIT),
    BEARING(R.string.bearing, UnitType.ANGLE_UNIT),                            // degrees
    BLOOD_PRESSURE_FEATURE(R.string.blood_pressure_feature,0),
    BLOOD_PRESSURE_SYSTOLIC(R.string.blood_pressure_systolic, 0, UnitType.PRESSURE_UNIT) {
        @Override public Unit getUnit() {
            return Unit.MMHG;
        }
    },
    BLOOD_PRESSURE_DIASTOLIC(R.string.blood_pressure_diastolic, 0, UnitType.PRESSURE_UNIT){
        @Override public Unit getUnit() {
            return Unit.MMHG;
        }
    },
    BLOOD_PRESSURE_MEAN(R.string.blood_pressure_mean, 0, UnitType.PRESSURE_UNIT) {
        @Override public Unit getUnit() {
            return Unit.MMHG;
        }
    },
    BODY_COMPOSITION_FEATURE(R.string.body_composition_feature,0),
    BODY_COMPOSITION_MEASUREMENT(R.string.body_composition_measurement,0),
    BODY_MASS_INDEX(R.string.bmi,2, UnitType.DOUBLE_WITH_NO_UNIT),
    BODY_SENSOR_LOCATION(R.string.body_sensor_location,0){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    BOND_MANAGEMENT_CONTROL_POINT(R.string.bond_management_control_point,0),
    BOND_MANAGEMENT_FEATURE(R.string.bond_management_feature,0),
    BOOT_KEYBOARD_INPUT_REPORT(R.string.boot_keyboard_input_report,0),
    BOOT_KEYBOARD_OUTPUT_REPORT(R.string.boot_keyboard_output_report,0),
    BOOT_MOUSE_INPUT_REPORT(R.string.boot_mouse_input_report,0),
    CADENCE(R.string.cadence, UnitType.CYCLES_UNIT),                            // rpm
    CADENCE_INSTANT(R.string.cadence_instant, UnitType.CYCLES_UNIT),                            // rpm
    CENTRAL_ADDRESS_RESOLUTION(R.string.central_address_resolution,0),
    CGM_FEATURE(R.string.cgm_feature,0),
    CGM_MEASUREMENT(R.string.cgm_measurement,0),
    CGM_SESSION_RUN_TIME(R.string.cgm_session_run_time,0),
    CGM_SESSION_START_TIME(R.string.cgm_session_start_time,0),
    CGM_SPECIFIC_OPS_CONTROL_POINT(R.string.cgm_specific_ops_control_point,0),
    CGM_STATUS(R.string.cgm_status,0),
    COMPASS(R.string.compass, UnitType.ANGLE_UNIT),                            // degrees
    CPU(R.string.cpu, 0, UnitType.RATIO_UNIT) {
        @Override public Unit getUnit() {
            return Unit.PERCENT;
        }
    },
    CRANK_TORQUE(R.string.crank_torque, UnitType.TORQUE_UNIT),
    CSC_FEATURE(R.string.csc_feature,0),
    CSC_MEASUREMENT(R.string.csc_measurement,0),
    CYCLING_POWER_CONTROL_POINT(R.string.cycling_power_control_point,0),
    CYCLING_POWER_FEATURE(R.string.cycling_power_feature,0),
    CYCLING_POWER_MEASUREMENT(R.string.cycling_power_measurement,0),
    CYCLING_POWER_VECTOR(R.string.cycling_power_vector,0),
    DATABASE_CHANGE_INCREMENT(R.string.database_change_increment,0),
    DATA_SOURCE(R.string.datasource, UnitType.TEXT_UNIT) {
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public boolean isSupported() {
            return Globals.verbose;
        }
    },
    DATE_OF_BIRTH(R.string.date_of_birth,0),
    DATE_OF_THRESHOLD_ASSESSMENT(R.string.date_of_threshold_assessment,0),
    DATE_TIME(R.string.date_time, UnitType.DATETIME_UNIT){
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SHORTTIME;
        }
    },
    DAY_DATE_TIME(R.string.day_date_time, UnitType.COUNT_UNIT) {
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SHORTTIME;
        }
    },
    DAY_OF_WEEK(R.string.day_of_week, UnitType.COUNT_UNIT),
    DESCENT(R.string.descent_total, UnitType.DISTANCE_UNIT)  {
        @Override public boolean hasStats() {
            return false;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },                        // m
    DESCRIPTOR_VALUE_CHANGED(R.string.descriptor_value_changed,0),
    DEVICE_NAME(R.string.device_name, UnitType.TEXT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    DIGITAL(R.string.digital,0),
    DISTANCE(R.string.distance, 1, UnitType.DISTANCE_UNIT) {
        @Override public double minInterval() { return 5000.0; }
        @Override public Unit getUnit() {
            return Unit.KM;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    DISTANCE_TOTAL(R.string.distanceTotal, 0, UnitType.DISTANCE_UNIT) {
        @Override public double minInterval() { return 10000.0; }
        @Override public Unit getUnit() {
            return Unit.KM;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    DISTANCE_START(R.string.start_distance, 1, UnitType.DISTANCE_UNIT) {
        @Override public double minInterval() { return 10000.0; }
        @Override public Unit getUnit() {
            return Unit.KM;
        }
        @Override public boolean isSharable() {
            return false;
        }
        @Override public boolean hasStats() {
            return false;
        }
    },
    DISTANCE_POI(R.string.distance_poi, 1, UnitType.DISTANCE_UNIT) {
        @Override public double minInterval() { return 10000.0; }
        @Override public Unit getUnit() {
            return Unit.KM;
        }
        @Override public boolean isSharable() {
            return false;
        }
        @Override public boolean hasStats() {
            return false;
        }
    },
    DEW_POINT(R.string.dew_point,0),
    DST_OFFSET(R.string.dst_offset,0),
    EFFICIENCY(R.string.efficiency, UnitType.EFFICIENCY_UNIT),                    // ratio
    ELEVATION(R.string.elevation,0),
    EMAIL_ADDRESS(R.string.email_address, UnitType.TEXT_UNIT),
    ENERGY(R.string.energy, 1, UnitType.ENERGY_UNIT) {
        @Override public Unit getUnit() {
            return Unit.KILOJOULE;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    EOF(R.string.eof, 0, UnitType.INT_WITH_NO_UNIT),
    EXACT_TIME_256(R.string.exact_time_256,0),
    FAT_RATIO(R.string.fat,2, UnitType.DOUBLE_WITH_NO_UNIT),
    FAT_BURN_HEART_RATE_LOWER_LIMIT(R.string.fat_burn_heart_rate_lower_limit,0),
    FAT_BURN_HEART_RATE_UPPER_LIMIT(R.string.fat_burn_heart_rate_upper_limit,0),
    FIRMWARE_REVISION_STRING(R.string.firmware_revision_string,0){
        @Override public boolean isSupported() {
            return Globals.verbose;
        }
    },
    FIRST_NAME(R.string.first_name,0),
    FIVE_ZONE_HEART_RATE_LIMITS(R.string.five_zone_heart_rate_limits,0),
    GAIN_RATIO(R.string.gearratio, UnitType.RATIO_UNIT),                            // ratio
    GEOID_HEIGHT(R.string.geoidHeight, UnitType.DISTANCE_UNIT),
    GENDER(R.string.gender,0),
    GLUCOSE_FEATURE(R.string.glucose_feature,0),
    GLUCOSE_MEASUREMENT(R.string.glucose_measurement,0),
    GLUCOSE_MEASUREMENT_CONTEXT(R.string.glucose_measurement_context,0),
    GPX_DISTANCE(R.string.gpx_distance, 1, UnitType.DISTANCE_UNIT) {
        @Override public Unit getUnit() {
            return Unit.KM;
        }
    },                        // m
    GPX_DISTANCE_TO_DO(R.string.gpx_distance_end_real, 1, UnitType.DISTANCE_UNIT) {
        @Override
        public Unit getUnit() {
            return Unit.KM;
        }

        @Override
        public boolean hasStats() {
            return false;
        }

        @Override
        public boolean isSharable() {
            return false;
        }

    },                        // m
    GUST_FACTOR(R.string.gust_factor,0),
    GYROSCOPE(R.string.gyroscope, UnitType.ANGLE3D_UNIT),
    HARDWARE_REVISION_STRING(R.string.hardware_revision_string,0){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }


    },
    HDOP(R.string.hdop, UnitType.RATIO_UNIT),
    HEART_RATE(R.string.heart_rate, UnitType.BEATS_UNIT) {
        @Override public double minInterval() { return 10.0; }
    },                        // bpm
    HEART_RATE_CONTROL_POINT(R.string.heart_rate_control_point,0),
    HEART_RATE_MAX(R.string.heart_rate_max,0),
    HEART_RATE_MEASUREMENT(R.string.heart_rate_measurement,0),
    HEART_RATE_ZONE(R.string.zone, UnitType.BEATS_UNIT),
    HEAT_INDEX(R.string.heat_index,0),
    HEIGHT(R.string.height,2, UnitType.DISTANCE_UNIT),
    HID_CONTROL_POINT(R.string.hid_control_point,0),
    HID_INFORMATION(R.string.hid_information,0),
    HIP_CIRCUMFERENCE(R.string.hip_circumference,0),
    HUMIDITY(R.string.humidity, R.string.humidity, 0, UnitType.RATIO_UNIT) {
        @Override
        public Unit getUnit() {
            return Unit.PERCENT;
        }
    },                            // ratio
    IBEACON_ADVERTISEMENT(R.string.ibeacon_advertisment, 0){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }


    },
    IBEACON_MAJOR(R.string.ibeacon_major, 0, UnitType.INT_WITH_NO_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }


    },
    IBEACON_MINOR(R.string.ibeacon_minor, 0, UnitType.INT_WITH_NO_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }


    },
    IBEACON_UUID(R.string.ibeacon_uuid, 0, UnitType.TEXT_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    IBEACON_POWER(R.string.ibeacon_power, 0, UnitType.REFERENCED_POWER_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    IEEE_CERTIFICATION(R.string.ieee_11073_20601_regulatory_certification_data_list,0),
    INFO(R.string.textInfo, 0, UnitType.TEXT_UNIT),
    INTERMEDIATE_CUFF_PRESSURE(R.string.intermediate_cuff_pressure,0),
    IRRADIANCE(R.string.irradiance,0),
    KEY(R.string.key, UnitType.COUNT_UNIT) {},
    LANGUAGE(R.string.language,0),
    LAST_NAME(R.string.last_name,0),
    LEVEL(R.string.level, UnitType.COUNT_UNIT) {},
    LIGHT(R.string.light, UnitType.LIGHT_UNIT),
    LN_CONTROL_POINT(R.string.ln_control_point,0),
    LN_FEATURE(R.string.ln_feature,0),
    LOCAL_TIME_INFORMATION(R.string.local_time_information,0),
    LOCATION(R.string.location, 4, UnitType.ANGLE_UNIT) {
        public boolean isMapType() {
            return true;
        }
    },                            // degrees
    LOCATION_NMEA(R.string.locationNmea, 4, UnitType.ANGLE_UNIT) {
        public boolean isMapType() {
            return true;
        }
    },                            // degrees
    LOCATION_AND_SPEED(R.string.location_and_speed,0),
    MAGNETIC_DECLINATION(R.string.magnetic_declination,0),
    MAGNETIC_FLUX_DENSITY_2D(R.string.magnetic_flux_density_2d,0),
    MAGNETIC_FLUX_DENSITY_3D(R.string.magnetic_flux_density_3d,0,UnitType.MAGNETIC_FIELD_UNIT),
    MANUFACTURER_NAME_STRING(R.string.manufacturer_name_string,0){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    MANUFACTURER_ID(R.string.manufacturer_id,0) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    MAXIMUM_RECOMMENDED_HEART_RATE(R.string.maximum_recommended_heart_rate,0),
    MEASUREMENT_INTERVAL(R.string.measurement_interval,0),
    MEMORY(R.string.allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_FREE(R.string.free_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_GC_ALLOCATED(R.string.gc_allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_GC_FREED(R.string.gc_freed_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_GC(R.string.gc_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_HEAP_ALLOCATED(R.string.heap_allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_TOTAL(R.string.total_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_MAX(R.string.max_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_PSS(R.string.pss_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MEMORY_RSS(R.string.rss_memory, 0, UnitType.DIGITAL_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MEGA_BYTE;
        }
    },
    MODEL_NUMBER_STRING(R.string.model_number_string, UnitType.TEXT_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    MUSCLE_RATIO(R.string.muscle_ratio, 2, UnitType.DOUBLE_WITH_NO_UNIT),
    NAVIGATION(R.string.navigation,0),
    NEW_ALERT(R.string.new_alert,0),
    NMEA(R.string.nmea, UnitType.TEXT_UNIT),
    ORIENTATION(R.string.orientation, 1, UnitType.ANGLE3D_UNIT),
    PEDAL_SMOOTHNESS(R.string.pedal_smoothness, 2, UnitType.RATIO_UNIT) {
        @Override public Unit getUnit() {
            return Unit.PERCENT;
        }
    },                        // ratio
    PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS(R.string.peripheral_preferred_connection_parameters,0){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    PERIPHERAL_PRIVACY_FLAG(R.string.peripheral_privacy_flag,0),
    PITCH(R.string.pitch, UnitType.ANGLE_UNIT),
    PNP_ID(R.string.pnp_id,0),
    POLLEN_CONCENTRATION(R.string.pollen_concentration,0),
    POSITION_QUALITY(R.string.position_quality, UnitType.COUNT_UNIT),
    POWER(R.string.power, UnitType.POWER_UNIT),                                    // W
    POWER_INSTANT(R.string.power_instant, UnitType.POWER_UNIT),
    POWER_ACCUMULATED(R.string.power_accumulated, UnitType.POWER_UNIT) {
        @Override
        public boolean isGraphable() {
            return false;
        }

        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },                                    // W
    POWER_PEDAL(R.string.power_pedal, 2, UnitType.RATIO_UNIT) {
        @Override public Unit getUnit() {
            return Unit.PERCENT;
        }
    },                        // ratio,                        // ratio
    PRESSURE(R.string.pressure,0, UnitType.PRESSURE_UNIT),
    PROTEIN(R.string.protein,2, UnitType.DOUBLE_WITH_NO_UNIT),
    PROTOCOL_MODE(R.string.protocol_mode,0),
    PROXIMITY(R.string.proximity, UnitType.DISTANCE_UNIT),
    PULSE_DATA(R.string.pulseData, 0, UnitType.INT_WITH_NO_UNIT),
    PULSE_WIDTH(R.string.pulsewidth, UnitType.PULSE_WIDTH_UNIT),
    RADBEACON_DEVICE_NAME(R.string.radbeacon_device_name, UnitType.TEXT_UNIT),
    RAINFALL(R.string.rainfall,0),
    RECONNECTION_ADDRESS(R.string.reconnection_address,0),
    RECORD_ACCESS_CONTROL_POINT(R.string.record_access_control_point,0),
    REFERENCE_TIME_INFORMATION(R.string.reference_time_information,0),
    REPORT(R.string.report,0),
    REPORT_MAP(R.string.report_map,0),
    RESTING_HEART_RATE(R.string.resting_heart_rate,0),
    RHO(R.string.rho, 3, UnitType.DENSITY_UNIT),
    RINGER_CONTROL_POINT(R.string.ringer_control_point,0),
    RINGER_SETTING(R.string.ringer_setting,0),
    ROLL(R.string.roll, UnitType.ANGLE_UNIT),
    ROTATION(R.string.rotation, 1, UnitType.ANGLE_UNIT),
    ROTATION_MAX(R.string.rotation_max, 1, UnitType.ANGLE_UNIT),
    ROTATION_MIN(R.string.rotation_min, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO(R.string.rotation_accelero, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO_MAX(R.string.rotation_accelero_max, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO_MIN(R.string.rotation_accelero_min, 1, UnitType.ANGLE_UNIT),
    ROTATION_RAW(R.string.rotation_raw, 1, UnitType.ANGLE_UNIT),
    ROTATION_SPEED(R.string.rotation_speed, 1, UnitType.ROTATIONSPEED_UNIT),
    ROTATION_TOTAL(R.string.rotationtotal, 0, UnitType.ANGLE_UNIT),
    RSC_FEATURE(R.string.rsc_feature,0),
    RSC_MEASUREMENT(R.string.rsc_measurement,0),
    RSSI(R.string.rssi, UnitType.REFERENCED_POWER_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }
    },
    RR_INTERVAL(R.string.interval, UnitType.SECOND_TIME_UNIT) {
        @Override public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    SATELLITES_USED(R.string.satellites_used, UnitType.INT_WITH_NO_UNIT) {
        @Override public boolean isSharable() {
            return false;
        }
    },                        // #
    SATELLITES_FIX(R.string.satellites_fix, UnitType.INT_WITH_NO_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }

        @Override public boolean isSharable() {
            return false;
        }
    },                        // #
    SATELLITES_EVENT(R.string.satellites_event, UnitType.INT_WITH_NO_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }

        @Override public boolean isSharable() {
            return false;
        }
    },                        // #,                        // #
    SATELLITES_VISIBLE(R.string.satellites_visible, UnitType.INT_WITH_NO_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }

        @Override public boolean isSharable() {
            return false;
        }
    },                        // #,                        // #
    SATELLITES_STATUS(R.string.satellites, UnitType.INT_WITH_NO_UNIT) {
        @Override public boolean isSharable() {
            return false;
        }
    },                        // #
    SC_CONTROL_POINT(R.string.sc_control_point,0),
    SCAN_INTERVAL_WINDOW(R.string.scan_interval_window,0),
    SCAN_REFRESH(R.string.scan_refresh,0),
    SENSOR_LOCATION(R.string.sensor_location,0),
    SERIAL_NUMBER_STRING(R.string.serial_number_string, UnitType.TEXT_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }
    },
    SERVICE_CHANGED(R.string.service_changed,0),
    SLOPE(R.string.slope, UnitType.RATIO_UNIT) {
        @Override public Unit getUnit() {
            return Unit.PERCENT;
        }
    },
    SOAPSENSE_PRESSED_COUNT(R.string.pressed_count, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_USED_COUNT(R.string.used_count, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_PARTIAL_STROKES_REFILL(R.string.partialStrokesSinceRefill, UnitType.INT_WITH_NO_UNIT){
        @Override
        public boolean isShareAlways() {
            return true;
        }
    },
    SOAPSENSE_PARTIAL_STROKES_REFILL_2(R.string.partialStrokesSinceRefill, UnitType.INT_WITH_NO_UNIT){
        @Override
        public boolean isShareAlways() {
            return true;
        }
    },
    SOAPSENSE_TOTAL_STROKES(R.string.totalStrokes, UnitType.INT_WITH_NO_UNIT),
    //SOAPSENSE_FULL_STROKES_REFILL(R.string.strokes_since_refill, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_REFILLS(R.string.refills, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_USED_VOLUME(R.string.usedVolume, 1, UnitType.VOLUME_UNIT){
        @Override
        public boolean isShareAlways() {
            return true;
        }
    },
    SOAPSENSE_USED_VOLUME_ACCELERO(R.string.usedVolume, 1, UnitType.VOLUME_UNIT){
        @Override
        public boolean isShareAlways() {
            return true;
        }
    },
    SOAPSENSE_TIME_SINCE_REFILL(R.string.timeSinceRefill, UnitType.SECOND_TIME_UNIT){
        @Override
        public Unit getUnit() {
            return Unit.HOURSECOND; }
    },
    SOAPSENSE_MIN_ANGLE(R.string.minAngle, 1, UnitType.ANGLE_UNIT),
    SOAPSENSE_EMPTY_TIME(R.string.emptytime, UnitType.DATETIME_UNIT) {
        @Override
        public Unit getUnit() {
            return Unit.SHORTDATE; }
    },
    SOAPSENSE_VOLUME_P_PUSH(R.string.volume_push, 1, UnitType.VOLUME_UNIT) {
        @Override
        public boolean isShareAlways() {
            return true;
        }
    },
    SOAPSENSE_VOLUME_P_USER(R.string.volume_user, 1, UnitType.VOLUME_UNIT),
    SOAPSENSE_PUSHES_P_USER(R.string.pushes_user, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_PUSHES_P_REFILL(R.string.pushes_refill, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_REFILLS_P_YEAR(R.string.refills_year, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_REFILL_COMMAND(R.string.refillCommand, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_ROTATION_SPEED_OFFSET(R.string.rotation_speed_offset, 1, UnitType.ROTATIONSPEED_UNIT),
    SOAPSENSE_CALIBRATION_RESULT(R.string.calibration_result, UnitType.INT_WITH_NO_UNIT),
    SOAPSENSE_STATE(R.string.state, UnitType.INT_WITH_NO_UNIT),
    SOFTWARE_REVISION_STRING(R.string.software_revision_string, UnitType.TEXT_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    SPEED(R.string.speed, 1, UnitType.SPEED_UNIT) {
        @Override public double minInterval() { return 5.0 / 3.6; }
    },                                    // m/s
    SPEED_AVG(R.string.speed_avg, R.string.speed_avg_voice, 1, UnitType.SPEED_UNIT) {
        @Override public boolean hasStats() {
            return false;
        }
    },                                    // m/s
    SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS(R.string.sport_type_for_aerobic_and_anaerobic_thresholds,0),
    STANDARD_DEVIATION(R.string.standardDeviation, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    STANDARD_DEVIATION_2(R.string.standardDeviation2, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    STATE(R.string.state, UnitType.TEXT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }
    },
    STEPS(R.string.steps, UnitType.STEP_UNIT) {
        @Override public double minInterval() { return 500.0; }

        @Override public boolean hasStats() {
            return false;
        }

        @Override public boolean isAggregate() {
            return true;
        }
    },
    STEPS_TOTAL(R.string.steps_total, UnitType.STEP_UNIT) {
        @Override public double minInterval() { return 1000.0; }
        @Override public boolean hasStats() {
            return false;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    STEP_SPEED(R.string.step_speed, UnitType.CYCLES_UNIT) {

    },
    SUPPORTED_NEW_ALERT_CATEGORY(R.string.supported_new_alert_category,0),
    SUPPORTED_UNREAD_ALERT_CATEGORY(R.string.supported_unread_alert_category,0),
    SYSTEM_ID(R.string.system_id,0),
    TEMPERATURE(R.string.temperature, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_2(R.string.temperature2, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_INTERMEDIATE(R.string.temperature_intermediate, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_IR(R.string.temperature_ir, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_OBJECT(R.string.temperature_object, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_TYPE(R.string.temperature_type,0, UnitType.TEMPERATURE_UNIT),
    THREE_ZONE_HEART_RATE_LIMITS(R.string.three_zone_heart_rate_limits,0),
    TI_TEMPERATURE_CONFIG(R.string.ti_temperature_config, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public boolean isGraphable() {
            return false;
        }
    },
    TI_TEMPERATURE_PERIOD(R.string.ti_temperature_period, UnitType.SECOND_TIME_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public boolean isGraphable() {
            return false;
        }

        @Override
        public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    TI_LIGHT_CONFIG(R.string.ti_light_config, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public boolean isGraphable() {
            return false;
        }
    },
    TI_LIGHT_PERIOD(R.string.ti_light_period, UnitType.SECOND_TIME_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    TI_HUMIDITY_CONFIG(R.string.ti_humidity_config, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_HUMIDITY_PERIOD(R.string.ti_humidity_period, UnitType.SECOND_TIME_UNIT) {

        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    TI_IO_DATA(R.string.ti_io, UnitType.HEXCOUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_IO_CONFIG(R.string.ti_io_config, UnitType.TEXT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_LEFT_KEY(R.string.ti_left_key, UnitType.HEXCOUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_RIGHT_KEY(R.string.ti_right_key, UnitType.HEXCOUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_REED_RELAY(R.string.ti_reed_relay, UnitType.HEXCOUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_BAROMETER_CONFIG(R.string.ti_barometer_config, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_BAROMETER_PERIOD(R.string.ti_barometer_period, UnitType.SECOND_TIME_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

        @Override
        public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    TI_MOVEMENT_CONFIG_GYRO_ON(R.string.ti_movement_config_gyro, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_MOVEMENT_CONFIG_ACCELEROMETER_ON(R.string.ti_movement_config_accelerometer, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_MOVEMENT_CONFIG_MAGNETOMETER_ON(R.string.ti_movement_config_magnetometer, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_MOVEMENT_CONFIG_WAKE_ON_MOTION(R.string.ti_movement_config_wake_on_motion, UnitType.COUNT_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE(R.string.ti_movement_config_accelerometer_range, UnitType.ACCELERATION_UNIT) {
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    TI_MOVEMENT_PERIOD(R.string.ti_movement_period, UnitType.SECOND_TIME_UNIT) {
        @Override public boolean isSupported() {
            return Globals.verbose;
        }
        @Override public Unit getUnit() {
            return Unit.MILLISECOND;
        }
    },
    TIME_CLOCK(R.string.time, UnitType.DATETIME_UNIT) {
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SHORTTIME;
        }
    },                                // milliseconds
    TIME_GPS(R.string.timeGps, UnitType.DATETIME_UNIT) {
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SHORTTIME;
        }
    },                                // milliseconds
    TIME_TOTAL(R.string.totalTime, UnitType.SECOND_TIME_UNIT) {
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SECOND;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    TIME_ACCURACY(R.string.time_accuracy,0),
    TIME_ACTIVITY(R.string.time_active, UnitType.SECOND_TIME_UNIT) {
        @Override public double minInterval() { return 300000; }
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SECOND;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    TIME_ACTIVE(R.string.time_moving, UnitType.SECOND_TIME_UNIT) {
        @Override public double minInterval() { return 300000; }
        @Override public boolean isGraphable() {
            return false;
        }
        @Override public Unit getUnit() {
            return Unit.SECOND;
        }
        @Override public boolean isAggregate() {
            return true;
        }
    },
    TIME_SOURCE(R.string.time_source,0),
    TIME_UPDATE_CONTROL_POINT(R.string.time_update_control_point,0),
    TIME_UPDATE_STATE(R.string.time_update_state,0),
    TIME_WITH_DST(R.string.time_with_dst,0),
    TIME_ZONE(R.string.time_zone,0),
    TRUE_WIND_DIRECTION(R.string.true_wind_direction,0),
    TRUE_WIND_SPEED(R.string.true_wind_speed,0),
    TWO_ZONE_HEART_RATE_LIMIT(R.string.two_zone_heart_rate_limit,0),
    TX_POWER_LEVEL(R.string.tx_power_level,0),
    TYPE_MAJOR_STRING(R.string.type_major, UnitType.TEXT_UNIT){
        @Override
        public boolean isSupported() {
            return Globals.verbose;
        }

    },
    UNREAD_ALERT_STATUS(R.string.unread_alert_status,0),
    USER_CONTROL_POINT(R.string.user_control_point,0),
    USER_INDEX(R.string.user_index,0),
    USER_NAME(R.string.user_name, UnitType.TEXT_UNIT),
    UV_INDEX(R.string.uv_index,0),
    VERTICAL_SPEED(R.string.vertspeed, UnitType.SPEED_UNIT),
    VISCERAL_FAT_AREA(R.string.vfa, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    VISCERAL_FAT_INDEX(R.string.vfi, UnitType.RATIO_UNIT),
    VO2_MAX(R.string.vo2_max,0),
    VOLTAGE(R.string.voltage, UnitType.VOLTAGE_UNIT),                            // V
    WAIST_CIRCUMFERENCE(R.string.waist_circumference,0),
    WAIST_HIP_RATIO(R.string.waist_hip_ratio,2, UnitType.RATIO_UNIT),
    WEIGHT(R.string.weight, 2, UnitType.WEIGHT_UNIT),
    WEIGHT_MEASUREMENT(R.string.weight_measurement,0),
    WEIGHT_SCALE_FEATURE(R.string.weight_scale_feature,0),
    WHEEL_REVOLUTIONS(R.string.wheelrevolutions, 0, UnitType.CYCLES_UNIT),
    WHEEL_TORQUE(R.string.wheel_torque, UnitType.TORQUE_UNIT),
    WHEEL_CIRCUMFERENCE(R.string.wheel_circumference, 3, UnitType.DISTANCE_UNIT) {
        @Override
        public Unit getUnit() {
            return Unit.METER;
        }
    },
    WIND_CHILL(R.string.wind_chill,0),
    WIND_DIRECTION(R.string.winddirection, UnitType.ANGLE_UNIT),                            // degrees
    WIND_SPEED(R.string.windspeed, 1, UnitType.WIND_UNIT),                            // m/s
    UNKNOWN(R.string.unknown, 0),
    ;

    final int label;
    final int voiceLabel;
    final int decimals;
    final UnitType unitType;
    final Unit defaultUnit;
//    static EnumMap<DataType, Unit> units = new EnumMap<>(DataType.class);

    DataType(final int label, int decimals) {
        this(label, label, decimals, UnitType.INT_WITH_NO_UNIT);
    }

    DataType(final int label, UnitType unitType) {
        this(label, label, 0, unitType);
    }

    DataType(final int label, final int decimals, UnitType unitType) {
        this(label, label, decimals, unitType);
    }

    DataType(final int label, final int voiceLabel, int decimals, UnitType unitType) {
        this.label = label;
        this.voiceLabel = voiceLabel;
        this.decimals = decimals;
        this.unitType = unitType;
        defaultUnit = unitType.defaultUnit();
    }

    public String toProperString() {
        Context appContext = Globals.getAppContext();
        if (appContext != null) return appContext.getString(label);
        return "";
    }

    public static DataType fromString(String value) {
        for (DataType type : DataType.values())
            if (type.name().equals(value)) return type;
        return null;
    }

    public static DataType fromInt(int value) {
        if (value < 0 || value >= DataType.values().length) return null;
        return DataType.values()[value];
    }

    public Unit getUnit() {
        return defaultUnit;
    }

    @Override
    public String toString() {
        Context appContext = Globals.getAppContext();
        if (appContext != null) return appContext.getString(label);
        return "";
    }

    public String toString(double value) {
        Context appContext = Globals.getAppContext();
        if (appContext != null) return appContext.getString(label) + " = " + toValueString(value, true, false);
        return "";
    }

    public String toString(int value) {
        Context appContext = Globals.getAppContext();         if (appContext == null) return "";
        switch (this.unitType) {
            case HEXCOUNT_UNIT: return appContext.getString(label) + " = " + StringUtil.toHexString(value);
            default: return appContext.getString(label) + " = " + toValueString(value, true, false);
        }
    }

    public String toString(String value) {
        Context appContext = Globals.getAppContext();         if (appContext == null) return "";
        return appContext.getString(label) + " = " + value;
    }

    public String toString(float[] value) {
        MathVector v = new MathVector(value, 3);
        Context appContext = Globals.getAppContext();         if (appContext == null) return "";
        return appContext.getString(label) + " = " + v.toString(decimals) +  " " + getUnit().toString();
    }

    public String toValueString(float[] value) {
        MathVector v = new MathVector(value, 3);
        return v.toString(decimals);
    }

    public String toString(double[] value) {
        MathVector v = new MathVector(value);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";

        return appContext.getString(label) + " = " + v.toString(decimals) +  " " + getUnit().toString();
    }

    public String toValueString(double[] value) {
        MathVector v = new MathVector(value);
        return v.toString(decimals);
    }

    public String toString(long value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        return appContext.getString(label) + " = " + toValueString(value, true, false);
    }

    public String toVoiceString(double value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        return appContext.getString(voiceLabel) + " " + toValueString(value, true, true);
    }

    public String toVoiceString(int value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        switch (this.unitType) {
            case HEXCOUNT_UNIT: return appContext.getString(voiceLabel) + " " + StringUtil.toHexString(value);
            default: return appContext.getString(voiceLabel) + " " + toValueString(value, true, true);
        }
    }

    public String toVoiceString(String value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        return appContext.getString(voiceLabel) +  " " + value;
    }

    public String toVoiceString(float[] value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        MathVector v = new MathVector(value, 3);
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) +  " " + getUnit().toVoiceString();
    }

    public String toVoiceString(double[] value) {
        MathVector v = new MathVector(value);
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) +  " " + getUnit().toVoiceString();
    }

    public String toVoiceString(long value) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return "";
        return appContext.getString(voiceLabel) + " " + toValueString(value, true, true);
    }


    public String toValueString(double value, boolean withUnit, boolean isSpeech) {
        String[] ret = toValueString(isSpeech, value);
        if (ret == null || ret.length == 0) return "";
        if (ret.length == 1 || !withUnit) return ret[0];
        return ret[0] + " " + ret[1];
    }

    public String toValueString(float value, boolean withUnit, boolean isSpeech) {
        String[] ret = toValueString(isSpeech, value);
        if (ret == null || ret.length == 0) return "";
        if (ret.length == 1 || !withUnit) return ret[0];
        return ret[0] + " " + ret[1];
    }

    public String toValueString(long value, boolean withUnit, boolean isSpeech) {
        String[] ret = toValueString(isSpeech, value);
        if (ret == null || ret.length == 0) return "";
        if (ret.length == 1 || !withUnit) return ret[0];
        return ret[0] + " " + ret[1];
    }

    public String toValueString(int value, boolean withUnit, boolean isSpeech) {
        String[] ret = toValueString(isSpeech, value);
        if (ret == null || ret.length == 0) return "";
        if (ret.length == 1 || !withUnit) return ret[0];
        return ret[0] + " " + ret[1];
    }

    public String[] toValueString(boolean isSpeech, double value) {
        if (isSpeech)
            return new String[] {Format.format(getUnit().scale(value), decimals, Locale.getDefault()),getUnit().toVoiceString()};
        return new String[] {Format.format(getUnit().scale(value), decimals),getUnit().toString()};
    }

    public String[] toValueString(boolean isSpeech, float value) {
        if (isSpeech)
            return new String[] {Format.format(getUnit().scale(value), decimals, Locale.getDefault()), getUnit().toVoiceString()};
        return new String[] {Format.format(getUnit().scale(value), decimals), getUnit().toString()};
    }

    public String[] toValueString(boolean isSpeech, long value) {
        Unit unit = getUnit();
        switch (unit.unitType) {
            case SECOND_TIME_UNIT:
                switch (unit) {
                    case MILLISECOND:
                        if (isSpeech) return new String[] {TimeUtil.formatElapsedMilliSpoken(value, true)};
                        return new String[] {TimeUtil.formatElapsedMilli(value, false)};
                    case TENTHSECOND:
                        if (isSpeech) return new String[] {TimeUtil.formatElapsedTenthsSpoken(value, true)};
                        return new String[] {TimeUtil.formatElapsedTenths(value, false)};
                    case SECOND:
                        if (isSpeech) return new String[] {TimeUtil.formatElapsedSecondsSpoken(value, true)};
                        return new String[] {TimeUtil.formatElapsedSeconds(value, false)};
                    case HOURSECOND:
                        if (isSpeech) return new String[] {TimeUtil.formatElapsedHourSecondsSpoken(value, true)};
                        return new String[] {TimeUtil.formatElapsedHourSeconds(value, false)};
                }
                break;
            case DATETIME_UNIT:
                switch(unit) {
                    default:
                    case DATETIME:
                    case LONGDATE:
                        return new String[] {TimeUtil.formatTime(value, "dd MMM yyyy HH:mm")};
                    case TIME:
                        return new String[] {TimeUtil.formatTime(value, "HH:mm:ss")};
                    case SHORTTIME:
                        return new String[] {TimeUtil.formatTime(value, "HH:mm")};
                    case SHORTDATE:
                        if(Math.abs(value - System.currentTimeMillis()) > 31557600000L)
                            return new String[] {TimeUtil.formatTime(value, "MMM yy")};
                        else if (Math.abs(value - System.currentTimeMillis()) > 2 * 86400000L)
                            return new String[] {TimeUtil.formatTime(value, "dd MMM")};
                        return new String[] {TimeUtil.formatTime(value, "EEE HH:mm")};
                }
        }
        if (isSpeech)
            return new String[] {"" + getUnit().scale(value), getUnit().toVoiceString()};
        return new String[] {"" + getUnit().scale(value), getUnit().toString()};
    }

    public String[] toValueString(boolean isSpeech, int value) {
        switch (this.unitType) {
            case HEXCOUNT_UNIT: return new String[] {StringUtil.toHexString(value)};
            default:
                if (isSpeech)
                    return new String[] {"" + getUnit().scale(value), getUnit().toVoiceString()};
                return new String[] {"" + getUnit().scale(value), getUnit().toString()};
        }
    }

    public JSONObject toJSON(Object value, String sensorId) {
        JSONObject jValue = new JSONObject();
        try {
            jValue.put(JSoNField.DATATYPE.toString(), name().toLowerCase());
            JSoNHelper.set(jValue, JSoNField.VALUE, value);
            jValue.put(JSoNField.CLASS.toString(), value.getClass().getSimpleName());
            jValue.put(JSoNField.UNIT.toString(), unitType.metricUnit());
            if (sensorId != null) jValue.put(JSoNField.SENSORID.toString(), sensorId);

        } catch (JSONException e) {
            LLog.e("Could not create JSON", e);
        }
        return jValue;
    }

//    public void toShortJSON(JSONObject jObj, Object value, String sensorId) {
//        if (sensorId != null && sensorId.length() > 0)
//            JSoNHelper.set(jObj, JSoNField.DATATYPE.name() + "@" + sensorId, value);
//        else
//            JSoNHelper.set(jObj, JSoNField.DATATYPE, value);
//    }

    public static DataType fromJSONString(String value) {
        for (DataType type : DataType.values())
            if (type.name().equalsIgnoreCase(value)) return type;
        return null;
    }

    public boolean isGraphable() {
        switch (unitType) {
            case DATETIME_UNIT:
            case SECOND_TIME_UNIT:
            case TEXT_UNIT:
                return false;
            default:
                return true;
        }

    }

    public boolean isShareAlways() {
        return false;
    }

    public boolean isSharable() {
        return true;
    }

    public boolean isAggregate() {
        return false;
    }

    public boolean hasMin() {
        return false;
    }

    public boolean hasAvg() {
        return hasStats();
    }

    public boolean hasMax() {
        return hasStats();
    }

    public boolean hasStats() {
        return isGraphable() && !isAggregate() && PreferenceKey.STATS.isTrue();
    }

    public boolean isSupported() {
        return true;
    }

    public boolean isMapType() {
        return false;
    }

    public double floatAvgFactor() {
        return 0.2;
    }

    public double minInterval() { return 1.0; }

}
