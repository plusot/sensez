package com.plusot.util.data;

/**
 * Created by peet on 2/11/16.
 */

public enum Gender {
    MALE,
    FEMALE;

    public static Gender fromOrdinal(int ord) {
            return Gender.values()[ord % Gender.values().length];
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static String[] getStrings() {
        String[] strings = new String[values().length];
        int i = 0;
        for (Gender gender: values()) {
            strings[i++] = gender.toString();
        }
        return strings;
    }
}
