package com.plusot.util.data;


import com.plusot.util.json.JSoNField;
import com.plusot.util.json.JSoNHelper;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.Format;
import com.plusot.util.util.MathVector;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Stringer;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.Tuple;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Data {
    private EnumMap<DataType, Map<String, Tuple<Object, Long>>> values = new EnumMap<>(DataType.class);
    private static final EnumMap<DataType, Map<String, List<Object>>> history = new EnumMap<>(DataType.class);
    private long time;

    public interface Checker {
        boolean isChecked(DataType type);
    }

    public Data() {
        this(System.currentTimeMillis());
    }

    public Data(long time) {
        this.time = time;
    }

    public boolean hasData() {
        return values.size() > 0;
    }

    public int length() {
        return values.size();
    }

    public Data add(DataType type) {
        if (values.get(type) == null) values.put(type, new HashMap<String, Tuple<Object, Long>>());
        return this;
    }

    private Data setValue(String sensorId, DataType type, Object value, Long time) {
        return setValue(sensorId, type, new Tuple<>(value, time));
    }

    private Data setValue(String sensorId, DataType type, Tuple<Object, Long> valueTime) {
        if (valueTime == null) return this;
        Map<String, Tuple<Object, Long>> map;
        if ((map = values.get(type)) == null) values.put(type, map = new HashMap<>());
        map.put(sensorId, valueTime);
        return this;
    }

    public Tuple<Object, Long> getValueTime(String sensorId, DataType type) {
        Map<String, Tuple<Object, Long>> map;
        if ((map = values.get(type)) == null) return null;
        if (sensorId != null) {
            Tuple<Object, Long> t = map.get(sensorId);
            if (t != null)  {
                return new Tuple<>(clone(t.t1()), t.t2());
            }
        }
        if (map.values().size() > 0) for (Tuple<Object, Long> t : map.values()) {
            if (t != null) return new Tuple<>(clone(t.t1()), t.t2());
        }
        return null;
    }

    private Object getValue(String sensorId, DataType type) {
        Tuple<Object, Long> t = getValueTime(sensorId, type);
        if (t != null) return t.t1();
        return null;
    }

    public Map<String, Data> split(SensorIdTranslator translator) {
        Map<String, Data> splitData = new HashMap<>();
        Map<String, Tuple<Object, Long>> map;
        for (DataType type: values.keySet()) {
            map = values.get(type);
            if (map != null) for (String sensorId: map.keySet()) {
                String translatedSensorId = translator.translate(sensorId);
                Data data;
                if ((data = splitData.get(translatedSensorId)) == null) splitData.put(translatedSensorId, data = new Data(/*sensorId, */time));
                data.setValue(translatedSensorId, type, getValueTime(sensorId, type));
            }
        }
        return splitData;
    }

    public static Data fromDataTypes(/*String sensorId, */DataType[] types) {
        if (types == null || types.length == 0) return null;
        return new Data(/*sensorId*/).add(types);
    }

    public Data add(DataType[] types) {
        if (types == null || types.length == 0) return this;
        for (DataType type : types) add(type);
        return this;
    }

    public Data add(String sensorId, DataType type, String value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, Double value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, Float value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, Integer value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, Integer value, boolean add2History) {
        return _add(sensorId, type, value, System.currentTimeMillis(), add2History);
    }

    public Data add(String sensorId, DataType type, Long value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, byte[] value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, int[] value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, int[] value, boolean add2History) {
        return _add(sensorId, type, value, System.currentTimeMillis(), add2History);
    }

    public Data add(String sensorId, DataType type, float[] value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, double[] value) {
        return _add(sensorId, type, value, time, false);
    }

    public Data add(String sensorId, DataType type, Tuple<Object, Long> valueTime) {
        if (valueTime == null) return null;
        return _add(sensorId, type, valueTime.t1(), valueTime.t2(), false);
    }

    public Data add(String sensorId, DataType type, Double value, long time) {
        return _add(sensorId, type, value, time, false);
    }

    private Data _add(String sensorId, DataType type, Object value, long time, boolean add2History) {
        //if (!type.isSupported()) return this;
        if (add2History) synchronized (history) {
            List<Object> typeHistory;
            Map<String, List<Object>> map;
            if ((map = history.get(type)) == null) history.put(type, map = new HashMap<>());
            if ((typeHistory = map.get(sensorId)) == null) map.put(sensorId, typeHistory = new ArrayList<>());
            typeHistory.add(value);
        }
        setValue(sensorId, type, value, time);
        this.time = time; //Math.max(this.time, time);
        return this;
    }

    public DataType[] getDataTypes() {
        return values.keySet().toArray(new DataType[values.keySet().size()]);
    }

//    public DataType[] getSupportedDataTypes() {
//        List<DataType> supported = new ArrayList<>();
//        for (DataType type: values.keySet()) {
//            if (type.isSupported()) supported.add(type);
//        }
//        return supported.toArray(new DataType[supported.size()]);
//    }

    public EnumSet<DataType> getSupportedDataTypes() {
        EnumSet<DataType> supported = EnumSet.noneOf(DataType.class);
        for (DataType type: values.keySet()) {
            if (type.isSupported()) supported.add(type);
        }
        return supported;
    }

    public String[] getSensorIds(DataType type) {
        Map<String, Tuple<Object, Long>> map = values.get(type);
        if (map == null) return null;
        return map.keySet().toArray(new String[map.size()]);
    }

    public boolean hasDataType(DataType dataType) {
        return values.keySet().contains(dataType) && values.get(dataType) != null;
    }

    private static double _getDouble(Object obj) {
        if (obj == null) return Double.NaN;
        if (obj instanceof Double) return (Double) obj;
        if (obj instanceof Integer) return ((Integer) obj).doubleValue();
        if (obj instanceof Long) return ((Long) obj).doubleValue();
        if (obj instanceof Float) return ((Float) obj).doubleValue();
        if (obj instanceof String) try {
            return Double.valueOf((String) obj);
        } catch (NumberFormatException e) {
            return Double.NaN;
        }
        if (obj instanceof float[]) {
            MathVector v = new MathVector((float[]) obj, 3);
            return v.length();
        }
        if (obj instanceof int[]) {
            int[] ints = ((int[]) obj);
            if (ints.length > 0) return 1.0 * ints[0];
            return 0;
        }
        if (obj instanceof byte[]) {
            byte[] bytes = ((byte[]) obj);
            if (bytes.length > 0) return 1.0 * bytes[0];
            return 0;
        }

        if (obj instanceof double[]) {
            MathVector v = new MathVector((double[]) obj);
            return v.length();
        }

        return Double.NaN;
    }

    @SuppressWarnings({"UnnecessaryBoxing", "BoxingBoxedValue"})
    private static Object clone (Object obj) {
        if (obj == null) return null;
        if (obj instanceof Double) return Double.valueOf(((Double) obj));
        if (obj instanceof Integer) return Integer.valueOf(((Integer) obj));
        if (obj instanceof Long) return Long.valueOf(((Long) obj));
        if (obj instanceof Float) return Float.valueOf(((Float) obj));
        if (obj instanceof String) return obj.toString() + "";
        if (obj instanceof float[]) return Arrays.copyOf((float[]) obj, ((float[]) obj).length);
        if (obj instanceof int[]) return Arrays.copyOf((int[]) obj, ((int[]) obj).length);
        if (obj instanceof byte[]) return Arrays.copyOf((byte[]) obj, ((byte[]) obj).length);
        if (obj instanceof double[]) return Arrays.copyOf((double[]) obj, ((double[]) obj).length);
        return obj;
    }

    public double getDouble(String sensorId, DataType type) {
        return _getDouble(getValue(sensorId, type));
    }

    private float getFloat(String sensorId, DataType type) {
        return (float) getDouble(sensorId, type);
    }

    public int getInt(String sensorId, DataType type, int defaultValue) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return defaultValue;
        if (obj instanceof Double) return ((Double) obj).intValue();
        if (obj instanceof Integer) return (Integer) obj;
        if (obj instanceof Long) return ((Long) obj).intValue();
        if (obj instanceof Float) return ((Float) obj).intValue();
        if (obj instanceof String) return Integer.valueOf((String) obj);
        if (obj instanceof float[]) {
            MathVector v = new MathVector((float[]) obj, 3);
            return (int) v.length();
        }

        if (obj instanceof double[]) {
            MathVector v = new MathVector((double[]) obj);
            return (int) v.length();
        }
        return defaultValue;
    }

    public Long getLong(String sensorId, DataType type) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return null;
        if (obj instanceof Double) return ((Double) obj).longValue();
        if (obj instanceof Integer) return ((Integer) obj).longValue();
        if (obj instanceof Long) return (Long) obj;
        if (obj instanceof Float) return ((Float) obj).longValue();
        if (obj instanceof String) return Long.valueOf((String) obj);
        if (obj instanceof float[]) {
            MathVector v = new MathVector((float[]) obj, 3);
            return (long) v.length();
        }

        if (obj instanceof double[]) {
            MathVector v = new MathVector((double[]) obj);
            return (long) v.length();
        }
        return null;
    }

    public String getString(String sensorId, DataType type) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return null;
        if (obj instanceof Double) return Format.format((Double) obj, type.decimals);
        if (obj instanceof Integer) return "" + obj;
        if (obj instanceof Long) return "" + obj;
        if (obj instanceof Float) return Format.format((Float) obj, type.decimals);
        if (obj instanceof String) return (String) obj;
        if (obj instanceof float[]) return StringUtil.toString((float[]) obj, ",", type.decimals);
        if (obj instanceof double[]) return StringUtil.toString((double[]) obj, ",", type.decimals);
        return null;
    }

    public double[] getDoubles(String sensorId, DataType type) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return new double[] {};
        if (obj instanceof Double) return new double[] {(Double) obj};
        if (obj instanceof Integer) return new double[] {((Integer) obj).doubleValue()};
        if (obj instanceof Long) return new double[] {((Long) obj).doubleValue()};
        if (obj instanceof Float) return new double[] {((Float) obj).doubleValue()};
        if (obj instanceof String) return new double[] {Double.valueOf((String) obj)};
        if (obj instanceof float[]) {
            double[] ret = new double[((float[]) obj).length];
            for (int i = 0; i < ret.length; i++) ret[i] = (double)((float[]) obj)[i];
            return ret;
        }

        if (obj instanceof int[]) {
            double[] ret = new double[((int[]) obj).length];
            for (int i = 0; i < ret.length; i++) ret[i] = (double)((int[]) obj)[i];
            return ret;
        }

        if (obj instanceof double[]) {
            return (double[]) obj;
        }

        return new double[] {};
    }

    public String getStringWithoutLabel(String sensorId, DataType type) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return null;
        if (obj instanceof Double) return type.toValueString((Double) obj, false, false);
        if (obj instanceof Integer) return type.toValueString((Integer) obj, false, false);
        if (obj instanceof Long) return type.toValueString((Long) obj, false, false);
        if (obj instanceof Float) return type.toValueString((Float) obj, false, false);
        if (obj instanceof String && !obj.equals("")) return (String) obj;
        if (obj instanceof float[]) return type.toValueString((float[]) obj);
        if (obj instanceof double[]) return type.toValueString((double[]) obj);
        return null;
    }

    public Boolean greater(String sensorId, DataType type, Data otherData) {
        Object obj = getValue(sensorId, type);
        if (obj == null || otherData == null) return null;
        if (!otherData.hasDataType(type)) return null;
        if (obj instanceof Double) return ((Double) obj) > otherData.getDouble(sensorId, type) ;
        if (obj instanceof Integer) return ((Integer) obj) > otherData.getInt(sensorId, type, 0);
        if (obj instanceof Long) return ((Long) obj) > otherData.getLong(sensorId, type);
        if (obj instanceof Float) return ((Float) obj) > otherData.getFloat(sensorId, type);
        if (obj instanceof String && !obj.equals("")) return ((String) obj).compareTo(otherData.getString(sensorId, type)) > 0;
        if (obj instanceof float[]) {
            float[] f1 = (float[]) obj;
            MathVector v1 = new MathVector(f1, f1.length);
            float[] f2 = (float[]) otherData.getValue(sensorId, type);
            if (f2 == null) return true;
            MathVector v2 = new MathVector(f2, f2.length);
            return v1.length() > v2.length();
        }
        if (obj instanceof double[]) {
            double[] f1 = (double[]) obj;
            MathVector v1 = new MathVector(f1);
            double[] f2 = (double[]) otherData.getValue(sensorId, type);
            MathVector v2 = new MathVector(f2);
            return v1.length() > v2.length();
        }
        return null;
    }

    public Boolean smaller(String sensorId, DataType type, Data otherData) {
        Object obj = getValue(sensorId, type);
        if (obj == null || otherData == null) return null;
        if (!otherData.hasDataType(type)) return null;
        if (obj instanceof Double) return ((Double) obj) < otherData.getDouble(sensorId, type) ;
        if (obj instanceof Integer) return ((Integer) obj) < otherData.getInt(sensorId, type, 0);
        if (obj instanceof Long) return ((Long) obj) < otherData.getLong(sensorId, type);
        if (obj instanceof Float) return ((Float) obj) < otherData.getFloat(sensorId, type);
        if (obj instanceof String && !obj.equals("")) return ((String) obj).compareTo(otherData.getString(sensorId, type)) < 0;
        if (obj instanceof float[]) {
            float[] f1 = (float[]) obj;
            MathVector v1 = new MathVector(f1, f1.length);
            float[] f2 = (float[]) otherData.getValue(sensorId, type);
            if (f2 == null) return false;
            MathVector v2 = new MathVector(f2, f2.length);
            return v1.length() < v2.length();
        }
        if (obj instanceof double[]) {
            double[] f1 = (double[]) obj;
            MathVector v1 = new MathVector(f1);
            double[] f2 = (double[]) otherData.getValue(sensorId, type);
            MathVector v2 = new MathVector(f2);
            return v1.length() < v2.length();
        }
        return null;
    }

    public Boolean equal(String sensorId, DataType type, Data otherData) {
        Object obj = getValue(sensorId, type);
        if (obj == null || otherData == null) return null;
        if (!otherData.hasDataType(type)) return null;
        if (obj instanceof Double) return (Double) obj == otherData.getDouble(sensorId, type) ;
        if (obj instanceof Integer) return (Integer) obj == otherData.getInt(sensorId, type, 0);
        if (obj instanceof Long) return obj == otherData.getLong(sensorId, type);
        if (obj instanceof Float) return (Float) obj == otherData.getFloat(sensorId, type);
        if (obj instanceof String && !obj.equals("")) return obj.equals(otherData.getString(sensorId, type));
        if (obj instanceof float[]) {
            float[] f1 = (float[]) obj;
            float[] f2 = (float[]) otherData.getValue(sensorId, type);
            if (f2 == null) return false;
            for (int i = 0; i < Math.min(f1.length, f2.length); i++) {
                if (f1[i] != f2[i]) return false;
            }
            return true;
        }
        if (obj instanceof double[]) {
            double[] f1 = (double[]) obj;
            double[] f2 = (double[]) otherData.getValue(sensorId, type);
            if (f2 == null) return false;
            for (int i = 0; i < Math.min(f1.length, f2.length); i++) {
                if (f1[i] != f2[i]) return false;
            }
            return true;
        }
        return null;
    }

    public String getVoiceString(String sensorId, DataType type) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return null;
        if (obj instanceof Double) return type.toVoiceString((Double) obj);
        if (obj instanceof Integer) return type.toVoiceString((Integer) obj);
        if (obj instanceof Long) return type.toVoiceString((Long) obj);
        if (obj instanceof Float) return type.toVoiceString((Float) obj);
        if (obj instanceof String) return type.toVoiceString((String) obj);
        if (obj instanceof float[]) return type.toVoiceString((float[]) obj);
        if (obj instanceof double[]) return type.toVoiceString((double[]) obj);
        return null;
    }

    public long getTime(String sensorId, DataType type) {
        Tuple<Object, Long> t = getValueTime(sensorId, type);
        if (t != null) return t.t2();
        return time;
    }

    public long getTime() {
        return time;
    }

    public Data add(Data otherData) {
        return merge(otherData);
    }

    public Data merge(Data otherData) {
        time = otherData.time; //Math.max(time, otherData.time);
        Map<String, Tuple<Object, Long>> otherMap;
        for (DataType type : otherData.values.keySet()) if ((otherMap = otherData.values.get(type)) != null) {
            for (String sensorId: otherMap.keySet()) {
                setValue(sensorId, type, otherData.getValueTime(sensorId, type));
            }
        }
        return this;
    }


    public boolean mergeCheckEqual(Data otherData, boolean replace) {
        boolean equal = true;
        time = otherData.time; //Math.max(time, otherData.time);
        if (values.size() != otherData.values.size()) equal = false;
        Map<String, Tuple<Object, Long>> otherMap;
        for (DataType type : otherData.values.keySet()) if ((otherMap = otherData.values.get(type)) != null) {
            Map<String, Tuple<Object, Long>> map = values.get(type);
            if (map == null || map.size() != otherMap.size()) equal = false;
            for (String sensorId: otherMap.keySet()) {
                Tuple<Object, Long> otherValueTime = otherData.getValueTime(sensorId, type);
                Tuple<Object, Long> valueTime = getValueTime(sensorId, type);
                if (otherValueTime == null && valueTime != null || otherValueTime != null && valueTime == null || otherValueTime != null && !otherValueTime.t1().equals(valueTime.t1())
                        ) equal = false;
                if ((!equal || replace) && otherValueTime != null) setValue(sensorId, type, otherValueTime);
            }
        }
        return equal;
    }

    public void mergeAggregates(Data otherData) {
        time = Math.max(time, otherData.time);
        Map<String, Tuple<Object, Long>> otherMap;
        for (DataType type : otherData.values.keySet()) if (type.isAggregate() && (otherMap = otherData.values.get(type)) != null) {
            for (String sensorId: otherMap.keySet()) {
                setValue(sensorId, type, otherData.getValueTime(sensorId, type));
            }
        }
    }

    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public Data clone() {
        Data newData = new Data(/*defaultSensor,*/ time);
        for (DataType type : values.keySet()) {
            Map<String, Tuple<Object, Long>> map = values.get(type);
            if (map != null) for (String sensorId: map.keySet()) {
                newData.setValue(sensorId, type, getValueTime(sensorId, type));
            }
        }
        return newData;
    }

    public Data cloneAggregates() {
        Data newData = new Data(/*defaultSensor,*/ time);
        for (DataType type : values.keySet()) if (type.isAggregate()) {
            Map<String, Tuple<Object, Long>> map = values.get(type);
            if (map != null) for (String sensorId: map.keySet()) {
                newData.setValue(sensorId, type, getValueTime(sensorId, type));
            }
        }
        return newData;
    }

    public Data clone(DataType type) {
        Map<String, Tuple<Object, Long>> map = values.get(type);
        if (map != null) {
            Data newData = new Data(/*defaultSensor,*/ time);
            for (String sensorId : map.keySet()) {
                newData.setValue(sensorId, type, getValueTime(sensorId, type));
            }
            return newData;
        }
        return null;
    }

    public Data clone(String sensorId, DataType type) {
        Map<String, Tuple<Object, Long>> map = values.get(type);
        if (map != null) {
            Tuple<Object, Long> vt = getValueTime(sensorId, type);
            if (vt != null)
                return new Data(time).setValue(sensorId, type, vt);

        }
        return null;
    }

    public Data clone(Checker checker) {

        Data newData = new Data(/*defaultSensor,*/ time);
        for (DataType type : values.keySet()) if (checker.isChecked(type)) {
            Map<String, Tuple<Object, Long>> map = values.get(type);
            if (map != null) for (String sensorId : map.keySet()) {
                newData.setValue(sensorId, type, getValueTime(sensorId, type));
            }
        }
        return newData;
    }

    @Override
    public String toString() {
        Stringer sb = new Stringer(", ");
        Map<String, Tuple<Object, Long>> map;

        for (DataType type : values.keySet()) if ((map = values.get(type)) != null) for (String sensorId: map.keySet()){
            Object obj = getValue(sensorId, type);
            if (obj instanceof Integer) {
                Integer value = (Integer) obj;
                sb.append(type.toString(value));
            } else if (obj instanceof Long) {
                Long value = (Long) obj;
                sb.append(type.toString(value));
            } else if (obj instanceof Double) {
                Double value = (Double) obj;
                sb.append(type.toString(value));
            } else if (obj instanceof Float) {
                Float value = (Float) obj;
                sb.append(type.toString(value));
            } else if (obj instanceof String) {
                String value = (String) obj;
                sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(value);

            } else if (obj instanceof byte[]) {
                String value = StringUtil.toReadableString((byte[]) obj);
                sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(value);

            } else if (obj instanceof int[]) {
                String value = StringUtil.toString((int[]) obj, ",");
                sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(value);
            } else if (obj instanceof float[]) {
                String value = StringUtil.toString((float[]) obj, ",", type.decimals);
                if (value != null) {
                    sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(value);
                }
            } else if (obj instanceof double[]) {
                String value = StringUtil.toString((double[]) obj, ",", type.decimals);
                sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(value);
            } else if (obj != null){
                sb.append(type.toProperString()).appendNoDelim(" = ").appendNoDelim(obj.toString() + " (" + obj.getClass().getSimpleName() + ")");
            }
        }
        sb.append(TimeUtil.formatTime(getTime()));
        return sb.toString();
    }

    public interface SensorIdTranslator {
        String translate(String sensorId);
    }

    public JSONObject toJSON(SensorIdTranslator translator) {
        JSONArray jArray = new JSONArray();
        Map<String, Tuple<Object, Long>> map;

        for (DataType type : values.keySet()) if ((map = values.get(type)) != null) for (String sensorId: map.keySet()) {
            String id = sensorId;
            if (translator != null) id = translator.translate(sensorId);
            Object obj = getValue(id, type);
            jArray.put(type.toJSON(obj, id));
        }
        if (jArray.length() == 0) return null;
        JSONObject jObj = new JSONObject();
        try {
            jObj.put(JSoNField.VALUES.toString(), jArray);
            jObj.put(JSoNField.TIME.toString(), getTime());
        } catch (JSONException e) {
            LLog.e("Could not create JSON object", e);
        }
        return jObj;
    }

//    private JSONObject toJSON(EnumSet<DataType> allowedTypes) {
//        JSONArray jArray = new JSONArray();
//        Map<String, Tuple<Object, Long>> map;
//
//        for (DataType type : values.keySet()) if ((allowedTypes == null || allowedTypes.contains(type)) && (map = values.get(type)) != null) for (String sensorId: map.keySet()){
//            Object obj = getValue(sensorId, type);
//            jArray.put(type.toJSON(obj, sensorId));
//        }
//        if (jArray.length() == 0) return null;
//        JSONObject jObj = new JSONObject();
//        try {
//            jObj.put(JSoNField.VALUES.toString(), jArray);
//            jObj.put(JSoNField.TIME_CLOCK.toString(), getTime());
//        } catch (JSONException e) {
//            LLog.e("Could not create JSON object", e);
//        }
//        return jObj;
//    }

//    public JSONObject toJSON(String sensorId, DataType type) {
//        Object obj = getValue(sensorId, type);
//        JSONObject jObj = type.toJSON(obj, sensorId);
//        try {
//            jObj.put(JSoNField.TIME_CLOCK.toString(), getTime());
//        } catch (JSONException e) {
//            LLog.e("Could not create JSON object", e);
//        }
//        return jObj;
//    }

    public static Data fromJSON(String jsonStr) {
        try {
            JSONObject json = new JSONObject(jsonStr);
            return fromJSON(json);
        } catch (JSONException e) {
            LLog.e("Could not create JSON object from " + jsonStr, e);
        }
        return null;
    }

    public static Data fromJSON(JSONObject json) {
        long time = JSoNHelper.get(json, JSoNField.TIME, -1L);
        if (time == -1) return null;
        JSONArray jArray = JSoNHelper.get(json, JSoNField.VALUES);
        if (jArray == null) return null;
        Data data = new Data(time);

        try {
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObj = jArray.getJSONObject(i);
                String sensorId = JSoNHelper.get(jObj, JSoNField.SENSORID, (String) null);
                String className = JSoNHelper.get(jObj, JSoNField.CLASS, (String) null);
                DataType dt = DataType.fromJSONString(JSoNHelper.get(jObj, JSoNField.DATATYPE, (String) null));
                if (dt == null) continue;
                if (className.equalsIgnoreCase("Integer")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0));
                } else if (className.equalsIgnoreCase("Double")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0.0));
                } else if (className.equalsIgnoreCase("Float")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0.0f));
                } else if (className.equalsIgnoreCase("Long")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0L));
                } else if (className.equalsIgnoreCase("String")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, (String) null));

                } else if (className.equalsIgnoreCase("int[]")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, new int[]{}));
                } else if (className.equalsIgnoreCase("float[]")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, new float[]{}));
                } else if (className.equalsIgnoreCase("double[]")) {
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, new double[]{}));
                } else {
                    LLog.i("Unknown classname: " + className);
                    data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, (String) null));
                }
                //if (sensorId != null) data.setSensorId(dt, sensorId);

            }
        } catch (JSONException e) {
            LLog.e("Could not create Data from JSON object", e);
        }
        return data;
    }

    public Tuple<String,String> toNameValueString() {
        Stringer names = new Stringer("\n");
        Stringer values = new Stringer("\n");
        Map<String, Tuple<Object, Long>> map;

        for (DataType type : this.values.keySet()) if ((map = this.values.get(type)) != null) for (String sensorId: map.keySet()){
            Object obj = this.getValue(sensorId, type);
            if (obj instanceof Double) {
                Double value = (Double) obj;
                names.append(type.toProperString());
                values.append(type.toValueString(value, true, false));
            } else if (obj instanceof Float) {
                Float value = (Float) obj;
                names.append(type.toProperString());
                values.append(type.toValueString(value, true, false));
            } else if (obj instanceof Integer) {
                Integer value = (Integer) obj;
                names.append(type.toProperString());
                values.append(type.toValueString(value, true, false));
            } else if (obj instanceof Long) {
                Long value = (Long) obj;
                names.append(type.toProperString());
                values.append(type.toValueString(value, true, false));
            } else if (obj instanceof String) {
                String value = (String) obj;
                if (!value.equals("")) {
                    names.append(type.toProperString());
                    values.append(value);
                }
            } else if (obj instanceof byte[]) {
                String value = StringUtil.toHex((byte[]) obj);
                names.append(type.toProperString());
                values.append(value);
            } else if (obj instanceof int[]) {
                String value = StringUtil.toString((int[]) obj, ", ");
                names.append(type.toProperString());
                values.append(value);
            } else if (obj instanceof double[]) {
                String value = StringUtil.toString((double[]) obj, ", ", type.decimals) + " " + type.getUnit().toString();
                names.append(type.toProperString());
                values.append(value);
            } else if (obj instanceof float[]) {
                String value = StringUtil.toString((float[]) obj, ", ", type.decimals) + " " + type.getUnit().toString();
                names.append(type.toProperString());
                values.append(value);
            } else if (obj != null) {
                LLog.i("Could not create NameValueString for " + type + " = " + obj.toString() + " (" + obj.getClass().getSimpleName() + ")");
            }
        }
        return new Tuple<>(names.toString(), values.toString());
    }

    public Tuple<String,String> toNameValueString(String sensorId, DataType type) {
        return toNameValueString(sensorId, type, 0);
    }

    private Tuple<String,String> toNameValueString(String sensorId, DataType type, float offset) {
        String value = toValueString(sensorId, type);
        if (value != null) return new Tuple<>(type.toProperString(), value);
        return null;
    }

    public String toValueString(String sensorId, DataType type) {
        return toValueString(sensorId, type, true, 0, 0, 0, false);
    }

    public String toValueString(String sensorId, DataType type, boolean withUnit) {
        return toValueString(sensorId, type, withUnit, 0, 0, 0, false);
    }

    public String toValueString(String sensorId, DataType type, float offset) {
        return toValueString(sensorId, type, true, 0, offset, 0, false);
    }

    public String toValueString(String sensorId, DataType type, int mask) {
        return toValueString(sensorId, type, true, mask, 0, 0, false);
    }

    private String toValueString(String sensorId, DataType type, boolean withUnit, int mask, float offset, float cutOff, boolean downSizeFraction) {
        String[] ret = _toValueString(sensorId, type, false, mask, offset, cutOff, downSizeFraction);
        if (ret == null || ret.length == 0) return "";
        if (ret.length == 1 || !withUnit) return ret[0];
        return ret[0] + " " + ret[1];
    }

    public String[] toValueStringParts(String sensorId, DataType type) {
        return _toValueString(sensorId, type, false, 0, 0, 0.0f, false);
    }

    private String[] _toValueString(String sensorId, DataType type, boolean speech, int mask, float offset, float cutOff, boolean downSizeFraction) {
        Object obj = getValue(sensorId, type);
        if (obj == null) return null;
        if (obj instanceof Double) {
            Double value = (Double) obj + offset;
            if (cutOff > 0) {
                if (downSizeFraction && Math.abs(value) < 1) value *= value;
                if (Math.abs(value) < cutOff) value = 0.0;
            }
            return type.toValueString(speech, value);
        } else if (obj instanceof Float) {
            Float value = (Float) obj + offset;
            if (cutOff > 0) {
                if (downSizeFraction && Math.abs(value) < 1f) value *= value;
                if (Math.abs(value) < cutOff) value = 0.0f;
            }
            return type.toValueString(speech, value);
        } else if (obj instanceof Integer) {
            Integer value = (Integer) obj + (int) offset;
            if (mask != 0) value &= mask;
            return type.toValueString(speech, value);
        } else if (obj instanceof Long) {
            Long value = (Long) obj + (int) offset;
            return type.toValueString(speech, value);
        } else if (obj instanceof String) {
            return new String[] {(String) obj};
        } else if (obj instanceof byte[]) {
            return new String[] {StringUtil.toHex((byte[]) obj)};
        } else if (obj instanceof int[]) {
            return new String[] {StringUtil.toString((int[]) obj, ", ")};
        } else if (obj instanceof double[]) {
            if (speech)
                return new String[] {StringUtil.toString((double[]) obj, ", ", type.decimals), type.getUnit().toVoiceString()};
            return new String[] {StringUtil.toString((double[]) obj, ", ", type.decimals), type.getUnit().toString()};
        } else if (obj instanceof float[]) {
            if (speech)
                return new String[]{StringUtil.toString((float[]) obj, ", ", type.decimals), type.getUnit().toVoiceString()};
            return new String[]{StringUtil.toString((float[]) obj, ", ", type.decimals), type.getUnit().toString()};
        } else {
            LLog.i("Could not create NameValueString for " + type + " = " + obj.toString() + " (" + obj.getClass().getSimpleName() + ")");
        }
        return null;
    }


    public void setTime(long time) {
        if (time > this.time) this.time = time;
    }

//    public void setTime() {
//        this.time = System.currentTimeMillis();
//    }

    public boolean valueChanged(Data newData, DataType type) {
        if (!newData.hasDataType(type)) return false;
        if (!hasDataType(type)) return true;
        Map<String, Tuple<Object, Long>> map;
        if ((map = newData.values.get(type)) != null) for (String sensorId: map.keySet()) {
            String[] strs1 = this.toValueStringParts(sensorId, type);
            String[] strs2 = newData.toValueStringParts(sensorId, type);
            if (strs1 != null && strs1.length > 0 && strs2 != null && strs2.length > 0) return !(strs1[0].equals(strs2[0]));
        }
        return true;
    }

    public boolean valueChanged(Data other) { //}, EnumSet<DataType> types) {
        if (other == null) return false;
//        if (types == null || types.size() == 0)
        for (DataType type: DataType.values()) {
            if (valueChanged(other, type)) return true;
        }
//        } else for (DataType type: types) {
//            if (valueChanged(other, type)) return true;
//        }
        return false;
    }

    public static int getHistoryLength(String sensorId, DataType type) {
        List<Object> typeHistory;
        Map<String, List<Object>> map;
        if ((map = history.get(type)) == null) return 0;
        if ((typeHistory = map.get(sensorId)) == null) return 0;
        return typeHistory.size();
    }

    public static void historyClear(String sensorId, DataType type) {
        List<Object> typeHistory;
        Map<String, List<Object>> map;
        if ((map = history.get(type)) == null) return;
        if ((typeHistory = map.get(sensorId)) == null) return;
        typeHistory.clear();

    }

    public static double[] getHistoryAsDoubles(String sensorId, DataType type, int offset) {
        List<Object> typeHistory;
        Map<String, List<Object>> map;

        double[] result = new double[0];
        if ((map = history.get(type)) == null) return result;
        typeHistory = map.get(sensorId);
        if (typeHistory == null) return result;
        if (typeHistory.size() < offset) return result;
        synchronized (history) {
            result = new double[typeHistory.size() - offset];
            for (int i = 0; i < result.length; i++) {
                result[i] = _getDouble(typeHistory.get(i + offset));
            }
        }
        return result;
    }
}
