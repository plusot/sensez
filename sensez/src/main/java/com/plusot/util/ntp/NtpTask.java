package com.plusot.util.ntp;

import android.os.AsyncTask;


public class NtpTask {

    public static void lookup(final Listener listener) {
        new NtpTask().doLookup(listener);
    }

    private void doLookup(final Listener listener) {
        new NtpTaskWorker().execute(new Input(listener));
    }

    public interface Listener {
        void onResult(NtpLookup.NtpResult result);
    }

    private class Input {
        Listener listener;

        public Input(final Listener listener) {
            this.listener = listener;
        }
    }

    private class Result {
        final NtpLookup.NtpResult ntpResult;
        final Listener listener;

        public Result(final Listener listener, final NtpLookup.NtpResult ntpResult) {
            this.listener = listener;
            this.ntpResult = ntpResult;
        }

    }

    private class NtpTaskWorker extends AsyncTask<Input, Void, Result> {
        @Override
        protected Result doInBackground(Input... params) {
            if (params == null || params.length < 1) return null;
            NtpLookup.NtpResult ntpResult = NtpLookup.getInfo();
            return new Result(params[0].listener, ntpResult);
        }

        protected void onPostExecute(Result result) {
            if (result != null && result.listener != null && result.ntpResult != null) {
                result.listener.onResult(result.ntpResult);
            }
        }
    }

}
