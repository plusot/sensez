package com.plusot.util.ntp;

import com.plusot.util.logging.LLog;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.DecimalFormat;

public class NtpLookup {
    private static final boolean DEBUG = false;


    private static final String[] ntpServers = {
//            "pool.ntp.org",
            "europe.pool.ntp.org",
            "time-a.nist.gov",
            "0.pool.ntp.org",
            "time-b.nist.gov",
            "1.pool.ntp.org",
            "time-c.nist.gov",
            "2.pool.ntp.org",
            "time-d.nist.gov",
            "3.pool.ntp.org",
    };
    private static final long[] delays = {
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE,
            Long.MAX_VALUE};
    private static int timeServerIndex = -1;
    private static boolean serverInvalidated = false;
    private static boolean cycled = false;


    private static void getBestServer() {
        cycled = false;
        long minDelay = Long.MAX_VALUE;
        timeServerIndex = 0;
        int iValid = 0;
        for (int i = 0; i < ntpServers.length; i++) {
            if (delays[i] < Long.MAX_VALUE) {
                iValid++;
            }
            if (delays[i] < minDelay) {
                minDelay = delays[i];
                timeServerIndex = i;
            }
        }
        if (iValid > 2) {
            cycled = true;
        } else {
            timeServerIndex = 0;
        }
    }

    private static String getServer() {
        if (serverInvalidated) {
            if (cycled) getBestServer();
            serverInvalidated = false;
        }
        if (!cycled) timeServerIndex++;
        if (timeServerIndex > (ntpServers.length - 1)) getBestServer();

        return ntpServers[timeServerIndex];
    }

    public static void selectNextServer() {
        serverInvalidated = true;
    }

    public static NtpResult getInfo() {
        String serverName = getServer();
        double localClockOffset = 0;
        try {
            // Send request
            DatagramSocket socket = new DatagramSocket();
            socket.setSoTimeout(20000);
            InetAddress address = InetAddress.getByName(serverName);
            byte[] buf = new NtpMessage().toByteArray();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 123);


            // Set the transmit timestamp *just* before sending the packet
            // Does this actually improve performance or not?
            NtpMessage.encodeTimestamp(packet.getData(), 40,
                    (System.currentTimeMillis() / 1000.0) + 2208988800.0);
            socket.send(packet);

            // Get response
            if (DEBUG) LLog.i("NTP request sent, waiting for response...");
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            // Immediately record the incoming timestamp
            double destinationTimestamp =
                    (System.currentTimeMillis() / 1000.0) + 2208988800.0;

            // Process response
            NtpMessage msg = new NtpMessage(packet.getData());

            // Corrected, according to RFC2030 errata
            double roundTripDelay = (destinationTimestamp - msg.originateTimestamp) -
                    (msg.transmitTimestamp - msg.receiveTimestamp);
            if (delays[timeServerIndex] == Long.MAX_VALUE) {
                delays[timeServerIndex] = (long) (roundTripDelay * 1000);
            } else {
                delays[timeServerIndex] = (4 * delays[timeServerIndex] + (long) (roundTripDelay * 1000)) / 5;
            }
            localClockOffset =
                    ((msg.receiveTimestamp - msg.originateTimestamp) +
                            (msg.transmitTimestamp - destinationTimestamp)) / 2;

            // Display response
            StringBuilder sb = new StringBuilder();
            sb.append("NTP server: ").append(serverName).append('\n');
            sb.append(msg.toString()); //.replace("\n", "</br>"));
            sb.append("Dest. timestamp: ").append(NtpMessage.timestampToString(destinationTimestamp)).append('\n');
            sb.append("Round-trip: ").append(new DecimalFormat("0.00").format(roundTripDelay)).append(" s\n");
            sb.append("Clock offset: ").append(new DecimalFormat("0.00").format(localClockOffset)).append(" s\n");
            //if (DEBUG)
            //LLog.i("NTP offset = " + localClockOffset);

            socket.close();
            return new NtpResult(true, serverName, localClockOffset, roundTripDelay, sb.toString());
        } catch (IOException e) {
            delays[timeServerIndex] = Long.MAX_VALUE;
            serverInvalidated = true;
            String error;
            if ((error = e.getMessage()) != null)
                return new NtpResult(false, serverName, localClockOffset, -1, "NTP error contacting " + serverName + ": " + error);
            else
                return new NtpResult(false, serverName, localClockOffset, -1, "NTP error contacting " + serverName + ": IO Error");
        }
    }


//    public static Date getDate(String serverName) throws IOException, NtpException {
//        if (serverName == null)
//            serverName = getServer();
//
//        //System.out.println("ntp Server " + timeServerIndex + ": " + serverName);
//
//        // Send request
//        DatagramSocket socket = new DatagramSocket();
//        socket.setSoTimeout(5000);
//
//        InetAddress address = InetAddress.getByName(serverName);
//        byte[] buf = new NtpMessage().toByteArray();
//        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 123);
//
//        // Set the transmit timestamp *just* before sending the packet
//        // Does this actually improve performance or not?
//        double timeStamp = (System.currentTimeMillis() / 1000.0) + 2208988800.0;
//        NtpMessage.encodeTimestamp(packet.getData(), 40, timeStamp);
//
//        socket.send(packet);
//
//        // Get response
//        //System.out.println("NTP request sent, waiting for response...\n");
//        packet = new DatagramPacket(buf, buf.length);
//        socket.receive(packet);
//
//        // Immediately record the incoming timestamp
//        double destinationTimestamp = (System.currentTimeMillis() / 1000.0) + 2208988800.0;
//
//        // Process response
//        NtpMessage msg = new NtpMessage(packet.getData());
//
//        socket.close();
//
//        if (msg.receiveTimestamp == 0)
//            throw new NtpException("unreliable time on " + serverName);
//
//        return NtpMessage.timestampToDate(
//                destinationTimestamp +
//                        ((msg.receiveTimestamp - msg.originateTimestamp) +
//                                (msg.transmitTimestamp - destinationTimestamp)) / 2);
//    }

//    public static Date getDate() throws IOException {
//        int retries = ntpServers.length + 1;
//        while (retries > 0) {
//            try {
//                return getDate(null);
//            } catch (SocketTimeoutException e) {
//                selectNextServer();
//                retries--;
//            } catch (NtpException e) {
//                LLog.e(e.getMessage());
//                selectNextServer();
//                retries--;
//            } catch (IOException e) {
//                selectNextServer();
//                timeServerIndex = -1;
//                throw e;
//            }
//        }
//
//        timeServerIndex = -1;
//        throw new IOException("Unable to connect to anny of the NTP-servers");
//    }

    public static class NtpResult {

        final boolean success;
        final String info;
        final double clockOffset;
        final double roundTripDelay;
        final String server;

        public NtpResult(final boolean success, final String server, final double clockOffset, final double roundTripDelay, final String info) {
            this.success = success;
            this.info = info;
            this.server = server;
            this.clockOffset = clockOffset;
            this.roundTripDelay = roundTripDelay;
        }

        public boolean isSuccess() {
            return success;
        }

        public String getInfo() {
            return info;
        }

        public  long getClockOffsetMs() {
            return (long)(1000.0 * clockOffset);
        }

        public  long getRoundTripDelayMs() {
            return (long)(1000.0 * roundTripDelay);
        }

        public String getServer() {
            return server;
        }


    }
}
