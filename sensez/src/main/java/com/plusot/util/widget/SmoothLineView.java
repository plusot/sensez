package com.plusot.util.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

import com.plusot.util.logging.Timing;

import java.util.ArrayList;

/**
 * Created by peet on 17-03-16.
 */
public class SmoothLineView extends View {
    private final int numberOfPoints;
    private final Paint pointStrokePaint;
    private final Paint pointFillPaint;
    private final Paint sparkLinePaint;
    private final Paint sparkLineFillPaint;
    private ArrayList<Float> dataPoints;
    private float displayWidth;
    private float maxVal;
    private float minVal;
    private float maxValMin = 0;
    private float minValMin = 0;
    private boolean autoScale = false;
    private boolean autoScaleBounceBack = false;
    private boolean hasCircles = false;
    private Point prevPoint = new Point();
    private Point currentPoint = new Point();
    private Point nextPoint = new Point();
    private Path path = new Path();
    private boolean fill;
    private final int viewNr;


    public SmoothLineView(Context context, int numberOfPoints, int viewNr) {
        this(context, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, numberOfPoints, true, true, viewNr);

    }

    public SmoothLineView(Context context, float maxValMin, float minValMin, int numberOfPoints, boolean autoScale, boolean fill, int viewNr) {
        super(context);
        this.viewNr = viewNr;
        this.fill = fill;
        this.numberOfPoints = numberOfPoints;
        this.autoScale = this.autoScaleBounceBack = autoScale;
        this.sparkLinePaint = new Paint() {
            {
                setStyle(Style.STROKE);
                setStrokeCap(Cap.ROUND);
                setStrokeWidth(5.0f);
                setAntiAlias(true);
                setARGB(255,255,0,0);
            }
        };
        this.sparkLineFillPaint = new Paint() {
            {
                setStyle(Style.FILL);
                setStrokeCap(Cap.ROUND);
                setStrokeWidth(5.0f);
                setAntiAlias(true);
                setARGB(128,255,0,0);
            }
        };
        this.pointStrokePaint = new Paint() {
            {
                setARGB(255, 255, 255, 255);
                setStyle(Style.FILL_AND_STROKE);
                setAntiAlias(true);
            }
        };
        this.pointFillPaint = new Paint() {
            {
                setARGB(255, 255, 0, 0);
                setStyle(Style.FILL);
                setAntiAlias(true);
            }
        };
        this.dataPoints = new ArrayList<>();
        this.maxVal = this.maxValMin = maxValMin;
        this.minVal = this.minValMin = minValMin;

//        for (int ii = 0; ii < numberOfPoints; ii++) {
//            this.dataPoints.addWatchable(Float.valueOf(0.0f));
//        }
        this.setWillNotDraw(false);
        this.setBackgroundColor(Color.TRANSPARENT);
        this.displayWidth = 200;
    }

    static class Point {
        float x, y;
        float dx, dy;

        Point set(float x, float y) {
            this.x = x;
            this.y = y;
            return this;
        }

        Point dset(float dx, float dy) {
            this.dx = dx;
            this.dy = dy;
            return this;
        }

        Point set(Point p) {
            this.x = p.x;
            this.y = p.y;
            this.dx = p.dx;
            this.dy = p.dy;
            return this;
        }

        @Override
        public String toString() {
            return x + ", " + y;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Timing.log(SmoothLineView.class.getSimpleName() + "_" + viewNr, Timing.Action.BEGIN);
        try {
            int ii;
            int iterations = dataPoints.size();
            float border = 15;
            float w = this.getWidth();
            float h = this.getHeight();
            float max = Float.NEGATIVE_INFINITY;
            float min = Float.POSITIVE_INFINITY;

            super.onDraw(canvas);
            path.reset();

//        LLog.i("Draw called");

            for (ii = 0; ii < iterations; ii++) {
                Float v = dataPoints.get(ii);

                if (v > max) max = v;
                if (v < min) min = v;
                if (this.autoScale) {
                    if (v > this.maxVal) {
                        if (v < 0)
                            this.minVal = v * 0.9f;
                        else
                            this.maxVal = v * 1.1f;
                        //if (this.minVal < -0.001) this.minVal = -v - 0.01f;
                        //else this.minVal = 0.0f;
                    } else if (v < this.minVal) {
                        if (v < 0)
                            this.minVal = v * 1.1f;
                        else
                            this.minVal = v + 0.9f;
                    }
                }
            }
            if (this.autoScaleBounceBack) {
                //max = Math.max(max,Math.abs(min));
                this.maxVal = Math.max(max, maxValMin);
                //if (this.minVal < -0.1f) this.minVal = -max;
                //else this.minVal = 0.0f;
                this.minVal = Math.min(min, minValMin);
            }

            for (ii = 0; ii < iterations - 1; ii++) {
                Float v1 = dataPoints.get(ii + 1);
                nextPoint.set(
                        ((w - (2 * border)) / iterations) * (ii + 1) + border,
                        h - border - (((h - (2 * border)) / (this.maxVal - this.minVal)) * (v1 - this.minVal))
                );
                if (ii == 0) {
                    Float v0 = dataPoints.get(ii);
                    currentPoint.set(0, h - ((h / (this.maxVal - this.minVal)) * (v0 - this.minVal)));
                    path.moveTo(currentPoint.x, currentPoint.y);
                    prevPoint.set(currentPoint);
                }
                currentPoint.dset(((nextPoint.x - prevPoint.x) / 5), ((nextPoint.y - prevPoint.y) / 5));
                if (ii != 0)
                    path.cubicTo(prevPoint.x + prevPoint.dx, prevPoint.y + prevPoint.dy,
                            currentPoint.x - currentPoint.dx, currentPoint.y - currentPoint.dy,
                            currentPoint.x, currentPoint.y);
                prevPoint.set(currentPoint);
                currentPoint.set(nextPoint);
            }
            currentPoint.dset(((currentPoint.x - prevPoint.x) / 5), ((currentPoint.y - prevPoint.y) / 5));
            path.cubicTo(prevPoint.x + prevPoint.dx, prevPoint.y + prevPoint.dy,
                    currentPoint.x - currentPoint.dx, currentPoint.y - currentPoint.dy,
                    currentPoint.x, currentPoint.y);
//        for (ii = 0; ii < iterations; ii++) {
//            Float v = dataPoints.get(ii);
//
//            if (v > max) max = v;
//            if (v < min) min = v;
//            if (this.autoScale) {
//                if (v > this.maxVal) {
//                    this.maxVal = v + 0.01f;
//                    if (this.minVal < -0.001) this.minVal = -v - 0.01f;
//                    else this.minVal = 0.0f;
//                }
//                else if (v < this.minVal)  {
//                    this.minVal = v - 0.01f;
//                    this.maxVal = -v + 0.01f;
//                }
//            }
//        }
//        if (this.autoScaleBounceBack) {
//            //max = Math.max(max,Math.abs(min));
//            this.maxVal = Math.max(max, maxValMin);
//            //if (this.minVal < -0.1f) this.minVal = -max;
//            //else this.minVal = 0.0f;
//            this.minVal = Math.min(min, minValMin);
//        }
//        for (ii = 0; ii < iterations; ii++) {
//            if (ii == 0) {
//                Float v = dataPoints.get(ii);
//                path.moveTo(0, h - ((h / (this.maxVal - this.minVal)) * (v - this.minVal)));
//                continue;
//            }
//            else {
//                //Last value
//                Float v0 = dataPoints.get(ii - 1);
//                //This value
//                Float v1 = dataPoints.get(ii);
//                //Last Point coordinate
//                PointF p0 = new PointF();
//                //This Point coordinate
//                PointF p1 = new PointF();
//                //Midpoint between p0 and p1
//                PointF midPoint;
//                //Control point
//                PointF c1;
//                PointF c2;
//
//                p0.x = ((w - (2 * border) )/ iterations) * (ii - 1) + border;
//                p0.y= h - border - (((h - (2 * border)) / (this.maxVal - this.minVal)) * (v0 - this.minVal));
//                p1.x = ((w - (2 * border) )/ iterations) * (ii) + border;
//                p1.y = h - border - (((h - (2 * border)) / (this.maxVal - this.minVal)) * (v1 - this.minVal));
//                midPoint = this.midPointForPoints(p0, p1);
//                c1 = this.controlPointForPoints(midPoint, p0);
//                path.quadTo(c1.x, c1.y, midPoint.x, midPoint.y);
//                c2 = this.controlPointForPoints(midPoint, p1);
//                path.quadTo(c2.x, c2.y, p1.x, p1.y);
//            }
//        }

            //sparkLinePaint.setPathEffect(new CornerPathEffect(10))''

            canvas.drawPath(path, this.sparkLinePaint);
            if (fill) {
                currentPoint.set(currentPoint.x, h);
                path.lineTo(currentPoint.x, currentPoint.y);
                currentPoint.set(0, h);
                path.lineTo(currentPoint.x, currentPoint.y);
                path.close();
                canvas.drawPath(path, this.sparkLineFillPaint);
            }

            if (hasCircles) for (ii = 0; ii < iterations; ii++) {
                Float v = dataPoints.get(ii);
                Float x, y;
                x = ((w - (2 * border)) / iterations) * ii + border;
                y = h - border - (((h - (2 * border)) / (this.maxVal - this.minVal)) * (v - this.minVal));
                canvas.drawCircle(x, y, 10, this.pointStrokePaint);
                canvas.drawCircle(x, y, 7, this.pointFillPaint);
            }
        } finally {
            Timing.log(SmoothLineView.class.getSimpleName() + "_" + viewNr, Timing.Action.END);
        }
    }

//    PointF controlPointForPoints(PointF p1, PointF p2) {
//        PointF controlPoint = midPointForPoints(p1, p2);
//        Float diffY = (float) Math.abs(p2.y - controlPoint.y);
//
//        if (p1.y < p2.y)
//            controlPoint.y += diffY;
//        else if (p1.y > p2.y)
//            controlPoint.y -= diffY;
//
//        return controlPoint;
//    }

//    PointF midPointForPoints(PointF p1, PointF p2) {
//        PointF tmp = new PointF();
//        tmp.x = (p1.x + p2.x) / 2;
//        tmp.y = (p1.y + p2.y) / 2;
//        return tmp;
//    }

    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int padding = 40;
        int h = MeasureSpec.getSize(heightMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec) - padding;
        if (h < 200) h = 200;
        if (w < this.displayWidth) w = (int)this.displayWidth;
        setMeasuredDimension(w, h);
    }

    public void addValue(float value) {
        Float val = Float.valueOf(value);
        this.dataPoints.add(val);
        if (dataPoints.size() > numberOfPoints + 1) dataPoints.remove(0);
        this.postInvalidate();
    }

    public void clearValues() {
        this.dataPoints.clear();
        this.postInvalidate();
    }

    public void setColor(int a,int r,int g, int b) {
        this.pointFillPaint.setARGB(a, r, g, b);
        this.sparkLineFillPaint.setARGB(7 * a / 10, r, g, b);
        this.sparkLinePaint.setARGB(a, r, g, b);
    }

    public void setStrokeWidth(float value) {
        this.sparkLinePaint.setStrokeWidth(value);
    }

    public void setColor(int color) {
        int a = (color >> 24) & 0xff;
        int r = (color >> 16) & 0xff;
        int g = (color >> 8) & 0xff;
        int b = (color) & 0xff;
        setColor(a, r, g, b);
    }

    public void setHasCircles(boolean hasCircles) {
        this.hasCircles = hasCircles;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }
}
