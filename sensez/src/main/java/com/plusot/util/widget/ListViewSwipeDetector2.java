package com.plusot.util.widget;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

import com.plusot.util.logging.LLog;

/**
 * Created by peet on 09-07-15.
 */
public class ListViewSwipeDetector2 implements View.OnTouchListener {
    private static final int MIN_LOCK_DISTANCE = 30; // disallow motion intercept
    private static final int MIN_MOVE = 10; // disallow motion intercept
    private boolean motionInterceptDisallowed = false;
    private float downX, upX;
    private View button;
    private final ListView listView;
    private boolean mayAct = false;
    private final Listener listener;

    public interface Listener {
        void onClick();
        void onRightSwipe();
    }

    public ListViewSwipeDetector2(final ListView listview, final View button, final Listener listener) {
        this.button = button;
        this.listView = listview;
        this.listener = listener;

    }

    public void setButton(View button) {
        this.button = button;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
//                LLog.i("down X = " + downX);
                mayAct = true;
                return true; // allow other events like Click to be processed

            case MotionEvent.ACTION_MOVE:
                upX = event.getX();
                float deltaX = upX - downX;
//                LLog.i("up X = " + upX + ", delta = " + deltaX);

                if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && listView != null && !motionInterceptDisallowed) {
                    listView.requestDisallowInterceptTouchEvent(true);
                    motionInterceptDisallowed = true;
                }

                if (mayAct) {
                    if (deltaX > MIN_MOVE) {
                        LLog.i("Button left made visible");
                        button.setVisibility(View.VISIBLE);
                        mayAct = false;
                    } else if (deltaX < -MIN_MOVE) {
                        LLog.i("Right swipe");
                        if (button.getVisibility() == View.VISIBLE) {
                            button.setVisibility(View.GONE);
                        } else {
                            listener.onRightSwipe();
                        }
                        mayAct = false;
                    }
                }

                return true;
            case MotionEvent.ACTION_UP:
                upX = event.getX();
                deltaX = Math.abs(upX - downX);


                if (listView != null) {
                    listView.requestDisallowInterceptTouchEvent(false);
                    motionInterceptDisallowed = false;
                }
                if (deltaX <= MIN_MOVE && listener != null) {
                    listener.onClick();
                }
                return true;
            case MotionEvent.ACTION_CANCEL:
                return false;
        }

        return true;
    }

}