package com.plusot.util.widget;

import android.view.MotionEvent;
import android.view.View;

import com.plusot.util.logging.LLog;

/**
 * Created by peet on 09-07-15.
 */
public class SwipeDetector implements View.OnTouchListener {
    private static final int MIN_MOVE = 10;
    private float downX, upX;
    private boolean mayAct = false;
    private final Listener listener;

    public interface Listener {
        void onClick();
        void onRightSwipe();
        void onLeftSwipe();
    }

    public SwipeDetector(final Listener listener) {
        this.listener = listener;
    }

//    public void setPosition(int pos) {
//        this.position = pos;
//    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                LLog.i("down X = " + downX);
                mayAct = true;
                return true; // allow other events like Click to be processed

            case MotionEvent.ACTION_MOVE:
                upX = event.getX();
                float deltaX = upX - downX;
                LLog.i("up X = " + upX + ", delta = " + deltaX);



                if (mayAct) {
                    if (deltaX > MIN_MOVE) {
                        listener.onRightSwipe();
                        mayAct = false;
                    } else if (deltaX < -MIN_MOVE) {
                        listener.onLeftSwipe();
                        mayAct = false;
                    }

                }

                return true;
            case MotionEvent.ACTION_UP:
                upX = event.getX();
                deltaX = Math.abs(upX - downX);

                if (deltaX <= MIN_MOVE && listener != null) {
                    listener.onClick();
                }
                return true;
            case MotionEvent.ACTION_CANCEL:
                return false;
        }

        return true;
    }

}