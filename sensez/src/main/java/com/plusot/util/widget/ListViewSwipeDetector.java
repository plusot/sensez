package com.plusot.util.widget;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

import com.plusot.sensez.R;
import com.plusot.util.logging.LLog;

public class ListViewSwipeDetector implements View.OnTouchListener {
    private static final int MIN_LOCK_DISTANCE = 30; // disallow motion intercept
    private static final int MIN_MOVE = 10; // disallow motion intercept
    private boolean motionInterceptDisallowed = false;
    private float downX, upX;
    private final View buttonLeft;
    private final View buttonRight;
    private final ListView listView;
    private boolean mayAct = false;
    private final Listener listener;

    public interface Listener {
        void onClick();
    }

    public ListViewSwipeDetector(final ListView listview, final View buttonLeft, View buttonRight, final Listener listener) {
        this.buttonLeft = buttonLeft;
        this.buttonRight = buttonRight;
        this.listView = listview;
        this.listener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
//                LLog.i("down X = " + downX);
                mayAct = true;
                return true; // allow other events like Click to be processed

            case MotionEvent.ACTION_MOVE:
                upX = event.getX();
                float deltaX = upX - downX;
//                LLog.i("up X = " + upX + ", delta = " + deltaX);

                if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && listView != null && !motionInterceptDisallowed) {
                    listView.requestDisallowInterceptTouchEvent(true);
                    motionInterceptDisallowed = true;
                }

                if (mayAct) {
                    if (deltaX > MIN_MOVE) {
                        if (buttonRight.getVisibility() == View.GONE) {
                           if (buttonLeft.getTag(R.id.maySwipe) instanceof Integer && ((Integer)buttonLeft.getTag(R.id.maySwipe)) == 1) {
                               LLog.i("Button left made visible");
                               buttonLeft.setVisibility(View.VISIBLE);
                           }
                        } else
                            buttonRight.setVisibility(View.GONE);
                        mayAct = false;
                    } else if (deltaX < -MIN_MOVE) {
                        if (buttonLeft.getVisibility() == View.GONE) {
                            if (buttonRight.getTag(R.id.maySwipe) instanceof Integer && ((Integer)buttonRight.getTag(R.id.maySwipe)) == 1) {
                                LLog.i("Button right made visible");
                                buttonRight.setVisibility(View.VISIBLE);
                            }
                        } else
                            buttonLeft.setVisibility(View.GONE);
                        mayAct = false;
                    }

                }

                return true;
            case MotionEvent.ACTION_UP:
                upX = event.getX();
                deltaX = Math.abs(upX - downX);


                if (listView != null) {
                    listView.requestDisallowInterceptTouchEvent(false);
                    motionInterceptDisallowed = false;
                }
                if (deltaX <= MIN_MOVE && listener != null) {
                    listener.onClick();
                }
                return true;
            case MotionEvent.ACTION_CANCEL:
                return false;
        }

        return true;
    }

}