package com.plusot.util.json;

import com.plusot.util.data.DataType;
import com.plusot.util.logging.LLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by peet on 04-11-14.
 */
public class JSoNHelper {


    public static class JSoNException extends Exception {
        public JSoNException(String message, Throwable cause) {
            super(message, cause);
        }
        public JSoNException(String message) {
            super(message);
        }
    }

    public static String decode(String value) throws UnsupportedEncodingException {
        return URLDecoder.decode(value, "UTF-8");
    }

    public static JSONObject fromString(String value)  {
        if (value == null || value.length() == 0) return new JSONObject();
        try {
            JSONObject json = new JSONObject(decode(value));
            return json;
        } catch (JSONException e) {
            LLog.e("Could not parseCharacteristic: " + value, e);
            return new JSONObject();
        } catch (UnsupportedEncodingException e) {
            LLog.e("Could not decode: " + value, e);
            return new JSONObject();
        }
    }

    public static Integer[] getIntArray(JSONObject json, String key) {
        try {
            JSONArray arr = json.getJSONArray(key);
            List<Integer> vals = new ArrayList<>();
            if (arr != null) for (int i = 0; i < arr.length(); i++) {
                int val;
                if ((val = arr.optInt(i, Integer.MIN_VALUE)) > Integer.MIN_VALUE) {
                    vals.add(val);
                }
            }
            return vals.toArray(new Integer[vals.size()]);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
        return null;
    }

    private static String toString(JSONObject json, String key, Object obj) throws JSONException, JSoNException {
        if (obj == null) throw new JSoNException("Could not get: " + key);
        if (obj instanceof String) return (String) obj;
        if (obj instanceof Double) return Double.toString(json.getDouble(key));
        if (obj instanceof Long) return Long.toString(json.getLong(key));
        if (obj instanceof Integer) return Integer.toString(json.getInt(key));
        if (obj instanceof Boolean) return Boolean.toString(json.getBoolean(key));
        return "";
    }

    private static String toString(JSONArray json, int i, Object obj) throws JSONException, JSoNException {
        if (obj == null) throw new JSoNException("Could not get array element: " + i);
        if (obj instanceof String) return (String) obj;
        if (obj instanceof Double) return Double.toString(json.getDouble(i));
        if (obj instanceof Long) return Long.toString(json.getLong(i));
        if (obj instanceof Integer) return Integer.toString(json.getInt(i));
        if (obj instanceof Boolean) return Boolean.toString(json.getBoolean(i));
        return "";
    }

    public static Map<String, String> fromJSONString(String value) throws JSoNException {
        try {
            JSONObject json = new JSONObject(JSoNHelper.decode(value));
            return fromJSON("", json);
        } catch (JSONException e) {
            throw new JSoNException("Could not parseCharacteristic: " + value, e);
        } catch (UnsupportedEncodingException e) {
            throw new JSoNException("Could not decode: " + value, e);
        }
    }

    private static void handleArray(String key, JSONArray arr, Map<String, String> map) throws JSONException, JSoNException {
        for (int i = 0; i < arr.length(); i++) {
            Object arrObj = arr.get(i);
            if (arrObj instanceof JSONObject)
                map.putAll(fromJSON(key + '_' + i, arr.getJSONObject(i)));
            else if (arrObj instanceof JSONArray) {
                JSONArray arr2 = ((JSONArray) arrObj).getJSONArray(i);
                handleArray(key + '_' + i, arr2, map);
            } else
                map.put(key, toString(arr, i, arrObj));
        }
    }

    private static Map<String, String> fromJSON(String masterKey, JSONObject json) throws JSoNException {
        Map<String, String> map = new HashMap<>();
        try {
            Iterator<String> keys = json.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                Object obj = json.get(key);
                if (obj instanceof JSONObject)
                    map.putAll(fromJSON(masterKey + key, json.getJSONObject(key)));
                else if (obj instanceof JSONArray) {
                    JSONArray arr = json.getJSONArray(key);
                    handleArray(masterKey + key, arr, map);
                } else
                    map.put(masterKey + key, toString(json, key, obj));

            }
        } catch (JSONException e) {
            throw new JSoNException("Could not parseCharacteristic JSON", e);
        }
        return map;

    }

    public static String get(JSONObject json, JSoNField key, String defaultValue) {
        if (json == null) return defaultValue;
        try {
            String string = null;
            if (json.has(key.toString())) string = json.getString(key.toString());
            if (string == null || string.length() == 0) return defaultValue;
            return string;
        } catch (JSONException e) {
            return defaultValue;
        }
    }


    public static JSONArray get(JSONObject json, JSoNField key) {
        if (json == null) return null;
        try {
            JSONArray arr = null;
            if (json.has(key.toString())) arr = json.getJSONArray(key.toString());
            if (arr == null || arr.length() == 0) return null;
            return arr;
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject getObj(JSONObject json, JSoNField key) {
        if (json == null) return null;
        try {
            JSONObject obj = null;
            if (json.has(key.toString())) obj = json.getJSONObject(key.toString());
            if (obj == null || obj.length() == 0) return null;
            return obj;
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject getObj(JSONObject json, String key) {
        if (json == null) return null;
        try {
            JSONObject obj = null;
            if (json.has(key)) obj = json.getJSONObject(key);
            if (obj == null || obj.length() == 0) return null;
            return obj;
        } catch (JSONException e) {
            return null;
        }
    }

    public static double get(JSONObject json, JSoNField key, double defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getDouble(key.toString());
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static double[] get(JSONObject json, JSoNField key, double[] defaultValue) {
        if (json == null) return defaultValue;
        try {
            JSONArray jArray = json.getJSONArray(key.toString());
            double[] values = new double[jArray.length()];
            for (int i = 0; i < jArray.length(); i++) {
                values[i] = jArray.getDouble(i);
            }
            return values;
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static int[] get(JSONObject json, JSoNField key, int[] defaultValue) {
        if (json == null) return defaultValue;
        try {
            JSONArray jArray = json.getJSONArray(key.toString());
            int[] values = new int[jArray.length()];
            for (int i = 0; i < jArray.length(); i++) {
                values[i] = jArray.getInt(i);
            }
            return values;
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static float[] get(JSONObject json, JSoNField key, float[] defaultValue) {
        if (json == null) return defaultValue;
        try {
            JSONArray jArray = json.getJSONArray(key.toString());
            float[] values = new float[jArray.length()];
            for (int i = 0; i < jArray.length(); i++) {
                values[i] = (float) jArray.getDouble(i);
            }
            return values;
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static int get(JSONObject json, JSoNField key, int defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getInt(key.toString());
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static long get(JSONObject json, JSoNField key, long defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getLong(key.toString());
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static String get(JSONArray json, int key, String defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getString(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static double get(JSONArray json, int key, double defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getDouble(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static int get(JSONArray json, int key, int defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getInt(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static long get(JSONArray json, int key, long defaultValue) {
        if (json == null) return defaultValue;
        try {
            return json.getLong(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static void set(JSONObject json, JSoNField key, Object value) {
        set(json, key.toString(), value);
    }

    public static void set(JSONObject json, JSoNField key, String value) {
        set(json, key.toString(), value);
    }

    public static void set(JSONObject json, JSoNField key, long value) {
        set(json, key.toString(), value);
    }

    public static void set(JSONObject json, DataType key, Object value) {
        set(json, key.name(), value);
    }

    public static void set(JSONObject json, String key, float[] values) {
        JSONArray array = new JSONArray();
        for (float value: values) {
            try {
                array.put(value);
            } catch (JSONException e) {
                LLog.e("JSONException " + e.getMessage());
            }
        }
        try {
            json.put(key.toLowerCase(), array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, double[] values) {
        JSONArray array = new JSONArray();
        for (double value: values) {
            try {
                array.put(value);
            } catch (JSONException e) {
                LLog.e("JSONException " + e.getMessage());
            }
        }
        try {
            json.put(key, array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, int[] values) {
        JSONArray array = new JSONArray();
        for (int value: values) {
            array.put(value);
        }
        try {
            json.put(key, array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, byte[] values) {
        JSONArray array = new JSONArray();
        for (byte value: values) {
            array.put(value);
        }
        try {
            json.put(key, array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, long[] values) {
        JSONArray array = new JSONArray();
        for (long value: values) {
            array.put(value);
        }
        try {
            json.put(key, array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, String[] values) {
        JSONArray array = new JSONArray();
        for (String value: values) {
            array.put(value);
        }
        try {
            json.put(key, array);
        } catch (JSONException e) {
            LLog.e("JSONException " + e.getMessage());
        }
    }

    public static void set(JSONObject json, String key, long value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            LLog.e("Could not convert " + value + " to JSON object");
        }
    }

    public static void set(JSONObject json, String key, String value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            LLog.e("Could not convert " + value + " to JSON object");
        }
    }

    public static void set(JSONObject json, String key, Object value) {
        if (value instanceof float[]) set(json, key, (float[]) value); else
        if (value instanceof double[]) set(json, key, (double[])value); else
        if (value instanceof String[]) set(json, key, (String[])value); else
        if (value instanceof int[]) set(json, key, (int[])value); else
        if (value instanceof byte[]) set(json, key, (byte[])value); else
        if (value instanceof long[]) set(json, key, (long[])value); else try {
            json.put(key, value);
        } catch (JSONException e) {
            LLog.e("Could not convert " + value + " to JSON object");
        }
    }
}
