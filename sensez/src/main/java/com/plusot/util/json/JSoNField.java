package com.plusot.util.json;

/**
 * Created by peet on 06-11-15.
 */
public enum JSoNField {
    NONE,
    ARRAY,
    DATA,
    VALUES,
    TIME,
    DATATYPE,
    VALUE,
    CLASS,
    UNIT,
    RESULT,
    INFO,
    NAME,
    DEVICEID,
    HUMANNAME,
    MODEL,
    ACTIVATORCOLOR,
    VOLUME,
    SENSORID,
    SESSIONID,
    ROOM,
    PASSWORD,
    ERROR,
    OK,
    ;
    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static JSoNField fromString(String value) {
        if (value == null) return NONE;
        for (JSoNField category: JSoNField.values()) {
            if (category.toString().equals(value.toLowerCase())) return category;
        }
        return NONE;
    }
}
