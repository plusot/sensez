package com.plusot.util.share

import android.Manifest
import android.accounts.AccountManager
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.plusot.sensez.R
import com.plusot.util.Globals
import com.plusot.util.kotlin.getString
import com.plusot.util.logging.LLog
import com.plusot.util.util.TimeUtil
import com.plusot.util.util.TimeUtil.formatTime
import com.plusot.util.util.ToastHelper
import java.io.File
import java.util.*

typealias MailCompleteListener = () -> Unit

class SendMail(
        val filePath: String,
        val subject: String,
        idMailContents: String = R.string.file_mail.getString(formatTime()),
        idSending: Int = R.string.sendingMail,
        idNoMail: Int = R.string.noMailToSend,
        listener: MailCompleteListener) {

    private var listener: MailCompleteListener? = null
    private val mailContents: String
    private val sending: String
    private val noMail: String
    private val address: String

    init {
        val appContext = Globals.getAppContext()
        this.listener = listener
        this.mailContents = idMailContents
        if (appContext != null) {
            this.sending = appContext.getString(idSending)
            this.noMail = appContext.getString(idNoMail)
        } else {
            this.sending = "Sending"
            this.noMail = "No mail"
        }
        this.address = plusotMail
        send()
    }

    private inner class Result internal constructor(internal val success: Boolean)

    private inner class SendMailTask : AsyncTask<Void, Void, Result>() {
        override fun doInBackground(vararg params: Void): Result {
            val appContext = Globals.getAppContext() ?: return Result(false)

            var name: String? = null
            if (ActivityCompat.checkSelfPermission(
                            appContext,
                            Manifest.permission.GET_ACCOUNTS
                    ) == PackageManager.PERMISSION_GRANTED
            ) {
                val manager = AccountManager.get(appContext)
                //Account[] accounts = manager.getAccounts();

                var accounts = manager.getAccountsByType("com.google") //manager.getAccounts();
                if (accounts.isEmpty())
                    accounts = manager.getAccountsByType("com.google.android.exchange")
                if (accounts.isEmpty())
                    accounts = manager.accounts
                for (account in accounts) {
                    LLog.i(".accounts: " + account.name + ", " + account.type)
                }
                if (accounts.isNotEmpty()) name = accounts[0].name
            }
            val success = mailMe(
                    arrayOf(address),
                    name,
                    subject,
                    mailContents,
                    arrayOf(filePath)
            )

            return Result(success)
        }

        override fun onPostExecute(result: Result) {
            if (!result.success) ToastHelper.showToastLong(noMail)
            listener?.invoke()
        }
    }

    private fun mailMe(
            emailTo: Array<String>,
            emailCC: String?,
            subject: String,
            emailText: String,
            filePaths: Array<String>
    ): Boolean {
        val appContext = Globals.getAppContext()
        val emailIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        emailIntent.type = "text/plain"
        //emailIntent.setType("application/zip"); message/rfc822
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo)
        if (emailCC != null) emailIntent.putExtra(Intent.EXTRA_CC, arrayOf(emailCC))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //		ArrayList<String> list = new ArrayList<String>();
        //		list.addWatchable(emailText);
        //		Log.i(" Email text = " + emailText);
        //		emailIntent.putStringArrayListExtra(android.content.Intent.EXTRA_TEXT, list); // emailText);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText)
        //has to be an ArrayList
        val uris = ArrayList<Uri>()
        //convert from paths to Android friendly Parcelable Uri's
        for (file in filePaths) {
            LLog.i("File $file")
            val fileIn = File(file)
            val uri: Uri
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M && appContext != null) {
                uri = FileProvider.getUriForFile(
                        appContext,
                        appContext.packageName + ".provider",
                        fileIn
                )
            } else
                uri = Uri.fromFile(fileIn)
            uris.add(uri)
        }
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris)
        return if (appContext != null)
            try {
                emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                appContext.startActivity(emailIntent) //Intent.createChooser(emailIntent, "Send mail..."));
                true
            } catch (e: Exception) {
                LLog.e(" Could not send email ", e)
                false
            }
        else
            false
    }

    private fun send() {
        ToastHelper.showToastShort(sending)
        SendMailTask().execute()
    }

    companion object {
        private const val plusotMail = "report@plusot.com"
    }
}
