package com.plusot.util.share;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.plusot.util.Globals;
import com.plusot.util.json.JSoNField;
import com.plusot.util.logging.LLog;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Tuple;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by peet on 07-07-15.
 */
public class Http {
    private static  Http INSTANCE = new Http();
    private static final Object instanceLock = new Object();
    private static final boolean DEBUG = false;
    private SSLContext sslContext = null;

    public interface Listener {
        void onResult(Error error, String info, JSONObject json);
    }

    public enum Error {
        NOERROR,
        JSONERROR,
        MALFORMEDURL,
        IOERROR,
        CERTIFICATEERROR,
        ;
    }

    public enum Method {
        POST,
        PUT,
        PATCH,
        GET
    }

    public static boolean isNetworkOnline() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return false;
        ConnectivityManager cm = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isAvailable() && netInfo.isConnected();
    }


    private Tuple<Error, String> get(String urlString) {
        HttpURLConnection urlConnection = null;
        byte[] buffer = new byte[4096];

        try {
            URL url = new URL(urlString);
            if (DEBUG) LLog.i("Sent in GET: " + url.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
//            urlConnection.setReadTimeout(30000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod("GET");
//            urlConnection.setDoOutput(false);
//            urlConnection.setDoInput(true);
//            urlConnection.setChunkedStreamingMode();
            urlConnection.connect();

            InputStream in = urlConnection.getInputStream();
            StringBuilder sb = new StringBuilder("");
            int totalBytes= 0;
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                String str = new String(buffer, 0, bytesRead);
                sb.append(str);
                totalBytes += bytesRead;
            }
            if (DEBUG) LLog.i("Bytes received in GET: " + totalBytes);

            in.close();
            return new Tuple<>(Error.NOERROR, sb.toString());

        } catch (MalformedURLException e) {
            return new Tuple<>(Error.MALFORMEDURL, e.getMessage());
        } catch (IOException e) {
            //LLog.e("IO Error", e);
            return new Tuple<>(Error.IOERROR, e.getMessage());

        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }
    }

    public Tuple<Error, String> sslGet(String urlString) {
        HttpsURLConnection urlConnection = null;
        byte[] buffer = new byte[4096];
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpsURLConnection) url.openConnection();
            //urlConnection.setSSLSocketFactory(getOpenSSLContext().getSocketFactory());
            urlConnection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostName, SSLSession sslSession) {
                    return true;
                }

            });
//            urlConnection.setReadTimeout(15000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod("GET");
//            urlConnection.setDoOutput(false);
//            urlConnection.setDoInput(true);


            urlConnection.connect();

            InputStream in = urlConnection.getInputStream();
            StringBuilder sb = new StringBuilder("");
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                String str = new String(buffer, 0, bytesRead);
                //LLog.i(Globals.TAG, "      " + str + " (" + bytesRead +')');
                sb.append(str);
            }
            in.close();
            return new Tuple<>(Error.NOERROR, sb.toString());

        } catch (MalformedURLException e) {
            return new Tuple<>(Error.MALFORMEDURL, e.getMessage());
        } catch (IOException e) {
            return new Tuple<>(Error.IOERROR, e.getMessage());
//        } catch (CertificateException e) {
//            return new Tuple<>(Error.CERTIFICATEERROR, e.getMessage());
//        } catch (NoSuchAlgorithmException e) {
//            return new Tuple<>(Error.CERTIFICATEERROR, e.getMessage());
//        } catch (KeyStoreException e) {
//            return new Tuple<>(Error.CERTIFICATEERROR, e.getMessage());
//        } catch (KeyManagementException e) {
//            return new Tuple<>(Error.CERTIFICATEERROR, e.getMessage());
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }
    }


    private Tuple<Error,String> post(Method method, String urlString, JSONObject json) {
        if (json == null) {
            LLog.e("json == null for post: " + urlString);
            return null;
        }
        HttpURLConnection urlConnection = null;
        byte[] buffer = new byte[4096];
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(60000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod(method.toString());
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            String str = json.toString();
            //LLog.i("Posting to " + urlString+ ":\n" + str);
            out.write(str.getBytes("UTF-8"));
            out.flush();
            out.close();
//            urlConnection.connect();

            InputStream in = urlConnection.getInputStream();
            StringBuilder sb = new StringBuilder("");
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                str = new String(buffer, 0, bytesRead);
                //LLog.i(str + " (" + bytesRead +')');
                sb.append(str);
            }
            in.close();
            //LLog.i("Received: " + sb.toString());
            return new Tuple<>(Error.NOERROR, sb.toString());

        } catch (MalformedURLException e) {
            return new Tuple<>(Error.MALFORMEDURL, e.getMessage());
        } catch (IOException e) {
//            LLog.i("IO Exception: " + e.getMessage());
            return new Tuple<>(Error.IOERROR, e.getMessage());
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }
    }

    private boolean _get(final String urlString, final Listener listener, boolean blocking) {
        if (urlString == null) return false;

        if (blocking)
            performBlocking(Method.GET, urlString, null, listener);
        else
            performNonBlocking(Method.GET, urlString, null, listener);
        return true;
    }

    public static boolean get(final String urlString, final Listener listener, boolean blocking) {
        boolean result;
        synchronized (instanceLock) {
            result = INSTANCE._get(urlString, listener, blocking);
        }
        return result;
    }


//    public void post(final Tuple<String, JSONObject> cmdAndJson, final Listener listener, boolean blocking) {
//        post(cmdAndJson.t1(), cmdAndJson.t2(), listener, blocking);
//    }

    private void _post(final String urlString, final JSONObject json, final Listener listener, boolean blocking) {
        if (urlString == null) return;
        if (blocking)
            performBlocking(Method.POST, urlString, json, listener);
        else
            performNonBlocking(Method.POST, urlString, json, listener);
    }

    public static void post(final String urlString, final JSONObject json, final Listener listener, boolean blocking) {
        synchronized (instanceLock) {
            INSTANCE._post(urlString, json, listener, blocking);
        }
    }

    private void performNonBlocking(final Method method, final String urlString, final JSONObject json, final Listener listener) {
        SleepAndWake.runInNewThread(new Runnable() {
            @Override
            public void run() {
                performBlocking(method, urlString, json, listener);
            }
        }, "Http.perform");
    }

    private void performBlocking(final Method method, final String urlString, final JSONObject json, final Listener listener) {
        final Tuple<Error, String> result = perform(method, urlString, json);
        if (result == null) {
            LLog.i("Result == null, http request not handled");
        } else if (result.t1() == Error.NOERROR) {
            if (DEBUG) {
                if (json == null)
                    LLog.i("Sent: " + urlString + ",\nResult: " + StringUtil.trimAll(result.t2()));
                else
                    LLog.i("Sent: " + urlString + "\n" + json.toString() + ",\nResult: " + StringUtil.trimAll(result.t2()));
            }
            try {
                JSONObject resultJson;
                if (result.t2().startsWith("[")) {
                    JSONArray jsonArray = new JSONArray(result.t2());
                    resultJson = new JSONObject();
                    resultJson.put(JSoNField.ARRAY.toString(), jsonArray);
                } else
                    resultJson = new JSONObject(result.t2());
                listener.onResult(result.t1(), result.t2(), resultJson);
            } catch (JSONException e) {
                listener.onResult(Error.JSONERROR, result.t2(), null);
                LLog.i("Could not parseCharacteristic to JSON: " + StringUtil.trimAll(result.t2()));
            }
        } else {
            if (DEBUG) {
                if (json == null)
                    LLog.i("Sent: " + urlString + ",\nError " + result.t1() + ",\nInfo: " + StringUtil.trimAll(result.t2()));
                else
                    LLog.i("Sent: " + urlString + "\n" + json.toString() + ",\nError " + result.t1() + ",\nInfo: " + StringUtil.trimAll(result.t2()));
            }
            listener.onResult(result.t1(), result.t2(), null);
        }
    }

    private Tuple<Error, String> perform(final Method method, final String urlString, final JSONObject json) {
        //LLog.i("Sending: " + urlString);
        switch (method) {
            case POST:
            case PUT:
            case PATCH:
                if (urlString.startsWith("https:")) return sslPost(method, urlString, json);
                return post(method, urlString, json);
            case GET:
            default:
                if (urlString.startsWith("https:"))  return sslGet(urlString);
                return get(urlString);
        }
    }

//    private HostnameVerifier hostnameVerifier = new HostnameVerifier() {
//        @Override
//        public boolean verify(String hostname, SSLSession session) {
//            HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
//            boolean result = hv.verify("localhost", session);
//            return result | true; //
//        }
//    };

    private Tuple<Error, String> sslPost(final Method method, String urlString, JSONObject json) {
        HttpsURLConnection urlConnection = null;
        byte[] buffer = new byte[4096];
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpsURLConnection) url.openConnection();
            //urlConnection.setSSLSocketFactory(getSSLContext().getSocketFactory());
            urlConnection.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }

            });
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod(method.toString());
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            out.write(json.toString().getBytes("UTF-8"));
            out.flush();
            out.close();
            urlConnection.connect();

            InputStream in = urlConnection.getInputStream();
            StringBuilder sb = new StringBuilder("");
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                String str = new String(buffer, 0, bytesRead);
                //LLog.i(Globals.TAG, "      " + str + " (" + bytesRead +')');
                sb.append(str);
            }
            in.close();
            return new Tuple<>(Error.NOERROR, sb.toString());

        } catch (MalformedURLException e) {
            return new Tuple<>(Error.MALFORMEDURL, e.getMessage());
        } catch (IOException e) {
            return new Tuple<>(Error.IOERROR, e.getMessage());
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }
    }

    private SSLContext getSSLContext() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        if (sslContext != null) return sslContext;
        Context appContext = Globals.getAppContext();
        if (appContext == null) return null;
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream input = new BufferedInputStream(appContext.getAssets().open("cert.crt"));
        Certificate ca = null;
        try{
            ca = cf.generateCertificate(input);
            LLog.i("ca=" + ((X509Certificate) ca).getSubjectDN());
        } finally {
            if (input != null) input.close();
        }
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keystore = KeyStore.getInstance(keyStoreType);
        keystore.load(null, null);
        keystore.setCertificateEntry("ca", ca);
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keystore);

        sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, tmf.getTrustManagers(), null);
        return sslContext;
    }

    private SSLContext getOpenSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        if (sslContext != null) return sslContext;
        TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[] { tm }, null);
        return sslContext;

    }

}
