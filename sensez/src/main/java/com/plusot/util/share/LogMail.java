package com.plusot.util.share;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.SimpleLog;
import com.plusot.util.util.FileExplorer;
import com.plusot.util.util.FileUtil;
import com.plusot.util.util.TimeUtil;
import com.plusot.util.util.ToastHelper;
import com.plusot.util.util.Watchdog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class LogMail {
    private static final String CLASSTAG = LogMail.class.getSimpleName();
    private static final String PLUSOTMAIL = "report@plusot.com";

    private final Listener listener;
    private final String mailContents;
    private final String sending;
    private final String no_mail;
    private final String address;

    private static Set<String> files = new HashSet<>();
    private static final Object filesLock = new Object();
    private static long filesMade = 0;
    private static final long NEWZIP_INTERVAL = 300000;



    public interface Listener {
        void onMailComplete();
    }

    @SuppressWarnings("unused")
    public LogMail(final String mailContents, final String sending, final String no_mail, final Listener listener) {
        this.listener = listener;
//		this.context = context;
        this.mailContents = mailContents;
        this.sending = sending;
        this.no_mail = no_mail;
        this.address = PLUSOTMAIL;

        send();
    }

    public LogMail(final String mailContents, final int idSending, final int idNoMail, final Listener listener) {
        Context appContext = Globals.getAppContext();
        this.listener = listener;
        this.mailContents = mailContents;
        if (appContext != null) {
            this.sending = appContext.getString(idSending);
            this.no_mail = appContext.getString(idNoMail);
        } else {
            this.sending = "Sending";
            this.no_mail = "No mail";
        }
        this.address = PLUSOTMAIL;
        send();
    }

    private class Result {
        final boolean success;

        Result(final boolean success) {
            this.success = success;
        }
    }

    private static String[] createFileList() {
        String[] result;
        synchronized (filesLock) {
            long now = System.currentTimeMillis();
            if (now - filesMade > NEWZIP_INTERVAL) {
                filesMade = System.currentTimeMillis();
                files.clear();
                LLog.i(".createZipFile: Creating new file list");

                FileUtil.systemLogToFile();
                FileExplorer exp = new FileExplorer(Globals.getLogPath(), Globals.LOG_SPEC, Globals.LOG_EXT, 0);
                String[] logs = exp.getFileList(now);
                if (logs != null) for (String file : logs) {
                    files.add(Globals.getLogPath() + file);
                }

            }
            if (SimpleLog.hasInstances()) {
                files.addAll(SimpleLog.getFileNames(null));
                SimpleLog.closeInstances();
            }
            result = files.toArray(new String[0]);
        }
//		if (zipIt) {
//			String zipFile = getZipFileName(timePart(), false);
//			FileUtil.toZip(result, zipFile);
//			LLog.i(".zipFile: Zipped new files to " + zipFile);
//			result = new String[]{ zipFile };
//		}
        return result;
    }

    private class SendMailTask extends AsyncTask<Void, Void, Result> {
        @Override
        protected Result doInBackground(Void... params) {
            Context appContext =Globals.getAppContext();
            if (appContext == null) return new Result(false);

            int id = Watchdog.addProcess(CLASSTAG + ".SendMailTask");
            String[] files = createFileList();
            for (String file : files) {
                LLog.i("ListFiles: " + file);
            }

            String name = null;
            if (ActivityCompat.checkSelfPermission(appContext, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                AccountManager manager = AccountManager.get(appContext);
                //Account[] accounts = manager.getAccounts();
                Account[] accounts = null;

                accounts = manager.getAccountsByType("com.google"); //manager.getAccounts();
                if (accounts.length == 0)
                    accounts = manager.getAccountsByType("com.google.android.exchange");
                if (accounts.length == 0)
                    accounts = manager.getAccounts();
                for (Account account : accounts) {
                    LLog.i(".accounts: " + account.name + ", " + account.type);
                }
                if (accounts.length > 0) name = accounts[0].name;
            }
            boolean success  = mailMe(
                    new String[]{address},
                    name,
                    Globals.TAG + "_" + TimeUtil.formatTime(System.currentTimeMillis()),
                    mailContents,
                    files);

            Watchdog.removeProcess(id, CLASSTAG);
            if (listener != null) listener.onMailComplete();
            return new Result(success);
    }

    protected void onPostExecute(Result result) {
        if (!result.success) ToastHelper.showToastLong(no_mail);
        if (listener != null) listener.onMailComplete();
    }
}

    private boolean mailMe(String[] emailTo, String emailCC, String subject, String emailText, String[] filePaths) {
        Context appContext = Globals.getAppContext();
        final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/plain");
        //emailIntent.setType("application/zip"); message/rfc822
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo);
        if (emailCC != null) emailIntent.putExtra(Intent.EXTRA_CC, new String[]{emailCC});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        //		ArrayList<String> list = new ArrayList<String>();
        //		list.addWatchable(emailText);
        //		Log.i(" Email text = " + emailText);
        //		emailIntent.putStringArrayListExtra(android.content.Intent.EXTRA_TEXT, list); // emailText);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);
        //has to be an ArrayList
        ArrayList<Uri> uris = new ArrayList<>();
        //convert from paths to Android friendly Parcelable Uri's
        for (String file : filePaths) if (file != null) {
            LLog.i("File " + file);
            File fileIn = new File(file);
            Uri uri;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M && appContext != null) {
                uri = FileProvider.getUriForFile(appContext,
                        appContext.getPackageName() + ".provider",
                        fileIn);
            } else
                uri = Uri.fromFile(fileIn);
            uris.add(uri);
        }
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        if (appContext != null) try {
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(emailIntent); //Intent.createChooser(emailIntent, "Send mail..."));
            return true;
        } catch (Exception e) {
            LLog.e(" Could not send email ", e);
            return false;
        } else
            return false;
    }

    private void send() {
        ToastHelper.showToastShort(sending);
        new SendMailTask().execute();
    }
}
