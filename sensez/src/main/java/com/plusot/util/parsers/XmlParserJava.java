package com.plusot.util.parsers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class XmlParserJava {
    private XmlNode rootNode = null;
    private XmlNode currentNode = null;
    private State state = State.AWAITING_NODE;
    StringBuilder tokenString = new StringBuilder();
    boolean created = false;
    String attributeName = null;
    private final Listener listener;

    public interface Listener {
        //void onNodeAttribute(XmlNode node, Attribute attribute);
        void onNodeComplete(XmlNode node);
        void onNodeInit(XmlNode node);
    }

    enum State {
        NODE_START,
        NODE_SINGULAR,
        NODE_END,
        NODE_VALUE,
        AFTER_VALUE,
        AWAITING_NAME,
        AWAITING_VALUE,
        ATTRIBUTE_NAME,
        ATTRIBUTE_VALUE,
        AWAITING_NODE
        ;

    }

    public class XmlNode {
        final String name;
        final XmlNode parent;
        Set<XmlNode> children = null;
        Map<String, Attribute> attributes = new HashMap<>();


        XmlNode(String name, XmlNode parent) {
            this.name = name;
            this.parent = parent;
            if (parent == null) {
                rootNode = this;
            } else {
                parent.addChild(this);
            }
            if (listener != null) listener.onNodeInit(currentNode);
        }

//        public void addAttribute(Attribute attribute) {
//            attributes.add(attribute);
//        }

        Attribute addAttribute(String name, String value) {
            Attribute<String> attribute  = new Attribute<>(name);
            attributes.put(name, attribute);
            attribute.setValue(value);
            return attribute;
        }

        Attribute addAttribute(String name, double value) {
            Attribute<Double> attribute = new Attribute<>(name);
            attributes.put(name, attribute);
            attribute.setValue(value);
            return attribute;
        }

        Attribute addAttribute(String name, long value) {
            Attribute<Long> attribute = new Attribute<>(name);
            attributes.put(name, attribute);
            attribute.setValue(value);
            return attribute;
        }

        public Attribute getAttribute(String name) {
            return attributes.get(name);
        }

        public Double getDouble(String name) {
            Attribute<Double> attribute = attributes.get(name);
            if (attribute != null && attribute.getValueClass().equals(Double.class)) {
                return attribute.getValue();
            }
            return null;
        }

        public String getString(String name) {
            Attribute<String> attribute = attributes.get(name);
            if (attribute != null && attribute.getValueClass().equals(String.class)) {
                return attribute.getValue();
            }
            return null;
        }

        void addChild(XmlNode node) {
            if (children == null) children = new HashSet<>();
            children.add(node);
        }

        public XmlNode getChild(String name) {
            for (XmlNode node: children) {
                if (node.getName().equals(name)) return node;
            }
            return null;
        }

        private String _toString(String prefix) {
            StringBuilder sb = new StringBuilder();
            sb.append(prefix);
            sb.append(name);
            sb.append(" {\r\n");
            for (Attribute attr : attributes.values()) {
                sb.append(prefix);
                sb.append("   ");
                sb.append(attr.toString());
                sb.append("\r\n");
            }
            if (children != null) for (XmlNode child : children) {
                sb.append(child._toString(prefix + "   "));
            }
            sb.append(prefix);
            sb.append("}\r\n");


            return sb.toString();
        }

        @Override public String toString() {
            return _toString("");
        }

        public String getName() {
            return name;
        }
    }

    public static class Attribute<T extends Object> {
        private final String name;
        private T value;

        public Attribute(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public Class getValueClass() {
            return value.getClass();
        }

        @Override public String toString() {
            return name + "="  + value; // + " " + value.getClass();
        }
    }

    private void createAttribute(String attributeName, StringBuilder tokenString) {
        if (currentNode == null || tokenString == null || tokenString.length() == 0) return;
        String str = tokenString.toString();
        Attribute attribute = null;
        if (str.contains(".")) {
            try {
                double dValue = Double.parseDouble(str);
                attribute = currentNode.addAttribute(attributeName, dValue);
            } catch (NumberFormatException e) {
                attribute = currentNode.addAttribute(attributeName, str);
            }
        } else {
            try {
                long lValue = Long.parseLong(str);
                attribute = currentNode.addAttribute(attributeName, lValue);
            } catch (NumberFormatException e) {
                attribute = currentNode.addAttribute(attributeName, str);
            }
        }
        //if (listener != null) listener.onNodeAttribute(currentNode, attribute);
    }

    public XmlParserJava(Listener listener) {
        this.listener = listener;
    }

    public void parse(String string) {
        int length = string.length();

        for (int i = 0; i < length; i++) {
            char ch = string.charAt(i);
            switch (ch) {
                case '<':
                    switch (state) {
                        case NODE_VALUE:
                            createAttribute("_", tokenString);
                        case AWAITING_NODE:
                            state = State.NODE_START;
                            tokenString = new StringBuilder();
                            break;
                        case ATTRIBUTE_VALUE:
                            tokenString.append(ch);
                            break;
                        default:
                            break;
                    }
                    break;
                case '>':
                    switch (state) {
                        case AFTER_VALUE:
                            state = State.AWAITING_NODE;
                            break;
                        case NODE_START:
                            if (tokenString.length() > 0 && !created) {
                                currentNode = new XmlNode(tokenString.toString(), currentNode);
                            }
                            state = State.AWAITING_NODE;
                            break;
                        case NODE_SINGULAR:
                            if (tokenString.length() > 0 && !created) {
                                XmlNode node = new XmlNode(tokenString.toString(), currentNode);
                                if (listener != null) listener.onNodeComplete(node);
                            }
                            state = State.AWAITING_NODE;
                            break;
                        case NODE_END:
                            if (currentNode != null && tokenString.toString().equals(currentNode.name)) {
                                if (listener != null) listener.onNodeComplete(currentNode);
                                currentNode = currentNode.parent;
                                state = State.AWAITING_NODE;
                            }
                            break;
                        case ATTRIBUTE_VALUE:
                            tokenString.append(ch);
                            break;
                        default:
                            break;
                    }
                    created = false;
                    break;
                case ' ':
                    switch (state) {
                        case NODE_START:
                            if (!created && tokenString.length() > 0) {
                                currentNode = new XmlNode(tokenString.toString(), currentNode);
                            }
                            created = true;
                            state = State.AWAITING_NAME;
                            break;
                        case NODE_SINGULAR:
                            if (!created && tokenString.length() > 0) {
                                new XmlNode(tokenString.toString(), currentNode);
                            }
                            created = true;
                            state = State.AWAITING_NAME;
                            break;
                        case ATTRIBUTE_VALUE:
                            tokenString.append(ch);
                            break;
                        case AFTER_VALUE:
                            state = State.AWAITING_NAME;
                            break;
                        default:
                            break;
                    }
                    break;
                case '=':
                    switch (state) {
                        case ATTRIBUTE_NAME:
                            if (tokenString.length() > 0) {
                                attributeName = tokenString.toString();
                            }
                            tokenString = new StringBuilder();
                            state = State.AWAITING_VALUE;
                            break;
                        default:
                        case ATTRIBUTE_VALUE:
                            tokenString.append(ch);
                            break;
                    }

                    break;
                case '/':
                    switch (state) {
                        case NODE_START:
                            if (tokenString.length() == 0)
                                state = State.NODE_END;
                            else
                                state = State.NODE_SINGULAR;
                            break;
                        case ATTRIBUTE_VALUE:
                            tokenString.append(ch);
                            break;
                        default:
                            break;
                    }
                    break;
                case '"':
                    switch (state) {
                        case ATTRIBUTE_VALUE:
                            if (    tokenString.length() > 1 &&
                                    tokenString.charAt(tokenString.length() - 1) == '\\' &&
                                    (tokenString.length() < 2 || tokenString.charAt(tokenString.length() - 2) != '\\'))
                                tokenString.append(ch);
                            else if (currentNode != null) {
                                createAttribute(attributeName, tokenString);
                                state=State.AFTER_VALUE;
                            }
                            break;
                        default:
                            state = State.ATTRIBUTE_VALUE;
                            break;
                    }
                    break;
                default:
                    switch (state) {
                        case AWAITING_NODE:
                            tokenString = new StringBuilder();
                            tokenString.append(ch);
                            state = State.NODE_VALUE;
                            break;
                        case AWAITING_NAME:
                            tokenString = new StringBuilder();
                            state = State.ATTRIBUTE_NAME;
                            tokenString.append(ch);
                            break;
                        default:
                            tokenString.append(ch);
                            break;
                    }
            }
        }

    }

    @Override public String toString() {
        if (rootNode == null) return "no xml";
        return rootNode.toString();
    }

}

