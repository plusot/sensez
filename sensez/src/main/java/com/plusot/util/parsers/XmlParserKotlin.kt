package com.plusot.util.parsers

class XmlParser(private val onNode: (node: XmlNode, event: NodeEvent) -> Unit) {
    private var rootNode : XmlNode? = null
    private var currentNode : XmlNode? = null
    private var state : State = State.AWAITING_NODE
    private var tokenString : String? = null
    private var attributeName : String? = null
    private var created: Boolean = false

    enum class NodeType {
        NORMAL_NODE,
        COMMENT_NODE,
        ;
    }

    enum class State {
        NODE_START,
        NODE_SINGULAR,
        NODE_END,
        NODE_VALUE,
        NODE_COMMENT_START,
        AFTER_VALUE,
        AWAITING_NAME,
        AWAITING_VALUE,
        ATTRIBUTE_NAME,
        ATTRIBUTE_VALUE,
        AWAITING_NODE

    }

    enum class NodeEvent {
        INIT,
        COMPLETE
    }

    abstract class AbstractAttribute (private val name: String){
        abstract val value: String

        override fun toString(): String {
            return name + " = "  + value // + " " + value.getClass()
        }

    }

    class Attribute<out T>(name: String, val realValue: T) : AbstractAttribute(name) {
        override val value: String
            get() = realValue.toString()
    }

    inner class XmlNode (val name: String, val parent: XmlNode? = null, val nodeType : NodeType = parent?.nodeType ?: NodeType.NORMAL_NODE){
        private val children : MutableSet<XmlNode> = hashSetOf()
        private val attributes : MutableMap<String, AbstractAttribute> = hashMapOf()

        init {
            if (parent == null) {
                rootNode = this
            } else {
                parent.addChild(this)
            }
            //if (parent?.nodeType == NodeType.COMMENT_NODE) nodeType = NodeType.COMMENT_NODE
            onNode(this, NodeEvent.INIT)
        }

        private fun addChild(node: XmlNode) {
            children.add(node)
        }

        fun <T>addAttribute(name : String, value : T) : Attribute<T> {
            val attribute = Attribute(name, value)
            attributes[name] = attribute
            return attribute
        }

        @Suppress("unused")
        fun getAttribute(name : String): AbstractAttribute? {
            return attributes[name]
        }

        @Suppress("unused")
        fun getDouble(name: String): Double? {
            val attribute = attributes[name]
            attribute?.let {
                if (it is Attribute<*>) when (it.realValue) {
                    is Double -> return it.realValue
                    is Float -> return it.realValue.toDouble()
                    is Int -> return it.realValue.toDouble()
                    is Long -> return it.realValue.toDouble()
                }
            }
            return null
        }

        @Suppress("unused")
        fun getString(name: String): String? {
            val attribute = attributes[name]
            attribute?.let {
                if (it is Attribute<*> && it.realValue is String) return it.realValue
            }
            return null
        }

        @Suppress("unused")
        fun getLong(name: String): Long? {
            val attribute = attributes[name]
            attribute?.let {
                if (it is Attribute<*>) when (it.realValue) {
                    is Long -> return it.realValue
                    is Int -> return it.realValue.toLong()
                    is Float -> return it.realValue.toLong()
                    is Double -> return it.realValue.toLong()
                }
            }
            return null
        }

        @Suppress("unused")
        fun getInt(name: String): Int? {
            val attribute = attributes[name]
            attribute?.let {
                if (it is Attribute<*>) when (it.realValue) {
                    is Long -> return it.realValue.toInt()
                    is Int -> return it.realValue
                    is Double -> return it.realValue.toInt()
                    is Float -> return it.realValue.toInt()
                }
            }
            return null
        }

        @Suppress("unused")
        fun getChild(name: String): XmlNode? {
            return children.firstOrNull { it.name == name }
        }

        private fun expandString(prefix: String?): String {
            var sb = ""
            val delim = if (prefix == null) " " else "\r\n"
            if (prefix != null) sb += prefix
            sb += name
            sb += " {"
            sb += delim

            for (attr in attributes.values) {
                if (prefix != null) {
                    sb += prefix
                    sb += "   "
                }
                sb += attr.toString()
                sb += delim
            }

            if (prefix == null) for (child in children) {
                sb += child.expandString(null)
            } else {
                for (child in children) {
                    sb += child.expandString("$prefix   ")
                }
                sb += prefix
            }

            sb += "}"
            sb += delim
            return sb
        }

        override fun toString(): String {
            return expandString(null)
        }
    }

    private fun createAttribute(name: String, value: String) {
        if (currentNode == null || value.isEmpty()) return
        if (value.contains(".")) {
            try {
                currentNode?.addAttribute(name, value.toDouble())
            } catch (e: NumberFormatException) {
                currentNode?.addAttribute(name, value)
            }
        } else {
            try {
                currentNode?.addAttribute(name, value.toLong())
            } catch (e: NumberFormatException) {
                currentNode?.addAttribute(name, value)
            }
        }
    }

    fun parse(string: String) {
        for (ch in string) when (ch) {
            '<' -> when (state) {
                State.NODE_VALUE -> {
                    tokenString?.let { createAttribute("_", it) }
                    state = State.NODE_START
                    tokenString = ""
                }
                State.AWAITING_NODE -> {
                    state = State.NODE_START
                    tokenString = ""
                }
                else -> tokenString += ch
            }
            '>' -> {
                when (state) {
                    State.AFTER_VALUE -> state = State.AWAITING_NODE
                    State.NODE_START -> {
                        if (!created && tokenString?.isEmpty() == false) tokenString?.let {
                            currentNode = XmlNode(it, currentNode)
                        }
                        state = State.AWAITING_NODE
                    }
                    State.NODE_COMMENT_START -> {
                        if (!created && tokenString?.isEmpty() == false) tokenString?.let {
                            currentNode = XmlNode(it, currentNode, NodeType.COMMENT_NODE)
                        }
                        state = State.AWAITING_NODE
                    }
                    State.NODE_SINGULAR -> {
                        if (!created && tokenString?.isEmpty() == false) tokenString?.let {

                            val node = XmlNode(it, currentNode)
                            onNode(node, NodeEvent.COMPLETE)
                        } else if (created) currentNode?.let{
                            onNode(it, NodeEvent.COMPLETE)
                            currentNode = it.parent
                        }
                        state = State.AWAITING_NODE
                    }

                    State.NODE_END -> currentNode?.let {
                        if (tokenString.equals(it.name)) {
                            onNode(it, NodeEvent.COMPLETE)
                            currentNode = it.parent
                            state = State.AWAITING_NODE
                        }
                    }
                    else -> tokenString += ch
                }
                created = false
            }
            ' ' -> when (state) {
                State.NODE_START -> {
                    tokenString?.let {
                        if (!created && it.isNotEmpty()) currentNode = XmlNode(it, currentNode)
                    }
                    created = true
                    state = State.AWAITING_NAME
                }
                State.NODE_COMMENT_START -> {
                    tokenString?.let {
                        if (!created && it.isNotEmpty()) currentNode = XmlNode(it, currentNode, NodeType.COMMENT_NODE)
                    }
                    created = true
                    state = State.AWAITING_NAME
                }
                State.NODE_SINGULAR -> {
                    tokenString?.let {
                        if (!created && it.isNotEmpty()) XmlNode(it, currentNode)
                        created = true
                        state = State.AWAITING_NAME
                    }
                }
                State.AFTER_VALUE -> state = State.AWAITING_NAME
                else -> tokenString += ch

            }
            '=' -> when (state) {
                State.ATTRIBUTE_NAME -> {
                    if (tokenString?.isEmpty() == false) attributeName = tokenString
                    tokenString = ""
                    state = State.AWAITING_VALUE
                }
                else -> tokenString += ch
            }
            '/' ->
                when (state) {
                    State.AFTER_VALUE -> {
                        state = State.NODE_SINGULAR
                    }
                    State.NODE_START -> {
                        state = if (tokenString?.isEmpty() == true)
                            State.NODE_END
                        else
                            State.NODE_SINGULAR
                    }
                    State.AWAITING_NAME -> {
                        state = State.NODE_SINGULAR
                    }
                    else -> tokenString += ch
                }
            '"' ->
                when (state) {
                    State.NODE_VALUE -> {
                        tokenString += ch
                    }
                    State.ATTRIBUTE_VALUE -> {
                        tokenString?.let {
                            if (!it.isEmpty() && it[it.length - 1] == '\\' && (it.length < 2 || it[it.length - 2] != '\\'))
                                tokenString += ch
                            else if (currentNode != null) {
                                val token = tokenString
                                if (token != null) attributeName?.let {
                                    createAttribute(it, token)
                                }
                                state = State.AFTER_VALUE
                            }
                        }
                    }
                    else -> {
                        state = State.ATTRIBUTE_VALUE
                    }
                }
            '!' ->
                when (state) {
                    State.NODE_START -> {
                        state = State.NODE_COMMENT_START
                    }
                    else -> {
                        tokenString += ch
                    }
                }
            '-'->
                when (state) {
                    State.NODE_COMMENT_START -> {

                    }
                    State.NODE_END -> {
                        if (currentNode?.nodeType != NodeType.COMMENT_NODE) {
                            tokenString += ch
                        }
                    }
                    else -> {
                        tokenString += ch
                    }
                }
            else -> {
                when (state) {
                    State.AWAITING_NODE -> {
                        state = State.NODE_VALUE
                        tokenString = ch.toString()
                    }
                    State.AWAITING_NAME -> {
                        state = State.ATTRIBUTE_NAME
                        tokenString = ch.toString()
                    }
                    else -> tokenString += ch
                }

            }
        }
    }

    override fun toString(): String {
        if (rootNode == null) return "no xml"
        return rootNode.toString()
    }
}