package com.plusot.util.preferences;

import android.preference.Preference;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

/**
 * Created by peet on 11-11-15.
 */
public class PreferenceKeyElements {

    private final long flags;
    private final String label;
    private final Class<?> valueClass;
    private final Object defaultValue;
    private Object fallBackValue;

    public PreferenceKeyElements(final String label, final Object defaultValue, final Class<?> valueClass, final long flags) {
        this(label, defaultValue, null, valueClass, flags);
    }

    public PreferenceKeyElements(final String label, final Object defaultValue, final Object fallBackValue, final Class<?> valueClass, final long flags) {
        this.label = label;
        this.defaultValue = defaultValue;
        this.fallBackValue = fallBackValue;
        this.valueClass = valueClass;
        this.flags = flags;
    }

	/*
     * Generic PreferenceKey methods
	 */


    public long getFlags() {
        return flags;
    }

    public String getLabel() {
        return label;
    }


    private int getDefaultIntValue() {
        if (defaultValue instanceof Integer) {
            return (Integer) defaultValue;
        }
        return 0;
    }

    private boolean getDefaultBoolValue() {
        if (defaultValue instanceof Boolean) {
            return (Boolean) defaultValue;
        }
        return false;
    }

    private Set<String> getDefaultSet() {
        if (defaultValue instanceof Set<?>) {
            try {
                return (Set<String>) defaultValue;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public String getFallbackStringValue() {
        if (fallBackValue instanceof String) {
            return (String) fallBackValue;
        } else if (fallBackValue instanceof Integer) {
            return String.valueOf(fallBackValue);
        } else if (fallBackValue instanceof Double) {
            return String.valueOf(fallBackValue);
        } else if (fallBackValue instanceof Float) {
            return String.valueOf(fallBackValue);
        } else if (fallBackValue instanceof Long) {
            return String.valueOf(fallBackValue);
        }
        return null;
    }

    private String getDefaultStringValue() {
        if (defaultValue instanceof String) {
            return (String) defaultValue;
        } else if (defaultValue instanceof Integer) {
            return String.valueOf(defaultValue);
        } else if (defaultValue instanceof Double) {
            return String.valueOf(defaultValue);
        } else if (defaultValue instanceof Float) {
            return String.valueOf(defaultValue);
        } else if (defaultValue instanceof Long) {
            return String.valueOf(defaultValue);
        }
        return null;
    }

    private double getDefaultDoubleValue() {
        if (defaultValue != null && defaultValue instanceof Double) {
            return (Double) defaultValue;
        }
        return 0;
    }

    private double getDefaultFloatValue() {
        if (defaultValue != null && defaultValue instanceof Float) {
            return (Float) defaultValue;
        }
        return 0;
    }

//    public EnumSet<?> getDefaultEnumSetValue() {
//        if (defaultValue instanceof EnumSet<?>) {
//            return (EnumSet<?>) defaultValue;
//        }
//        return null;
//    }

    private long getDefaultLongValue() {
        if (defaultValue != null && defaultValue instanceof Long) {
            return (Long) defaultValue;
        }
        return 0;
    }

//    public Class<?> getValueClass() {
//        return valueClass;
//    }

    @Override
    public String toString() {
        return label;
    }

    public boolean isTrue() {
        //LLog.i("getBoolean: This is " + this);
        return PreferenceHelper.get(getKey(), this.getDefaultBoolValue());
    }

    public void get() {
        if (valueClass.equals(Long.class)) {
            PreferenceHelper.get(getKey(), this.getDefaultLongValue());
        } else if (valueClass.equals(Integer.class)) {
            if ((flags & PreferenceDefaults.FLAG_ISARRAY) > 0)
                PreferenceHelper.get(getKey(), this.getDefaultStringValue());
            else
                PreferenceHelper.get(getKey(), this.getDefaultIntValue());
        } if (valueClass.equals(int.class)) {
            if ((flags & PreferenceDefaults.FLAG_ISARRAY) > 0)
                PreferenceHelper.get(getKey(), this.getDefaultStringValue());
            else
                PreferenceHelper.get(getKey(), this.getDefaultIntValue());
        } else if (valueClass.equals(Boolean.class)) {
            PreferenceHelper.get(getKey(), this.getDefaultBoolValue());
        } else if (valueClass.equals(String.class)) {
            if ((flags & PreferenceDefaults.FLAG_ISSTRINGSET) > 0)
                PreferenceHelper.getStringSet(getKey(), this.getDefaultSet());
            else
                PreferenceHelper.get(getKey(), this.getDefaultStringValue());
        } else if (valueClass.equals(Double.class)) {
            PreferenceHelper.get(getKey(), this.getDefaultDoubleValue());
        } else if (valueClass.equals(Float.class)) {
            PreferenceHelper.get(getKey(), this.getDefaultFloatValue());
        } else if (valueClass.equals(Set.class)) {
            PreferenceHelper.getStringSet(getKey(), this.getDefaultSet());
        }
    }

    public String getString() {
        return PreferenceHelper.get(getKey(), this.getDefaultStringValue());
    }


    public String getStringCheckUnset(String unsetCheck) {
        String value = getString();
        if (value.contains(unsetCheck)) return getFallbackStringValue();
        return value;
    }

    public String getString(int index) {
        return PreferenceHelper.get(getKey(), index, this.getDefaultStringValue());
    }

    public String getString(final String defaultValue) {
        return PreferenceHelper.get(getKey(), defaultValue);
    }

    public String[] getStrings() {
        String str = getString();
        return str.split(";");
    }

    public List<String> getStringList() {
        return Arrays.asList(getStrings());
    }

    @SuppressWarnings("unchecked")
    public Set<String> getStringSet() {
        if (defaultValue instanceof Set<?>)
            return PreferenceHelper.getStringSet(this.toString(), (Set<String>) defaultValue);
        return null;
    }

    public String getStringFromSet(int index) {
        if ((flags & PreferenceDefaults.FLAG_ISSTRINGSET) == 0) return null;
        return PreferenceHelper.getStringFromSet(this.toString(), index);
    }

    public boolean addStringToSet(String value) {
        if ((flags & PreferenceDefaults.FLAG_ISSTRINGSET) == 0) return false;
        return PreferenceHelper.addToSet(this.toString(), value);
    }

    public void deleteStringFromSet(String value) {
        if ((flags & PreferenceDefaults.FLAG_ISSTRINGSET) == 0) return;
        PreferenceHelper.deleteFromSet(this.toString(), value);
    }

    public long getLong() {
        //LLog.i("getLong: " + this);
        return PreferenceHelper.get(getKey(), this.getDefaultLongValue());
    }

    public double getDouble() {
        //LLog.i("getDouble: This is " + this);
        return PreferenceHelper.get(getKey(), this.getDefaultDoubleValue());
    }

    public boolean getBoolean() {
        //LLog.i("getDouble: This is " + this);
        return PreferenceHelper.get(getKey(), this.getDefaultBoolValue());
    }

    private String getKey() {
        return this.toString();
    }

    public int getInt() {
        if (!valueClass.equals(Integer.class) && !valueClass.equals(int.class)) return 0;
        //LLog.i("getInt: This is " + this);

        if ((flags & PreferenceDefaults.FLAG_ISARRAY) > 0)
            return PreferenceHelper.getFromListArray(getKey(), (Integer) defaultValue);
        return PreferenceHelper.get(getKey(), this.getDefaultIntValue());
    }

    public void set(long value) {
        PreferenceHelper.set(getKey(), value);
    }

    public boolean set(int value) {
        if (getInt() == value) return false;
        if ((flags & PreferenceDefaults.FLAG_ISARRAY) > 0)
            PreferenceHelper.setFromListArray(getKey(), value);
        else
            PreferenceHelper.set(getKey(), value);
        return true;
    }

    public void set(boolean value) {
        PreferenceHelper.set(getKey(), value);
    }

    public void set(double value) {
        PreferenceHelper.set(getKey(), value);
    }

    public void set(String value) {
        PreferenceHelper.set(getKey(), value);
    }

    public void set(Set<String> value) {
        PreferenceHelper.setStringSet(getKey(), value);
    }

    public void setString(int index, String value) {
        PreferenceHelper.set(getKey(), index, value);

    }

    public void setDefault() {
        if (valueClass.equals(Long.class)) set((long) defaultValue);
        else if (valueClass.equals(Integer.class)) set((int) defaultValue);
        else if (valueClass.equals(int.class)) set((int) defaultValue);
        else if (valueClass.equals(String.class)) set((String) defaultValue);
        else if (valueClass.equals(Boolean.class)) set((boolean) defaultValue);
        else if (valueClass.equals(boolean.class)) set((boolean) defaultValue);
        else if (valueClass.equals(Double.class)) set((double) defaultValue);
        else if (valueClass.equals(double.class)) set((double) defaultValue);
        else if (valueClass.equals(Set.class)) set((Set<String>) defaultValue);
    }
}
