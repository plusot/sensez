package com.plusot.util.preferences;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.plusot.util.Globals;
import com.plusot.util.logging.LLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferenceHelper {
    private static SharedPreferences prefs = null;

    public static synchronized SharedPreferences getPrefs() {
        if (prefs == null) try {
            Context appContext = Globals.getAppContext();
            if (appContext == null) return prefs = null;
            prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        } catch(NoClassDefFoundError e) {
            LLog.i("Could not find class");
        }
        return prefs;
    }

    static void setFromListArray(String key, int value) {
        if (getPrefs() == null) return;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, String.valueOf(value));
        editor.apply();
    }


    static int getFromListArray(String key, int defaultValue) {
        if (getPrefs() == null) return defaultValue;
        if (prefs == null) return defaultValue;

        if (!prefs.contains(key)) setFromListArray(key, defaultValue);
        try {
            return Integer.valueOf(prefs.getString(key, String.valueOf(defaultValue)));
        } catch (NumberFormatException e) {
            LLog.e("Could not convert " + prefs.getString(key, String.valueOf(defaultValue)) + " to integer for " + key);
            return defaultValue;
        } catch (ClassCastException e) {
            LLog.e("Could not convert to integer for " + key, e);
            return defaultValue;
        }
    }

    public static String get(String key, String defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;

        if (prefs.contains(key)) return prefs.getString(key, defaultValue);
        prefs.edit().putString(key, defaultValue).apply();
        return defaultValue;
    }


    public static double get(String key, double defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;
        if (prefs.contains(key))
            return prefs.getFloat(key, Double.valueOf(defaultValue).floatValue());
        prefs.edit().putFloat(key, (float) defaultValue).apply();
        return defaultValue;
    }

    public static double get(String key, float defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;
        if (prefs.contains(key)) return prefs.getFloat(key, defaultValue);
        prefs.edit().putFloat(key, defaultValue).apply();
        return defaultValue;
    }

    public static int get(String key, int defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;
        if (prefs.contains(key)) return prefs.getInt(key, defaultValue);
        prefs.edit().putInt(key, defaultValue).apply();
        return defaultValue;
    }

    public static long get(String key, long defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;
        if (prefs.contains(key)) return prefs.getLong(key, defaultValue);
        prefs.edit().putLong(key, defaultValue).apply();
        return defaultValue;
    }

    public static boolean get(String key, boolean defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;
        if (prefs.contains(key)) return prefs.getBoolean(key, defaultValue);
        prefs.edit().putBoolean(key, defaultValue).apply();
        return defaultValue;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static Set<String> getStringSet(String key, Set<String> defaultSet) {
        getPrefs();
        if (prefs == null) return defaultSet;

        if (Build.VERSION.SDK_INT >= 11) try {

            LLog.i("getStringSet " + key);
            if (prefs.contains(key)) {
                Set<String> set = new HashSet<>();
                Set<String> storedSet = prefs.getStringSet(key, defaultSet);
                if (storedSet != null) set.addAll(storedSet);
                return set;
            }
            prefs.edit().putStringSet(key, defaultSet).apply();
            return defaultSet;
        } catch (ClassCastException e) {
            LLog.i("getStringSet Invalid cast");
        }
        String strings[] = null;
        if (defaultSet == null) {
            String str = get(key, (String) null);
            if (str != null && str.length() > 0) strings = str.split(";");
        } else {
            StringBuilder sb = new StringBuilder();
            for (String tmp : defaultSet) {
                if (sb.length() > 0) sb.append(";");
                sb.append(tmp);
            }
            String str = get(key, sb.toString());
            if (str != null && str.length() > 0) strings = str.split(";");
        }
        if (strings == null) return defaultSet;
        Set<String> set = new HashSet<>();
        Collections.addAll(set, strings);

        return set;
    }

    public static List<String> get(String key, List<String> defaultList) {
        getPrefs();
        if (prefs == null) return defaultList;

        String strings[] = null;
        if (defaultList == null) {
            String str = get(key, (String) null);
            if (str != null && str.length() > 0) strings = str.split(";");
        } else {
            StringBuilder sb = new StringBuilder();
            for (String tmp : defaultList) {
                if (sb.length() > 0) sb.append(";");
                sb.append(tmp);
            }
            String str = get(key, sb.toString());
            if (str != null && str.length() > 0) strings = str.split(";");
        }
        if (strings == null) return defaultList;
        List<String> list = new ArrayList<>();
        Collections.addAll(list, strings);

        return list;
    }

    public static List<Integer> getIntList(String key) {
        getPrefs();
        if (prefs == null) return null;

        String strings[] = null;
        String str = get(key, (String) null);
        if (str != null && str.length() > 0) strings = str.split(";");

        if (strings == null) return null;
        List<Integer> list = new ArrayList<>();
        for (String sNumber : strings) try {
            list.add(Integer.decode(sNumber));
        } catch (NumberFormatException n) {
            //do nothing
        }
        return list;
    }

    public static String get(String key, int index, String defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;

        List<String> list = get(key, (List<String>) null);
        if (list == null) {
            set(key, index, defaultValue);
            return defaultValue;
        }
        if (index < 0) return defaultValue;
        if (index >= list.size()) {
            set(key, index, defaultValue);
            return defaultValue;
        }
        return list.get(index);
    }

    @SuppressWarnings("unused")
    public static String[] getStrings(String key, String[] defaultValue) {
        getPrefs();
        if (prefs == null) return defaultValue;

        if (defaultValue == null) {
            String str = get(key, (String) null);
            return str.split(";");
        }
        StringBuilder sb = new StringBuilder();
        for (String tmp : defaultValue) {
            if (sb.length() > 0) sb.append(";");
            sb.append(tmp);
        }
        String str = get(key, sb.toString());
        return str.split(";");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static void setStringSet(String key, Set<String> set) {
        getPrefs();
        if (Build.VERSION.SDK_INT >= 11) {
            prefs.edit().putStringSet(key, set).apply();
        } else {
            StringBuilder sb = new StringBuilder();
            for (String tmp : set) {
                if (sb.length() > 0) sb.append(";");
                sb.append(tmp);
            }
            set(key, sb.toString());
        }
    }

    public static void set(String key, List<String> list) {
        getPrefs();
        StringBuilder sb = new StringBuilder();
        for (String tmp : list) {
            if (sb.length() > 0) sb.append(";");
            sb.append(tmp);
        }
        set(key, sb.toString());
    }


    public static void setIntList(String key, List<Integer> list) {
        getPrefs();
        StringBuilder sb = new StringBuilder();
        for (Integer tmp : list) {
            if (sb.length() > 0) sb.append(";");
            sb.append("" + tmp);
        }
        set(key, sb.toString());
    }

//	private static List<String> _set(String key, int index, String value) {
//		getPrefs();
//		List<String> list = get(key, (List<String>) null);
//		if (list == null) {
//			list = new ArrayList<String>();
//			for (int i = 0; i < index; i++) list.addWatchable("");
//		}
//		if (index >= list.size()) for (int i = list.size(); i <= index; i++) list.addWatchable("");
//		list.set(index, value);
//		StringBuilder sb = new StringBuilder();
//		for (String tmp : list) {
//			if (sb.length() > 0) sb.append(";");
//			sb.append(tmp);
//		}
//		set(key, sb.toString());
//		return list;
//	}

    static boolean addToSet(String key, String value) {
        Set<String> localSet = getStringSet(key, null);
        if (localSet == null) {
            localSet = new HashSet<>();
        } else if (localSet.contains(value)) return false;
        localSet.add(value);
        setStringSet(key, localSet);
        return true;
    }

    static void deleteFromSet(String key, String value) {
        Set<String> set = getStringSet(key, null);
        if (set == null) return;
        for (String string : set) {
            if (string.startsWith(value)) {
                set.remove(string);
                break;
            }
        }
        setStringSet(key, set);
    }

    static String getStringFromSet(String key, int index) {
        Set<String> set = getStringSet(key, null);
        if (set != null) {
            String[] array = set.toArray(new String[0]);
            if (index >= 0 && index < array.length) return array[index];
        }
        return null;
    }

    public static void set(String key, int index, String value) {
        getPrefs();
        List<String> list = get(key, (List<String>) null);
        if (list == null) {
            list = new ArrayList<>();
            if (index > 0) for (int i = 0; i < index; i++) list.add("");
        }
        if (index < 0) {
            if (list.contains(value)) return;
            list.add(value);
        } else {
            if (index >= list.size()) for (int i = list.size(); i <= index; i++) list.add("");
            list.set(index, value);
        }
        StringBuilder sb = new StringBuilder();
        for (String tmp : list) {
            if (sb.length() > 0) sb.append(";");
            sb.append(tmp);
        }
        set(key, sb.toString());
    }

//	public static void set(String key, int index, String value) {
//		_set(key, index, value);
//	}

    public static void set(String key, long value) {
        getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static boolean contains(String key) {
        getPrefs();
        return prefs.contains(key);
    }

    public static void set(String key, String value) {
        getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void set(String key, boolean value) {
        getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void set(String key, int value) {
        getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void set(String key, double value) {
        getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, (float) value);
        editor.apply();
    }

}
