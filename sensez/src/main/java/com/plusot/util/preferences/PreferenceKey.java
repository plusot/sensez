package com.plusot.util.preferences;


public enum PreferenceKey {
    SESSION_TIME("session_time", 0L, Long.class),
    STATS("stats_preference", true, Boolean.class),
    PERMISSIONS_ACCEPTED("permissions_accepted_v1", false, Boolean.class),
    PREVIOUS_PID("previous_pid", 0, Integer.class),
    ID("device_id", null, String.class),
    ;

    private final PreferenceKeyElements elements;

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass) {
        this(label, defaultValue, valueClass, 0);
    }

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass, final long flags) {
        elements = new PreferenceKeyElements (label, defaultValue, valueClass, flags);
    }

    public int getInt() { return elements.getInt(); }
    public long getLong() { return elements.getLong(); }
    public String getString() { return elements.getString(); }
    public boolean isTrue() { return elements.getBoolean(); }

    public boolean set(int value) { return elements.set(value); }
    public void set(long value) {  elements.set(value); }
    public void set(boolean value) {  elements.set(value); }
    public void set(String value) {  elements.set(value); }

    public static PreferenceKey fromString(final String label) {
        for (PreferenceKey key : PreferenceKey.values()) {
            if (key.elements.getLabel().equals(label)) return key;
        }
        return null;
    }

}