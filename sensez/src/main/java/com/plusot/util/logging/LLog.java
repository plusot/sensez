package com.plusot.util.logging;

import android.content.Context;
import android.util.Log;

import com.plusot.sensez.BuildConfig;
import com.plusot.util.Globals;
import com.plusot.util.util.TimeUtil;

import java.util.HashMap;
import java.util.Map;

public class LLog {
    private static Map<String, Long> history = new HashMap<>();
    public static Mode mode = Mode.NORMAL;
    public static boolean debugAsInfo = false;

    public enum Mode{
        NORMAL,
        VERBOSE,
        SHORT,
    }

    private static String getMethod(int level) {
        if (mode == Mode.SHORT) return "";
        Thread current = Thread.currentThread();
        StackTraceElement[] stack = current.getStackTrace();
        StringBuilder sb = new StringBuilder();
        String className = "";
        if (stack.length >= level + 1 && stack[level] != null && !(stack[level].isNativeMethod())) {
            switch (mode) {
                case VERBOSE:
                    className = stack[level].getClassName();
                    break;
                case NORMAL:
                    className = stack[level].getFileName();
                    if (className != null) className = className.replace(".java", "");
                    break;
                default:
                    break; }

            int lineNumber = stack[level].getLineNumber();
            String methodName = stack[level].getMethodName();
            if (sb.length() > 0) sb.append('/');
            sb.append(className + '.' + methodName + '(' + lineNumber + "):");
        } else for (StackTraceElement element : stack) {
            if (!element.isNativeMethod()) {
                switch (mode) {
                    case VERBOSE:
                        className = element.getClassName();
                        break;
                    case NORMAL:
                        className = element.getFileName();
                        if (className != null) className = className.replace(".java", "");
                    default:
                        break;
                }
                int lineNumber = element.getLineNumber();
                String methodName = element.getMethodName();
                if (sb.length() > 0) sb.append('/');
                sb.append(className + '.' + methodName + '(' + lineNumber + "):");
            }
        }
        return sb.toString();

    }

    public static String getMethod() {
        return getMethod(4);
    }

    public static final String SPEC = "applog";

    private static boolean save(char level, String tag, String msg, Throwable tw, long historyTimeOut) {
        if (historyTimeOut >= 0) {
            Long time = history.get(msg);
            Long now = System.currentTimeMillis();
            if (time != null && (historyTimeOut == 0 || now - time < historyTimeOut)) return false;
            if (history.size() > 1000) {
                String oldestMsg = null;
                long oldest = Long.MAX_VALUE;
                for (String oldMsg : history.keySet()) {
                    long t = history.get(oldMsg) ;
                    if (t < oldest) {
                        oldest = t;
                        oldestMsg = oldMsg;
                    }
                }
                if (oldestMsg != null) {
                    history.remove(oldestMsg);
                }
            }
            history.put(msg, now);
        }
//        if (!SensezLib.isRequestedPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) return true;

        Context appContext = Globals.getAppContext();
        if (appContext == null) return true;
        //if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(appContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) return true;

        SimpleLog log;
        if ((log = SimpleLog.getInstance(SimpleLogType.LOG, SPEC)) == null) return true;
        if (tw == null)
            log.log(TimeUtil.formatMilli() + ';' + level + ';' + tag + ';' + msg);
        else
            log.log(TimeUtil.formatMilli() + ';' + level + ';' + tag + ';' + msg, tw); //tw.getMessage());
        return true;
    }

    public static void v(String msg, Throwable tw) {
        v(Globals.TAG, getMethod(4) + " " + msg, tw, -1);
    }

    public static void v(String msg) {
        v(Globals.TAG, getMethod(4) + " " + msg, null, -1);
    }

    private static void v(String tag, String msg, Throwable tw, long checkHistory) {
        if (save('v', tag, msg, tw, checkHistory)) Log.v(tag, msg, tw);
    }

//    public static void v(String tag, String msg) {
//        v(tag, msg, null, false);
//    }

    private static void w(String tag, String msg, Throwable tw, long checkHistory) {
        save('w', tag, msg, tw, checkHistory);
        Log.w(tag, msg, tw);
    }

//    public static void w(String tag, String msg) {
//        w(tag, msg, null, false);
//    }

    public static void w(String msg) {
        w(Globals.TAG, getMethod(4) + " " + msg, null, -1);
    }


    private static boolean d(String tag, String msg, Throwable tw, long checkHistory) {
        boolean result;
        if (result = save('d', tag, msg, tw, checkHistory)) {
            if (debugAsInfo)
                Log.i(tag, msg, tw);
            else
                Log.d(tag, msg, tw);
        }
        return result;
    }

    public static void d(String msg) {
        d(Globals.TAG, getMethod(4) + " " + msg, null, -1);
    }

    public static void d(String msg, long checkHistory) {
        d(Globals.TAG, getMethod(4) + " " + msg, null, checkHistory);
    }

    public static void d(int id) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        d(Globals.TAG, getMethod(4) + " " + appContext.getText(id), null, -1);
    }

    public static void d(String msg, Throwable tw) {
        d(Globals.TAG, getMethod(4) + " " + msg, tw, -1);
    }

//    public static void d(String tag, String msg) {
//        d(tag, msg, null, false);
//    }

//    public static void d(String tag, String msg, boolean checkHistory) {
//        d(tag, msg, null, checkHistory);
//    }

    private static void i(String tag, String msg, Throwable tw, long checkHistory) {
        if (save('i', tag, msg, tw, checkHistory)) Log.i(tag, msg, tw);
    }

    public static void i(String msg, Throwable tw) {
        i(Globals.TAG, getMethod(4) + " " + msg, tw, -1);
    }

//    public static void i(String tag, String msg) {
//        i(tag, msg, null, false);
//    }

    public static void i(String msg) {
        i(Globals.TAG, getMethod(4) + " " + msg, null, -1);
    }

    public static void i(int id) {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;
        i(Globals.TAG, getMethod(4) + " " + appContext.getText(id), null, -1);
    }
    private static void e(String tag, String msg, Throwable tw, long checkHistory) {
        if (save('e', tag, msg, tw, checkHistory)) Log.e(tag, msg, tw);
    }

    public static void e(String msg, Throwable tw) {
        e(Globals.TAG, getMethod(4) + " " + msg, tw, -1);
    }

    public static void e(String msg) {
        e(Globals.TAG, getMethod(4) + " " + msg, null, -1);
    }

    public static void e(String msg, long checkHistory) {
        e(Globals.TAG, getMethod(4) + " " + msg, null, checkHistory);
    }

//    public static void e(String tag, String msg) {
//        e(tag, msg, null, false);
//    }

}
