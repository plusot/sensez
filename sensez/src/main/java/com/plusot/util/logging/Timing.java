package com.plusot.util.logging;

import com.plusot.util.Globals;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Stringer;
import com.plusot.util.util.Watchdog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Timing implements Watchdog.Watchable {
    public static boolean DEBUG = false;
    public static long LOG_INTERVAL = 120000;
    private static Timing instance = null;
    private static long startTime = 0;
    private static Map<String, TimeData> data = new HashMap<>();
    private static final Object logLock = new Object();

    private Timing() {
        startTime = System.currentTimeMillis();
        Watchdog.addWatchable(this, LOG_INTERVAL);
    }

    public static Timing getInstance() {
        if (instance == null) return instance = new Timing();
        return instance;
    }

    @Override
    public void onWatchdogTimer(long count) {
        if (Globals.runMode.isRun()) log();
    }

    @Override
    public void onWatchdogClose() {

    }

    public enum Action {
        BEGIN,
        END,
        RESULT
    }

    private static class TimeData {
        final String name;
        long timeSpent = 0;
        long timeResult = 0;
        int timesCalled = 0;
        int timesResult = 0;
        long timeIn = -1;
        Set<String> threads = new HashSet<>();
        String remark = null;


        private TimeData(String name) {
            this.name = name;
        }

        public long log(Action action, long refTimeIn, String remark) {
            threads.add(Thread.currentThread().getName() + "_" + Thread.currentThread().getId());
            long time = System.nanoTime();
            this.remark = remark;
            switch (action) {

                case BEGIN:
                    timeIn = time;
                    timesCalled++;
                    break;
                case END:
                    if (timeIn >= 0) {
                        timeSpent += time - timeIn;
                    }
                    timeIn = -1;
                    break;
                case RESULT:
                    if (refTimeIn <= 0) break;
                    timesResult++;
                    timeResult += time - refTimeIn;
                    break;


            }
            return timeIn;
        }

        @Override
        public String toString() {
            long running = (System.currentTimeMillis() - startTime);
            String threadStr = StringUtil.toString(threads.toArray(new String[threads.size()]), ",");
            String result =
                    Format.format(name + "(" + threadStr + ")", Globals.TAG_WIDTH) + " => " +
                    "duration: " + Format.format(0.000001 * timeSpent / timesCalled, 3, 2) + "ms avg, " + Format.format(100 * 0.000001 * timeSpent /running, 2, 2) + "%, " +
                    Format.format(timesCalled, 4) + "x";
            if (timeResult > 0)
                result += ", wait: " + Format.format(0.000001 * timeResult / timesResult, 3, 2) + "ms avg, " + Format.format(100 * 0.000001 * timeResult /running, 2, 2) + "%";
            if (remark != null)
                result += ", " + remark;
            return result;
            //μs
        }
    }

    public static long log(String sectionName, Action action) {
        return log(sectionName, action, 0, null);
    }

    public static long log(String sectionName, Action action, long refTime) {
        return log(sectionName, action, refTime, null);
    }

    public static long log(String sectionName, Action action, long refTime, String remark) {
        if (instance == null) getInstance();
        TimeData timeData;
        if ((timeData = data.get(sectionName)) == null) data.put(sectionName, timeData = new TimeData(sectionName));
        return timeData.log(action, refTime, remark);
    }

    public static long log(String sectionName, Action action, String remark) {
        return log(sectionName, action, 0, remark);
    }

    private static void log() {
        synchronized (logLock) {
            try {
                Stringer stringer = new Stringer("\n");
                List<TimeData> list = new ArrayList<>();
                list.addAll(data.values());
                Collections.sort(list, new Comparator<TimeData>() {
                    @Override
                    public int compare(TimeData lhs, TimeData rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                for (TimeData timeData : list) {
                    stringer.append(Globals.TAB + timeData.toString());
                }
                if (DEBUG) LLog.i("Timing: " + (System.currentTimeMillis() - startTime) / 1000 + " s\n" + stringer.toString());
                SleepAndWake.logPoolSize();
            } catch (ConcurrentModificationException e) {
                LLog.e("Concurrency exception", e);
            }
        }

    }
}
