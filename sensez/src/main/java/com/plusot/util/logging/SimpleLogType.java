package com.plusot.util.logging;

public enum SimpleLogType {
    JSON(null, ",", null, "", ".json", true),
    TEXT(null, null, null, "", ".txt", true),
    CSV(null, null, null, "", ".csv", true),
    HRV(null, null, null, "", ".hrv", true),
    LOG(null, null, null, "", ".txt", true),
    ;
//    ARGOS("Minutes,Torq (N-m),Km/h,Watts,Km,Cadence,Hrate,ID,Altitude (m)", null, null, ",", ".csv", true, true);
    public final String sep;
    private final String behindLines;
    private final String beforeLines;
    private final String betweenLines;
    private final String ext;
    private final boolean append;

    SimpleLogType(final String beforeLines, final String betweenLines, final String behindLines, final String sep, final String ext, final boolean append) {
        this.behindLines = behindLines;
        this.betweenLines = betweenLines;
        this.beforeLines = beforeLines;
        this.sep = sep;
        this.ext = ext;
        this.append = append;
    }

    public String getBehindLines() {
        return behindLines;
    }

    public String getBeforeLines() {
        return beforeLines;
    }

    public String getBetweenLines() {
        return betweenLines;
    }

    public String getExt() {
        return ext;
    }

    public boolean isAppend() {
        return append;
    }

}
