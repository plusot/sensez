package com.plusot.shareddata.preferences;

import com.plusot.util.Globals;
import com.plusot.util.preferences.PreferenceKeyElements;
import com.plusot.util.util.UserInfo;


public enum PreferenceKey {
    NAME("name", "Plusot", String.class),
    PASSWORD("password", "let me in", String.class),
    ROOM("room", "Plusot", String.class),
    SERVER_URL("server_url", "https://plusot.net:6443", String.class)
    ;

    private final PreferenceKeyElements elements;
    private static int currentUrlIndex = 0;

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass) {
        this(label, defaultValue, valueClass, 0);
    }

    PreferenceKey(final String label, final Object defaultValue, final Class<?> valueClass, final long flags) {
        elements = new PreferenceKeyElements (label, defaultValue, valueClass, flags);
    }

    public static String getUrl() {
        String[] urls = SERVER_URL.getString().split(";");
        if (urls.length == 0) return SERVER_URL.getString();
        return (urls[currentUrlIndex]);

    }

    public static String nextUrl() {
        currentUrlIndex++;
        String[] urls = SERVER_URL.getString().split(";");
        if (urls.length == 0) currentUrlIndex = 0;
        currentUrlIndex %= urls.length;
        return getUrl();
    }

    public static boolean isSSL() {
        return (getUrl().contains("https"));
    }

    public static String getMapUrl() {
        return getUrl() + "/map.html?device=" + UserInfo.getDeviceId() + "&session=" + Globals.getSessionId(); // + "&replace=1";
    }

    public static String getSessionsUrl() {
        return getUrl() + "/sessions.html?device=" + UserInfo.getDeviceId();
    }

    public int getInt() { return elements.getInt(); }
    public String getString() { return elements.getString(); }

    public boolean set(int value) { return elements.set(value); }

    public static PreferenceKey fromString(final String label) {
        for (PreferenceKey key : PreferenceKey.values()) {
            if (key.elements.getLabel().equals(label)) return key;
        }
        return null;
    }

}