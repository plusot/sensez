package com.plusot.shareddata.share;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;

import com.plusot.shareddata.db.DbData;
import com.plusot.shareddata.preferences.PreferenceKey;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.json.JSoNField;
import com.plusot.util.json.JSoNHelper;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.Timing;
import com.plusot.util.share.Http;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.UserInfo;
import com.plusot.util.util.Watchdog;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;

public class HttpTask implements DbData.NewDataListener, Watchdog.Watchable {
    private static HttpTask instance;
    private static final boolean DEBUG = false;
    private static final int PASS = -2;
    //    private final IBinder binder = new LocalBinder();
//    private ConnectivityManager connectivityManager = null;
    private Socket socket = null;
    private static long socketActive = 0;

    private String deviceId = "";
    public static boolean useServer = true;
    //    private int failCount = 0;
    private static Listener listener;
    //    private List<DbData.DataObj> listToHandle = null;
//    private final Object listLock = new Object();
    private static final Object listenerLock = new Object();
    private static final Object sendLock = new Object();
    private static long listenerCalled = 0;
    private List<DbData.DataObj> oldSocketData = new ArrayList<>();
    private final static String MOBILE = "MOBILE";
    private boolean checkingOld = false;

    private HttpTask() {
        LLog.i("HttpTask started");
        deviceId = "" + UserInfo.getDeviceId();
        DbData.addNewDataListener(this);
        socketCreate();

        Watchdog.addWatchable(this, 60000);
    }

    public static HttpTask startInstance() {
        if (instance != null) return instance;
        if (!Globals.runMode.isRun()) return null;
        synchronized(HttpTask.class) {
            if (instance == null) instance = new HttpTask();
        }
        return instance;
    }

    public static void stopInstance() {
        if (instance != null) synchronized(HttpTask.class) {
            if (instance != null) instance.close();
            instance = null;
        }
    }

    public interface Listener {
        void onResult(Http.Error error, long time);
    }

    public static void setListener(Listener listener) {
        synchronized (listenerLock) {
            HttpTask.listener = listener;
        }
    }

    public static void clearListener() {
        synchronized (listenerLock) {
            HttpTask.listener = null;
        }
    }

    private static void callListener(Http.Error error, long time) {
        long now = System.currentTimeMillis();
        synchronized (listenerLock) {
            if (listener != null && (now - listenerCalled) > 1500) {
                listener.onResult(error, time);
                listenerCalled = now;
            }
        }
    }

    private enum SocketIOEvent {
        LOGIN("login"),
        CONTENT("content"),
        SESSION("session"),
        ;
        private final String label;

        SocketIOEvent(final String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    private void socketCreate() {
        try {
            socket = IO.socket(PreferenceKey.getUrl());
            socket.on(Socket.EVENT_CONNECT, args -> {
                try {
                    JSONObject json = new JSONObject();
                    socketActive = System.currentTimeMillis();
                    JSoNHelper.set(json, JSoNField.NAME, PreferenceKey.NAME.getString());
                    JSoNHelper.set(json, JSoNField.PASSWORD, PreferenceKey.PASSWORD.getString());
                    JSoNHelper.set(json, JSoNField.ROOM, PreferenceKey.ROOM.getString());
                    JSoNHelper.set(json, JSoNField.DEVICEID, deviceId);
                    socket.emit(SocketIOEvent.LOGIN.toString(), json);
                } catch (Exception e) {
                    LLog.e("Could not interpret response as JSON: " + e.getMessage());
                }
            });
            socket.on(SocketIOEvent.LOGIN.toString(), args -> {
                try {
                    JSONObject data = (JSONObject) args[0];
                    LLog.i("Received " + SocketIOEvent.LOGIN + ": " + data.toString());
                } catch (ClassCastException e) {
                    LLog.e("Could not interpret response as JSON: " + e.getMessage());
                }
            });
            socket.on(SocketIOEvent.CONTENT.toString(), args -> {
                try {
                    JSONObject data = (JSONObject) args[0];
//                    LLog.i("Received " + SocketIOEvent.CONTENT + ": " + data.toString());
                    JSoNField result = JSoNField.fromString(JSoNHelper.get(data, JSoNField.RESULT, (String) null));
                    Long time = JSoNHelper.get(data, JSoNField.TIME, -1L);
                    if (result == JSoNField.OK)
                        callListener(Http.Error.NOERROR, time);
                    else
                        callListener(Http.Error.IOERROR, time);
                } catch (Exception e) {
                    LLog.e("Could not interpret response as JSON: " + e.getMessage());
                }

            });
            socket.on(SocketIOEvent.SESSION.toString(), args -> {
                try {
                    JSONObject data = (JSONObject) args[0];
                    LLog.i("Received " + SocketIOEvent.SESSION + ": " + data.toString());
                } catch (Exception e) {
                    LLog.e("Could not interpret response as JSON: " + e.getMessage());
                }

            });
            socket.connect();
            if (socket.connected()) socketActive = System.currentTimeMillis();
        } catch (URISyntaxException e) {
            LLog.e("Socket IO error", e);
        }
    }

    private long lastId = -1;

    private void sendOld() {
        synchronized (sendLock) {
            SleepAndWake.runInNewThread(() -> {
//            LLog.i("Sending old data (requested by id " + id + ")");
                while (oldSocketData.size() > 0 && Globals.runMode.isRun()) {
                    DbData.DataObj obj = oldSocketData.get(0);
                    JSONObject json = new JSONObject();
                    LLog.i("Sending old item " + obj.getId());

                    JSoNHelper.set(json, JSoNField.DEVICEID, deviceId);
                    JSoNHelper.set(json, JSoNField.SESSIONID, obj.getSessionId());
                    JSoNHelper.set(json, JSoNField.DATA, obj.getData());
                    lastId = /*Math.max(*/ obj.getId() /*, lastId)*/;
                    socket.emit(SocketIOEvent.CONTENT.toString(), json);
                    oldSocketData.remove(0);
                    DbData.updateShared(obj, DbData.Shared.SHARED);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }, "HttpTask.socketConnectSendOld");
        }
    }

    private boolean socketConnectSendNew(final long id) {
        boolean result = false;
        synchronized (sendLock) {
            if (socket == null) socketCreate();
            if (socket == null) return false;
            if (!socket.connected()) socket.connect();
            if (socket.connected()) {
                checkingOld = true;
                socketActive = System.currentTimeMillis();
                long now = System.currentTimeMillis();
                result = true;
                if (now - socketActive < 2000 && (id < 0 || id - lastId != 1) && (oldSocketData == null || oldSocketData.size() == 0)) {
                    oldSocketData = DbData.getData(JSoNField.DATA, DbData.Shared.NOT_SHARED);
                    if (oldSocketData != null && oldSocketData.size() > 0) {
                        sendOld();
                        result = id == PASS;
                    }
                }
            } else {
                LLog.i("SocketIO not connected to " + PreferenceKey.getUrl() + "!");
            }
        }
        return result;
    }

    private void socketCheckOld() {
        if (checkingOld) return;
        if (oldSocketData != null && oldSocketData.size() > 0) return;
        synchronized (sendLock) {
            if (socket == null) socketCreate();
            if (socket == null) return;
            if (!socket.connected()) socket.connect();
            if (socket.connected()) {
                socketActive = System.currentTimeMillis();
                long now = System.currentTimeMillis();
                if (now - socketActive < 2000 && (oldSocketData == null || oldSocketData.size() == 0)) {
                    oldSocketData = DbData.getData(JSoNField.DATA, DbData.Shared.NOT_SHARED);
                    if (oldSocketData != null && oldSocketData.size() > 0) sendOld();
                }
            } else {
                LLog.i("SocketIO not connected to " + PreferenceKey.getUrl() + "!");
            }
        }
    }

    private boolean socketSendData(SocketIOEvent ioEvent, JSONObject json, long id) {
//        LLog.i("Request send for socketIO " + ioEvent + " for id = " + id); // + ": " + json.toString());
        if (socketConnectSendNew(id) && isSocketAlive() && socket != null && ioEvent != null && json != null) {
//            LLog.i("Sending socketIO " + ioEvent + " for id = " + id); // + ": " + json.toString());
            socket.emit(ioEvent.toString(), json);
            if (id > 0) lastId = id;
            return true;
        }
        return false;
    }

    private static boolean isSocketAlive() {
        return System.currentTimeMillis() - socketActive < 10000;
    }

    private void socketSendData(final DbData.DataObj obj) {
        JSONObject json = new JSONObject();
        //LLog.i("Using device id " + deviceId);
        JSoNHelper.set(json, JSoNField.DEVICEID, deviceId);
        JSoNHelper.set(json, JSoNField.SESSIONID, Globals.getSessionId());
        JSoNHelper.set(json, JSoNField.DATA, obj.getData());
        if (socketSendData(SocketIOEvent.CONTENT, json, obj.getId())) DbData.updateShared(obj, DbData.Shared.SHARED);
    }

    private void socketSendSession(final DbData.SessionObj obj) {
        JSONObject json = new JSONObject();
        //LLog.i("Using device id " + deviceId);
        JSoNHelper.set(json, JSoNField.DEVICEID, deviceId);
        JSoNHelper.set(json, JSoNField.SESSIONID, Globals.getSessionId());
        JSoNHelper.set(json, JSoNField.DATA, obj.getData().toJSON(DbData.translator));
        socketSendData(SocketIOEvent.SESSION, json, PASS);
    }

    private Runnable sendDeviceData = () -> {
        if (!Globals.runMode.isRun()) return;
        LLog.i("Sending device data!!!!");
        final long timeIn = Timing.log(HttpTask.class.getSimpleName() + "_sendDeviceData", Timing.Action.BEGIN);

        try {
            Data data = new Data();
            data.add(MOBILE, DataType.DEVICE_NAME, Build.MODEL);
            data.add(MOBILE, DataType.HARDWARE_REVISION_STRING, Build.HARDWARE);

            Field[] fields = Build.VERSION_CODES.class.getFields();
            int iVersion = Math.min(Build.VERSION.SDK_INT + 1, fields.length - 1);
            String osName = fields[iVersion].getName();

            data.add(MOBILE, DataType.ANDROID_VERSION, osName + " (" + Build.VERSION.SDK_INT + ")");
            data.add(MOBILE, DataType.SOFTWARE_REVISION_STRING, UserInfo.appNameVersion());
//                data.add(MOBILE, DataType.USER_NAME, UserInfo.getUserData().name);
//                data.add(MOBILE, DataType.EMAIL_ADDRESS, "info@plusot.com"); //UserInfo.getUserData().email);

            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Context appContext = Globals.getAppContext();
            if (appContext != null) {
                Intent batteryStatus = appContext.registerReceiver(null, ifilter);
//                int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
//                boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
//                        status == BatteryManager.BATTERY_STATUS_FULL;
//                int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
//                boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
//                boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
                if (batteryStatus != null) {
                    int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                    double batteryState = 1.0 * level / scale;
                    data.add(MOBILE, DataType.BATTERY_LEVEL, batteryState);
                }
            }

            final String url = PreferenceKey.getUrl() + "/api/device/" + deviceId + "/" + Globals.getSessionId();
            LLog.i("Posting device data to " + /* + data.toString() + "\nto\n"*/ url);
            Http.post(url, data.toJSON(null), (error, info, json) -> {
                Timing.log(HttpTask.class.getSimpleName() + "_sendDeviceData", Timing.Action.RESULT, timeIn);

                if (error == Http.Error.NOERROR) {
                    if (DEBUG) LLog.i("Success " + info + ": " + json.toString());
                } else if (json == null) {
                    LLog.i("Failed " + info + " with error: " + error);
//                            failCount++;
                } else {
                    LLog.i("Failed " + info + " with error: " + error + " and " + json.toString());
//                            failCount++;
                }
            }, false);
        } finally {
            Timing.log(HttpTask.class.getSimpleName() + "_sendDeviceData", Timing.Action.END);
        }
    };

    @Override
    public void onWatchdogTimer(long count) {
        if (Globals.runMode.isRun()) {
            SleepAndWake.runInNewThread(sendDeviceData, "HttpService.onWatchdogCheck");
            if (!checkingOld) socketCheckOld();
        }
    }

    @Override
    public void onWatchdogClose() {}


    @Override
    public void onData(DbData.DataObj obj) {
        socketSendData(obj);
    }

    @Override
    public void onSession(DbData.SessionObj obj) {
        socketSendSession(obj);
    }

    private void close() {
        LLog.i("Close called");
        if (socket != null) {
            socket.disconnect();
            socket.off();
        }
        DbData.removeNewDataListener(this);
    }

}
