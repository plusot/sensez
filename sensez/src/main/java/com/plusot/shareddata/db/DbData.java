package com.plusot.shareddata.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.plusot.sensez.device.Device;
import com.plusot.sensez.device.Man;
import com.plusot.sensez.device.ScanManager;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.Globals;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;
import com.plusot.util.json.JSoNField;
import com.plusot.util.json.JSoNHelper;
import com.plusot.util.logging.LLog;
import com.plusot.util.logging.Timing;
import com.plusot.util.preferences.PreferenceHelper;
import com.plusot.util.util.Format;
import com.plusot.util.util.SleepAndWake;
import com.plusot.util.util.StringUtil;
import com.plusot.util.util.Stringer;
import com.plusot.util.util.TimeUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.Thread.sleep;

public class DbData implements ScanManager, Man.Listener {
    private static DbData instance = null;
    private static final Object instanceLock = new Object();
    private final Object dbLock = new Object();
    private static final boolean DEBUG = false;
    private SQLiteDatabase db;
    private static final long SESSION_INTERVAL = 60000;
    private static final long DATA_INTERVAL = 5000;

    private Map<String, ScannedDevice> playbackDevices = new HashMap<>();
    private static  Map<String, SessionObj> sessions = new HashMap<>();
    private Set<NewDataListener> newDataListeners = new HashSet<>();

    private long lastSessionSave = 0;
    private int checkCount = 0;
    private int saveCount = 0;


//    public interface DataListener {
//        void onData(DataObj obj);
//    }

    public interface NewDataListener {
        void onData(DataObj obj);
        void onSession(SessionObj obj);
    }

    public interface DoneListener {
        void onDone();
    }

    public enum Shared {
        NOT_SHARED,
        SHARED,
        NEW,
        ;

        public static Shared fromOrdinal(int value) {
            return Shared.values()[value % Shared.values().length];
        }
    }

    public static class SessionObj {
        private long id;
        final long timeStamp;
        final String sessionId;
        private Data data;

        SessionObj(long id, String sessionId, long timeStamp, JSONObject json) {
            this.id = id;
            this.timeStamp = timeStamp;
            this.sessionId = sessionId;
            this.data = Data.fromJSON(json);
        }

        public long getId() {
            return id;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            if (this.data == null)
                this.data = data.cloneAggregates();
            else
                this.data.mergeAggregates(data);
        }

        public String getSessionId() {
            return sessionId;
        }

        @Override
        public String toString() {
            return "SessionObj id: " + id + " time: " + TimeUtil.formatTime(timeStamp);
        }
    }

    public static class DataObj {
        private long id;
        final long timeStamp;
        final String sessionId;
        final JSoNField category;
        private Shared shared;
        final JSONObject data;

        DataObj(long id, String sessionId, JSoNField category, long timeStamp, Shared shared, JSONObject data) {
            this.id = id;
            this.timeStamp = timeStamp;
            this.category = category;
            this.sessionId = sessionId;
            this.shared = shared;
            this.data = data;
        }

        DataObj(String sessionId, JSoNField category, long timeStamp, Shared shared, JSONObject data) {
            this(-1, sessionId, category, timeStamp, shared, data);
        }

        public long getId() {
            return id;
        }


        Shared getShared() {
            return shared;
        }

        void setShared(Shared shared) {
            this.shared = shared;
        }

        public JSONObject getData() {
            return data;
        }

        public String getSessionId() {
            return sessionId;
        }

        @Override
        public String toString() {
            return "DataObj id: " + id + " time: " + TimeUtil.formatTime(timeStamp);
        }
    }

    static class PropObj {
        private long id;
        final long timeStamp;
        final String name;
        final JSoNField category;
        final JSONObject data;

        PropObj(long id, String name, JSoNField category, long timeStamp, JSONObject data) {
            this.id = id;
            this.timeStamp = timeStamp;
            this.category = category;
            this.name = name;
            this.data = data;
        }

        PropObj(String name, JSoNField category, JSONObject data) {
            this(-1, name, category, System.currentTimeMillis(), data);
        }

//        public PropObj(String name, JSoNField category, long timeStamp, JSONObject data) {
//            this(-1, name, category, timeStamp, data);
//        }

        public long getId() {
            return id;
        }

//        public long getTimeStamp() {
//            return timeStamp;
//        }
//
//        public JSoNField getCategory() {
//            return category;
//        }


        public JSONObject getData() {
            return data;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "PropObj id: " + id + " time: " + TimeUtil.formatTime(timeStamp);
        }
    }

    private static boolean mayStoreInDB = true;

    public static void setMayUseDB(boolean value) {
        mayStoreInDB = value;
    }

    private DbData() {
        Context appContext = Globals.getAppContext();
        if (appContext == null) return;

        DbHelper dbHelper = new DbHelper(appContext, null);
//        LLog.v("Trying to open database");
        if (db == null) db = dbHelper.getWritableDatabase();
        Man.addListener(this);
    }

    private static DbData getInstance() {
        if (!mayStoreInDB) {
            if (instance != null) stopInstance();
            return null;
        }
        if (instance != null) return instance;
        if (!Globals.runMode.isRun() ) return null;
        //synchronized (instanceLock) {
        //if (instance == null)
        instance = new DbData();
        //}
        return instance;
    }

    public static void stopInstance() {
        synchronized (instanceLock) {
            if (instance != null) synchronized (DbData.class) {
                LLog.i("Stopping Instance");
                if (instance != null) instance.close();
                instance = null;
            }
        }
    }

    private void _addNewDataListener(NewDataListener listener) {
        newDataListeners.add(listener);
    }

    public static void addNewDataListener(NewDataListener listener) {
        if (!Globals.runMode.isRun()) return;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                instance._addNewDataListener(listener);
            }
        }
    }

    private void _removeNewDataListener(NewDataListener listener) {
        newDataListeners.remove(listener);
    }

    public static void removeNewDataListener(NewDataListener listener) {
        if (!Globals.runMode.isRun()) return;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                instance._removeNewDataListener(listener);
            }
        }
    }

    @Override
    public Collection<ScannedDevice> getScannedDevices() {
        return playbackDevices.values();
    }

    @Override
    public void close() {
        Man.removeListener(this);
        if (db == null) return;
        db.close();
        db = null;
    }

    @Override
    public boolean startScan(ScanType type) {
        return false;
    }

    @Override
    public void stopScan(ScanType type) {

    }

    private SessionObj storeSession(String sessionId, boolean checkGet) {
        if (db == null) return null;
        SessionObj sessionObj = sessions.get(sessionId);
        if (sessionObj != null) return sessionObj;
        long id = -1;
        long now = System.currentTimeMillis();
        synchronized (dbLock) {
            if (checkGet) {
                sessionObj = _getSession(sessionId, true);
                if (sessionObj != null) {
                    sessions.put(sessionId, sessionObj);
                }
                return sessionObj;
            }
            try {
                ContentValues values = new ContentValues();
                values.put(DbHelper.COL_SESSION, sessionId);
                values.put(DbHelper.COL_TIMESTAMP, now);
                id = db.insert(DbHelper.SESSION_TABLE, null, values);
            } catch (SQLiteConstraintException e) {
                LLog.i("SessionId " + sessionId + " already exists in the database");
            } catch (Exception e) {
                LLog.e("Exception in DB insert", e);
            }

        }
        sessionObj = new SessionObj(id, sessionId, now, null);
        sessions.put(sessionId, sessionObj);
        return sessionObj;
    }

    private SessionObj _getSession(String sessionId, boolean queryDb) {
        if (sessions.size() == 0 || queryDb) {
            if (db == null) return null;
            String selection = DbHelper.COL_SESSION + " = ?";
            String[] selectionArgs = new String[]{sessionId};

            List<SessionObj> list = getSessions(selection, selectionArgs, null, 1, false);
            if (list.size() >= 1 ) return list.get(0);
            return null;
        }
        return sessions.get(sessionId);
    }

    public static SessionObj getSession(String sessionId) {
        if (!Globals.runMode.isRun()) return null;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                return instance._getSession(sessionId, false);
            }
        }
        return null;
    }

    private List<SessionObj> getSessions(String selection, String[] selectionArgs, String orderby, int limit, boolean addTodayIfNull) {
        List<SessionObj> list = new ArrayList<>();
        if (db == null) return list;
        String strLimit = null;
        if (limit > 0) strLimit = "" + limit;

        synchronized (dbLock) {
            try {
                Cursor cursor = db.query(
                        DbHelper.SESSION_TABLE,            // table
                        new String[]{
                                DbHelper.COL_ID,
                                DbHelper.COL_SESSION,
                                DbHelper.COL_TIMESTAMP,
                                DbHelper.COL_DATA
                        },        // columns
                        selection,                      //selection
                        selectionArgs,                  // selection args
                        null,                           // group by
                        null,                           // having
                        orderby,                        // order by
                        strLimit);                      // limit

                SessionObj sessionObj;
                if (cursor != null) {
                    if (cursor.moveToFirst()) do {
                        long id = cursor.getLong(0);
                        String sessionId = cursor.getString(1);
                        long timestamp = cursor.getLong(2);
                        JSONObject data = JSoNHelper.fromString(cursor.getString(3));

                        list.add(sessionObj = new SessionObj(id, sessionId, timestamp, data));
                        sessions.put(sessionId, sessionObj);
                    } while (cursor.moveToNext());
                    if (addTodayIfNull && sessions.get(Globals.getSessionId()) == null) {
                        list.add(storeSession(Globals.getSessionId(), false));
                    }
                    cursor.close();
                }
            } catch (Exception e) {
                LLog.e("Exception in DB query or cursor", e);
            }
        }
        return list;
    }

    private List<SessionObj> _getSessions(boolean addTodayIfNull) {
        String orderBy = DbHelper.COL_SESSION;
        return getSessions(null, null, orderBy, 0, addTodayIfNull);
    }

    public static List<SessionObj> getSessions(boolean addTodayIfNull) {
        List<SessionObj> result = new ArrayList<>();
        if (!Globals.runMode.isRun()) return result;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._getSessions(addTodayIfNull);
            }
        }
        return result;
    }

    private boolean updateSession(Data data) {
        if (db == null) return false;
        if (data == null || !data.hasData()) return false;
        String sessionId = Globals.getSessionId();
        if (sessions.size() == 0) getSessions(true);
        SessionObj sessionObj = sessions.get(sessionId);
        if (sessionObj == null) sessionObj = storeSession(sessionId, false);
        if (sessionObj != null) sessionObj.setData(data);
        return saveSession();
    }

    private boolean saveSession() {
        if (System.currentTimeMillis() - lastSessionSave < SESSION_INTERVAL) return false;

        lastSessionSave = System.currentTimeMillis();

        SessionObj sessionObj = sessions.get(Globals.getSessionId());

        if (sessionObj == null) return false;
        if (!sessionObj.data.hasData()) return false;
        boolean success = false;
        LLog.i("Saving session data");
        synchronized (dbLock) {
            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_DATA, sessionObj.data.toJSON(translator).toString());
            try {
                success = db.update(DbHelper.SESSION_TABLE,
                        values,
                        DbHelper.COL_SESSION + " = ?",
                        new String[] {sessionObj.sessionId}) > 0;
            } catch (Exception e) {
                LLog.e("Exception in DB update", e);
            }
            for (NewDataListener listener: newDataListeners) listener.onSession(sessionObj);

        }
        return success;
    }

    private long insertData(DataObj dataObj) {
        if (db == null) return -1;
        synchronized (dbLock) {

            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_SESSION, dataObj.sessionId);
            values.put(DbHelper.COL_CATEGORY, dataObj.category.toString());
            values.put(DbHelper.COL_SHARED, dataObj.getShared().ordinal());
            values.put(DbHelper.COL_TIMESTAMP, dataObj.timeStamp);
            values.put(DbHelper.COL_DATA, dataObj.data.toString());
            try {
                dataObj.id = db.insert(DbHelper.DATA_TABLE, null, values);
            } catch (Exception e) {
                LLog.e("Exception in DB insert",e);
            }
        }
        //LLog.i("Inserted: " + dataObj);

        return dataObj.id;
    }

//    public boolean updateData(DataObj dataObj) {
//        if (db == null) return false;
//        boolean success;
//
//        if (dataObj.id > -1) synchronized (dbLock) {
//            LLog.i("Update using id for " + dataObj);
//            ContentValues values = new ContentValues();
//            values.put(DbHelper.COL_TIMESTAMP, dataObj.timeStamp);
//            values.put(DbHelper.COL_DATA, dataObj.data.toString());
//            success = db.update(DbHelper.DATA_TABLE,
//                    values,
//                    DbHelper.COL_ID + " = ?",
//                    new String[] {"" + dataObj.id}) > 0;
//        } else synchronized (dbLock) {
//            LLog.i("Update using sensorId, category and session for " + dataObj);
//            ContentValues values = new ContentValues();
//            values.put(DbHelper.COL_TIMESTAMP, dataObj.timeStamp);
//            values.put(DbHelper.COL_DATA, dataObj.data.toString());
//            success = db.update(DbHelper.DATA_TABLE,
//                    values,
//                    DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_SESSION + " = ?",
//                    new String[] {dataObj.category.toString(), dataObj.sessionId.toString()}) > 0;
//        }
//        //LLog.i("Updated: " + dataObj + ", success = " + success);
//
//        return success;
//    }

    private boolean _updateShared(DataObj dataObj, Shared shared) {
        if (db == null) return false;
        boolean success = false;
        if (dataObj.getId() > -1) synchronized (dbLock) {
            //LLog.i("Update shared with " + shared + ", using id for " + dataObj);
            dataObj.shared = shared;
            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_SHARED, shared.ordinal());
            try {
                success = db.update(DbHelper.DATA_TABLE,
                        values,
                        DbHelper.COL_ID + " = ?",
                        new String[]{"" + dataObj.id}) > 0;
            } catch (Exception e) {
                LLog.e("Exception in DB update",e);
            }
        } else synchronized (dbLock) {
            LLog.i("Update shared with " +shared + ", using sensorId, category and session for " + dataObj);
            dataObj.shared = shared;
            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_SHARED, shared.ordinal());
            try {
                success = db.update(DbHelper.DATA_TABLE,
                        values,
                        DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_SESSION + " = ? AND " + DbHelper.COL_TIMESTAMP + " = ?",
                        new String[] {dataObj.category.toString(), dataObj.sessionId, "" + dataObj.timeStamp}) > 0;
            } catch (Exception e) {
                LLog.e("Exception in DB update",e);
            }
        }
        return success;
    }


    private boolean _updateShared(String sessionId, Shared shared) {
        if (db == null) return false;
        boolean success = false;
        ContentValues values = new ContentValues();
        values.put(DbHelper.COL_SHARED, shared.ordinal());
        try {
            success = db.update(DbHelper.DATA_TABLE,
                    values,
                    DbHelper.COL_SESSION + " = ?",
                    new String[]{sessionId}) > 0;
        } catch (Exception e) {
            LLog.e("Exception in DB update",e);
        }

        return success;
    }

    public static boolean updateShared(DataObj dataObj, Shared shared) {
        boolean result = false;
        if (!Globals.runMode.isRun()) return false;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._updateShared(dataObj, shared);
            }
        }
        return result;
    }

    public static boolean updateShared(String sessionId, Shared shared) {
        boolean result = false;
        if (!Globals.runMode.isRun()) return false;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._updateShared(sessionId, shared);
            }
        }
        return result;
    }


//    public List<DataObj> getData(JSoNField category) {
//        String selection = DbHelper.COL_CATEGORY + " = ? ";
//        String[] selectionArgs = new String[]{ category.toString()};
//        return getData(selection, selectionArgs, null, 0);
//    }

//    public List<DataObj> getData(JSoNField category, String sessionId) {
//
//        String selection = DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_SESSION + " = ?";
//        String[] selectionArgs = new String[]{ category.toString(), sessionId};
//        return getData(selection, selectionArgs, null, 0);
//    }

    private List<DataObj> getData(String selection, String[] selectionArgs, String orderby, int limit) {
        List<DataObj> list = new ArrayList<>();
        if (db == null) return list;
        String strLimit = null;
        if (limit > 0) strLimit = "" + limit;

        synchronized (dbLock) {
            try {
                Cursor cursor = db.query(
                        DbHelper.DATA_TABLE,            // table
                        new String[]{
                                DbHelper.COL_ID,
                                DbHelper.COL_SESSION,
                                DbHelper.COL_CATEGORY,
                                DbHelper.COL_SHARED,
                                DbHelper.COL_TIMESTAMP,
                                DbHelper.COL_DATA
                        },        // columns
                        selection,                      //selection
                        selectionArgs,                  // selection args
                        null,                           // group by
                        null,                           // having
                        orderby,                        // order by
                        strLimit);                      // limit

                if (cursor != null) {
                    if (cursor.moveToFirst()) do {
                        long id = cursor.getLong(0);
                        String sessionId = cursor.getString(1);
                        String categoryRead = cursor.getString(2);
                        int sharedRead = cursor.getInt(3);
                        long timestamp = cursor.getLong(4);
                        JSONObject data = JSoNHelper.fromString(cursor.getString(5));
                        list.add(new DataObj(id, sessionId, JSoNField.fromString(categoryRead), timestamp, Shared.fromOrdinal(sharedRead), data));
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            } catch (Exception e) {
                LLog.e("Exception: Could not query DB or move through cursor", e);
            }
        }
        return list;
    }

//    private void getData(final String selection, final String[] selectionArgs, final String orderby, final DataListener dataListener) {
//        SleepAndWake.runInNewThread(new Runnable() {
//            @Override
//            public void run() {
//                List<DataObj> list = getData(selection, selectionArgs, orderby, 0);
//                for (DataObj obj : list) {
//                    final DataObj finalObj = obj;
//                    SleepAndWake.runInMain(new Runnable() {
//                        @Override
//                        public void run() {
//                            dataListener.onData(finalObj);
//                        }
//                    }, "DbData.getData.onData");
//                    try {
//                        sleep(10);
//                    } catch (InterruptedException e) {
//                    }
//                }
//            }
//
//        }, "DbData.getData");
//    }

    private List<DataObj> _getData(JSoNField category, Shared shared) {
        String selection = DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_SHARED + " = ?";
        String[] selectionArgs = new String[]{category.toString(), "" + shared.ordinal()};
        return getData(selection, selectionArgs, null, 0);
    }

    public static List<DataObj> getData(JSoNField category, Shared shared) {
        List<DataObj> result = null;
        if (!Globals.runMode.isRun()) return null;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._getData(category, shared);
            }
        }
        return result;
    }


    private List<PropObj> _getProp(String selection, String[] selectionArgs, String orderby, int limit) {
        List<PropObj> list = new ArrayList<>();
        if (db == null) return list;
        String strLimit = null;
        if (limit > 0) strLimit = "" + limit;

        synchronized (dbLock) {
            try {
                Cursor cursor = db.query(
                        DbHelper.PROP_TABLE,            // table
                        new String[]{
                                DbHelper.COL_ID,
                                DbHelper.COL_NAME,
                                DbHelper.COL_CATEGORY,
                                DbHelper.COL_TIMESTAMP,
                                DbHelper.COL_DATA
                        },        // columns
                        selection,                      //selection
                        selectionArgs,                  // selection args
                        null,                           // group by
                        null,                           // having
                        orderby,                        // order by
                        strLimit);                      // limit

                if (cursor != null) {
                    if (cursor.moveToFirst()) do {
                        long id = cursor.getLong(0);
                        String nameRead = cursor.getString(1);
                        String categoryRead = cursor.getString(2);
                        long timestamp = cursor.getLong(3);
                        JSONObject data = JSoNHelper.fromString(cursor.getString(4));
                        list.add(new PropObj(id, nameRead, JSoNField.fromString(categoryRead), timestamp, data));
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            } catch (Exception e) {
                LLog.e("Exception in DB query or cursor", e);
            }

        }
        return list;
    }

    private PropObj _getProp(final JSoNField category, final String name) {
        if (db == null) return null;
        String selection = DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_NAME + " = ?";
        String[] selectionArgs = new String[]{category.toString(), name};
        List<PropObj> props = _getProp(selection, selectionArgs, null, 1);
        if (props.size() > 0) return props.get(0);
        return null;
    }

    static DbData.PropObj getProp(final JSoNField category, final String name){
        DbData.PropObj result = null;
        if (!Globals.runMode.isRun()) return null;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._getProp(category, name);
            }
        }
        return result;
    }

    private boolean _updateProp(PropObj propObj) {
        if (db == null) return false;
        boolean success = false;

        if (propObj.id > -1) synchronized (dbLock) {
            LLog.i("Update using id for " + propObj);
            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_TIMESTAMP, propObj.timeStamp);
            values.put(DbHelper.COL_DATA, propObj.data.toString());
            try {
                success = db.update(DbHelper.PROP_TABLE,
                        values,
                        DbHelper.COL_ID + " = ?",
                        new String[]{"" + propObj.id}) > 0;
            } catch (Exception e) {
                LLog.e("Exception in DB update", e);
            }
        } else synchronized (dbLock) {
            LLog.i("Update using sensorId, category and session for " + propObj);
            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_TIMESTAMP, propObj.timeStamp);
            values.put(DbHelper.COL_DATA, propObj.data.toString());
            try {
                success = db.update(DbHelper.PROP_TABLE,
                        values,
                        DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_NAME + " = ?",
                        new String[] {propObj.category.toString(), propObj.name}) > 0;
            } catch (Exception e) {
                LLog.e("Exception in DB update", e);
            }
        }
        //LLog.i("Updated: " + propObj + ", success = " + success);

        return success;
    }

//    public static boolean updateProp(PropObj propObj){
//        boolean result = false;
//        if (!Globals.runMode.isRun()) return false;
//        synchronized (instanceLock) {
//            if (instance != null) {
//                result = instance._updateProp(propObj);
//            }
//        }
//        return result;
//    }

    private long _insertProp(PropObj propObj) {
        if (db == null) return -1;
        synchronized (dbLock) {

            ContentValues values = new ContentValues();
            values.put(DbHelper.COL_NAME, propObj.name);
            values.put(DbHelper.COL_CATEGORY, propObj.category.toString());
            values.put(DbHelper.COL_TIMESTAMP, propObj.timeStamp);
            values.put(DbHelper.COL_DATA, propObj.data.toString());
            try {
                propObj.id = db.insert(DbHelper.DATA_TABLE, null, values);
            } catch (Exception e) {
                LLog.e("Exception in DB insert", e);
            }
        }
        //LLog.i("Inserted: " + propObj);

        return propObj.id;
    }

    static boolean updateOrInsertProp(PropObj propObj){
        boolean result = false;
        if (!Globals.runMode.isRun()) return false;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                result = instance._updateProp(propObj) || instance._insertProp(propObj) == -1;
            }
        }
        return result;
    }

    private void _restoreData(final String sessionId, final long interval, final DoneListener listener) {
        if (db == null) return;
        SleepAndWake.runInNewThread(() -> {
            String selection = DbHelper.COL_CATEGORY + " = ? AND " + DbHelper.COL_SESSION + " = ?";
            String[] selectionArgs = new String[]{JSoNField.DATA.toString(), sessionId};
            String orderBy = DbHelper.COL_TIMESTAMP;
            List<DataObj> list = getData(selection, selectionArgs, orderBy, 0);
            Man.add(DbData.this);
            ScannedDevice device;
            Data data = null;
            for (DataObj obj : list) {
                data = Data.fromJSON(obj.getData());
                if (data != null) {
                    Map<String, Data> splitData = data.split(reverseTranslator);
                    if (splitData != null && splitData.size() > 0) for (String sensorId: splitData.keySet()) {

                        if ((device = playbackDevices.get(sensorId)) == null) {
                            String name = PreferenceHelper.get("ADDRESS2NAME_" + sensorId, sensorId);
                            playbackDevices.put(sensorId, device = new DbDevice(sensorId, name));
                        }
                        Data split = splitData.get(sensorId);
                        ((DbDevice)device).addData(split);
                        Man.fireData(device, split, false);
                    }
                }
                try {
                    sleep(interval);
                } catch (InterruptedException e) {
                    LLog.i("Interrupted");
                }
            }
            if (data != null) LLog.i("Last read data of " + TimeUtil.formatMilli(data.getTime(), 1));

            Man.remove(DbData.this);
            playbackDevices.clear();
            SleepAndWake.runInMain(listener::onDone, "DbData.restoreData.done");
        },"DbData.restoreData");

    }

    public static void restoreData(final String sessionId, final long interval, final DoneListener listener) {
        if (!Globals.runMode.isRun()) return;
        synchronized (instanceLock) {
            DbData instance = getInstance();
            if (instance != null) {
                instance._restoreData(sessionId, interval, listener);
            }
        }
    }

    private String currentSessionId = null;
    private Data mergedData = null;
    private Data sendData = null;
    private boolean valueChanged = false;
    private long timeSent = 0;
    private Map<String, Integer> onDeviceDataCounts = new HashMap<>();
    private Map<String, Data> onDeviceDataData = new HashMap<>();
    private long prevLog = System.currentTimeMillis();

    @Override
    public void onDeviceData(final ScannedDevice device, Data incomingData, boolean isNew) {
        if (!isNew) return;
        if (!Globals.runMode.isRun()) {
            LLog.i("Removing from Man");
            Man.removeListener(this);
            return;
        }

        long now = System.currentTimeMillis();
        onDeviceDataCounts.put(device.getName(), onDeviceDataCounts.get(device.getName()) == null ? 1 : onDeviceDataCounts.get(device.getName()) + 1);
        if (onDeviceDataData.get(device.getName()) == null) {
            PreferenceHelper.set("ADDRESS2NAME_" + device.getAddress(), device.getName());
        }
        onDeviceDataData.put(device.getName(), incomingData);
        if (now - prevLog > Timing.LOG_INTERVAL) {
            prevLog = now;
            Stringer stringer = new Stringer("\n");
            stringer.append("DbData.onDeviceData called by:");
            for (String deviceName : onDeviceDataCounts.keySet()) {
                Data data;
                if ((data = onDeviceDataData.get(deviceName)) != null)
                    stringer.append("   " + Format.format(deviceName, Globals.TAG_WIDTH) + " " + Format.format("" + onDeviceDataCounts.get(deviceName), 8) + " " + StringUtil.toString(data.getDataTypes()));
                else
                    stringer.append("   " + Format.format(deviceName, Globals.TAG_WIDTH) + " " + Format.format("" + onDeviceDataCounts.get(deviceName), 8));
            }
            if (DEBUG) LLog.i(stringer.toString());
        }

        Timing.log(DbData.class.getSimpleName() + "_onDeviceData", Timing.Action.BEGIN);
        checkCount++;
        try {
            if (currentSessionId == null || !currentSessionId.equals(Globals.getSessionId())) {
                storeSession(currentSessionId = Globals.getSessionId(), true);
            }

            //incomingData.setSensorId(device.getAddress());
            Data filteredData = incomingData.clone(new Data.Checker() {
                @Override
                public boolean isChecked(DataType type) {
                    return type.isShareAlways() || (type.isSharable() && ScannedDevice.isDataTypeViewOrSpeech(device.getAddress(), type));
                }
            });
            if (!filteredData.hasData()) {
                //LLog.i("Filtered mergedDatas is empty for " + incomingData);
                return;
            }

            boolean shouldSave = false;
            if (sendData == null) {
                sendData = filteredData.clone();
            } else {
                sendData.merge(filteredData);
            }
            if (mergedData == null) {
                mergedData = filteredData.clone();
                shouldSave = true;
            } else {
                if (mergedData.valueChanged(filteredData)) valueChanged = true;
                if (filteredData.getTime() - timeSent > DATA_INTERVAL && valueChanged) shouldSave = true;
                mergedData.merge(filteredData);
            }
            if (shouldSave) {
                saveCount++;
                updateSession(mergedData);
                valueChanged = false;
                timeSent = System.currentTimeMillis();
                //final JSONObject mergedJSON = mergedData.toJSON(translator);
                final JSONObject mergedJSON = sendData.toJSON(translator);
                final long time = mergedData.getTime();
                //final String address = device.getAddress();

                DbData.DataObj obj = new DbData.DataObj(currentSessionId, JSoNField.DATA, time, DbData.Shared.NEW, mergedJSON);
                obj.setShared(DbData.Shared.NOT_SHARED);
                insertData(obj);
                for (NewDataListener listener: newDataListeners) listener.onData(obj);
                sendData = null;
            }
        } finally {
            Timing.log(DbData.class.getSimpleName() + "_onDeviceData", Timing.Action.END, "checked: " + checkCount + ", saved: " + saveCount);
        }
    }

    private static Map<String, String> idTranslations = new HashMap<>();
    private static Map<String, String> idReverseTranslations = new HashMap<>();


    public static Data.SensorIdTranslator translator = new Data.SensorIdTranslator() {
        @Override
        public String translate(String sensorId) {
            String name;
            if ((name = idTranslations.get(sensorId)) == null) {
                ScannedDevice device = Man.getDeviceByAddress(sensorId);
                if (device == null)
                    name = PreferenceHelper.get("SensorIdName_" + sensorId, (String) null);
                else
                    name = device.getName();

                if (name != null) {
                    name = name.replace(" ", "_").replace("-", "_");
                    PreferenceHelper.set("SensorIdName_" + sensorId, name);
                    if (!name.equals(sensorId)) {
                        String sensorIdReverse = idReverseTranslations.get(name);
                        if (sensorIdReverse != null && !sensorIdReverse.equals(sensorId))
                            name = name + "_" + sensorId;
                    }
                    PreferenceHelper.set("NameSensorId_" + name, sensorId);
                    idTranslations.put(sensorId, name);
                    idReverseTranslations.put(name, sensorId);
                    PreferenceHelper.set(sensorId, name);
                    return name;
                }

            } else {
                return name;
            }
            return sensorId;
        }
    };

    private static Data.SensorIdTranslator reverseTranslator = new Data.SensorIdTranslator() {
        @Override
        public String translate(String name) {
            String sensorId = idReverseTranslations.get(name);
            if (sensorId != null) return sensorId;
            sensorId = PreferenceHelper.get("NameSensorId_" + name, (String) null);
            if (sensorId != null) {
                idReverseTranslations.put(name, sensorId);
                return sensorId;
            }
            ScannedDevice device = Man.getDeviceByName(name);
            if (device != null) {
                sensorId = device.getAddress();
                idReverseTranslations.put(name, sensorId);
                return sensorId;
            }
            return name;
        }
    };


    @Override
    public void onDeviceScanned(ScannedDevice device, boolean isNew) {

    }

    @Override
    public void onDeviceState(ScannedDevice device, Device.StateInfo state) {

    }

    @Override
    public void onScanning(ScanType scanType, boolean on) {

    }

}

