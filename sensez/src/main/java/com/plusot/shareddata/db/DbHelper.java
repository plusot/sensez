package com.plusot.shareddata.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.plusot.util.logging.LLog;


public class DbHelper extends SQLiteOpenHelper {
//    public enum Type {
//        EVENT,
//        CATEGORIES,
//        ATHLETES,
//        RESULTS,
//        UPDATE,
//        ;
//    }

    public static final String DATA_TABLE = "data";
    public static final String PROP_TABLE = "props";
    public static final String COL_ID = "id";
    public static final String COL_CATEGORY = "cat";
    public static final String COL_NAME = "name";
    public static final String COL_SESSION = "session";
    public static final String COL_TIMESTAMP = "timestamp";
    public static final String COL_SHARED = "shared";
    public static final String COL_DATA = "data";
    public static final String SESSION_TABLE = "sessions";
    private static final String[] CREATE_DATABASE = {
            "CREATE TABLE " + DATA_TABLE + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_CATEGORY + " TEXT NOT NULL, " +
                    COL_SESSION + " TEXT NOT NULL, " +
                    COL_TIMESTAMP + " INTEGER DEFAULT 0, " +
                    COL_SHARED + " INTEGER DEFAULT 0, " +
                    COL_DATA + " TEXT NOT NULL " +   //max size = 1000.000.000 bytes (Sqlite specification).
                    ")",
            "CREATE INDEX data_session_idx ON " + DATA_TABLE + " (" + COL_SESSION + ")",
            "CREATE INDEX data_category_idx ON " + DATA_TABLE + " (" + COL_CATEGORY + ")",
            "CREATE INDEX data_shared_idx ON " + DATA_TABLE + " (" + COL_SHARED + ")",
            "CREATE TABLE " + SESSION_TABLE + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SESSION + " TEXT NOT NULL, " +
                    COL_TIMESTAMP + " INTEGER DEFAULT 0, " +
                    COL_DATA + " TEXT DEFAULT NULL " +   //max size = 1000.000.000 bytes (Sqlite specification).
                    ")",
            "CREATE UNIQUE INDEX sessions_session_idx ON " + SESSION_TABLE + " (" + COL_SESSION + ")",
            "CREATE TABLE " + PROP_TABLE + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_NAME + " TEXT NOT NULL, " +
                    COL_CATEGORY + " TEXT NOT NULL, " +
                    COL_TIMESTAMP + " INTEGER DEFAULT 0, " +
                    COL_DATA + " TEXT NOT NULL " +   //max size = 1000.000.000 bytes (Sqlite specification).
                    ")",
            "CREATE INDEX props_name_idx ON " + PROP_TABLE + " (" + COL_NAME + ")",
            "CREATE UNIQUE INDEX props_name_category_idx ON " + PROP_TABLE + " (" + COL_NAME + "," + COL_CATEGORY + ")",
            "CREATE INDEX props_category_idx ON " + PROP_TABLE + " (" + COL_CATEGORY + ")",


    };
    public static final String DATABASE_NAME = "sense_data.db";
    public static final int DATABASE_VERSION = 10;

    public DbHelper(Context context, CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    // Called when no database exists in disk and the helper class needs
    // to create a new one.
    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String statement: CREATE_DATABASE) db.execSQL(statement);
    }

    // Called when there is a database version mismatch meaning that the version
    // of the database on disk needs to be upgraded to the current version.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Log the version upgrade.
        LLog.i("Upgrading from version " +
                oldVersion + " to " +
                newVersion + ", which will destroy all old data");

        // The simplest case is to drop the old table and create a new one.
        db.execSQL("DROP TABLE IF EXISTS " + PROP_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATA_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SESSION_TABLE);
        // Create a new one.
        onCreate(db);
    }
}
