package com.plusot.shareddata.db;

import com.plusot.sensez.device.DeviceType;
import com.plusot.sensez.device.ScannedDevice;
import com.plusot.util.data.Data;
import com.plusot.util.data.DataType;

/**
 * Created by peet on 14/12/16.
 */

public class DbDevice extends ScannedDevice {
    private final String address;
    private final String name;

    public DbDevice(String address, String name) {
        super(DeviceType.TYPE_UNKNOWN);
        this.address = address;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public int getIconId() {
        return 0;
    }

    @Override
    public void close() {

    }

    @Override
    public String getManufacturer() {
        return "";
    }

    @Override
    public boolean isSelected() {
        return true;
    }

    @Override
    public DataType[] getDataTypes() {
        return deviceData.getDataTypes();
    }

    public void addData(Data data) {
        deviceData.merge(data);
    }
}
