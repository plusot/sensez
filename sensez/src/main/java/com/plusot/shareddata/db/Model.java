package com.plusot.shareddata.db;

import android.content.Context;

import com.plusot.shareddata.share.HttpTask;
import com.plusot.util.Globals;
import com.plusot.util.json.JSoNField;
import com.plusot.util.json.JSoNHelper;
import com.plusot.util.share.Http;
import com.plusot.util.logging.LLog;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by peet on 17-03-16.
 */
public class Model {
    public static final String DELIM = ";";
    private static Model instance = null;
    private Map<String, DbData.PropObj> map = new HashMap<>();
    private JSONObject assetJson = null;
    private static boolean readFromDb = false;


    public static Model getInstance() {
        if (instance != null) return instance;
        if (!Globals.runMode.isRun()) return null;
        synchronized (DbData.class) {
            if (instance == null) instance = new Model();
        }
        return instance;
    }

    public static void stopInstance() {
        if (instance != null) synchronized (Model.class) {
            if (instance != null) instance.close();
            instance = null;
        }
    }

    private Model() {

    }

    private void close() {

    }

    private JSONObject readFromAsset(String name, JSoNField field) {
        if (assetJson == null) {
            Context appContext = Globals.getAppContext();
            if (appContext == null) return null;

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(appContext.getAssets().open("db/db.json")));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                assetJson = JSoNHelper.fromString(sb.toString());

            } catch (IOException e) {
                LLog.e("Could not read db.json from assets: " + e.getMessage());
            } finally {

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
        }
        return JSoNHelper.getObj(assetJson, name);

    }

    private String readFromAsset(String name, JSoNField field, String defaultValue) {
        JSONObject obj =  readFromAsset(name, field);
        return JSoNHelper.get(obj, field, defaultValue);
    }

    private int readFromAsset(String name, JSoNField field, int defaultValue) {
        JSONObject obj =  readFromAsset(name, field);
        return JSoNHelper.get(obj, field, defaultValue);
    }

    public DbData.PropObj getProp(final String name, final JSoNField field) {
        DbData.PropObj obj;
        final String nameCat = name + DELIM + field.toString();
        if ((obj = map.get(nameCat)) == null) {
            if (HttpTask.useServer) Http.get("api/sensor/" + name + "/" + field.toString(), new Http.Listener() {
                @Override
                public void onResult(Http.Error error, String info, JSONObject json) {
                    if (error == Http.Error.NOERROR) {
                        String result = JSoNHelper.get(json, JSoNField.RESULT, (String) null);
                        if (result != null && result.equals(JSoNField.ERROR.toString())) {
                            LLog.i("Failed with server error: " + JSoNHelper.get(json, JSoNField.INFO, (String) null));
                        } else {
                            DbData.PropObj tempObj;
                            DbData.updateOrInsertProp(tempObj = new DbData.PropObj(name, field, json));
                            map.put(nameCat, tempObj);
                            LLog.i("Success " + info + ": " + json.toString());
                        }
                    } else if (json == null) {
                        LLog.i("Failed " + info + " with error: " + error);
                    } else {
                        LLog.i("Failed " + info + " with error: " + error + " and " + json.toString());
                    }
                }
            }, false);
            obj = DbData.getProp(field, name);
            if (obj != null) map.put(nameCat, obj);
        }
        return obj;
    }


    public String getString(String name, JSoNField field, String defaultValue) {
        if (readFromDb) {
            DbData.PropObj obj = getProp(name, field);
            if (obj != null && obj.getData().has(field.toString()))
                return JSoNHelper.get(obj.getData(), field, defaultValue);
        }
        return readFromAsset(name, field, defaultValue);
    }

    public int getHexInt(String name, JSoNField field, int defaultValue) {
        DbData.PropObj obj = getProp(name, field);
        String strValue;
        if (readFromDb && obj != null && obj.getData().has(field.toString()))
            strValue = JSoNHelper.get(obj.getData(), field, String.format("0x%x", defaultValue));
        else
            strValue = readFromAsset(name, field, String.format("0x%x", defaultValue));

        if (strValue != null) try {
            return Integer.parseInt(strValue.replace("0x", ""), 16);
        } catch (NumberFormatException e) {
            LLog.e("NumberFormatException " + e.getMessage());

        }
        return defaultValue;
    }

    public int getInt(String name, JSoNField field, int defaultValue) {
        DbData.PropObj obj = getProp(name, field);
        int value;
        if (readFromDb && obj != null && obj.getData().has(field.toString()))
            value = JSoNHelper.get(obj.getData(), field, defaultValue);
        else
            value = readFromAsset(name, field, defaultValue);
        return value;

    }
}
