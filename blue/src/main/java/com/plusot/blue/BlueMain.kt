package com.plusot.blue

import android.app.Activity
import android.content.Context
import android.hardware.SensorManager
import android.os.Bundle
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.LibOption
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.*
import com.plusot.bluelib.sensor.bluetooth.ble.BlePeripheralServer
import com.plusot.bluelib.sensor.bluetooth.ble.HeartRateBlePeripheral
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.sensor.data.DeviceDataType
import com.plusot.bluelib.util.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.concurrent.timer
import kotlin.coroutines.CoroutineContext

/**
 * Package: com.plusot.blue
 * Project: sensez
 *
 * Created by Peter Bruinink on 2019-06-11.
 * Copyright © 2019 Plusot. All rights reserved.
 */

class BlueMain: Activity(), CoroutineScope {
    private lateinit var job: Job
    private lateinit var sensorManager: SensorManager

    private val deviceDataTypes = mutableListOf<DeviceDataType>()
    private val newDeviceDataTypes = mutableListOf<DeviceDataType>()
    private val currentData = Data()
    private val currentAccuracy = mutableMapOf<String, SensorAccuracy>()
    private var currentDeviceDataType: DeviceDataType? = null
    private var switchTime = 0L
    private var periServer: BlePeripheralServer? = null

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val handler = CoroutineExceptionHandler { _, throwable ->
        LLog.e("Exception: $throwable")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        job = Job()
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // Enables Always-on

        launch(handler) {
            delay(1000)
            LLog.d("Inet Address: " + this@BlueMain.getIpAddress())
            BlueLib.start(
                    this@BlueMain,
//                    LibOption.USE_INTERNAL_SENSORS,
                    LibOption.USE_BODY_SENSORS,
                    LibOption.USE_BLE_SENSORS
            )
            BlueLib.listeners.add(object: BlueLib.Listener {
                override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {
                    LLog.d("$device - ${device.manufacturer} scanned")
                    device.isSelected = when (device.type) {
                        DeviceType.BLUETOOTH_TYPE_LE -> device.name.startsWith("Ticwatch", true) // || device.name.startsWith("Mio-FUSE", true)
                        DeviceType.INTERNAL_GPS -> false
                        DeviceType.INTERNAL_CLOCK,
                        DeviceType.INTERNAL_ACCELEROMETER,
                        DeviceType.INTERNAL_LINEAR_ACCELERATION,
                        DeviceType.INTERNAL_STEP_COUNTER,
                        DeviceType.INTERNAL_HEART_RATE_MONITOR -> false
                        else -> false
                    }
                }

                override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo, accuracy: SensorAccuracy?) {
                    LLog.d("$device state= $state, accuracy= $accuracy")
                    if (accuracy != null) currentAccuracy[device.address] = accuracy
                }

                override fun onScanning(scanType: ScanType, on: Boolean) {
                    LLog.d("Scan type $scanType = $on")
                }

                override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
                    //LLog.d("$device data $data")
                    currentData += data

                    data.dataTypes
                            .filter {
                                when (it) {
                                    DataType.TIME_CLOCK,
                                    DataType.HEART_RATE,
                                    DataType.ACCELERATION,
                                    DataType.STEPS,
                                    DataType.STEPS_TOTAL -> true
                                    else -> false
                                }
                            }
                            .forEach {
                                val ddt = DeviceDataType(device, it)
                                deviceDataTypes.addIf(ddt)
                                if (ddt == currentDeviceDataType) showValue(ddt)
                                if (it == DataType.HEART_RATE) periServer?.setValue(data)
                            }

                    if (isNew) data.dataTypes.forEach {
                        newDeviceDataTypes.addIf(DeviceDataType(device, it)) { LLog.d("New device - datatype: $it") }
                    }
                }
            })

            timer(name ="ScreenWriter",
                    daemon = true,
                    period = 1000) {
                val now = System.currentTimeMillis()
                if (currentDeviceDataType == null)
                    currentDeviceDataType = deviceDataTypes.firstOrNull()
                else
                    currentDeviceDataType?.let {
                        if (now - switchTime > 5000L) {
                            currentDeviceDataType = deviceDataTypes.nextElement(it)
                            switchTime = now
                        }
                    }
                currentDeviceDataType?.let { showValue(it) }
            }

            periServer = BlePeripheralServer(setOf(HeartRateBlePeripheral()))
        }
    }

    private fun showValue(ddt: DeviceDataType) {
        runInMain {
            val address = ddt.device.address
            valueView.setTextColor(currentAccuracy[address]?.color ?: resources.getColored(R.color.unknownColor))
            sensorNameView.text = ddt.device.name
            fieldView.text = ddt.dataType.toProperString()
            currentData.toValuePair(address, ddt.dataType)?.let { (value, unit) ->
                valueView.text = value
                unitView.text = unit
            }
        }
    }

    override fun onResume() {
        BlueLib.resume()
        super.onResume()
    }

    override fun onPause() {
        BlueLib.pause()
        super.onPause()
    }

    override fun onDestroy() {
        BlueLib.close()
        periServer?.close()
        job.cancel()
        super.onDestroy()
    }

}