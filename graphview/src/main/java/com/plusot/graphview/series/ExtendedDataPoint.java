/**
 * GraphView
 * Copyright 2016 Jonas Gehring
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.plusot.graphview.series;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * default data point implementation.
 * This stores the x and y values.
 *
 * @author jjoe64
 */
public class ExtendedDataPoint implements ExtendedDataPointInterface, Serializable {
    private static final long serialVersionUID=1428263322645L;

    private double x;
    private double[] ys;

    public ExtendedDataPoint(double x, double y) {
        this.x=x;
        this.ys = new double[] {y};
    }

    public ExtendedDataPoint(Date x, double y) {
        this.x = x.getTime();
        this.ys = new double[] {y};
    }

    public ExtendedDataPoint(Date x, double[] ys) {
        this.x = x.getTime();
        this.ys = Arrays.copyOf(ys, ys.length);
        if (ys.length > 1) for (int i = 1; i < ys.length; i++) {
            this.ys[i] += this.ys[i - 1];
        }
    }


    public ExtendedDataPoint(Date x, double[] parts, double y) {
        this.x = x.getTime();
        this.ys = Arrays.copyOf(parts, parts.length);

        double sum = 0;
        for (int i = 0; i < ys.length; i++) sum += parts[i];
        if (ys.length > 0) for (int i = 0; i < ys.length; i++) if (i > 0) {
            this.ys[i] = this.ys[i - 1] + y * parts[i] / sum;
        } else {
            this.ys[i] = y * parts[i] / sum;
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return ys[ys.length - 1];
    }

    @Override
    public double[] getYs() {
        return Arrays.copyOf(ys, ys.length);
    }

    @Override
    public String toString() {
        return "["+x+"/"+ys[ys.length - 1]+"]";
    }


}
