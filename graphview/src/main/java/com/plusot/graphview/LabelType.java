package com.plusot.graphview;

/**
 * Package: com.plusot.graphview
 * Project: $diabetes24android
 * <p>
 * Created by Peter Bruinink on 20-03-18.
 * Copyright © 2018 Brons Ten Kate. All rights reserved.
 */

public enum LabelType {
    X,
    Y,
    VERTICAL;

    public boolean isValueX() {
        switch(this) {
            case X: return true;
            case Y: return false;
            case VERTICAL: return true;
        }
        return true;
    }

    public static LabelType fromIsValueX(boolean value) {
        if (value) return X;
        return Y;
    }
}
