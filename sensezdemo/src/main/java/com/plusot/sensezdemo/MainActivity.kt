package com.plusot.sensezdemo

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.FrameLayout
import com.plusot.sensez.SensezLib
import com.plusot.sensez.activities.DeviceListActivity
import com.plusot.sensez.device.*
import com.plusot.sensez.internal.GpsDevice
import com.plusot.util.data.Data
import com.plusot.util.data.DataType
import com.plusot.util.dialog.Alerts
import com.plusot.util.kotlin.format
import com.plusot.util.kotlin.getString
import com.plusot.util.kotlin.startActivity
import com.plusot.util.kotlin.visible
import com.plusot.util.logging.LLog
import com.plusot.util.preferences.PreferenceHelper
import com.plusot.util.util.FileUtil
import com.plusot.util.util.SleepAndWake
import com.plusot.util.util.TimeUtil
import com.plusot.util.util.ToastHelper
import com.plusot.util.widget.SmoothLineView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class TypeAddress(val type: DataType, val device: ScannedDevice) {
    override fun toString() = "$type - $device"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TypeAddress

        if (type != other.type) return false
        if (device.address != other.device.address) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + device.address.hashCode()
        return result
    }
}


class MainActivity : Activity(), Man.Listener {
    private val types = mutableListOf<TypeAddress>()//EnumSet.noneOf(DataType::class.java)
    private val validTypes = mutableListOf<TypeAddress>()
    private var typeToShow: TypeAddress? = null
    private var prevSwitch: Long = 0L
    private val smoothLines = mutableListOf<SmoothLineView>()
    private val currentData = Data()
    private var mayToggleTypeToShow = true

    private fun getLineRColorInt(i: Int) = when (i) {
        0 -> R.color.colorX
        1 -> R.color.colorY
        2 -> R.color.colorZ
        else -> R.color.color0
    }

    private fun getLineColor(i: Int) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        resources.getColor(getLineRColorInt(i), null)
    } else {
        @Suppress("DEPRECATION")
        resources.getColor(getLineRColorInt(i))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

//        Globals.init(application, "onCreate")
        Man.addListener(this)
//        GpsDevice.debugNmea = true
        SensezLib.create(
                application,
                this,
                EnumSet.of(
                        SensezLib.Option.USE_INTERNAL_SENSORS,
                        SensezLib.Option.USE_ANT_SENSORS,
                        SensezLib.Option.USE_BT_SENSORS,
                        SensezLib.Option.USE_BLE_SENSORS,
                        SensezLib.Option.USE_NMEA,
//                        SensezLib.Option.CHECK_GPS,
                        SensezLib.Option.START_IMMEDIATELY
                )
        )



        hamburger.setOnClickListener {
            //            startActivity<DeviceListActivity>{ intent ->
//                intent.putExtra(DeviceListActivity.EXTRA_DEVICETYPES, arrayOf("BLUETOOTH_TYPE_CLASSIC", "BLUETOOTH_DUAL", "INTERNAL_GPS"))
//            }

            val menuItems = mutableListOf(
                    R.string.sensors.getString(),
                    R.string.types.getString()
            )

            Alerts.showItemsDialog(this, R.string.menu, menuItems.toTypedArray())  { _, whichString, _ ->
                when (whichString) {
                    R.string.sensors.getString() -> startActivity<DeviceListActivity>()
                    R.string.types.getString() -> {
                        val typeList = types.map { it.toString() }
                        val selected = types.map { validTypes.contains(it)}
                        Alerts.showItemsDialog(this, R.string.types, typeList.toTypedArray(), selected.toBooleanArray()) { result, whichStr, which ->
                            if (which >= 0) {
                                if (result == Alerts.ClickResult.YES) {
                                    validTypes.add(types[which])
                                    PreferenceHelper.set("Valid_$whichStr", true)
                                } else {
                                    validTypes.remove(types[which])
                                    PreferenceHelper.set("Valid_$whichStr", false)
                                }
                            }
                        }
                    }
                }
            }
        }

        //GattAttribute plugin to override or implement not existing characteristic parsing.
//        GattAttribute.HEART_RATE_MEASUREMENT.setParseDelegate { gatt, characteristic ->
//            LLog.i("*** Heart rate characteristic value to parse: ${StringUtil.toHexString(characteristic.value)} ***")
//            GattAttribute.HEART_RATE_MEASUREMENT.parse(gatt, characteristic)
//        }
//
//        GattAttribute.BATTERY_LEVEL.setParseDelegate { gatt, characteristic ->
//            LLog.i("Battery level characteristic value to parse: ${StringUtil.toHexString(characteristic.value)}")
//            val data = Data().add(gatt.device.address, DataType.BATTERY_VOLTAGE, 0.033 * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0))
//            data.add(GattAttribute.BATTERY_LEVEL.parse(gatt, characteristic))
//        }

        for (i in 0..2) {
            val smoothLine = object : SmoothLineView(this, 200, 1) {
                //            val smoothLine = object : SmoothLineView(this, 20.0f, -10.0f, 200, false, false, 1) {
                init {
                    setHasCircles(false)
                    id = View.NO_ID
                    setColor(getLineColor(i)) //0x70cd81fc);
                    setStrokeWidth(6f)
                    visibility = View.INVISIBLE
                    setFill(false)
                }
            }
            lineView.addView(smoothLine)
            smoothLine.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            smoothLines.add(smoothLine)
        }
        dataValue.setOnClickListener {
            mayToggleTypeToShow = mayToggleTypeToShow xor true
            smoothLines.forEach { line -> line.clearValues() }
            ToastHelper.showToastLong(getString(R.string.mayToggle, mayToggleTypeToShow.toString()))
        }
        val info = packageManager.getPackageInfo(packageName, 0)
        @Suppress("DEPRECATION")
        version.text = R.string.version.getString(
                R.string.app_name.getString(),
                info.versionName,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                    info.longVersionCode
                else
                    info.versionCode,
                TimeUtil.formatTime(BuildConfig.BUILD_TIME),
                BuildConfig.FLAVOR
        )
        if (firstTime) Alerts.showOkDialog(this,R.string.instructionTitle, R.string.instruction, 5000)
        firstTime = false
        Man.setScanPeriods(10000, 2000)
        scanCount.visible = false
        FileUtil.systemLogToFile()


    }

    override fun onResume() {
        super.onResume()
        Man.addListener(this)
    }

    override fun onPause() {
        super.onPause()
        Man.removeListener(this)
        if (isFinishing) {
            FileUtil.systemLogToFile()
            SensezLib.close()
        }
    }

    override fun onDeviceScanned(device: ScannedDevice?, isNew: Boolean) {
        if (device == null) return
        SleepAndWake.runInMain { scanned.text = getString(R.string.scanned, device.name) }
    }

    override fun onDeviceState(device: ScannedDevice?, state: Device.StateInfo?) {
        LLog.i("Device ${device.toString()} state = $state")
    }

    override fun onScanning(scanType: ScanManager.ScanType?, on: Boolean) {
        SleepAndWake.runInMain {scanning.text = getString(R.string.scanning, scanType.toString())}
    }


    override fun onDeviceData(device: ScannedDevice?, data: Data?, isNew: Boolean) {
        val dataItems = data ?: return
        val now = System.currentTimeMillis()
        if (device == null) return
//        LLog.i("Data = $data")

        currentData.add(data)
        if (typeToShow == null) typeToShow = validTypes.firstOrNull()
        if (validTypes.size == 1 && typeToShow != validTypes.first()) typeToShow = validTypes.firstOrNull()
        if (validTypes.size > 1 && now - prevSwitch > 2000 && mayToggleTypeToShow) {
            prevSwitch = now
            typeToShow?.let {
                var index = validTypes.indexOf(it)
                index += 1
                index %= validTypes.size
                typeToShow = validTypes.getOrNull(index) ?: return
                SleepAndWake.runInMain {
                    val showDevice = typeToShow?.device ?: return@runInMain
                    val valueParts = currentData.toValueStringParts(showDevice.address, typeToShow?.type)
                    if (valueParts != null) {
                        typeToShow?.let {
                            dataDevice.text = if (showDevice.manufacturer != null) "${showDevice.manufacturer}\n${showDevice.name
                                    ?: ""}" else showDevice.name ?: ""
                        }
                        dataParameter.text = typeToShow?.type?.toProperString()
                        scanCount.text = if (showDevice.scanCount > 0) {
                            scanCount.visible = true
                            R.string.times.getString(showDevice.scanCount.format(1), showDevice.rssi)
                        } else {
                            scanCount.visible = false
                            ""
                        }

                        if (valueParts.size >= 2) {
                            dataValue.setText(valueParts[0])
                            dataUnit.text = valueParts[1] ?: ""
                        } else if (valueParts.size == 1) {
                            dataValue.setText(valueParts[0])
                            dataUnit.text = ""
                        }
                    }
                }
            }

        }
        for (type in dataItems.dataTypes) {
            if (!type.isSupported) continue
            val typeDevice = types.firstOrNull{ it.type == type && it.device.address == device.address } ?: TypeAddress(type, device).also {
                types.add(it)
                if (PreferenceHelper.get("Valid_$it", false)) validTypes.add(it)
                LLog.i("Added type $type for ${device.name}")
            }
            if (!validTypes.contains(typeDevice)) continue
            if (typeToShow == null) typeToShow = typeDevice

            if (type == DataType.ACCELERATION_XYZ && mayToggleTypeToShow) SleepAndWake.runInMain {
                smoothLines.forEach {  if (it.visibility == View.INVISIBLE) it.visibility = View.VISIBLE }
                val value = data.getDoubles(device.address, type)
                if (value.size >= 3) {
                    val fValues = value.map { it.toFloat() }
                    smoothLines.getOrNull(0)?.addValue(fValues[0])
                    smoothLines.getOrNull(1)?.addValue(fValues[1])
                    smoothLines.getOrNull(2)?.addValue(fValues[2])
                }
            } else if (!mayToggleTypeToShow && type == typeToShow?.type && device.address == typeToShow?.device?.address) SleepAndWake.runInMain {
                smoothLines.forEachIndexed { index, view ->
                    if (index == 0) {
                        if (view.visibility == View.INVISIBLE) { view.visibility = View.VISIBLE }
                    } else {
                        if (view.visibility == View.VISIBLE) { view.visibility = View.INVISIBLE }
                    }
                }
                smoothLines.getOrNull(0)?.addValue(data.getDouble(device.address, type).toFloat())
            }
            if (type == typeToShow?.type && device == typeToShow?.device) SleepAndWake.runInMain {
                dataDevice.text = if (device.manufacturer != null) "${device.manufacturer}\n${device.name ?: ""}" else device.name
                dataParameter.text = type.toProperString()
                scanCount.text = if (device.scanCount > 0) {
                    scanCount.visible = true
                    R.string.times.getString(device.scanCount.format(1), device.rssi)
                } else {
                    scanCount.visible = false
                    ""
                }
                if (type == DataType.POSITION_QUALITY) {
                    dataValue.setText(NmeaFixQuality.fromId(data.getInt(device.address, DataType.POSITION_QUALITY, 0)).toString())
                    dataUnit.text = ""
                } else {
                    val valueParts = data.toValueStringParts(device.address, type)
                    if (valueParts != null) {
                        if (valueParts.size >= 2) {
                            dataValue.setText(valueParts[0])
                            dataUnit.text = valueParts[1] ?: ""
                        } else if (valueParts.size == 1) {
                            dataValue.setText(valueParts[0])
                            dataUnit.text = ""
                        }
                    }
                }
            }
        }
    }

    companion object {
        var firstTime = true
    }
}
