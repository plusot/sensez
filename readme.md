# Android SensezLib library and Meterz app
The SensezLib library is an Android library for easy use of Bluetooth, Bluetooth Low Energy, ANT+ and the devices internal sensors in a 
generalized way. Your app only needs to implement a single listener interface to receive information on the found sensors and after
selection of these sensors listen for the data these sensors supply.
Meterz is an app that makes full usage of the library. Meterz is focused on supplying all kind of data of interest to recreational, amateur 
and professional runners, walkers and cyclists. And to their friends who can follow them live.
SensezDemo is a simple app to show you how to use the basics of SensezLib library.

## Measure
The Sensez library is meant to measure all sorts of data provided by ANT+, Bluetooth, Bluetooth Low Energy (BLE) and your device's internal sensors. 
It is mainly focused at measuring while doing an activity. But you can just as well use it to monitor a Bluetooth weather station. Or to
read data from a blood pressure testing device.
To name a few things you can measure:

* Speed, 
* steps, 
* distance, 
* activity time, 
* wind speed, 
* humidity, 
* temperature, 
* air pressure, 
* air density, 
* slope, 
* cadence, 
* heart rate, 
* bicycle power, 
* battery level of devices and sensors, 
* altitude, 
* blood pressure, 
* weight. 

To supply all this data many BLE and ANT+ devices have been implemented in de library. For BLE this means implementing GATT
characteristics. Many GATT characteristics have been implemented. But also many have not. If you are interested to contribute, 
please mail me on peter@plusot.com. You may write your own Gatt Characteristic plugin as is explained below.

## Gradle
The SensezLib library is hosted by jCenter and can be include with:

```gradle
dependencies {
    implementation 'com.plusot.sensez:sensez:1.7.2'
}
```
    
SensezLib itself depends on: 

```gradle
dependencies {
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'org.jetbrains.kotlin:kotlin-stdlib-jre7:1.3.50'
    implementation('io.socket:socket.io-client:1.0.0') {
        exclude group: 'org.json', module: 'json'
    }
}
```
    
SensezLib is built with Java 8 and Kotlin.

```gradle
...
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'

android {
    ... 
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

dependencies {
    ...
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
}
```
    
## Implementing the SensezLib library
To explain how to use the library it is easiest to explain the SensezDemo app. The SensezDemo app is written in
Kotlin, but can probably be read very well by Java programmers too.

First of all we need to create the library and delegate some events to the library. Also we want to listen for sensors and 
sensor data by implementing the Man.Listener interface.

#### The Man.Listener interface
We can implement the Man.listener interface in the MainActivity:

```kotlin
class MainActivity : Activity(), Man.Listener
```
    
This gives us 4 methods we need to implement (see later on):

```kotlin
fun onDeviceScanned(device: ScannedDevice?, isNew: Boolean) 
fun onDeviceState(device: ScannedDevice?, state: Device.StateInfo?) 
fun onScanning(scanType: ScanManager.ScanType?, on: Boolean) 
fun onDeviceData(device: ScannedDevice?, data: com.plusot.util.data.Data?, isNew: Boolean) 
``` 

#### onCreate
To create the SensezLib library we need to give it a reference to the application instance and the 
current activity. We have a choice of options to choose. Here we have chosen to override the preference 
on which technologies to use in the detection of sensors. The Meterz app e.g. gives the user of the app
the possibility to choose the technologies themselves by implementing a settings dialog for the related 
preferences. START_IMMEDIATELY makes that the library starts looking for sensors and their data immediately. 
In a sports app this is not always the require behaviour. Yo may want to have your user press a button to 
start an activity.
The "null" makes that SensezLib decides by itself which permissions it needs to ask for. You can override 
this behaviour and define the permissions to ask for yourselves.

```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    ...
    SensezLib.create(
            application,
            this,
            EnumSet.of(
                    SensezLib.Option.OVERRIDE_SENSOR_PREFERENCES,
                    SensezLib.Option.USE_INTERNAL_SENSORS,
                    SensezLib.Option.USE_ANT_SENSORS,
                    SensezLib.Option.USE_BT_SENSORS,
                    SensezLib.Option.USE_BLE_SENSORS,
                    SensezLib.Option.START_IMMEDIATELY)
            )
}
```
    
#### onResume
On the onResume event, we connect to the Man.Listener.

```kotlin
override fun onResume() {
    ...
    Man.addListener(this)
}
```

#### onPause
In the onPause we release the connection to the Man.Listener. If the app is finishing we need to close 
the library.

```kotlin
override fun onPause() {
    ...
    Man.removeListener(this)
    if (isFinishing) SensezLib.close()
}
```
    
## The sensor events
The ANT+, Bluetooth and internal sensors (also called devices) will generate events when scanned and when
delivering data.

#### onDeviceScanned
When a sensor is scanned by the library this event will be triggered. This will happen many times. You can 
decide here whether or not the sensor should start to deliver data by setting the isSelected property.

```kotlin
override fun onDeviceScanned(device: ScannedDevice?, isNew: Boolean) {
    SleepAndWake.runInMain({scanned.text = getString(R.string.scanned, device?.name)})
    if (device?.deviceType == DeviceType.INTERNAL_ACCELEROMETER) device.isSelected = true
}
```
    
You can also select which devices you want to receive data from by using the built in devices dialog
    
```kotlin
hamburger.setOnClickListener {
    startActivity(Intent(this, DeviceListActivity::class.java))
}
```
        
    
#### onDeviceState
With this event you receive information on whether or not the device is connected.

```kotlin
override fun onDeviceState(device: ScannedDevice?, state: Device.StateInfo?) {
    LLog.i("Device ${device.toString()} state = $state")
}
```

#### onDeviceScanning
With this event you receive information on what technology the library is currently scanning.

```kotlin
override fun onScanning(scanType: ScanManager.ScanType?, on: Boolean) {
    SleepAndWake.runInMain({scanning.text = getString(R.string.scanning, scanType.toString())})
}
```

#### onDeviceData
When the sensor is selected it will start to send data to your app. You will receive this data in the 
onDeviceData event. The Data object may contain data of many different DataTypes. E.g. data received from the
GPS may contain the location, altitude, number of satellites in view, etc.

```kotlin
override fun onDeviceData(device: ScannedDevice?, data: com.plusot.util.data.Data?, isNew: Boolean) {
    ...
    if (data == null) return
    for (type in data.dataTypes) {
        ...
        SleepAndWake.runInMain({
            dataDevice.text = device?.name
            dataParameter.text = type.toProperString()
            val valueParts = data.toValueStringParts(device?.address, type)
            if (valueParts != null) {
                if (valueParts.size >= 2) {
                    dataValue.setText(valueParts[0])
                    dataUnit.text = valueParts[1] ?: ""
                } else if (valueParts.size == 1) {
                    dataValue.setText(valueParts[0])
                    dataUnit.text = ""
                }
            }
        })
    }
}
```
    
To get value from the Data object e.g. in double format you can use a method like:

```kotlin
data.getDouble(device?.address, type)
```
    
as is shown in the SensezDemo app to create a smooth line displaying the acceleration of your Android device. Where
type is the DataType you would like to get and device?.address is the unique address of a specific device. 
You may supply the address of the device to get data from a Data object for a specific device. 

The Data object can be merged with another e.g. to provide one Data object containing all current data of to build a 
history of data at specific times by simply creating an object like List<Data>().

```kotlin
val history: MutableList<Data> = mutableListOf()
var currentData = Data()

...

if (data.time > currentData.time + 1000) {
    history.add(currentData)
    currentData = data
} else
    currentData.add(data)
```
    
The Data object has many other useful features.

#### GATT Characteristic plugin
To write a plugin for a Bluetooth Low Energy GATT characteristic:

```kotlin
GattAttribute.BATTERY_LEVEL.setParseDelegate { gatt, characteristic ->
    LLog.i("Characteristic value to parse: ${StringUtil.toHex(characteristic.value)}")
    Data().add(gatt.device.address, DataType.BATTERY_LEVEL, 0.01 * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0))
}
```
    
The parsing lambda needs to return on object of type Data or null. You may add multiple DataType / values to the Data object. 

You can also preview the characteristic and let it be handle by the default implementation
    
```kotlin
GattAttribute.HEART_RATE_MEASUREMENT.setParseDelegate { gatt, characteristic ->
    LLog.i("*** Characteristic value to parse: ${StringUtil.toHex(characteristic.value)} ***")
    GattAttribute.HEART_RATE_MEASUREMENT.parse(gatt, characteristic)
}
```
    
Suppose you want to add your own calculate data to those calculated by the default implementation:

```kotlin
GattAttribute.BATTERY_LEVEL.setParseDelegate { gatt, characteristic ->
    LLog.i("Characteristic value to parse: ${StringUtil.toHex(characteristic.value)}")
    val data = Data().add(gatt.device.address, DataType.BATTERY_VOLTAGE, 0.033 * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0))
    data.add(GattAttribute.BATTERY_LEVEL.parse(gatt, characteristic))
}
``` 
    
## Meterz and the web
The Meterz app by default sends all data to the Meterz website. The server displays all your data in graphs on a map.
The source of this web site are available on: [MeterzServe on Bitbucket](https://bitbucket.org/plusot/meterzserve)


## Meterz and SensezLib depend on ...
Meterz uses [Osmdroid](https://github.com/osmdroid/osmdroid) and [OpenStreetMap](https://www.openstreetmap.org) to draw maps, 
[Sqlite](https://www.sqlite.org/) and [NeDB](https://github.com/louischatriot/nedb) to store data, 
[Node.js](https://nodejs.org) for scripting the server.

To build and distribute the AAR library I followed the blog of: [The Cheese Factory](https://inthecheesefactory.com/blog/how-to-upload-library-to-jcenter-maven-central-as-dependency/en)